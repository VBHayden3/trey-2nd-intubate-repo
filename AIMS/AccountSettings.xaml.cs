﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Npgsql;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;

namespace AIMS
{
	public partial class AccountSettings
	{
        string firstName, lastName, username, email, password, newPassword, securityQuestion, securityAnswer,
            newFirstName, newLastName, newEmail, newSecurityQuestion, newSecurityAnswer;

		public AccountSettings()
		{
			this.InitializeComponent();
		}


        // *************** EVENT HANDLERS ***************

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            /*
            // load current account object with ((App)App.Current).CurrentAccount
            // for now just load my profile into Current Account (lines 39-60) can be removed once implemented with project***********

            DatabaseManager.OpenConnection();
            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand(); //new NpgsqlCommand(query, DatabaseManager.Connection);

            command.CommandType = CommandType.Text;
            command.CommandText = @"SELECT uid FROM accounts WHERE username = :username;";
            command.Parameters.Add("username", NpgsqlTypes.NpgsqlDbType.Text).Value = "tiffany.nowalski@simisinc.com";

            int id = 0;

            try
            {
                id = (int)command.ExecuteScalar();  // read the selected id
            }
            catch
            {
                // no account with that username/password is in our database
            }

            DatabaseManager.CloseConnection();

            Account acct = Account.GetAccountFromAccountID(id, true);  // get the account from the database, true = close the database connection
            ((App)App.Current).CurrentAccount = acct;   // store the account object in the currently running App so that it can be accessed from anywhere within the application.
            
            */

            // loads page with content of the current account
            Account account = ((App)App.Current).CurrentAccount;
            Student student = ((App)App.Current).CurrentStudent;/*Student.GetStudentFromAccountID(((App)App.Current).CurrentAccount.ID, true);    // get the student from the database, true = close the database connection*/

            if (student != null)
            {
                firstName = newFirstName = student.FirstName;
                lastName = newLastName = student.LastName;

                if (student.Account != null)
                {
                    username = student.Account.Username;
                    email = newEmail = student.Account.Email;
                    securityQuestion = newSecurityQuestion = student.Account.SecurityQuestion;
                    securityAnswer = newSecurityAnswer = student.Account.SecurityAnswer;
                    password = newPassword = student.Account.Password;
                }
            }

            LastName.Text = lastName;
            FirstName.Text = firstName;
            FirstNameBox.Text = firstName;
            LastNameBox.Text = lastName;
            UsernameBox.Text = username;
            EmailBox.Text = email;
            SecurityQuesBox.Text = securityQuestion;
            SecurityAnswBox.Text = securityAnswer;
        }

        /// <summary>
        /// - Checks that all form fields have been completed, are valid, and match
        /// - If incorrect -> display warning label to complete fields
        /// - If correct -> Update Account and Student information in the database
        /// </summary>
        private void SaveButtonClick(object sender, RoutedEventArgs e)
        {
            if (!CheckNameEmpty() && !CheckEmailEmpty() && !CheckEmailInvalid()
                && !CheckSecurityQuestionEmpty() && !CheckSecurityAnswerEmpty()  & !CheckPasswordFieldsIncomplete())
            {
                emptyFieldWarningLabel.Visibility = Visibility.Hidden;
                Account.UpdateAccount(newEmail, newPassword, newSecurityQuestion, newSecurityAnswer);
                Student.UpdateStudentLinkedToAccount(newFirstName, newLastName);

                this.Page.NavigationService.Navigate(new Uri("../MainPage3.xaml", UriKind.Relative));
            }
            else
                emptyFieldWarningLabel.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// - Checks that all form fields have been completed, are valid, and match
        /// - If incorrect -> display warning label to complete fields
        /// - If correct -> Update Account and Student information in the database
        /// </summary>
        private void ChoosePhotoButtonClick(object sender, RoutedEventArgs e)
        {
            //System.Diagnostics.Process.Start("explorer.exe", Environment.GetFolderPath(Environment.SpecialFolder.MyPictures));
            /*Bitmap img = new Bitmap("Assets/Graphics/FemaleUser.png");
            ImageConverter converter = new ImageConverter();
            byte[] pixeldata = (byte[])converter.ConvertTo(img, typeof(byte[]));
            BitmapSource bmap = BitmapSource.Create(img.Width, img.Height, 96, 96, PixelFormats.Bgr32, null, pixeldata, img.Width * img.BytesPerPixel);
            image1.Source = bmap;*/
        }

        /// <summary>
        /// Button handler, checks for Caps Lock and checks if the Enter button has been hit.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.CapsLock)  // CapsLock key toggles on/off the caps lock warning message.
            {
                CheckCapsLock();
            }

            if (e.Key == Key.Enter) // Enter key submits the current password input.
            {
                SaveButtonClick(null, null);
            }
        }

        private void FirstNameBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            FirstName.Text = FirstNameBox.Text;
            newFirstName = FirstNameBox.Text;

            CheckNameEmpty();
        }

        private void LastNameBox_TextChanged(object sender, RoutedEventArgs e)
        {
            LastName.Text = LastNameBox.Text;
            newLastName = LastNameBox.Text;

            CheckNameEmpty();
        }

        private void LastNameCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            CheckNameEmpty();
        }

        private void EmailBox_TextChanged(object sender, RoutedEventArgs e)
        {
            newEmail = EmailBox.Text;

            CheckEmailEmpty();
            CheckEmailInvalid();
        }

        private void SecurityQuesBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            newSecurityQuestion = SecurityQuesBox.Text;

            CheckSecurityQuestionEmpty();
        }

        private void SecurityAnswBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            newSecurityAnswer = SecurityAnswBox.Text;

            CheckSecurityAnswerEmpty();
        }

        private void OldPasswordBox_TextChanged(object sender, RoutedEventArgs e)
        {
            CheckOldPasswordEmpty();
            CheckOldPasswordInvalid();
        }

        private void NewPasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            CheckNewPasswordEmpty();
            CheckPasswordMismatch();
        }

        private void NewPasswordBox_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            CheckNewPasswordEmpty();
            CheckPasswordMismatch();
        }

        private void ConfirmPasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            CheckConfirmPasswordEmpty();
            CheckPasswordMismatch();
        }

        private void ConfirmPasswordBox_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            CheckConfirmPasswordEmpty();
            CheckPasswordMismatch();
        }


        // *************** FUNCTIONS ***************

        /// <summary>
        /// Checks if the CapsLock key is on
        /// </summary>
        /// <returns></returns>
        private void CheckCapsLock()
        {
            if (Console.CapsLock)
            {
                // Warn that the caps lock key is enabled.
                capsLockWarningLabel.Content = "* Caps Lock enabled";
                capsLockWarningLabel.Visibility = Visibility.Visible;
            }
            else
            {
                capsLockWarningLabel.Content = "";
                capsLockWarningLabel.Visibility = Visibility.Hidden;
            }
        }

        /// <summary>
        /// Checks if the first or last name boxes were left empty, or if it is simply white space.
        /// </summary>
        /// <returns></returns>
        private bool CheckNameEmpty()
        {
            if (emptyFirstNameWarningLabel == null || emptyLastNameWarningLabel == null)
                return true;
            if (String.IsNullOrWhiteSpace(FirstNameBox.Text) && !String.IsNullOrWhiteSpace(LastNameBox.Text))
            {
                emptyFirstNameWarningLabel.Visibility = Visibility.Visible;
                emptyLastNameWarningLabel.Visibility = Visibility.Hidden;
                emptyNameWarningLabel.Visibility = Visibility.Hidden;
                return true;
            }
            if (!String.IsNullOrWhiteSpace(FirstNameBox.Text) && String.IsNullOrWhiteSpace(LastNameBox.Text))
            {
                emptyFirstNameWarningLabel.Visibility = Visibility.Hidden;
                emptyLastNameWarningLabel.Visibility = Visibility.Visible;
                emptyNameWarningLabel.Visibility = Visibility.Hidden;
                return true;
            }
            if (String.IsNullOrWhiteSpace(FirstNameBox.Text) && String.IsNullOrWhiteSpace(LastNameBox.Text))
            {
                emptyFirstNameWarningLabel.Visibility = Visibility.Hidden;
                emptyLastNameWarningLabel.Visibility = Visibility.Hidden;
                emptyNameWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                emptyFirstNameWarningLabel.Visibility = Visibility.Hidden;
                emptyLastNameWarningLabel.Visibility = Visibility.Hidden;
                emptyNameWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// Checks if the email box was left empty, or if it is simply white space.
        /// </summary>
        /// <returns></returns>
        private bool CheckEmailEmpty()
        {
            if (emptyEmailWarningLabel == null)
                return true;
            if (String.IsNullOrWhiteSpace(EmailBox.Text))
            {
                emptyEmailWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                emptyEmailWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// method for determining is the user provided an invalid email address
        /// We use regular expressions in this check, as it is a more thorough
        /// way of checking the address provided
        /// </summary>
        /// <returns>true is invalid, false if valid</returns>
        public bool CheckEmailInvalid()
        {
            if (invalidEmailWarningLabel == null)
                return true;
            if (!String.IsNullOrWhiteSpace(EmailBox.Text))
            {
                //regular expression pattern for valid email
                //addresses, allows for the following domains:
                //com,edu,info,gov,int,mil,net,org,biz,name,museum,coop,aero,pro,tv
                string pattern = @"^[-a-zA-Z0-9][-.a-zA-Z0-9]*@[-.a-zA-Z0-9]+(\.[-.a-zA-Z0-9]+)*\.(com|edu|info|gov|int|mil|net|org|biz|name|museum|coop|aero|pro|tv|[a-zA-Z]{2})$";
                //Regular expression object
                Regex check = new Regex(pattern, RegexOptions.IgnorePatternWhitespace);
                //boolean variable to return to calling method
                bool valid;
                //use IsMatch to validate the address
                valid = check.IsMatch(EmailBox.Text);
                if (!valid)
                {
                    invalidEmailWarningLabel.Visibility = Visibility.Visible;
                    return true;
                }
                else
                {
                    invalidEmailWarningLabel.Visibility = Visibility.Hidden;
                    return false;
                }
            }
            else
            {
                invalidEmailWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// Checks if the security question box was left empty, or if it is simply white space.
        /// </summary>
        /// <returns></returns>
        private bool CheckSecurityQuestionEmpty()
        {
            if (emptySecurityQuestionWarningLabel == null)
                return true;
            if (String.IsNullOrWhiteSpace(SecurityQuesBox.Text))
            {
                emptySecurityQuestionWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                emptySecurityQuestionWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// Checks if the security answer box was left empty, or if it is simply white space.
        /// </summary>
        /// <returns></returns>
        private bool CheckSecurityAnswerEmpty()
        {
            if (emptySecurityAnswerWarningLabel == null)
                return true;
            if (String.IsNullOrWhiteSpace(SecurityAnswBox.Text))
            {
                emptySecurityAnswerWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                emptySecurityAnswerWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// Checks if the old password box was left empty, or if it is simply white space.
        /// </summary>
        /// <returns></returns>
        private bool CheckOldPasswordEmpty()
        {
            if (emptyOldPasswordWarningLabel == null)
                return true;
            if (String.IsNullOrWhiteSpace(OldPasswordBox.Password))
            {
                emptyOldPasswordWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                emptyOldPasswordWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// Checks if the old password box entered is invalid.
        /// </summary>
        /// <returns></returns>
        private bool CheckOldPasswordInvalid()
        {
            if (invalidOldPasswordWarningLabel == null)
                return true;
            if (!String.IsNullOrWhiteSpace(OldPasswordBox.Password) && OldPasswordBox.Password != password)
            {
                invalidOldPasswordWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            if (!String.IsNullOrWhiteSpace(OldPasswordBox.Password) && OldPasswordBox.Password == password)
            {
                NewPasswordBox.IsEnabled = true;
                CheckNewPasswordEmpty();
                ConfirmPasswordBox.IsEnabled = true;
                CheckConfirmPasswordEmpty();

                invalidOldPasswordWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
            else
            {
                invalidOldPasswordWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// Checks if the new password box was left empty, or if it is simply white space.
        /// </summary>
        /// <returns></returns>
        private bool CheckNewPasswordEmpty()
        {
            if (emptyNewPasswordWarningLabel == null)
                return true;
            if (String.IsNullOrWhiteSpace(NewPasswordBox.Password))
            {
                emptyNewPasswordWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                emptyNewPasswordWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// Checks if the confirm password box was left empty, or if it is simply white space.
        /// </summary>
        /// <returns></returns>
        private bool CheckConfirmPasswordEmpty()
        {
            if (emptyConfirmPasswordWarningLabel == null)
                return true;
            if (String.IsNullOrWhiteSpace(ConfirmPasswordBox.Password))
            {
                emptyConfirmPasswordWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                emptyConfirmPasswordWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// Checks if the password boxes match
        /// </summary>
        /// <returns></returns>
        private bool CheckPasswordMismatch()
        {
            if (mismatchPasswordWarningLabel == null)
                return true;
            if (!NewPasswordBox.Password.Equals(ConfirmPasswordBox.Password) && !String.IsNullOrWhiteSpace(NewPasswordBox.Password) && !String.IsNullOrWhiteSpace(ConfirmPasswordBox.Password))
            {
                mismatchPasswordWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            if (NewPasswordBox.Password.Equals(ConfirmPasswordBox.Password) && !String.IsNullOrWhiteSpace(NewPasswordBox.Password) && !String.IsNullOrWhiteSpace(ConfirmPasswordBox.Password))
            {
                newPassword = ConfirmPasswordBox.Password;

                mismatchPasswordWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
            else
            {
                mismatchPasswordWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// Checks if the password fields are incomplete
        /// </summary>
        /// <returns></returns>
        private bool CheckPasswordFieldsIncomplete()
        {
            if (!String.IsNullOrWhiteSpace(OldPasswordBox.Password))
            {
                if (!CheckOldPasswordInvalid() && !CheckNewPasswordEmpty() && !CheckConfirmPasswordEmpty() && !CheckPasswordMismatch())
                    return false;
                else
                    return true;
            }
            else
                return false;
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            this.Page.NavigationService.Navigate(new Uri("../MainPage3.xaml", UriKind.Relative));
        }
	}
}