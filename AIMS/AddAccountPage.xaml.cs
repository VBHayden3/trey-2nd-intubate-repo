﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Npgsql;

namespace AIMS
{
    /// <summary>
    /// Interaction logic for AddAccountPage.xaml
    /// </summary>
    public partial class AddAccountPage : Page
    {
        private string host = "172.16.10.83";
        private string port = "5432";
        private string user = "aims";
        private string password = "@1m$";
        private string databasename = "aims";

        public AddAccountPage()
        {
            InitializeComponent();
        }

        private void AddAccount()
        {
            string connstring = String.Format("Server={0};Port={1};" + "User Id={2};Password={3};Database={4};", host, port, user, password, databasename);
            NpgsqlConnection db = new NpgsqlConnection(connstring);

            try
            {
                db.Open();
            }
            catch
            {
            }

            if(db.State == System.Data.ConnectionState.Open)
            {

                NpgsqlCommand sql = db.CreateCommand();

                sql.CommandType = System.Data.CommandType.Text;

                sql.CommandText = @"INSERT INTO accounts (UID, Username, Password, AccessLevel, CreationDate, ExpirationDate, ChangePasswordNextLogin, Classes) VALUES ( @UID, @Username, @Password, @AccessLevel, now()::date, @ExpirationDate::date, @ChangePasswordNextLogin, @Classes);";

                sql.Parameters.Add("@UID", NpgsqlTypes.NpgsqlDbType.Integer ).Value = GetAccountID();

                sql.Parameters.Add("@Username", NpgsqlTypes.NpgsqlDbType.Text).Value = GetAccountUsername();

                sql.Parameters.Add("@Password", NpgsqlTypes.NpgsqlDbType.Text).Value = GetAccountPassword();

                sql.Parameters.Add("@AccessLevel", NpgsqlTypes.NpgsqlDbType.Smallint).Value = GetAccountAccessLevel();

                sql.Parameters.Add("@ExpirationDate", NpgsqlTypes.NpgsqlDbType.Date).Value = GetAccountExpirationDate();

                sql.Parameters.Add("@ChangePasswordNextLogin", NpgsqlTypes.NpgsqlDbType.Boolean).Value = GetAccountChangePasswordNextLogin();

                sql.Parameters.Add("@Classes", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Integer ).Value = GetAccountClasses();

                try
                {
                    if (sql.ExecuteNonQuery() != 0)
                    {
                        MessageBox.Show(String.Format(@"New User {0} had beem created successfully.", GetAccountUsername() )); //Try don't use + with strings, is litle to slow
                    }
                }

                catch( Exception except )
                {
                    MessageBox.Show(except.ToString());
                }
            } 
        }

        private int GetAccountID()
        {
            int id = 0;

            if (!String.IsNullOrEmpty(IDTextBox.Text))
            {
                Int32.TryParse(IDTextBox.Text, out id); // attempts to parse the string into an int, and then sets "id" to that parsed value, if it worked.
            }

            return id;
        }

        private string GetAccountUsername()
        {
            string username = "";

            if (!String.IsNullOrEmpty(UsernameTextBox.Text))
                username = UsernameTextBox.Text;

            return username;
        }

        private string GetAccountPassword()
        {
            string pw = "";

            if (!String.IsNullOrEmpty(PasswordTextBox.Text))
                pw = PasswordTextBox.Text;

            //encrpyt pw before storing in DB

            return pw;
        }

        private int GetAccountAccessLevel()
        {
            int accessLevel = 0;

            string val = ((ComboBoxItem)AccessLevelComboBox.SelectedValue).Content.ToString();

            switch (val)
            {
                case "User": accessLevel = 0; break;
                case "Doctor": accessLevel = 1; break;
                case "Admin": accessLevel = 2; break;

                default: accessLevel = 0; break;
            }

           // MessageBox.Show(String.Format("{0}: {1}", accessLevel, val));

            return accessLevel;
        }

        private DateTime GetAccountCreationDate()
        {
            return DateTime.Now;
        }

        private DateTime GetAccountExpirationDate()
        {
            return DateTime.MaxValue;
        }

        private bool GetAccountChangePasswordNextLogin()
        {
            return false;
        }

        private int[] GetAccountClasses()
        {
            int[] classes = new int[]{-1};

            if (!String.IsNullOrEmpty(ClassesTextBox.Text))
            {
                int clss = 0;
                if (Int32.TryParse(ClassesTextBox.Text, out clss))
                    classes = new int[] { clss };
            }

            return classes;
        }

        private void CreateAccountButton_Clicked(object sender, RoutedEventArgs e)
        {
            AddAccount();
        }
    }
}
