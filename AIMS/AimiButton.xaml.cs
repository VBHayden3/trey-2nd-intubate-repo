﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIMS.Components
{
	/// <summary>
	/// Interaction logic for AimiButton.xaml
	/// </summary>
	public partial class AimiButton : UserControl
	{
        private List<Image> allImages = new List<Image>();

		public AimiButton()
		{
			this.InitializeComponent();

            this.Loaded += AimiButton_Loaded;
            this.Unloaded += AimiButton_Unloaded;

            this.allImages.Add(this.AIMI_Off_Image);
            this.allImages.Add(this.AIMI_On_Image);
            this.allImages.Add(this.AIMI_Talking_Image);
            this.allImages.Add(this.AIMI_Warning_Image);

		}

        private void AimiButton_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (KinectManager.KinectSpeechCommander != null)
                {
                    KinectManager.KinectSpeechCommander.ActiveListeningModeChanged += KinectSpeechCommander_ActiveListeningModeChanged;
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void KinectSpeechCommander_ActiveListeningModeChanged(object sender, Speech.ActiveListeningModeChangedEventArgs e)
        {
            if (e.ActiveListeningModeEnabled)
            {
                // Keyword on.
                this.SetShowingImage(this.AIMI_On_Image);
            }
            else
            {
                // Keyword off.
                this.SetShowingImage(this.AIMI_Off_Image);
            }
        }

        private void AimiButton_Unloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (KinectManager.KinectSpeechCommander != null)
                {
                    KinectManager.KinectSpeechCommander.ActiveListeningModeChanged -= KinectSpeechCommander_ActiveListeningModeChanged;
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void SetShowingImage(Image image)
        {
            foreach (Image i in allImages)
            {
                if (i == image)
                {
                    if( i.Visibility != System.Windows.Visibility.Visible )
                        i.Visibility = Visibility.Visible;  // Set the visibility if it isn't set already.
                }
                else
                {
                    if( i.Visibility != System.Windows.Visibility.Hidden )
                        i.Visibility = Visibility.Hidden;   // Hide all others.
                }
            }
        }
	}
}