﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.IO;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Navigation;
using System.Diagnostics;


namespace AIMS
{
    public enum Task
    {
        None = 0,
        Intubation = 1,
        VerticalLift = 2,
        LateralTransfer = 3,
        CPR,
        SafePatientHandling
    }

    public enum AccessLevel
    {
        User = 0,
        Doctor,
        Administrator
    }

    public enum AppStartupMode
    {
        LoginScreen = 0,
        StudentDashboard,
        TaskTrainer,
        PhysicalTherapy,
        Test,
        Results,
        AddAccount,
        TestResult,
        ODU_Dev,
        AccountCreation,
        AccountSettings,
        DataLogger,
        ColorCalibration,
        SkeletonPathComparison,
        CircularProgressBar,
        DevTools,
        TaskFeedback,
        TempPage,
        MainWindow
    };

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        // TREY: Pointless get/set but why not do in 7 lines what can be done in 3 :-J
        private bool hasCheckPulseLicense = false;
        private bool hasAEDLicense = false;
        private bool hasTrialLicense = false;

        public bool HasCheckPulseLicense { get { return hasCheckPulseLicense; } set { hasCheckPulseLicense = value; } }
        public bool HasAEDLicense { get { return hasAEDLicense; } set { hasAEDLicense = value; } }
        public bool HasTrialLicense { get { return hasTrialLicense; } set { hasTrialLicense = value; } }

        // TREY: LICENSING FROM CPR
        public IntPtr rh = IntPtr.Zero;
        public IntPtr lic = IntPtr.Zero;
        public IntPtr timeLic = IntPtr.Zero;

        public int license_days_remaining = 0;

        // Startup mode of the application
        public readonly AppStartupMode AppStartupMode = AppStartupMode.MainWindow;    // Set to LoginScreen mode for production
        //public readonly AppStartupMode AppStartupMode = AppStartupMode.DevTools;

        //public readonly AppStartupMode AppStartupMode = AppStartupMode.Test; // DEBUG
        //public readonly AppStartupMode AppStartupMode = AppStartupMode.ColorCalibration;

        public bool useGreenScreen = false;
        public bool DoHandle = false;

        private Task currentTask = Task.None;
        private TaskMode currentTaskMode = TaskMode.Practice;
        private MasteryLevel currentMasteryLevel = MasteryLevel.Novice;
        private Account currentAccount = null;
        private Student currentStudent = null;
        private Doctor currentDoctor = null;
        private Administrator currentAdministrator = null;                                                                                                                                                      

        public Task CurrentTask { get { return currentTask; } set { currentTask = value; } }
        public TaskMode CurrentTaskMode { get { return currentTaskMode; } set { currentTaskMode = value; } }
        public MasteryLevel CurrentMasteryLevel { get { return currentMasteryLevel; } set { currentMasteryLevel = value; } }
        public AccessLevel CurrentUserAccessLevel { get { return currentAccount == null ? AccessLevel.User : currentAccount.AccessLevel; } }
        public string CurrentUserName { get { return currentAccount == null ? null : currentAccount.Username; } }
        public int CurrentUserID { get { return currentAccount == null ? 0 : currentAccount.ID; } }
        public string CurrentUserEmail { get { return currentAccount == null ? null : currentAccount.Username; } }
        public string CurrentUserFirstName
        {   
            get
            {
                if( CurrentAdministrator != null )
                    return currentAdministrator.FirstName;
                if (CurrentDoctor != null)
                    return currentDoctor.FirstName;
                if (CurrentStudent != null)
                    return currentStudent.FirstName;
                return null;
            }
        }

        public string CurrentUserLastName
        {
            get
            {
                if (CurrentAdministrator != null)
                    return currentAdministrator.LastName;
                if (CurrentDoctor != null)
                    return currentDoctor.LastName;
                if (CurrentStudent != null)
                    return currentStudent.LastName;
                return null;
            }
        }

        public string CurrentUserOrganizationName
        {
            get
            {
                if (CurrentAdministrator != null && CurrentAdministrator.Organization != null)
                    return currentAdministrator.Organization.Name;
                if (CurrentDoctor != null && CurrentDoctor.Organization != null)
                    return currentDoctor.Organization.Name;
                if (CurrentStudent != null && CurrentStudent.Organization != null )
                    return currentStudent.Organization.Name;
                return null;
            }
        }

        public Account CurrentAccount { get { return currentAccount; } set { currentAccount = value; } }
        public Student CurrentStudent { get { return currentStudent; } set { currentStudent = value; } }
        public Doctor CurrentDoctor { get { return currentDoctor; } set { currentDoctor = value; } }
        public Administrator CurrentAdministrator { get { return currentAdministrator; } set { currentAdministrator = value; } }

        void App_Navigated(object sender, NavigationEventArgs e)
        {
            try
            {
                NavigationWindow ws = (e.Navigator as NavigationWindow);

                if (ws != null)
                {
                    ws.ShowsNavigationUI = false;

                    ws.RemoveBackEntry();   // prevent moving backwards in the application.
                }
            }
            catch(Exception exc)
            {
                System.Diagnostics.Debug.Print("App_Navigated AIMS.App ERROR: {0}", exc.Message);
            }
        }

        private void ApplicationStartup(object sender, StartupEventArgs e)
        {
            this.Navigated += new NavigatedEventHandler(App_Navigated);

            currentTask = Task.None;
            currentTaskMode = TaskMode.Practice;

            // Select Layout Mode
            switch (AppStartupMode)
            {
                case AppStartupMode.AddAccount:
                {
                    this.StartupUri = new Uri(@"../../AddAccountPage.xaml", UriKind.Relative);
                    break;
                }
                case AppStartupMode.AccountCreation:
                {
                    this.StartupUri = new Uri(@"../../UserAccountCreation.xaml", UriKind.Relative);
                    break;
                }
                case AppStartupMode.AccountSettings:
                {
                    this.StartupUri = new Uri(@"../../AccountSettings.xaml", UriKind.Relative);
                    break;
                }
                case AppStartupMode.Results:
                {
                    this.StartupUri = new Uri(@"../../StudentResultsPage.xaml", UriKind.Relative);
                    break;
                }
                case AppStartupMode.StudentDashboard:
                {
                    this.StartupUri = new Uri(@"../../StudentDashboard.xaml", UriKind.Relative);
                    break;
                }
                case AppStartupMode.LoginScreen:
                {
                    this.StartupUri = new Uri(@"../../LoginPage.xaml", UriKind.Relative);
                    break;
                }
                case AppStartupMode.TaskTrainer:
                {
                    this.StartupUri = new Uri(@"../../Tasks/Intubation/IntubationPage.xaml", UriKind.Relative);
                    break;
                }
                case AppStartupMode.PhysicalTherapy:
                {
                    this.StartupUri = new Uri(@"../../PhysicalTherapy.xaml", UriKind.Relative);
                    break;
                }
                case AppStartupMode.Test:
                {
                    this.StartupUri = new Uri(@"../../TestPage.xaml", UriKind.Relative);
                    break;
                }
                case AppStartupMode.TestResult:
                {
                    this.StartupUri = new Uri(@"../../Components/Results.xaml", UriKind.Relative);
                    break;
                }
                case AppStartupMode.ODU_Dev:
                {
                    this.StartupUri = new Uri(@"../../ODU_Dev/DevelopmentPage.xaml", UriKind.Relative);
                    break;
                }
                case AppStartupMode.DataLogger:
                {
                    this.StartupUri = new Uri(@"../../Tasks/DataLogger/DataLoggerPage.xaml", UriKind.Relative);
                    break;
                }
                case AppStartupMode.ColorCalibration:
                {
                    this.StartupUri = new Uri(@"../../Pages/ColorCalibrationPage.xaml", UriKind.Relative);
                    break;
                }
                case AIMS.AppStartupMode.SkeletonPathComparison:
                {
                    this.StartupUri = new Uri(@"../../Pages/SkeletonPathComparisonPage.xaml", UriKind.Relative);
                    break;
                }
                case AIMS.AppStartupMode.CircularProgressBar:
                {
                    this.StartupUri = new Uri(@"../../ProgressBarPage.xaml", UriKind.Relative);
                    break;
                }
                case AppStartupMode.DevTools:
                {
                    this.StartupUri = new Uri(@"../../DevTools.xaml", UriKind.Relative);
                    break;
                }
                case AppStartupMode.TaskFeedback:
                {
                    this.StartupUri = new Uri(@"../../Pages/TaskFeedbackPage.xaml", UriKind.Relative);
                    break;
                }
                case AppStartupMode.TempPage:
                {
                    this.StartupUri = new Uri("@../../TempPage.xaml", UriKind.Relative);
                    break;
                }
                case AppStartupMode.MainWindow:
                {
                    this.StartupUri = new Uri("@../../MainWindow.xaml", UriKind.Relative);
                    break;
                }
                default:
                    throw(new Exception("Startup Mode Not Recognized"));
            }
            //kinectSensorManager = new KinectSensorManager();
            
            // Initialize KinectManager
            KinectManager.Startup();

            ResourceDictionary dictionary = new ResourceDictionary();
            dictionary.Source = new Uri(@"../../Assets/Styles/Default.xaml", UriKind.Relative);
            Resources.MergedDictionaries.Add(dictionary);
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            KinectManager.Shutdown();

            System.Diagnostics.Debug.Print("AIMS Application_Exit(): " + e.ApplicationExitCode.ToString());
        }


        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            KinectManager.Shutdown();

            if (this.DoHandle)
            {
                // Display error message and leave the application open.
                MessageBox.Show(e.Exception.Message + e.Exception.StackTrace, "Exception Caught", MessageBoxButton.OK, MessageBoxImage.Error);
                e.Handled = true;
            }
            else
            {
                // If e.Handled is false, the application will close due to crash.
                System.Diagnostics.Debug.Print("\nAIMS Application_DispatcherUnhandledException(): " + e.Exception.Message + e.Exception.StackTrace + "\nsender type: " + sender.ToString() + "\n");
                e.Handled = false;
            }
        }

        /// <summary>
        /// Returns the ProgramData directory for this product.
        /// 
        /// </summary>
        /// <returns>ie. C:/ProgramData/HealthCare Simulations/AIMSIntubation/</returns>
        public static string GetProgramDataDirectory()
        {
            FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetEntryAssembly().Location);

            string companyName = "HealthCare Simulations";
            string productName = "AIMS Intubation";

            string filePath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData, Environment.SpecialFolderOption.Create) + @"\" + companyName + @"\" + productName;

            DirectoryInfo dir = Directory.CreateDirectory(filePath);

            return filePath;
        }

        // Copied from CPR
        public static void SaveSettings()
        {
            AIMS.Properties.Settings.Default.Save();   // Persist our user settings between builds.
            Debug.WriteLine("Settings Saved.");
        }
    }
}
