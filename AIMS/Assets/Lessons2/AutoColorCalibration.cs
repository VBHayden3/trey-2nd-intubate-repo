﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Diagnostics;
using System.Threading;

using Microsoft.Kinect;

using AIMS.Utilities.Kinect;

namespace AIMS.Assets.Lessons2
{
    class AutoColorCalibration
    {
        #region Events

        /// <summary>
        /// Fires when Analysis begins; FindBestColorRange has started analyzing the ColorRanges.
        /// </summary>
        public event EventHandler AnalysisStarted;
        /// <summary>
        /// Fires when Analysis is finished; FindBestColorRange has found best ColorRange.
        /// EventArgs contain a ColorRange if one was found, otherwise contains null.
        /// </summary>
        public event AnalysisDoneEventArgs AnalysisCompleted;
        /// <summary>
        /// Fires whenever a property on AutoColorCalibration changes.
        /// </summary>
        public event PropertyChangedEventArgs PropertyChanged;

        // *** Delegates for events ***

        /// <summary>
        /// EventArgs for the AutoColorCalibration PropertyChanged event.
        /// </summary>
        /// <param name="sender">AutoColorCalibration instance.</param>
        /// <param name="propertyName">Name of the property which was changed.</param>
        public delegate void PropertyChangedEventArgs(object sender, string propertyName);
        /// <summary>
        /// EventArgs for the AutoColorCalibration AnalysisCompleted event.
        /// </summary>
        /// <param name="sender">AutoColorCalibration instance.</param>
        /// <param name="foundColorRange">ColorRange which was found, or null if not found.</param>
        public delegate void AnalysisDoneEventArgs(object sender, ColorRange foundColorRange);

        #endregion // Events

        #region Properties

        /// <summary>
        /// Amount of frames to analyze.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Throws an exception if set while Analyzing.</exception>
        public int Frames 
        { 
            get { return this._frames; }
            set
            {
                if (this._frames == value)
                    return;
                this.CheckIfAnalyzing();
                this._frames = value;
                this.FirePropertyChanged("Frames");
            }
        }
        /// <summary>
        /// True when analyzing, FindBestColorRange is running, false otherwise. Properties may not be altered while analyzing.
        /// </summary>
        public bool Analyzing 
        { 
            get { return this._analyzing; }
            private set
            {
                if (this._analyzing == value)
                    return;
                this._analyzing = value;
                this.FirePropertyChanged("Analyzing");
            }
        }
        /// <summary>
        /// Get amount of frames which have been collected while analyzing.
        /// </summary>
        public int FramesCollected
        {
            get { return this._framesCollected; }
            private set
            {
                if (this._framesCollected == value)
                    return;
                this._framesCollected = value;
                this.FirePropertyChanged("FramesCollected");
            }
        }
        /// <summary>
        /// Get amount of frames which have been processed while analyzing.
        /// </summary>
        public int FramesProcessed
        {
            get { return this._framesProcessed; }
            private set
            {
                if (this._framesProcessed == value)
                    return;
                this._framesProcessed = value;
                this.FirePropertyChanged("FramesProcessed");
            }
        }

        #endregion // Properties

        #region Fields

        // *** Used to store property value. Edit through property ***
        private bool _analyzing;
        private int _framesCollected;
        private int _framesProcessed;

        // *** Constant fields ***
        private const int DEFAULT_FRAMES = 30;
        private const double CAPTURE_THRESH = 0.85; // %

        // *** Regular fields ***
        private int _frames;
        private List<ColorRange> _colorRanges;
        private Int32Rect _scanArea;
        private ImageData[] _imageDatas;
        private TimeSpan _timeSpan;
        // Dictionary stores a list of lists of BlobClusters for every ColorRange. 
        // The outer list is for the collection of frames.
        // The inner list is a collection of BlobClusters for an individual frame.
        // The dictionary allows a 2D list, per color range, of BlobClusters, for every frame. ( very detailed because it can be confusing )
        private Dictionary<ColorRange, List<List<BlobCluster>>> _rangeClusterDict;

        // *** Static fields ***
        private static object _lockRangeClustDict;
        
        #endregion // Fields

        #region Constructor

        /// <summary>
        /// Create an AutoColorCalibration without specifying a color to use.
        /// </summary>
        /// <param name="frames">Amount of frames to analyze.</param>
        public AutoColorCalibration(int frames = DEFAULT_FRAMES)
        {
            this._frames = frames;
            this._rangeClusterDict = new Dictionary<ColorRange, List<List<BlobCluster>>>();
            AutoColorCalibration._lockRangeClustDict = new object();
        }

        #endregion // Constructor

        #region Public Methods

        /// <summary>
        /// Analyzes ColorRanges for the best ColorRange with color of colorName. Value is returned in the AnalysisCompleted event.
        /// </summary>
        /// <param name="colorName">Name of color to discover the best ColorRange for.</param>
        /// <param name="scanArea">Specific area within the frame to scan for.</param>
        /// <exception cref="system.ArgumentNullException">Thrown if either of the parameters are null or empty.</exception>
        public void FindBestColorRange(string colorName, Int32Rect scanArea)
        {
            if (String.IsNullOrEmpty(colorName))
                throw new ArgumentNullException("colorName", "colorName cannot be null or empty.");

            if (scanArea == null)
                throw new ArgumentNullException("scanArea", "scanArea cannot be null.");

            this._scanArea = scanArea;

            // Get the ColorRanges to use from the ColorTracker.
            this._colorRanges = ColorTracker.GetColorRanges(colorName);

            // Subscribes to the ColorDataReady of KinectManager and
            // calls the process frame function whenever a new frame is ready.
            this.StartAnalyzing();

            // Kill processing if no ColorRanges.
            if (this._colorRanges == null)
            {
                Debug.Print("AutoColorCalibration FindBestColorRange : No ColorRanges with that colorName.");
                this.StopAnalyzing(null);
                return;
            }

            // NOTE: Start and stop events fire even when ColorRanges are null to show that an attempt to auto-calibrate took place.
        }

        #endregion // Public Methods

        #region Private Methods

        #region Processing Private Methods

        /// <summary>
        /// Function to process frames from the kinect. RUNS ON A THREAD
        /// </summary>
        /// <param name="frameIndex">Index of which ImageData to use.</param>
        private void ProcessFrame(object _frameIndex)
        {
            int frameIndex = 0;
            try
            { 
                frameIndex = (int)_frameIndex; 
            }
            catch { new ArgumentException("_frameIndex must be castable as an int.", "_frameIndex"); }

            // No ranges to use, quit analyzing.
            if (this._colorRanges == null)
            {
                Debug.Print("AutoColorCalibration ProcessFrame : No ColorRanges to process against.");
                this.StopAnalyzing(null);
                return;
            }

            // Iterate through ColorRange definitions and generate blob clusters for each.
            foreach (ColorRange colorRange in this._colorRanges)
            {
                try
                {
                    // Need to create a new temp ImageData for each range, or they'll all modify the same ImageData.
                    ImageData tmpImageData = this._imageDatas[frameIndex].Clone();

                    tmpImageData.Scan(colorRange.Hue, colorRange.HueThreshold, colorRange.Saturation, colorRange.SaturationThreshold,
                        colorRange.Value, colorRange.ValueThreshold);
                    
                    tmpImageData.Filter(5, 3);
                    
                    List<Int32Rect> blobs = ColorTracker.PerformCannyBlobDetectionRect(tmpImageData.getBytes(), tmpImageData.width, tmpImageData.height, false);

                    List<BlobCluster> clusters = new List<BlobCluster>();
                    clusters = BlobCluster.GenerateBlobClusters(blobs.ToArray());

                    #region DEBUG OUTPUT
                    //Debug.Print("!!!!!!!!!!!!!!!!!!  PROCESS FRAME, COLORRANGE !!!!!!!!!!!!!!!!!!!!!!! \n {0}", colorRange);
                    //Debug.Print("!!!!!!!!!!!!!!!!!!  PROCESS FRAME, BLOBS !!!!!!!!!!!!!!!!!!!!!!! Count: {0}", blobs.Count);
                    //Debug.Print("!!!!!!!!!!!!!!!!!!  PROCESS FRAME, CLUSTERS !!!!!!!!!!!!!!!!!!!!!!! Count: {0}", clusters == null ? 0 : clusters.Count);
                    //if (clusters != null)
                    //{
                    //    foreach (BlobCluster cluster in clusters)
                    //        cluster.ToString();
                    //}
                    #endregion // DEBUG OUTPUT

                    lock (AutoColorCalibration._lockRangeClustDict)
                    {
                        // Dictionary doesn't have this colorRange yet, add it.
                        if (!this._rangeClusterDict.ContainsKey(colorRange) && clusters != null)
                            this._rangeClusterDict.Add(colorRange, new List<List<BlobCluster>>());

                        // Add clusters list to dictionary for this ColorRange on this frame.
                        if (clusters != null)
                            this._rangeClusterDict[colorRange].Add(clusters);
                    }
                }
                catch (Exception exc)
                {
                    Debug.Print("AutoColorCalibration ProcessFrame ERROR: {0}", exc.Message);
                    this.StopAnalyzing(null);
                    return;
                }
            }

            Debug.Print("Processed Frame: {0}", frameIndex + 1);

            // Using Interlocked to make Atomic for Threading. Basically, this._framesProcessed++
            Interlocked.Add(ref this._framesProcessed, 1);

            // Finished processing frames. Analyze results.
            if (this.FramesProcessed == this._frames)
                this.AnalyzeResults();
        }

        /// <summary>
        /// Analyzes the results of the processed frames. RUNS ON THE LAST THREAD TO PROCESS FRAMES.
        /// </summary>
        private void AnalyzeResults()
        {
            // Make sure the rangeClusterDict isn't empty or null.
            if (this._rangeClusterDict == null || this._rangeClusterDict.Count == 0)
            {
                Debug.Print("AutoColorCalibration AnalyzeResults : RangeCluster Dictionary is empty. Exiting");
                this.StopAnalyzing(null);
                return;
            }

            Debug.Print("ABOUT TO ANALYZE!!!");

            // Dictionary of AnalysisResults for each ColorRange.
            Dictionary<ColorRange, ResultAnalysis> rangeResults = new Dictionary<ColorRange, ResultAnalysis>();

            // Iterate through colorRanges in rangeClusterDict.
            foreach (ColorRange colorRange in this._rangeClusterDict.Keys)
            {
                // Add the colorRange to the rangeResults dictionary.
                rangeResults.Add(colorRange, new ResultAnalysis());

                // Iterate across frames through array of cluster lists
                foreach (List<BlobCluster> clusterArrayList in this._rangeClusterDict[colorRange])
                {
                    // Inspect each cluster list to identify properties of this definition.
                    rangeResults[colorRange].InspectClusterList(clusterArrayList);
                }
            }

            // Analaysis Debug Output
            foreach (var result in rangeResults)
            {
                try
                {
                    Debug.Print("\nRange: {0} \n{1}\n", result.Key.ToString(), result.Value.ToString());
                }
                catch { }
            }

            Debug.Print("ANALYSIS OVER!");

            // Choose best result, and finish analyzing.
            this.ChooseBestColorRange(rangeResults);
        }

        /// <summary>
        /// Choose the best ColorRange based on rangeResults. RUNS ON THE LAST THREAD TO PROCESS FRAMES.
        /// </summary>
        /// <param name="rangeResults">Results of the analysis</param>
        private void ChooseBestColorRange(Dictionary<ColorRange, ResultAnalysis> rangeResults)
        {
            // Chosen ColorRange must hit colors at least CAPTURE_THRESH % of the time. ( CAPTURE_THRESH % of the analyzed frames should have clusters )
            bool removing = true;

            while (removing)
            {
                bool foundOne = false;
                ColorRange toRemove = new ColorRange("N/A", 0, 0, 0, 0, 0, 0);

                foreach (var result in rangeResults)
                {
                    if (result.Value.AnalysisCount <= (this._framesProcessed * CAPTURE_THRESH))
                    {
                        toRemove = result.Key;
                        foundOne = true;
                        break;
                    }
                }
                if (foundOne)
                {
                    Debug.Print("Removed Range because below capture threshold of {0}%\n{1}", CAPTURE_THRESH, toRemove.ToString());
                    rangeResults.Remove(toRemove);
                }
                else
                    removing = false;
            }

            // Stop analyzing if not enough RangeResults left to analyze further.
            if (rangeResults.Count == 0)
            {
                this.StopAnalyzing(null);
                return;
            }
            else if (rangeResults.Count == 1)
            {
                this.StopAnalyzing(rangeResults.First().Key);
                return;
            }

            // Find most consistent.
            this.StopAnalyzing(ResultAnalysis.MostConsistent(rangeResults));

            // Should add some Strength logic in here comparing against each other.
        }

        #endregion // Processing Private Methods

        #region Maintenance Private Methods

        /// <summary>
        /// Performs the logic required when starting analyzing.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Can only analyze once at a time. Not concurrently.</exception>
        private void StartAnalyzing()
        {
            if (this.Analyzing)
                throw new InvalidOperationException("AutoColorCalibration StartAnalyzing : Cannot concurrently analyze.");

            if (this.AnalysisStarted != null)
                this.AnalysisStarted(this, null);
            this.Analyzing = true;

            // Set the framesCollected and framesProcessed to 0. ( Counts frames collected and processed )
            this.FramesCollected = 0;
            this.FramesProcessed = 0;

            // Setup the collection to store frames.
            this._imageDatas = new ImageData[this._frames];

            // Subscribe to color frames.
            KinectManager.ColorDataReady += KinectManager_ColorDataReady;
        }

        /// <summary>
        /// Performs the logic required when stopping analyzing.
        /// </summary>
        private void StopAnalyzing(ColorRange foundColorRange)
        {
            if (!this.Analyzing)
                return;

            if (foundColorRange != null)
                Debug.Print("Winner Found: \n{0}", foundColorRange.ToString());

            // Unsubscribe from color frames.
            KinectManager.ColorDataReady -= KinectManager_ColorDataReady;

            // Dump frame and color data.
            this._imageDatas = null;
            this._colorRanges = null;
            this._rangeClusterDict = new Dictionary<ColorRange, List<List<BlobCluster>>>();

            this.Analyzing = false;

            // Fire analysis finished event. Run on Application.Current.Dispatcher in-case StopAnalyzing called from a Thread.
            if (this.AnalysisCompleted != null)
                Application.Current.Dispatcher.Invoke(new Action(() => this.AnalysisCompleted(this, foundColorRange)));
        }

        /// <summary>
        /// Throws an exception if AutoColorCalibration is currently analyzing.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">Thrown when a property is modified while analyzing.</exception>
        private void CheckIfAnalyzing()
        {
            if (this.Analyzing)
                throw new InvalidOperationException("AutoColorCalibration Properties may not be modified while Analyzing.");
        }

        /// <summary>
        /// Fires the PropertyChagned event.
        /// </summary>
        private void FirePropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, propertyName);
        }

        #endregion // Maintenance Private Methods

        #endregion // Private Methods

        #region Event Handlers

        // Get the notification that colorData is ready and fill internal collection of colorData frames.
        private void KinectManager_ColorDataReady()
        {
            try
            {
                // Don't process more frames than necessary.
                if (this.FramesCollected >= this._frames)
                    return;

                {
                    {
                        // FrameNumber is not guaranteed, use timestamp
                        TimeSpan span5 = TimeSpan.FromMilliseconds(-500);

                        if (KinectManager.ColorFrameTime.CompareTo(span5) == 1)
                        {
                            // Initialize the place to put the frame data, add the frame data, increment the counter.
                            this._imageDatas[this.FramesCollected] = new ImageData(KinectManager.ColorData, KinectManager.ColorWidth,
                                KinectManager.ColorHeight, this._scanArea);

                            this.FramesCollected++;

                            Debug.Print("Frames Collected : {0}", this.FramesCollected);

                            // Process the frame just given by the Kinect.
                            new Thread(this.ProcessFrame) 
                                { Name = String.Format("Thread on Frame:{0}", KinectManager.ColorFrameTime) }
                                .Start(this.FramesCollected - 1);
                        }
                    }
                }
            }
            catch (Exception exc) 
            { 
                Debug.Print("AutoColorCalibration KinectManager_ColorDataReady ERROR: {0}", exc.Message);
                this.StopAnalyzing(null);
            }
        }

        #endregion // Event Handlers

        #region Internal Objects ( AnalysisResult )

        /// <summary>
        /// Class inspects BlobCluster lists and exposes properties discovered from analyzing all of the inspected lists.
        /// </summary>
        private class ResultAnalysis
        {
            #region Properties 

            // *** Other Properties ***
            public int AnalysisCount { get; private set; }

            // *** Num clusters properties ***
            public double AvgNumClusters { get; private set; }
            public int ModeCountNumClusters { get; private set; }
            public int ModeNumClusters { get; private set; }
            public int MaxNumClusters { get; private set; }
            public int MinNumClusters { get; private set; }
            
            // *** ClusterSize properties ***
            public double AvgClusterSize { get; private set; }
            public double ModeClusterSize { get; private set; }
            public double ModeCountClusterSize { get; private set; }
            public double MaxClusterSize { get; private set; }
            public double MinClusterSize { get; private set; }

            #endregion // Properties

            #region Fields

            // Store counts of how many clusters per analyzed frame.
            private List<int> _clusterCounts;
            // Store sizes of each cluster across all frames.
            private List<double> _clusterSizes;

            #endregion // Fields

            #region Constructor

            /// <summary>
            /// Analyzes collections of clusters and exposes discovered properties.
            /// </summary>
            public ResultAnalysis()
            {
                this.DefaultParameters();
            }

            #endregion // Constructor

            #region Public Class-Wide Methods

            /// <summary>
            /// Order the given dictionary of ColorRange, ResultAnalysis by consistency.
            /// </summary>
            /// <param name="rangeResults">Dictionary to order.</param>
            public static ColorRange MostConsistent(Dictionary<ColorRange, ResultAnalysis> rangeResults)
            {
                // Consistency is having a modecount close to analysis count and a mode close to average.
                // Find the one with the highest modecount, with a mode close to average.

                try
                {
                    // Look for most consistent number of clusters across definitions.
                    double maxModeCount = int.MinValue;
                    Dictionary<ColorRange, ResultAnalysis> mostConsistentNumClusters = new Dictionary<ColorRange, ResultAnalysis>();
                   // List<ColorRange> mostConsistentNumClusters = new List<ColorRange>();

                    foreach (var result in rangeResults)
                    {
                        if (result.Value.ModeCountNumClusters > maxModeCount)
                        {
                            // Stored most consistent aren't most consistent.
                            mostConsistentNumClusters.Clear();
                            // Update maxModeCount
                            maxModeCount = result.Value.ModeCountNumClusters;
                            mostConsistentNumClusters.Add(result.Key, result.Value);
                        }
                        else if (result.Value.ModeCountNumClusters == maxModeCount)
                        {
                            mostConsistentNumClusters.Add(result.Key, result.Value);
                        }
                    }

                    // If multiple definitions share best consistency for number of clusters, 
                    // check against consistency of cluster size. If there's a tie, unlikely, arbitrarily 
                    // return one of them.
                    double maxModeSize = int.MinValue;
                    ColorRange winner = mostConsistentNumClusters.First().Key;

                    foreach (var result in mostConsistentNumClusters)
                    {
                        if (( result.Value.ModeCountClusterSize - result.Value.AvgNumClusters ) > maxModeSize)
                        {
                            maxModeSize = (result.Value.ModeCountClusterSize - result.Value.AvgNumClusters);
                            winner = result.Key;
                        }
                    }

                    return winner;
                }
                catch (Exception exc)
                {
                    System.Diagnostics.Debug.Print("ResultAnalysis MostConsistent ERROR: {0}", exc.Message);
                    return null;
                }
            }

            #endregion // Public Class-Wide Methods

            #region Public Methods

            /// <summary>
            /// Reset all of the parameters on this AnalysisResult.
            /// </summary>
            public void Clean()
            {
                this.DefaultParameters();
            }

            /// <summary>
            /// Inspects the given list and updates parameters accordingly. Parameters altered across all lists inspected.
            /// </summary>
            /// <param name="blobClusters">List of BlobClusters to inspect.</param>
            public void InspectClusterList(List<BlobCluster> blobClusters)
            {
                if (blobClusters == null)
                    return;

                try
                {
                    this.AnalysisCount++;

                    foreach (BlobCluster blobCluster in blobClusters)
                        blobCluster.ToString();

                    // *** Cluster Count Analysis *** //

                    this._clusterCounts.Add(blobClusters.Count);

                    this._clusterCounts.Sort();

                    this.AvgNumClusters = this._clusterCounts.Average();
                    this.MaxNumClusters = this._clusterCounts.Last();
                    this.MinNumClusters = this._clusterCounts.First();

                    // Finding Mode only works because sorted.
                    int cntModeCheck = 0;
                    this.ModeCountNumClusters = 0;
                    this.ModeNumClusters = 0;
                    foreach (int clusterCnt in this._clusterCounts)
                    {
                        if (this.ModeNumClusters == clusterCnt)
                        {
                            this.ModeCountNumClusters++;
                            cntModeCheck = 0;
                        }
                        else
                        {
                            cntModeCheck++;
                            if (cntModeCheck >= this.ModeCountNumClusters)
                            {
                                this.ModeNumClusters = clusterCnt;
                                this.ModeCountNumClusters = cntModeCheck;
                                cntModeCheck = 0;
                            }
                        }
                    }

                    // *** Cluster Size Analysis *** //

                    // Collect sizes.
                    foreach (BlobCluster blobCluster in blobClusters)
                        this._clusterSizes.Add(blobCluster.Size);

                    this._clusterSizes.Sort();

                    this.MaxClusterSize = this._clusterSizes.Last();
                    this.MinClusterSize = this._clusterSizes.First();
                    this.AvgClusterSize = this._clusterSizes.Average();

                    // Discover mode and mode count.
                    int sizeModeCheck = 0;
                    this.ModeCountClusterSize = 0;
                    this.ModeClusterSize = 0;
                    foreach (int clusterSize in this._clusterSizes)
                    {
                        if (this.ModeClusterSize == clusterSize)
                        {
                            this.ModeCountClusterSize++;
                            sizeModeCheck = 0;
                        }
                        else
                        {
                            sizeModeCheck++;
                            if (sizeModeCheck >= this.ModeCountClusterSize)
                            {
                                this.ModeClusterSize = clusterSize;
                                this.ModeCountClusterSize = sizeModeCheck;
                                sizeModeCheck = 0;
                            }
                        }
                    }
                }
                catch (Exception exc)
                {
                    System.Diagnostics.Debug.Print("AutoColorCalibration InspectClusterList() ERROR: {0}", exc.Message);
                }
            }

            #endregion // Public Methods

            #region Private Methods

            private void DefaultParameters()
            {
                this._clusterCounts = new List<int>();
                this._clusterSizes = new List<double>();

                this.AnalysisCount = 0;

                this.MinNumClusters = int.MaxValue;
                this.MaxNumClusters = int.MinValue;
                this.ModeNumClusters = 0;
                this.ModeCountNumClusters = 0;
                this.AvgNumClusters = 0;

                this.MinClusterSize = int.MaxValue;
                this.MaxClusterSize = int.MinValue;
                this.AvgClusterSize = 0;
                this.ModeClusterSize = 0;
                this.ModeCountClusterSize = 0;
            }

            #endregion // Private Methods

            #region Overridden Methods

            public override string ToString()
            {
                string output = string.Format("AnalysisCount: {0}\n COUNT STATS: AvgNumClusters: {1}  ModeCountNumClusters: {2}  ModeNumClusters: {3}  " +
                    "MaxNumClusters: {4}  MinNumClusters: {5}\n" +
                    "SIZE STATS: AvgClusterSize: {6}  ModeCountClusterSize: {7}  ModeClusterSize: {8}  MaxClusterSize: {9}  MinClusterSize: {10}",
                    this.AnalysisCount, this.AvgNumClusters, this.ModeCountNumClusters, this.ModeNumClusters, this.MaxNumClusters, this.MinNumClusters,
                    this.AvgClusterSize, this.ModeCountClusterSize, this.ModeClusterSize, this.MaxClusterSize, this.MinClusterSize);

                return output;
            }

            #endregion // Overridden Methods
        }

        #endregion // Internal Objects ( AnalysisResult )
    }
}
