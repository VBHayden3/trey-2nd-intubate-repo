﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace AIMS.Assets.Lessons2
{
    /// <summary>
    /// BlobClusters cluster blobs. Essentially a larger blob around smaller blobs to improve
    /// Color Calibration analysis and to better identify an area of a colored object.
    /// </summary>
    public class BlobCluster
    {
        #region Properties

        /// <summary>
        /// Amount of blobs used to create this BlobCluster.
        /// </summary>
        public int BlobCount { get { return this._blobs.Count; } }
        /// <summary>
        /// Location of this BlobCluster given as a rectangle. The rectangle
        /// is essentially a bounding box around all of the blobs which this BlobCluster consists of.
        /// </summary>
        public Int32Rect Location { get; private set; }
        /// <summary>
        /// Center X,Y coordinates of the Location rectangle.
        /// </summary>
        public Point CenterLocation 
        {
            get
            {
                return this.Location.IsEmpty
                ? new Point()
                : new Point(this.Location.X + (this.Location.Width / 2), this.Location.Y + (this.Location.Height / 2));
            }
        }
        /// <summary>
        /// Top left corner of the BlobCluster. Equivalent to (Location.X, Location.Y).
        /// </summary>
        public Point TopLeftCorner 
        { 
            get { return this.Location.IsEmpty ? new Point() : new Point(this.Location.X, this.Location.Y); } 
        }
        /// <summary>
        /// Bottom right corner of the BlobCluster. Equivalent to (Location.X + Location.Width, Location.Y + Location.Height).
        /// </summary>
        public Point BottomRightCorner
        {
            get { return this.Location.IsEmpty ? new Point() : new Point(this.Location.X + this.Location.Width, this.Location.Y + this.Location.Height); }
        }
        /// <summary>
        /// Size of the BlobCluster; area in pixels. Equivalent to (Location.Width * Location.Height).
        /// </summary>
        public double Size 
        { 
            get { return this.Location.IsEmpty ? 0 : (Location.Width * Location.Height); }
        }
        /// <summary>
        /// Width of BlobCluster.
        /// </summary>
        public int Width { get { return this.Location.Width; } }
        /// <summary>
        /// Height of BlobCluster.
        /// </summary>
        public int Height { get { return this.Location.Height; } }
        /// <summary>
        /// Radius of the cluster to consume neighbors.
        /// </summary>
        public int NeighborRadius { get { return this._neighborRadius; } }

        #endregion // Properties

        #region Fields

        // Collection of blobs native to a cluster.
        private List<Int32Rect> _blobs;
        // Range to identify neighboring blobs as close enough to be consumed by the cluster or not.
        private int _neighborRadius;

        #endregion // Fields

        #region Constructor

        /// <summary>
        /// Identifies properties of blob collections, grouping them into a cluster.
        /// </summary>
        public BlobCluster(int neighborRadius)
        {
            this._blobs = new List<Int32Rect>();
            this.Location = new Int32Rect();
            this._neighborRadius = neighborRadius;
        }

        /// <summary>
        /// Identifies properties of blob collections, grouping them into a cluster.
        /// </summary>
        /// <param name="blobs"></param>
        /// <exception cref="System.ArgumentNullException">Thrown when the blobs parameter is null.</exception>
        public BlobCluster(int neighborRadius, Int32Rect[] blobs) 
            :this(neighborRadius)
        {
            if (blobs == null)
                throw new ArgumentNullException("blobs", "blobs may not be null.");

            // Add the blobs parameter to the internal list of blobs.
            this._blobs.AddRange(blobs);

            // Analyze the blobs and find the Location.
            if (this.BlobCount > 0)
                this.AnalyzeBlobs();
        }

        #endregion // Constructor

        #region Public Class-Wide Methods

        /// <summary>
        /// Iterate through array of blobs and return a list of BlobClusters which encompass the blobs.
        /// Returns null if there are no blobs.
        /// </summary>
        /// <param name="blobs">Array of blobs to cluster.</param>
        /// <returns>List of BlobClusters which encompass blobs. Null if no blobs are in the array.</returns>
        public static List<BlobCluster> GenerateBlobClusters(Int32Rect[] blobs, int neighborRadius = 10)
        {
            if (blobs.Length == 0)
                return null;

            // Create list of blobs from passed in array of blobs. Ensures doesn't ruin array of blobs.
            List<Int32Rect> blobsList = new List<Int32Rect>(blobs);

            // Collection of blobClusters to return.
            List<BlobCluster> blobClusters = new List<BlobCluster>();

            // Add first BlobCluster to work with.
            blobClusters.Add(new BlobCluster(neighborRadius));

            // Index to keep track of where we are in the blobsList collection.
            int blobClustersIndex = 0;

            // Keep working until there aren't any blobs left.
            while (blobsList.Count > 0)
            {
                bool blobConsumed = false;

                for (int i = 0; i < blobsList.Count; i++)
                {
                    // Only set consumed to true if consumed, keeps track if was ever consumed.
                    // Also, remove blob if consumed.
                    if (blobClusters[blobClustersIndex].TryConsumeBlob(blobsList[i]))
                    {
                        blobsList.Remove(blobsList[i]);
                        blobConsumed = true;
                    }
                    // Remove from list if blob is an empty blob.
                    else if (blobsList[i].IsEmpty)
                        blobsList.Remove(blobsList[i]);
                }
                // If no blobs were consumed on the last iteration through the blobsList, create a new cluster.
                if (!blobConsumed)
                {
                    blobClusters.Add(new BlobCluster(neighborRadius));
                    blobClustersIndex++;
                }
            }

            return blobClusters;
        }

        #endregion // Public Class-Wide Methods

        #region Public Methods

        /// <summary>
        /// Attempt to consume a blob. If the blob is consumed by the BlobCluster, returns true. Otherwise, returns false.
        /// </summary>
        /// <param name="blob">Blob which may be consumed if close enough to the cluster, or if the cluster is empty.</param>
        /// <returns>True if the blob is consumed, False otherwise.</returns>
        public bool TryConsumeBlob(Int32Rect blob)
        {
            // Empty blob given, so not consumed.
            if (blob.IsEmpty)
            {
                return false;
            }
            // Empty collection of blobs, so consume non-empty blob.
            else if (this._blobs.Count == 0)
            {
                this._blobs.Add(blob);
                this.AnalyzeBlobs();
                return true;
            }
            // Analyze blob and add if within a neighborly radius.
            else if (this._blobs.Count > 0)
            {
                if (this.InAlignedRange(blob))
                {
                    this._blobs.Add(blob);
                    this.AnalyzeBlobs();
                    return true;
                }
            }

            return false;
        }

        #endregion // Public Methods

        #region Overridden Methods

        /// <summary>
        /// Gets a string representation of a BlobCluster.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string output = string.Format("***BlobCluster***\nLocation: ({0},{1})  Center: ({2},{3})  Size: {4}  BlobCount: {5}\n\t***BLOBS***",
                this.Location.X, this.Location.Y, this.CenterLocation.X, this.CenterLocation.Y, this.Size, this.BlobCount);

            foreach (Int32Rect blob in this._blobs)
            {
                output += string.Format("\n\t Location: ({0}, {1})  Width: {2}  Height: {3}", blob.X, blob.Y, blob.Width, blob.Height);
            }
            return output;
        }

        #endregion // Overridden Methods

        #region Private Methods

        /// <summary>
        /// Identify whether or not a blob is within neighborly range of this BlobCluster.
        /// Works for aligned blobs; aligned with the horizontal and vertical axis.
        /// </summary>
        /// <param name="blob">Blob to compare against.</param>
        /// <returns>True if in range. False otherwise.</returns>
        private bool InAlignedRange(Int32Rect blob)
        {
            // NOTE: 0,0 origin of frame is in TOP LEFT corner
            // http://math.stackexchange.com/questions/261336/intersection-between-a-rectangle-and-a-circle

            // Four corners of the blob.
            Point tl = new Point(blob.X, blob.Y);                               // Top Left corner.
            Point tr = new Point(blob.X + blob.Width, blob.Y);                  // Top Right corner.
            Point bl = new Point(blob.X, blob.Y + blob.Height);                 // Bottom Left corner.
            Point br = new Point(blob.X + blob.Width, blob.Y + blob.Height);    // Bottom Right corner.

            // Top horizontal edge intersection.
            if (this.CenterLocation.X >= tl.X && this.CenterLocation.X <= tr.X)
            {
                if (System.Math.Abs(tl.Y - this.CenterLocation.Y) <= this._neighborRadius)
                    return true;
            }
            // Bottom horizontal edge intersection.
            if (this.CenterLocation.X >= bl.X && this.CenterLocation.X <= br.X)
            {
                if (Math.Abs(bl.Y - this.CenterLocation.Y) <= this._neighborRadius)
                    return true;
            }
            // Left vertical edge intersection.
            if (this.CenterLocation.Y <= tl.Y && this.CenterLocation.Y >= bl.Y)
            {
                if (Math.Abs(tl.X - this.CenterLocation.X) <= this._neighborRadius)
                    return true;
            }
            // Right vertical edge intersection.
            if (this.CenterLocation.Y <= tr.Y && this.CenterLocation.Y >= br.Y)
            {
                if (Math.Abs(tr.X - this.CenterLocation.X) <= this._neighborRadius)
                    return true;
            }

            // Check if points are within circle.
            double neighborRadiousSquared = Math.Pow(this._neighborRadius, 2);

            if (Math.Pow(tl.X - this.CenterLocation.X, 2) + Math.Pow(tl.Y - this.CenterLocation.Y, 2) <= neighborRadiousSquared ||
                Math.Pow(tr.X - this.CenterLocation.X, 2) + Math.Pow(tr.Y - this.CenterLocation.Y, 2) <= neighborRadiousSquared ||
                Math.Pow(bl.X - this.CenterLocation.X, 2) + Math.Pow(bl.Y - this.CenterLocation.Y, 2) <= neighborRadiousSquared ||
                Math.Pow(br.X - this.CenterLocation.X, 2) + Math.Pow(br.Y - this.CenterLocation.Y, 2) <= neighborRadiousSquared)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Discover properties of the cluster based on the blobs collection. Sets Location property.
        /// Call this whenever the blobs collection is changed.
        /// </summary>
        /// <param name="blobs">Collection of blobs to analyze.</param>
        private void AnalyzeBlobs()
        {
            // Get out if no work to do.
            if (this._blobs.Count == 0)
                return;

            // Keep track if all of the blobs are empty or if at least one is filled.
            bool filledBlobExists = false;

            // Variables for the top left corner of the cluster.
            int minX = int.MaxValue;
            int minY = int.MaxValue;
            // Variables for the bottom right corner of the cluster.
            int maxX = int.MinValue;
            int maxY = int.MinValue;

            // Iterate over the blobs and discover the bounding box.
            foreach (Int32Rect blob in this._blobs)
            {
                // Make sure attributes are there.
                if (!blob.IsEmpty)
                {
                    filledBlobExists = true;

                    if (blob.X < minX)
                        minX = blob.X;
                    if (blob.Y < minY)
                        minY = blob.Y;
                    if (blob.X + blob.Width > maxX)
                        maxX = blob.X + blob.Width;
                    if (blob.Y + blob.Height > maxY)
                        maxY = blob.Y + blob.Height;
                }
            }

            // Set Location from discovered attributes of the blobs which aren't empty.
            if (filledBlobExists)
                this.Location = new Int32Rect(minX, minY, maxX - minX, maxY - minY);
        }
        // fun ascii art just because... <(. .^) (^. .^) (^. .)>  cheer up developer! Life's not doom and gloom but code and ... well there's code.
        #endregion // Private Methods
    }
}
