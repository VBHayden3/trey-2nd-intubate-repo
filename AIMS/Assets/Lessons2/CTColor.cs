﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIMS.Assets.Lessons2
{
    /// <summary>
    /// ColorTracker Color class wich stores a ColorTracker.ColorRange and a Lighting.
    /// </summary>
    public class CTColor
    {
        #region Properties

        /// <summary>
        /// Returns ColorTracker.Color.colorRange.
        /// </summary>
        public ColorRange ColorRange { get { return this.colorRange; } }
        /// <summary>
        /// Returns ColorTracker.Color.lighting.
        /// </summary>
        public Lighting Lighting { get { return this.lighting; } }

        #endregion Properties

        #region Fields

        private ColorRange colorRange;
        private Lighting lighting;

        #endregion Fields

        #region Constructor

        /// <summary> 
        /// CTColor containing a ColorRange and Lighting. Defaults Lighting to none (None supplied) and has immutable properties.
        /// </summary>
        /// <param name="ColorRange">ColorTracker.ColorRange to be stored in ColorTracker.Color.</param>
        /// <param name="Lighting">Lighting for ColorTracker.ColorRange supplied to ColorTracker.Color. ( Defaults to None )</param>
        public CTColor(ColorRange ColorRange, Lighting Lighting = Lighting.none)
        {
            this.lighting = Lighting;
            this.colorRange = ColorRange;
        }

        #endregion Constructor

        #region Overridden Methods

        /// <summary>
        /// Outputs the ColorTracker.Color in a human readable format.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("lighting: {0}  ColorRange:\n{1}", this.lighting.ToString(), this.colorRange.ToString());
        }

        #endregion Overridden Methods
    }
}
