﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIMS.Assets.Lessons2
{
    public class ColorRange
    {
        #region Properties

        /// <summary>
        /// Description of the color for this ColorRange.
        /// </summary>
        public string Color
        {
            get { return this.color; }
            set
            {
                if (this.color == value)
                    return;
                this.color = value;
            }
        }
        /// <summary>
        /// Hue base value.
        /// </summary>
        public double Hue 
        { 
            get { return this.hue; }
            set 
            {
                if (this.hue == value)
                    return; 
                this.hue = value;
            }
        }
        /// <summary>
        /// Hue threshold allowing deviation from the Hue base value.
        /// </summary>
        public double HueThreshold 
        { 
            get { return this.hueThreshold; }
            set 
            { 
                if (this.hueThreshold == value)
                    return;
                this.hueThreshold = value;
            }
        }
        /// <summary>
        /// Saturation base value.
        /// </summary>
        public double Saturation 
        { 
            get { return this.saturation; }
            set 
            {
                if (this.saturation == value)
                    return;
                this.saturation = value;
            } 
        }
        /// <summary>
        /// Saturation threshold allowing deviation from the Saturation base value.
        /// </summary>
        public double SaturationThreshold 
        { 
            get { return this.saturationThreshold; } 
            set 
            {
                if (this.saturationThreshold == value)
                    return;
                this.saturationThreshold = value;
            }
        }
        /// <summary>
        /// Value base value.
        /// </summary>
        public double Value 
        { 
            get { return this.value; } 
            set 
            {
                if (this.value == value)
                    return;
                this.value = value;
            }
        }
        /// <summary>
        /// Value threshold allowing deviation from the Value base value.
        /// </summary>
        public double ValueThreshold
        {
            get { return this.valueThreshold; }
            set
            {
                if (ValueThreshold == value)
                    return;
                this.valueThreshold = value;
            }
        }

        #endregion // Properties

        #region Fields

        private string color;
        private double hue;
        private double hueThreshold;
        private double saturation;
        private double saturationThreshold;
        private double value;
        private double valueThreshold;

        #endregion

        #region Constructor

        /// <summary>
        /// Create a color range in HSV color space with base values and threshold from those base values.
        /// </summary>
        /// <param name="hue">Hue base value.</param>
        /// <param name="hueThreshold">Hue threshold allowing deviation from the Hue base value.</param>
        /// <param name="saturation">Saturation base value.</param>
        /// <param name="saturationThreshold">Saturation threshold allowing deviation from the Saturation base value.</param>
        /// <param name="value">Value base value.</param>
        /// <param name="valueThreshold">Value threshold allowing deviation from the Value base value.</param>
        public ColorRange(string color, double hue, double hueThreshold, double saturation, double saturationThreshold, double value,
            double valueThreshold)
        {
            this.color = color;
            this.hue = hue;
            this.hueThreshold = hueThreshold;
            this.saturation = saturation;
            this.saturationThreshold = saturationThreshold;
            this.value = value;
            this.valueThreshold = valueThreshold;
        }

        #endregion // Constructor

        #region Overridden Methods

        /// <summary>
        /// Exposes color range values in string format.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("hue: {0}  hueThreshold: {1}  saturation: {2}  saturationThreshold: {3} \n" +
                "value: {4}  valueThreshold: {5}", this.hue, this.hueThreshold, this.saturation, this.saturationThreshold, this.value, this.valueThreshold);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            ColorRange cr = obj as ColorRange;
            if ((object)cr == null)
                return false;

            return (this.color == cr.color && this.hue == cr.hue && this.hueThreshold == cr.hueThreshold &&
                this.saturation == cr.saturation && this.saturationThreshold == cr.saturationThreshold &&
                this.value == cr.value && this.valueThreshold == cr.valueThreshold);
        }

        public override int GetHashCode()
        {
            return this.color.GetHashCode() ^ this.hue.GetHashCode() ^ this.hueThreshold.GetHashCode() ^ this.saturation.GetHashCode() ^ 
                this.saturationThreshold.GetHashCode() ^ this.value.GetHashCode() ^ this.valueThreshold.GetHashCode();
        }

        public static bool operator == (ColorRange a, ColorRange b)
        {
            if (System.Object.ReferenceEquals(a, b))
                return true;

            if ((object)a == null || (object)b == null)
                return false;

            return a.Equals(b);
        }

        public static bool operator != (ColorRange a, ColorRange b)
        {
            return !(a == b);
        }

        #endregion // Overridden Methods
    }
}
