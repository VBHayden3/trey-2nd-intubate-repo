﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Interop;
using System.Diagnostics;
using System.Xml;
using System.IO;
using System.Reflection;

using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

namespace AIMS.Assets.Lessons2
{
    public class ColorTracker
    {
        #region Events

        /// <summary>
        /// Fires whenever the ColorRanges collection is altered.
        /// </summary>
        public static event EventHandler ColorRangesUpdated;
        public static event EventHandler ObjectColorsUpdated;

        #endregion // Events

        #region Properties

        /// <summary>
        /// Dictionary of ColorRanges. The key is the Color.
        /// </summary>
        public static Dictionary<string, List<CTColor>> ColorRanges { get { return new Dictionary<string, List<CTColor>>(ColorTracker.colorRanges); } }
        public static Dictionary<string, CTColor> ObjectColors { get { return new Dictionary<string, CTColor>(ColorTracker.objectColors); } }
        /// <summary>
        /// Available Colors within ColorRanges.
        /// </summary>
        public static string[] ColorNames { get { return ColorRanges.Keys.ToArray(); } }
        public static string[] ObjectColorNames { get { return ObjectColors.Keys.ToArray(); } }
        public static List<ImageData> Images { get { return imageDatas; } }

        public static System.Windows.Point FoundPoint { get { return foundPoint; } }

        // Public Easily Accessable Color Properties ( Reverse-Compatability, may remove later? Need team feedback ).
        [Obsolete("Use ColorTracker.GetColorRange(\"yellow\", Lighting.none) Instead", false)]
        public static ColorRange YellowTape { get { return ColorTracker.GetColorRange("yellow", Lighting.none); } }
        [Obsolete("Use ColorTracker.GetColorRange(\"red\", Lighting.none) Instead", false)]
        public static ColorRange RedTape { get { return ColorTracker.GetColorRange("red", Lighting.none); } }
        [Obsolete("Use ColorTracker.GetColorRange(\"pink\", Lighting.none) Instead", false)]
        public static ColorRange PinkTape { get { return ColorTracker.GetColorRange("pink", Lighting.none); } }
        [Obsolete("Use ColorTracker.GetColorRange(\"green\", Lighting.none) Instead", false)]
        public static ColorRange GreenTape { get { return ColorTracker.GetColorRange("green", Lighting.none); } }
        [Obsolete("Use ColorTracker.GetColorRange(\"red\", Lighting.low) Instead", false)]
        public static ColorRange RedTape_LowLight { get { return ColorTracker.GetColorRange("red", Lighting.low); } }
        [Obsolete("Use ColorTracker.GetColorRange(\"yellow\", Lighting.low) Instead", false)]
        public static ColorRange YellowTape_LowLight { get { return ColorTracker.GetColorRange("yellow", Lighting.low); } }
        [Obsolete("Use ColorTracker.GetColorRange(\"pink\", Lighting.low) Instead", false)]
        public static ColorRange PinkTape_LowLight { get { return ColorTracker.GetColorRange("pink", Lighting.low); } }
        [Obsolete("Use ColorTracker.GetColorRange(\"yellow\", Lighting.high) Instead", false)]
        public static ColorRange YellowTape_HiLight { get { return ColorTracker.GetColorRange("yellow", Lighting.high); } }

        #endregion // Properties

        #region Fields

        // Public Fields.
        public static readonly int MAX_IMAGES_COUNT = 5;

        // Private Fields.
        private static DateTime colorRangesXmlFileModifiedDate = DateTime.MinValue;
        private static DateTime objectColorsXmlFileModifiedDate = DateTime.MinValue;
        private static Dictionary<string, List<CTColor>> colorRanges = new Dictionary<string, List<CTColor>>();
        private static Dictionary<string, CTColor> objectColors = new Dictionary<string, CTColor>();
        private static List<ImageData> imageDatas = new List<ImageData>();
        private static System.Windows.Point foundPoint;
        // The color tracker xml file path.
        private static string colorTrackerXmlFile = Directory.GetCurrentDirectory() + "\\colorRanges.xml";
        private static string objectColorsXmlFile = Directory.GetCurrentDirectory() + "\\objectColors.xml";

        // Thread Locking Mechanism
        private static object _lock = new object();

        #endregion // Fields

        #region Constructor

        /// <summary>
        /// Static ColorTracker Constructor. Performs necessary actions to initialize ColorTracker.
        /// </summary>
        static ColorTracker()
        {
            // First run of parsing the default XML file for Colors.
            ColorTracker.ParseColorRangeXMLFile();
            ColorTracker.ParseObjectColorsXMLFile();
        }

        #endregion // Constructor

        #region Public Methods

        public static System.Windows.Point find(ColorRange range, Int32Rect scanArea, byte[] colorData, int numNeigbors = 5, int radius = 3)
        {
            try
            {
                return find(range.Hue, range.HueThreshold, range.Saturation, range.SaturationThreshold, range.Value, range.ValueThreshold,
                    scanArea, colorData, numNeigbors, radius);
            }
            catch (Exception exc)
            {
                if (range == null)
                    Debug.Print("ColorTracker.find was passed a null range! Returning an empty Point: {0}", exc.Message);

                return new System.Windows.Point();
            }
        }

        /// <summary>
        /// Returns a point at the center of the color found. Assumes 0,0 is the top left corner of the image.
        /// </summary>
        /// <param name="hue"></param>
        /// <param name="hueThreshold"></param>
        /// <param name="saturation"></param>
        /// <param name="saturationThreshold"></param>
        /// <param name="value"></param>
        /// <param name="valueThreshold"></param>
        /// <param name="scanArea">The area to be scanned in the image. Will attempt to keep the scan area the same size before clipping</param>
        /// <param name="colorData">A 1920x1080 bgr32 image</param>
        /// <returns></returns>
        public static System.Windows.Point find(double hue, double hueThreshold, double saturation, double saturationThreshold, double value, double valueThreshold,
            Int32Rect scanArea, byte[] colorData, int numNeighbors = 5, int radius = 3)
        {
            ColorTracker.ParseColorRangeXMLFile();
            ColorTracker.ParseObjectColorsXMLFile();

            System.Windows.Point point = new System.Windows.Point(0, 0);


            int frameWidth = 1920;
            int frameHeight = 1080;

            // Make sure the scan area is the same size or smaller than the image
            if (scanArea.Width > frameWidth)
            {
                scanArea.Width = frameWidth;
                scanArea.X = 0;
            }

            if (scanArea.Height > frameHeight)
            {
                scanArea.Height = frameHeight;
                scanArea.Y = 0;
            }

            // Make sure the scan area is within the image bounds
            if (scanArea.X < 0)
            {
                scanArea.X = 0;
            }

            if (scanArea.Y < 0)
            {
                scanArea.Y = 0;
            }

            if (scanArea.X + scanArea.Width > frameWidth)
            {
                scanArea.X = frameWidth - scanArea.Width;
            }

            if (scanArea.Y + scanArea.Height > frameHeight)
            {
                scanArea.Y = frameHeight - scanArea.Height;
            }

            // Copy the scan area into ImageData to prevent side-effects
            ImageData imageData = new ImageData(colorData, frameWidth, frameHeight, scanArea);

            imageData.Scan(hue, hueThreshold, saturation, saturationThreshold, value, valueThreshold);

            imageData.Filter(numNeighbors, radius);

            List<Int32Rect> blobs = PerformCannyBlobDetectionRect(imageData.getBytes(),
                    imageData.width, imageData.height,
                    false, 140, 181, 0, 255);

            foreach (Int32Rect blob in blobs)
            {
                // Draw horizontal lines around the blob
                for (int i = 0; i < blob.Width; i++)
                {
                    try
                    {
                        imageData.data[blob.Y * scanArea.Width + blob.X + i].B = 255;
                        imageData.data[blob.Y * scanArea.Width + blob.X + i].G = 255;
                        imageData.data[blob.Y * scanArea.Width + blob.X + i].R = 0;
                        imageData.data[blob.Y * scanArea.Width + blob.X + i].A = 255;

                        imageData.data[(blob.Y + blob.Height) * scanArea.Width + blob.X + i].B = 255;
                        imageData.data[(blob.Y + blob.Height) * scanArea.Width + blob.X + i].G = 255;
                        imageData.data[(blob.Y + blob.Height) * scanArea.Width + blob.X + i].R = 0;
                        imageData.data[(blob.Y + blob.Height) * scanArea.Width + blob.X + i].A = 255;
                    }
                    catch
                    {
                    }
                }

                // Draw vertical lines
                for (int i = 0; i < blob.Height; i++)
                {
                    try
                    {
                        imageData.data[(blob.Y + i) * scanArea.Width + blob.X].B = 255;
                        imageData.data[(blob.Y + i) * scanArea.Width + blob.X].G = 255;
                        imageData.data[(blob.Y + i) * scanArea.Width + blob.X].R = 0;
                        imageData.data[(blob.Y + i) * scanArea.Width + blob.X].A = 255;

                        imageData.data[(blob.Y + i) * scanArea.Width + blob.X + blob.Width].B = 255;
                        imageData.data[(blob.Y + i) * scanArea.Width + blob.X + blob.Width].G = 255;
                        imageData.data[(blob.Y + i) * scanArea.Width + blob.X + blob.Width].R = 0;
                        imageData.data[(blob.Y + i) * scanArea.Width + blob.X + blob.Width].A = 255;
                    }
                    catch
                    {
                    }
                }
            }


            // Cluster blobs and color them.
            try
            {
                List<BlobCluster> blobClusters = BlobCluster.GenerateBlobClusters(blobs.ToArray());

                if (blobClusters != null)
                {
                    foreach (BlobCluster cluster in blobClusters)
                    {
                        Debug.Print(cluster.ToString());
                    }

                    if (blobClusters.Count > 0)
                    {
                        // Draw blobClusters.
                        foreach (BlobCluster cluster in blobClusters)
                        {
                            // Draw horizontal lines around cluster.
                            for (int i = 0; i < cluster.Width; i++)
                            {
                                try
                                {
                                    // Top horizontal line index.
                                    int topHoriz = cluster.Location.Y * scanArea.Width + cluster.Location.X + i;
                                    // Color line.
                                    imageData.data[topHoriz].B = 0;
                                    imageData.data[topHoriz].G = 255;
                                    imageData.data[topHoriz].R = 255;
                                    imageData.data[topHoriz].A = 255;

                                    // Bottom horizontal line index.
                                    int botHoriz = (cluster.Location.Y + cluster.Location.Height) * scanArea.Width + cluster.Location.X + i;
                                    imageData.data[botHoriz].B = 0;
                                    imageData.data[botHoriz].G = 255;
                                    imageData.data[botHoriz].R = 255;
                                    imageData.data[botHoriz].A = 255;
                                }
                                catch { }
                            }
                            // Draw vertical lines around cluster.
                            for (int i = 0; i < cluster.Height; i++)
                            {
                                try
                                {
                                    int leftVert = (cluster.Location.Y + i) * scanArea.Width + cluster.Location.X;
                                    imageData.data[leftVert].B = 0;
                                    imageData.data[leftVert].G = 255;
                                    imageData.data[leftVert].R = 255;
                                    imageData.data[leftVert].A = 255;

                                    int rightVert = (cluster.Location.Y + i) * scanArea.Width + cluster.Location.X + cluster.Location.Width;
                                    imageData.data[rightVert].B = 0;
                                    imageData.data[rightVert].G = 255;
                                    imageData.data[rightVert].R = 255;
                                    imageData.data[rightVert].A = 255;
                                }
                                catch { }
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                Debug.Print("ColorTracker find() ERROR on BlobCluster: {0}", exc.Message);
            }

            if (blobs.Count < 1)
            {
                point.X = double.NaN;
                point.Y = double.NaN;
            }
            else
            {
                Int32Rect largest = new Int32Rect(0, 0, 0, 0);

                foreach (Int32Rect blob in blobs)
                {
                    if ((blob.Width * blob.Height) > (largest.Width * largest.Height))
                        largest = blob;
                }

                point.X = scanArea.X + largest.X + (largest.Width / 2);
                point.Y = scanArea.Y + largest.Y + (largest.Height / 2);
            }

            foundPoint = point;

            // Lock to make this relative resource thread safe.
            lock (ColorTracker._lock)
            {
                if (imageDatas.Count >= MAX_IMAGES_COUNT)
                    imageDatas.RemoveAt(System.Math.Max(imageDatas.Count - 1, 0));  // remove the one at the end.

                imageDatas.Insert(0, imageData);    // insert to the begining.
            }

            return point;
        }

        public static byte[] ToHCY(byte[] data)
        {
            byte[] colorData = new byte[data.Length];

            if (colorData == null)
                return null;

            for (int i = 0; i < colorData.Length; i = i + 4)
            {
                HCY hcy = new HCY(data[i], data[i + 1], data[i + 2]);

                colorData[i] = (byte)(255 * hcy.H);
                colorData[i + 1] = (byte)(255 * hcy.C);
                colorData[i + 2] = (byte)(255 * hcy.Y);
            }

            return colorData;
        }

        /// <summary>
        /// Returns true or false depending on whether or not the ColorTracker ColorRanges are empty.
        /// </summary>
        /// <returns>True: ColorRanges is empty. False: ColorRanges has values.</returns>
        public static bool IsColorRangesEmpty()
        {
            return ColorTracker.colorRanges.Count <= 0;
        }

        /// <summary>
        /// Get a ColorRange from the ColorTracker based on color and optionally lighting.
        /// </summary>
        /// <param name="colorName">Which color ColorRange to return. red, yellow, pink, etc. Maps to color element in XML file.</param>
        /// <param name="light">Which lighting of color to look for. If unspecified, first looks for Lighting.none, then if 
        /// none found looks for any Lighting.</param>
        /// <returns>ColorRange from the ColorTracker.ColorRanges collection. If multiple ColorRanges of the specified parameters 
        /// exist, returns the first found ( Multiples can exist in the XML file ). If no ColorRange is found, then returns null.</returns>
        public static ColorRange GetColorRange(string colorName, Lighting? light = null)
        {
            if (ColorTracker.colorRanges.ContainsKey(colorName))
            {
                bool tmpAnySet = false;
                ColorRange tmpAny = new ColorRange("", 0, 0, 0, 0, 0, 0);

                foreach (CTColor colorVal in ColorTracker.colorRanges[colorName])
                {
                    // If the caller supplied a Lighting value, only look for that value.
                    if (light.HasValue)
                    {
                        if (colorVal.Lighting == light.Value)
                        {
                            return colorVal.ColorRange;
                        }
                    }
                    // If the caller didn't supply a Lighting value, attempt to return a Lighting.none. If that is not found, attempt to return anything else of that color.
                    else
                    {
                        if (colorVal.Lighting == Lighting.none)
                        {
                            return colorVal.ColorRange;
                        }
                        else
                        {
                            tmpAnySet = true;
                            tmpAny = colorVal.ColorRange;
                        }
                    }
                }
                if (tmpAnySet)
                    return tmpAny;
            }
            return null;
        }

        /// <summary>
        /// Get a List of ColorRange from ColorTracker based on color and optionally lighting.
        /// </summary>
        /// <param name="colorName">Which color ColorRange to return. red, yellow, pink, etc. Maps to color element in XML file.</param>
        /// <param name="light">Which lighting of color to look for. If unspecified, returns list of specified color regardless of lighting.</param>
        /// <returns></returns>
        public static List<ColorRange> GetColorRanges(string colorName, Lighting? light = null)
        {
            if (ColorTracker.colorRanges.ContainsKey(colorName))
            {
                List<ColorRange> colorRanges = new List<ColorRange>();

                foreach (CTColor colorVal in ColorTracker.colorRanges[colorName])
                {
                    // If the caller specified a Lighting value, only look for that color Lighting pair.
                    if (light.HasValue)
                    {
                        if (colorVal.Lighting == light.Value)
                        {
                            colorRanges.Add(colorVal.ColorRange);
                        }
                    }
                    // If the caller did not supply a Lighting value, return everything of that color.
                    else
                    {
                        colorRanges.Add(colorVal.ColorRange);
                    }
                }
                if (colorRanges.Count > 0)
                {
                    return colorRanges;
                }
            }
            return null;
        }

        /// <summary>
        /// Adds a CTColor ColorRange to the color definitions file.
        /// </summary>
        /// <param name="ctColor">CTColor to add to ColorRanges file. Includes ColorRange and Lighting.</param>
        public static void AddColorRange(CTColor ctColor)
        {
            try // Load xml document chosen by save button.
            {
                ColorTracker.ParseColorRangeXMLFile();

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(ColorTracker.colorTrackerXmlFile);
                XmlNode root = xmlDoc.DocumentElement;

                // No matter wat, append value.
                XmlElement colorRange = xmlDoc.CreateElement("ColorRange");

                XmlElement color = xmlDoc.CreateElement("color");
                color.InnerText = ctColor.ColorRange.Color;
                color.SetAttribute("lighting", ctColor.Lighting.ToString());
                XmlElement hue = xmlDoc.CreateElement("hue");
                hue.InnerText = ctColor.ColorRange.Hue.ToString();
                XmlElement huethreshold = xmlDoc.CreateElement("hueThreshold");
                huethreshold.InnerText = ctColor.ColorRange.HueThreshold.ToString();
                XmlElement saturation = xmlDoc.CreateElement("saturation");
                saturation.InnerText = ctColor.ColorRange.Saturation.ToString();
                XmlElement saturationThreshold = xmlDoc.CreateElement("saturationThreshold");
                saturationThreshold.InnerText = ctColor.ColorRange.SaturationThreshold.ToString();
                XmlElement value = xmlDoc.CreateElement("value");
                value.InnerText = ctColor.ColorRange.Value.ToString();
                XmlElement valueThreshold = xmlDoc.CreateElement("valueThreshold");
                valueThreshold.InnerText = ctColor.ColorRange.ValueThreshold.ToString();

                colorRange.AppendChild(color);
                colorRange.AppendChild(hue);
                colorRange.AppendChild(huethreshold);
                colorRange.AppendChild(saturation);
                colorRange.AppendChild(saturationThreshold);
                colorRange.AppendChild(value);
                colorRange.AppendChild(valueThreshold);

                root.AppendChild(colorRange);
                xmlDoc.Save(ColorTracker.colorTrackerXmlFile);

                ColorTracker.colorRangesXmlFileModifiedDate = colorRangesXmlFileModifiedDate.Subtract(new TimeSpan(0, 0, 5));

                ColorTracker.ParseColorRangeXMLFile();
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print("ColorCalibrationControl ExportToXML() ERROR: {0}", exc.Message);
            }
        }

        /// <summary>
        /// Remove a ColorRange from the ColorRanges XML file. Returns true on removal, false otherwise.
        /// </summary>
        /// <param name="ctColor">Color to remove from the ColorRanges XML file.</param>
        /// <returns>Returns true on removal, false otherwise.</returns>
        /// <exception cref="System.ArgumentNullException">Thrown when ctColor is null.</exception>
        public static bool RemoveColorRange(CTColor ctColor)
        {
            if (ctColor == null)
                throw new ArgumentNullException("ctColor", "ctColor cannot be null.");

            try
            {
                // Variable to hold ctColor ColorRange for convenience.
                ColorRange colorRange = ctColor.ColorRange;

                ColorTracker.ParseColorRangeXMLFile();

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(ColorTracker.colorTrackerXmlFile);
                XmlNode root = xmlDoc.DocumentElement;

                XmlNodeList xmlColorRanges = root.SelectNodes("ColorRange");

                // Search through xml colorRanges and look for the given one. If it exists, remove it.
                for (int i = 0; i < xmlColorRanges.Count; i++)
                {
                    XmlNode color = xmlColorRanges[i].SelectSingleNode("color");

                    // Only keep processing if the color is correct. Skip otherwise, ( just do less work ).
                    if (color.InnerText == colorRange.Color)
                    {
                        XmlNode hue = xmlColorRanges[i].SelectSingleNode("hue");
                        XmlNode hueThreshold = xmlColorRanges[i].SelectSingleNode("hueThreshold");
                        XmlNode saturation = xmlColorRanges[i].SelectSingleNode("saturation");
                        XmlNode saturationThreshold = xmlColorRanges[i].SelectSingleNode("saturationThreshold");
                        XmlNode value = xmlColorRanges[i].SelectSingleNode("value");
                        XmlNode valueThreshold = xmlColorRanges[i].SelectSingleNode("valueThreshold");

                        try
                        {
                            double _hue = Double.Parse(hue.InnerText);
                            double _hueThresh = Double.Parse(hueThreshold.InnerText);
                            double _sat = Double.Parse(saturation.InnerText);
                            double _satThresh = Double.Parse(saturationThreshold.InnerText);
                            double _val = Double.Parse(value.InnerText);
                            double _valThresh = Double.Parse(valueThreshold.InnerText);

                            string lighting = (color.Attributes["lighting"] == null) ? Lighting.none.ToString() : color.Attributes["lighting"].InnerText;

                            // Compare ColorRange in file against passed in CTColor.
                            if (_hue == colorRange.Hue && _hueThresh == colorRange.HueThreshold && _sat == colorRange.Saturation &&
                                _satThresh == colorRange.SaturationThreshold && _val == colorRange.Value && _valThresh == colorRange.ValueThreshold &&
                                lighting == ctColor.Lighting.ToString())
                            {
                                // Remove this ColorRange from the xml file.
                                root.RemoveChild(xmlColorRanges[i]);
                                xmlDoc.Save(ColorTracker.colorTrackerXmlFile);
                                ColorTracker.ParseColorRangeXMLFile();
                                return true;
                            }
                        }
                        catch (Exception exc)
                        {
                        }
                    }
                }
                return false;
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print("ColorTracker RemoveColorRange() ERROR: {0}", exc.Message);
                return false;
            }
        }

        /// <summary>
        /// Sets the ColorRange of an object specified by name.
        /// </summary>
        /// <param name="object_name">Name of the object to set the ColorRange of. For instance, manikin-side.</param>
        /// <param name="ct_color">ColorRange used to identify an object with color tracking.</param>
        /// <exception cref="System.ArgumentNullException">If object_name, ct_color, or any of ct_colors properties are null, this exception is thrown.</exception>
        public static void SetObjectColorRange(string object_name, CTColor ct_color)
        {
            // Check that values to save exist.
            if (String.IsNullOrEmpty(object_name))
                throw new ArgumentNullException("object_name", "object_name cannot be null.");

            if (ct_color == null)
                throw new ArgumentNullException("ct_color", "ct_color cannot be null.");

            if (double.IsNaN(ct_color.ColorRange.Hue) || double.IsNaN(ct_color.ColorRange.Saturation) ||
                double.IsNaN(ct_color.ColorRange.Value) || double.IsNaN(ct_color.ColorRange.HueThreshold) ||
                double.IsNaN(ct_color.ColorRange.SaturationThreshold) || double.IsNaN(ct_color.ColorRange.ValueThreshold))
            {
                throw new ArgumentNullException("ct_color", "ct_color's properties have null attributes.");
            }

            try // Load xml document chosen by save button.
            {
                // Make sure most up-to-date file.
                ColorTracker.ParseObjectColorsXMLFile();

                XmlDocument xmlDoc = new XmlDocument();

                xmlDoc.Load(ColorTracker.objectColorsXmlFile);
                XmlNode root = xmlDoc.DocumentElement;

                // Look for existing ObjectColor with this objectName in XML file and remove it.
                XmlNodeList xmlObjectColors = root.SelectNodes("ObjectColor");

                // Iterate over all of the ObjectColor elements in the XML file
                for (int i = 0; i < xmlObjectColors.Count; i++)
                {
                    XmlNode objectName_ele = xmlObjectColors[i].SelectSingleNode("objectName");

                    // Look for this objectName, remove if found.
                    if (objectName_ele.InnerText == object_name)
                        root.RemoveChild(xmlObjectColors[i]);
                } // done searching ObjectColors

                // Save the new ObjectColor
                XmlElement objectColor = xmlDoc.CreateElement("ObjectColor");
                XmlElement objectName = xmlDoc.CreateElement("objectName");
                objectName.InnerText = object_name;
                XmlElement colorRange = xmlDoc.CreateElement("ColorRange");

                XmlElement color = xmlDoc.CreateElement("color");
                color.InnerText = ct_color.ColorRange.Color;
                color.SetAttribute("lighting", ct_color.Lighting.ToString());
                XmlElement hue = xmlDoc.CreateElement("hue");
                hue.InnerText = ct_color.ColorRange.Hue.ToString();
                XmlElement huethreshold = xmlDoc.CreateElement("hueThreshold");
                huethreshold.InnerText = ct_color.ColorRange.HueThreshold.ToString();
                XmlElement saturation = xmlDoc.CreateElement("saturation");
                saturation.InnerText = ct_color.ColorRange.Saturation.ToString();
                XmlElement saturationThreshold = xmlDoc.CreateElement("saturationThreshold");
                saturationThreshold.InnerText = ct_color.ColorRange.SaturationThreshold.ToString();
                XmlElement value = xmlDoc.CreateElement("value");
                value.InnerText = ct_color.ColorRange.Value.ToString();
                XmlElement valueThreshold = xmlDoc.CreateElement("valueThreshold");
                valueThreshold.InnerText = ct_color.ColorRange.ValueThreshold.ToString();

                colorRange.AppendChild(color);
                colorRange.AppendChild(hue);
                colorRange.AppendChild(huethreshold);
                colorRange.AppendChild(saturation);
                colorRange.AppendChild(saturationThreshold);
                colorRange.AppendChild(value);
                colorRange.AppendChild(valueThreshold);

                objectColor.AppendChild(objectName);
                objectColor.AppendChild(colorRange);

                root.AppendChild(objectColor);
                xmlDoc.Save(ColorTracker.objectColorsXmlFile);

                ColorTracker.objectColorsXmlFileModifiedDate = ColorTracker.objectColorsXmlFileModifiedDate.Subtract(new TimeSpan(0, 0, 5));

                ColorTracker.ParseObjectColorsXMLFile();
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print("ColorTracker SetObjectColorRange() ERROR: {0}", exc.Message);
            }
        }

        /// <summary>
        /// Gets the ColorRange of an object specified by name.
        /// </summary>
        /// <param name="objectName">Name of the object to set the ColorRange of. For instance, manikin-side.</param>
        /// <returns>ColorRange used to identify an object with color tracking.</returns>
        /// <exception cref="System.IndexOutOfRangeException">If the objectName does not exist, throws exception.</exception>
        public static CTColor GetObjectColorRange(string objectName)
        {
            try
            {
                if (ColorTracker.ObjectColors.ContainsKey(objectName))
                {
                    return ColorTracker.ObjectColors[objectName];
                }
                else
                {
                    throw new IndexOutOfRangeException("Object name does not exist within ColorTracker.ObjectColors.");
                }
            }
            catch (Exception exc)
            {
                Debug.Print("ColorTracker GetObjectColorRange() ERROR: {0}", exc.Message);
                return null;
            }
        }

        #endregion // Public Methods

        #region Private Methods

        /// <summary>
        /// Parses an XML file for ColorRanges and stores them in the ColorTracker.
        /// </summary>
        /// <param name="ColorTracker.colorTrackerXmlFile">XML File to parse ColorRanges from. Defaults to OUTPUTDIR/Assets/Lessons2/colorRanges.xml</param>
        private static void ParseColorRangeXMLFile()
        {
            // If xmlFile not supplied, create one from embedded colorRanges.xml file.
            if (!File.Exists(ColorTracker.colorTrackerXmlFile))
            {
                // Pull embedded resource stream.
                Assembly _assembly = Assembly.GetExecutingAssembly();
                Stream embeddedColorRangesFile = _assembly.GetManifestResourceStream("AIMS.Assets.Lessons2.colorRanges.xml");
                // Throw error if resource file isn't apart of the project.
                if (embeddedColorRangesFile == null)
                    throw new FileNotFoundException("colorRanges embedded resource missing. Make sure it's apart of the project.", "colorRanges.xml");

                // Create a new file to write to.
                var newFileStream = File.Create(ColorTracker.colorTrackerXmlFile);
                // Write embedded resource to new file.
                embeddedColorRangesFile.CopyTo(newFileStream);
                // Clean-up streams.
                embeddedColorRangesFile.Dispose();
                newFileStream.Dispose();

                Debug.Print("colorRanges.xml file didn't exist. Wrote new colorRanges.xml file to : {0}", ColorTracker.colorTrackerXmlFile);
            }

            try
            {
                // If the file has been modified since last we ran, re-build ColorTracker.ColorRanges
                DateTime lastWriteTime = File.GetLastWriteTime(ColorTracker.colorTrackerXmlFile);
                if (lastWriteTime > ColorTracker.colorRangesXmlFileModifiedDate)
                {
                    ColorTracker.colorRangesXmlFileModifiedDate = lastWriteTime;
                    ColorTracker.colorRanges.Clear();
                    ColorTracker.FireColorRangesUpdatedEvent();
                }
                // If the file has not been modified, there is no work to be done.
                else
                    return;

                XmlTextReader xmlReader = new XmlTextReader(ColorTracker.colorTrackerXmlFile);

                // Local variables to keep track of values for ColorTracker.ColorRanges Dictionary.
                string colorName = "";
                Lighting light = Lighting.none;
                double hue = Double.NaN, hueThreshold = Double.NaN, saturation = Double.NaN, saturationThreshold = Double.NaN,
                    value = Double.NaN, valueThreshold = Double.NaN;

                // Read an XML ColorRanges file.
                while (xmlReader.Read())
                {
                    switch (xmlReader.NodeType)
                    {
                        // <Element>
                        case XmlNodeType.Element:
                            // Mark which element should be SET when the XmlNodeType == XmlNodeType.Text
                            if (xmlReader.Name == "color")
                                colorName = "_SET_";
                            else if (xmlReader.Name == "hue")
                                hue = Double.NaN;
                            else if (xmlReader.Name == "hueThreshold")
                                hueThreshold = Double.NaN;
                            else if (xmlReader.Name == "saturation")
                                saturation = Double.NaN;
                            else if (xmlReader.Name == "saturationThreshold")
                                saturationThreshold = Double.NaN;
                            else if (xmlReader.Name == "value")
                                value = Double.NaN;
                            else if (xmlReader.Name == "valueThreshold")
                                valueThreshold = Double.NaN;

                            // Default lighting to none. ( May not be specified in the file / not required )
                            if (xmlReader.Name == "ColorRange")
                            {
                                light = Lighting.none;
                            }

                            // If there are attributes on the current element, check them.
                            if (xmlReader.HasAttributes)
                            {
                                while (xmlReader.MoveToNextAttribute())
                                {
                                    // Check if a supported lighting attribute exists. ( supported attributes exist in the Lighting enumerator )
                                    for (int i = 0; i < Enum.GetNames(typeof(Lighting)).Length; i++)
                                    {
                                        try
                                        {
                                            if (xmlReader.Value == Enum.GetName(typeof(Lighting), i))
                                                light = (Lighting)i;
                                        }
                                        catch (Exception exc)
                                        {
                                            throw new Exception("Make sure Lighting's values are from 0..n without skipping values!", exc);
                                        }
                                    }
                                }
                            }
                            break;

                        // <Element>Text</Element>
                        case XmlNodeType.Text:
                            // Set local variables based on which element it is.
                            try
                            {
                                if (colorName == "_SET_")
                                    colorName = xmlReader.Value;
                                else if (Double.IsNaN(hue))
                                {
                                    if (!Double.TryParse(xmlReader.Value, out hue))
                                        hue = 0;
                                }
                                else if (Double.IsNaN(hueThreshold))
                                {
                                    if (!Double.TryParse(xmlReader.Value, out hueThreshold))
                                        hueThreshold = 0;
                                }
                                else if (Double.IsNaN(saturation))
                                {
                                    if (!Double.TryParse(xmlReader.Value, out saturation))
                                        saturation = 0;
                                }
                                else if (Double.IsNaN(saturationThreshold))
                                {
                                    if (!Double.TryParse(xmlReader.Value, out saturationThreshold))
                                        saturationThreshold = 0;
                                }
                                else if (Double.IsNaN(value))
                                {
                                    if (!Double.TryParse(xmlReader.Value, out value))
                                        value = 0;
                                }
                                else if (Double.IsNaN(valueThreshold))
                                {
                                    if (!Double.TryParse(xmlReader.Value, out valueThreshold))
                                        valueThreshold = 0;
                                }
                            }
                            catch (FormatException fexc)
                            {
                                Debug.Print("********\nBad Format of {0} in ColorRanges XML File. Value replaced with 0 : {1}\n********",
                                    xmlReader.Value, fexc.Message);
                            }
                            catch (Exception exc)
                            {
                                Debug.Print("ColorTracker.cs ParseXMLFile ERROR: {0}", exc.Message);
                            }
                            break;

                        // </Element>
                        case XmlNodeType.EndElement:
                            // Add the ColorRange to the ColorTracker.ColorRanges Dictionary.
                            if (xmlReader.Name == "ColorRange")
                            {
                                try
                                {
                                    foreach (string key in ColorTracker.colorRanges.Keys)
                                    {
                                        // If the color is already in the dictionary, add to that color's list.
                                        if (colorName == key)
                                        {
                                            ColorTracker.colorRanges[colorName].Add(new CTColor(new ColorRange(colorName, hue, hueThreshold, saturation,
                                                saturationThreshold, value, valueThreshold), light));
                                            ColorTracker.FireColorRangesUpdatedEvent();
                                            colorName = String.Empty;
                                        }
                                    }
                                    // Color was not in the dictionary, add it to the dictionary.
                                    if (colorName != String.Empty)
                                    {
                                        ColorTracker.colorRanges.Add(colorName, new List<CTColor>(10));
                                        ColorTracker.colorRanges[colorName].Add(new CTColor(new ColorRange(colorName, hue, hueThreshold, saturation,
                                                saturationThreshold, value, valueThreshold), light));
                                        ColorTracker.FireColorRangesUpdatedEvent();
                                    }
                                }
                                catch (Exception exc)
                                {
                                    Debug.Print("********\nFailed to add ColorRange of color {0} to ColorTracker.ColorRanges ! ERROR: {1}", colorName, exc.Message);
                                }
                            }
                            break;

                        // Other Node Types.
                        default:
                            //       Debug.Print("XML UnSupported {2} : {0} - {1}", xmlReader.Name, xmlReader.Value, xmlReader.NodeType);
                            break;
                    }
                }
                xmlReader.Dispose();
            }
            catch (Exception exc)
            {
                Debug.Print("ColorTracker.cs ParseXMLFile ERROR: {0}", exc.Message);
            }
            // Show the result of loading the xml file.
            ColorTracker.PrintColorRanges();
        }

        /// <summary>
        /// Parses an XML file for Object Colors and stores them in the ColorTracker.
        /// </summary>
        private static void ParseObjectColorsXMLFile()
        {
            // If xmlFile not supplied, create one from embedded colorRanges.xml file.
            if (!File.Exists(ColorTracker.objectColorsXmlFile))
            {
                // Pull embedded resource stream.
                Assembly _assembly = Assembly.GetExecutingAssembly();
                Stream embeddedColorRangesFile = _assembly.GetManifestResourceStream("AIMS.Assets.Lessons2.objectColors.xml");
                // Throw an error if resource file isn't apart of the project.
                if (embeddedColorRangesFile == null)
                    throw new FileNotFoundException("objectColors embedded resource missing. Make sure it's apart of the project.", "objectColors.xml");

                // Create a new file to write to.
                var newFileStream = File.Create(ColorTracker.objectColorsXmlFile);
                // Write embedded resource to new file.
                embeddedColorRangesFile.CopyTo(newFileStream);
                // Clean-up streams.
                embeddedColorRangesFile.Dispose();
                newFileStream.Dispose();

                Debug.Print("objectColors.xml file didn't exist. Wrote new objectColors.xml file to : {0}", ColorTracker.objectColorsXmlFile);
            }

            try
            {
                // If the file has been modified since last we ran, re-build ColorTracker.ColorRanges
                DateTime lastWriteTime = File.GetLastWriteTime(ColorTracker.colorTrackerXmlFile);
                if (lastWriteTime > ColorTracker.objectColorsXmlFileModifiedDate)
                {
                    ColorTracker.objectColorsXmlFileModifiedDate = lastWriteTime;
                    ColorTracker.objectColors.Clear();
                    ColorTracker.FireObjectColorsUpdatedEvent();
                }
                // If the file has not been modified, there is no work to be done.
                else
                    return;

                XmlTextReader xmlReader = new XmlTextReader(ColorTracker.objectColorsXmlFile);

                // Local variables to keep track of values for ColorTracker.ColorRanges Dictionary.
                string objectName = "", colorName = "";
                Lighting light = Lighting.none;
                double hue = Double.NaN, hueThreshold = Double.NaN, saturation = Double.NaN, saturationThreshold = Double.NaN,
                    value = Double.NaN, valueThreshold = Double.NaN;

                // Read an XML ColorRanges file.
                while (xmlReader.Read())
                {
                    switch (xmlReader.NodeType)
                    {
                        // <Element>
                        case XmlNodeType.Element:
                            // Mark which element should be SET when the XmlNodeType == XmlNodeType.Text
                            if (xmlReader.Name == "objectName")
                                objectName = "_SET_";
                            if (xmlReader.Name == "color")
                                colorName = "_SET_";
                            else if (xmlReader.Name == "hue")
                                hue = Double.NaN;
                            else if (xmlReader.Name == "hueThreshold")
                                hueThreshold = Double.NaN;
                            else if (xmlReader.Name == "saturation")
                                saturation = Double.NaN;
                            else if (xmlReader.Name == "saturationThreshold")
                                saturationThreshold = Double.NaN;
                            else if (xmlReader.Name == "value")
                                value = Double.NaN;
                            else if (xmlReader.Name == "valueThreshold")
                                valueThreshold = Double.NaN;

                            // Default lighting to none. ( May not be specified in the file / not required )
                            if (xmlReader.Name == "ColorRange")
                            {
                                light = Lighting.none;
                            }

                            // If there are attributes on the current element, check them.
                            if (xmlReader.HasAttributes)
                            {
                                while (xmlReader.MoveToNextAttribute())
                                {
                                    // Check if a supported lighting attribute exists. ( supported attributes exist in the Lighting enumerator )
                                    for (int i = 0; i < Enum.GetNames(typeof(Lighting)).Length; i++)
                                    {
                                        try
                                        {
                                            if (xmlReader.Value == Enum.GetName(typeof(Lighting), i))
                                                light = (Lighting)i;
                                        }
                                        catch (Exception exc)
                                        {
                                            throw new Exception("Make sure Lighting's values are from 0..n without skipping values!", exc);
                                        }
                                    }
                                }
                            }
                            break;

                        // <Element>Text</Element>
                        case XmlNodeType.Text:
                            // Set local variables based on which element it is.
                            try
                            {
                                if (objectName == "_SET_")
                                    objectName = xmlReader.Value;
                                else if (colorName == "_SET_")
                                    colorName = xmlReader.Value;
                                else if (Double.IsNaN(hue))
                                {
                                    if (!Double.TryParse(xmlReader.Value, out hue))
                                        hue = 0;
                                }
                                else if (Double.IsNaN(hueThreshold))
                                {
                                    if (!Double.TryParse(xmlReader.Value, out hueThreshold))
                                        hueThreshold = 0;
                                }
                                else if (Double.IsNaN(saturation))
                                {
                                    if (!Double.TryParse(xmlReader.Value, out saturation))
                                        saturation = 0;
                                }
                                else if (Double.IsNaN(saturationThreshold))
                                {
                                    if (!Double.TryParse(xmlReader.Value, out saturationThreshold))
                                        saturationThreshold = 0;
                                }
                                else if (Double.IsNaN(value))
                                {
                                    if (!Double.TryParse(xmlReader.Value, out value))
                                        value = 0;
                                }
                                else if (Double.IsNaN(valueThreshold))
                                {
                                    if (!Double.TryParse(xmlReader.Value, out valueThreshold))
                                        valueThreshold = 0;
                                }
                            }
                            catch (FormatException fexc)
                            {
                                Debug.Print("********\nBad Format of {0} in ColorRanges XML File. Value replaced with 0 : {1}\n********",
                                    xmlReader.Value, fexc.Message);
                            }
                            catch (Exception exc)
                            {
                                Debug.Print("ColorTracker.cs ParseXMLFile ERROR: {0}", exc.Message);
                            }
                            break;

                        // </Element>
                        case XmlNodeType.EndElement:
                            // Add the ColorRange to the ColorTracker.ColorRanges Dictionary.
                            if (xmlReader.Name == "ObjectColor")
                            {
                                try
                                {
                                    foreach (string key in ColorTracker.objectColors.Keys)
                                    {
                                        // If the color is already in the dictionary, add to that color's list.
                                        if (objectName == key)
                                        {
                                            ColorTracker.objectColors[objectName] = new CTColor(new ColorRange(colorName, hue, hueThreshold, saturation,
                                                saturationThreshold, value, valueThreshold), light);
                                            /*ColorTracker.colorRanges[colorName].Add(new CTColor(new ColorRange(hue, hueThreshold, saturation,
                                                saturationThreshold, value, valueThreshold), light));*/
                                            ColorTracker.FireObjectColorsUpdatedEvent();
                                            objectName = String.Empty;
                                            colorName = String.Empty;
                                        }
                                    }
                                    // Object was not in the dictionary, add it to the dictionary.
                                    if (objectName != String.Empty)
                                    {
                                        ColorTracker.objectColors.Add(objectName, new CTColor(new ColorRange(colorName, hue, hueThreshold, saturation,
                                                saturationThreshold, value, valueThreshold), light));
                                        ColorTracker.FireObjectColorsUpdatedEvent();
                                    }
                                }
                                catch (Exception exc)
                                {
                                    Debug.Print("********\nFailed to add ObjectColor for object {2} of color {0} to ColorTracker.ObjectColors! ERROR: {1}", colorName, exc.Message, objectName);
                                }
                            }
                            break;

                        // Other Node Types.
                        default:
                            //       Debug.Print("XML UnSupported {2} : {0} - {1}", xmlReader.Name, xmlReader.Value, xmlReader.NodeType);
                            break;
                    }
                }
                xmlReader.Dispose();
            }
            catch (Exception exc)
            {
                Debug.Print("ColorTracker.cs ParseXMLFile ERROR: {0}", exc.Message);
            }
            // Show the result of loading the xml file.
            ColorTracker.PrintObjectColors();
        }

        /// <summary>
        /// Prints ColorTracker.ColorRanges data to System.Diagnostics.Debug.
        /// </summary>
        private static void PrintColorRanges()
        {
            Debug.Print("ColorTracker ColorRanges!");
            foreach (var item in ColorTracker.colorRanges)
            {
                Debug.Print("\tColor: {0}", item.Key.ToString());
                foreach (CTColor color in item.Value)
                {
                    Debug.Print(color.ToString());
                }
            }
        }

        /// <summary>
        /// Prints ColorTracker.ObjectColors data to System.Diagnostics.Debug.
        /// </summary>
        private static void PrintObjectColors()
        {
            Debug.Print("Object ColorRanges!");
            foreach (var item in ColorTracker.objectColors)
            {
                Debug.Print("Object: {0}\nColorRange: {1}", item.Key.ToString(), item.Value.ToString());
            }
        }

        private static void FireColorRangesUpdatedEvent()
        {
            if (ColorTracker.ColorRangesUpdated != null)
                ColorTracker.ColorRangesUpdated(typeof(ColorTracker), null);
        }

        private static void FireObjectColorsUpdatedEvent()
        {
            if (ColorTracker.ObjectColorsUpdated != null)
                ColorTracker.ObjectColorsUpdated(typeof(ColorTracker), null);
        }

        private static void DiscoverClusters(Int32Rect[] blobs, int neighborRange)
        {

        }

        #endregion

        #region ODU Color Tracker

        /// <summary>
        /// Return a Bitmap by processing a byte array of color data.
        /// </summary>
        /// <param name="colorData"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="region"></param>
        /// <returns></returns>
        public static Bitmap ImageToBitmap(byte[] colorData, int width, int height)
        {
            byte[] pixeldata = new byte[colorData.Length];
            // Image.CopyPixelDataTo(pixeldata);
            for (int i = 0; i < colorData.Length; i++)
            {
                pixeldata[i] = colorData[i];
            }

            Bitmap bmap = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppRgb);

            System.Drawing.Imaging.BitmapData bmapdata = bmap.LockBits(new System.Drawing.Rectangle(0, 0, width, height),
                                                                          System.Drawing.Imaging.ImageLockMode.WriteOnly,
                                                                          bmap.PixelFormat);
            IntPtr ptr = bmapdata.Scan0;
            System.Runtime.InteropServices.Marshal.Copy(pixeldata, 0, ptr, colorData.Length);
            bmap.UnlockBits(bmapdata);
            return bmap;
        }

        /// <summary>
        /// Analyze a byte array of color data for canny blobs.
        /// </summary>
        public static List<System.Drawing.Point> PerformCannyBlobDetection(byte[] colorData, int width, int height, Boolean filter_flag = true,
            int min_hue_Value = 113, int max_hue_Value = 255, int min_sat_Value = 147, int max_sat_Value = 255)
        {
            List<System.Drawing.Point> itemsFoundList = new List<System.Drawing.Point>();
            Bitmap image = ImageToBitmap(colorData, width, height);

            Image<Bgr, Byte> img = new Image<Bgr, byte>(image);
            Image<Gray, Byte> canny;
            Image<Gray, Byte> frame1 = null;
            Image<Gray, Byte> smoothImg = null;

            if (filter_flag == true)
            {
                Image<Hsv, Byte> hsvimg = img.Convert<Hsv, Byte>();                                                                                                               //extract the hue and value channels 
                Image<Gray, Byte>[] channels = hsvimg.Split();                                                        //split into components
                Image<Gray, Byte> imghue = channels[0];                                                               //hsv, so channels[0] is hue.
                Image<Gray, Byte> imgval = channels[2];                                                               //hsv, so channels[2] is value.
                Image<Gray, Byte> imgsat = channels[1];                                                               //hsv, so channels[1] is saturation.

                //filter out all but "the color you want
                Image<Gray, byte> huefilter = imghue.InRange(new Gray(min_hue_Value), new Gray(max_hue_Value));
                Image<Gray, byte> satfilter = imgsat.InRange(new Gray(min_sat_Value), new Gray(max_sat_Value));
                //huefilter = huefilter.SmoothGaussian(17);                                                           //this improved results fairly well
                //now and the two to get the parts of the imaged that are colored and above some brightness.
                Image<Gray, byte> detimg = huefilter.And(satfilter);
                smoothImg = detimg.SmoothGaussian(17);                                                                //this offered better results than .SmoothGaussian(9, 9, 2, 2); or .SmoothGaussian(5, 5, 1.5, 1.5);
                canny = smoothImg.Canny(255, 255);
            }
            else
            {
                frame1 = img.Convert<Gray, Byte>();
                //find Canny edges 
                canny = frame1.Canny(255, 255);
            }

            //In this loop, we find contours of the canny image and using the
            //Bounding Rectangles, we find the location of the blob(s)
            Contour<System.Drawing.Point> contours = canny.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE,
                Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_EXTERNAL);

            System.Drawing.Point coordinate = new System.Drawing.Point();

            for (; contours != null; contours = contours.HNext)
            {
                coordinate = new System.Drawing.Point(contours.BoundingRectangle.X, contours.BoundingRectangle.Y);
                itemsFoundList.Add(coordinate);
            }

            return itemsFoundList;

        } // PerformCannyBlobDetection

        /// <summary>
        /// Analyze a byte array of color data for canny blobs within a specificed area.
        /// </summary>
        public static List<Int32Rect> PerformCannyBlobDetectionRect(byte[] colorData, int width, int height, Boolean filter_flag = true,
            int min_hue_Value = 113, int max_hue_Value = 255, int min_sat_Value = 147, int max_sat_Value = 255)
        {
            List<Int32Rect> itemsFoundList = new List<Int32Rect>();
            Bitmap image = ImageToBitmap(colorData, width, height);

            Image<Bgr, Byte> img = new Image<Bgr, byte>(image);
            Image<Gray, Byte> canny;
            Image<Gray, Byte> frame1 = null;
            Image<Gray, Byte> smoothImg = null;

            if (filter_flag == true)
            {
                Image<Hsv, Byte> hsvimg = img.Convert<Hsv, Byte>();                                                                                                               //extract the hue and value channels 
                Image<Gray, Byte>[] channels = hsvimg.Split();                                                        //split into components
                Image<Gray, Byte> imghue = channels[0];                                                               //hsv, so channels[0] is hue.
                Image<Gray, Byte> imgval = channels[2];                                                               //hsv, so channels[2] is value.
                Image<Gray, Byte> imgsat = channels[1];                                                               //hsv, so channels[1] is saturation.

                //filter out all but "the color you want
                Image<Gray, byte> huefilter = imghue.InRange(new Gray(min_hue_Value), new Gray(max_hue_Value));
                Image<Gray, byte> satfilter = imgsat.InRange(new Gray(min_sat_Value), new Gray(max_sat_Value));
                //huefilter = huefilter.SmoothGaussian(17);                                                           //this improved results fairly well
                //now and the two to get the parts of the imaged that are colored and above some brightness.
                Image<Gray, byte> detimg = huefilter.And(satfilter);
                smoothImg = detimg.SmoothGaussian(17);                                                                //this offered better results than .SmoothGaussian(9, 9, 2, 2); or .SmoothGaussian(5, 5, 1.5, 1.5);
                canny = smoothImg.Canny(255, 255);
            }
            else
            {
                frame1 = img.Convert<Gray, Byte>();
                //find Canny edges 
                //     smoothImg = frame1.SmoothGaussian(17);
                canny = frame1.Canny(255, 255);
                //   canny = smoothImg.Canny(new Gray(255), new Gray(255));
            }

            //In this loop, we find contours of the canny image and using the
            //Bounding Rectangles, we find the location of the blob(s)
            Contour<System.Drawing.Point> contours = canny.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE,
                Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_EXTERNAL);

            Int32Rect boundingBox = new Int32Rect();

            for (; contours != null; contours = contours.HNext)
            {
                boundingBox = new Int32Rect(contours.BoundingRectangle.X, contours.BoundingRectangle.Y, contours.BoundingRectangle.Width,
                    contours.BoundingRectangle.Height);

                itemsFoundList.Add(boundingBox);
            }
            return itemsFoundList;
        } // PerformCannyBlobDetectionRect

        #endregion // ODU Color Tracker
    }
}
