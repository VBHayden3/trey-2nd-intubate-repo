﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIMS.Assets.Lessons2
{
    /// <summary>
    /// Stores Hue, Chroma, and Lumen, all in a range of [0,1]
    /// </summary>
    internal class HCY
    {
        internal float H = 0;
        internal float C = 0;
        internal float Y = 0;

        internal HCY(byte B, byte G, byte R)
        {
            float blue = (float)B / 255;
            float green = (float)G / 255;
            float red = (float)R / 255;

            float M;
            float m;

            M = System.Math.Max(red, blue);
            M = System.Math.Max(M, green);

            m = System.Math.Min(red, blue);
            m = System.Math.Min(m, green);

            C = M - m;


            float alpha = 0.5f * (2 * red - green - blue);
            float beta = 0.866025f * (green - blue);

            H = (float)(System.Math.Atan2(alpha, beta));
            if (H < 0)
            {
                H += (float)(2 * System.Math.PI);
            }

            H *= 0.159154943f;

            Y = 0.3f * red + 0.59f * green + 0.11f * blue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="H">Hue</param>
        /// <param name="C">Chroma</param>
        /// <param name="Y">Luma</param>
        internal HCY(float H, float C, float Y)
        {
            this.H = H;
            this.C = C;
            this.Y = Y;
        }

        /// <summary>
        /// Creates a copy of an existing HCY color
        /// </summary>
        /// <param name="hcy"></param>
        internal HCY(HCY hcy)
        {
            this.H = hcy.H;
            this.C = hcy.C;
            this.Y = hcy.Y;
        }
    }
}
