﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;

namespace AIMS.Assets.Lessons2
{
    public class ImageData
    {
        public Pixel[] data;
        public int width;
        public int height;
        public int x;
        public int y;

        private byte[] _colorData;
        private Int32Rect _scanArea;

        internal ImageData(byte[] colorData, int width, int height, Int32Rect scanArea)
        {
            this.data = new Pixel[scanArea.Width * scanArea.Height];
            this.width = scanArea.Width;
            this.height = scanArea.Height;
            this.x = scanArea.X;
            this.y = scanArea.Y;

            for (int i = 0; i < this.height; i++)
            {
                for (int j = 0; j < this.width; j++)
                {
                    int colorIndex = (((scanArea.Y + i) * width) + scanArea.X + j) * 4;
                    int pixelIndex = i * scanArea.Width + j;

                    data[pixelIndex].B = colorData[colorIndex];
                    data[pixelIndex].G = colorData[colorIndex + 1];
                    data[pixelIndex].R = colorData[colorIndex + 2];
                    data[pixelIndex].A = colorData[colorIndex + 3];
                }
            }

            this._colorData = colorData.Clone() as byte[];
            this._scanArea = scanArea;
        }

        internal byte[] getBytes()
        {
            byte[] bytes = new byte[data.Length * 4];

            for (int i = 0; i < data.Length; i++)
            {
                bytes[4 * i] = data[i].B;
                bytes[4 * i + 1] = data[i].G;
                bytes[4 * i + 2] = data[i].R;
                bytes[4 * i + 3] = data[i].A;
            }

            return bytes;
        }

        /// <summary>
        /// Scans the image section for pixels within the specified range. Turns all pixels outside the range to black.
        /// </summary>
        /// <param name="hue"></param>
        /// <param name="hueThreshold"></param>
        /// <param name="saturation"></param>
        /// <param name="saturationThreshold"></param>
        /// <param name="value"></param>
        /// <param name="valueThreshold"></param>
        internal void Scan(double hue, double hueThreshold, double saturation, double saturationThreshold, double value,
            double valueThreshold)
        {
            for (int i = 0; i < data.Length; i++)
            {
                HCY hcy = new HCY(data[i].B, data[i].G, data[i].R);

                if (hcy.H > hue - hueThreshold && hcy.H < hue + hueThreshold && hcy.C > saturation - saturationThreshold && 
                    hcy.C < saturation + saturationThreshold && hcy.Y > value - valueThreshold && hcy.Y < value + valueThreshold)
                {
                    data[i].B = (byte)(hcy.H * 255);
                    data[i].G = (byte)(hcy.C * 255);
                    data[i].R = (byte)(hcy.Y * 255);
                    data[i].A = 255;
                }
                else
                {
                    data[i].setBlack();
                }
            }
        }

        internal void Filter(int minNeighbors, int radius)
        {
            Pixel[] buffer = new Pixel[data.Length];

            // Create a copy of the data so that all changes can occure at once
            data.CopyTo(buffer, 0);

            try
            {
                // For each pixel
                for (int i = 0; i < height; i++)
                {
                    for (int j = 0; j < width; j++)
                    {
                        // Only filter if the current pixel is not black already
                        if (!data[i * width + j].isBlack())
                        {
                            int count = 0;

                            // check if the pixels within radius are not black
                            for (int m = System.Math.Max(i - radius, 0); m < System.Math.Min(i + radius, height); m++)
                            {
                                for (int n = System.Math.Max(j - radius, 0); n < System.Math.Min(j + radius, width); n++)
                                {
                                    int index = m * width + n;

                                    if (index < buffer.Length)
                                    {
                                        // If the pixel is not black, increment the count
                                        if (!buffer[index].isBlack())
                                            count++;
                                    }
                                }
                            }

                            // set the pixel to black if there are not enough neighbors
                            if (count < minNeighbors)
                            {
                                data[i * width + j].setBlack();
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
        }

        internal ImageData Clone()
        {
            return new ImageData(this._colorData, this.width, this.height, this._scanArea);
        }
    }
}
