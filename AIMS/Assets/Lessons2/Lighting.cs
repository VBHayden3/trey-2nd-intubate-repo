﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIMS.Assets.Lessons2
{
    /// <summary>
    /// Supported color lightings.
    /// </summary>
    // lighting values MUST start at 0 and increment by 1! The values are used in a for-loop iterating over these values!
    // Also, do not remove Lighting.none. Any of the others may be removed / changed without breaking logic. These values 
    // correspond to the color lighting attribute in the color ranges xml schema.
    public enum Lighting
    {
        none = 0,
        low = 1,
        medium = 2,
        high = 3
    }
}
