﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIMS.Assets.Lessons2
{
    /// <summary>
    /// A struct with B, G, R, A, bytes and functions to check if its black and set it to black.
    /// </summary>
    public struct Pixel
    {
        public byte B;
        public byte G;
        public byte R;
        public byte A;

        /// <summary>
        /// Returns true if B, G, and R are all 0
        /// </summary>
        /// <returns></returns>
        public bool isBlack()
        {
            return ((B == 0) && (G == 0) && (R == 0) && (A == 16));
        }

        /// <summary>
        /// Resets the pixel to black
        /// </summary>
        public void setBlack()
        {
            B = 0;
            G = 0;
            R = 0;
            A = 16;
        }
    }
}
