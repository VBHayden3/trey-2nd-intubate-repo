﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Windows;
using Microsoft.Kinect;

namespace AIMS.Assets.Sessions
{
    public class Exercise
    {
        public delegate void ExerciseFinishedEventHandler(object sender);
        public event ExerciseFinishedEventHandler exerciseFinished;

        public delegate void RepFinishedEventHandler(object sender, int currentReps);
        public event RepFinishedEventHandler repFinished;

        public delegate void MaxApexChangedEventHandler(object sender, double maxAngle);
        public event MaxApexChangedEventHandler maxApexChanged;

        /// <summary>
        /// Returns a value ranging from 0.0 to 1.0. Returns 0 when the angle is 0, and 1.0 when the angle is equal
        /// to the target angle.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="currentAngle"></param>
        public delegate void AngleChangedEventHandler(object sender, double currentAngle);
        public event AngleChangedEventHandler angleChanged;

        public delegate double CustomMeasure(Joint[] measuredJoints);
        public delegate void CustomDraw(Joint[] drawnJoints);

        private int currentReps = 0;                    // The current number of reps the user has completed.
        private int maxReps = 1;                        // The number of reps the user has to complete.
        private double targetAngle = 90;                // The goal angle the user is trying to reach.

        private JointType[] measuredJoints;             // The 3 joints that the angle will be measured between.
        private JointType[] drawnJoints;                // Draws a line to connect all of these joints.
        private CustomMeasure customMeasure = null;     // For use if custom measurements need to be taken.
        private CustomDraw customDraw = null;           // For use if custom drawing is needed.
        private bool isFinished = false;                // Set to true when currentReps = maxReps.
        private bool apexReached = false;               // Set to true whe the user has gotten close to the target angle.
        private double maxApex = 0.0;                   // Highest angle reached by the user.
        private double currentAngle = 0.0;

        private Stopwatch stopwatch = new Stopwatch();

        #region constructors
        /// <summary>
        /// Creates an exercise routine.
        /// </summary>
        /// <param name="measuredJoints">Must Be an array of length 3. The 3 joints will be connected by bones,
        /// the angle is measured for the joint at array index 1</param>
        /// <param name="maxReps">The number of reps the user must complete to finish the exercise</param>
        /// <param name="targetAngle">The angle the user must reach</param>
        public Exercise(JointType[] measuredJoints, int maxReps, int targetAngle)
        {
            if(measuredJoints.Length != 3)
                throw(new Exception("Exercise: measuredJoints must be of length 3"));

            this.measuredJoints = measuredJoints;
            this.drawnJoints = measuredJoints;
            this.maxReps = maxReps;
            this.targetAngle = targetAngle;
        }

        /// <summary>
        /// Creates an exercise routine
        /// </summary>
        /// <param name="measuredJoints">Must Be an array of length 3. The 3 joints will be connected by bones,
        /// the angle is measured for the joint at array index 1</param>
        /// <param name="drawnJoints">An array of joints to be drawn. When drawing, a bone is drawn between
        /// the joints at indecies [n] and [n + 1] for n = 0 through (length - 1).</param>
        /// <param name="maxReps">The number of reps the user must complete to finish the exercise.</param>
        /// <param name="targetAngle">The angle the user must reach.</param>
        public Exercise(JointType[] measuredJoints, JointType[] drawnJoints, int maxReps, int targetAngle)
        {
            if (measuredJoints.Length != 3)
                throw (new Exception("Exercise: measuredJoints must be of length 3"));

            this.measuredJoints = measuredJoints;
            this.drawnJoints = drawnJoints;
            this.maxReps = maxReps;
            this.targetAngle = targetAngle;
        }

        /// <summary>
        /// Create an exercise routine with custom measurements or drawing
        /// </summary>
        /// <param name="measuredJoints">The Joints that will be measured. Must Be an array of length 3 if customMeasure is null</param>
        /// <param name="drawnJoints">The Joints that will be drawn. If customDraw is null, a bone is drawn between
        /// the joints at indecies [n] and [n + 1] for n = 0 through (length - 1).</param>
        /// <param name="maxReps">The number of reps the user must complete to finish the exercise.</param>
        /// <param name="targetAngle">The angle the user must reach. This is what the result of customMeasure is compared against for correctness</param>
        /// <param name="customMeasure">A function to have custom measurements. the result is compared against targetAngle for correctness. Leave null if
        /// to use default measurements. The function signature is: double customMeasure(Joint[] measuredJoints)</param>
        /// <param name="customDraw">A function to have custom drawing code. The function signature is: void customDraw(Joint[] drawnJoints)</param>
        public Exercise(JointType[] measuredJoints, JointType[] drawnJoints, int maxReps, int targetAngle, CustomMeasure customMeasure, CustomDraw customDraw)
        {
            if(measuredJoints.Length != 3 && customMeasure == null)
                throw (new Exception("Exercise: measuredJoints must be of length 3 or customMeasure must not be null"));

            this.measuredJoints = measuredJoints;
            this.drawnJoints = drawnJoints;
            this.maxReps = maxReps;
            this.targetAngle = targetAngle;
            this.customMeasure += customMeasure;
            this.customDraw += customDraw;
        }
        #endregion

        public void update(object sender, Body skeleton)
        {
            if (!isFinished)
            {
                // Find the angle between the joints.
                AIMS_Math.Vector3f v1 = new AIMS_Math.Vector3f(skeleton.Joints[measuredJoints[1]].Position.X - skeleton.Joints[measuredJoints[0]].Position.X,
                                                        skeleton.Joints[measuredJoints[1]].Position.Y - skeleton.Joints[measuredJoints[0]].Position.Y,
                                                        skeleton.Joints[measuredJoints[1]].Position.Z - skeleton.Joints[measuredJoints[0]].Position.Z);

                AIMS_Math.Vector3f v2 = new AIMS_Math.Vector3f(skeleton.Joints[measuredJoints[2]].Position.X - skeleton.Joints[measuredJoints[1]].Position.X,
                                                        skeleton.Joints[measuredJoints[2]].Position.Y - skeleton.Joints[measuredJoints[1]].Position.Y,
                                                        skeleton.Joints[measuredJoints[2]].Position.Z - skeleton.Joints[measuredJoints[1]].Position.Z);

                double newAngle = AIMS_Math.Vector3f.Angle(v1, v2);

                // Check to see if the angle changed
                if (newAngle != currentAngle)
                {
                    currentAngle = newAngle;

                    if (angleChanged != null)
                        angleChanged(this, currentAngle / targetAngle);
                }

                // Check to see if its a new max
                if (currentAngle > maxApex)
                {
                    maxApex = currentAngle;

                    if (maxApexChanged != null)
                        maxApexChanged(this, currentAngle);
                }

                // If an apex has not been reached yet, check if it has been
                if (!apexReached)
                {
                    if (currentAngle > targetAngle * 0.9)
                    {
                        apexReached = true;
                        currentReps++;

                        if (repFinished != null)
                            repFinished(this, currentReps);
                    }
                }
                // otherwise, check if the user has returned to close enough to the start to begin the next rep
                else
                {

                }

                // Check if we are finished
                if (currentReps >= maxReps)
                {
                    if (exerciseFinished != null)
                        exerciseFinished(this);

                    isFinished = true;
                }
            }
        }

        
    }
}
