﻿/****************************************************************************
 * 
 *  Class: Session
 *  Author: Mitchel Weate
 *  
 *  Sesson is an abstract class that provides PhysicalTherapy with methods for linking
 *  other components to the lesson as well as an update method. Sessions are for use
 *  with the physical therapy variant.
 *  
 *  A session contains a list of exercises that will need to be completed. Each
 *  exercise has 3 joints defined to measure the angle between as well as a
 *  list of bones to draw.
 *   
 * ***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Microsoft.Kinect;
using AIMS.Components;

namespace AIMS.Assets.Sessions
{
    public class Session
    {
        Stack<Exercise> exercises;       // A list of exercises. exercises are poped off the front when completed, as if it were a stack.

        public Session(Stack<Exercise> exercises)
        {
            this.exercises = exercises;

            foreach (Exercise exercise in exercises)
            {
                exercise.exerciseFinished += exerciseFinished;
            }
        }

        public void Update(object sender, Dictionary<JointType, Point> mappedPoints)
        {
            if (exercises.Count > 0)
            {
                //exercises.Peek().update(mappedPoints);
            }
        }

        public void exerciseFinished(object sender)
        {
            Exercise current = exercises.Pop();

            // Save any information about the exercise that was completed here, before the exercise is discarded.
        }

    }
}
