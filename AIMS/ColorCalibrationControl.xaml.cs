﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Xml;
using System.Diagnostics;

using AIMS.Assets.Lessons2;
using AIMS.Utilities.Kinect;

namespace AIMS
{
    /// <summary>
    /// Interaction logic for Test.xaml
    /// </summary>
    public partial class ColorCalibrationControl : UserControl, INotifyPropertyChanged
    {
        #region Events

        // Method used to notify property changes with binding.
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion // Events

        private const int width = 1920;
        private const int height = 1080;

        #region Enums

        public enum CurrentTool
        {
            Picker = 0,
            ColorRanges = 1,
            ObjectColors = 2
        }

        #endregion

        #region Constructor

        public ColorCalibrationControl()
        {
            InitializeComponent();

            this.DataContext = this;
        }

        #endregion // Constructor

        #region Control Properties

        /// <summary>
        /// Returns an enum of CurrentTool for the Selected tool in the UI.
        /// </summary>
        public CurrentTool SelectedTool
        {
            get
            {
                switch (this.tabCtrl_Tool.SelectedIndex)
                {
                    case 0:
                        return CurrentTool.Picker;
                    case 1:
                        return CurrentTool.ColorRanges;
                    case 2:
                        return CurrentTool.ObjectColors;
                    default:
                        return CurrentTool.Picker;
                }
            }
        }
        /// <summary>
        /// Radius around mouse-click to pick-up pixels.
        /// </summary>
        public int PixelRadius
        {
            get { return _pixelRadius; }
            set
            {
                if (_pixelRadius == value)
                    return;
                _pixelRadius = value;
                NotifyPropertyChanged("PixelRadius");
            }
        }
        /// <summary>
        /// Defines whether or not duplicate entries may exist within the collection of chosen colors.
        /// </summary>
        public bool AllowDuplicateEntries
        {
            get { return this._allowDuplicateEntries; }
            set
            {
                if (this._allowDuplicateEntries == value)
                    return;
                this._allowDuplicateEntries = value;
                this.NotifyPropertyChanged("AllowDuplicateEntries");
            }
        }
        /// <summary>
        /// ColorTracker Color Names.
        /// </summary>
        public String[] CTColorNames
        {
            get { return ColorTracker.ColorNames; }
        }
        /// <summary>
        /// Names of objects which have ColorRanges.
        /// </summary>
        public string[] ObjectColorNames
        {
            get { return ColorTracker.ObjectColorNames; }
        }
        /// <summary>
        /// ColorTracker ColorRanges.
        /// </summary>
        public Dictionary<string, List<CTColor>> CTColorRanges
        {
            get { return ColorTracker.ColorRanges; }
        }
        /// <summary>
        /// ColorTracker Object ColorRanges.
        /// </summary>
        public Dictionary<string, CTColor> ObjectColorRanges
        {
            get { return ColorTracker.ObjectColors; }
        }
        /// <summary>
        /// Collection of ColorData points built from mouse-clicks.
        /// </summary>
        public ObservableRangeCollection<ColorDataPoint> ColorDataPoints
        {
            get { return _colorDataPoints; }
            set { _colorDataPoints = value; }
        }
        /// <summary>
        /// The average ColorPoint
        /// </summary>
        public ColorDataPoint AverageColorPoint
        {
            get
            {
                switch (this.SelectedTool)
                {
                    case CurrentTool.Picker:
                        int totalR = 0;
                        int totalG = 0;
                        int totalB = 0;

                        int count = 0;

                        foreach (ColorDataPoint cdp in _colorDataPoints)
                        {
                            totalR += cdp.R;
                            totalG += cdp.G;
                            totalB += cdp.B;

                            count++;
                        }

                        if (count > 0)
                        {
                            this.CurrentColorRange = this.DetermineRange();
                            return new ColorDataPoint((byte)((double)totalR / count), (byte)(System.Math.Round((double)totalG / count)), (byte)(System.Math.Round((double)totalB / count)));
                        }
                        else
                        {
                            this.CurrentColorRange = new ColorRange("N/A", 0, 0, 0, 0, 0, 0);
                            return new ColorDataPoint(0, 0, 0);
                        }
                    case CurrentTool.ColorRanges:
                        try
                        {
                            CTColor selectedColor = this.lstVw_CTColorRanges.SelectedValue as CTColor;

                            if (selectedColor != null)
                                this.CurrentColorRange = selectedColor.ColorRange;

                            return new ColorDataPoint(0, 0, 0);
                        }
                        catch (Exception exc)
                        {
                            System.Diagnostics.Debug.Print("ColorCalibrationControl AverageColorPoint ColorRanges ERROR: {0}", exc.Message);
                            this.CurrentColorRange = new ColorRange("N/A", 0, 0, 0, 0, 0, 0);
                            return new ColorDataPoint(0, 0, 0);
                        }
                    case CurrentTool.ObjectColors:
                        try
                        {
                            CTColor selectedColor = this.ObjectColorRanges[this.cmboBx_objectColorName.SelectedValue as string];

                            if (selectedColor != null)
                                this.CurrentColorRange = selectedColor.ColorRange;

                            return new ColorDataPoint(0, 0, 0);
                        }
                        catch (Exception exc)
                        {
                            System.Diagnostics.Debug.Print("ColorCalibrationControl AverageColorPoint ObjectColors ERROR: {0}", exc.Message);
                            this.CurrentColorRange = new ColorRange("N/A", 0, 0, 0, 0, 0, 0);
                            return new ColorDataPoint(0, 0, 0);
                        }
                    default:
                        this.CurrentColorRange = new ColorRange("N/A", 0, 0, 0, 0, 0, 0);
                        return new ColorDataPoint(0, 0, 0);
                }
            }
        }
        /// <summary>
        /// Current described ColorRange
        /// </summary>
        public ColorRange CurrentColorRange
        {
            get { return this._currentColorRange; }
            private set
            {
                if (this._currentColorRange == value)
                    return;
                this._currentColorRange = value;
                this.NotifyPropertyChanged("CurrentColorRange");
            }
        }

        #endregion // Control Properties

        #region Control Fields

        // *** Background values for properties... Set with property not field! ***
        private int _pixelRadius = 0;
        private bool _allowDuplicateEntries = false;
        private ObservableRangeCollection<ColorDataPoint> _colorDataPoints = new ObservableRangeCollection<ColorDataPoint>();
        private ColorRange _currentColorRange = new ColorRange("N/A", 0, 0, 0, 0, 0, 0);
        private AutoColorCalibration _autoColorCalibration = new AutoColorCalibration(30);
        private DateTime _autoCalibStartTime = new DateTime();

        #endregion // Control Fields

        #region Public Methods

        public void AddAllValuesWithinRadiusOfPixelCoordinate(int x, int y, int radius, bool allowDuplicateEntires)
        {
            // Circle
            for (int i = -radius; i <= radius; i++)
            {
                for (int j = -radius; j <= radius; j++)
                {
                    if (System.Math.Pow(i, 2) + System.Math.Pow(j, 2) <= System.Math.Pow(radius, 2)) // add the ones that fall within the radius
                    {
                        this.AddPixelAtLocation(x + i, y + j);
                    }
                }
            }
        }

        public void RemoveAllValuesWithinRadiusOfPixelCoordinate(int x, int y, int radius)
        {
            // Circle
            for (int i = -radius; i <= radius; i++)
            {
                for (int j = -radius; j <= radius; j++)
                {
                    // Remove pixels within the radius.
                    if (System.Math.Pow(i, 2) + System.Math.Pow(j, 2) <= System.Math.Pow(radius, 2))
                    {
                        this.RemovePixelAtLocation(x + i, y + j);
                    }
                }
            }
        }

        #endregion // Public Methods

        #region Private Methods

        private void AddPixelAtLocation(int xCoordinate, int yCoordinate)
        {
            if (xCoordinate > 0 && xCoordinate < Display.ActualWidth && yCoordinate > 0 && yCoordinate < Display.ActualHeight)
            {
                int index = (yCoordinate * width + xCoordinate) * sizeof(int);

                if (KinectManager.ColorData != null && KinectManager.ColorData.Length > index)
                {
                    ColorDataPoint cdp = new ColorDataPoint(KinectManager.ColorData[index], KinectManager.ColorData[index + 1], KinectManager.ColorData[index + 2]);

                    if (this.AllowDuplicateEntries || !(this._colorDataPoints.FirstOrDefault(o => (o.R == cdp.R && o.G == cdp.G && cdp.B == cdp.B)) != null))
                    {
                        this._colorDataPoints.Add(cdp);
                    }
                }
            }
        }

        private void RemovePixelAtLocation(int XCoordinate, int YCoordinate)
        {
            if (XCoordinate > 0 && XCoordinate < Display.ActualWidth && YCoordinate > 0 && YCoordinate < Display.ActualHeight)
            {
                List<int> removeIndices = new List<int>();
                int index = (YCoordinate * width + XCoordinate) * sizeof(int);

                if (KinectManager.ColorData != null && KinectManager.ColorData.Length > index)
                {
                    try
                    {
                        ColorDataPoint cdp = new ColorDataPoint(KinectManager.ColorData[index], KinectManager.ColorData[index + 1], KinectManager.ColorData[index + 2]);

                        for (int i = 0; i < this._colorDataPoints.Count; i++)
                        {
                            if (this._colorDataPoints[i].Near(cdp))
                                removeIndices.Add(i);
                        }
                        foreach (int i in removeIndices)
                        {
                            this._colorDataPoints.RemoveAt(i);
                        }
                    }
                    catch (Exception exc)
                    {
                        System.Diagnostics.Debug.Print("ColorCalibrationControl RemovePixelAtLocation ERROR:{0}", exc.Message);
                    }
                }
            }
        }

        private ColorRange DetermineRange()
        {
            int count = 0;
            double hue = 0;
            double saturation = 0;
            double value = 0;
            double hueThreshold = 0;
            double saturationThreshold = 0;
            double valueThreshold = 0;

            if (this.ColorDataPoints != null)
            {
                foreach (ColorDataPoint cdp in this.ColorDataPoints)
                {
                    HCY hcy = new HCY(cdp.R, cdp.G, cdp.B);

                    /*  colorData[i] = (byte)(255 * hcy.H);
                      colorData[i + 1] = (byte)(255 * hcy.C);
                      colorData[i + 2] = (byte)(255 * hcy.Y);
                      HSVDataPoint hdp = ColorDataPoint.FromRGB(cdp.R, cdp.G, cdp.B);*/
                    hue += hcy.H;
                    saturation += hcy.C;
                    value += hcy.Y;

                    count++;
                }

                hue /= count;
                saturation /= count;
                value /= count;

                foreach (ColorDataPoint cdp in this.ColorDataPoints)
                {
                    //HSVDataPoint hdp = ColorDataPoint.FromRGB(cdp.R, cdp.G, cdp.B);
                    HCY hcy = new HCY(cdp.R, cdp.G, cdp.B);

                    if (System.Math.Abs(hcy.H - hue) > hueThreshold)
                        hueThreshold = System.Math.Abs(hcy.H - hue);

                    if (System.Math.Abs(hcy.C - saturation) > saturationThreshold)
                        saturationThreshold = System.Math.Abs(hcy.C - saturation);

                    if (System.Math.Abs(hcy.Y - value) > valueThreshold)
                        valueThreshold = System.Math.Abs(hcy.Y - value);
                }
            }

            System.Diagnostics.Debug.WriteLine(String.Format("h: {0}, hT: {1}, s: {2}, sT: {3}, v: {4}, vT: {5}", hue, hueThreshold, saturation,
                saturationThreshold, value, valueThreshold));

            return new ColorRange("N/A", hue, hueThreshold, saturation, saturationThreshold, value, valueThreshold);
        }

        // Sorts color data.
        private void SortColorData()
        {
            List<ColorDataPoint> sortList = this.ColorDataPoints.ToList();
            sortList.Sort();

            this.ColorDataPoints.ReplaceRange(sortList);
        }

        // Clears color data.
        private void ClearColorData()
        {
            this.ColorDataPoints.Clear();
        }

        #region Notification Methods

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion // Notification Methods

        #endregion // Private Methods

        #region Event Handlers

        // ColorCalibration tool Loaded
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.tabCtrl_Tool.SelectionChanged += tabCtrl_Tool_SelectionChanged;
            this.ColorDataPoints.CollectionChanged += ColorDataPoints_CollectionChanged;
            this._autoColorCalibration.AnalysisCompleted += _autoColorCalibration_AnalysisCompleted;
            this._autoColorCalibration.AnalysisStarted += _autoColorCalibration_AnalysisStarted;
        }

        // ColorCalibration tool Unloaded
        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            this.tabCtrl_Tool.SelectionChanged -= tabCtrl_Tool_SelectionChanged;
            this.ColorDataPoints.CollectionChanged -= ColorDataPoints_CollectionChanged;
            this._autoColorCalibration.AnalysisCompleted -= _autoColorCalibration_AnalysisCompleted;
        }

        // Tab Control Selection has been altered.
        private void tabCtrl_Tool_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.NotifyPropertyChanged("SelectedTool");
            this.NotifyPropertyChanged("AverageColorPoint");
            this.NotifyPropertyChanged("CTColorNames");
            this.NotifyPropertyChanged("CTColorRanges");
            this.NotifyPropertyChanged("ObjectColorNames");
            this.NotifyPropertyChanged("ObjectColorRanges");
        }

        // ColorDataPoints has been Added to or Removed From.
        private void ColorDataPoints_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            this.NotifyPropertyChanged("AverageColorPoint");
        }

        // AutoColorCalibration finished with analysis.
        private void _autoColorCalibration_AnalysisCompleted(object sender, ColorRange foundColorRange)
        {
            TimeSpan diff = DateTime.Now - this._autoCalibStartTime;
            if (foundColorRange != null)
            {
                foreach (var item in this.lstVw_CTColorRanges.Items)
                {
                    CTColor ctColor = item as CTColor;
                    if (ctColor.ColorRange == foundColorRange)
                    {
                        this.lstVw_CTColorRanges.SelectedItem = item;
                        //this.NotifyPropertyChanged("AverageColorPoint");
                        return;
                    }
                }
            }
        }

        // AutoColorCalibration started analysis.
        private void _autoColorCalibration_AnalysisStarted(object sender, EventArgs e)
        {
            this._autoCalibStartTime = DateTime.Now;
        }

        // ListView to select existing ColorRange definitions has changed.
        private void lstVw_CTColorRanges_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.NotifyPropertyChanged("AverageColorPoint");
        }

        // Object Color Name selection changed.
        private void cmboBx_objectColorName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.NotifyPropertyChanged("AverageColorPoint");
        }

        /// <summary>
        /// Occurs on MouseUp for the DislpayGrid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplayGrid_MouseUp(object sender, MouseButtonEventArgs e) // This is fired by the DisplayGrid because the ZoomBorder has it set as it's child element, which uses child.CaptureMouse() on mouse down, changing the focus to the grid rather than the image by the time MouseUp occurs.
        {
            if (e != null)
            {
                Point clicked = e.GetPosition(this.Display);

                // Left mouse button actions.
                if (e.ChangedButton == MouseButton.Left)
                {
                    if (!this.ZoomBorder.Dragging)
                    {
                        // if it wasn't a drag.
                        if (clicked != null)
                            AddAllValuesWithinRadiusOfPixelCoordinate((int)(clicked.X), (int)(clicked.Y), this.PixelRadius, this.AllowDuplicateEntries);
                    }
                }
                // Right mouse button actions.
                else if (e.ChangedButton == MouseButton.Right)
                {
                    if (clicked != null)
                        RemoveAllValuesWithinRadiusOfPixelCoordinate((int)clicked.X, (int)clicked.Y, this._pixelRadius);
                }
            }
        }

        private void DepictedColor_PreviewMouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            // Right Clicking the displayed value will drop it from the list.
            ColorDataPoint cdp = ((Border)sender).DataContext as ColorDataPoint;    // The CDP can be reclaimed through the border's DataContext property.

            if (cdp != null)
            {
                this.ColorDataPoints.Remove(cdp);   // Remove the cdp from the ObservableCollection.
            }
        }

        private void ColorListView_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete && this.ColorListView.SelectedItems != null && this.ColorListView.SelectedItems.Count > 0)
            {
                while (this.ColorListView.SelectedItems.Count > 0)
                {
                    ColorDataPoint cdp = this.ColorListView.SelectedItem as ColorDataPoint;    // The CDP can be reclaimed through the border's DataContext property.

                    if (cdp != null)
                        this.ColorDataPoints.Remove(cdp);   // Remove the cdp from the ObservableCollection.
                }
            }
        }

        // Delete Selected Color ( on Color Definitions tab ) clicked.
        private void btn_DeleteSelectedColor_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button DeleteButton = sender as Button;
                CTColor deleteColor = DeleteButton.DataContext as CTColor;

                ColorTracker.RemoveColorRange(deleteColor);
                this.NotifyPropertyChanged("CTColorNames");
                this.NotifyPropertyChanged("CTColorRanges");
            }
            catch (Exception exc) { Debug.Print("ColorCalibrationControl btn_DeleteSelectedColor_Click() ERROR: {0}", exc.Message); }
        }

        // Discover Best Color clicked ( on Color Definitions tab ).
        private void btn_DiscoverBestColor_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string color = this.cmboBx_CTColorNames.SelectedValue.ToString();
                Int32Rect scanArea = new Int32Rect(0, 0, width, height);

                this._autoColorCalibration.FindBestColorRange(color, scanArea);
            }
            catch (Exception exc) { Debug.Print("ColorCalibrationControl btn_DiscoverBestColor_Click() ERROR: {0}", exc.Message); }
        }

        // SortList button clicked.
        private void btn_SortList_Click(object sender, RoutedEventArgs e)
        {
            this.SortColorData();
        }

        // Clear List button clicked.
        private void btn_ClearList_Click(object sender, RoutedEventArgs e)
        {
            this.ClearColorData();
        }

        // Export Color button clicked.
        private void btn_ExportColor_Click(object sender, RoutedEventArgs e)
        {
            ColorTracker.AddColorRange(new CTColor(this.CurrentColorRange));
            this.NotifyPropertyChanged("CTColorNames");
            this.NotifyPropertyChanged("CTColorRanges");
        }

        // Reset Pan/Zoom button clicked.
        private void btn_ResetTransforms_Click(object sender, RoutedEventArgs e)
        {
            this.ZoomBorder.Reset();
        }

        // Clear Test button clicked.
        private void btn_ClearTest_Click(object sender, RoutedEventArgs e)
        {
            ColorTracker.Images.Clear();
            this.ColorTrackerOverlay.UpdateOverlay();
        }

        // Test Range button clicked.
        private void btn_TestRange_Click(object sender, RoutedEventArgs e)
        {
            int frameWidth = width;
            int frameHeight = height;

            ColorTracker.Images.Clear();
            ColorTracker.find(this.CurrentColorRange, new Int32Rect(0, 0, frameWidth, frameHeight), KinectManager.ColorData);
            this.ColorTrackerOverlay.UpdateOverlay();
        }

        // Set Object Value button clicked.
        private void btn_setObjectValue_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ColorTracker.SetObjectColorRange(this.cmboBx_setObjectNames.SelectedValue as string, new CTColor(this.CurrentColorRange));
                this.NotifyPropertyChanged("ObjectColorNames");
                this.NotifyPropertyChanged("ObjectColorRanges");
            }
            catch (Exception exc) { Debug.Print("ColorCalibrationControl btn_setObjectValue_Click ERROR: {0}", exc.Message); }
        }

        #endregion // Event Handlers

        #region Local Classes ( May Remove Later )

        public class ColorDataPoint : IComparable<ColorDataPoint>
        {
            private byte r, g, b = 0;

            public byte R { get { return r; } set { r = value; } }
            public byte G { get { return g; } set { g = value; } }
            public byte B { get { return b; } set { b = value; } }

            public ColorDataPoint(byte r, byte g, byte b)
            {
                R = r;
                G = g;
                B = b;
            }

            public override string ToString()
            {
                return String.Format("({0},{1},{2})", R, G, B);
            }

            public HSVDataPoint ToHSV()
            {
                return new HSVDataPoint(((double)B) / 255, ((double)G) / 255, ((double)R) / 255);
            }

            public double WeightedBrightness
            {
                get
                {
                    // Unweighhted -> V = (R + G + B)/3;
                    // Weighted -> V = 0.299R + 0.587G + 0.114B
                    return (0.299 * R) + (0.587 * G) + (0.114 * B);
                }
            }

            public int CompareTo(ColorDataPoint that)
            {
                // Note: This sorts by R, then G, then B. There are many alternative ways to sort.
                HSVDataPoint thisHSV = FromRGB(this.R, this.G, this.B);
                HSVDataPoint thatHSV = FromRGB(that.R, that.G, that.B);

                if (thisHSV.H < thatHSV.H)
                {
                    // return Less than zero if this object 
                    // is less than the object specified by the CompareTo method.
                    return -1;
                }
                else if (thisHSV.H > thatHSV.H)
                {
                    // return Greater than zero if this object is greater than 
                    // the object specified by the CompareTo method.
                    return 1;
                }
                else if (thisHSV.H == thatHSV.H)
                {
                    // return Zero if this object is equal to the object 
                    // specified by the CompareTo method.
                    if (thisHSV.S < thatHSV.S)
                    {
                        // return Less than zero if this object 
                        // is less than the object specified by the CompareTo method.
                        return -1;
                    }
                    else if (thisHSV.S > thatHSV.S)
                    {
                        // return Greater than zero if this object is greater than 
                        // the object specified by the CompareTo method.
                        return 1;
                    }
                    else if (thisHSV.S == thatHSV.S)
                    {
                        if (thisHSV.V < thatHSV.V)    // Equal R, Equal G, Less B
                        {
                            return -1;
                        }
                        else if (thisHSV.V > thatHSV.V)  // Equal R, Equal G, Greater B
                        {
                            return 1;
                        }
                        else // Equal R, Equal G, Equal B
                        {
                            return 0;
                        }
                    }
                }

                return 0;   // sanity
            }

            /// <summary>
            /// Checks whether or not another ColorDataPoint is close to this ColorDataPoint.
            /// </summary>
            /// <param name="cdp">ColorDataPoint to compare to.</param>
            /// <returns>True if near, False if not.</returns>
            public bool Near(ColorDataPoint cdp)
            {
                double nearRange = 0.000000000000000000000005;

                if ((this.R > cdp.R + nearRange || this.R < cdp.R - nearRange) &&
                    (this.G > cdp.G + nearRange || this.G < cdp.G - nearRange) &&
                    (this.B > cdp.B + nearRange || this.B < cdp.B - nearRange))
                {
                    return false;
                }

                return true;
            }

            public static HSVDataPoint FromRGB(Byte R, Byte G, Byte B)
            {
                float _R = (R / 255f);
                float _G = (G / 255f);
                float _B = (B / 255f);

                float _Min = System.Math.Min(System.Math.Min(_R, _G), _B);
                float _Max = System.Math.Max(System.Math.Max(_R, _G), _B);
                float _Delta = _Max - _Min;

                float H = 0;
                float S = 0;
                float L = (float)((_Max + _Min) / 2.0f);

                if (_Delta != 0)
                {
                    if (L < 0.5f)
                    {
                        S = (float)(_Delta / (_Max + _Min));
                    }
                    else
                    {
                        S = (float)(_Delta / (2.0f - _Max - _Min));
                    }

                    if (_R == _Max)
                    {
                        H = (_G - _B) / _Delta;
                    }
                    else if (_G == _Max)
                    {
                        H = 2f + (_B - _R) / _Delta;
                    }
                    else if (_B == _Max)
                    {
                        H = 4f + (_R - _G) / _Delta;
                    }
                }

                return new HSVDataPoint(H, S, L);
            }
        }

        public class HSVDataPoint
        {
            private double h, s, v = 0;

            public double H { get { return h; } set { h = value; } }
            public double S { get { return s; } set { s = value; } }
            public double V { get { return v; } set { v = value; } }

            public HSVDataPoint(double h, double s, double v)
            {
                H = h;
                S = s;
                V = v;
            }

            public override string ToString()
            {
                return String.Format("({0:0.#######},{1:0.#######},{2:0.#######})", H, S, V);
            }

            public ColorDataPoint ToColor()
            {
                return new ColorDataPoint((byte)(V * 255), (byte)(S * 255), (byte)(H * 255));
            }

            public ColorDataPoint ToRGB()
            {
                byte r, g, b;

                if (s == 0)
                {
                    r = (byte)System.Math.Round(v * 255d);
                    g = (byte)System.Math.Round(v * 255d);
                    b = (byte)System.Math.Round(v * 255d);
                }
                else
                {
                    double t1, t2;
                    double th = h / 6.0d;

                    if (v < 0.5d)
                    {
                        t2 = v * (1d + s);
                    }
                    else
                    {
                        t2 = (v + s) - (v * s);
                    }
                    t1 = 2d * v - t2;

                    double tr, tg, tb;

                    tr = th + (1.0d / 3.0d);
                    tg = th;
                    tb = th - (1.0d / 3.0d);

                    tr = ColorCalc(tr, t1, t2);
                    tg = ColorCalc(tg, t1, t2);
                    tb = ColorCalc(tb, t1, t2);
                    r = (byte)System.Math.Round(tr * 255d);
                    g = (byte)System.Math.Round(tg * 255d);
                    b = (byte)System.Math.Round(tb * 255d);
                }
                return new ColorDataPoint(r, g, b);
            }

            private static double ColorCalc(double c, double t1, double t2)
            {

                if (c < 0) c += 1d;
                if (c > 1) c -= 1d;
                if (6.0d * c < 1.0d) return t1 + (t2 - t1) * 6.0d * c;
                if (2.0d * c < 1.0d) return t2;
                if (3.0d * c < 2.0d) return t1 + (t2 - t1) * (2.0d / 3.0d - c) * 6.0d;
                return t1;
            }
        }

        #endregion
    }
}
