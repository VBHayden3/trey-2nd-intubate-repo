﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for _3DViewer.xaml
    /// </summary>
    public partial class _3DViewer : UserControl
    {
        private static readonly int colorWidth = 1920;
        private static readonly int colorHeight = 1080;
        private static readonly int depthWidth = 512;
        private static readonly int depthHeight = 424;

        // @todo phase out width/height with correct width/height
        private static readonly int width = 512;
        private static readonly int height = 424;


        private static readonly double scale = 1.0;
        private static readonly double depthThreshold = 2.0;

        private Point3DCollection pointCollection;
        private Int32Collection indecies;
        private PointCollection textureCoordinates;
        private MeshGeometry3D mesh;
        private WriteableBitmap bitmap = new WriteableBitmap(width, height, 96, 96, PixelFormats.Bgr32, null);
        private WriteableBitmap bitmap2D = new WriteableBitmap(width, height, 96, 96, PixelFormats.Bgr32, null);
        public WriteableBitmap Bitmap2D { get { return bitmap2D; } set { bitmap2D = value; } }

        private byte[] colorData = new byte[colorWidth * colorHeight * 4];
        private byte[] textureData;
        private ushort[] depthData = new ushort[depthWidth * depthHeight];
        private ColorSpacePoint[] colorCoordinates;

        private double depthScale = 0.0025;
        private short maxDepth = 0;

        // Variables for camera positioning
        double r = 4.0;
        double theta = 0.0;
        double phi = 0.0;
        Point3D lookAt = new Point3D(3.2, 2.4, 0.0);
        Point3D cameraOffset = new Point3D(0, 0, 4.0);   // offset from lookAt

        // Variables for mouse input
        Point mouseStart = new Point();
        int mouseWheelMin = 15000000;

        public _3DViewer()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();

                ImageBorder.Background = new ImageBrush(bitmap);

                Loaded += UserControl_Loaded;

                this.Focusable = true;

                Keyboard.Focus(this);

                int scaledWidth = (int)(width * scale);
                int scaledHeight = (int)(height * scale);

                this.Camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                this.Camera.LookDirection = new Vector3D(lookAt.X - this.Camera.Position.X, lookAt.Y - this.Camera.Position.Y, lookAt.Z - this.Camera.Position.Z);

                // Set up the mesh
                mesh = new MeshGeometry3D();
                pointCollection = new Point3DCollection(scaledWidth * scaledHeight);
                textureCoordinates = new PointCollection(scaledWidth * scaledHeight);
                indecies = new Int32Collection((3 * scaledWidth * scaledHeight - 2 * scaledHeight - 2 * scaledWidth + 1) * 6);

                textureData = new byte[colorData.Length];
                // @todo determine width/height
                colorCoordinates = new ColorSpacePoint[width * height];

                // Build out the mesh with a vertext at each pixel
                for (int i = 0; i < scaledHeight; i++)
                {
                    for (int j = 0; j < scaledWidth; j++)
                    {
                        Point3D point = new Point3D(0.01 * width - (0.01 / scale * j), 0.01 / scale * (scaledHeight - i), 0.0);
                        Point coordinate = new Point((double)j / (scaledWidth - 1), (double)i / (scaledHeight - 1));

                        pointCollection.Add(point);
                        textureCoordinates.Add(coordinate);
                    }
                }

                // Build out the triangles in the mesh
                for (int i = 0; i < scaledHeight - 1; i++)
                {
                    for (int j = 0; j < scaledWidth - 1; j++)
                    {
                        int index = 6 * (scaledWidth * i + j);
                        int position = (scaledWidth * i + j);

                        indecies.Add(position);
                        indecies.Add(position + scaledWidth + 1);
                        indecies.Add(position + 1);

                        indecies.Add(position);
                        indecies.Add(position + scaledWidth);
                        indecies.Add(position + scaledWidth + 1);
                    }
                }

                textureCoordinates.Freeze();

                mesh.Positions = pointCollection;
                mesh.TextureCoordinates = textureCoordinates;
                mesh.TriangleIndices = indecies;

                //Material material = new DiffuseMaterial(Brushes.CornflowerBlue);
                Material material = new DiffuseMaterial(new ImageBrush(bitmap));
                GeometryModel3D geoModel = new GeometryModel3D(mesh, material);

                ModelVisual3D visual = new ModelVisual3D();
                visual.Content = geoModel;
                this.MainViewport.Children.Add(visual);

                mesh.Positions = null;
                for (int i = 0; i < scaledHeight; i++)
                {
                    for (int j = 0; j < scaledWidth; j++)
                    {
                        ushort rawDepth = depthData[(int)(depthWidth * (i / scale) + (j / scale))];

                        // @todo how to upgrade this to Kinect v2?
                        //double depth = 5 - ((ushort)rawDepth >> DepthImageFrame.PlayerIndexBitmaskWidth) * depthScale;

                        // @note in the meantime...
                        double depth = 5 - maxDepth * depthScale;


                        //if (rawDepth < 0)
                        //depth = 5 - maxDepth * depthScale;

                        Point3D point = new Point3D(0.01 / scale * j, 0.01 / scale * (scaledHeight - i), depth);
                        pointCollection[scaledWidth * i + j] = point;
                    }
                }

                mesh.Positions = pointCollection;
            }
        }

        public void WipeClean()
        {
            this.setImages( new byte[width * height * 4], new ushort[width * height] );
        }

        public void ResetToInitialView()
        {
            depthScale = 0.0025;
            maxDepth = 0;

            // Variables for camera positioning
            r = 4.0;
            theta = 0.0;
            phi = 0.0;

            lookAt = new Point3D(3.2, 2.4, 0.0);
            cameraOffset = new Point3D(0, 0, 4.0);   // offset from lookAt

            this.Camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
            this.Camera.LookDirection = new Vector3D(lookAt.X - this.Camera.Position.X,
                                                        lookAt.Y - this.Camera.Position.Y,
                                                        lookAt.Z - this.Camera.Position.Z);
        }

        /// <summary>
        /// Takes a color and depth frame and builds the scene
        /// </summary>
        /// <param name="colorData"></param>
        /// <param name="depthData"></param>
        public void setImages(byte[] colorData, ushort[] depthData)
        {
            this.colorData = colorData;
            this.depthData = depthData;
            
            bitmap.WritePixels(new Int32Rect(0, 0, colorWidth, colorHeight), colorData, colorWidth * 4, 0);

            ImageBorder.Background = new ImageBrush(bitmap);
                 
            int scaledWidth = (int)(width * scale);
            int scaledHeight = (int)(height * scale);

            try
            {
                if (mesh.Positions != null)
                {
                    // Set the positions of the vertecies
                    mesh.Positions.Clear();
                    mesh.Positions = null;
                }
                for (int i = 0; i < scaledHeight; i++)
                {
                    for (int j = 0; j < scaledWidth; j++)
                    {
                        ushort rawDepth = depthData[(int)(depthWidth * (i / scale) + (j / scale))];
                        
                        
                        // @todo how to upgrade this to Kinect v2?
                        //double depth = 5 - ((ushort)rawDepth >> DepthImageFrame.PlayerIndexBitmaskWidth) * depthScale;

                        // @note in the meantime...
                        double depth = 5 - maxDepth * depthScale;


                        //if (rawDepth < 0)
                        //depth = 5 - maxDepth * depthScale;

                        Point3D point = new Point3D(0.01 / scale * j, 0.01 / scale * (scaledHeight - i), depth);
                        pointCollection[scaledWidth * i + j] = point;
                    }
                }
                mesh.Positions = pointCollection;

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }

            // Build out the triangles in the mesh
            mesh.TriangleIndices = null;
            indecies.Clear();

            try
            {
                for (int i = 0; i < scaledHeight - 1; i++)
                {
                    for (int j = 0; j < scaledWidth - 1; j++)
                    {
                        int index = 6 * (scaledWidth * i + j);
                        int position = (scaledWidth * i + j);
                        bool depthIsClose = true;

                        // Make sure the depths are all similar for the first triangle
                        depthIsClose = (System.Math.Abs((pointCollection[position].Z - pointCollection[position + 1].Z)) < depthThreshold) &&
                            (System.Math.Abs((pointCollection[position].Z - pointCollection[position + scaledWidth + 1].Z)) < depthThreshold);

                        if (depthIsClose)
                        {
                            indecies.Add(position);
                            indecies.Add(position + scaledWidth + 1);
                            indecies.Add(position + 1);
                        }
                        else
                        {
                            indecies.Add(position);
                            indecies.Add(position);
                            indecies.Add(position);
                        }

                        // Make sure all the depths are similar for the 2nd triangle
                        depthIsClose = (System.Math.Abs((pointCollection[position].Z - pointCollection[position + scaledWidth].Z)) < depthThreshold) &&
                            (System.Math.Abs((pointCollection[position].Z - pointCollection[position + scaledWidth + 1].Z)) < depthThreshold);

                        if (depthIsClose)
                        {
                            indecies.Add(position);
                            indecies.Add(position + scaledWidth);
                            indecies.Add(position + scaledWidth + 1);
                        }
                        else
                        {
                            indecies.Add(position);
                            indecies.Add(position);
                            indecies.Add(position);
                        }
                    }
                }
                mesh.TriangleIndices = indecies;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        private void UserControl_Loaded(object sender, EventArgs e)
        {
            
        }

        #region Input Events

        private void UserControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (this.ImageBorder.IsVisible)
                return;

            Key key = e.Key;

            double R;       // the radius of the circle cut along the xz plane
            switch (key)
            {
                case Key.Right:
                    // Orbit right
                    theta += 0.01;

                    //Find the Y value
                    cameraOffset.Y = r * System.Math.Sin(phi);

                    // Find the radius along the xz plane at the specified Y
                    R = r * System.Math.Cos(phi);

                    // Find the X and Z values
                    cameraOffset.X = R * System.Math.Sin(theta);
                    cameraOffset.Z = R * System.Math.Cos(theta);

                    this.Camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                    this.Camera.LookDirection = new Vector3D(lookAt.X - this.Camera.Position.X, lookAt.Y - this.Camera.Position.Y, lookAt.Z - this.Camera.Position.Z);

                    break;
                case Key.Left:
                    // Orbit left
                    theta -= 0.01;

                    //Find the Y value
                    cameraOffset.Y = r * System.Math.Sin(phi);

                    // Find the radius along the xz plane at the specified Y
                    R = r * System.Math.Cos(phi);

                    // Find the X and Z values
                    cameraOffset.X = R * System.Math.Sin(theta);
                    cameraOffset.Z = R * System.Math.Cos(theta);

                    this.Camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                    this.Camera.LookDirection = new Vector3D(lookAt.X - this.Camera.Position.X, lookAt.Y - this.Camera.Position.Y, lookAt.Z - this.Camera.Position.Z);

                    break;
                case Key.Up:
                    /*
                    // Orbit up
                    phi += 0.01;

                    // Clamp phi between -pi/2 and pi/2
                    phi = Math.Min(Math.PI / 2 - 0.001, phi);
                    phi = Math.Max(-Math.PI / 2 + 0.001, phi);

                    //Find the Y value
                    cameraOffset.Y = r * Math.Sin(phi);

                    // Find the radius along the xz plane at the specified Y
                    R = r * Math.Cos(phi);

                    // Find the X and Z values
                    cameraOffset.X = R * Math.Sin(theta);
                    cameraOffset.Z = R * Math.Cos(theta);
                    
                    this.Camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                    this.Camera.LookDirection = new Vector3D(lookAt.X - this.Camera.Position.X,
                                                                lookAt.Y - this.Camera.Position.Y,
                                                                lookAt.Z - this.Camera.Position.Z);
                     */
                    depthScale += 0.001;

                    break;
                case Key.Down:
                    // Orbit down
                    phi -= 0.01;

                    // Clamp phi between -pi/2 and pi/2
                    phi = System.Math.Min(System.Math.PI / 2 - 0.001, phi);
                    phi = System.Math.Max(-System.Math.PI / 2 + 0.001, phi);

                    //Find the Y value
                    cameraOffset.Y = r * System.Math.Sin(phi);

                    // Find the radius along the xz plane at the specified Y
                    R = r * System.Math.Cos(phi);

                    // Find the X and Z values
                    cameraOffset.X = R * System.Math.Sin(theta);
                    cameraOffset.Z = R * System.Math.Cos(theta);

                    this.Camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                    this.Camera.LookDirection = new Vector3D(lookAt.X - this.Camera.Position.X, lookAt.Y - this.Camera.Position.Y, lookAt.Z - this.Camera.Position.Z);

                    break;
                case Key.Space:
                    // Reset the camera to the start position
                    r = 10.0;
                    theta = 0.0;
                    phi = 0.0;
                    lookAt.X = 3.2;
                    lookAt.Y = 2.4;
                    lookAt.Z = 0.0;

                    cameraOffset.X = 0.0;
                    cameraOffset.Y = 0.0;
                    cameraOffset.Z = 10.0;

                    this.Camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                    this.Camera.LookDirection = new Vector3D(lookAt.X - this.Camera.Position.X,
                                                                lookAt.Y - this.Camera.Position.Y,
                                                                lookAt.Z - this.Camera.Position.Z);

                    break;
            }
        }
        
        private void UserControl_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (this.ImageBorder.IsVisible)
                return;

            mouseStart = e.GetPosition(null);
        }

        private void UserControl_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (this.ImageBorder.IsVisible)
                return;

            if (System.Math.Abs(e.Delta) < mouseWheelMin && e.Delta != 0)
                mouseWheelMin = System.Math.Abs(e.Delta);

            int ticks = e.Delta / mouseWheelMin;

            Vector3D cameraDirection = new Vector3D(cameraOffset.X, cameraOffset.Y, cameraOffset.Z);
            cameraDirection.Normalize();

            // Make sure that the radius will stay above 0
            // Distance traveled 
            if (r - (ticks * 0.1 * 1.732) > 0.1)
            {
                cameraOffset.X -= (0.1 * ticks) * cameraDirection.X;
                cameraOffset.Y -= (0.1 * ticks) * cameraDirection.Y;
                cameraOffset.Z -= (0.1 * ticks) * cameraDirection.Z;


                // Find the new radius
                r = System.Math.Sqrt(cameraOffset.X * cameraOffset.X + cameraOffset.Y * cameraOffset.Y + cameraOffset.Z * cameraOffset.Z);
            }

            this.Camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
            this.Camera.LookDirection = new Vector3D(lookAt.X - this.Camera.Position.X,
                                                        lookAt.Y - this.Camera.Position.Y,
                                                        lookAt.Z - this.Camera.Position.Z);

            e.Handled = true;
        }

        private void UserControl_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (this.ImageBorder.IsVisible)
                return;

            Point currentPosition = e.GetPosition(null);
            Vector diff = new Vector(currentPosition.X - mouseStart.X, currentPosition.Y - mouseStart.Y);

            // If dragging with the left mouse button, orbit the camera
            if (e.LeftButton == MouseButtonState.Pressed &&
                e.RightButton == MouseButtonState.Released &&
                (System.Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                System.Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                mouseStart = currentPosition;

                phi += 0.005 * diff.Y;
                theta -= 0.005 * diff.X;

                // Clamp phi between -pi/2 and pi/2
                phi = System.Math.Min(System.Math.PI / 2 - 0.001, phi);
                phi = System.Math.Max(-System.Math.PI / 2 + 0.001, phi);

                //Find the Y value
                cameraOffset.Y = r * System.Math.Sin(phi);

                // Find the radius along the xz plane at the specified Y
                double R = r * System.Math.Cos(phi);

                // Find the X and Z values
                cameraOffset.X = R * System.Math.Sin(theta);
                cameraOffset.Z = R * System.Math.Cos(theta);

                this.Camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                this.Camera.LookDirection = new Vector3D(lookAt.X - this.Camera.Position.X,
                                                            lookAt.Y - this.Camera.Position.Y,
                                                            lookAt.Z - this.Camera.Position.Z);
            }
            // Pan the camera on right click or mouswheel click
            else if (e.LeftButton == MouseButtonState.Released &&
                (e.RightButton == MouseButtonState.Pressed ||
                e.MiddleButton == MouseButtonState.Pressed) &&
                (System.Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                System.Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                mouseStart = currentPosition;

                double deltaY = 0.005 * diff.Y;
                double deltaX = -0.005 * diff.X;

                // Vectors for determining which direction the pan should move the camera
                Vector3D viewDirection = new Vector3D(this.Camera.LookDirection.X, this.Camera.LookDirection.Y, this.Camera.LookDirection.Z);
                Vector3D upDirection = new Vector3D(0, 1, 0);
                Vector3D xDirection;
                Vector3D yDirection;

                // Normalize the view vector so all following vecotrs will be unit vectors
                viewDirection.Normalize();

                // Move along the camera's x direction
                xDirection = Vector3D.CrossProduct(viewDirection, upDirection);
                lookAt.X += xDirection.X * deltaX;
                lookAt.Y += xDirection.Y * deltaX;
                lookAt.Z += xDirection.Z * deltaX;

                // Move along the camera's y direction
                yDirection = Vector3D.CrossProduct(xDirection, viewDirection);
                lookAt.X += yDirection.X * deltaY;
                lookAt.Y += yDirection.Y * deltaY;
                lookAt.Z += yDirection.Z * deltaY;

                this.Camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                this.Camera.LookDirection = new Vector3D(lookAt.X - this.Camera.Position.X,
                                                            lookAt.Y - this.Camera.Position.Y,
                                                            lookAt.Z - this.Camera.Position.Z);
            }
        }

        #endregion

        private void Toggle2D3DButton_Click(object sender, RoutedEventArgs e)
        {
            if (ImageBorder.Visibility == System.Windows.Visibility.Visible)
            {
                ImageBorder.Visibility = System.Windows.Visibility.Hidden;
            }
            else
            {
                ResetToInitialView();
                ImageBorder.Visibility = System.Windows.Visibility.Visible;
            }
        }
    }
}
