﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for AIMIVisualizer.xaml
    /// </summary>
    public partial class AIMIVisualizer : UserControl
    {
        private Random rand = new Random();

        private double scale = 0;
        private SpeechSynthesizer synth;
        private double distance = 50;
        private double length;

        private MemoryStream soundData = new MemoryStream();

        public double Scale { get { return scale; } set { if (scale == value) scale = value; else { scale = value; NotifyPropertyChanged("Scale"); } } }

        public double SoundIntensityLength
        {
            get { return length; }
            set
            {
                length = value;
                NotifyPropertyChanged("SoundIntensityLength");
            }
        }

        public double BassOffsetLength
        {
            get { return distance; }
            set
            {
                if (this.distance == value)
                    this.distance = value;
                else
                {
                    this.distance = value;
                    this.NotifyPropertyChanged("BassOffsetLength");
                }
            }
        }

        private DispatcherTimer timer;

        public AIMIVisualizer()
        {
            InitializeComponent();

            this.DataContext = this;

            this.Loaded += MainWindow_Loaded;
            this.Unloaded += MainWindow_Unloaded;

            CommandBinding customCommandBinding = new CommandBinding(SpeakPhraseCommand, ExecutedSpeakPhraseCommand, CanExecuteSpeakPhraseCommand);

            // attach CommandBinding to root window 
            this.CommandBindings.Add(customCommandBinding);
        }

        void MainWindow_Unloaded(object sender, RoutedEventArgs e)
        {
            synth.PhonemeReached -= PhonemeReached;

            if (this.synth != null)
            {
                this.synth.Dispose();
            }

            if (this.timer != null)
            {
                this.timer.Stop();
                this.timer = null;
            }
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            synth = new SpeechSynthesizer();
            this.timer = new DispatcherTimer() { Interval = TimeSpan.FromMilliseconds(100) };
            //this.timer.Tick += delegate { ConstantPulse(); };
            timer.Start();

            synth.PhonemeReached += PhonemeReached;

            synth.SetOutputToDefaultAudioDevice();
        }

        public void PhonemeReached(object sender, PhonemeReachedEventArgs args)
        {
            this.PulseBass(args);
        }

        public void PulseSound()
        {
            if (!this.IsEnabled)
                return;

            var n = 60;
            var r = 0.2 + 0.015 * this.BassOffsetLength + 5;    // this is supposed to be bass (soundData.bass)
            var wr = r - 0.1;

            for (int i = 0; i < 60; i++)
            {
                var n2 = 120;

                var wave = rand.NextDouble() * ((i / n2 * 256) >> 0); // this is supposed to be sound intensity (soundData.waveDataL[(i/n2*256)>>0])
                var d = i / n * System.Math.PI * 2 - System.Math.PI * 0.5;
                var r2 = wr + wave * 0.05;

                Line line = this.Canvas.Children[i] as Line;

                if (line != null)
                {
                    line.Y2 = (0.5 + rand.Next(1, 5) * r2) * 0.15;
                }

                Line innerLine = this.InnerCanvas.Children[i] as Line;

                if (innerLine != null)
                {
                    innerLine.Y2 = (0.5 + rand.Next(1, 5) * -r2) * 0.15;
                }
            }
        }

        public void PulseBass(PhonemeReachedEventArgs arg)
        {
            this.BassOffsetLength = System.Math.Max(50, System.Math.Min(65, (arg.Duration.TotalSeconds * 25) + 50));   // clamp between 50 and 65

            PulseSound();

            // System.Diagnostics.Debug.WriteLine(arg.Phoneme);
        }

        public void ConstantPulse()
        {
            PulseSound();
            return;
            var n = 120;

            // draw blue wave bars
            var n2 = 120;   // 128 is 720 degree wrapping

            for (var i = 0; i < n2; i++)
            {
                var r = 0.2 + 0.015 * this.BassOffsetLength * (int)(i / (double)n2) * 5;    // this is supposed to be bass (soundData.bass)
                var wr = r - 0.1;

                var wave = synth.State == SynthesizerState.Speaking ? rand.Next(32, 64) : 32; // this is supposed to be sound intensity (soundData.waveDataL[(i/n2*256)>>0])
                var d = i / n * System.Math.PI * 2 - System.Math.PI * 0.5;
                var r2 = wr + wave * 0.05;

                Line line = i < 60 ? this.Canvas.Children[i] as Line : this.InnerCanvas.Children[i - 60] as Line;

                if (line != null)
                {
                    line.X1 = 0.5 + System.Math.Cos(d) * wr;
                    line.Y1 = 0.5 + System.Math.Sin(d) * wr;
                    line.X2 = 0.5 + System.Math.Cos(d) * (i >= 60 ? -r2 : r2);
                    line.Y2 = 0.5 + System.Math.Sin(d) * (i >= 60 ? -r2 : r2);
                }
            }
        }
        /*
        public void PulseWav()
        {
            var n = 5;
            var r = 0.2 + 0.015 * rand.NextDouble();    // this is supposed to be bass (soundData.bass)
            var wr = r - 0.1;

            // draw blue wave bars
            var n2 = 120;   // 128 is 720 degree wrapping

            for (var i = 0; i < n2; i++)
            {
                var wave = rand.Next(32, 128); // this is supposed to be sound intensity (soundData.waveDataL[(i/n2*256)>>0])
                var d = i / n * System.Math.PI * 2 - System.Math.PI * 0.5;
                var r2 = wr + wave * 0.05;

                Line line = i < 60 ? this.Canvas.Children[i] as Line : this.InnerCanvas.Children[i-60] as Line;
                
                if( line != null )
                {
                    line.X1 = 0.5 + System.Math.Cos(d) * wr;
                    line.Y1 = 0.5 + System.Math.Sin(d) * wr;
                    line.X2 = 0.5 + System.Math.Cos(d) * r2 * (i >= 60 ? -1 : 1 );
                    line.Y2 = 0.5 + System.Math.Sin(d) * r2 * (i >= 60 ? -1 : 1);
                    //System.Diagnostics.Debug.WriteLine(String.Format("From ({0}, {1}) to ({2}, {3})", line.X1, line.Y1, line.X2, line.Y2));
                }
            }
        }*/
        /*
        this.Canvas.Children.Clear();

        var n = 60;
        //var p = [];
        //var t = soundData.currentTime / soundData.duration; // percentage of time completed
        var r = 0.2 + 0.015 * rand.NextDouble() + 5;    // this is supposed to be bass (soundData.bass)
        var wr = r - 0.1;

        // draw blue wave bars
        var n2 = 128;   // 128 is 720 degree wrapping

        for (var i=0; i < n2; i++) 
        {
            var wave = rand.NextDouble(); // this is supposed to be sound intensity (soundData.waveDataL[(i/n2*256)>>0])
            var d = i/n * System.Math.PI*2 - System.Math.PI*0.5;
            var r2 = wr + wave * 0.05;
            Polygon poly = new Polygon();

            poly.Points.Add(new Point(0.5 + System.Math.Cos(d) * wr, 0.5 + System.Math.Sin(d) * wr));
            poly.Points.Add(new Point(0.5 + System.Math.Cos(d) * r2, 0.5 + System.Math.Sin(d) * r2));

            poly.StrokeThickness = 2;
            poly.Fill = Brushes.Red;
            poly.Stretch = Stretch.Fill;

            poly.Height = 100;
            poly.Width = 100;

            this.Canvas.Children.Add(poly);
        }*/
        /*
        foreach (UIElement uiele in this.Canvas.Children)
        {
            Line line = uiele as Line;

            if (line == null)
                continue;


        }
        */

        public static RoutedCommand SpeakPhraseCommand = new RoutedCommand();

        private void ExecutedSpeakPhraseCommand(object sender, ExecutedRoutedEventArgs e)
        {
            //MessageBox.Show("SpeakPhrase Command Executed");
            switch (rand.Next(1))
            {
                case 0:
                    synth.SetOutputToWaveStream(this.soundData);
                    synth.SpeakAsync("Place the heel of one of your hands on the patient’s chest, between the nipples. Place the heel of your other hand on top of the first hand. Position yourself directly over your hands, locking your arms at your elbows.");
                    synth.SetOutputToDefaultAudioDevice();
                    synth.SpeakAsync("Place the heel of one of your hands on the patient’s chest, between the nipples. Place the heel of your other hand on top of the first hand. Position yourself directly over your hands, locking your arms at your elbows.");
                    break;
                case 1: synth.SpeakAsync("Welcome back to aims. Verifying user identity..."); break;
                case 2: synth.SpeakAsync("With your left hand, pick up the laryngoscope - locking the blade into an open position."); break;
                case 3: synth.SpeakAsync("Goodbye."); break;
                case 4: synth.SpeakAsync("10, 9, 8, 7, 6, 5, 4, 3, 2, 1... Boom."); break;
            }
        }

        // CanExecuteRoutedEventHandler that only returns true if 
        // the source is a control. 
        private void CanExecuteSpeakPhraseCommand(object sender, CanExecuteRoutedEventArgs e)
        {
            Control target = e.Source as Control;

            if (target != null)
            {
                if (this.synth != null && this.synth.State != SynthesizerState.Speaking)
                    e.CanExecute = true;
            }
            else
            {
                e.CanExecute = false;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
