﻿/****************************************************************************
 * 
 *  Class: AccuracyMeter
 *  Author: Mitchel Weate
 *  
 *  Displays the accuracy passed in on a guage. Use setRightAccuracy and
 *  setLeft accuracy to adjust the angle of the needles. accuracy ranges from
 *  0.0 to 1.0, with 0.0 being in the red and 1.0 being in the green.  
 *   
 * ***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for AccuracyMeter.xaml
    /// </summary>
    public partial class AccuracyMeter : UserControl
    {
        private MatrixTransform mirrorTransform;
        private RotateTransform leftRotation;
        private RotateTransform rightRotation;

        public AccuracyMeter()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();
                
                this.Loaded += AccuracyMeter_Loaded;
            }
        }

        private void AccuracyMeter_Loaded(object sender, RoutedEventArgs e)
        {
            // Set up the rotation transforms for the left and right needles
            leftRotation = new RotateTransform();
            leftRotation.CenterX = LeftNeedle.ActualWidth * 0.841;
            leftRotation.CenterY = LeftNeedle.ActualHeight * 0.159;
            leftRotation.Angle = 0;

            rightRotation = new RotateTransform();
            rightRotation.CenterY = RightNeedle.ActualWidth * 0.159;
            rightRotation.CenterX = RightNeedle.ActualHeight * 0.159;
            rightRotation.Angle = 0;

            // Mirror meter image for the left meter
            TransformGroup mirrorGroup = new TransformGroup();
            mirrorGroup.Children.Add(new TranslateTransform(-(LeftMeter.ActualWidth), 0.0));
            mirrorGroup.Children.Add(new ScaleTransform(-1.0f, 1.0));
            mirrorTransform = new MatrixTransform(mirrorGroup.Value);

            // Assign Rendertransforms to children
            TransformGroup leftGroup = new TransformGroup();
            leftGroup.Children.Add(mirrorTransform);
            leftGroup.Children.Add(leftRotation);

            LeftMeter.RenderTransform = mirrorTransform;
            LeftNeedle.RenderTransform = leftGroup;
            RightNeedle.RenderTransform = rightRotation;

        }

        // Sets the angle of the right accuracy meter. accuracy should range from 0 to 1
        // where 0 will put the meter in the red and 1 will put the meter in the green.
        public void setRightAccuracy(double accuracy)
        {
            accuracy = System.Math.Max(accuracy, 0.0);
            accuracy = System.Math.Min(accuracy, 1.0);

            double angle = -90 + accuracy * 90;
            rightRotation.Angle = angle;
            RightShadow.Direction = 235 + angle;
        }

        // Sets the angle of the left accuracy meter. accuracy should range from 0 to 1
        // where 0 will put the meter in the red and 1 will put the meter in the green.
        public void setLeftAccuracy(double accuracy)
        {
            accuracy = System.Math.Max(accuracy, 0.0);
            accuracy = System.Math.Min(accuracy, 1.0);

            double angle = 90 - (accuracy * 90);
            leftRotation.Angle = angle;
            LeftShadow.Direction = 305 - angle;
        }

    }
}
