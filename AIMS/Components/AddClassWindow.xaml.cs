﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using Npgsql;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for AddClassWindow.xaml
    /// </summary>
    public partial class AddClassWindow : UserControl
    {
        private ObservableCollection<Email> emailCollection = new ObservableCollection<Email>();
        public ObservableCollection<Email> EmailCollection
        {
            get { return emailCollection; }
        }

        public AddClassWindow()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();

                EmailListView.ItemsSource = emailCollection;

                this.Loaded += AddClassWindow_Loaded;
                this.Unloaded += AddClassWindow_Unloaded;
            }
        }

        /// <summary>
        /// Event that fires every time the overlay gets loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddClassWindow_Loaded(object sender, EventArgs e)
        {
            
        }

        private void AddClassWindow_Unloaded(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Closes the overlay, clearing feilds and 
        /// </summary>
        public void CancelOverlay()
        {
           /* this.ResetInputFields();
            this.ErrorMessageTextBlock.Text = "";
            */
            this.IsEnabled = false;
            this.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Reveal the overlay, allowing the doctor to type a message to the toStudent about the relatedCourse.
        /// </summary>
        /// <param name="toStudent"></param>
        /// <param name="relatedCourse"></param>
        public void ShowOverlay()
        {
            /*this.ToStudent = toStudent;             // set the student
            this.RelatedCourse = relatedCourse;     // set the related course

            this.FromDoctor = ((App)App.Current).CurrentDoctor; // doctor is the one that's currently signed in*/

            this.Visibility = Visibility.Visible;   // show the overlay
            this.IsEnabled = true;                  // enable the overlay for mouse interaction
        }

        private void DateInput_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            int output;

            if (!Int32.TryParse(textBox.Text, out output))
            {
                textBox.Text = "";
            }
        }

        private void DateInput_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            int output;

            if (textBox.Text.Length == 0 || !Int32.TryParse(textBox.Text, out output))
            {
                if (textBox == StartDayInput || textBox == EndDayInput)
                    textBox.Text = "dd";
                else if (textBox == StartMonthInput || textBox == EndMonthInput)
                    textBox.Text = "mm";
                else if (textBox == StartYearInput || textBox == EndYearInput)
                    textBox.Text = "yyyy";
            }
        }

        private void AddEmail_Click(object sender, EventArgs e)
        {
            AddEmail( EmailInput.Text, true );
        }

        private void AddEmail( string emailstring, bool resetEmailInputFeild)
        {
            if (!String.IsNullOrWhiteSpace(emailstring))
            {
                if (IsValidEmail(emailstring))
                {
                    emailCollection.Add(new Email { address = emailstring });
                }
                else
                {
                    MessageBox.Show(String.Format("{0} is not a valid email address.", emailstring));
                }
            }

            if (resetEmailInputFeild)
            {
                EmailInput.Text = null;
                EmailInput.Focus();
            }
        }

        /// <summary>
        /// method for determining is the user provided a valid email address
        /// We use regular expressions in this check, as it is a more thorough
        /// way of checking the address provided
        /// </summary>
        /// <param name="email">email address to validate</param>
        /// <returns>true is valid, false if not valid</returns>
        public static bool IsValidEmail(string email)
        {
            //regular expression pattern for valid email
            //addresses, allows for the following domains:
            //com,edu,info,gov,int,mil,net,org,biz,name,museum,coop,aero,pro,tv
            string pattern = @"^[-a-zA-Z0-9][-.a-zA-Z0-9]*@[-.a-zA-Z0-9]+(\.[-.a-zA-Z0-9]+)*\.(com|edu|info|gov|int|mil|net|org|biz|name|museum|coop|aero|pro|tv|[a-zA-Z]{2})$";
            //Regular expression object
            Regex check = new Regex(pattern, RegexOptions.IgnorePatternWhitespace);
            //boolean variable to return to calling method
            bool valid = false;

            //make sure an email address was provided
            if (string.IsNullOrEmpty(email))
            {
                valid = false;
            }
            else
            {
                //use IsMatch to validate the address
                valid = check.IsMatch(email);
            }
            //return the value to the calling method
            return valid;
        }

        private void RemoveEmail_Click(object sender, EventArgs e)
        {
            List<Email> removeList = new List<Email>();

            foreach( Email email in EmailListView.SelectedItems )
            {
                removeList.Add(email);
            }

            foreach (Email email in removeList)
            {
                emailCollection.Remove(email);
            }
        }

        private Student FindStudentFromEmail(string email)
        {
            Student student = null;

            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "SELECT uid FROM accounts WHERE username = :username";
            command.Parameters.Add("username", NpgsqlTypes.NpgsqlDbType.Text).Value = email;

            int foundAcctID = (int)command.ExecuteScalar();
            command.Dispose();

            if (foundAcctID > 0)// Account found.
            {
                //Get the student.
                student = Student.GetStudentFromAccountID(foundAcctID, false);
            }

            DatabaseManager.CloseConnection();

            return student;
        }

        private void LoadEmail_Click(object sender, EventArgs e)
        {

        }

        private void Submit_Click(object sender, EventArgs e)
        {
            CreateNewClassWithWindowInputDetails();
        }

        public class Email
        {
            public string address { get; set; }

            public override string ToString()
            {
                return address;
            }
        }

        private DateTime GetStartDate()
        {
            DateTime startdate = DateTime.Now;



            return startdate;
        }

        private DateTime GetEndDate()
        {
            DateTime enddate = DateTime.MaxValue;



            return enddate;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            //Close the overlay
            CancelOverlay();
        }

        private void EmailInputTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Handled || e.IsRepeat)
                return;

            if (e.Key == Key.Enter)
            {
                AddEmail(EmailInput.Text, true);
                e.Handled = true;
            }
        }

        public void CreateNewClassWithWindowInputDetails()
        {
            if (VerifyWindowDetails())
            {
                Course course = null;
                Roster roster = null;
                List<Student> students = new List<Student>();

                //Create the class
                foreach (Email email in emailCollection)
                {
                    Student student = null;

                    DatabaseManager.OpenConnection();

                    NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "SELECT uid FROM accounts WHERE username = :username";
                    command.Parameters.Add("username", NpgsqlTypes.NpgsqlDbType.Text).Value = email;

                    int foundAcctID = -1;

                    try
                    {
                        foundAcctID = (int)command.ExecuteScalar();
                    }
                    catch
                    {

                    }

                    command.Dispose();

                    if (foundAcctID > 0)// Account found.
                    {
                        //Get the student.
                        student = Student.GetStudentFromAccountID(foundAcctID, false);
                    }
                    else
                    {
                        // Create an account for the user using the email address as their username, and store it's ID.
                        Account acct = Account.CreateAccount(email.address, email.address, AccessLevel.User, DateTime.Now, DateTime.MaxValue, true, email.address, email.address, email.address, ((App)App.Current).CurrentDoctor.Organization);

                        foundAcctID = acct.ID;
                    }

                    if (student == null)
                    {
                        // Create a new student, linked to their account.
                        student = Student.CreateStudentLinkedToAccount(foundAcctID);
                    }

                    if (student != null)
                    {
                        students.Add(student);
                    }

                    DatabaseManager.CloseConnection();
                }


                course = Course.CreateCourseNoRoster(((App)App.Current).CurrentDoctor.ID, GetStartDate(), GetEndDate(), ClassNameInput.Text, ClassDescritpionInput.Text, IntubationInput.IsChecked.Value, VerticalLiftInput.IsChecked.Value, CPRInput.IsChecked.Value);
                roster = Roster.CreateRosterLinkedToCourse(course.ID, students);

                course.Roster = roster;

                if (course != null && roster != null)
                {
                    this.CancelOverlay();

                    Page page = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

                    if (page as DoctorDashboard != null)
                    {
                        ((DoctorDashboard)page).PopulateCourseButtonList(); // refresh the button list with th new
                    }

                    //Course successfully created.
                    MessageBox.Show("The course has been created successfully.");
                }
                else
                {
                    MessageBox.Show("Your course was not created successfully.");
                }
            }
        }

        private bool VerifyWindowDetails()
        {
            if (String.IsNullOrWhiteSpace(this.ClassNameInput.Text))
                return false;
            if (String.IsNullOrWhiteSpace(this.ClassDescritpionInput.Text))
                return false;

            return true;
        }

        /// <summary>
        /// Walks up the object tree hierachy and get's the parent object.
        /// </summary>
        /// <param name="startObject"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private DependencyObject GetDependencyObjectFromVisualTree(DependencyObject startObject, Type type)
        {
            DependencyObject parent = startObject;

            while (parent != null)
            {
                if (type.IsInstanceOfType(parent))
                    break;

                else
                    parent = VisualTreeHelper.GetParent(parent);
            }

            return parent;
        }
    }
}
