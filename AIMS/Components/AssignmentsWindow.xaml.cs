﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Npgsql;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for AssignmentsWindow.xaml
    /// </summary>
    public partial class AssignmentsWindow : UserControl
    {
        private Course course = null;
        private List<Assignment> assignments = new List<Assignment>();
        private int currentAssignmentIndex = 0;

        public AssignmentsWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Converts the index passed into a stage score object within the current result.
        /// </summary>
        /// <param name="index"></param>
        public void ShowAssignmentInformation(int index)
        {
            if (assignments == null || assignments.Count == 0)
            {
                //MessageBox.Show("There are no assignments to display.");
                return;
            }
            if (index < 0 || index >= assignments.Count)
            {
                //MessageBox.Show("The assignemnt index is invalid.");
                return;
            }

            try
            {
                this.currentAssignmentIndex = index;
                this.NewAssignmentNameTB.Text = assignments[index].Name;
                this.NewAssignmentDescriptionTB.Text = assignments[index].Description;
                this.NewAssignmentStartDateCalendar.SelectedDate = assignments[index].StartDate;
                this.NewAssignmentEndDateCalendar.SelectedDate = assignments[index].EndDate;
                SetTaskType(assignments[index].Task);
                SetTaskMode(assignments[index].AssignmentType);
            }
            catch
            {
            }
        }

        /// <summary>
        /// Closes the overlay, clearing feilds and 
        /// </summary>
        public void CancelOverlay()
        {
            this.ResetInputFields();
            this.CreateAssignmentSubmitButton.IsEnabled = false;
            this.IsEnabled = false;
            this.Visibility = Visibility.Hidden;
        }

        private void ResetInputFields()
        {
            this.course = null;
            this.NewAssignmentNameTB.Text = "";
            this.NewAssignmentDescriptionTB.Text = "";
            this.AssignmentTaskComboBox.SelectedValue = null;
            this.AssignmentTaskModeComboBox.SelectedValue = null;
            this.NewAssignmentStartDateCalendar.SelectedDate = null;
            this.NewAssignmentEndDateCalendar.SelectedDate = null;
            this.ErrorMessageTextBlock.Text = "";
        }

        /// <summary>
        /// Reveal the overlay, allowing the doctor to type a message to the toStudent about the relatedCourse.
        /// </summary>
        /// <param name="toStudent"></param>
        /// <param name="relatedCourse"></param>
        public void ShowOverlay()
        {
            this.ResetInputFields();
            this.course = this.GetCurrentlySelectedCourse();

            PopulateAssignmentsButtonList();

            this.CreateAssignmentSubmitButton.IsEnabled = true;

            if (course == null)
            {
                MessageBox.Show("An error has occurred on the page. The pertaining course could not be ascertained.");
                return;
            }

            currentAssignmentIndex = 0;

            ShowAssignmentInformation(currentAssignmentIndex);

            this.Visibility = Visibility.Visible;   // show the overlay
            this.IsEnabled = true;                  // enable the overlay for mouse interaction
        }

        private void UpdateAssignmentsList()
        {
            if (course != null)
            {
                assignments = Assignment.GetAssignmentsForCourseID(course.ID);
            }
        }

        private void CreateAssignmentSubmitButton_Clicked(object sender, RoutedEventArgs e)
        {
            GenerateAssignmentFromInputFeilds();
        }

        /// <summary>
        /// Fills the button list with buttons representing each StageScore object from the Result object
        /// passed in as the parameter. If existing buttons exist, they will first be cleared out.
        /// </summary>
        /// <param name="result"></param>
        public void PopulateAssignmentsButtonList()
        {
            UpdateAssignmentsList();

            // Clear out old buttons before re-populating StageScore button list.
            AssignmentsButtonsPanel.Items.Clear();

            // Repopulate the results stage button list from the passed in result.
            for (int i = 0; i < assignments.Count; i++)
            {
                System.Windows.Controls.Button bttn = new Button();
                bttn.Uid = i.ToString();
                bttn.Content = assignments[i].Name;
                AssignmentsButtonsPanel.Items.Add(bttn);
            }
        }

        private void GenerateAssignmentFromInputFeilds()
        {
            CreateAssignmentSubmitButton.IsEnabled = false;

            ErrorMessageTextBlock.Text = "Verifying User Input...";

            if (String.IsNullOrWhiteSpace(this.NewAssignmentNameTB.Text))
            {
                ErrorMessageTextBlock.Text = "* Include an assignment name.";
                MessageBox.Show("You must include an assignment name.");
                CreateAssignmentSubmitButton.IsEnabled = false;
                return;
            }
            else if (String.IsNullOrWhiteSpace(this.NewAssignmentDescriptionTB.Text))
            {
                ErrorMessageTextBlock.Text = "* Include an assignment description.";
                MessageBox.Show("You must include an assignment description.");
                CreateAssignmentSubmitButton.IsEnabled = false;
                return;
            }
            else if (this.NewAssignmentStartDateCalendar.SelectedDate == null)
            {
                ErrorMessageTextBlock.Text = "* You must specify a start date for the assignment.";
                MessageBox.Show("You must include a starting date.");
                CreateAssignmentSubmitButton.IsEnabled = false;
                return;
            }
            else if (this.NewAssignmentEndDateCalendar.SelectedDate == null )
            {
                ErrorMessageTextBlock.Text = "* You must specify a start date for the assignment.";
                MessageBox.Show("You must include an ending date.");
                CreateAssignmentSubmitButton.IsEnabled = false;
                return;
            }
            else if (this.NewAssignmentEndDateCalendar.SelectedDate < this.NewAssignmentStartDateCalendar.SelectedDate)
            {
                ErrorMessageTextBlock.Text = "* The end date for the assignment cannot be before the start date.";
                MessageBox.Show("The ending date must be equal to or greater than the starting date.");
                CreateAssignmentSubmitButton.IsEnabled = false;
                return;
            }

            ErrorMessageTextBlock.Text = "Input verified successfully.";

            ErrorMessageTextBlock.Text = "Creating course...";

            ErrorMessageTextBlock.Text = "Establishing connection";
            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "INSERT INTO course_assignments ( courseid, tasktype, taskmode, name, description, startdate, duedate ) VALUES ( :courseid, :tasktype, :taskmode, :name, :description, :startdate, :duedate ) RETURNING uid;";
            command.Parameters.Add("courseid", NpgsqlTypes.NpgsqlDbType.Integer).Value = course.ID;
            command.Parameters.Add("tasktype", NpgsqlTypes.NpgsqlDbType.Integer).Value = GetTaskType();
            command.Parameters.Add("taskmode", NpgsqlTypes.NpgsqlDbType.Integer).Value = GetTaskMode();
            command.Parameters.Add("name", NpgsqlTypes.NpgsqlDbType.Text).Value = NewAssignmentNameTB.Text;
            command.Parameters.Add("description", NpgsqlTypes.NpgsqlDbType.Text).Value = NewAssignmentDescriptionTB.Text;
            command.Parameters.Add("startdate", NpgsqlTypes.NpgsqlDbType.Timestamp).Value = NewAssignmentStartDateCalendar.SelectedDate;
            command.Parameters.Add("duedate", NpgsqlTypes.NpgsqlDbType.Timestamp).Value = NewAssignmentEndDateCalendar.SelectedDate;
            
            int id = -1;

            try
            {
                id = (int)command.ExecuteScalar();
            }
            catch (Exception except)
            {
                MessageBox.Show(except.ToString());
            }

            ErrorMessageTextBlock.Text = "Closing connection";
            DatabaseManager.CloseConnection();

            if (id != -1)
            {
                Console.WriteLine("Assignment created successfully.");
                ErrorMessageTextBlock.Text = "Assignment created successfully.";
                CreateAssignmentSubmitButton.IsEnabled = false;
                this.CancelOverlay();
            }
            else
            {
                Console.WriteLine("Assignment creation failed.");
                ErrorMessageTextBlock.Text = "Assignment creation failed.";
                CreateAssignmentSubmitButton.IsEnabled = true;
            }
        }

        private int GetTaskType()
        {
            if (AssignmentTaskComboBox.SelectedIndex == 0)
            {
                return (int)Task.Intubation;
            }
            else if (AssignmentTaskComboBox.SelectedIndex == 1)
            {
                return (int)Task.VerticalLift;
            }
            else if (AssignmentTaskComboBox.SelectedIndex == 2)
            {
                return (int)Task.CPR;
            }

            return 0;
        }

        private void SetTaskType( Task task )
        {
            switch (task)
            {
                case Task.Intubation: this.AssignmentTaskComboBox.SelectedIndex = 0; break;
                case Task.VerticalLift: this.AssignmentTaskComboBox.SelectedIndex = 1; break;
                case Task.CPR: this.AssignmentTaskComboBox.SelectedIndex = 2; break;
                default: this.AssignmentTaskComboBox.SelectedItem = null; break;
            }
        }

        private void SetTaskMode(AssignmentType type)
        {
            switch (type)
            {
                case AssignmentType.None: this.AssignmentTaskModeComboBox.SelectedIndex = 0; break;
                case AssignmentType.Practice: this.AssignmentTaskModeComboBox.SelectedIndex = 1; break;
                case AssignmentType.Assessment: this.AssignmentTaskModeComboBox.SelectedIndex = 2; break;
                default: this.AssignmentTaskModeComboBox.SelectedItem = null; break;
            }
        }

        private int GetTaskMode()
        {
            if (AssignmentTaskModeComboBox.SelectedIndex == 0)
            {
                return (int)AssignmentType.None;
            }
            if (AssignmentTaskModeComboBox.SelectedIndex == 1)
            {
                return (int)AssignmentType.Practice;
            }
            else if (AssignmentTaskModeComboBox.SelectedIndex == 2)
            {
                return (int)AssignmentType.Assessment;
            }

            return 0;
        }

        private Course GetCurrentlySelectedCourse()
        {
            Page page = this.GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

            Course course = null;

            if (page as DoctorDashboard != null)
            {
                DoctorDashboard dd = (DoctorDashboard)page;

                if (dd.CourseList != null && dd.SelectedCourseIndex >= 0 && dd.SelectedCourseIndex < dd.CourseList.Count)
                {
                    course = dd.CourseList[dd.SelectedCourseIndex];
                }
            }

            return course;
        }

        private void CreateAssignmentCancelButton_Clicked(object sender, RoutedEventArgs e)
        {
            this.CancelOverlay();
        }

        private void AssignmentListButton_Clicked(object sender, RoutedEventArgs e)
        {
            Button bttn = e.Source as Button;

            if (bttn != null)
            {
                this.ShowAssignmentInformation( this.AssignmentsButtonsPanel.Items.IndexOf(bttn) );
            }

            e.Handled = true;
        }

        /// <summary>
        /// Walks up the object tree hierachy and get's the parent object.
        /// </summary>
        /// <param name="startObject"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private DependencyObject GetDependencyObjectFromVisualTree(DependencyObject startObject, Type type)
        {
            DependencyObject parent = startObject;

            while (parent != null)
            {
                if (type.IsInstanceOfType(parent))
                    break;
                else
                    parent = VisualTreeHelper.GetParent(parent);
            }

            return parent;
        }
    }
}
