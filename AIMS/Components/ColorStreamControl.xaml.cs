﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for ColorStreamControl.xaml
    /// </summary>
    public partial class ColorStreamControl : UserControl
    {
        /// <summary>
        /// Shows the property controls when set to True. Only shows the ColorStream image otherwise.
        /// </summary>
        public bool ShowSettings 
        { 
            get { return (bool)GetValue(ShowSettingsProperty); }
            set 
            { 
                var oldValue = (bool)GetValue(ShowSettingsProperty);
                if (oldValue != value) SetValue(ShowSettingsProperty, value);
            }
        }

        public static readonly DependencyProperty ShowSettingsProperty = DependencyProperty.Register("ShowSettings",
            typeof(bool), typeof(ColorStreamControl), new PropertyMetadata() { DefaultValue = false });

        public double ImageOpacity
        {
            get { return (double)GetValue(ImageOpacityProperty); }
            set
            {
                var oldValue = (double)GetValue(ImageOpacityProperty);
                if (oldValue != value) SetValue(ImageOpacityProperty, value);
            }
        }

        public static readonly DependencyProperty ImageOpacityProperty = DependencyProperty.Register("ImageOpacity",
            typeof(double), typeof(ColorStreamControl), new PropertyMetadata() { DefaultValue = 1.00 });

        public ColorStreamControl()
        {
            InitializeComponent();
        }
    }
}
