﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for ColorTrackerOverlay.xaml
    /// </summary>
    public partial class ColorTrackerOverlay : UserControl
    {
        public static readonly DependencyProperty ShowCrosshairProperty =
            DependencyProperty.Register("ShowCrosshair", typeof(bool), typeof(ColorTrackerOverlay), new PropertyMetadata(false, OverlaySettingChanged));

        public static readonly DependencyProperty ShowScanProperty =
            DependencyProperty.Register("ShowScan", typeof(bool), typeof(ColorTrackerOverlay), new PropertyMetadata(false, OverlaySettingChanged));

        public bool ShowCrosshair
        {
            get { return (bool)GetValue(ShowCrosshairProperty); }
            set { SetValue(ShowCrosshairProperty, value); }
        }
        public bool ShowScan
        { 
            get { return (bool)GetValue(ShowScanProperty); }
            set { SetValue(ShowScanProperty, value); }
        }

        private WriteableBitmap bitmap = null;

        private List<WriteableBitmap> bitmaps = new List<WriteableBitmap>();

        private Line Line1 = new Line();
        private Line Line2 = new Line();

        public ColorTrackerOverlay()
        {
            InitializeComponent();

            Loaded += ColorTrackerOverlay_Loaded;
            Unloaded += ColorTrackerOverlay_Unloaded;
        }

        private void ColorTrackerOverlay_Loaded(object sender, EventArgs e)
        {
            KinectManager.ColorDataReady += ColorTrackerOverlay_ColorDataReady;
        }

        private void ColorTrackerOverlay_Unloaded( object sender, EventArgs e )
        {
            KinectManager.ColorDataReady -= ColorTrackerOverlay_ColorDataReady;
        }

        private List<Rectangle> rectangles = new List<Rectangle>();

        public void UpdateOverlay()
        {
            if (this.IsVisible)
            {
                int width = 1920;
                int height = 1080;

                OverlayCanvas.Children.Clear();

                if (this.ShowScan)
                {
                    //AIMS.Assets.Lessons2.ColorTracker.ImageData imageData = AIMS.Assets.Lessons2.ColorTracker.Image;
                    for (int i = AIMS.Assets.Lessons2.ColorTracker.Images.Count - 1; i >= 0; i--)
                    {
                        AIMS.Assets.Lessons2.ImageData imageData = AIMS.Assets.Lessons2.ColorTracker.Images[i];

                        if (imageData == null)
                            continue;

                        Rectangle rect = new Rectangle();
                        rect.Width = AIMS.Assets.Lessons2.ColorTracker.Images[i].width * this.ActualWidth / width;
                        rect.Height = AIMS.Assets.Lessons2.ColorTracker.Images[i].height * this.ActualHeight / height;
                        rect.Visibility = System.Windows.Visibility.Visible;

                        WriteableBitmap bitmap = new WriteableBitmap(imageData.width, imageData.height, 96, 96, PixelFormats.Bgra32, null);

                        rect.Fill = new ImageBrush(bitmap);

                        // Write the imageData to the bitmap
                        bitmap.WritePixels(new Int32Rect(0, 0, imageData.width, imageData.height), imageData.data, imageData.width * 4, 0);

                        OverlayCanvas.Children.Add(rect);

                        // Set up the color overlay
                        Canvas.SetLeft(rect, imageData.x * this.ActualWidth / width);
                        Canvas.SetTop(rect, imageData.y * this.ActualHeight / height);
                    }
                }

                if ((!ShowScan || ShowCrosshair) && ( !double.IsNaN(AIMS.Assets.Lessons2.ColorTracker.FoundPoint.X) && !double.IsNaN(AIMS.Assets.Lessons2.ColorTracker.FoundPoint.Y) ))
                {
                    double scaledCrosshairLength = 6 * this.ActualWidth / width;

                    Line1.Visibility = Visibility.Visible;
                    Line2.Visibility = Visibility.Visible;

                    Line1.X1 = AIMS.Assets.Lessons2.ColorTracker.FoundPoint.X * this.ActualWidth / width - scaledCrosshairLength;
                    Line1.X2 = AIMS.Assets.Lessons2.ColorTracker.FoundPoint.X * this.ActualWidth / width + scaledCrosshairLength;
                    Line1.Y1 = AIMS.Assets.Lessons2.ColorTracker.FoundPoint.Y * this.ActualHeight / height - scaledCrosshairLength;
                    Line1.Y2 = AIMS.Assets.Lessons2.ColorTracker.FoundPoint.Y * this.ActualHeight / height + scaledCrosshairLength;

                    Line2.X1 = AIMS.Assets.Lessons2.ColorTracker.FoundPoint.X * this.ActualWidth / width - scaledCrosshairLength;
                    Line2.X2 = AIMS.Assets.Lessons2.ColorTracker.FoundPoint.X * this.ActualWidth / width + scaledCrosshairLength;
                    Line2.Y1 = AIMS.Assets.Lessons2.ColorTracker.FoundPoint.Y * this.ActualHeight / height + scaledCrosshairLength;
                    Line2.Y2 = AIMS.Assets.Lessons2.ColorTracker.FoundPoint.Y * this.ActualHeight / height - scaledCrosshairLength;

                    OverlayCanvas.Children.Add(Line1);
                    OverlayCanvas.Children.Add(Line2);
                }
                else
                {
                    Line1.Visibility = Visibility.Hidden;
                    Line2.Visibility = Visibility.Hidden;
                }
            }
        }

        private void ColorTrackerOverlay_ColorDataReady()
        {
            UpdateOverlay();
        }

        private static void OverlaySettingChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            ColorTrackerOverlay overlay = sender as ColorTrackerOverlay;

            if (overlay != null)
            {
                overlay.UpdateOverlay();
            }

        }
    }
}
