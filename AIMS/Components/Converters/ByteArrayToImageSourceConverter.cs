﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace AIMS.Components.Converters
{
    public class ByteArrayToImageSourceConverter : MarkupExtension, IValueConverter
    {
        int width = 1920;
        int height = 1080;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            byte[] byteArrayImage = value as byte[];

            WriteableBitmap bitmap = new WriteableBitmap(width, height, 96, 96, PixelFormats.Bgr32, null);

            if (byteArrayImage != null && byteArrayImage.Length > 0)
            {
                bitmap.WritePixels(new System.Windows.Int32Rect(0, 0, width, height), byteArrayImage, width * 4, 0);
            }

            return bitmap;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }


        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
