﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Markup;

namespace AIMS.Components.Converters
{
    /// <summary>
    /// Converts a score value (double) into a reflection of whether or not the score passed by exceding the PassValue (bool).
    /// </summary>
    public class DoubleToBoolPassConverter : MarkupExtension, IValueConverter
    {
        public DoubleToBoolPassConverter()
        {
        }
        /// <summary>
        /// Minimum pass value. (ie. PassValue = 0.7 would result in 0.69 = false and 0.70 = true)
        /// </summary>
        public double PassValue { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double val = System.Convert.ToDouble(value);

            return val >= PassValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // left as an exercise for the reader
            // don't be a wimp, it's not hard
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
