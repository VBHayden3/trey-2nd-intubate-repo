﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Media;
using System.Globalization;
using System.Windows;

namespace AIMS.Components
{
    public class DoubleToColorConverter :IValueConverter
    {
        public object Convert(object obj, Type targetType, object parameter, CultureInfo culture)
        {
            double value = (double)obj;
            string filePath = string.Empty;

            if (value >= 0.70d)
                filePath = "/Assets/Graphics/AreaGraph_Green.png";
            else
                filePath = "/Assets/Graphics/AreaGraph_Red.png";

            return filePath;
        }

        /*
        public object Convert(object obj, Type targetType, object parameter, CultureInfo culture)
        {
            double value = (double)obj;
            string filePath = "";
            //double value = (double)((DataPoint)obj).IndependentValue;
            //Brush foreground = Brushes.Green;

            //if (value >= 70d)
            //{
            //    foreground = Brushes.Green;
            //}
            //else
            //{
            //    foreground = Brushes.Red;
            //}

            //LinearGradientBrush brush = new LinearGradientBrush();
            //brush.StartPoint = new Point(0, 1);
            //brush.EndPoint = new Point(0, 0);

            if (value >= 70d)
            {
                filePath = "/Assets/Graphics/AreaGraph_Green.png";
            //    //bottom Gradient
            //    brush.GradientStops.Add(new GradientStop()
            //    {
            //        //Color = Colors.White,
            //        Color = Color.FromArgb(255, 0, 150, 0),
            //        //Color = Color.FromArgb(255, 0, 220, 0),//light green
            //        Offset = 0
            //    });
            //    //Top Gradient
            //    brush.GradientStops.Add(new GradientStop()
            //    {
            //        Color = Colors.White,
            //        //Color = Color.FromArgb(255, 0, 150, 0),
            //        //Color = Color.FromArgb(255, 97, 0, 0),//dark green
            //        Offset = 1
            //    });
            }

            else
            {
                filePath = "/Assets/Graphics/AreaGraph_Red.png";
            //    //bottom Gradient
            //    brush.GradientStops.Add(new GradientStop()
            //    {
            //        //Color = Colors.White,
            //        Color = Color.FromArgb(255, 150, 0, 0),
            //        //Color = Color.FromArgb(255, 220, 0, 0),//light red
            //        Offset = 0
            //    });
            //    //Top Gradient
            //    brush.GradientStops.Add(new GradientStop()
            //    {
            //        Color = Colors.White,
            //        //Color = Color.FromArgb(255, 150, 0, 0),
            //        //Color = Color.FromArgb(255, 97, 0, 0),//dark red
            //        Offset = 1
            //    });
            }

            return filePath;
        }
        */

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
