﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Data;
using System.Globalization;

namespace AIMS.Components.Converters
{
    public class HSVBrushConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.All(v => v is byte))
            {
                //Do regular computation
                return new SolidColorBrush(Color.FromArgb(255, (byte)values[0], (byte)values[1], (byte)values[2]));
            }
            else
            {
                //Handle edge case
                return null;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
