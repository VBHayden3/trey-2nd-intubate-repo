﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Markup;

namespace AIMS.Components.Converters
{
    public class MultiplyConverter : MarkupExtension, IValueConverter
    {
        public double Factor { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double val = System.Convert.ToDouble(value);
            return val * Factor;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // left as an exercise for the reader
            // don't be a wimp, it's not hard
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }


    public class ResultIndexConverter : IMultiValueConverter
    {

        public object Convert(object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] == null || values[1] == null)
                return null;

            ObservableRangeCollection<AIMS.Tasks.TaskResult> tasks = (ObservableRangeCollection<AIMS.Tasks.TaskResult>)values[0];
            string uid = values[1].ToString();
            int idx = 1;

            foreach (AIMS.Tasks.TaskResult task in tasks)
            {
                if (task.UID.ToString() == uid)
                {
                    return idx.ToString();
                }

                idx++;
            }

            return null;
        }


        /*
        public object Convert(object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] == null || values[1] == null)
                return null;

            IEnumerable<object> ienum = (IEnumerable<object>)values[0];
            int idx = 1;

            foreach (object item in ienum)
            {
                if (object.ReferenceEquals(item, values[1]))
                {
                    return idx.ToString();
                }

                idx++;
            }

            return null;
        }
        */


        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }

}
