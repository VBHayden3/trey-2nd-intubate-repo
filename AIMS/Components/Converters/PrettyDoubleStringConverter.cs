﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;

namespace AIMS.Components.Converters
{
    [ValueConversion(typeof(double), typeof(string))]
    class PrettyDoubleStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.GetType() != typeof(double))
                return null;

            if (parameter != null)
            {
                if (parameter.GetType() == typeof(string))
                {
                    int cnt;
                    if (Int32.TryParse((string)parameter, out cnt))
                    {
                        string precision = ".";
                        for (int i = 0; i < cnt; i++)
                            precision += "#";

                        return String.Format("{0:0" + precision + "}", (double)value);
                    }
                }
            }
            
            return String.Format("{0:0}", (double)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.GetType() != typeof(string))
                return null;

            else
            {
                double result;
                if (Double.TryParse((string)value, out result))
                    return result;
                else
                    return null;
            }
        }
    }
}
