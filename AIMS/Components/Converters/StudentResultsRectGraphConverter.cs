﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Media;
using System.Globalization;
using System.Windows;

namespace AIMS.Components
{
    public class StudentResultsRectGraphConverter : IValueConverter
    {
        public object Convert(object obj, Type targetType, object parameter, CultureInfo culture)
        {
            double value = (double)obj;
            //string filePath = "";
            
            //Brush foreground = Brushes.Green;

            //if (value >= 70d)
            //{
            //    foreground = Brushes.Green;
            //}
            //else
            //{
            //    foreground = Brushes.Red;
            //}

            LinearGradientBrush brush = new LinearGradientBrush();
            brush.StartPoint = new Point(0.5, 0);
            brush.EndPoint = new Point(0.5, 1);

            if (value >= 70)    // GREEN
            {
                //bottom Gradient
                brush.GradientStops.Add(new GradientStop()
                {
                    Color = Color.FromArgb(255, 255, 255, 255),//white
                    Offset = -0.1
                });
                //bottom Gradient
                brush.GradientStops.Add(new GradientStop()
                {
                    Color = Color.FromArgb(255, 0, 220, 0),//light green
                    Offset = System.Math.Min( System.Math.Max( 0.7 - ( (100-value)/100 * 2.142857 ), 0.25), 0.75 )
                });/*
                //Top Gradient
                brush.GradientStops.Add(new GradientStop()
                {
                    Color = Color.FromArgb(255, 0, 97, 0),//dark green
                    Offset = 0.9
                });*/
                //Top Gradient
                brush.GradientStops.Add(new GradientStop()
                {
                    Color = Color.FromArgb(255, 0, 0, 0),//black
                    Offset = 1.5
                });
            }
                /*
            else if (value >= 75)    // Yellow
            {

                //bottom Gradient
                brush.GradientStops.Add(new GradientStop()
                {
                    Color = Color.FromArgb(255, 255, 255, 130),//light yellow
                    Offset = 0
                });
                //Top Gradient
                brush.GradientStops.Add(new GradientStop()
                {
                    Color = Color.FromArgb(255, 255, 255, 0),//dark yellow
                    Offset = 1
                });
            }
            else if (value >= 85)    // ORANGE
            {

                //bottom Gradient
                brush.GradientStops.Add(new GradientStop()
                {
                    Color = Color.FromArgb(255, 255, 160, 60),//light orange
                    Offset = 0
                });
                //Top Gradient
                brush.GradientStops.Add(new GradientStop()
                {
                    Color = Color.FromArgb(255, 255, 128, 0),//dark orange
                    Offset = 1
                });
            }*/

            else // RED
            {
                //bottom Gradient
                brush.GradientStops.Add(new GradientStop()
                {
                    Color = Color.FromArgb(255, 255, 255, 255),//white
                    Offset = -0.1
                });
                //bottom Gradient
                brush.GradientStops.Add(new GradientStop()
                {
                    Color = Color.FromArgb(255, 220, 0, 0),//light red
                    Offset = 0.7
                });/*
                //Top Gradient
                brush.GradientStops.Add(new GradientStop()
                {
                    Color = Color.FromArgb(255, 97, 0, 0),//dark red
                    Offset = 0.9
                });*/

                //Top Gradient
                brush.GradientStops.Add(new GradientStop()
                {
                    Color = Color.FromArgb(255, 0, 0, 0),//black
                    Offset = 1.5
                });
            }




            return brush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}

