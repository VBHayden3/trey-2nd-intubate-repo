﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Diagnostics;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class CountdownTimer : UserControl
    {
        public delegate void CountdownCompleteEventHandler(object sender);
        public event CountdownCompleteEventHandler CountdownComplete;
        int secondsRemaining = 3;

        DispatcherTimer dt = new DispatcherTimer();

        public void startCountdown()
        {
            textblock.Text = "" + secondsRemaining;
            dt.Start();
        }

        private void dtTick(object sender, EventArgs e)
        {
            secondsRemaining--;
            textblock.Text = "" + secondsRemaining;
            if (secondsRemaining < 0)
            {
                dt.Stop();
                textblock.Text = "";
                secondsRemaining = 3;
                if (CountdownComplete != null)
                {
                    CountdownComplete(this);
                }    
            }
        }
       
        public CountdownTimer()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();
            
                dt.Interval = new TimeSpan(0, 0, 0, 1, 0);
                dt.Tick += dtTick;
            }
        }

        
    }
}
