﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for DateTimeNowControl.xaml
    /// </summary>
    public partial class DateTimeNowControl : UserControl, INotifyPropertyChanged
    {
        private DispatcherTimer timer;

        public DateTime CurrentTime
        {
            get { return DateTime.Now; }
        }

        public DateTimeNowControl()
        {
            InitializeComponent();

            this.DataContext = this;

            Loaded += DateTimeNowControl_Loaded;
            Unloaded += DateTimeNowControl_Unloaded;

            timer = new DispatcherTimer();

            timer.Tick += timer_Tick;

            timer.Interval = TimeSpan.FromSeconds(1);
        }

        void timer_Tick(object sender, EventArgs e)
        {
            // Notify the property changed to trigger update.
            NotifyPropertyChanged("CurrentTime");

            this.timer.Start();
        }

        void DateTimeNowControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.timer.Start();
        }

        void DateTimeNowControl_Unloaded(object sender, RoutedEventArgs e)
        {
            this.timer.Stop();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
