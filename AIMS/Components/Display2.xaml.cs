﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for Display2.xaml
    /// </summary>
    public partial class Display2 : UserControl
    {
        private const int width = 1920;
        private const int height = 1080;

        private WriteableBitmap bitmap = null;
        private WriteableBitmap mappedBitmap = null;
        private uint bytesPerPixel;

        public static readonly DependencyProperty DisplayModeProperty =
            DependencyProperty.Register("DisplayMode", typeof(DisplayModes), typeof(Display2), new PropertyMetadata(DisplayModes.Color));

        public DisplayModes DisplayMode
        {
            get { return (DisplayModes)GetValue(DisplayModeProperty); }
            set { SetValue(DisplayModeProperty, value); }
        }

        public enum DisplayModes { Color, MappedColor, HSV };

        // Counter used to skip frames when in HSV mode
        int counter = 0;

        public Display2()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();

                Loaded += Display2Loaded;
                Unloaded += Display2Unloaded;

                this.ColorBorder.Source = bitmap;
            }
        }

        private void Display2Loaded(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Display2 -> Display2Loaded");
            KinectManager.ColorDataReady += ColorUpdated;
            KinectManager.AllDataReady += AllDataUpdated;
        }


        private void Display2Unloaded(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Display2 -> Display2Unloaded");
            KinectManager.ColorDataReady -= ColorUpdated;
            KinectManager.AllDataReady -= AllDataUpdated;
        }

        private void ColorUpdated()
        {
            if (DisplayMode == DisplayModes.Color)
            {
                int width = KinectManager.ColorWidth;
                int height = KinectManager.ColorHeight;

                if (this.bitmap == null)
                {
                    this.bitmap = new WriteableBitmap(width, height, 96, 96, PixelFormats.Bgr32, null);
                }
//                if (counter > 2)
                {
                    counter = 0;
                    bitmap.WritePixels(new Int32Rect(0, 0, width, height), KinectManager.ColorData, width * 4, 0);
                    this.ColorBorder.Source = bitmap;
                }
                counter++;
            }
        }

        private void AllDataUpdated()
        {
            int width = KinectManager.DepthWidth;
            int height = KinectManager.DepthHeight;

            if (this.mappedBitmap == null)
            {
                this.mappedBitmap = new WriteableBitmap(width, height, 96, 96, PixelFormats.Bgr32, null);
            }

//            if (counter > 2)
            {
                counter = 0;
                if (DisplayMode == DisplayModes.MappedColor)
                {
                    mappedBitmap.WritePixels(new Int32Rect(0, 0, width, height), KinectManager.MappedColorData, width * 4, 0);
                    this.ColorBorder.Source = mappedBitmap;
                }
                else if (DisplayMode == DisplayModes.HSV)
                {                                      
                    mappedBitmap.WritePixels(new Int32Rect(0, 0, width, height), AIMS.Assets.Lessons2.ColorTracker.ToHCY(KinectManager.MappedColorData), width * 4, 0);
                    this.ColorBorder.Source = mappedBitmap;
                }
            }
            counter++;
        }

        private void display_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            Point p = e.GetPosition( sender as UIElement );

            System.Diagnostics.Debug.WriteLine(String.Format("Display2 -> Point Clicked on Display2: ({0},{1})", p.X, p.Y));
            switch( this.DisplayMode )
            {
                case Display2.DisplayModes.Color:
                {
                    //int _bytePerPixel = 4;
                    int _bytePerPixel = (PixelFormats.Bgr32.BitsPerPixel + 7) / 8;
                    int clickedIndex = ((int)p.Y * 1920 + (int)p.X) * _bytePerPixel;

                    if (clickedIndex >= KinectManager.ColorData.Length)
                    {
                        System.Diagnostics.Debug.WriteLine("Display2 -> clickedIndex larger than " + KinectManager.ColorData.Length);
                        return;
                    }

                    int r = KinectManager.ColorData[clickedIndex];
                    int g = KinectManager.ColorData[clickedIndex + 1];
                    int b = KinectManager.ColorData[clickedIndex + 2];
                    double h = b/255.0;
                    double s = g / 255.0;
                    double v = r / 255.0;

                    System.Diagnostics.Debug.WriteLine(String.Format("Display2 -> R:{0:0.00} G:{1:0.00} B:{2:0.00}; H:{3:0.00} S:{4:0.00} V:{5:0.00}", r, g, b, h, s, v));

                    break;
                }
            }
            System.Diagnostics.Debug.WriteLine(String.Format("Display2 -> XY: ({0:0},{1:0})", p.X, p.Y));
        }

        private void display_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            Point p = e.GetPosition(sender as UIElement);

            double x, y, z;
            int r, g, b;
            double h, s, v;

            string output = String.Format("X:{0:0} Y:{1:0}", p.X, p.Y);

            switch (this.DisplayMode)
            {
                case Display2.DisplayModes.Color:
                {
                    if (KinectManager.ColorData != null)
                    {
                        //960x540 convert to 1920x1080
                        int xValue = (int)p.X * 2;
                        int yValue = (int)p.Y * 2;

                        int _bytePerPixel = (PixelFormats.Bgr32.BitsPerPixel + 7) / 8;
                        int clickedIndex = (yValue * 1920 + xValue) * _bytePerPixel;
                        if (clickedIndex >= KinectManager.ColorData.Length)
                        {
                            System.Diagnostics.Debug.WriteLine("Display2 -> color index " + clickedIndex + " is larger than " + KinectManager.ColorData.Length);
                            break;
                        }

                        b = KinectManager.ColorData[clickedIndex];
                        g = KinectManager.ColorData[clickedIndex + 1];
                        r = KinectManager.ColorData[clickedIndex + 2];
                        h = b / 255.0;
                        s = g / 255.0;
                        v = r / 255.0;

                        output += String.Format("      R:{0} G:{1} B:{2}", r, g, b);
                        output += String.Format("      H:{0:0.00} S:{1:0.00} V:{2:0.00}", h, s, v);

                        // @note could show depth for color point too
                    }
                    break;
                }

                case Display2.DisplayModes.MappedColor:
                {
                    if (KinectManager.MappedColorData != null)
                    {
                        //869x720 convert to 512x424
                        //640x480 convert to 512x424
                        //int xValue = (int)(p.X * .58888);
                        //int yValue = (int)(p.Y * .58888);
                        int xValue = (int)(p.X * .58888);
                        int yValue = (int)(p.Y * .58888);

                        int _bytePerPixel = (PixelFormats.Bgr32.BitsPerPixel + 7) / 8;
                        int clickedIndex = (yValue * 512 + xValue) * _bytePerPixel;
                        if (clickedIndex >= KinectManager.MappedColorData.Length)
                        {
                            System.Diagnostics.Debug.WriteLine("Display2 -> mapped index " + clickedIndex + " is larger than " + KinectManager.MappedColorData.Length);
                            return;
                        }

                        b = KinectManager.MappedColorData[clickedIndex];
                        g = KinectManager.MappedColorData[clickedIndex + 1];
                        r = KinectManager.MappedColorData[clickedIndex + 2];
                        h = b / 255.0;
                        s = g / 255.0;
                        v = r / 255.0;

                        output += String.Format("      R:{0} G:{1} B:{2}", r, g, b);
                        output += String.Format("      H:{0:0.00} S:{1:0.00} V:{2:0.00}", h, s, v);

                        if (KinectManager.DepthFrameData != null)
                        {
                            double depth = 0;
                            try
                            {
                                depth = KinectManager.GetDepthValue(xValue, yValue);
                            }
                            catch (Exception ex)
                            {
                            }
                            output += String.Format("      Depth: {0} mm", depth);
                        }
                    }                    
                    break;
                }
                case Display2.DisplayModes.HSV:
                {
                    if (KinectManager.MappedColorData != null)
                    {
                        //869x720 convert to 512x424
                        int xValue = (int)(p.X * .58888);
                        int yValue = (int)(p.Y * .58888);

                        int _bytePerPixel = (PixelFormats.Bgr32.BitsPerPixel + 7) / 8;
                        int clickedIndex = (yValue * 512 + xValue) * _bytePerPixel;
                        if (clickedIndex >= KinectManager.MappedColorData.Length)
                        {
                            System.Diagnostics.Debug.WriteLine("Display2 -> mapped index " + clickedIndex + " is larger than " + KinectManager.MappedColorData.Length);
                            return;
                        }

                        b = KinectManager.MappedColorData[clickedIndex];
                        g = KinectManager.MappedColorData[clickedIndex + 1];
                        r = KinectManager.MappedColorData[clickedIndex + 2];
                        h = b / 255.0;
                        s = g / 255.0;
                        v = r / 255.0;

                        output += String.Format("      R:{0} G:{1} B:{2}", r, g, b);
                        output += String.Format("      H:{0:0.00} S:{1:0.00} V:{2:0.00}", h, s, v);

                        if (KinectManager.DepthFrameData != null)
                        {
                            double depth = 0;
                            try
                            {
                                depth = KinectManager.GetDepthValue(xValue, yValue);
                            }
                            catch (Exception ex)
                            {
                            }

                            output += String.Format("      Depth: {0} mm", depth);
                        }
                    }
                    break;
                }
            }

            DebugTB.Text = output;
        }
    }
}
