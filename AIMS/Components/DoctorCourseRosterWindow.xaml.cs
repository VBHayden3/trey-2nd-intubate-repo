﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Npgsql;
using System.Data;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for DoctorCourseRosterWindow.xaml
    /// </summary>
    public partial class DoctorCourseRosterWindow : UserControl
    {
        private Course relatedCourse;
        public Course RelatedCourse { get { return relatedCourse; } set { relatedCourse = value; } }

        private DataTable dataTable;

        public DoctorCourseRosterWindow()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();

                Loaded += DoctorCourseRosterWindow_Loaded;
                Unloaded += DoctorCourseRosterWindow_Unloaded;
            }
        }

        private void DoctorCourseRosterWindow_Loaded(object sender, EventArgs e)
        {

        }

        private void DoctorCourseRosterWindow_Unloaded(object sender, EventArgs e)
        {
            
        }

        private void SubmitButton_Clicked(object sender, RoutedEventArgs e)
        {
            
        }

        public void UpdateRosterGridForDoctorCourse(int courseID)
        {
            if (courseID <= 0)
                return;
            
            Roster roster = Roster.GetRosterFromCourseID(courseID);

            PopulateRosterTable(roster);

            this.RosterDataGrid.UpdateLayout();
        }

        private void WipeOverlayClean()
        {
            this.RelatedCourse = null;
        }

        
        private void CancelButton_Clicked(object sender, RoutedEventArgs e)
        {
            CancelOverlay();
        }

        /// <summary>
        /// Closes the overlay, clearing feilds and 
        /// </summary>
        public void CancelOverlay()
        {
            this.WipeOverlayClean();

            this.IsEnabled = false;
            this.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Reveal the overlay, allowing the doctor to type a message to the toStudent about the relatedCourse.
        /// </summary>
        /// <param name="toStudent"></param>
        /// <param name="relatedCourse"></param>
        public void ShowOverlay(Course relatedCourse )
        {
            this.RelatedCourse = relatedCourse;     // set the related course

            this.UpdateOverlayData();

            this.Visibility = Visibility.Visible;   // show the overlay
            this.IsEnabled = true;                  // enable the overlay for mouse interaction
        }

        public void UpdateOverlayData()
        {
            if (relatedCourse == null)
                return;

            UpdateRosterGridForDoctorCourse(relatedCourse.ID);
            /*
            if (relatedCourse.Roster == null)
            {
                // Create a new, empty roster. Place it in the Database.
                
            }
            */
        }
        
        /// <summary>
        /// Walks up the object tree hierachy and get's the parent object.
        /// </summary>
        /// <param name="startObject"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private DependencyObject GetDependencyObjectFromVisualTree(DependencyObject startObject, Type type)
        {
            DependencyObject parent = startObject;

            while (parent != null)
            {
                if (type.IsInstanceOfType(parent))
                    break;

                else
                    parent = VisualTreeHelper.GetParent(parent);
            }

            return parent;
        }

        private void DataGrid_RowEditEnding(object sender, Microsoft.Windows.Controls.DataGridRowEditEndingEventArgs e)
        {

        }

        private DataTable CreateDataTableLayout()
        {
            dataTable = new DataTable();

            DataColumn myDataColumn;

            myDataColumn = new DataColumn();
            myDataColumn.DataType = Type.GetType("System.Int32");
            myDataColumn.ColumnName = "#";
            dataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = Type.GetType("System.Int32");
            myDataColumn.ColumnName = "StudentID";
            dataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = Type.GetType("System.Int32");
            myDataColumn.ColumnName = "AccountID";
            dataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = Type.GetType("System.String");
            myDataColumn.ColumnName = "First Name";
            dataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = Type.GetType("System.String");
            myDataColumn.ColumnName = "Last Name";
            dataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = Type.GetType("System.String");
            myDataColumn.ColumnName = "Email";
            dataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = Type.GetType("System.DateTime");
            myDataColumn.ColumnName = "Last Login";
            dataTable.Columns.Add(myDataColumn);

            return dataTable;
        }

        public void PopulateRosterTable(Roster roster)
        {
            if (dataTable == null)
                this.CreateDataTableLayout();

            dataTable.Clear();

            if (roster == null)
                return;

            int indx = 1;

            foreach (int studentid in roster.StudentIDs)
            {
                DatabaseManager.OpenConnection();

                //Get student information from students table.
                NpgsqlCommand command = new NpgsqlCommand(String.Format("select uid, accountid, firstname, lastname FROM students where uid='{0}'", studentid), DatabaseManager.Connection);
                NpgsqlDataReader reader = command.ExecuteReader();

                DataRow row = dataTable.NewRow();

                row[0] = indx;

                reader.Read();  // read the single result set.
                row[1] = reader.GetInt32(reader.GetOrdinal("uid"));         // get the studentid from the student
                row[2] = reader.GetInt32(reader.GetOrdinal("accountid"));   // get the linked accountid from the student
                row[3] = reader.GetString(reader.GetOrdinal("firstname"));  // get the first name from the student
                row[4] = reader.GetString(reader.GetOrdinal("lastname"));   // get the last name from the student
                reader.Dispose();   // dispose of the reader

                DatabaseManager.CloseConnection();

                
                DatabaseManager.OpenConnection();
                // Now get info from the user's account.
                command = new NpgsqlCommand(String.Format("select email, lastlogin FROM accounts WHERE uid='{0}'", row[2]), DatabaseManager.Connection);
                reader = command.ExecuteReader();

                reader.Read();  // read the single result
                row[5] = reader.GetString(reader.GetOrdinal("email"));  // get the email address from the account

                if ( reader.IsDBNull( reader.GetOrdinal("lastlogin") ) )
                {
                    row[6] = DateTime.MinValue;
                }
                else
                {
                    row[6] = reader.GetDateTime(reader.GetOrdinal("lastlogin"));
                }

                reader.Dispose();   // dispose the reader

                DatabaseManager.CloseConnection();

                dataTable.Rows.Add(row);    // add the row

                indx++; // increment index
            }

            this.RosterDataGrid.DataContext = dataTable.DefaultView;
        }
    }
}
