﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for ForgotPasswordOverlay.xaml
    /// </summary>
    public partial class ForgotPasswordOverlay : UserControl
    {
        public ForgotPasswordOverlay()
        {
            InitializeComponent();
        }

        private void UsernameSubmitButton_Click(object sender, RoutedEventArgs e)
        {
            AttemptUsernameSubmission();
        }

        private void SecurityAnswerSubmitButton_Click(object sender, RoutedEventArgs e)
        {
            AttemptSecurityAnswerSubmission();
        }

        private void PasswordSubmitButton_Click(object sender, RoutedEventArgs e)
        {
            AttemptPasswordSubmission();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close(true);   // Close the overlay, wipeing it clean.
        }

        public void AttemptUsernameSubmission()
        {
            if (String.IsNullOrWhiteSpace(this.UserNameTBox.Text))  // check if the Text in the textbox exists.
                return;

            // try to retreive the account from the database.
            DatabaseManager.OpenConnection();
            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            // prepare the query.
            command.CommandType = CommandType.Text;
            command.CommandText = @"SELECT uid FROM accounts WHERE lower(username) = lower(:username);";
            command.Parameters.Add("username", NpgsqlTypes.NpgsqlDbType.Text).Value = UserNameTBox.Text;

            int id = 0;

            // execute the query.
            try
            {
                id = (int)command.ExecuteScalar();  // read the selected id
            }
            catch
            {
                // no account with that username/password is in our database
            }

            DatabaseManager.CloseConnection();

            // return if it doesn't exist.
            if( id == 0 )
                return;

            DatabaseManager.OpenConnection();
            NpgsqlCommand command2 = DatabaseManager.Connection.CreateCommand();

            // prepare the query.
            command2.CommandType = CommandType.Text;
            command2.CommandText = @"SELECT securityquestion FROM accounts WHERE username = :username;";
            command2.Parameters.Add("username", NpgsqlTypes.NpgsqlDbType.Text).Value = UserNameTBox.Text;

            string seqQuestion = null;

            try
            {
                 seqQuestion = (string)command2.ExecuteScalar();  // retreive the security question for the given account.
            }
            catch
            {
                // no account with that username/password is in our database
            }

            DatabaseManager.CloseConnection();

            // retreive the security question for the account.

            if (seqQuestion == null)
            {
                MessageBox.Show("No security question linked to this account.");
                return;
            }

            this.SecurityQuestionTB.Text = seqQuestion;

            this.SecurityQuestionGrid.Visibility = Visibility.Visible;  // show the security question section.
            this.UsernameRequestGrid.Visibility = Visibility.Collapsed; // hide the username section.
        }

        public void AttemptSecurityAnswerSubmission()
        {
            if (String.IsNullOrWhiteSpace(UserNameTBox.Text))
                return; // no username

            if (String.IsNullOrWhiteSpace(SecurityQuestionTB.Text))
                return; // no security question

            if (String.IsNullOrWhiteSpace(this.SecurityAnswerTBox.Text))
                return; // no security answer.

            DatabaseManager.OpenConnection();
            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            // prepare the query.
            command.CommandType = CommandType.Text;
            command.CommandText = @"SELECT uid FROM accounts WHERE lower(username) = lower(:username) AND lower(securityquestion) = lower(:securityquestion) AND lower(securityanswer) = lower(:securityanswer);";
            command.Parameters.Add("username", NpgsqlTypes.NpgsqlDbType.Text).Value = UserNameTBox.Text;
            command.Parameters.Add("securityquestion", NpgsqlTypes.NpgsqlDbType.Text).Value = SecurityQuestionTB.Text;
            command.Parameters.Add("securityanswer", NpgsqlTypes.NpgsqlDbType.Text).Value = SecurityAnswerTBox.Text;

            int? id = 0;
            
            try
            {
                 id = (int?)command.ExecuteScalar();  // retreive the security question for the given account.
            }
            catch( Exception e )
            {
                MessageBox.Show(e.ToString());
                // no account with that username/password is in our database
            }

            DatabaseManager.CloseConnection();

            if ( !id.HasValue || id == 0 )    // return if the search turned up nothing.
            {
                MessageBox.Show("Your answer was incorrect.");
                this.Close(true);
                return;
            }

            this.PasswordChangeGrid.Visibility = System.Windows.Visibility.Visible;     // show the password change options.
            this.SecurityQuestionGrid.Visibility = System.Windows.Visibility.Collapsed; // hide the security question panel.
        }

        public void AttemptPasswordSubmission()
        {
            if (String.IsNullOrWhiteSpace(UserNameTBox.Text))
                return; // no username

            if (String.IsNullOrWhiteSpace(SecurityQuestionTB.Text))
                return; // no security question

            if (String.IsNullOrWhiteSpace(this.SecurityAnswerTBox.Text))
                return; // no security answer.

            if (String.IsNullOrWhiteSpace(this.NewPasswordPBox.Password))
                return; // no password.

            if (String.IsNullOrWhiteSpace(this.ConfirmPasswordPBox.Password))
                return; // no confirmed password.

            if (!CompareNewAndComparePassword())
                return; // password comparison failed.

            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = CommandType.Text;
            command.CommandText = "UPDATE accounts SET password = :password WHERE lower(username) = lower(:username)";
            command.Parameters.Add("password", NpgsqlTypes.NpgsqlDbType.Text).Value = this.ConfirmPasswordPBox.Password;
            command.Parameters.Add("username", NpgsqlTypes.NpgsqlDbType.Text).Value = this.UserNameTBox.Text;

            int queryResultVal = 0;

            try
            {
                queryResultVal = command.ExecuteNonQuery();
            }
            catch
            {

            }

            DatabaseManager.CloseConnection();

            if (queryResultVal == 0)
            {
                // Error creating Last Login Information.
                MessageBox.Show("There was an error updating your new password.");
            }
            else
            {
                this.Close(true);
                MessageBox.Show("Your password has been changed.");
            }
        }

        private bool CompareNewAndComparePassword()
        {
            if (String.IsNullOrWhiteSpace(this.NewPasswordPBox.Password))
                return false; // no password.

            if (String.IsNullOrWhiteSpace(this.ConfirmPasswordPBox.Password))
                return false; // no confirmed password.

            if (this.NewPasswordPBox.Password != this.ConfirmPasswordPBox.Password)
                return false;   // passwords do not match.

            return true;
        }

        public void WipeAll()
        {
            this.UserNameTBox.Text = null;
            this.NewPasswordPBox.Password = null;
            this.ConfirmPasswordPBox.Password = null;
            this.SecurityQuestionTB.Text = null;
            this.SecurityAnswerTBox.Text = null;

            this.PasswordChangeGrid.Visibility = System.Windows.Visibility.Collapsed;
            this.SecurityQuestionGrid.Visibility = System.Windows.Visibility.Collapsed;
            this.UsernameRequestGrid.Visibility = System.Windows.Visibility.Visible;
        }

        public void Close(bool wipe)
        {
            if (wipe)
            {
                this.WipeAll();
            }

            this.Visibility = System.Windows.Visibility.Collapsed;
        }

        public void Show()
        {
            this.WipeAll();
            this.Visibility = System.Windows.Visibility.Visible;
        }
    }
}
