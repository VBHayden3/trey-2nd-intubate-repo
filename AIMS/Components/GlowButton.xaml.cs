﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for GlowButton.xaml
    /// </summary>
    public partial class GlowButton : Button
    {
        public static readonly DependencyProperty ButtonImageProperty = DependencyProperty.Register("ButtonImage", typeof(string), typeof(GlowButton), null);
        public string ButtonImage
        {
            get { return (string)this.GetValue(ButtonImageProperty); }
            set { base.SetValue(ButtonImageProperty, value); }
        }

        public static readonly DependencyProperty GlowImageProperty = DependencyProperty.Register("GlowImage", typeof(string), typeof(GlowButton), null);
        public string GlowImage
        {
            get { return (string)this.GetValue(GlowImageProperty); }
            set { base.SetValue(GlowImageProperty, value); }
        }

        public GlowButton()
        {
            InitializeComponent();

            cmdHelp.DataContext = this;
            imgHelpButtonGlow.DataContext = this;
        }

        private void cmdHelp_MouseEnter(object sender, MouseEventArgs e)
        {
            this.BeginStoryboard((Storyboard)cmdHelp.Template.FindName("StartGlow", cmdHelp));
        }

        private void cmdHelp_MouseLeave(object sender, MouseEventArgs e)
        {
            this.BeginStoryboard( (Storyboard)cmdHelp.Template.FindName("EndGlow", cmdHelp) );
        }
    }
}
