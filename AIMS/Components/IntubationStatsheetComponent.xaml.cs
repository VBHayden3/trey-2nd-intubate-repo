﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.DataVisualization.Charting;
using System.ComponentModel;
using AIMS.Tasks;

namespace AIMS.Components
{

    public partial class IntubationStatsheetComponent : UserControl
    {
        private Student currentStudent;
        public Student CurrentStudent { get { return currentStudent; } set { currentStudent = value; } }

        private LessonStatsheet currentStatsheet;
        public LessonStatsheet CurrentStatsheet { get { return currentStatsheet; } set { currentStatsheet = value; } }

        private Result currentResult;
        public Result CurrentResult { get { return currentResult; } set { currentResult = value; } }

        private Result lastNoviceTestResult;
        public Result LastNoviceTestResult { get { return lastNoviceTestResult; } set { lastNoviceTestResult = value; } }

        private Result lastNovicePracticeResult;
        public Result LastNovicePracticeResult { get { return lastNovicePracticeResult; } set { lastNovicePracticeResult = value; } }

        private Result lastIntermediateTestResult;
        public Result LastIntermediateTestResult { get { return lastIntermediateTestResult; } set { lastIntermediateTestResult = value; } }

        private Result lastIntermediatePracticeResult;
        public Result LastIntermediatePracticeResult { get { return lastIntermediatePracticeResult; } set { lastIntermediatePracticeResult = value; } }

        private Result lastMasterTestResult;
        public Result LastMasterTestResult { get { return lastMasterTestResult; } set { lastMasterTestResult = value; } }

        private Result lastMasterPracticeResult;
        public Result LastMasterPracticeResult { get { return lastMasterPracticeResult; } set { lastMasterPracticeResult = value; } }

        public IntubationStatsheetComponent()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();

                Loaded += IntubationStatsheetComponent_Loaded;
                Unloaded += IntubationStatsheetComponent_Unloaded;
            }
        }

        private void IntubationStatsheetComponent_Loaded(object sender, EventArgs e)
        {
            
        }

        private void IntubationStatsheetComponent_Unloaded(object sender, EventArgs e)
        {
            
        }

        private void WipeOverlayClean()
        {
            this.CurrentStudent = null;
            this.CurrentStatsheet = null;
            this.CurrentResult = null;

            this.LastNovicePracticeResult = null;
            this.LastNoviceTestResult = null;
            this.LastIntermediatePracticeResult = null;
            this.LastIntermediateTestResult = null;
            this.LastMasterPracticeResult = null;
            this.LastMasterTestResult = null;

            this.DisableAllLastRunButtons();

            this.WipeLastResultsViewPanel();

            this.NovicePracticeAverageScoreTB.Text = null;
            this.NoviceTestAverageScoreTB.Text = null;
            this.IntermediatePracticeAverageScoreTB.Text = null;
            this.IntermediateTestAverageScoreTB.Text = null;
            this.MasterPracticeAverageScoreTB.Text = null;
            this.MasterTestAverageScoreTB.Text = null;

            this.NovicePracticeCountTB.Text = null;
            this.NoviceTestCountTB.Text = null;
            this.IntermediatePracticeCountTB.Text = null;
            this.IntermediateTestCountTB.Text = null;
            this.MasterPracticeCountTB.Text = null;
            this.MasterTestCountTB.Text = null;

            this.MasteryLevelTB.Text = null;
            this.PointsTB.Text = null;
            this.TimesPracticedTB.Text = null;
            this.TimesTestedTB.Text = null;
            this.TimeLastPracticedTB.Text = null;
            this.TimeLastTestedTB.Text = null;
        }


        private void CancelButton_Clicked(object sender, RoutedEventArgs e)
        {
            CancelOverlay();
        }

        /// <summary>
        /// Closes the overlay, clearing feilds and 
        /// </summary>
        public void CancelOverlay()
        {
            this.WipeOverlayClean();

            this.IsEnabled = false;
            this.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Reveal the overlay, allowing the doctor to type a message to the toStudent about the relatedCourse.
        /// </summary>
        /// <param name="toStudent"></param>
        /// <param name="relatedCourse"></param>
        public void ShowOverlay(Student student)
        {
            this.CurrentStudent = student;

            if (student != null && student.IntubationStatsheet != null)
                this.CurrentStatsheet = student.IntubationStatsheet;

            this.UpdateOverlayData();
            this.CloseLastResultsViewPanel();   // ensures that the last results view panel section is hidden/disabled

            this.Visibility = Visibility.Visible;   // show the overlay
            this.IsEnabled = true;                  // enable the overlay for mouse interaction
        }

        public void UpdateOverlayData()
        {
            if (currentStudent == null)
            {
                DisableAllLastRunButtons();
                return;
            }

            if (currentStatsheet == null)   // statsheet wasn't found.
            {
                if (currentStudent.IntubationStatsheet != null) // check for the statsheet on the student
                {
                    // set the student's statsheet as the currentStatsheet
                    currentStatsheet = currentStudent.IntubationStatsheet;
                }
                else
                {
                    // attempt to find or create a statsheet for the student   
                    LessonStatsheet statsheet = LessonStatsheet.GetOrCreateStatsheetForStudent(currentStudent.ID, LessonType.Intubation);

                    if (statsheet != null)  // a statsheet now exists for the student.
                    {
                        currentStudent.IntubationStatsheet = statsheet; // set the currentStudent's statsheet to the statsheet
                        currentStatsheet = currentStudent.IntubationStatsheet;  // set the currentStatsheet as the currentStudent's statsheet
                    }
                    else
                    {
                        DisableAllLastRunButtons();
                        // statsheet wasn't found for the student, and no statsheet could be created for them either.
                        MessageBox.Show("No statsheet could be found.");
                        return;
                    }
                }
            }

            EnableDisableLastRunButtons(currentStatsheet);

            MasteryLevelTB.Text = String.Format("{0}", currentStatsheet.MasteryLevel);
            TimesPracticedTB.Text = String.Format("{0}", currentStatsheet.TimesPracticed);
            TimesTestedTB.Text = String.Format("{0}", currentStatsheet.TimesTested);
            PointsTB.Text = String.Format("{0}", currentStatsheet.Points);
            TimeLastPracticedTB.Text = String.Format("{0}", currentStatsheet.TimeLastPracticed);
            TimeLastTestedTB.Text = String.Format("{0}", currentStatsheet.TimeLastTested);
            NovicePracticeCountTB.Text = String.Format("{0}", currentStatsheet.TimesPracticedNovice);
            NovicePracticeAverageScoreTB.Text = String.Format("{0:P2}", currentStatsheet.AverageNovicePracticeScore);
            NoviceTestCountTB.Text = String.Format("{0}", currentStatsheet.TimesTestedNovice);
            NoviceTestAverageScoreTB.Text = String.Format("{0:P2}", currentStatsheet.AverageNoviceTestScore);
            IntermediatePracticeCountTB.Text = String.Format("{0}", currentStatsheet.TimesPracticedIntermediate);
            IntermediatePracticeAverageScoreTB.Text = String.Format("{0:P2}", currentStatsheet.AverageIntermediatePracticeScore);
            IntermediateTestCountTB.Text = String.Format("{0}", currentStatsheet.TimesTestedIntermediate);
            IntermediateTestAverageScoreTB.Text = String.Format("{0:P2}", currentStatsheet.AverageIntermediateTestScore);
            MasterPracticeCountTB.Text = String.Format("{0}", currentStatsheet.TimesPracticedMaster);
            MasterPracticeAverageScoreTB.Text = String.Format("{0:P2}", currentStatsheet.AverageMasterPracticeScore);
            MasterTestCountTB.Text = String.Format("{0}", currentStatsheet.TimesTestedMaster);
            MasterTestAverageScoreTB.Text = String.Format("{0}", currentStatsheet.AverageMasterTestScore);
        }

        /// <summary>
        /// Walks up the object tree hierachy and get's the parent object.
        /// </summary>
        /// <param name="startObject"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private DependencyObject GetDependencyObjectFromVisualTree(DependencyObject startObject, Type type)
        {
            DependencyObject parent = startObject;

            while (parent != null)
            {
                if (type.IsInstanceOfType(parent))
                    break;

                else
                    parent = VisualTreeHelper.GetParent(parent);
            }

            return parent;
        }

        private void DisableAllLastRunButtons()
        {
            this.LastNovicePracticeButton.IsEnabled = false;
            this.LastNoviceTestButton.IsEnabled = false;
            this.LastIntermediatePracticeButton.IsEnabled = false;
            this.LastIntermediateTestButton.IsEnabled = false;
            this.LastMasterPracticeButton.IsEnabled = false;
            this.LastMasterTestButton.IsEnabled = false;
        }

        private void EnableDisableLastRunButtons(LessonStatsheet statsheet)
        {
            if (statsheet == null)
            {
                DisableAllLastRunButtons();
                return;
            }

            this.LastNovicePracticeButton.IsEnabled = (statsheet.LastNovicePracticeResultID > 0);
            this.LastNoviceTestButton.IsEnabled = (statsheet.LastNoviceTestResultID > 0);
            this.LastIntermediatePracticeButton.IsEnabled = (statsheet.LastIntermediatePracticeResultID > 0);
            this.LastIntermediateTestButton.IsEnabled = (statsheet.LastIntermediateTestResultID > 0);
            this.LastMasterPracticeButton.IsEnabled = (statsheet.LastMasterPracticeResultID > 0);
            this.LastMasterTestButton.IsEnabled = (statsheet.LastMasterTestResultID > 0);
        }

        private void ColumnSeries_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ColumnSeries colseries = sender as ColumnSeries;

            if (e == null || sender == null || colseries == null || e.AddedItems == null)
                return;

            if (colseries != null)
            {
                if (e.AddedItems.Count > 0 && e.AddedItems[0] is KeyValuePair<int, double>)
                {
                    //deserializeScreenShotFromDatabase((KeyValuePair<int, double>)(e.AddedItems[0]));

                    ShowStageScoreInformation(((KeyValuePair<int, double>)(e.AddedItems[0])).Key - 1, currentResult);

                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// Converts the index passed into a stage score object within the current result.
        /// </summary>
        /// <param name="index"></param>
        public void ShowStageScoreInformation(int index, Result result)
        {
            if (result != null)
            {
                if (result.StageScores != null)
                {
                    if (index >= 0 && index < result.StageScores.Count)
                    {
                        ShowStageScoreInformation(result.StageScores[index]);
                    }
                    else
                    {
                        MessageBox.Show(String.Format("Index {0} is outside the bounds of the Result StageScore collection.", index));
                    }
                }
                else
                {
                    MessageBox.Show("The linked result consists of no stage score information.");
                }
            }
            else
            {
                MessageBox.Show("No result object has been linked to display.");
            }
        }

        private void BuildSingleResultChart(List<StageScore> stageScoreList)
        {
            List<KeyValuePair<int, double>> values = new List<KeyValuePair<int, double>>(); // list of key value pairs (int = stage#, double = score )

            foreach (StageScore stageScore in stageScoreList)
            {
                values.Add(new KeyValuePair<int, double>(stageScore.StageNumber + 1, stageScore.Score * 100));
                ////MessageBox.Show(String.Format("New keyValuePair added: stage: {0}, score: {1}", stageScore.StageNumber + 1, stageScore.Score * 100));
            }

            try
            {
                (singleChart.Series[0] as DataPointSeries).ItemsSource = null;
                (singleChart.Series[0] as DataPointSeries).ItemsSource = values;
                singleChart.UpdateLayout();

                if (values.Count > 0)
                    (singleChart.Series[0] as DataPointSeries).SelectedItem = values[0];

                ////MessageBox.Show("SinglesChart updated");
            }
            catch (Exception ex)
            {
                ////MessageBox.Show(ex.ToString());
                Console.WriteLine(ex.ToString());
            }
        }

        /// <summary>
        /// Updates the Results Overlay information with data from the stagescore.
        /// </summary>
        /// <param name="stagescore"></param>
        public void ShowStageScoreInformation(StageScore stagescore)
        {
            if (stagescore != null)
            {
                if (stagescore.HasScreenshot && stagescore.ScreenShot != null)
                {
                    _3DViewer.setImages(stagescore.ScreenShot.colorData, stagescore.ScreenShot.depthData);
                    //bitmap.WritePixels(new Int32Rect(0, 0, 640, 480), stagescore.screenshot.colorData, 640 * 4, 0);
                }

                this.ScoreNumber.Text = String.Format("{0:P2}", stagescore.Score);
                this.TimeNumber.Text = String.Format("{0:#0.#} sec{1}", stagescore.TimeInStage.TotalSeconds, stagescore.TimeInStage.TotalSeconds == 1 ? "" : "s");
                this.StageName.Text = stagescore.StageName;
                this.StageDescription.Text = stagescore.Target;
                this.DetailsText.Text = String.IsNullOrWhiteSpace(stagescore.Result) ? ""/*"No detailed information is available for this stage."*/ : stagescore.Result;
            }
            else
            {
                MessageBox.Show("The stage score data does not exist.");
            }
        }

        private void LastNoviceTestButton_Clicked(object sender, RoutedEventArgs e)
        {
            ShowLastNoviceTestResult();
        }

        private void ShowLastNoviceTestResult()
        {
            if (currentStatsheet == null)
                return;

            if (lastNoviceTestResult == null)   // If the result has already been retreived and was stored, then we can re-use this result object rather than re-retreiving it from the Database.
            {
                lastNoviceTestResult = Result.GetResultFromResultID(currentStatsheet.LastNoviceTestResultID.HasValue ? (int)currentStatsheet.LastNoviceTestResultID : 0, true, true);
            }

            if (lastNoviceTestResult != null)
            {
                currentResult = lastNoviceTestResult;

                BuildSingleResultChart(lastNoviceTestResult.StageScores);
                ShowStageScoreInformation(0, lastNoviceTestResult);

                ShowResultsViewPanel();
            }
        }

        private void LastNovicePracticeButton_Clicked(object sender, RoutedEventArgs e)
        {
            ShowLastNovicePracticeResult();
        }

        private void ShowLastNovicePracticeResult()
        {
            if (currentStatsheet == null)
                return;

            if (lastNovicePracticeResult == null)   // If the result has already been retreived and was stored, then we can re-use this result object rather than re-retreiving it from the Database.
            {
                lastNovicePracticeResult = Result.GetResultFromResultID(currentStatsheet.LastNovicePracticeResultID.HasValue ? (int)currentStatsheet.LastNovicePracticeResultID : 0, true, true);
            }

            if (lastNovicePracticeResult != null)
            {
                currentResult = lastNovicePracticeResult;

                BuildSingleResultChart(lastNovicePracticeResult.StageScores);
                ShowStageScoreInformation(0, lastNovicePracticeResult);

                ShowResultsViewPanel();
            }
        }

        private void LastIntermediateTestButton_Clicked(object sender, RoutedEventArgs e)
        {
            ShowLastIntermediateTestResult();
        }

        private void ShowLastIntermediateTestResult()
        {
            if (currentStatsheet == null)
                return;

            if (lastIntermediateTestResult == null)   // If the result has already been retreived and was stored, then we can re-use this result object rather than re-retreiving it from the Database.
            {
                lastIntermediateTestResult = Result.GetResultFromResultID(currentStatsheet.LastIntermediateTestResultID.HasValue ? (int)currentStatsheet.LastIntermediateTestResultID : 0, true, true);
            }

            if (lastIntermediateTestResult != null)
            {
                currentResult = lastIntermediateTestResult;

                BuildSingleResultChart(lastIntermediateTestResult.StageScores);
                ShowStageScoreInformation(0, lastIntermediateTestResult);

                ShowResultsViewPanel();
            }
        }

        private void LastIntermediatePracticeButton_Clicked(object sender, RoutedEventArgs e)
        {
            ShowLastIntermediatePracticeResult();
        }

        private void ShowLastIntermediatePracticeResult()
        {
            if (currentStatsheet == null)
                return;

            if (lastIntermediatePracticeResult == null)   // If the result has already been retreived and was stored, then we can re-use this result object rather than re-retreiving it from the Database.
            {
                lastIntermediatePracticeResult = Result.GetResultFromResultID(currentStatsheet.LastIntermediatePracticeResultID.HasValue ? (int)currentStatsheet.LastIntermediatePracticeResultID : 0, true, true);
            }

            if (lastIntermediatePracticeResult != null)
            {
                currentResult = lastIntermediatePracticeResult;

                BuildSingleResultChart(lastIntermediatePracticeResult.StageScores);
                ShowStageScoreInformation(0, lastIntermediatePracticeResult);

                ShowResultsViewPanel();
            }
        }

        private void LastMasterTestButton_Clicked(object sender, RoutedEventArgs e)
        {
            ShowLastMasterTestResult();
        }

        private void ShowLastMasterTestResult()
        {
            if (currentStatsheet == null)
                return;

            if (lastMasterTestResult == null)   // If the result has already been retreived and was stored, then we can re-use this result object rather than re-retreiving it from the Database.
            {
                lastMasterTestResult = Result.GetResultFromResultID(currentStatsheet.LastMasterTestResultID.HasValue ? (int)currentStatsheet.LastMasterTestResultID : 0, true, true);
            }

            if (lastMasterTestResult != null)
            {
                currentResult = lastMasterTestResult;

                BuildSingleResultChart(lastMasterTestResult.StageScores);
                ShowStageScoreInformation(0, lastMasterTestResult);

                ShowResultsViewPanel();
            }
        }

        private void LastMasterPracticeButton_Clicked(object sender, RoutedEventArgs e)
        {
            ShowLastMasterPracticeResult();
        }

        private void ShowLastMasterPracticeResult()
        {
            if (currentStatsheet == null)
                return;

            if (lastMasterPracticeResult == null)   // If the result has already been retreived and was stored, then we can re-use this result object rather than re-retreiving it from the Database.
            {
                lastMasterPracticeResult = Result.GetResultFromResultID(currentStatsheet.LastMasterPracticeResultID.HasValue ? (int)currentStatsheet.LastMasterPracticeResultID : 0, true, true);
            }

            if (lastMasterPracticeResult != null)
            {
                currentResult = lastMasterPracticeResult;

                BuildSingleResultChart(lastMasterPracticeResult.StageScores);
                ShowStageScoreInformation(0, lastMasterPracticeResult);

                ShowResultsViewPanel();
            }
        }

        /// <summary>
        /// Enables and displays all elements of the LastResultsViewPanel section.
        /// </summary>
        private void ShowResultsViewPanel()
        {
            CloseResultViewButton.IsEnabled = true;
            CloseResultViewButton.Visibility = Visibility.Visible;

            this._3DViewer.IsEnabled = true;
            this.singleChart.IsEnabled = true;

            this.LastResultDetailsCanvas.Visibility = Visibility.Visible;
            this.LastResultDetailsCanvas.IsEnabled = true;

            this._3DViewer.Visibility = Visibility.Visible;
            this.singleChart.Visibility = Visibility.Visible;
        }

        /// <summary>
        ///  Wipes the data out of the LastResultsViewPanel section.
        /// </summary>
        private void WipeLastResultsViewPanel()
        {
            if (_3DViewer.IsVisible)
                _3DViewer.WipeClean();

            (singleChart.Series[0] as DataPointSeries).ItemsSource = null;
            (singleChart.Series[0] as DataPointSeries).ItemsSource = new List<KeyValuePair<int, double>>();
            singleChart.UpdateLayout();

            this.StageDescription.Text = null;
            this.StageName.Text = null;
            this.ScoreNumber.Text = null;
            this.TimeNumber.Text = null;
            this.DetailsText.Text = null;
        }

        /// <summary>
        /// Wipes, hides, and disables all elements of the LastResultsViewPanel section.
        /// </summary>
        private void CloseLastResultsViewPanel()
        {
            CloseResultViewButton.IsEnabled = false;
            CloseResultViewButton.Visibility = Visibility.Hidden;

            this.singleChart.IsEnabled = false;
            this._3DViewer.IsEnabled = false;
            this._3DViewer.Visibility = Visibility.Hidden;
            this.singleChart.Visibility = Visibility.Hidden;

            this.LastResultDetailsCanvas.Visibility = Visibility.Hidden;
            this.LastResultDetailsCanvas.IsEnabled = false;

            this.WipeLastResultsViewPanel();
        }

        private void CloseResultViewButton_Clicked(object sender, RoutedEventArgs e)
        {
            this.CloseLastResultsViewPanel();
        }
    }
}
