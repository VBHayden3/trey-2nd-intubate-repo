﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;
using System.Windows.Media.Media3D;
using AIMS;

namespace AIMS.Components
{
    /// <summary>
    /// Skeleton overlay will display specified joints for the skeleton that is passed in.
    /// </summary>
    public partial class IntubationToolsOverlay : UserControl
    {

        int colorWidth = 1920;
        int colorHeight = 1080;
        int scale = 2;

        private bool showCrosshair = true;
        private bool showCrosshairText = false;
        private Point3D laryngoscope3DPosition = new Point3D(0,0,0);
        private Point3D endotrachealTube3DPosition = new Point3D(0, 0, 0);
        private Point3D stylet3DPosition = new Point3D(0, 0, 0);
        private Point3D syringe3DPosition = new Point3D(0, 0, 0);

        public bool ShowCrosshair { get { return showCrosshair; } set { showCrosshair = value; UpdateLaryngoscopeCrosshair(); } }
        public bool ShowCrosshairText { get { return showCrosshairText; } set { showCrosshairText = value; UpdateLaryngoscopeCrosshair(); } }
        public Point3D Laryngoscope3DPosition { get { return laryngoscope3DPosition; } set { laryngoscope3DPosition = value; UpdateLaryngoscopeCrosshair(); } }
        public Point3D EndotrachealTube3DPosition { get { return endotrachealTube3DPosition; } set { endotrachealTube3DPosition = value; UpdateEndotrachealTubeCrosshair(); } }
        public Point3D Stylet3DPosition { get { return stylet3DPosition; } set { stylet3DPosition = value; UpdateStyletCrosshair(); } }
        public Point3D Syringe3DPosition { get { return syringe3DPosition; } set { syringe3DPosition = value; UpdateSyringeCrosshair(); } }

        public static readonly DependencyProperty ShowCrosshairProperty =
            DependencyProperty.Register("ShowCrosshair", typeof(bool), typeof(IntubationToolsOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowScanProperty =
            DependencyProperty.Register("ShowCrosshairText", typeof(bool), typeof(IntubationToolsOverlay), new PropertyMetadata(false, OverlaySettingChanged));

        public static readonly DependencyProperty Laryngoscope3DPositionProperty =
            DependencyProperty.Register("Laryngoscope3DPosition", typeof(Point3D), typeof(IntubationToolsOverlay), new PropertyMetadata(new Point3D(), OverlaySettingChanged));

        public static readonly DependencyProperty EndotrachealTube3DPositionProperty =
            DependencyProperty.Register("EndotrachealTube3DPosition", typeof(Point3D), typeof(IntubationToolsOverlay), new PropertyMetadata(new Point3D(), OverlaySettingChanged));

        public static readonly DependencyProperty Stylet3DPositionProperty =
            DependencyProperty.Register("Stylet3DPosition", typeof(Point3D), typeof(IntubationToolsOverlay), new PropertyMetadata(new Point3D(), OverlaySettingChanged));

        public static readonly DependencyProperty Syringe3DPositionProperty =
            DependencyProperty.Register("Syringe3DPosition", typeof(Point3D), typeof(IntubationToolsOverlay), new PropertyMetadata(new Point3D(), OverlaySettingChanged));

        public IntubationToolsOverlay()
        {
            InitializeComponent();
        }
        
        public void UpdateLaryngoscopeCrosshair( double x, double y )
        {
            laryngoscope3DPosition.X = x;
            laryngoscope3DPosition.Y = y;

            UpdateLaryngoscopeCrosshair();
        }

        public void UpdateLaryngoscopeCrosshair(double x, double y, double z)
        {
            // CameraSpacePoint
            laryngoscope3DPosition.X = x;
            laryngoscope3DPosition.Y = y;
            laryngoscope3DPosition.Z = z;

            UpdateLaryngoscopeCrosshair();
        }

        public void UpdateLaryngoscopeCrosshair()
        {
            if (ShowCrosshair && !Double.IsNaN(laryngoscope3DPosition.X) && !Double.IsNaN(laryngoscope3DPosition.Y) && laryngoscope3DPosition.X != 0 && laryngoscope3DPosition.Y != 0)
            {
                System.Diagnostics.Debug.WriteLine("IntubationToolsOverlay-> UpdateLaryngoscopeCrosshair " + laryngoscope3DPosition.X + "," + laryngoscope3DPosition.Y + "," + laryngoscope3DPosition.Z + "mm");
                
                // Z is in meters
                //double scaledCrosshairLength = (laryngoscope3DPosition.Z == 0 ? 3 : (3 * laryngoscope3DPosition.Z / 2)) * (this.ActualWidth / colorWidth) * scale;
                double scaledCrosshairLength = 3;

                // Convert the 3DPosition CameraSpace to 2DPosition ColorSpace
                CameraSpacePoint laryngoscope = new CameraSpacePoint() { X = (float)laryngoscope3DPosition.X, Y = (float)laryngoscope3DPosition.Y, Z = (float)laryngoscope3DPosition.Z / 1000 };
                ColorSpacePoint colorPoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(laryngoscope);
                System.Diagnostics.Debug.WriteLine("IntubationToolsOverlay-> using laryngoscope color point (" + colorPoint.X + "," + colorPoint.Y + ") and z=" + laryngoscope3DPosition.Z + "mm");

                laryngoscopeLine1.Visibility = Visibility.Visible;
                laryngoscopeLine2.Visibility = Visibility.Visible;

                laryngoscopeLine1.X1 = colorPoint.X * this.ActualWidth / colorWidth - scaledCrosshairLength;
                laryngoscopeLine1.X2 = colorPoint.X * this.ActualWidth / colorWidth + scaledCrosshairLength;
                laryngoscopeLine1.Y1 = colorPoint.Y * this.ActualHeight / colorHeight - scaledCrosshairLength;
                laryngoscopeLine1.Y2 = colorPoint.Y * this.ActualHeight / colorHeight + scaledCrosshairLength;

                laryngoscopeLine2.X1 = colorPoint.X * this.ActualWidth / colorWidth - scaledCrosshairLength;
                laryngoscopeLine2.X2 = colorPoint.X * this.ActualWidth / colorWidth + scaledCrosshairLength;
                laryngoscopeLine2.Y1 = colorPoint.Y * this.ActualHeight / colorHeight + scaledCrosshairLength;
                laryngoscopeLine2.Y2 = colorPoint.Y * this.ActualHeight / colorHeight - scaledCrosshairLength;
            }
            else
            {
                laryngoscopeLine1.Visibility = Visibility.Hidden;
                laryngoscopeLine2.Visibility = Visibility.Hidden;
            }

            if (ShowCrosshairText && !Double.IsNaN(laryngoscope3DPosition.X) && !Double.IsNaN(laryngoscope3DPosition.Y) && laryngoscope3DPosition.X != 0 && laryngoscope3DPosition.Y != 0)
            {
                // Convert the 3DPosition CameraSpace to 2DPosition ColorSpace
                CameraSpacePoint laryngoscope = new CameraSpacePoint() { X = (float)laryngoscope3DPosition.X, Y = (float)laryngoscope3DPosition.Y, Z = (float)laryngoscope3DPosition.Z / 1000 };
                ColorSpacePoint colorPoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(laryngoscope);
                System.Diagnostics.Debug.WriteLine("IntubationToolsOverlay-> using laryngoscope point (" + colorPoint.X + "," + colorPoint.Y + ") and z=" + laryngoscope3DPosition.Z + "mm");

                crosshairPositionTextBlock.Visibility = Visibility.Visible;

                crosshairPositionTextBlock.FontSize = 16 * (double)this.ActualHeight / colorHeight;
                crosshairPositionTextBlock.Text = String.Format("({0:###.#},{1:###.#})", colorPoint.X, colorPoint.Y);
                Canvas.SetLeft( crosshairPositionTextBlock, (colorPoint.X - (25 * scale)) * this.ActualWidth/colorWidth );
                Canvas.SetTop( crosshairPositionTextBlock, (colorPoint.Y - (25 * scale)) * this.ActualHeight/colorHeight );
            }
            else
            {
                crosshairPositionTextBlock.Visibility = Visibility.Hidden;
            }

            //this.InvalidateVisual();
        }

        public void UpdateEndotrachealTubeCrosshair(double x, double y)
        {
            endotrachealTube3DPosition.X = x;
            endotrachealTube3DPosition.Y = y;

            UpdateEndotrachealTubeCrosshair();
        }

        public void UpdateEndotrachealTubeCrosshair(double x, double y, double z)
        {
            // CameraSpacePoint
            endotrachealTube3DPosition.X = x;
            endotrachealTube3DPosition.Y = y;
            endotrachealTube3DPosition.Z = z;

            UpdateEndotrachealTubeCrosshair();
        }

        public void UpdateEndotrachealTubeCrosshair()
        {
            if (ShowCrosshair && !Double.IsNaN(endotrachealTube3DPosition.X) && !Double.IsNaN(endotrachealTube3DPosition.Y) && endotrachealTube3DPosition.X != 0 && endotrachealTube3DPosition.Y != 0)
            {
                // Z is in meters
                //double scaledCrosshairLength = (endotrachealTube3DPosition.Z == 0 ? 3 : (3 * endotrachealTube3DPosition.Z / 2)) * (this.ActualWidth / colorWidth) * scale;
                double scaledCrosshairLength = 3;

                // Convert the 3DPosition CameraSpace to 2DPosition ColorSpace
                CameraSpacePoint endotrachealTube = new CameraSpacePoint() { X = (float)endotrachealTube3DPosition.X, Y = (float)endotrachealTube3DPosition.Y, Z = (float)endotrachealTube3DPosition.Z / 1000 };
                ColorSpacePoint colorPoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(endotrachealTube);
                System.Diagnostics.Debug.WriteLine("IntubationToolsOverlay-> using endotrachealTube point (" + colorPoint.X + "," + colorPoint.Y + ") and z=" + endotrachealTube3DPosition.Z + "mm");

                endotrachealTubeLine1.Visibility = Visibility.Visible;
                endotrachealTubeLine2.Visibility = Visibility.Visible;

                endotrachealTubeLine1.X1 = colorPoint.X * this.ActualWidth / colorWidth - scaledCrosshairLength;
                endotrachealTubeLine1.X2 = colorPoint.X * this.ActualWidth / colorWidth + scaledCrosshairLength;
                endotrachealTubeLine1.Y1 = colorPoint.Y * this.ActualHeight / colorHeight - scaledCrosshairLength;
                endotrachealTubeLine1.Y2 = colorPoint.Y * this.ActualHeight / colorHeight + scaledCrosshairLength;

                endotrachealTubeLine2.X1 = colorPoint.X * this.ActualWidth / colorWidth - scaledCrosshairLength;
                endotrachealTubeLine2.X2 = colorPoint.X * this.ActualWidth / colorWidth + scaledCrosshairLength;
                endotrachealTubeLine2.Y1 = colorPoint.Y * this.ActualHeight / colorHeight + scaledCrosshairLength;
                endotrachealTubeLine2.Y2 = colorPoint.Y * this.ActualHeight / colorHeight - scaledCrosshairLength;
            }
            else
            {
                endotrachealTubeLine1.Visibility = Visibility.Hidden;
                endotrachealTubeLine2.Visibility = Visibility.Hidden;
            }
        }

        public void UpdateStyletCrosshair(double x, double y)
        {
            stylet3DPosition.X = x;
            stylet3DPosition.Y = y;

            UpdateStyletCrosshair();
        }

        public void UpdateStyletCrosshair(double x, double y, double z)
        {
            // CameraSpacePoint
            stylet3DPosition.X = x;
            stylet3DPosition.Y = y;
            stylet3DPosition.Z = z;

            UpdateStyletCrosshair();
        }

        public void UpdateStyletCrosshair()
        {
            if (ShowCrosshair && !Double.IsNaN(stylet3DPosition.X) && !Double.IsNaN(stylet3DPosition.Y) && stylet3DPosition.X != 0 && stylet3DPosition.Y != 0)
            {
                // Z is in meters
                //double scaledCrosshairLength = (stylet3DPosition.Z == 0 ? 3 : (3 * stylet3DPosition.Z / 2)) * (this.ActualWidth / colorWidth) * scale;
                double scaledCrosshairLength = 3;

                // Convert the 3DPosition CameraSpace to 2DPosition ColorSpace
                CameraSpacePoint stylet = new CameraSpacePoint() { X = (float)stylet3DPosition.X, Y = (float)stylet3DPosition.Y, Z = (float)stylet3DPosition.Z / 1000 };
                ColorSpacePoint colorPoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(stylet);
                System.Diagnostics.Debug.WriteLine("IntubationToolsOverlay-> using stylet point (" + colorPoint.X + "," + colorPoint.Y + ") and z=" + stylet3DPosition.Z + "mm");

                styletLine1.Visibility = Visibility.Visible;
                styletLine2.Visibility = Visibility.Visible;

                styletLine1.X1 = colorPoint.X * this.ActualWidth / colorWidth - scaledCrosshairLength;
                styletLine1.X2 = colorPoint.X * this.ActualWidth / colorWidth + scaledCrosshairLength;
                styletLine1.Y1 = colorPoint.Y * this.ActualHeight / colorHeight - scaledCrosshairLength;
                styletLine1.Y2 = colorPoint.Y * this.ActualHeight / colorHeight + scaledCrosshairLength;

                styletLine2.X1 = colorPoint.X * this.ActualWidth / colorWidth - scaledCrosshairLength;
                styletLine2.X2 = colorPoint.X * this.ActualWidth / colorWidth + scaledCrosshairLength;
                styletLine2.Y1 = colorPoint.Y * this.ActualHeight / colorHeight + scaledCrosshairLength;
                styletLine2.Y2 = colorPoint.Y * this.ActualHeight / colorHeight - scaledCrosshairLength;
            }
            else
            {
                styletLine1.Visibility = Visibility.Hidden;
                styletLine2.Visibility = Visibility.Hidden;
            }
        }


        public void UpdateSyringeCrosshair(double x, double y)
        {
            syringe3DPosition.X = x;
            syringe3DPosition.Y = y;

            UpdateSyringeCrosshair();
        }

        public void UpdateSyringeCrosshair(double x, double y, double z)
        {
            // CameraSpacePoint
            syringe3DPosition.X = x;
            syringe3DPosition.Y = y;
            syringe3DPosition.Z = z;

            UpdateSyringeCrosshair();
        }

        public void UpdateSyringeCrosshair()
        {
            if (ShowCrosshair && !Double.IsNaN(syringe3DPosition.X) && !Double.IsNaN(syringe3DPosition.Y) && syringe3DPosition.X != 0 && syringe3DPosition.Y != 0)
            {
                // Z is in meters
                //double scaledCrosshairLength = (syringe3DPosition.Z == 0 ? 3 : (3 * syringe3DPosition.Z / 2)) * (this.ActualWidth / colorWidth) * scale;
                double scaledCrosshairLength = 3;

                // Convert the 3DPosition CameraSpace to 2DPosition ColorSpace
                CameraSpacePoint syringe = new CameraSpacePoint() { X = (float)syringe3DPosition.X, Y = (float)syringe3DPosition.Y, Z = (float)syringe3DPosition.Z / 1000 };
                ColorSpacePoint colorPoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(syringe);
                System.Diagnostics.Debug.WriteLine("IntubationToolsOverlay-> using syringe point (" + colorPoint.X + "," + colorPoint.Y + ") and z=" + syringe3DPosition.Z + "mm");

                syringeLine1.Visibility = Visibility.Visible;
                syringeLine2.Visibility = Visibility.Visible;

                syringeLine1.X1 = colorPoint.X * this.ActualWidth / colorWidth - scaledCrosshairLength;
                syringeLine1.X2 = colorPoint.X * this.ActualWidth / colorWidth + scaledCrosshairLength;
                syringeLine1.Y1 = colorPoint.Y * this.ActualHeight / colorHeight - scaledCrosshairLength;
                syringeLine1.Y2 = colorPoint.Y * this.ActualHeight / colorHeight + scaledCrosshairLength;

                syringeLine2.X1 = colorPoint.X * this.ActualWidth / colorWidth - scaledCrosshairLength;
                syringeLine2.X2 = colorPoint.X * this.ActualWidth / colorWidth + scaledCrosshairLength;
                syringeLine2.Y1 = colorPoint.Y * this.ActualHeight / colorHeight + scaledCrosshairLength;
                syringeLine2.Y2 = colorPoint.Y * this.ActualHeight / colorHeight - scaledCrosshairLength;
            }
            else
            {
                syringeLine1.Visibility = Visibility.Hidden;
                syringeLine2.Visibility = Visibility.Hidden;
            }
        }

        // Converts the point from the dictionary into pixel coordinates so that it can be drawn.
        private Point Get2DPosition(RecZone zone)
        {
            Point point = new Point();

            ColorSpacePoint colorPoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(new CameraSpacePoint() { X = (float)zone.X, Y = (float)zone.Y, Z = (float)zone.Z / 1000 });

            point.X = (double)colorPoint.X / KinectManager.ColorWidth;
            point.Y = (double)colorPoint.Y / KinectManager.ColorHeight;

            point.X = point.X * this.RenderSize.Width;
            point.Y = point.Y * this.RenderSize.Height;

            System.Diagnostics.Debug.WriteLine("IntubationToolsOverlay -> XY:" + String.Format("{0}{1}", point.X, point.Y) + " and z=" + zone.Z + "mm");
            return point;
        }

        private static void OverlaySettingChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            IntubationToolsOverlay overlay = sender as IntubationToolsOverlay;

            if (overlay != null)
            {
                overlay.UpdateLaryngoscopeCrosshair();
                overlay.UpdateEndotrachealTubeCrosshair();
                overlay.UpdateStyletCrosshair();
                overlay.UpdateSyringeCrosshair();
            }
        }
    }
}
