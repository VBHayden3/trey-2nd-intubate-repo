﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for Kinect3DViewport.xaml
    /// </summary>
    public partial class Kinect3DViewport : UserControl
    {
        public Kinect3DViewport()
        {
            InitializeComponent();
            Loaded += Kinect3DViewport_Loaded;
            Unloaded += Kinect3DViewport_Unloaded;
        }

        public void Kinect3DViewport_Loaded(object sender, EventArgs e)
        {
            if (KinectManager.Kinect != null)
            {
                KinectManager.AllFramesReady += Kinect_AllFramesReady;
            }
        }

        public void Kinect3DViewport_Unloaded(object sender, EventArgs e)
        {
            if (KinectManager.Kinect != null)
            {
                KinectManager.AllFramesReady -= Kinect_AllFramesReady;
            }
        }

        //private void Kinect_AllFramesReady(object sender, Microsoft.Kinect.AllFramesReadyEventArgs e)
        private void Kinect_AllFramesReady()
        {

        }
    }
}
