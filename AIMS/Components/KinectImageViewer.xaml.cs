﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for KinectImageViewer.xaml
    /// </summary>
    public partial class KinectImageViewer : UserControl
    {
        public static readonly DependencyProperty KinectImageProperty =
            DependencyProperty.Register("KinectImage", typeof(KinectImage), typeof(KinectImageViewer), new PropertyMetadata(null, OnKinectImageChanged));

        private static void OnKinectImageChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            if (obj is KinectImageViewer)
            {
                (obj as KinectImageViewer).OnKinectImageChanged(args);
            }
        }

        public event DependencyPropertyChangedEventHandler KinectImage_Changed;

        private void OnKinectImageChanged(DependencyPropertyChangedEventArgs args)
        {
            if (KinectImage_Changed != null)
            {
                KinectImage_Changed(this, args);
            }

            this.SetCurrentImages(); 
        }

        public KinectImage KinectImage
        {
            get { return (KinectImage)GetValue(KinectImageProperty); }
            set { SetValue(KinectImageProperty, value); SetCurrentImages(); }
        }

        public static readonly DependencyProperty DisplayModeProperty =
            DependencyProperty.Register("DisplayMode", typeof(DisplayModes), typeof(KinectImageViewer), new PropertyMetadata(DisplayModes.ColorOnly2D));

        public DisplayModes DisplayMode
        {
            get { return (DisplayModes)GetValue(DisplayModeProperty); }
            set { SetValue(DisplayModeProperty, value); }
        }

        public enum DisplayModes
        {
            /// <summary>
            /// For dislpaying the un-mapped, clean color image frame.
            /// </summary>
            ColorOnly2D,    // Regular Color Image
            /// <summary>
            /// For displaying the mapped color depth data, in a 2D image.
            /// </summary>
            ColorDepth2D,   // Mapped Color-Depth Image
            /// <summary>
            /// For displaying the color data in 3D, using the depth data.
            /// </summary>
            ColorDepth3D    // 3D Interactable Color-Depth Image
        };

        private void KinectImageViewer_Loaded( object sender, RoutedEventArgs args )
        {
            this.DataContext = this;

            #region Veiwport3D Initialization
            this.Focusable = true;

            Keyboard.Focus(this);

            int scaledWidth = (int)(width * scale);
            int scaledHeight = (int)(height * scale);

            this.Camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
            this.Camera.LookDirection = new Vector3D(lookAt.X - this.Camera.Position.X, lookAt.Y - this.Camera.Position.Y, lookAt.Z - this.Camera.Position.Z);

            // Set up the mesh
            mesh = new MeshGeometry3D();

            // Moved to declaration
            pointCollection = new Point3DCollection(scaledWidth * scaledHeight);
            textureCoordinates = new PointCollection(scaledWidth * scaledHeight);
            indecies = new Int32Collection((3 * scaledWidth * scaledHeight - 2 * scaledHeight - 2 * scaledWidth + 1) * 6);

            // Build out the mesh with a vertext at each pixel
            for (int i = 0; i < scaledHeight; i++)
            {
                for (int j = 0; j < scaledWidth; j++)
                {
                    Point3D point = new Point3D(0.01 * width - (0.01 / scale * j), 0.01 / scale * (scaledHeight - i), 0.0);
                    Point coordinate = new Point((double)j / (scaledWidth - 1), (double)i / (scaledHeight - 1));

                    pointCollection.Add(point);
                    textureCoordinates.Add(coordinate);
                }
            }

            // Build out the triangles in the mesh
            for (int i = 0; i < scaledHeight - 1; i++)
            {
                for (int j = 0; j < scaledWidth - 1; j++)
                {
                    int index = 6 * (scaledWidth * i + j);
                    int position = (scaledWidth * i + j);

                    indecies.Add(position);
                    indecies.Add(position + scaledWidth + 1);
                    indecies.Add(position + 1);

                    indecies.Add(position);
                    indecies.Add(position + scaledWidth);
                    indecies.Add(position + scaledWidth + 1);
                }
            }

            textureCoordinates.Freeze();

            mesh.Positions = pointCollection;
            mesh.TextureCoordinates = textureCoordinates;
            mesh.TriangleIndices = indecies;

            //Material material = new DiffuseMaterial(Brushes.CornflowerBlue);
            Material material = new DiffuseMaterial(new ImageBrush(bitmap));
            GeometryModel3D geoModel = new GeometryModel3D(mesh, material);

            ModelVisual3D visual = new ModelVisual3D();
            visual.Content = geoModel;

            mesh.Positions = null;

            for (int i = 0; i < scaledHeight; i++)
            {
                for (int j = 0; j < scaledWidth; j++)
                {
                    ushort rawDepth = depthData[(int)(width * (i / scale) + (j / scale))];
                    
                    // @todo how to upgrade this to Kinect v2?
                    //double depth = 5 - ((ushort)rawDepth >> DepthImageFrame.PlayerIndexBitmaskWidth) * depthScale;

                    // @note in the meantime...
                    double depth = 5 - maxDepth * depthScale;


                    //if (rawDepth < 0)
                    //depth = 5 - maxDepth * depthScale;

                    Point3D point = new Point3D(0.01 / scale * j, 0.01 / scale * (scaledHeight - i), depth);
                    pointCollection[scaledWidth * i + j] = point;
                }
            }

            mesh.Positions = pointCollection;
            #endregion

            this.Viewport3D.Children.Add(visual);
        }

        private void KinectImageViewer_Unloaded( object sender, RoutedEventArgs args )
        {

        }


        #region Viewport3D support
        private static readonly int width = 1920;
        private static readonly int height = 1080;
        private static readonly double scale = 1.0;
        private static readonly double depthThreshold = 2.0;

        private Point3DCollection pointCollection = new Point3DCollection((int)((width * scale) * (height * scale)));
        private Int32Collection indecies = new Int32Collection((int)((3 * (width * scale) * (width * scale) - 2 * (height * scale) - 2 * (width * scale) + 1) * 6));
        private PointCollection textureCoordinates = new PointCollection((int)((width * scale) * (height * scale)));
        private MeshGeometry3D mesh;
        private WriteableBitmap bitmap = new WriteableBitmap(width, height, 96, 96, PixelFormats.Bgra32, null);

        private byte[] colorData = new byte[width * height * 4];
        private ushort[] depthData = new ushort[width * height];

        private double depthScale = 0.0025;
        private short maxDepth = 0;

        // Variables for camera positioning
        double r = 4.0;
        double theta = 0.0;
        double phi = 0.0;
        Point3D lookAt = new Point3D(3.2, 2.4, 0.0);
        Point3D cameraOffset = new Point3D(0, 0, 4.0);   // offset from lookAt

        // Variables for mouse input
        Point mouseStart = new Point();
        int mouseWheelMin = 15000000;

        public KinectImageViewer()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();

                this.Loaded += KinectImageViewer_Loaded;
                this.Unloaded += KinectImageViewer_Unloaded;
            }
        }

        public void WipeClean()
        {
            this.setImages( new byte[width * height * 4], new ushort[width * height] );
        }

        public void ResetToInitialView()
        {
            depthScale = 0.0025;
            maxDepth = 0;

            // Variables for camera positioning
            r = 4.0;
            theta = 0.0;
            phi = 0.0;

            lookAt = new Point3D(3.2, 2.4, 0.0);
            cameraOffset = new Point3D(0, 0, 4.0);   // offset from lookAt

            this.Camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
            this.Camera.LookDirection = new Vector3D(lookAt.X - this.Camera.Position.X,
                                                        lookAt.Y - this.Camera.Position.Y,
                                                        lookAt.Z - this.Camera.Position.Z);
        }

        public void SetCurrentImages()
        {
            if (this.DisplayMode != DisplayModes.ColorDepth3D)
                return;

            if (this.KinectImage == null)
            {
                this.setImages(null, null);
            }
            else
            {
                this.setImages(this.KinectImage.ColorData, this.KinectImage.DepthData);
            }
        }

        /// <summary>
        /// Takes a color and depth frame and builds the scene
        /// </summary>
        /// <param name="colorData"></param>
        /// <param name="depthData"></param>
        public void setImages(byte[] colorData, ushort[] depthData)
        {
            this.colorData = colorData;
            this.depthData = depthData;

            int scaledWidth = (int)(width * scale);
            int scaledHeight = (int)(height * scale);

            bitmap.WritePixels(new Int32Rect(0, 0, width, height), colorData == null ? new byte[width * height] : colorData, width * 4, 0);    // set the bitmap which is used as the mesh's material.

            //Image2DImage.Background = new ImageBrush(bitmap);

            // Set the positions of the vertecies
            mesh.Positions = null;  // Break the tie between the pointCollection and the (in-use)mesh, so that we can update the pointCollection, without effecting the in-use mesh until our entire collection is calculated.

            for (int i = 0; i < scaledHeight; i++)
            {
                for (int j = 0; j < scaledWidth; j++)
                {
                    try
                    {
                        int depthIndex = (int)(width * (i / scale) + (j / scale));

                        double depth = 0;

                        if (depthData != null && depthData.Length > depthIndex)
                        {
                            ushort rawDepth = depthData[depthIndex];

                            // @todo how to upgrade this to Kinect v2?
                            //depth = 5 - ((ushort)rawDepth >> DepthImageFrame.PlayerIndexBitmaskWidth) * depthScale;

                            // @note in the meantime...
                            depth = 5 - maxDepth * depthScale;

                        }
                        //if (rawDepth < 0)
                        //depth = 5 - maxDepth * depthScale;

                        Point3D point = new Point3D(0.01 / scale * j, 0.01 / scale * (scaledHeight - i), depth);

                        pointCollection[scaledWidth * i + j] = point;
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine(e.ToString());
                    }
                }
            }

            mesh.Positions = pointCollection;   // Re-attach the tie between Positions and pointCollection, updating the model.

            // Build out the triangles in the mesh
            mesh.TriangleIndices = null;
            indecies.Clear();

            for (int i = 0; i < scaledHeight - 1; i++)
            {
                for (int j = 0; j < scaledWidth - 1; j++)
                {
                    try
                    {
                        int index = 6 * (scaledWidth * i + j);
                        int position = (scaledWidth * i + j);

                        bool depthIsClose = true;

                        // Make sure the depths are all similar for the first triangle
                        depthIsClose = (System.Math.Abs((pointCollection[position].Z - pointCollection[position + 1].Z)) < depthThreshold) &&
                            (System.Math.Abs((pointCollection[position].Z - pointCollection[position + scaledWidth + 1].Z)) < depthThreshold);

                        if (depthIsClose)
                        {
                            indecies.Add(position);
                            indecies.Add(position + scaledWidth + 1);
                            indecies.Add(position + 1);
                        }
                        else
                        {
                            indecies.Add(position);
                            indecies.Add(position);
                            indecies.Add(position);
                        }

                        // Make sure all the depths are similar for the 2nd triangle
                        depthIsClose = (System.Math.Abs((pointCollection[position].Z - pointCollection[position + scaledWidth].Z)) < depthThreshold) &&
                            (System.Math.Abs((pointCollection[position].Z - pointCollection[position + scaledWidth + 1].Z)) < depthThreshold);

                        if (depthIsClose)
                        {
                            indecies.Add(position);
                            indecies.Add(position + scaledWidth);
                            indecies.Add(position + scaledWidth + 1);
                        }
                        else
                        {
                            indecies.Add(position);
                            indecies.Add(position);
                            indecies.Add(position);
                        }

                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine(e.ToString());
                    }
                }
            }

            mesh.TriangleIndices = indecies;


        }

        #region Input Events

        private void ViewportCanvas_KeyDown(object sender, KeyEventArgs e)
        {
            if (this.Image2DImage.IsVisible)
                return;

            Key key = e.Key;

            double R;       // the radius of the circle cut along the xz plane
            switch (key)
            {
                case Key.Right:
                    // Orbit right
                    theta += 0.01;

                    //Find the Y value
                    cameraOffset.Y = r * System.Math.Sin(phi);

                    // Find the radius along the xz plane at the specified Y
                    R = r * System.Math.Cos(phi);

                    // Find the X and Z values
                    cameraOffset.X = R * System.Math.Sin(theta);
                    cameraOffset.Z = R * System.Math.Cos(theta);

                    this.Camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                    this.Camera.LookDirection = new Vector3D(lookAt.X - this.Camera.Position.X, lookAt.Y - this.Camera.Position.Y, lookAt.Z - this.Camera.Position.Z);

                    break;
                case Key.Left:
                    // Orbit left
                    theta -= 0.01;

                    //Find the Y value
                    cameraOffset.Y = r * System.Math.Sin(phi);

                    // Find the radius along the xz plane at the specified Y
                    R = r * System.Math.Cos(phi);

                    // Find the X and Z values
                    cameraOffset.X = R * System.Math.Sin(theta);
                    cameraOffset.Z = R * System.Math.Cos(theta);

                    this.Camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                    this.Camera.LookDirection = new Vector3D(lookAt.X - this.Camera.Position.X, lookAt.Y - this.Camera.Position.Y, lookAt.Z - this.Camera.Position.Z);

                    break;
                case Key.Up:
                    /*
                    // Orbit up
                    phi += 0.01;

                    // Clamp phi between -pi/2 and pi/2
                    phi = Math.Min(Math.PI / 2 - 0.001, phi);
                    phi = Math.Max(-Math.PI / 2 + 0.001, phi);

                    //Find the Y value
                    cameraOffset.Y = r * Math.Sin(phi);

                    // Find the radius along the xz plane at the specified Y
                    R = r * Math.Cos(phi);

                    // Find the X and Z values
                    cameraOffset.X = R * Math.Sin(theta);
                    cameraOffset.Z = R * Math.Cos(theta);
                    
                    this.Camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                    this.Camera.LookDirection = new Vector3D(lookAt.X - this.Camera.Position.X,
                                                                lookAt.Y - this.Camera.Position.Y,
                                                                lookAt.Z - this.Camera.Position.Z);
                     */
                    depthScale += 0.001;

                    break;
                case Key.Down:
                    // Orbit down
                    phi -= 0.01;

                    // Clamp phi between -pi/2 and pi/2
                    phi = System.Math.Min(System.Math.PI / 2 - 0.001, phi);
                    phi = System.Math.Max(-System.Math.PI / 2 + 0.001, phi);

                    //Find the Y value
                    cameraOffset.Y = r * System.Math.Sin(phi);

                    // Find the radius along the xz plane at the specified Y
                    R = r * System.Math.Cos(phi);

                    // Find the X and Z values
                    cameraOffset.X = R * System.Math.Sin(theta);
                    cameraOffset.Z = R * System.Math.Cos(theta);

                    this.Camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                    this.Camera.LookDirection = new Vector3D(lookAt.X - this.Camera.Position.X, lookAt.Y - this.Camera.Position.Y, lookAt.Z - this.Camera.Position.Z);

                    break;
                case Key.Space:
                    // Reset the camera to the start position
                    r = 10.0;
                    theta = 0.0;
                    phi = 0.0;
                    lookAt.X = 3.2;
                    lookAt.Y = 2.4;
                    lookAt.Z = 0.0;

                    cameraOffset.X = 0.0;
                    cameraOffset.Y = 0.0;
                    cameraOffset.Z = 10.0;

                    this.Camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                    this.Camera.LookDirection = new Vector3D(lookAt.X - this.Camera.Position.X,
                                                                lookAt.Y - this.Camera.Position.Y,
                                                                lookAt.Z - this.Camera.Position.Z);

                    break;
            }
        }
        
        private void ViewportCanvas_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (this.Image2DImage.IsVisible)
                return;

            mouseStart = e.GetPosition(null);
        }

        private void ViewportCanvas_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (this.Image2DImage.IsVisible)
                return;

            if (System.Math.Abs(e.Delta) < mouseWheelMin && e.Delta != 0)
                mouseWheelMin = System.Math.Abs(e.Delta);

            int ticks = e.Delta / mouseWheelMin;

            Vector3D cameraDirection = new Vector3D(cameraOffset.X, cameraOffset.Y, cameraOffset.Z);
            cameraDirection.Normalize();

            // Make sure that the radius will stay above 0
            // Distance traveled 
            if (r - (ticks * 0.1 * 1.732) > 0.1)
            {
                cameraOffset.X -= (0.1 * ticks) * cameraDirection.X;
                cameraOffset.Y -= (0.1 * ticks) * cameraDirection.Y;
                cameraOffset.Z -= (0.1 * ticks) * cameraDirection.Z;


                // Find the new radius
                r = System.Math.Sqrt(cameraOffset.X * cameraOffset.X + cameraOffset.Y * cameraOffset.Y + cameraOffset.Z * cameraOffset.Z);
            }

            this.Camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
            this.Camera.LookDirection = new Vector3D(lookAt.X - this.Camera.Position.X,
                                                        lookAt.Y - this.Camera.Position.Y,
                                                        lookAt.Z - this.Camera.Position.Z);

            e.Handled = true;
        }

        private void ViewportCanvas_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (this.Image2DImage.IsVisible)
                return;

            Point currentPosition = e.GetPosition(null);
            Vector diff = new Vector(currentPosition.X - mouseStart.X, currentPosition.Y - mouseStart.Y);

            // If dragging with the left mouse button, orbit the camera
            if (e.LeftButton == MouseButtonState.Pressed &&
                e.RightButton == MouseButtonState.Released &&
                (System.Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                System.Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                mouseStart = currentPosition;

                phi += 0.005 * diff.Y;
                theta -= 0.005 * diff.X;

                // Clamp phi between -pi/2 and pi/2
                phi = System.Math.Min(System.Math.PI / 2 - 0.001, phi);
                phi = System.Math.Max(-System.Math.PI / 2 + 0.001, phi);

                //Find the Y value
                cameraOffset.Y = r * System.Math.Sin(phi);

                // Find the radius along the xz plane at the specified Y
                double R = r * System.Math.Cos(phi);

                // Find the X and Z values
                cameraOffset.X = R * System.Math.Sin(theta);
                cameraOffset.Z = R * System.Math.Cos(theta);

                this.Camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                this.Camera.LookDirection = new Vector3D(lookAt.X - this.Camera.Position.X,
                                                            lookAt.Y - this.Camera.Position.Y,
                                                            lookAt.Z - this.Camera.Position.Z);
            }
            // Pan the camera on right click or mouswheel click
            else if (e.LeftButton == MouseButtonState.Released &&
                (e.RightButton == MouseButtonState.Pressed ||
                e.MiddleButton == MouseButtonState.Pressed) &&
                (System.Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                System.Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                mouseStart = currentPosition;

                double deltaY = 0.005 * diff.Y;
                double deltaX = -0.005 * diff.X;

                // Vectors for determining which direction the pan should move the camera
                Vector3D viewDirection = new Vector3D(this.Camera.LookDirection.X, this.Camera.LookDirection.Y, this.Camera.LookDirection.Z);
                Vector3D upDirection = new Vector3D(0, 1, 0);
                Vector3D xDirection;
                Vector3D yDirection;

                // Normalize the view vector so all following vecotrs will be unit vectors
                viewDirection.Normalize();

                // Move along the camera's x direction
                xDirection = Vector3D.CrossProduct(viewDirection, upDirection);
                lookAt.X += xDirection.X * deltaX;
                lookAt.Y += xDirection.Y * deltaX;
                lookAt.Z += xDirection.Z * deltaX;

                // Move along the camera's y direction
                yDirection = Vector3D.CrossProduct(xDirection, viewDirection);
                lookAt.X += yDirection.X * deltaY;
                lookAt.Y += yDirection.Y * deltaY;
                lookAt.Z += yDirection.Z * deltaY;

                this.Camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                this.Camera.LookDirection = new Vector3D(lookAt.X - this.Camera.Position.X,
                                                            lookAt.Y - this.Camera.Position.Y,
                                                            lookAt.Z - this.Camera.Position.Z);
            }
        }

        #endregion
        #endregion

        /*
        private void Toggle2D3DButton_Click(object sender, RoutedEventArgs e)
        {
            if (Image2DImage.Visibility == System.Windows.Visibility.Visible)
            {
                Image2DImage.Visibility = System.Windows.Visibility.Hidden;
            }
            else
            {
                ResetToInitialView();
                Image2DImage.Visibility = System.Windows.Visibility.Visible;
            }
        }
        */

    }
}
