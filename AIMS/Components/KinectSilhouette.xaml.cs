﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for KinectSilhouette.xaml
    /// </summary>
    public partial class KinectSilhouette : UserControl
    {
        public WriteableBitmap Silhouette { get; set; }

        public KinectSilhouette()
        {
            InitializeComponent();

            this.Loaded += KinectSilhouette_Loaded;
            this.Unloaded += KinectSilhouette_Unloaded;
        }

        void KinectSilhouette_Unloaded(object sender, EventArgs e)
        {
            KinectManager.DepthDataReady -= KinectManager_DepthDataReady;
        }

        void KinectManager_DepthDataReady()
        {
            this.Silhouette = new KinectManager().Silhouette;
            this.SilhouetteImage.Source = this.Silhouette;
        }

        void KinectSilhouette_Loaded(object sender, EventArgs e)
        {
            KinectManager.DepthDataReady += KinectManager_DepthDataReady;
        }
    }
}