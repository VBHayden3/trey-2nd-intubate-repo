﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for KinectTilt.xaml
    /// </summary>
    public partial class KinectTilt : UserControl
    {
        public KinectTilt()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            // Setup slider values based on current sensor values.
            this.sldr_kinectAngle.Minimum = KinectManager.MinElevationAngle;
            this.sldr_kinectAngle.Maximum = KinectManager.MaxElevationAngle;

            
        }
    }
}
