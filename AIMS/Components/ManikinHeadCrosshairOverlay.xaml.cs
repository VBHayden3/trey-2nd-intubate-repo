﻿/****************************************************************************
 * 
 *  Class: ManikinOverlay
 *  Author: Mitchel Weate
 *  
 *  ManikinOverlay is used to paint tracked joints over the colored image.
 *  TrackedJoint is used to specify which joints should be drawn as well as
 *  how they should be drawn.  
 *   
 * ***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;
using System.Windows.Media.Media3D;
using AIMS;

namespace AIMS.Components
{
    /// <summary>
    /// Skeleton overlay will display specified joints for the skeleton that is passed in.
    /// </summary>
    public partial class ManikinHeadCrosshairOverlay : UserControl
    {
        int colorWidth = 1920;
        int colorHeight = 1080;
        int scale = 3;

        private bool showCrosshair = false;
        private bool showCrosshairText = false;
        private Point3D manikinHead3DPosition = new Point3D(0,0,0);

        public bool ShowCrosshair { get { return showCrosshair; } set { showCrosshair = value; UpdateManikinHeadCrosshair(); } }
        public bool ShowCrosshairText { get { return showCrosshairText; } set { showCrosshairText = value; UpdateManikinHeadCrosshair(); } }
        public Point3D ManikinHead3DPosition { get { return manikinHead3DPosition; } set { manikinHead3DPosition = value; UpdateManikinHeadCrosshair(); } }

        public static readonly DependencyProperty ShowCrosshairProperty =
            DependencyProperty.Register("ShowCrosshair", typeof(bool), typeof(ManikinHeadCrosshairOverlay), new PropertyMetadata(false, OverlaySettingChanged));

        public static readonly DependencyProperty ShowScanProperty =
            DependencyProperty.Register("ShowCrosshairText", typeof(bool), typeof(ManikinHeadCrosshairOverlay), new PropertyMetadata(false, OverlaySettingChanged));

        public static readonly DependencyProperty ManikinHead3DPositionProperty =
            DependencyProperty.Register("ManikinHead3DPosition", typeof(Point3D), typeof(ManikinHeadCrosshairOverlay), new PropertyMetadata(new Point3D(), OverlaySettingChanged));

        public ManikinHeadCrosshairOverlay()
        {
            InitializeComponent();
        }
        
        public void UpdateManikinHeadCrosshair( double x, double y )
        {
            manikinHead3DPosition.X = x;
            manikinHead3DPosition.Y = y;

            UpdateManikinHeadCrosshair();
        }

        public void UpdateManikinHeadCrosshair(double x, double y, double z)
        {
            // CameraSpacePoint
            manikinHead3DPosition.X = x;
            manikinHead3DPosition.Y = y;
            manikinHead3DPosition.Z = z;

            UpdateManikinHeadCrosshair();
        }

        public void UpdateManikinHeadCrosshair()
        {
            if (showCrosshair && !Double.IsNaN(manikinHead3DPosition.X) && !Double.IsNaN(manikinHead3DPosition.Y) && manikinHead3DPosition.X != 0 && manikinHead3DPosition.Y != 0)
            {
                // Z is in meters
                //double scaledCrosshairLength = (manikinHead3DPosition.Z == 0 ? 6 : (6 * manikinHead3DPosition.Z / 2)) * (this.ActualWidth / colorWidth) * scale;
                double scaledCrosshairLength = 3;

                // Convert the 3DPosition CameraSpace to 2DPosition ColorSpace
                CameraSpacePoint manikin = new CameraSpacePoint() { X = (float)manikinHead3DPosition.X, Y = (float)manikinHead3DPosition.Y, Z = (float)manikinHead3DPosition.Z / 1000 };
                ColorSpacePoint colorPoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(manikin);
                System.Diagnostics.Debug.WriteLine("ManikinHeadCrosshairOverlay-> using crosshair point (" + colorPoint.X + "," + colorPoint.Y + ") and z=" + manikinHead3DPosition.Z + "mm");

                line1.Visibility = Visibility.Visible;
                line2.Visibility = Visibility.Visible;

                line1.X1 = colorPoint.X * this.ActualWidth / colorWidth - scaledCrosshairLength;
                line1.X2 = colorPoint.X * this.ActualWidth / colorWidth + scaledCrosshairLength;
                line1.Y1 = colorPoint.Y * this.ActualHeight / colorHeight - scaledCrosshairLength;
                line1.Y2 = colorPoint.Y * this.ActualHeight / colorHeight + scaledCrosshairLength;

                line2.X1 = colorPoint.X * this.ActualWidth / colorWidth - scaledCrosshairLength;
                line2.X2 = colorPoint.X * this.ActualWidth / colorWidth + scaledCrosshairLength;
                line2.Y1 = colorPoint.Y * this.ActualHeight / colorHeight + scaledCrosshairLength;
                line2.Y2 = colorPoint.Y * this.ActualHeight / colorHeight - scaledCrosshairLength;
            }
            else
            {
                line1.Visibility = Visibility.Hidden;
                line2.Visibility = Visibility.Hidden;
            }

            if (showCrosshairText && !Double.IsNaN(manikinHead3DPosition.X) && !Double.IsNaN(manikinHead3DPosition.Y) && manikinHead3DPosition.X != 0 && manikinHead3DPosition.Y != 0)
            {
                crosshairPositionTextBlock.Visibility = Visibility.Visible;

                CameraSpacePoint manikin = new CameraSpacePoint() { X = (float)manikinHead3DPosition.X, Y = (float)manikinHead3DPosition.Y, Z = (float)manikinHead3DPosition.Z / 1000 };
                ColorSpacePoint colorPoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(manikin);

                crosshairPositionTextBlock.FontSize = 16 * (double)this.ActualHeight / colorHeight;
                crosshairPositionTextBlock.Text = String.Format("({0:###.#},{1:###.#})", colorPoint.X, colorPoint.Y);
                Canvas.SetLeft(crosshairPositionTextBlock, (colorPoint.X - (25 * scale)) * this.ActualWidth / colorWidth);
                Canvas.SetTop(crosshairPositionTextBlock, (colorPoint.Y - (25 * scale)) * this.ActualHeight / colorHeight);
            }
            else
            {
                crosshairPositionTextBlock.Visibility = Visibility.Hidden;
            }

            //this.InvalidateVisual();
        }

        // Converts the point from the dictionary into pixel coordinates so that it can be drawn.
        private Point Get2DPosition(RecZone zone)
        {
            Point point = new Point();

            ColorSpacePoint colorPoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(new CameraSpacePoint() { X = (float)zone.X, Y = (float)zone.Y, Z = (float)zone.Z / 1000 });

            point.X = (double)colorPoint.X / KinectManager.ColorWidth;
            point.Y = (double)colorPoint.Y / KinectManager.ColorHeight;

            point.X = point.X * this.RenderSize.Width;
            point.Y = point.Y * this.RenderSize.Height;

            System.Diagnostics.Debug.WriteLine("ManikinHeadCrosshairOverlay -> XY:" + String.Format("{0}{1}", point.X, point.Y) + " and z=" + zone.Z + "mm");
            return point;
        }

        private static void OverlaySettingChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            ManikinHeadCrosshairOverlay overlay = sender as ManikinHeadCrosshairOverlay;

            if (overlay != null)
            {
                overlay.UpdateManikinHeadCrosshair();
            }
        }
    }
}
