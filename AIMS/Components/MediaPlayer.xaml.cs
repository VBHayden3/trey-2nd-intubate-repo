﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIMS.Components
{
    public enum PlayStatus
    {
        Playing,
        Paused,
        Stopped
    }

    /// <summary>
    /// Interaction logic for MediaPlayer.xaml
    /// </summary>
    public partial class MediaPlayer : UserControl
    {
        public PlayStatus PlayStatus { get; set; }

        public Uri CurrentSourceUri { get { return this.MediaElement.Source; } set { this.MediaElement.Source = value; } }

        public MediaPlayer()
        {
            InitializeComponent();

            Loaded += MediaPlayer_Loaded;
            Unloaded += MediaPlayer_Unloaded;
        }

        private void MediaPlayer_Loaded( object sender, EventArgs e )
        {

        }

        private void MediaPlayer_Unloaded( object sender, EventArgs e )
        {

        }

        public void SetSourceUri(Uri sourceUri)
        {
            this.MediaElement.Source = sourceUri;
        }

        public void ResetVideo()
        {

        }

        public void PlayVideo(Uri sourceUri)
        {
            this.SetSourceUri( sourceUri );
            this.MediaElement.Play();
            this.PlayStatus = PlayStatus.Playing;
        }

        public void PauseVideo()
        {
            this.MediaElement.Pause();
            this.PlayStatus = PlayStatus.Paused;
        }

        public void StopVideo()
        {
            this.MediaElement.Stop();
            this.MediaElement.Close();
            this.PlayStatus = PlayStatus.Stopped;
        }

        public void PlayCurrentVideo()
        {
            this.MediaElement.Play();
        }

        private void HideVideoPlayer()
        {

        }

        private void ShowVideoPlayer()
        {

        }

        private void cmdPlayCode_Click(object sender, RoutedEventArgs e)
        {
            /*
            MediaTimeline timeline = new MediaTimeline(new Uri("test.mpg", UriKind.Relative));
            timeline.RepeatBehavior = System.Windows.Media.Animation.RepeatBehavior.Forever;

            MediaClock clock = timeline.CreateClock();
            MediaPlayer player = new MediaPlayer();
            player.Clock = clock;

            VideoDrawing videoDrawing = new VideoDrawing();
            videoDrawing.Rect = new Rect(150, 0, 100, 100);
            videoDrawing.Player = player;

            DrawingBrush brush = new DrawingBrush(videoDrawing);
            this.Background = brush;

            clock.Controller.Begin();*/
        }
    }
}
