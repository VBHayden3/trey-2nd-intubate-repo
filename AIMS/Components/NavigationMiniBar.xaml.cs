﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIMS.Components
{
    public partial class NavigationMiniBar : UserControl
    {
        NavigationService navigationService;

        public NavigationMiniBar()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();

                Loaded += NavigationBar_Loaded;
            }
        }

        private void NavigationBar_Loaded(object sender, EventArgs e)
        {
            Page page = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

            navigationService = page.NavigationService;
            navigationService.RemoveBackEntry();

            if (((App)(App.Current)).CurrentUserID == -1)
            {
                navigationService.Navigate(new Uri("../LoginPage.xaml", UriKind.Relative));
                return;
            }

            if (((App)App.Current).CurrentStudent != null || ((App)App.Current).CurrentDoctor != null || ((App)App.Current).CurrentAdministrator != null)
            {
                this.UserID.Text = String.Format("{0}{1} {2}", ((App)App.Current).CurrentUserAccessLevel == AccessLevel.Doctor ? "Dr. " : "", ((App)App.Current).CurrentUserFirstName, ((App)App.Current).CurrentUserLastName);
                this.UserOrganization.Text = ((App)App.Current).CurrentUserOrganizationName;
            }
            else
            {
                this.UserID.Text = "";
                this.UserOrganization.Text = "";
            }
        }

        private void LogoutButton_Click(object sender, RoutedEventArgs e)
        {
            this.Logout();
        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            this.GoHome();
        }

        /// <summary>
        /// Goes to the home dashboard based upon the current user's AccessLevel.
        /// </summary>
        public void GoHome()
        {
            if (((App)App.Current).CurrentAccount == null)
            {
                this.Logout();
                return;
            }
            switch (((App)App.Current).CurrentUserAccessLevel)
            {
                case AccessLevel.User:
                    {
                        //navigationService.Navigate(new Uri("../StudentDashboard.xaml", UriKind.Relative));
                        navigationService.Navigate(new Uri("../Pages/UserDashboard.xaml", UriKind.Relative));
                        break;
                    }
                case AccessLevel.Doctor:
                    {
                        navigationService.Navigate(new Uri("../DoctorDashboard.xaml", UriKind.Relative));
                        break;
                    }
                case AccessLevel.Administrator:
                    {
                        navigationService.Navigate(new Uri("../AdministratorDashboard.xaml", UriKind.Relative));
                        break;
                    }
            }
        }

        /// <summary>
        /// Get's the highest parent dependency object of the page - the page itself.
        /// </summary>
        /// <param name="startObject"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private DependencyObject GetDependencyObjectFromVisualTree(DependencyObject startObject, Type type)
        {
            DependencyObject parent = startObject;

            while (parent != null)
            {
                if (type.IsInstanceOfType(parent))
                    break;

                else
                    parent = VisualTreeHelper.GetParent(parent);
            }

            return parent;
        }

        /// <summary>
        /// Logs off the user, clearing the current student, doctor, admin, and account.
        /// Navigates the application to the Log-in screen.
        /// </summary>
        public void Logout()
        {
            ((App)(App.Current)).CurrentStudent = null;
            ((App)(App.Current)).CurrentDoctor = null;
            ((App)(App.Current)).CurrentAdministrator = null;
            ((App)(App.Current)).CurrentAccount = null;

            navigationService.Navigate(new Uri("../LoginPage.xaml", UriKind.Relative));
        }
    }
}
