﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Npgsql;

namespace AIMS.Components
{
    /// <summary>
    /// This component allows administrators and doctors to create new users and user accounts.
    /// 
    /// It will check the account if it already exists,
    /// It will check if there is already a user linked to this account,
    /// It will verify that all information is filled out before allowing the user to be created,
    /// It will create both an account and user (student, doctor, or admin) simultaneously (unless the acct, or user, already exists).
    /// </summary>
    public partial class NewUserOverlay : UserControl
    {
        Page parentPage;

        public NewUserOverlay()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();
                Loaded += NewUserOverlay_Loaded;
            }
        }

        private void NewUserOverlay_Loaded(object sender, EventArgs e)
        {
            parentPage = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

            this.Height = parentPage.ActualHeight;
            this.Width = parentPage.ActualWidth;
        }

        private void SubmitButton_Clicked(object sender, RoutedEventArgs e)
        {
            if (TryCreateUser())  // Attempt to change the password of the given user
            {
                // Password change successful
                this.CancelOverlay();   // Close the overlay
            }
        }

        public bool TryCreateUser()
        {
            if (VerifyNewUser()) // check to make sure all the feilds are right
            {
                if (CreateUser())  // submit the password change, returns true if successful
                {
                    return true;
                }
            }

            return false;
        }

        private bool VerifyNewUser()
        {
            if (String.IsNullOrWhiteSpace(this.FirstNameTextBox.Text))
            {
                this.ErrorMessageTextBlock.Text = "* Must fill out all fields.";
                return false;
            }

            if (String.IsNullOrWhiteSpace(this.LastNameTextBox.Text))
            {
                this.ErrorMessageTextBlock.Text = "* Must fill out all fields.";
                return false;
            }

            this.ErrorMessageTextBlock.Text = "All fields verified successfully.";
            return true;    // Everything checks out
        }

        private string GetNewUserFirstName()
        {
            return null;
        }

        private string GetNewUserLastName()
        {
            return null;
        }

        private int GetNewUserOrganizationID()
        {
            return 0;
        }

        private bool CheckAccountAlreadyExists()
        {
            DatabaseManager.OpenConnection();

            int id = -1;
            string username = this.AccountUsernameTextBox.Text;

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "SELECT uid FROM accounts WHERE username = :username;";
            command.Parameters.Add("username", NpgsqlTypes.NpgsqlDbType.Text).Value = username.ToLower();

            id = (int)command.ExecuteScalar();

            DatabaseManager.CloseConnection();

            if (id <= 0)
            {
                MessageBox.Show("Account Not Found");
                return false;
            }

            MessageBox.Show("An account already exists with username " + username);

            return true;
        }

        private bool CreateUser()
        {
            this.ErrorMessageTextBlock.Text = "Opening connection to the database.";
            DatabaseManager.OpenConnection();
            if( DatabaseManager.Connection.State == System.Data.ConnectionState.Open )
                this.ErrorMessageTextBlock.Text = "Database connection aquired.";

            this.ErrorMessageTextBlock.Text = "Creating User.";
            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            // The command updates the password, and also updates the flag in the DB so they don't need to update their password again.
            command.CommandText = "INSERT INTO :tableNameBasedOnAccessLevel ( accountid, organizationid, firstname, lastname ) VALUES ( :accountid, :organizationid, :firstname, :lastname ) ;";

            command.Parameters.Add("tableNameBasedOnAccessLevel", NpgsqlTypes.NpgsqlDbType.Text).Value = GetTableNameBasedOnAccessLevel();
            command.Parameters.Add("accountid", NpgsqlTypes.NpgsqlDbType.Integer ).Value = GetNewUserAccountID();
            command.Parameters.Add("organizationid", NpgsqlTypes.NpgsqlDbType.Integer).Value = GetNewUserOrganizationID();
            command.Parameters.Add("firstname", NpgsqlTypes.NpgsqlDbType.Text).Value = GetNewUserFirstName();
            command.Parameters.Add("lastname", NpgsqlTypes.NpgsqlDbType.Text).Value = GetNewUserLastName();

            int enq = command.ExecuteNonQuery();

            this.ErrorMessageTextBlock.Text = "Closing connection to database.";
            DatabaseManager.CloseConnection();

            if( DatabaseManager.Connection.State == System.Data.ConnectionState.Closed )
                this.ErrorMessageTextBlock.Text = "Database connection closed.";

            if ( enq != 0)  // commit the update
            {
                this.ErrorMessageTextBlock.Text = "User created successfully.";
                return true;
            }
            else
            {
                this.ErrorMessageTextBlock.Text = "* Password not updated.";
                return false;
            }
        }
        
        private void CancelButton_Clicked(object sender, RoutedEventArgs e)
        {
            CancelOverlay();
        }

        private string GetTableNameBasedOnAccessLevel()
        {
            AccessLevel level = AccessLevel.User;

            // Get AccessLevel from DropBox.

            switch (level)
            {
                case AccessLevel.User:
                    return "students";
                case AccessLevel.Doctor:
                    return "doctors";
                case AccessLevel.Administrator:
                    return "administrators";
            }

            return null;
        }

        private int GetNewUserAccountID()
        {
            int id = 0;


            return id;
        }

        public void CancelOverlay()
        {
            LoginPage lp = parentPage as LoginPage;

            if (lp != null)
            {
                lp.BlurEffect.Radius = 0;   // remove the blur effect
                lp.userNameBox.IsEnabled = true;    // re-enable the username text box
                lp.SignInButton.IsEnabled = true;   // re-enable the sign-in button
                lp.passwordBox.IsEnabled = true;    // re-enable the password box
                lp.userNameBox.Focus(); // focus on the username text box
            }

            // unset all account/current user feilds
            ((App)App.Current).CurrentAccount = null;
            ((App)App.Current).CurrentAdministrator = null;
            ((App)App.Current).CurrentDoctor = null;
            ((App)App.Current).CurrentStudent = null;

            //this.ResetInputFields();
            this.ErrorMessageTextBlock.Text = "";
            this.IsEnabled = false;
            this.Visibility = Visibility.Hidden;

            parentPage.NavigationService.Navigate(new System.Uri(@"../LoginPage.xaml", System.UriKind.Relative));
        }

        private DependencyObject GetDependencyObjectFromVisualTree(DependencyObject startObject, Type type)
        {
            DependencyObject parent = startObject;

            while (parent != null)
            {
                if (type.IsInstanceOfType(parent))
                    break;

                else
                    parent = VisualTreeHelper.GetParent(parent);
            }

            return parent;
        }
    }
}
