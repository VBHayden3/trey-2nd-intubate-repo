﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Microsoft.Kinect;
using AIMS.Assets.Lessons2;

namespace AIMS.Components
{
    /// <summary>
    /// The Overlay Toggle overlay is used to toggle options on and off for the other Display overlays.
    /// </summary>
    public partial class OverlayToggleOverlay : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        // Private variables
        private bool isMinimized = false;
        private bool showBones = false;
        private bool showJoints = false;
        private bool showJointText = false;
        private bool showColorScan = false;
        private bool showCrosshair = false;
        private bool showCrosshairText = false;
        private bool showZoneDisplay = false;
        private bool showZoneText = false;
        private AIMS.Components.Display2.DisplayModes displayMode = Display2.DisplayModes.MappedColor;        

        // Public properties
        public bool IsMinimized
        {
            get
            {
                return isMinimized;
            }
            set
            {
                if (value != isMinimized)
                {
                    MinimizeOverlayToggle();
                }
            }
        }
        public bool ShowBones
        {
            get
            {
                return showBones;
            }
            set
            {
                if (value != showBones)
                {
                    ShowSkeletalLinesToggle();
                }
            }
        }
        public bool ShowJoints
        {
            get
            {
                return showJoints;
            }
            set
            {
                if (value != showJoints)
                {
                    ShowSkeletalJointsToggle();
                }
            }
        }
        public bool ShowJointText
        {
            get
            {
                return showJointText;
            }
            set
            {
                if (value != showJointText)
                {
                    ShowJointTextToggle();
                }
            }
        }
        public bool ShowColorScan
        {
            get
            {
                return showColorScan;
            }
            set
            {
                if (value != showColorScan)
                {
                    ShowColorScanToggle();
                }
            }
        }
        public bool ShowCrosshair
        {
            get
            {
                return showCrosshair;
            }
            set
            {
                if (value != showCrosshair)
                {
                    ShowColorCrosshairToggle();
                }
            }
        }
        public bool ShowCrosshairText
        {
            get
            {
                return showCrosshairText;
            }
            set
            {
                if (value != showCrosshairText)
                {
                    ShowCrosshairTextToggle();
                }
            }
        }
        public bool ShowZoneDisplay
        {
            get
            {
                return showZoneDisplay;
            }
            set
            {
                if (value != showZoneDisplay)
                {
                    ToggleShowZonesDisplayToggle();
                }
            }
        }
        public bool ShowZoneText
        {
            get
            {
                return showZoneText;
            }
            set
            {
                if (value != showZoneText)
                {
                    ToggleShowZoneTextDisplayToggle();
                }
            }
        }

        public AIMS.Components.Display2.DisplayModes DisplayMode
        {
            get
            {
                return displayMode;
            }
            set
            {
                if (value != displayMode)
                {
                    ToggleMappedColorDisplayToggle();
                }
            }
        }
        
        public OverlayToggleOverlay()
        {
            InitializeComponent();

            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                this.Loaded += ToggleOverlay_Loaded;
                this.Unloaded += ToggleOverlay_Unloaded;
            }
        }

        /// <summary>
        /// Event that fires every time the overlay gets loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToggleOverlay_Loaded(object sender, EventArgs e)
        {
            
        }

        private void ToggleOverlay_Unloaded(object sender, EventArgs e)
        {
        }
        
        private void ShowSkeletalLines_Checked(object sender, RoutedEventArgs e)
        {
            ShowSkeletalLinesToggle();
        }

        private void ShowSkeletalLinesToggle()
        {
            // Change whether bones are shown
            showBones = !showBones;

            // Set the image for if the bones are to be displayed
            if (showBones)
            {
                ShowSkeletalLinesImage.Source = new BitmapImage(new Uri("/Assets/Graphics/Overlay Toggles/skeleton_lines.png", UriKind.RelativeOrAbsolute));
            }
            else
            {
                ShowSkeletalLinesImage.Source = new BitmapImage(new Uri("/Assets/Graphics/Overlay Toggles/skeleton_plain.png", UriKind.RelativeOrAbsolute));
            }

            NotifyPropertyChanged("ShowBones");
        }
        
        private void ShowSkeletalJoints_Checked(object sender, RoutedEventArgs e)
        {
            ShowSkeletalJointsToggle();
        }

        private void ShowSkeletalJointsToggle()
        {
            // Change whether joints are shown
            showJoints = !showJoints;

            // Set the image for if the joints are to be displayed
            if (showJoints)
            {
                ShowJointImage.Source = new BitmapImage(new Uri("/Assets/Graphics/Overlay Toggles/skeleton_joints.png", UriKind.RelativeOrAbsolute));
            }
            else
            {
                ShowJointImage.Source = new BitmapImage(new Uri("/Assets/Graphics/Overlay Toggles/skeleton_plain.png", UriKind.RelativeOrAbsolute));
            }

            NotifyPropertyChanged("ShowJoints");
        }

        private void ShowJointText_Checked(object sender, RoutedEventArgs e)
        {
            ShowJointTextToggle();
        }

        private void ShowJointTextToggle()
        {
            // Change whether show bones is on or off
            showJointText = !showJointText;

            // Set the image for if the bones are to be displayed
            if (showJointText)
            {
                JointTextImage.Source = new BitmapImage(new Uri("/Assets/Graphics/Overlay Toggles/skeleton_joint_text.png", UriKind.RelativeOrAbsolute));
            }
            else
            {
                JointTextImage.Source = new BitmapImage(new Uri("/Assets/Graphics/Overlay Toggles/skeleton_plain.png", UriKind.RelativeOrAbsolute));
            }

            NotifyPropertyChanged("ShowJointText");
        }
        
        private void ShowColorScan_Checked(object sender, RoutedEventArgs e)
        {
            ShowColorScanToggle();
        }

        private void ShowColorScanToggle()
        {
            // Change whether show bones is on or off
            showColorScan = !showColorScan;

            // Set the image for if the bones are to be displayed
            if (showColorScan)
            {
                ShowScanImage.Source = new BitmapImage(new Uri("/Assets/Graphics/Overlay Toggles/manikin_colorscan.png", UriKind.RelativeOrAbsolute));
            }
            else
            {
                ShowScanImage.Source = new BitmapImage(new Uri("/Assets/Graphics/Overlay Toggles/manikin_plain.png", UriKind.RelativeOrAbsolute));
            }

            NotifyPropertyChanged("ShowColorScan");
        }
        
        private void ShowColorCrosshair_Checked(object sender, RoutedEventArgs e)
        {
            ShowColorCrosshairToggle();
        }

        private void ShowColorCrosshairToggle()
        {
            // Change whether show bones is on or off
            showCrosshair = !showCrosshair;

            // Set the image for if the bones are to be displayed
            if (showCrosshair)
            {
                CrossHairImage.Source = new BitmapImage(new Uri("/Assets/Graphics/Overlay Toggles/manikin_crosshair.png", UriKind.RelativeOrAbsolute));
            }
            else
            {
                CrossHairImage.Source = new BitmapImage(new Uri("/Assets/Graphics/Overlay Toggles/manikin_plain.png", UriKind.RelativeOrAbsolute));
            }

            NotifyPropertyChanged("ShowCrosshair");
        }

        private void ShowCrosshairText_Checked(object sender, RoutedEventArgs e)
        {
            ShowCrosshairTextToggle();
        }

        private void ShowCrosshairTextToggle()
        {
            // Change whether show bones is on or off
            showCrosshairText = !showCrosshairText;

            // Set the image for if the bones are to be displayed
            if (showCrosshairText)
            {
                CrossHairTextImage.Source = new BitmapImage(new Uri("/Assets/Graphics/Overlay Toggles/manikin_crosshair_text.png", UriKind.RelativeOrAbsolute));
            }
            else
            {
                CrossHairTextImage.Source = new BitmapImage(new Uri("/Assets/Graphics/Overlay Toggles/manikin_plain.png", UriKind.RelativeOrAbsolute));
            }

            NotifyPropertyChanged("ShowCrosshairText");
        }

        private void ToggleMappedColorDisplay_Checked(object sender, RoutedEventArgs e)
        {
            ToggleMappedColorDisplayToggle();
        }

        private void ToggleMappedColorDisplayToggle()
        {
            // Set the image for if the bones are to be displayed
            if (displayMode == Display2.DisplayModes.Color)
            {
                displayMode = Display2.DisplayModes.MappedColor;
                ToggleMappedColorDisplayImage.Source = new BitmapImage(new Uri("/Assets/Graphics/Overlay Toggles/display_color_mapped.png", UriKind.RelativeOrAbsolute));
            }
            else if (displayMode == Display2.DisplayModes.MappedColor)
            {
                displayMode = Display2.DisplayModes.HSV;
                ToggleMappedColorDisplayImage.Source = new BitmapImage(new Uri("/Assets/Graphics/Overlay Toggles/display_color_HSV.png", UriKind.RelativeOrAbsolute));
            }
            else if (displayMode == Display2.DisplayModes.HSV)
            {
                displayMode = Display2.DisplayModes.Color;
                ToggleMappedColorDisplayImage.Source = new BitmapImage(new Uri("/Assets/Graphics/Overlay Toggles/display_color_actual.png", UriKind.RelativeOrAbsolute));
            }

            NotifyPropertyChanged("DisplayMode");
        }

        private void ToggleShowZonesDisplay_Checked(object sender, RoutedEventArgs e)
        {
            ToggleShowZonesDisplayToggle();
        }

        private void ToggleShowZonesDisplayToggle()
        {
            // Change whether show bones is on or off
            showZoneDisplay = !showZoneDisplay;

            // Set the image for if the bones are to be displayed
            if (showZoneDisplay)
            {
                ToggleShowZonesDisplayImage.Source = new BitmapImage(new Uri("/Assets/Graphics/Overlay Toggles/zones_visible.png", UriKind.RelativeOrAbsolute));
            }
            else
            {
                ToggleShowZonesDisplayImage.Source = new BitmapImage(new Uri("/Assets/Graphics/Overlay Toggles/zones_hidden.png", UriKind.RelativeOrAbsolute));
            }

            NotifyPropertyChanged("ShowZoneDisplay");
        }

        private void ToggleShowZoneTextDisplay_Checked(object sender, RoutedEventArgs e)
        {
            ToggleShowZoneTextDisplayToggle();
        }

        private void ToggleShowZoneTextDisplayToggle()
        {
            // Change whether show bones is on or off
            showZoneText = !showZoneText;

            // Set the image for if the bones are to be displayed
            if (showZoneText)
            {
                ToggleShowZoneTextDisplayImage.Source = new BitmapImage(new Uri("/Assets/Graphics/Overlay Toggles/zones_text.png", UriKind.RelativeOrAbsolute));
            }
            else
            {
                ToggleShowZoneTextDisplayImage.Source = new BitmapImage(new Uri("/Assets/Graphics/Overlay Toggles/zones_visible.png", UriKind.RelativeOrAbsolute));
            }

            NotifyPropertyChanged("ShowZoneText");
        }

        private void MinimizeOverlay_Checked(object sender, RoutedEventArgs e)
        {
            MinimizeOverlayToggle();
        }

        private void MinimizeOverlayToggle()
        {
            isMinimized = !isMinimized;

            // if it is not minimized
            if (!isMinimized)
            {
                //MinimizeOverlay.IsChecked = true;
                MinimizeOverlayImage.Source = new BitmapImage(new Uri("/Assets/Graphics/Overlay Toggles/overlay_minimize.png", UriKind.RelativeOrAbsolute));

                ShowBonesCheckBox.Visibility = Visibility.Visible;
                ShowJointsCheckBox.Visibility = Visibility.Visible;
                ShowJointTextCheckBox.Visibility = Visibility.Visible;
                ShowColorScanCheckBox.Visibility = Visibility.Visible;
                ShowCrosshairCheckBox.Visibility = Visibility.Visible;
                ShowCrosshairTextCheckBox.Visibility = Visibility.Visible;
                MappedColorDisplayCheckBox.Visibility = Visibility.Visible;

                ShowBonesCheckBox.IsEnabled = true;
                ShowJointsCheckBox.IsEnabled = true;
                ShowJointTextCheckBox.IsEnabled = true;
                ShowColorScanCheckBox.IsEnabled = true;
                ShowCrosshairCheckBox.IsEnabled = true;
                ShowCrosshairTextCheckBox.IsEnabled = true;
                MappedColorDisplayCheckBox.IsEnabled = true;

                this.Width = 360;
            }
            else
            {
                //MinimizeOverlay.IsChecked = false;
                MinimizeOverlayImage.Source = new BitmapImage(new Uri("/Assets/Graphics/Overlay Toggles/overlay_maximize.png", UriKind.RelativeOrAbsolute));

                ShowBonesCheckBox.Visibility = Visibility.Hidden;
                ShowJointsCheckBox.Visibility = Visibility.Hidden;
                ShowJointTextCheckBox.Visibility = Visibility.Hidden;
                ShowColorScanCheckBox.Visibility = Visibility.Hidden;
                ShowCrosshairCheckBox.Visibility = Visibility.Hidden;
                ShowCrosshairTextCheckBox.Visibility = Visibility.Hidden;
                MappedColorDisplayCheckBox.Visibility = Visibility.Hidden;

                ShowBonesCheckBox.IsEnabled = false;
                ShowJointsCheckBox.IsEnabled = false;
                ShowJointTextCheckBox.IsEnabled = false;
                ShowColorScanCheckBox.IsEnabled = false;
                ShowCrosshairCheckBox.IsEnabled = false;
                ShowCrosshairTextCheckBox.IsEnabled = false;
                MappedColorDisplayCheckBox.IsEnabled = false;

                this.Width = 36;
            }

            NotifyPropertyChanged("IsMinimized");
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
