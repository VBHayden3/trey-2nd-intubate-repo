﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Npgsql;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for PasswordChangeOverlay.xaml
    /// </summary>
    public partial class PasswordChangeOverlay : UserControl
    {
        Page parentPage;

        public PasswordChangeOverlay()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();
                Loaded += PasswordChangeOverlay_Loaded;
            }
        }

        private void PasswordChangeOverlay_Loaded(object sender, EventArgs e)
        {
            parentPage = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

            this.Height = parentPage.ActualHeight;
            this.Width = parentPage.ActualWidth;
        }

        private void SubmitButton_Clicked(object sender, RoutedEventArgs e)
        {
            if (TryChangePassword())  // Attempt to change the password of the given user
            {
                // Password change successful
                this.CancelOverlay();   // Close the overlay
            }
        }

        public bool TryChangePassword()
        {
            if (VerifyPasswordChange()) // check to make sure all the feilds are right
            {
                if (ChangePassword(ConfirmPasswordPasswordBox.Password))  // submit the password change, returns true if successful
                {
                    return true;
                }
            }

            return false;
        }

        private void ResetInputFields()
        {
            this.CurrentPasswordPasswordBox.Password = "";  // reset the current password text
            this.NewPasswordPasswordBox.Password = "";      // reset the new password text
            this.ConfirmPasswordPasswordBox.Password = "";  // reset the confirm password text
        }

        private bool VerifyPasswordChange()
        {
            if (!this.CurrentPasswordPasswordBox.Password.Equals(((App)App.Current).CurrentAccount.Password))
            {
                // Incorrect password
                this.ResetInputFields();    // reset all password text
                this.ErrorMessageTextBlock.Text = "* Incorrect password.";
                return false;
            }

            if (String.IsNullOrWhiteSpace(this.NewPasswordPasswordBox.Password))
            {
                // New password left blank
                this.NewPasswordPasswordBox.Password = "";      // reset new password text
                this.ConfirmPasswordPasswordBox.Password = "";  // reset confirm password text
                this.ErrorMessageTextBlock.Text = "* Complete all input fields.";
                return false;
            }

            if (String.IsNullOrWhiteSpace(this.ConfirmPasswordPasswordBox.Password))
            {
                // Confirm password left blank
                this.ConfirmPasswordPasswordBox.Password = "";  // reset confirm password text
                this.ErrorMessageTextBlock.Text = "* Complete all input fields.";
                return false;
            }
            
            if (!this.NewPasswordPasswordBox.Password.Equals(this.ConfirmPasswordPasswordBox.Password))
            {
                // Confirm password doesn't match new password
                this.ResetInputFields();    // reset all fields
                this.ErrorMessageTextBlock.Text = "* Confirmation password does not match.";
                return false;
            }

            if (this.CurrentPasswordPasswordBox.Password.Equals(this.NewPasswordPasswordBox.Password))
            {
                // New password is the same as the old password
                this.NewPasswordPasswordBox.Password = "";  // reset new password text  
                this.ConfirmPasswordPasswordBox.Password = "";  // reset confirm password text
                this.ErrorMessageTextBlock.Text = "* New password must differ from the current password.";
                return false;
            }

            this.ErrorMessageTextBlock.Text = "All fields verified successfully.";
            return true;    // Everything checks out
        }

        private bool ChangePassword( string password )
        {
            this.ErrorMessageTextBlock.Text = "Opening connection to the database.";
            DatabaseManager.OpenConnection();
            if( DatabaseManager.Connection.State == System.Data.ConnectionState.Open )
                this.ErrorMessageTextBlock.Text = "Database connection aquired.";

            this.ErrorMessageTextBlock.Text = "Updating password.";
            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            // The command updates the password, and also updates the flag in the DB so they don't need to update their password again.
            command.CommandText = "UPDATE accounts SET password = :password, changepasswordnextlogin = false WHERE uid = :accountID;";

            command.Parameters.Add("password", NpgsqlTypes.NpgsqlDbType.Text).Value = password;
            command.Parameters.Add("accountID", NpgsqlTypes.NpgsqlDbType.Integer ).Value = ((App)App.Current).CurrentAccount.ID;

            int enq = command.ExecuteNonQuery();

            this.ErrorMessageTextBlock.Text = "Closing connection to database.";
            DatabaseManager.CloseConnection();

            if( DatabaseManager.Connection.State == System.Data.ConnectionState.Closed )
                this.ErrorMessageTextBlock.Text = "Database connection closed.";

            if ( enq != 0)  // commit the update
            {
                this.ErrorMessageTextBlock.Text = "Password updated successfully.";
                return true;
            }
            else
            {
                this.ErrorMessageTextBlock.Text = "* Password not updated.";
                return false;
            }
        }
        
        private void CancelButton_Clicked(object sender, RoutedEventArgs e)
        {
            CancelOverlay();
        }

        public void CancelOverlay()
        {
            LoginPage lp = parentPage as LoginPage;

            if (lp != null)
            {
                lp.BlurEffect.Radius = 0;   // remove the blur effect
                lp.userNameBox.IsEnabled = true;    // re-enable the username text box
                lp.SignInButton.IsEnabled = true;   // re-enable the sign-in button
                lp.passwordBox.IsEnabled = true;    // re-enable the password box
                lp.userNameBox.Focus(); // focus on the username text box
            }

            // unset all account/current user feilds
            ((App)App.Current).CurrentAccount = null;
            ((App)App.Current).CurrentAdministrator = null;
            ((App)App.Current).CurrentDoctor = null;
            ((App)App.Current).CurrentStudent = null;

            this.ResetInputFields();
            this.ErrorMessageTextBlock.Text = "";
            this.IsEnabled = false;
            this.Visibility = Visibility.Hidden;

            parentPage.NavigationService.Navigate(new System.Uri(@"../LoginPage.xaml", System.UriKind.Relative));
        }

        private DependencyObject GetDependencyObjectFromVisualTree(DependencyObject startObject, Type type)
        {
            DependencyObject parent = startObject;

            while (parent != null)
            {
                if (type.IsInstanceOfType(parent))
                    break;

                else
                    parent = VisualTreeHelper.GetParent(parent);
            }

            return parent;
        }
    }
}
