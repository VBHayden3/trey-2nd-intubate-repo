﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for ProgressBar.xaml
    /// </summary>
    public partial class ProgressBar : UserControl
    {
        private int currentStage = 0;
        private int totalStages = 0;

        private Storyboard storyboard;
        private Storyboard flashStoryboard;
        private DoubleAnimation widthAnimation;
        private DoubleAnimation positionAnimation;
        private ColorAnimation darkAnimation;
        private ColorAnimation lightAnimation;
        private TranslateTransform transform;
        private bool isPulsing = false;

        public ProgressBar()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();
                
                transform = new TranslateTransform();
                this.RegisterName("Transform", transform);
                transform.X = 0;

                ProgressTriangle.RenderTransform = transform;
                TextArrow.RenderTransform = transform;
            }   
        }

        /// <summary>
        /// Sets how many stages there will be in whatever the progress is being measured for.
        /// </summary>
        /// <param name="stages">The total number of stages.</param>
        /// <param name="instructions">A string with the instructions that will be displayed</param>
        public void setStages(int stages, string instructions)
        {
            this.currentStage = 0;
            this.totalStages = stages;
            Instructions.Text = instructions;

            this.LayoutUpdated += layoutUpdated;

            // Set up the animations
            widthAnimation = new DoubleAnimation();
            widthAnimation.Name = "WidthAnimation";
            widthAnimation.From = 0.0;
            widthAnimation.To = this.ActualWidth;
            widthAnimation.Duration = new Duration(TimeSpan.FromSeconds(1.5));
            widthAnimation.AutoReverse = false;
            this.RegisterName(widthAnimation.Name, widthAnimation);

            positionAnimation = new DoubleAnimation();
            positionAnimation.Name = "PositionAnimation";
            positionAnimation.From = 0.0;
            positionAnimation.To = this.ActualWidth;
            positionAnimation.Duration = widthAnimation.Duration;
            positionAnimation.AutoReverse = false;
            this.RegisterName(positionAnimation.Name, positionAnimation);

            darkAnimation = new ColorAnimation();
            darkAnimation.Name = "DarkAnimation";
            darkAnimation.From = DarkProgressColor.Color;
            darkAnimation.To = Colors.LightGray;
            darkAnimation.Duration = new Duration(TimeSpan.FromSeconds(0.75));
            darkAnimation.AutoReverse = true;
            darkAnimation.RepeatBehavior = RepeatBehavior.Forever;
            this.RegisterName(darkAnimation.Name, darkAnimation);

            lightAnimation = new ColorAnimation();
            lightAnimation.Name = "LightAnimation";
            lightAnimation.From = LightProgressColor.Color;
            lightAnimation.To = Colors.White;
            lightAnimation.Duration = darkAnimation.Duration;
            lightAnimation.AutoReverse = true;
            lightAnimation.RepeatBehavior = RepeatBehavior.Forever;
            this.RegisterName(lightAnimation.Name, lightAnimation);

            storyboard = new Storyboard();

            storyboard.Children.Add(widthAnimation);
            Storyboard.SetTargetName(widthAnimation, ProgressRectangle.Name);
            Storyboard.SetTargetProperty(widthAnimation, new PropertyPath(Rectangle.WidthProperty));

            storyboard.Children.Add(positionAnimation);
            Storyboard.SetTargetName(positionAnimation, "Transform");
            Storyboard.SetTargetProperty(positionAnimation, new PropertyPath(TranslateTransform.XProperty));

            flashStoryboard = new Storyboard();

            flashStoryboard.Children.Add(darkAnimation);
            Storyboard.SetTargetName(darkAnimation, "DarkProgressColor");
            Storyboard.SetTargetProperty(darkAnimation, new PropertyPath(GradientStop.ColorProperty));

            flashStoryboard.Children.Add(lightAnimation);
            Storyboard.SetTargetName(lightAnimation, "LightProgressColor");
            Storyboard.SetTargetProperty(lightAnimation, new PropertyPath(GradientStop.ColorProperty));
        }
        
        private void layoutUpdated(object sender, EventArgs e)
        {
            // Add in marker for each stage
            double xOffset = TransparentRectangle.ActualWidth / (totalStages - 1); ;
            for (int i = 1; i < totalStages - 1; i++)
            {
                StreamGeometry geometry = new StreamGeometry();
                geometry.FillRule = FillRule.EvenOdd;

                using (StreamGeometryContext ctx = geometry.Open())
                {
                    ctx.BeginFigure(new Point(xOffset * i, 0), false, false);
                    ctx.LineTo(new Point(20 + xOffset * i, 30), true, false);
                    ctx.LineTo(new Point(0 + xOffset * i, 60), true, false);
                }
                geometry.Freeze();

                Path path = new Path();
                path.Stroke = (Brush)App.Current.Resources["TertiaryUIBrush"];
                path.StrokeThickness = 2;
                path.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                path.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                path.Data = geometry;

                MainGrid.Children.Add(path);
                Grid.SetColumn(path, 1);
            }
            
            this.LayoutUpdated -= layoutUpdated;
        }

        /// <summary>
        /// Call this function whenever the progress bar should advance to the next stage.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="currentStage"></param>
        /// <param name="instructions"></param>
        public void stageFinished(object sender, int currentStage, string instructions)
        {
            // Stage incremented
            this.currentStage = currentStage;
            Instructions.Text = instructions;

            // Make the bar stop flashing
            //isPulsing = false;
            flashStoryboard.Stop(this);
            LightProgressColor.Color = Colors.LightGreen;
            DarkProgressColor.Color = Colors.Green;

            //Set up the to and from properties for the animations
            widthAnimation.From = ProgressRectangle.Width;
            widthAnimation.To = TransparentRectangle.ActualWidth / (totalStages - 1) * currentStage;

            positionAnimation.From = transform.X;
            positionAnimation.To = TransparentRectangle.ActualWidth / (totalStages - 1) * currentStage;

            // Begin the animations
            storyboard.Begin(this);    
        }

        // The bar will start pulsing when this is called
        public void pulse(object sender)
        {
            if (!isPulsing)
            {
                flashStoryboard.Begin(this, true);
                isPulsing = true;
            }
        }
    }
}
