﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Security.Cryptography;
using System.IO;
using System.IO.IsolatedStorage;
using System.Web;
using System.Runtime.Serialization.Formatters.Binary;
using Npgsql;
using AIMS.Assets.Lessons2;
using System.ComponentModel;
using System.Windows.Controls.DataVisualization.Charting;
using AIMS.Tasks;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for ResultsTest.xaml
    /// </summary>
    public partial class ResultsTest : UserControl
    {
        private AIMS.Result result;
        //private int currentStageIndex = 0;

        private WriteableBitmap bitmap = new WriteableBitmap(1920, 1080, 96, 96, PixelFormats.Bgr32, null);

        public ResultsTest()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();

                //result = ResultGenerator.generateResult();

                result = new Result();


                /*ImageBorder.Background = new ImageBrush(bitmap);*/

                ShowResults(result);
            }
        }

        /// <summary>
        /// Converts the index passed into a stage score object within the current result.
        /// </summary>
        /// <param name="index"></param>
        public void ShowStageScoreInformation(int index)
        {
            if (result != null )
            {
                if( result.StageScores != null )
                {
                    if (index >= 0 && index < result.StageScores.Count)
                    {
                        ShowStageScoreInformation(result.StageScores[index]);
                    }
                    else
                    {
                        MessageBox.Show(String.Format("Index {0} is outside the bounds of the Result StageScore collection.", index));
                    }
                }
                else
                {
                    MessageBox.Show("The linked result consists of no stage score information.");
                }
            }
            else
            {
                MessageBox.Show("No result object has been linked to display.");
            }
        }

        /// <summary>
        /// Builds the result chart using values from the collection of StageScore objects it's passed.
        /// </summary>
        /// <param name="stageScoreList"></param>
        private void BuildSingleResultChart(List<StageScore> stageScoreList)
        {
            List<KeyValuePair<int, double>> values = new List<KeyValuePair<int, double>>(); // list of key value pairs (int = stage#, double = score )

            foreach (StageScore stageScore in stageScoreList)
            {
                values.Add(new KeyValuePair<int, double>(stageScore.StageNumber + 1, stageScore.Score * 100));
                ////MessageBox.Show(String.Format("New keyValuePair added: stage: {0}, score: {1}", stageScore.StageNumber + 1, stageScore.Score * 100));
            }

            try
            {
                (singleChart.Series[0] as DataPointSeries).ItemsSource = null;
                (singleChart.Series[0] as DataPointSeries).ItemsSource = values;
                if (values.Any())
                    this.xAxis.Maximum = values.Last().Key + 0.5;
                singleChart.UpdateLayout();

                if (values.Count > 0)
                    (singleChart.Series[0] as DataPointSeries).SelectedItem = values[0];

                ////MessageBox.Show("SinglesChart updated");
            }
            catch (Exception ex)
            {
                ////MessageBox.Show(ex.ToString());
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        /// <summary>
        /// Updates the Results Overlay information with data from the stagescore.
        /// </summary>
        /// <param name="stagescore"></param>
        public void ShowStageScoreInformation(StageScore stagescore)
        {
            if (stagescore != null)
            {
                if (stagescore.HasScreenshot)
                {
                    _3DViewer.setImages(stagescore.ScreenShot.colorData, stagescore.ScreenShot.depthData);
                    //bitmap.WritePixels(new Int32Rect(0, 0, 640, 480), stagescore.screenshot.colorData, 640 * 4, 0);
                }

                this.ScoreNumber.Text = String.Format("{0:##0}%", stagescore.Score * 100);
                this.TimeNumber.Text = String.Format("{0:#0.#} sec{1}", stagescore.TimeInStage.TotalSeconds, stagescore.TimeInStage.TotalSeconds == 1 ? "" : "s");
                this.StageName.Text = stagescore.StageName;
                this.StageDescription.Text = stagescore.Target;
                this.DetailsText.Text = String.IsNullOrWhiteSpace( stagescore.Result ) ? ""/*"No detailed information is available for this stage."*/ : stagescore.Result;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("StageScore is null");
            }
        }

        /// <summary>
        /// Cleans out the information being stored in the overlay.
        /// </summary>
        public void WipeInformationClean()
        {
            if (result != null)
            {
                result = null;
            }

            this.DetailsText.Text = ""; // clear the displayed details.
            this.TimeNumber.Text = "--:--:--";  // clear the displayed time.
            this.ScoreNumber.Text = "--";   // clear the displayed score.
            //this.bitmap = new WriteableBitmap(640, 480, 96, 96, PixelFormats.Bgr32, null);
            //ImageBorder.Background = new ImageBrush(bitmap);
        }

        /// <summary>
        /// Closes (Hides/Disables/Un-blurs) the Results Overlay.
        /// </summary>
        public void CloseResults( bool wipeInformation )
        {
            this.Visibility = System.Windows.Visibility.Hidden;
            this.IsEnabled = false;

            Page parentPage = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

            if (parentPage as IntubationPage != null)
            {
                IntubationPage tp = parentPage as IntubationPage;

                tp.BlurEffect.Radius = 0;
            }

            if (wipeInformation)
                WipeInformationClean();
        }

        /// <summary>
        /// Shows and enables the Results Overlay, blurring the background.
        /// </summary>
        /// <param name="result"></param>
        public void ShowResults(Result result)
        {
            this.result = result;

            if (this.result != null)
            {
                PopulateStageScoreButtonList(this.result);

                BuildSingleResultChart(result.StageScores);

                TimeSpan totalTime = TimeSpan.Zero;

                for (int i = 1; i < result.StageScores.Count; i++)
                {
                    totalTime += result.StageScores[i].TimeInStage;
                }

                this.TimeNumber.Text = String.Format("{0:#0.#} sec{1}", totalTime.TotalSeconds, totalTime.TotalSeconds == 1 ? "" : "s");
                this.ScoreNumber.Text = String.Format("{0:##0}%", result.TotalScore * 100);
            }

            this.Visibility = System.Windows.Visibility.Visible;
            this.IsEnabled = true;

            SubmitButton.IsEnabled = true;
            SubmitButton.Content = "Submit";    // reset submition text
            TryAgainButton.IsEnabled = true;
            ExitButton.IsEnabled = true;

            Page parentPage = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

            if (parentPage as IntubationPage != null)
            {
                IntubationPage tp = parentPage as IntubationPage;

                tp.BlurEffect.Radius = 25;
            }
        }

        /// <summary>
        /// Fills the button list with buttons representing each StageScore object from the Result object
        /// passed in as the parameter. If existing buttons exist, they will first be cleared out.
        /// </summary>
        /// <param name="result"></param>
        public void PopulateStageScoreButtonList( Result result )
        {
            // Clear out old buttons before re-populating StageScore button list.
            StepButtonsPanel.Items.Clear();

            // Repopulate the results stage button list from the passed in result.
            for (int i = 0; i < result.StageScores.Count; i++)
            {
                System.Windows.Controls.Button bttn = new Button();
                bttn.Uid = i.ToString();
                int score = (int)(100 * result.StageScores[i].Score);
                bttn.Content = result.StageScores[i].StageName.ToString() + " = " + score.ToString();
                StepButtonsPanel.Items.Add(bttn);
            }
        }

        /// <summary>
        /// Event triggered when the currently selected object on the column chart depicting the currently selected stage changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ColumnSeries_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ColumnSeries colseries = sender as ColumnSeries;

            if (e == null || sender == null || colseries == null || e.AddedItems == null)
                return;

            if (colseries != null)
            {
                if (e.AddedItems.Count > 0 && e.AddedItems[0] is KeyValuePair<int, double>)
                {
                    //deserializeScreenShotFromDatabase((KeyValuePair<int, double>)(e.AddedItems[0]));

                    ShowStageScoreInformation(((KeyValuePair<int, double>)(e.AddedItems[0])).Key - 1);

                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// Event handler attached to every StageScore button. When clicked, we get the StageScore object
        /// at the index of the button within the Result object's StageScore collection. We then populate 
        /// the Results Component with information from that StageScore object (Name, number, time, score, 
        /// scenario effect, etc).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void stageButton_Click(object sender, RoutedEventArgs e)
        {
            Button clicked = e.OriginalSource as Button;

            ShowStageScoreInformation( Int32.Parse( clicked.Uid ) );
        }

        /// <summary>
        /// Event triggered when the Exit button on the component is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitClicked(object sender, RoutedEventArgs e)
        {
            DependencyObject dependencyObject = this;
            Type type = typeof(Page);

            while (dependencyObject != null)
            {
                if (type.IsInstanceOfType(dependencyObject))
                    break;
                else
                    dependencyObject = VisualTreeHelper.GetParent(dependencyObject);
            }

            if (dependencyObject != null)
            {
                Page pg = (Page)dependencyObject;
                Uri uri = new Uri("../../MainPage3.xaml", UriKind.Relative);
                pg.NavigationService.Navigate(uri);
            }
        }

        /// <summary>
        /// Cycles up the visual tree from the "startObject" in search for a DependencyObject of type "type".
        /// </summary>
        /// <param name="startObject"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private DependencyObject GetDependencyObjectFromVisualTree(DependencyObject startObject, Type type)
        {
            DependencyObject parent = startObject;

            while (parent != null)
            {
                if (type.IsInstanceOfType(parent))
                    break;

                else
                    parent = VisualTreeHelper.GetParent(parent);
            }

            return parent;
        }

        /// <summary>
        /// Event which fires when the TryAgainButton on the component is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TryAgainClicked(object sender, RoutedEventArgs e)
        {
            Page parentPage = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

            if (parentPage as IntubationPage != null)
            {
                IntubationPage tp = parentPage as IntubationPage;
                tp.Lesson.ResetLesson();// Restart();
            }

            CloseResults(true);
        }

        /// <summary>
        /// Event triggered when the submit button on the component is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubmitClicked(object sender, RoutedEventArgs e)
        {
            SubmitButton.IsEnabled = false;
            SubmitButton.Content = "Submitting...";

            // Disable the TryAgainButton and ExitButton to be reenabled upon submission (otherwise this was causing a null reference crash before)
            this.ExitButton.IsEnabled = false;
            this.TryAgainButton.IsEnabled = false;

            if (((App)App.Current).CurrentAccount == null || ((App)App.Current).CurrentStudent == null || ((App)App.Current).CurrentUserID <= 0)
            {
                MessageBox.Show("You cannot submit results to the database unless you are logged in as a student.");
                return;
            }

            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                SerializeResultsAndSubmit();
            };

            worker.RunWorkerCompleted += delegate(object s, RunWorkerCompletedEventArgs args)
            {
                SubmitButton.Content = "Success";
                //Re-enable TryAgain and Exit buttons
                TryAgainButton.IsEnabled = true;
                ExitButton.IsEnabled = true;
            };

            worker.RunWorkerAsync();
            
            e.Handled = true;
        }

        /// <summary>
        /// Takes the result currently stored by the component, serializes it into binary data, and then submits it into the database.
        /// </summary>
        public void SerializeResultsAndSubmit()
        {
            /*
                // Testing Mode: Generate temporary results object

                this.result = null;
                this.result = ResultGenerator.generateResult();
             */

            if (this.result != null)
            {
                int resultID = 0;
                // Submit to database here.
                if (result.StageScores != null)
                {
                    BinaryFormatter binaryFormatter = new BinaryFormatter();

                    DateTime startTime = DateTime.Now;

                    DatabaseManager.OpenConnection();   // Open Database.

                    #region SerializingAndSubmittingToDB

                    resultID = WriteResultToTable(); // writes non-serialized Result object information to student_results table

                    for (int i = 0; i < result.StageScores.Count; i++) // loop through each stagescore, serialize the data, and submit it to the DB
                    {
                        MemoryStream stageScoresMemoryStream = new MemoryStream();

                        byte[] stageScoreByteArray = SerializationManager.ObjectToByteArray((StageScore)result.StageScores[i], stageScoresMemoryStream, binaryFormatter);

                        int stageScoreID = SerializeStageScoreToTable(stageScoreByteArray, resultID); // writes serialized StageScore object to student_results_stage_scores table, linking to the Result object in the student_results table.

                        StageScore.Screenshot screenshot = ((StageScore)result.StageScores[i]).ScreenShot;

                        if (screenshot != null)
                        {
                            MemoryStream screenshotsMemoryStream = new MemoryStream();

                            byte[] screenshotByteArray = SerializationManager.ObjectToByteArray(screenshot, screenshotsMemoryStream, binaryFormatter );

                            SerializeScreenshotsToTable(screenshotByteArray, stageScoreID);    // writes serialized Screenshot object to student_results_3dscreenshots table, linking to the StageScore object in the student_results_stage_scores table.

                            screenshotsMemoryStream.Close();    // close screenshot memory stream, so it isn't just sitting there with old data
                        }

                        stageScoresMemoryStream.Close();    // close stage score memory stream, so it isn't just sitting there with old data
                    }

                    #endregion SerializingAndSubmittingToDB

                    DatabaseManager.CloseConnection();  // Close Database

                    DateTime endTime = DateTime.Now;

                    TimeSpan span = endTime.Subtract(startTime);

                    #region Update Intubation Statsheet
                    LessonStatsheet statsheet = null;

                    switch( result.LessonType )
                    {
                        case LessonType.Intubation:
                        {
                            statsheet = ((App)App.Current).CurrentStudent.IntubationStatsheet; break;
                        }
                        case LessonType.CatheterInsertion:
                        {
                            statsheet = ((App)App.Current).CurrentStudent.CatheterInsertionStatsheet; break;
                        }
                        case LessonType.IVInsertion:
                        {
                            statsheet = ((App)App.Current).CurrentStudent.IVInsertionStatsheet; break;
                        }
                    }

                    if (statsheet != null)
                    {
                        statsheet.LogRun(this.result, resultID);    // updates the local intubation statsheet
                        statsheet.UpdateInDatabase();   // sends the information to the database.
                    }

                    #endregion
                    //MessageBox.Show( String.Format( "Serialization completed in {0} seconds.", span.TotalSeconds ) ); // Display output of time spent serializing.
                }
            }
        }

        /// <summary>
        /// Writes the result object to the database.
        /// </summary>
        /// <returns></returns>
        private int WriteResultToTable()
        {
            int pkey = -1;

            if (DatabaseManager.Connection.State == System.Data.ConnectionState.Open)
            {
                NpgsqlCommand sql = DatabaseManager.Connection.CreateCommand();

                sql.CommandType = System.Data.CommandType.Text;

                sql.CommandText = @"INSERT INTO student_results ( studentid, tasktype, taskmode, masterylevel, date, passed, totalscore, totaltime ) VALUES ( :studentid, :tasktype, :taskmode, :masterylevel, :date, :passed, :totalscore, :totaltime ) RETURNING uid;";

                sql.Parameters.Add("studentid", NpgsqlTypes.NpgsqlDbType.Integer).Value = ((App)App.Current).CurrentStudent.ID;
                sql.Parameters.Add("tasktype", NpgsqlTypes.NpgsqlDbType.Integer).Value = (int)((App)App.Current).CurrentTask;
                sql.Parameters.Add("taskmode", NpgsqlTypes.NpgsqlDbType.Integer).Value = (int)((App)App.Current).CurrentTaskMode;
                sql.Parameters.Add("masterylevel", NpgsqlTypes.NpgsqlDbType.Integer).Value = (int)((App)App.Current).CurrentMasteryLevel;
                sql.Parameters.Add("date", NpgsqlTypes.NpgsqlDbType.Timestamp).Value = DateTime.Now;
                sql.Parameters.Add("passed", NpgsqlTypes.NpgsqlDbType.Boolean).Value = result.Passed;
                sql.Parameters.Add("totalscore", NpgsqlTypes.NpgsqlDbType.Double).Value = result.TotalScore;
                sql.Parameters.Add("totaltime", NpgsqlTypes.NpgsqlDbType.Interval).Value = result.TotalTime;

                try
                {
                    pkey = (int)sql.ExecuteScalar();

                    if (pkey != -1)
                    {
                        Console.WriteLine("Result sucessfully serialized to database.");
                    }
                    else
                    {
                        MessageBox.Show("Error retreiving pkey of new result entry...");
                    }
                }
                catch (Exception except)
                {
                    System.Diagnostics.Debug.WriteLine(except.ToString());
                }
            }

            return pkey;
        }

        /// <summary>
        /// Takes the serialized array of stage score data and stores it into the database.
        /// </summary>
        /// <param name="serializedByteArray"></param>
        /// <param name="linkedID"></param>
        /// <returns></returns>
        private int SerializeStageScoreToTable(byte[] serializedByteArray, int linkedID)
        {
            int pkey = -1;

            if (DatabaseManager.Connection.State == System.Data.ConnectionState.Open)
            {
                NpgsqlCommand sql = DatabaseManager.Connection.CreateCommand();

                sql.CommandType = System.Data.CommandType.Text;

                sql.CommandText = @"INSERT INTO student_results_stage_scores ( resultid, stagescore ) VALUES ( @resultid, @stagescore ) RETURNING uid;";

                sql.Parameters.Add("@resultid", NpgsqlTypes.NpgsqlDbType.Integer).Value = linkedID;
                sql.Parameters.Add("@stagescore", /*NpgsqlTypes.NpgsqlDbType.Array |*/ NpgsqlTypes.NpgsqlDbType.Bytea).Value = serializedByteArray;

                try
                {
                    pkey = (int)sql.ExecuteScalar();

                    if (pkey != -1)
                    {
                        Console.WriteLine("Stage score successfully serialized to the database.");
                    }
                    else
                    {
                        MessageBox.Show("Error retreiving pkey of new stage score entry...");
                    }
                }
                catch (Exception except)
                {
                    //throw;
                    System.Diagnostics.Debug.WriteLine(except.ToString());
                }
            }

            return pkey;
        }

        /// <summary>
        /// Takes the serialized byte array of screenshot data and stores it into the database.
        /// </summary>
        /// <param name="serializedByteArray"></param>
        /// <param name="linkedID"></param>
        private void SerializeScreenshotsToTable(byte[] serializedByteArray, int linkedID)
        {
            if (DatabaseManager.Connection.State == System.Data.ConnectionState.Open)
            {
                NpgsqlCommand sql = DatabaseManager.Connection.CreateCommand();

                sql.CommandType = System.Data.CommandType.Text;

                sql.CommandText = @"INSERT INTO student_results_3dscreenshots ( stagescoreid, coloranddepthdata ) VALUES ( @stagescoreid, @coloranddepthdata );";

                sql.Parameters.Add("@stagescoreid", NpgsqlTypes.NpgsqlDbType.Integer).Value = linkedID;
                sql.Parameters.Add("@coloranddepthdata", /*NpgsqlTypes.NpgsqlDbType.Array |*/ NpgsqlTypes.NpgsqlDbType.Bytea).Value = serializedByteArray;

                try
                {
                    if (sql.ExecuteNonQuery() != 0 )
                    {
                        Console.WriteLine("Screenshot successfully submitted to the database");
                    }
                }
                catch (Exception except)
                {
                    //throw;
                    System.Diagnostics.Debug.WriteLine(except.ToString());
                }
            }
        }
    }
}
