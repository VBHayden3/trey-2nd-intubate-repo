﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for SPHBars.xaml
    /// </summary>
    public partial class SPHBars : UserControl
    {
        public SPHBars()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Sets the bars based on a score from 0.0 to 1.0
        /// </summary>
        /// <param name="val1">Vertical</param>
        /// <param name="val2">Horizontal</param>
        /// <param name="val3">Asymetrical</param>
        public void changeValues(double val1, double val2, double val3)
        {
            val1 = System.Math.Max(val1, 0.0);
            val1 = System.Math.Min(val1, 1.0);

            val2 = System.Math.Max(val2, 0.0);
            val2 = System.Math.Min(val2, 1.0);

            val3 = System.Math.Max(val3, 0.0);
            val3 = System.Math.Min(val3, 1.0);


            VerticalRectangle.Width = val1 * VerticalBorder.Width;
            HorizontalRectangle.Width = val2 * VerticalBorder.Width;
            AsymetricRectangle.Width = val3 * VerticalBorder.Width;

            colorRectangle(VerticalRectangle, val1);
            colorRectangle(HorizontalRectangle, val2);
            colorRectangle(AsymetricRectangle, val3);
        }

        public void colorRectangle(Rectangle rect, double value)
        {
            byte R = 0;
            byte G = 0;
            byte B = 0;

            if (value <= .1)
            {
                R = 255;
                G = 0;
            }
            else if (value >= .9)
            {
                R = 0;
                G = 255;
            }
            else
            {
                R = (byte)(-255.0 / .8 * value + 286.875);
                G = (byte)(255 - R);
            }
            SolidColorBrush brush = new SolidColorBrush();
            brush.Color = Color.FromRgb(R, G, B);
            rect.Fill = brush;
        }
    }
}
