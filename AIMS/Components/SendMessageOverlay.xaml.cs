﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Npgsql;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for SendMessageOverlay.xaml
    /// </summary>
    public partial class SendMessageOverlay : UserControl
    {
        private Page parentPage;

        private Doctor fromDoctor;
        private Student toStudent;
        private Course relatedCourse;

        public Page ParentPage { get { return parentPage; } }
        public Doctor FromDoctor { get { return fromDoctor; } set { fromDoctor = value; } }
        public Student ToStudent { get { return toStudent; } set { toStudent = value; } }
        public Course RelatedCourse { get { return relatedCourse; } set { relatedCourse = value; } }

        public SendMessageOverlay()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();

                Loaded += SendMessageOverlay_Loaded;
            }
        }

        private void SendMessageOverlay_Loaded(object sender, EventArgs e)
        {
            parentPage = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

            this.Height = parentPage.ActualHeight;
            this.Width = parentPage.ActualWidth;
        }

        private void SubmitButton_Clicked(object sender, RoutedEventArgs e)
        {
            if (TrySendMessage())  // Attempt to send the message to the database
            {
                this.CancelOverlay();   // Close the overlay
            }
            else
            {
                MessageBox.Show("The message failed to send.");
            }
        }

        public bool TrySendMessage()
        {
            if (VerifySendMessage()) // check to make sure all the feilds are right
            {
                if (SendMessage())  // submit the message to the database, returns true if successful
                {
                    return true;
                }
            }

            return false;
        }

        private void ResetInputFields()
        {
            this.BodyTextBox.Text = null;  // reset the body text
            this.SubjectTextBox.Text = null;      // reset the subject text
            this.ToTextBox.Text = (this.ToStudent == null ? null : String.Format( "{0} {1}", this.ToStudent.FirstName, this.ToStudent.LastName ) );
            this.FromTextBox.Text = (this.FromDoctor == null ? null : String.Format("{0} {1}", this.FromDoctor.FirstName, this.FromDoctor.LastName));
        }

        private void WipeOverlayClean()
        {
            this.FromDoctor = null;
            this.ToStudent = null;
            this.RelatedCourse = null;
            this.SubjectTextBox.Text = null;
            this.BodyTextBox.Text = null;
            this.ToTextBox.Text = null;
            this.FromTextBox.Text = null;
        }

        private bool VerifySendMessage()
        {
            if (this.toStudent == null)
            {
                MessageBox.Show("No student selected.");
                this.ErrorMessageTextBlock.Text = "No student selected.";
                return false;
            }

            if (this.fromDoctor == null)
            {
                MessageBox.Show("No doctor selected.");
                this.ErrorMessageTextBlock.Text = "No doctor selected.";
                return false;
            }

            if (this.relatedCourse == null)
            {
                MessageBox.Show("The message is not linked to any particular course.");
                this.ErrorMessageTextBlock.Text = "The message is not linked to any particular course.";
                return false;
            }

            if (String.IsNullOrWhiteSpace(this.BodyTextBox.Text))
            {
                MessageBox.Show("The message has no body.");
                this.ErrorMessageTextBlock.Text = "The message has no body.";
                return false;
            }

            if (String.IsNullOrWhiteSpace(this.SubjectTextBox.Text))
            {
                MessageBox.Show("The message has no subject.");
                this.ErrorMessageTextBlock.Text = "The message has no subject.";
                return false;
            }

            MessageBox.Show("All fields verified successfully.");
            this.ErrorMessageTextBlock.Text = "All fields verified successfully.";
            return true;    // Everything checks out
        }

        private bool SendMessage()
        {
            this.ErrorMessageTextBlock.Text = "Opening connection to the database.";
            DatabaseManager.OpenConnection();
            if( DatabaseManager.Connection.State == System.Data.ConnectionState.Open )
                this.ErrorMessageTextBlock.Text = "Database connection aquired.";

            this.ErrorMessageTextBlock.Text = "Sending Message.";
            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            // The command inserts the message into the database.
            command.CommandText = "INSERT INTO messages ( courseid, toid, fromid, subject, body, datesent, hasbeenread ) VALUES ( :courseid, :toid, :fromid, :subject, :body, :datesent, :hasbeenread )";

            command.Parameters.Add("courseid", NpgsqlTypes.NpgsqlDbType.Integer ).Value = RelatedCourse.ID;
            command.Parameters.Add("toid", NpgsqlTypes.NpgsqlDbType.Integer ).Value = ToStudent.ID;
            command.Parameters.Add("fromid", NpgsqlTypes.NpgsqlDbType.Integer ).Value = FromDoctor.ID;
            command.Parameters.Add("subject", NpgsqlTypes.NpgsqlDbType.Text ).Value = SubjectTextBox.Text;
            command.Parameters.Add("body", NpgsqlTypes.NpgsqlDbType.Text ).Value = BodyTextBox.Text;
            command.Parameters.Add("datesent", NpgsqlTypes.NpgsqlDbType.Timestamp ).Value = DateTime.Now;
            command.Parameters.Add("hasbeenread", NpgsqlTypes.NpgsqlDbType.Boolean ).Value = false;

            int enq = command.ExecuteNonQuery();

            this.ErrorMessageTextBlock.Text = "Closing connection to database.";
            DatabaseManager.CloseConnection();

            if( DatabaseManager.Connection.State == System.Data.ConnectionState.Closed )
                this.ErrorMessageTextBlock.Text = "Database connection closed.";

            if ( enq != 0)  // commit the submission
            {
                this.ErrorMessageTextBlock.Text = "Message sent successfully.";
                return true;
            }
            else
            {
                this.ErrorMessageTextBlock.Text = "* Message not sent.";
                return false;
            }
        }
        
        private void CancelButton_Clicked(object sender, RoutedEventArgs e)
        {
            CancelOverlay();
        }

        /// <summary>
        /// Closes the overlay, clearing feilds and 
        /// </summary>
        public void CancelOverlay()
        {
            this.ResetInputFields();
            this.ErrorMessageTextBlock.Text = "";
            this.IsEnabled = false;
            this.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Reveal the overlay, allowing the doctor to type a message to the toStudent about the relatedCourse.
        /// </summary>
        /// <param name="toStudent"></param>
        /// <param name="relatedCourse"></param>
        public void ShowOverlay(Student toStudent, Course relatedCourse )
        {
            this.ToStudent = toStudent;             // set the student
            this.RelatedCourse = relatedCourse;     // set the related course

            this.FromDoctor = ((App)App.Current).CurrentDoctor; // doctor is the one that's currently signed in

            this.Visibility = Visibility.Visible;   // show the overlay
            this.IsEnabled = true;                  // enable the overlay for mouse interaction
        }
        
        /// <summary>
        /// Walks up the object tree hierachy and get's the parent object.
        /// </summary>
        /// <param name="startObject"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private DependencyObject GetDependencyObjectFromVisualTree(DependencyObject startObject, Type type)
        {
            DependencyObject parent = startObject;

            while (parent != null)
            {
                if (type.IsInstanceOfType(parent))
                    break;

                else
                    parent = VisualTreeHelper.GetParent(parent);
            }

            return parent;
        }
    }
}
