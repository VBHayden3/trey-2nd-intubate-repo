﻿/****************************************************************************
 * 
 *  Class: SkeletonOverlay
 *  Author: Mitchel Weate
 *  
 *  SkeletonOverlay is used to paint tracked joints over the colored image.
 *  TrackedJoint is used to specify which joints should be drawn as well as
 *  how they should be drawn.  
 *   
 * ***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;

namespace AIMS.Components
{
    /// <summary>
    /// Skeleton overlay will display specified joints for the skeleton that is passed in.
    /// </summary>
    public partial class SkeletonOverlay : UserControl
    {
        public static readonly DependencyProperty ShowJointOverlaysProperty =
            DependencyProperty.Register("ShowJointOverlays", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(false, OverlaySettingChanged));

        public static readonly DependencyProperty ShowJointTextOverlaysProperty =
            DependencyProperty.Register("ShowJointTextOverlays", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(false, OverlaySettingChanged));

        public static readonly DependencyProperty ShowBoneOverlaysProperty =
            DependencyProperty.Register("ShowBoneOverlays", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(false, OverlaySettingChanged));
			
		public static readonly DependencyProperty ShowSkeletonIdentifierProperty = 
			DependencyProperty.Register("ShowSkeletonIdentifier", typeof( bool ), typeof( SkeletonOverlay), new PropertyMetadata( true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowClosestProperty =
            DependencyProperty.Register("ShowClosest", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowStickyProperty =
            DependencyProperty.Register("ShowSticky", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        #region Toggling Bone Display (Per Joint)
        public static readonly DependencyProperty ShowHeadToCenterShoulderBoneProperty =
            DependencyProperty.Register("ShowHeadToCenterShoulderBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowCenterShoulderToRightShoulderBoneProperty =
            DependencyProperty.Register("ShowCenterShoulderToRightShoulderBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowRightShoulderToRightElbowBoneProperty =
            DependencyProperty.Register("ShowRightShoulderToRightElbowBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowRightElbowToRightWristBoneProperty =
            DependencyProperty.Register("ShowRightElbowToRightWristBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowRightWristToRightHandBoneProperty =
            DependencyProperty.Register("ShowRightWristToRightHandBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowCenterShoulderToLeftShoulderBoneProperty =
            DependencyProperty.Register("ShowCenterShoulderToLeftShoulderBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowLeftShoulderToLeftElbowBoneProperty =
            DependencyProperty.Register("ShowLeftShoulderToLeftElbowBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowLeftElbowToLeftWristBoneProperty =
            DependencyProperty.Register("ShowLeftElbowToLeftWristBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowLeftWristToLeftHandBoneProperty =
            DependencyProperty.Register("ShowLeftWristToLeftHandBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowCenterShoulderToSpineBoneProperty =
            DependencyProperty.Register("ShowCenterShoulderToSpineBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowSpineToCenterHipBoneProperty =
            DependencyProperty.Register("ShowSpineToCenterHipBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowCenterHipToRightHipBoneProperty =
            DependencyProperty.Register("ShowCenterHipToRightHipBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowRightHipToRightKneeBoneProperty =
            DependencyProperty.Register("ShowRightHipToRightKneeBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowRightKneeToRightAnkleBoneProperty =
            DependencyProperty.Register("ShowRightKneeToRightAnkleBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowRightAnkleToRightFootBoneProperty =
            DependencyProperty.Register("ShowRightAnkleToRightFootBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowCenterHipToLeftHipBoneProperty =
            DependencyProperty.Register("ShowCenterHipToLeftHipBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowLeftHipToLeftKneeBoneProperty =
            DependencyProperty.Register("ShowLeftHipToLeftKneeBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowLeftKneeToLeftAnkleBoneProperty =
            DependencyProperty.Register("ShowLeftKneeToLeftAnkleBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowLeftAnkleToLeftFootBoneProperty =
            DependencyProperty.Register("ShowLeftAnkleToLeftFootBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public bool ShowHeadToCenterShoulderBone
        {
            get { return (bool)GetValue(ShowHeadToCenterShoulderBoneProperty); }
            set { SetValue(ShowHeadToCenterShoulderBoneProperty, value); }
        }

        public bool ShowCenterShoulderToRightShoulderBone
        {
            get { return (bool)GetValue(ShowCenterShoulderToRightShoulderBoneProperty); }
            set { SetValue(ShowCenterShoulderToRightShoulderBoneProperty, value); }
        }

        public bool ShowRightShoulderToRightElbowBone
        {
            get { return (bool)GetValue(ShowRightShoulderToRightElbowBoneProperty); }
            set { SetValue(ShowRightShoulderToRightElbowBoneProperty, value); }
        }

        public bool ShowRightElbowToRightWristBone
        {
            get { return (bool)GetValue(ShowRightElbowToRightWristBoneProperty); }
            set { SetValue(ShowRightElbowToRightWristBoneProperty, value); }
        }

        public bool ShowRightWristToRightHandBone
        {
            get { return (bool)GetValue(ShowRightWristToRightHandBoneProperty); }
            set { SetValue(ShowRightWristToRightHandBoneProperty, value); }
        }

        public bool ShowCenterShoulderToLeftShoulderBone
        {
            get { return (bool)GetValue(ShowCenterShoulderToLeftShoulderBoneProperty); }
            set { SetValue(ShowCenterShoulderToLeftShoulderBoneProperty, value); }
        }

        public bool ShowLeftShoulderToLeftElbowBone
        {
            get { return (bool)GetValue(ShowLeftShoulderToLeftElbowBoneProperty); }
            set { SetValue(ShowLeftShoulderToLeftElbowBoneProperty, value); }
        }

        public bool ShowLeftElbowToLeftWristBone
        {
            get { return (bool)GetValue(ShowLeftElbowToLeftWristBoneProperty); }
            set { SetValue(ShowLeftElbowToLeftWristBoneProperty, value); }
        }

        public bool ShowLeftWristToLeftHandBone
        {
            get { return (bool)GetValue(ShowLeftWristToLeftHandBoneProperty); }
            set { SetValue(ShowLeftWristToLeftHandBoneProperty, value); }
        }

        public bool ShowShoulderCenterToSpineBone
        {
            get { return (bool)GetValue(ShowCenterShoulderToSpineBoneProperty); }
            set { SetValue(ShowCenterShoulderToSpineBoneProperty, value); }
        }

        public bool ShowSpineToCenterHipBone
        {
            get { return (bool)GetValue(ShowSpineToCenterHipBoneProperty); }
            set { SetValue(ShowSpineToCenterHipBoneProperty, value); }
        }

        public bool ShowCenterHipToRightHipBone
        {
            get { return (bool)GetValue(ShowCenterHipToRightHipBoneProperty); }
            set { SetValue(ShowCenterHipToRightHipBoneProperty, value); }
        }

        public bool ShowRightHipToRightKneeBone
        {
            get { return (bool)GetValue(ShowRightHipToRightKneeBoneProperty); }
            set { SetValue(ShowRightHipToRightKneeBoneProperty, value); }
        }

        public bool ShowRightKneeToRightAnkleBone
        {
            get { return (bool)GetValue(ShowRightKneeToRightAnkleBoneProperty); }
            set { SetValue(ShowRightKneeToRightAnkleBoneProperty, value); }
        }

        public bool ShowRightAnkleToRightFootBone
        {
            get { return (bool)GetValue(ShowRightAnkleToRightFootBoneProperty); }
            set { SetValue(ShowRightAnkleToRightFootBoneProperty, value); }
        }

        public bool ShowCenterHipToLeftHipBone
        {
            get { return (bool)GetValue(ShowCenterHipToLeftHipBoneProperty); }
            set { SetValue(ShowCenterHipToLeftHipBoneProperty, value); }
        }

        public bool ShowLeftHipToLeftKneeBone
        {
            get { return (bool)GetValue(ShowLeftHipToLeftKneeBoneProperty); }
            set { SetValue(ShowLeftHipToLeftKneeBoneProperty, value); }
        }

        public bool ShowLeftKneeToLeftAnkleBone
        {
            get { return (bool)GetValue(ShowLeftKneeToLeftAnkleBoneProperty); }
            set { SetValue(ShowLeftKneeToLeftAnkleBoneProperty, value); }
        }

        public bool ShowLeftAnkleToLeftFootBone
        {
            get { return (bool)GetValue(ShowLeftAnkleToLeftFootBoneProperty); }
            set { SetValue(ShowLeftAnkleToLeftFootBoneProperty, value); }
        }
        #endregion

        private const double inferredBoneWidth = 0.50;
        private const double primaryBoneWidth = 2.00;
        private const double stickyBoneWidth = 1.75;
        private const double trackedBoneWidth = 1.25;

        public bool ShowJointOverlays
        {
            get { return (bool)GetValue(ShowJointOverlaysProperty); }
            set { SetValue(ShowJointOverlaysProperty, value); }
        }

        public bool ShowJointTextOverlays 
        {
            get { return (bool)GetValue(ShowJointTextOverlaysProperty); } 
            set { SetValue(ShowJointTextOverlaysProperty, value); } 
        }

        public bool ShowBoneOverlays 
        {
            get { return (bool)GetValue(ShowBoneOverlaysProperty); }
            set { SetValue(ShowBoneOverlaysProperty, value); } 
        }

		public bool ShowSkeletonIdentifier 
        {
            get { return (bool)GetValue(ShowSkeletonIdentifierProperty); }
            set { SetValue(ShowSkeletonIdentifierProperty, value); } 
        }

        public bool ShowClosest
        {
            get { return (bool)GetValue(ShowClosestProperty); }
            set { SetValue(ShowClosestProperty, value); }
        }

        public bool ShowSticky
        {
            get { return (bool)GetValue(ShowStickyProperty); }
            set { SetValue(ShowStickyProperty, value); }
        }
        
        private List<TrackedJoint> trackedJoints;

        public SkeletonOverlay()
        {
            InitializeComponent();

            trackedJoints = new List<TrackedJoint>();
        }
        
        public void UpdateSkeleton()
        {
            this.InvalidateVisual();
        }

        public readonly Brush primarySkeletonBrush = Brushes.Lime;
        public readonly Brush[] stickySkeletonBrushes = { Brushes.Coral, Brushes.OrangeRed, Brushes.LightCyan, Brushes.LightGoldenrodYellow, Brushes.LightCoral, Brushes.LightSeaGreen };
        public readonly Brush inferredBoneBrush = Brushes.LightGray;
        public readonly Brush trackedBoneBrush = Brushes.White;

        private void DrawBone( ref DrawingContext drawingContext, Joint a, Joint b, ref bool skeletonIsPrimary, ref bool skeletonIsSticky, ref int stickyIndex, ref double scale )
        {
            if (null == a || null == b)
                return;

            if (JointTrackingState.NotTracked == a.TrackingState || JointTrackingState.NotTracked == b.TrackingState )
                return;

            bool bothFullyTracked = (JointTrackingState.Tracked == a.TrackingState && JointTrackingState.Tracked == b.TrackingState);

            if (bothFullyTracked)
            {
                drawingContext.DrawLine(new Pen( ( ShowClosest && skeletonIsPrimary ) ? primarySkeletonBrush : ( ShowSticky && skeletonIsSticky ) ? stickySkeletonBrushes[stickyIndex] : inferredBoneBrush, (skeletonIsPrimary ? primaryBoneWidth : skeletonIsSticky ? stickyBoneWidth : trackedBoneWidth) * scale), Get2DPosition(a), Get2DPosition(b));
            }
            else
            {
                drawingContext.DrawLine(new Pen(inferredBoneBrush, inferredBoneWidth * scale), Get2DPosition(a), Get2DPosition(b));
            }
        }

        private void DrawBone(ref DrawingContext drawingContext, JointProfile a, JointProfile b, ref bool skeletonIsPrimary, ref bool skeletonIsSticky, ref int stickyIndex, ref double scale)
        {
            if (null == a || null == b)
                return;

            if (JointTrackingState.NotTracked == a.CurrentTrackingState || JointTrackingState.NotTracked == b.CurrentTrackingState)
                return;

            bool bothFullyTracked = (JointTrackingState.Tracked == a.CurrentTrackingState && JointTrackingState.Tracked == b.CurrentTrackingState);

            if (bothFullyTracked)
            {
                drawingContext.DrawLine(new Pen( ShowClosest && skeletonIsPrimary ? primarySkeletonBrush : ShowSticky && skeletonIsSticky ? stickySkeletonBrushes[stickyIndex] : trackedBoneBrush, (skeletonIsPrimary ? primaryBoneWidth : skeletonIsSticky ? stickyBoneWidth : trackedBoneWidth) * scale), Get2DPosition(a), Get2DPosition(b));
            }
            else
            {
                drawingContext.DrawLine(new Pen(inferredBoneBrush, inferredBoneWidth * scale), Get2DPosition(a), Get2DPosition(b));
            }
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            double scale = ActualWidth / 640;
            double scaledJointSize = 1.5 * scale;

            foreach (SkeletonProfile skeletonProfile in KinectManager.SkeletonProfiles)
            {
                if (skeletonProfile.CurrentTrackingState != SkeletonTrackingState.Tracked)
                    continue;   // Skip this skeletonProfile and move on to the next.

                bool skeletonIsSticky = false;
                bool skeletonIsClosest = false;

                int stickyIndex = 0;

                if (skeletonProfile == KinectManager.ClosestSkeletonProfile)
                {
                    skeletonIsClosest = true;
                }
                if (KinectManager.GetSkeletonProfile(skeletonProfile.CurrentTrackingID, true, out stickyIndex) == skeletonProfile)
                {
                    skeletonIsSticky = true;
                }

                if (ShowSkeletonIdentifier)
                {
                    if (skeletonProfile != null)
                    {
                        //drawingContext.DrawEllipse(Brushes.Red, null, Get2DPosition(skeletonProfile.CurrentPosition, true), scaledJointSize, scaledJointSize);
                        Point p = Get2DPosition(skeletonProfile.CurrentPosition, false);   // DOESN'T SCALE THIS POINT TO RENDER SIZE!
                        try
                        {
                            drawingContext.DrawText(new FormattedText(skeletonProfile.CurrentTrackingID.HasValue ? skeletonProfile.CurrentTrackingID.Value.ToString() : "", System.Globalization.CultureInfo.InvariantCulture, System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Regular, FontStretches.Normal), 10.0 * ActualHeight / 480, Brushes.GhostWhite), new Point((p.X - 3) * this.ActualWidth / 640, (p.Y - 15) * this.ActualHeight / 480));
                        }
                        catch
                        {
                        }
                    }
                }

                if (ShowBoneOverlays)
                {
                 //   skeletonProfile.JointProfiles.Find(o => o.JointType.Equals( JointType.AnkleLeft ));   // This means: "Get the JointProfile from the skeletonProfile's list of JointProfiles which fullfills the condition (it's jointType == the specified jointtype)."

                    if( ShowHeadToCenterShoulderBone )
                        DrawBone(ref drawingContext, skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.Head)), skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.ShoulderCenter)), ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if( ShowCenterShoulderToRightShoulderBone )
                        DrawBone(ref drawingContext, skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.ShoulderCenter)), skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.ShoulderRight)), ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if( ShowCenterShoulderToLeftShoulderBone )
                        DrawBone(ref drawingContext, skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.ShoulderCenter)), skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.ShoulderLeft)), ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if( ShowRightShoulderToRightElbowBone )
                        DrawBone(ref drawingContext, skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.ShoulderRight)), skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.ElbowRight)), ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if( ShowLeftShoulderToLeftElbowBone )
                        DrawBone(ref drawingContext, skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.ShoulderLeft)), skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.ElbowLeft)), ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if( ShowRightElbowToRightWristBone )
                        DrawBone(ref drawingContext, skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.ElbowRight)), skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.WristRight)), ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if( ShowLeftElbowToLeftWristBone )
                        DrawBone(ref drawingContext, skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.ElbowLeft)), skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.WristLeft)), ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if( ShowRightWristToRightHandBone )
                        DrawBone(ref drawingContext, skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.WristRight)), skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.HandRight)), ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if( ShowLeftWristToLeftHandBone )
                        DrawBone(ref drawingContext, skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.WristLeft)), skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.HandLeft)), ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if( ShowShoulderCenterToSpineBone )
                        DrawBone(ref drawingContext, skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.ShoulderCenter)), skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.Spine)), ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if( ShowSpineToCenterHipBone )
                        DrawBone(ref drawingContext, skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.Spine)), skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.HipCenter)), ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if( ShowCenterHipToRightHipBone )
                        DrawBone(ref drawingContext, skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.HipCenter)), skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.HipRight)), ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if( ShowCenterHipToLeftHipBone )
                        DrawBone(ref drawingContext, skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.HipCenter)), skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.HipLeft)), ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if( ShowRightHipToRightKneeBone )
                        DrawBone(ref drawingContext, skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.HipRight)), skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.KneeRight)), ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if( ShowLeftHipToLeftKneeBone )
                        DrawBone(ref drawingContext, skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.HipLeft)), skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.KneeLeft)), ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if( ShowRightKneeToRightAnkleBone )
                        DrawBone(ref drawingContext, skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.KneeRight)), skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.AnkleRight)), ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if( ShowLeftKneeToLeftAnkleBone )
                        DrawBone(ref drawingContext, skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.KneeLeft)), skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.AnkleLeft)), ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if( ShowRightAnkleToRightFootBone )
                        DrawBone(ref drawingContext, skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.AnkleRight)), skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.FootRight)), ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if( ShowLeftAnkleToLeftFootBone )
                        DrawBone(ref drawingContext, skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.AnkleLeft)), skeletonProfile.JointProfiles.Find(o => o.JointType.Equals(JointType.FootLeft)), ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                }

                if (ShowJointOverlays)
                {
                    foreach (JointProfile jointProfile in skeletonProfile.JointProfiles)
                    {
                        drawingContext.DrawEllipse(Brushes.Red, null, Get2DPosition(jointProfile, true), scaledJointSize, scaledJointSize);

                        Point p = Get2DPosition(jointProfile, false);   // DOESN'T SCALE THIS POINT TO RENDER SIZE!

                        if (jointProfile.MovementAnalysis != null)
                        {
                            MovementAnalysis analysis = jointProfile.MovementAnalysis;

                            if (analysis.RelativeDirection.X == DeltaX.Right)
                            {
                                drawingContext.DrawText(new FormattedText("→", System.Globalization.CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Regular, FontStretches.Normal), 10.0 * ActualHeight / 480, Brushes.Lime), new Point((p.X + 7) * this.ActualWidth / 640, (p.Y - 6) * this.ActualHeight / 480));
                            }
                            else if (analysis.RelativeDirection.X == DeltaX.Left)
                            {
                                drawingContext.DrawText(new FormattedText("←", System.Globalization.CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Regular, FontStretches.Normal), 10.0 * ActualHeight / 480, Brushes.Lime), new Point((p.X - 17) * this.ActualWidth / 640, (p.Y - 6) * this.ActualHeight / 480));
                            }

                            if (analysis.RelativeDirection.Y == DeltaY.Up)
                            {
                                drawingContext.DrawText(new FormattedText("↑", System.Globalization.CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Regular, FontStretches.Normal), 10.0 * ActualHeight / 480, Brushes.Lime), new Point((p.X - 3) * this.ActualWidth / 640, (p.Y - 15) * this.ActualHeight / 480));
                            }
                            else if (analysis.RelativeDirection.Y == DeltaY.Down)
                            {
                                drawingContext.DrawText(new FormattedText("↓", System.Globalization.CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Regular, FontStretches.Normal), 10.0 * ActualHeight / 480, Brushes.Lime), new Point((p.X - 3) * this.ActualWidth / 640, (p.Y + 5) * this.ActualHeight / 480));
                            }

                            if (analysis.RelativeDirection.Z == DeltaZ.Backward)
                            {
                                drawingContext.DrawText(new FormattedText("+", System.Globalization.CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Regular, FontStretches.Normal), 8.0 * ActualHeight / 480, Brushes.Cyan), new Point((p.X - 1) * this.ActualWidth / 640, (p.Y - 4) * this.ActualHeight / 480));
                            }
                            else if (analysis.RelativeDirection.Z == DeltaZ.Forward)
                            {
                                drawingContext.DrawText(new FormattedText("-", System.Globalization.CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Regular, FontStretches.Normal), 10.0 * ActualHeight / 480, Brushes.Cyan), new Point((p.X - 1) * this.ActualWidth / 640, (p.Y - 7) * this.ActualHeight / 480));
                            }
                        }
                    }

                    if (ShowJointTextOverlays)
                    {
                        foreach (JointProfile jointProfile in skeletonProfile.JointProfiles)
                        {
                            Point p = Get2DPosition(jointProfile, false);   // DOESN'T SCALE THIS POINT TO RENDER SIZE!
                            drawingContext.DrawText(new FormattedText(String.Format("{3} \r{4:0.###} \r({0:0.###},{1:0.###},{2:0.###})", (int)(jointProfile.CurrentPosition.X * 1000), (int)(jointProfile.CurrentPosition.Y * 1000), (int)(jointProfile.CurrentPosition.Z * 1000), jointProfile.JointType, String.Format("ColorXY: ({0:0.###},{1:0.###})", p.X, p.Y)), System.Globalization.CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Regular, FontStretches.Normal), 5.0 * ActualHeight / 480, Brushes.Lime), new Point((p.X - 10) * this.ActualWidth / 640, (p.Y - 10) * this.ActualHeight / 480));
                        }
                    }
                }
            }
        }

        public void ClearJoints()
        {
            trackedJoints.Clear();
        }

        public void AddJoint(TrackedJoint joint)
        {
            trackedJoints.Add(joint);
        }

        // Converts the point from the dictionary into pixel coordinates so that it can be drawn.
        private Point Get2DPosition(Point point)
        {
            point.X = point.X * this.RenderSize.Width;
            point.Y = point.Y * this.RenderSize.Height;

            return point;
        }

        private Point Get2DPosition(Joint joint)
        {
            return Get2DPosition(joint, true);
        }

        // Converts the point from the dictionary into pixel coordinates so that it can be drawn.
        private Point Get2DPosition(Joint joint, bool scaleToDisplaySize)
        {
            Point point = new Point();
            
            if (joint == null || KinectManager.DepthImageFrame == null)
                return point;

            //DepthImagePoint depthPoint = KinectManager.DepthImageFrame.MapFromSkeletonPoint(joint.Position);
            DepthImagePoint depthPoint = KinectManager.Kinect.CoordinateMapper.MapSkeletonPointToDepthPoint(joint.Position, KinectManager.DepthImageFrame.Format);

            point.X = (double)depthPoint.X / KinectManager.DepthImageFrame.Width;
            point.Y = (double)depthPoint.Y / KinectManager.DepthImageFrame.Height;

            point.X = point.X * 640;
            point.Y = point.Y * 480;

            if (scaleToDisplaySize)
            {
                point.X *= this.ActualWidth / 640;
                point.Y *= this.ActualHeight / 480;
            }

            return point;
        }

        private Point Get2DPosition(JointProfile jointprofile)
        {
            return Get2DPosition(jointprofile, true);
        }

        // Converts the point from the dictionary into pixel coordinates so that it can be drawn.
        private Point Get2DPosition(JointProfile jointprofile, bool scaleToDisplaySize)
        {
            Point point = new Point();

            if (jointprofile == null || KinectManager.DepthImageFrame == null)
                return point;

            //DepthImagePoint depthPoint = KinectManager.DepthImageFrame.MapFromSkeletonPoint(joint.Position);
            DepthImagePoint depthPoint = KinectManager.Kinect.CoordinateMapper.MapSkeletonPointToDepthPoint(jointprofile.CurrentPosition, KinectManager.DepthImageFrame.Format);

            point.X = (double)depthPoint.X / KinectManager.DepthImageFrame.Width;
            point.Y = (double)depthPoint.Y / KinectManager.DepthImageFrame.Height;

            point.X = point.X * 640;
            point.Y = point.Y * 480;

            if (scaleToDisplaySize)
            {
                point.X *= this.ActualWidth / 640;
                point.Y *= this.ActualHeight / 480;
            }

            return point;
        }

        private Point Get2DPosition(SkeletonPoint point)
        {
            return Get2DPosition(point, true);
        }

        // Converts the point from the dictionary into pixel coordinates so that it can be drawn.
        private Point Get2DPosition(SkeletonPoint skelepoint, bool scaleToDisplaySize)
        {
            Point point = new Point();

            if (KinectManager.DepthImageFrame == null)
                return point;

            //DepthImagePoint depthPoint = KinectManager.DepthImageFrame.MapFromSkeletonPoint(joint.Position);
            DepthImagePoint depthPoint = KinectManager.Kinect.CoordinateMapper.MapSkeletonPointToDepthPoint(skelepoint, KinectManager.DepthImageFrame.Format);

            point.X = (double)depthPoint.X / KinectManager.DepthImageFrame.Width;
            point.Y = (double)depthPoint.Y / KinectManager.DepthImageFrame.Height;

            point.X = point.X * 640;
            point.Y = point.Y * 480;

            if (scaleToDisplaySize)
            {
                point.X *= this.ActualWidth / 640;
                point.Y *= this.ActualHeight / 480;
            }

            return point;
        }

        private static void OverlaySettingChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            SkeletonOverlay overlay = sender as SkeletonOverlay;

            if (overlay != null)
            {
                overlay.UpdateSkeleton();
            }
        }
    }
}
