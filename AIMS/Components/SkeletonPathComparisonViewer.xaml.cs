﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for SkeletonPathComparisonViewer.xaml
    /// </summary>
    public partial class SkeletonPathComparisonViewer : UserControl, INotifyPropertyChanged
    {
        private SkeletonPathComparison skeletonPathComparison;
        public SkeletonPathComparison SkeletonPathComparison
        {
            get { return skeletonPathComparison; }
            set
            {
                skeletonPathComparison = value;
                NotifyPropertyChanged("SkeletonPathComparison");
            }
        }

        private Model3DGroup mGeometry = new Model3DGroup();
        private Model3DGroup mGeometryGP;
        private bool mDown;
        private Point mLastPos;
        private static bool rotateLock = false;

        public SkeletonPathComparisonViewer()
        {
            InitializeComponent();

            BuildSolid();

            this.camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
            this.camera.LookDirection = new Vector3D(lookAt.X - this.camera.Position.X, lookAt.Y - this.camera.Position.Y, lookAt.Z - this.camera.Position.Z);
        }

        private bool showFloor = true;
        public bool ShowFloor
        {
            get { return showFloor; }
            set
            {
                if (showFloor == value)
                    showFloor = value;
                else
                {
                    showFloor = value;
                    NotifyPropertyChanged("ShowFloor");
                }

                if (showFloor)
                {
                    ShowFloorMesh();
                }
                else
                {
                    HideFloorMesh();
                }
            }
        }


        #region Input Events


        private static readonly int width = 1920;
        private static readonly int height = 1080;
        private static readonly double scale = 1.0;
        private static readonly double depthThreshold = 2.0;

        private double depthScale = 0.0025;
        private short maxDepth = 0;

        // Variables for camera positioning
        double r = 4.0;
        double theta = 0.0;
        double phi = 0.0;

        Point3D lookAt = new Point3D(0, 0, 0);
        Point3D cameraOffset = new Point3D(0, 0, 5);   // offset from lookAt

        // Variables for mouse input
        Point mouseStart = new Point();
        int mouseWheelMin = 15000000;

        private void UserControl_KeyDown(object sender, KeyEventArgs e)
        {
            Key key = e.Key;

            double R;       // the radius of the circle cut along the xz plane
            switch (key)
            {
                case Key.Right:
                    // Orbit right
                    theta += 0.01;

                    //Find the Y value
                    cameraOffset.Y = r * System.Math.Sin(phi);

                    // Find the radius along the xz plane at the specified Y
                    R = r * System.Math.Cos(phi);

                    // Find the X and Z values
                    cameraOffset.X = R * System.Math.Sin(theta);
                    cameraOffset.Z = R * System.Math.Cos(theta);

                    this.camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                    this.camera.LookDirection = new Vector3D(lookAt.X - this.camera.Position.X, lookAt.Y - this.camera.Position.Y, lookAt.Z - this.camera.Position.Z);

                    break;
                case Key.Left:
                    // Orbit left
                    theta -= 0.01;

                    //Find the Y value
                    cameraOffset.Y = r * System.Math.Sin(phi);

                    // Find the radius along the xz plane at the specified Y
                    R = r * System.Math.Cos(phi);

                    // Find the X and Z values
                    cameraOffset.X = R * System.Math.Sin(theta);
                    cameraOffset.Z = R * System.Math.Cos(theta);

                    this.camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                    this.camera.LookDirection = new Vector3D(lookAt.X - this.camera.Position.X, lookAt.Y - this.camera.Position.Y, lookAt.Z - this.camera.Position.Z);

                    break;
                case Key.Up:
                    /*
                    // Orbit up
                    phi += 0.01;

                    // Clamp phi between -pi/2 and pi/2
                    phi = Math.Min(Math.PI / 2 - 0.001, phi);
                    phi = Math.Max(-Math.PI / 2 + 0.001, phi);

                    //Find the Y value
                    cameraOffset.Y = r * Math.Sin(phi);

                    // Find the radius along the xz plane at the specified Y
                    R = r * Math.Cos(phi);

                    // Find the X and Z values
                    cameraOffset.X = R * Math.Sin(theta);
                    cameraOffset.Z = R * Math.Cos(theta);
                    
                    this.Camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                    this.Camera.LookDirection = new Vector3D(lookAt.X - this.Camera.Position.X,
                                                                lookAt.Y - this.Camera.Position.Y,
                                                                lookAt.Z - this.Camera.Position.Z);
                     */
                    depthScale += 0.001;

                    break;
                case Key.Down:
                    // Orbit down
                    phi -= 0.01;

                    // Clamp phi between -pi/2 and pi/2
                    phi = System.Math.Min(System.Math.PI / 2 - 0.001, phi);
                    phi = System.Math.Max(-System.Math.PI / 2 + 0.001, phi);

                    //Find the Y value
                    cameraOffset.Y = r * System.Math.Sin(phi);

                    // Find the radius along the xz plane at the specified Y
                    R = r * System.Math.Cos(phi);

                    // Find the X and Z values
                    cameraOffset.X = R * System.Math.Sin(theta);
                    cameraOffset.Z = R * System.Math.Cos(theta);

                    this.camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                    this.camera.LookDirection = new Vector3D(lookAt.X - this.camera.Position.X, lookAt.Y - this.camera.Position.Y, lookAt.Z - this.camera.Position.Z);

                    break;
                case Key.Space:
                    // Reset the camera to the start position
                    r = 10.0;
                    theta = 0.0;
                    phi = 0.0;
                    lookAt.X = 3.2;
                    lookAt.Y = 2.4;
                    lookAt.Z = 0.0;

                    cameraOffset.X = 0.0;
                    cameraOffset.Y = 0.0;
                    cameraOffset.Z = 10.0;

                    this.camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                    this.camera.LookDirection = new Vector3D(lookAt.X - this.camera.Position.X,
                                                                lookAt.Y - this.camera.Position.Y,
                                                                lookAt.Z - this.camera.Position.Z);

                    break;
            }
        }

        private void UserControl_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            mouseStart = e.GetPosition(null);
        }

        private void UserControl_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (System.Math.Abs(e.Delta) < mouseWheelMin && e.Delta != 0)
                mouseWheelMin = System.Math.Abs(e.Delta);

            int ticks = e.Delta / mouseWheelMin;

            Vector3D cameraDirection = new Vector3D(cameraOffset.X, cameraOffset.Y, cameraOffset.Z);
            cameraDirection.Normalize();

            // Make sure that the radius will stay above 0
            // Distance traveled 
            if (r - (ticks * 0.1 * 1.732) > 0.1)
            {
                cameraOffset.X -= (0.1 * ticks) * cameraDirection.X;
                cameraOffset.Y -= (0.1 * ticks) * cameraDirection.Y;
                cameraOffset.Z -= (0.1 * ticks) * cameraDirection.Z;

                // Find the new radius
                r = System.Math.Sqrt(cameraOffset.X * cameraOffset.X + cameraOffset.Y * cameraOffset.Y + cameraOffset.Z * cameraOffset.Z);
            }

            this.camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
            this.camera.LookDirection = new Vector3D(lookAt.X - this.camera.Position.X,
                                                        lookAt.Y - this.camera.Position.Y,
                                                        lookAt.Z - this.camera.Position.Z);

            e.Handled = true;
        }

        private void UserControl_PreviewMouseMove(object sender, MouseEventArgs e)
        {

            Point currentPosition = e.GetPosition(null);
            Vector diff = new Vector(currentPosition.X - mouseStart.X, currentPosition.Y - mouseStart.Y);

            // If dragging with the left mouse button, orbit the camera
            if (e.LeftButton == MouseButtonState.Pressed &&
                e.RightButton == MouseButtonState.Released &&
                (System.Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                System.Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                mouseStart = currentPosition;

                phi += 0.005 * diff.Y;
                theta -= 0.005 * diff.X;

                // Clamp phi between -pi/2 and pi/2
                phi = System.Math.Min(System.Math.PI / 2 - 0.001, phi);
                phi = System.Math.Max(-System.Math.PI / 2 + 0.001, phi);

                //Find the Y value
                cameraOffset.Y = r * System.Math.Sin(phi);

                // Find the radius along the xz plane at the specified Y
                double R = r * System.Math.Cos(phi);

                // Find the X and Z values
                cameraOffset.X = R * System.Math.Sin(theta);
                cameraOffset.Z = R * System.Math.Cos(theta);

                this.camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                this.camera.LookDirection = new Vector3D(lookAt.X - this.camera.Position.X,
                                                            lookAt.Y - this.camera.Position.Y,
                                                            lookAt.Z - this.camera.Position.Z);
            }
            // Pan the camera on right click or mouswheel click
            else if (e.LeftButton == MouseButtonState.Released &&
                (e.RightButton == MouseButtonState.Pressed ||
                e.MiddleButton == MouseButtonState.Pressed) &&
                (System.Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                System.Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                mouseStart = currentPosition;

                double deltaY = 0.005 * diff.Y;
                double deltaX = -0.005 * diff.X;

                // Vectors for determining which direction the pan should move the camera
                Vector3D viewDirection = new Vector3D(this.camera.LookDirection.X, this.camera.LookDirection.Y, this.camera.LookDirection.Z);
                Vector3D upDirection = new Vector3D(0, 1, 0);
                Vector3D xDirection;
                Vector3D yDirection;

                // Normalize the view vector so all following vecotrs will be unit vectors
                viewDirection.Normalize();

                // Move along the camera's x direction
                xDirection = Vector3D.CrossProduct(viewDirection, upDirection);
                lookAt.X += xDirection.X * deltaX;
                lookAt.Y += xDirection.Y * deltaX;
                lookAt.Z += xDirection.Z * deltaX;

                // Move along the camera's y direction
                yDirection = Vector3D.CrossProduct(xDirection, viewDirection);
                lookAt.X += yDirection.X * deltaY;
                lookAt.Y += yDirection.Y * deltaY;
                lookAt.Z += yDirection.Z * deltaY;

                this.camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                this.camera.LookDirection = new Vector3D(lookAt.X - this.camera.Position.X,
                                                            lookAt.Y - this.camera.Position.Y,
                                                            lookAt.Z - this.camera.Position.Z);
            }
        }

        #endregion


        /// <summary>
        /// Draws the paths of both skeletons from the SkeletonPathComparison using the passed parameters.
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="smoothingValue"></param>
        public void DrawPaths(DateTime startTime, DateTime endTime, int smoothingValue, bool smoothBeforeClamp)
        {
            if (this.SkeletonPathComparison == null)    // No SkeletonPathComparison to draw.
                return;

            int count = group.Children.Count;

            while (group.Children.Count > 3)    // Remove the old drawn paths from the model group, leaving the first 3 (ambient light, directional light, and floor mesh) elements.
            {
                group.Children.RemoveAt(group.Children.Count - 1);
            }

            mGeometry.Children.Clear();

            mGeometry.Transform = mGeometryGP.Transform;

            #region Draw Skeleton1
            if (this.SkeletonPathComparison.Skeleton1 != null)
            {
                // Draw the paths taken by the 1st skeleton.
                ComparableSkeleton s1 = this.SkeletonPathComparison.Skeleton1;

                // Draw the Head.
                if (s1.TrackHead)   // Check if the Head was tracked.
                {
                    if (s1.ShowHead)    // Check if the Head should be drawn.
                    {
                        if (s1.HeadPath != null)    // Ensure that the Head's path exists.
                        {
                            this.DrawPath(s1.HeadPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the ShoulderCenter.
                if (s1.TrackShoulderCenter)   // Check if the ShoulderCenter was tracked.
                {
                    if (s1.ShowShoulderCenter)    // Check if the ShoulderCenter should be drawn.
                    {
                        if (s1.ShoulderCenterPath != null)    // Ensure that the ShoulderCenter's path exists.
                        {
                            this.DrawPath(s1.ShoulderCenterPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the ShoulderRight.
                if (s1.TrackShoulderRight)   // Check if the ShoulderRight was tracked.
                {
                    if (s1.ShowShoulderRight)    // Check if the ShoulderRight should be drawn.
                    {
                        if (s1.ShoulderRightPath != null)    // Ensure that the ShoulderRight's path exists.
                        {
                            this.DrawPath(s1.ShoulderRightPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the ShoulderLeft.
                if (s1.TrackShoulderLeft)   // Check if the ShoulderLeft was tracked.
                {
                    if (s1.ShowShoulderLeft)    // Check if the ShoulderLeft should be drawn.
                    {
                        if (s1.ShoulderLeftPath != null)    // Ensure that the ShoulderLeft's path exists.
                        {
                            this.DrawPath(s1.ShoulderLeftPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the ElbowRight.
                if (s1.TrackElbowRight)   // Check if the ElbowRight was tracked.
                {
                    if (s1.ShowElbowRight)    // Check if the ElbowRight should be drawn.
                    {
                        if (s1.ElbowRightPath != null)    // Ensure that the ElbowRight's path exists.
                        {
                            this.DrawPath(s1.ElbowRightPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the ElbowLeft.
                if (s1.TrackElbowLeft)   // Check if the ElbowLeft was tracked.
                {
                    if (s1.ShowElbowLeft)    // Check if the ElbowLeft should be drawn.
                    {
                        if (s1.ElbowLeftPath != null)    // Ensure that the ElbowLeft's path exists.
                        {
                            this.DrawPath(s1.ElbowLeftPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the WristRight.
                if (s1.TrackWristRight)   // Check if the WristRight was tracked.
                {
                    if (s1.ShowWristRight)    // Check if the WristRight should be drawn.
                    {
                        if (s1.WristRightPath != null)    // Ensure that the WristRight's path exists.
                        {
                            this.DrawPath(s1.WristRightPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the WristLeft.
                if (s1.TrackWristLeft)   // Check if the WristLeft was tracked.
                {
                    if (s1.ShowWristLeft)    // Check if the WristLeft should be drawn.
                    {
                        if (s1.WristLeftPath != null)    // Ensure that the WristLeft's path exists.
                        {
                            this.DrawPath(s1.WristLeftPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the HandRight.
                if (s1.TrackHandRight)   // Check if the HandRight was tracked.
                {
                    if (s1.ShowHandRight)    // Check if the HandRight should be drawn.
                    {
                        if (s1.HandRightPath != null)    // Ensure that the HandRight's path exists.
                        {
                            this.DrawPath(s1.HandRightPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the HandLeft.
                if (s1.TrackHandLeft)   // Check if the HandLeft was tracked.
                {
                    if (s1.ShowHandLeft)    // Check if the HandLeft should be drawn.
                    {
                        if (s1.HandLeftPath != null)    // Ensure that the HandLeft's path exists.
                        {
                            this.DrawPath(s1.HandLeftPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the Spine.
                if (s1.TrackSpine)   // Check if the Spine was tracked.
                {
                    if (s1.ShowSpine)    // Check if the Spine should be drawn.
                    {
                        if (s1.SpinePath != null)    // Ensure that the Spine's path exists.
                        {
                            this.DrawPath(s1.SpinePath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the SpineBase.
                if (s1.TrackHipCenter)   // Check if the HipCenter was tracked.
                {
                    if (s1.ShowHipCenter)    // Check if the HipCenter should be drawn.
                    {
                        if (s1.HipCenterPath != null)    // Ensure that the HipCenter's path exists.
                        {
                            this.DrawPath(s1.HipCenterPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the HipRight.
                if (s1.TrackHipRight)   // Check if the HipRight was tracked.
                {
                    if (s1.ShowHipRight)    // Check if the HipRight should be drawn.
                    {
                        if (s1.HipRightPath != null)    // Ensure that the HipRight's path exists.
                        {
                            this.DrawPath(s1.HipRightPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the HipLeft.
                if (s1.TrackHipLeft)   // Check if the HipLeft was tracked.
                {
                    if (s1.ShowHipLeft)    // Check if the HipLeft should be drawn.
                    {
                        if (s1.HipLeftPath != null)    // Ensure that the HipLeft's path exists.
                        {
                            this.DrawPath(s1.HipLeftPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the KneeRight.
                if (s1.TrackKneeRight)   // Check if the KneeRight was tracked.
                {
                    if (s1.ShowKneeRight)    // Check if the KneeRight should be drawn.
                    {
                        if (s1.KneeRightPath != null)    // Ensure that the KneeRight's path exists.
                        {
                            this.DrawPath(s1.KneeRightPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the KneeLeft.
                if (s1.TrackKneeLeft)   // Check if the KneeLeft was tracked.
                {
                    if (s1.ShowKneeLeft)    // Check if the KneeLeft should be drawn.
                    {
                        if (s1.KneeLeftPath != null)    // Ensure that the KneeLeft's path exists.
                        {
                            this.DrawPath(s1.KneeLeftPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the AnkleRight.
                if (s1.TrackAnkleRight)   // Check if the AnkleRight was tracked.
                {
                    if (s1.ShowAnkleRight)    // Check if the AnkleRight should be drawn.
                    {
                        if (s1.AnkleRightPath != null)    // Ensure that the AnkleRight's path exists.
                        {
                            this.DrawPath(s1.AnkleRightPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the AnkleLeft.
                if (s1.TrackAnkleLeft)   // Check if the AnkleLeft was tracked.
                {
                    if (s1.ShowAnkleLeft)    // Check if the AnkleLeft should be drawn.
                    {
                        if (s1.AnkleLeftPath != null)    // Ensure that the AnkleLeft's path exists.
                        {
                            this.DrawPath(s1.AnkleLeftPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the FootRight.
                if (s1.TrackFootRight)   // Check if the FootRight was tracked.
                {
                    if (s1.ShowFootRight)    // Check if the FootRight should be drawn.
                    {
                        if (s1.FootRightPath != null)    // Ensure that the FootRight's path exists.
                        {
                            this.DrawPath(s1.FootRightPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the FootLeft.
                if (s1.TrackFootLeft)   // Check if the FootLeft was tracked.
                {
                    if (s1.ShowFootLeft)    // Check if the FootLeft should be drawn.
                    {
                        if (s1.FootLeftPath != null)    // Ensure that the FootLeft's path exists.
                        {
                            this.DrawPath(s1.FootLeftPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
            }
            #endregion

            #region Draw Skeleton2
            if (this.SkeletonPathComparison.Skeleton2 != null)
            {
                // Draw the paths taken by the 2st skeleton.
                ComparableSkeleton s2 = this.SkeletonPathComparison.Skeleton2;

                // Draw the Head.
                if (s2.TrackHead)   // Check if the Head was tracked.
                {
                    if (s2.ShowHead)    // Check if the Head should be drawn.
                    {
                        if (s2.HeadPath != null)    // Ensure that the Head's path exists.
                        {
                            this.DrawPath(s2.HeadPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the ShoulderCenter.
                if (s2.TrackShoulderCenter)   // Check if the ShoulderCenter was tracked.
                {
                    if (s2.ShowShoulderCenter)    // Check if the ShoulderCenter should be drawn.
                    {
                        if (s2.ShoulderCenterPath != null)    // Ensure that the ShoulderCenter's path exists.
                        {
                            this.DrawPath(s2.ShoulderCenterPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the ShoulderRight.
                if (s2.TrackShoulderRight)   // Check if the ShoulderRight was tracked.
                {
                    if (s2.ShowShoulderRight)    // Check if the ShoulderRight should be drawn.
                    {
                        if (s2.ShoulderRightPath != null)    // Ensure that the ShoulderRight's path exists.
                        {
                            this.DrawPath(s2.ShoulderRightPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the ShoulderLeft.
                if (s2.TrackShoulderLeft)   // Check if the ShoulderLeft was tracked.
                {
                    if (s2.ShowShoulderLeft)    // Check if the ShoulderLeft should be drawn.
                    {
                        if (s2.ShoulderLeftPath != null)    // Ensure that the ShoulderLeft's path exists.
                        {
                            this.DrawPath(s2.ShoulderLeftPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the ElbowRight.
                if (s2.TrackElbowRight)   // Check if the ElbowRight was tracked.
                {
                    if (s2.ShowElbowRight)    // Check if the ElbowRight should be drawn.
                    {
                        if (s2.ElbowRightPath != null)    // Ensure that the ElbowRight's path exists.
                        {
                            this.DrawPath(s2.ElbowRightPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the ElbowLeft.
                if (s2.TrackElbowLeft)   // Check if the ElbowLeft was tracked.
                {
                    if (s2.ShowElbowLeft)    // Check if the ElbowLeft should be drawn.
                    {
                        if (s2.ElbowLeftPath != null)    // Ensure that the ElbowLeft's path exists.
                        {
                            this.DrawPath(s2.ElbowLeftPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the WristRight.
                if (s2.TrackWristRight)   // Check if the WristRight was tracked.
                {
                    if (s2.ShowWristRight)    // Check if the WristRight should be drawn.
                    {
                        if (s2.WristRightPath != null)    // Ensure that the WristRight's path exists.
                        {
                            this.DrawPath(s2.WristRightPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the WristLeft.
                if (s2.TrackWristLeft)   // Check if the WristLeft was tracked.
                {
                    if (s2.ShowWristLeft)    // Check if the WristLeft should be drawn.
                    {
                        if (s2.WristLeftPath != null)    // Ensure that the WristLeft's path exists.
                        {
                            this.DrawPath(s2.WristLeftPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the HandRight.
                if (s2.TrackHandRight)   // Check if the HandRight was tracked.
                {
                    if (s2.ShowHandRight)    // Check if the HandRight should be drawn.
                    {
                        if (s2.HandRightPath != null)    // Ensure that the HandRight's path exists.
                        {
                            this.DrawPath(s2.HandRightPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the HandLeft.
                if (s2.TrackHandLeft)   // Check if the HandLeft was tracked.
                {
                    if (s2.ShowHandLeft)    // Check if the HandLeft should be drawn.
                    {
                        if (s2.HandLeftPath != null)    // Ensure that the HandLeft's path exists.
                        {
                            this.DrawPath(s2.HandLeftPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the Spine.
                if (s2.TrackSpine)   // Check if the Spine was tracked.
                {
                    if (s2.ShowSpine)    // Check if the Spine should be drawn.
                    {
                        if (s2.SpinePath != null)    // Ensure that the Spine's path exists.
                        {
                            this.DrawPath(s2.SpinePath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the HipCenter.
                if (s2.TrackHipCenter)   // Check if the HipCenter was tracked.
                {
                    if (s2.ShowHipCenter)    // Check if the HipCenter should be drawn.
                    {
                        if (s2.HipCenterPath != null)    // Ensure that the HipCenter's path exists.
                        {
                            this.DrawPath(s2.HipCenterPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the HipRight.
                if (s2.TrackHipRight)   // Check if the HipRight was tracked.
                {
                    if (s2.ShowHipRight)    // Check if the HipRight should be drawn.
                    {
                        if (s2.HipRightPath != null)    // Ensure that the HipRight's path exists.
                        {
                            this.DrawPath(s2.HipRightPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the HipLeft.
                if (s2.TrackHipLeft)   // Check if the HipLeft was tracked.
                {
                    if (s2.ShowHipLeft)    // Check if the HipLeft should be drawn.
                    {
                        if (s2.HipLeftPath != null)    // Ensure that the HipLeft's path exists.
                        {
                            this.DrawPath(s2.HipLeftPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the KneeRight.
                if (s2.TrackKneeRight)   // Check if the KneeRight was tracked.
                {
                    if (s2.ShowKneeRight)    // Check if the KneeRight should be drawn.
                    {
                        if (s2.KneeRightPath != null)    // Ensure that the KneeRight's path exists.
                        {
                            this.DrawPath(s2.KneeRightPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the KneeLeft.
                if (s2.TrackKneeLeft)   // Check if the KneeLeft was tracked.
                {
                    if (s2.ShowKneeLeft)    // Check if the KneeLeft should be drawn.
                    {
                        if (s2.KneeLeftPath != null)    // Ensure that the KneeLeft's path exists.
                        {
                            this.DrawPath(s2.KneeLeftPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the AnkleRight.
                if (s2.TrackAnkleRight)   // Check if the AnkleRight was tracked.
                {
                    if (s2.ShowAnkleRight)    // Check if the AnkleRight should be drawn.
                    {
                        if (s2.AnkleRightPath != null)    // Ensure that the AnkleRight's path exists.
                        {
                            this.DrawPath(s2.AnkleRightPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the AnkleLeft.
                if (s2.TrackAnkleLeft)   // Check if the AnkleLeft was tracked.
                {
                    if (s2.ShowAnkleLeft)    // Check if the AnkleLeft should be drawn.
                    {
                        if (s2.AnkleLeftPath != null)    // Ensure that the AnkleLeft's path exists.
                        {
                            this.DrawPath(s2.AnkleLeftPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the FootRight.
                if (s2.TrackFootRight)   // Check if the FootRight was tracked.
                {
                    if (s2.ShowFootRight)    // Check if the FootRight should be drawn.
                    {
                        if (s2.FootRightPath != null)    // Ensure that the FootRight's path exists.
                        {
                            this.DrawPath(s2.FootRightPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
                // Draw the FootLeft.
                if (s2.TrackFootLeft)   // Check if the FootLeft was tracked.
                {
                    if (s2.ShowFootLeft)    // Check if the FootLeft should be drawn.
                    {
                        if (s2.FootLeftPath != null)    // Ensure that the FootLeft's path exists.
                        {
                            this.DrawPath(s2.FootLeftPath, startTime, endTime, smoothBeforeClamp, smoothingValue);
                        }
                    }
                }
            #endregion

            }
        }


        private void DrawPath(JointPath path, DateTime startTime, DateTime endTime, bool smoothBeforeClamp, int smoothingValue = 1)
        {
            if (path == null)
                return;

            if (path.PathPoints == null)
                return;

            DiffuseMaterial dm = new DiffuseMaterial(new SolidColorBrush(path.PathColor));

            //#region Scale to Viewport Coordinate Space
            //for (int i = 0; i < path.PathPoints.Count; i++)
            //{
            //    path.PathPoints[i].Location = new Point3D(path.PathPoints[i].Location.X/* / 640 * 1000*/, path.PathPoints[i].Location.Y/* / 480 * 1000*/, /*path.PathPoints[i].Location.Z*/ (path.PathPoints[i].Location.Z));
            //}
            //#endregion

            /*
            #region Rotate and Transform
            for (int i = 0; i < path.PathPoints.Count; i++)
            {
                path.PathPoints[i].Location = new Point3D(( path.PathPoints[i].Location.X / 100 ) * 100, (10 - (path.PathPoints[i].Location.Y / 100)) * 100, (10 - (path.PathPoints[i].Location.Z / 100)) * 100);
            }
            #endregion
            */

            #region Draw/Connect Points
            MeshGeometry3D mesh = new MeshGeometry3D();

            #region Add Points
            //AddPoints(ref mesh, Points, thickness);
            double thickness = 0.0025;

            if (smoothBeforeClamp)
            {
                #region Smooth, then Clamp, then Draw.

                List<JointPath.PathPoint> smoothedPathPoints = new List<JointPath.PathPoint>();

                if (smoothingValue > 1)
                {
                    for (int i = 0; i < path.PathPoints.Count; i++)
                    {
                        int cnt = 0;
                        double totalX = 0;
                        double totalY = 0;
                        double totalZ = 0;

                        for (int j = smoothingValue; j >= 0; j--)
                        {
                            if (i - j >= 0) // Average the previous (#)x of points based on the smoothing value.
                            {
                                cnt++;

                                totalX += path.PathPoints[i - j].Location.X;
                                totalY += path.PathPoints[i - j].Location.Y;
                                totalZ += path.PathPoints[i - j].Location.Z;
                            }
                        }

                        if (cnt > 0)
                        {
                            Point3D avgP3D = new Point3D(totalX / cnt, totalY / cnt, totalZ / cnt);
                            smoothedPathPoints.Add(new JointPath.PathPoint(avgP3D, path.PathPoints[i].Timestamp));
                        }
                    }
                }
                else
                {
                    smoothedPathPoints = path.PathPoints.ToList();
                }

                // CLAMP THE PATHS TO TIME RANGE:
                List<JointPath.PathPoint> clampedPathPoints = smoothedPathPoints.Where(o => (o.Timestamp >= startTime && o.Timestamp <= endTime)).ToList();


                if (clampedPathPoints != null && clampedPathPoints.Count > 0)
                {
                    mesh.Positions.Add(new Point3D(clampedPathPoints[0].Location.X, clampedPathPoints[0].Location.Y, clampedPathPoints[0].Location.Z));
                    mesh.Positions.Add(new Point3D(clampedPathPoints[0].Location.X + thickness, clampedPathPoints[0].Location.Y, clampedPathPoints[0].Location.Z));
                    mesh.Positions.Add(new Point3D(clampedPathPoints[0].Location.X + thickness, clampedPathPoints[0].Location.Y - thickness, clampedPathPoints[0].Location.Z));
                    mesh.Positions.Add(new Point3D(clampedPathPoints[0].Location.X, clampedPathPoints[0].Location.Y - thickness, clampedPathPoints[0].Location.Z));
                    for (int i = 0; i < clampedPathPoints.Count - 1; i++)
                    {
                        #region Joint Points
                        //joinPoints(ref mesh, Points[i], Points[++i], thickness);
                        mesh.Positions.Add(new Point3D(clampedPathPoints[i + 1].Location.X, clampedPathPoints[i + 1].Location.Y, clampedPathPoints[i + 1].Location.Z));
                        mesh.Positions.Add(new Point3D(clampedPathPoints[i + 1].Location.X + thickness, clampedPathPoints[i + 1].Location.Y, clampedPathPoints[i + 1].Location.Z));
                        mesh.Positions.Add(new Point3D(clampedPathPoints[i + 1].Location.X + thickness, clampedPathPoints[i + 1].Location.Y - thickness, clampedPathPoints[i + 1].Location.Z));
                        mesh.Positions.Add(new Point3D(clampedPathPoints[i + 1].Location.X, clampedPathPoints[i + 1].Location.Y - thickness, clampedPathPoints[i + 1].Location.Z));

                        int k = mesh.Positions.Count - 8;

                        //Front face
                        miniBuildFace(ref mesh, 0 + k, 1 + k, 2 + k);
                        miniBuildFace(ref mesh, 0 + k, 2 + k, 3 + k);

                        //Back face
                        miniBuildFace(ref mesh, 4 + k, 5 + k, 6 + k);
                        miniBuildFace(ref mesh, 4 + k, 6 + k, 7 + k);

                        //Right face
                        miniBuildFace(ref mesh, 3 + k, 2 + k, 5 + k);
                        miniBuildFace(ref mesh, 3 + k, 5 + k, 4 + k);

                        //Top face
                        miniBuildFace(ref mesh, 0 + k, 3 + k, 7 + k);
                        miniBuildFace(ref mesh, 0 + k, 7 + k, 4 + k);

                        //Left face
                        miniBuildFace(ref mesh, 4 + k, 5 + k, 1 + k);
                        miniBuildFace(ref mesh, 4 + k, 1 + k, 0 + k);

                        //Bottom face
                        miniBuildFace(ref mesh, 2 + k, 1 + k, 5 + k);
                        miniBuildFace(ref mesh, 2 + k, 5 + k, 6 + k);
                        #endregion
                    }
                }
                #endregion
            }
            else
            {
                #region Clamp, then Smooth, then Draw.
                // CLAMP THE PATHS TO TIME RANGE:
                List<JointPath.PathPoint> clampedPathPoints = path.PathPoints.Where(o => (o.Timestamp >= startTime && o.Timestamp <= endTime)).ToList();

                if (smoothingValue > 1)
                {
                    List<JointPath.PathPoint> smoothedPathPoints = new List<JointPath.PathPoint>();

                    for (int i = 0; i < clampedPathPoints.Count; i++)
                    {
                        int cnt = 0;
                        double totalX = 0;
                        double totalY = 0;
                        double totalZ = 0;

                        for (int j = smoothingValue; j >= 0; j--)
                        {
                            if (i - j >= 0) // Average the previous (#)x of points based on the smoothing value.
                            {
                                cnt++;

                                totalX += clampedPathPoints[i - j].Location.X;
                                totalY += clampedPathPoints[i - j].Location.Y;
                                totalZ += clampedPathPoints[i - j].Location.Z;
                            }
                        }

                        if (cnt > 0)
                        {
                            Point3D avgP3D = new Point3D(totalX / cnt, totalY / cnt, totalZ / cnt);
                            smoothedPathPoints.Add(new JointPath.PathPoint(avgP3D, clampedPathPoints[i].Timestamp));
                        }
                    }

                    clampedPathPoints.Clear();
                    clampedPathPoints = smoothedPathPoints;
                }


                if (clampedPathPoints != null && clampedPathPoints.Count > 0)
                {
                    mesh.Positions.Add(new Point3D(clampedPathPoints[0].Location.X, clampedPathPoints[0].Location.Y, clampedPathPoints[0].Location.Z));
                    mesh.Positions.Add(new Point3D(clampedPathPoints[0].Location.X + thickness, clampedPathPoints[0].Location.Y, clampedPathPoints[0].Location.Z));
                    mesh.Positions.Add(new Point3D(clampedPathPoints[0].Location.X + thickness, clampedPathPoints[0].Location.Y - thickness, clampedPathPoints[0].Location.Z));
                    mesh.Positions.Add(new Point3D(clampedPathPoints[0].Location.X, clampedPathPoints[0].Location.Y - thickness, clampedPathPoints[0].Location.Z));
                    for (int i = 0; i < clampedPathPoints.Count - 1; i++)
                    {
                        #region Joint Points
                        //joinPoints(ref mesh, Points[i], Points[++i], thickness);
                        mesh.Positions.Add(new Point3D(clampedPathPoints[i + 1].Location.X, clampedPathPoints[i + 1].Location.Y, clampedPathPoints[i + 1].Location.Z));
                        mesh.Positions.Add(new Point3D(clampedPathPoints[i + 1].Location.X + thickness, clampedPathPoints[i + 1].Location.Y, clampedPathPoints[i + 1].Location.Z));
                        mesh.Positions.Add(new Point3D(clampedPathPoints[i + 1].Location.X + thickness, clampedPathPoints[i + 1].Location.Y - thickness, clampedPathPoints[i + 1].Location.Z));
                        mesh.Positions.Add(new Point3D(clampedPathPoints[i + 1].Location.X, clampedPathPoints[i + 1].Location.Y - thickness, clampedPathPoints[i + 1].Location.Z));

                        int k = mesh.Positions.Count - 8;

                        //Front face
                        miniBuildFace(ref mesh, 0 + k, 1 + k, 2 + k);
                        miniBuildFace(ref mesh, 0 + k, 2 + k, 3 + k);

                        //Back face
                        miniBuildFace(ref mesh, 4 + k, 5 + k, 6 + k);
                        miniBuildFace(ref mesh, 4 + k, 6 + k, 7 + k);

                        //Right face
                        miniBuildFace(ref mesh, 3 + k, 2 + k, 5 + k);
                        miniBuildFace(ref mesh, 3 + k, 5 + k, 4 + k);

                        //Top face
                        miniBuildFace(ref mesh, 0 + k, 3 + k, 7 + k);
                        miniBuildFace(ref mesh, 0 + k, 7 + k, 4 + k);

                        //Left face
                        miniBuildFace(ref mesh, 4 + k, 5 + k, 1 + k);
                        miniBuildFace(ref mesh, 4 + k, 1 + k, 0 + k);

                        //Bottom face
                        miniBuildFace(ref mesh, 2 + k, 1 + k, 5 + k);
                        miniBuildFace(ref mesh, 2 + k, 5 + k, 6 + k);
                        #endregion
                    }
                }
                #endregion
            }
            #endregion
            #endregion

            //mGeometry = new GeometryModel3D(mesh, diffuseMaterial);
            //mGeometry.Transform = new Transform3DGroup();

            mGeometry.Children.Add(new GeometryModel3D(mesh, dm));

            group.Children.Add(mGeometry);
        }

        private void miniBuildFace(ref MeshGeometry3D mesh, int one, int two, int three)
        {
            mesh.TriangleIndices.Add(one);
            mesh.TriangleIndices.Add(two);
            mesh.TriangleIndices.Add(three);

            mesh.TriangleIndices.Add(one);
            mesh.TriangleIndices.Add(three);
            mesh.TriangleIndices.Add(two);

        }


        private void BuildSolid()
        {
            mGeometryGP = new Model3DGroup();

            BuildFace(1, 3, 6, 2, Colors.Black);
        }

        private void BuildFace(int one, int two, int three, int four, Color col)
        {
            MeshGeometry3D mesh = new MeshGeometry3D();

            mesh.Positions.Add(new Point3D(-1, 3, -1));
            mesh.Positions.Add(new Point3D(-1, -1, -1));
            mesh.Positions.Add(new Point3D(3, -1, -1));
            mesh.Positions.Add(new Point3D(-1, -1, 3));
            mesh.Positions.Add(new Point3D(-1, 3, 3));
            mesh.Positions.Add(new Point3D(3, 3, -1));
            mesh.Positions.Add(new Point3D(3, -1, 3));

            miniBuildFace(ref mesh, one, two, three);
            miniBuildFace(ref mesh, one, three, four);

            Brush br = new SolidColorBrush(col);
            br.Opacity = 1;//original 0.2

            // Geometry creation
            mGeometryGP.Children.Add(new GeometryModel3D(mesh, new DiffuseMaterial(br)));
            mGeometryGP.Transform = new Transform3DGroup();
            group.Children.Add(mGeometryGP);

        }

        /// <summary>
        /// Shows the FloorMesh in the PathDisplayViewport.
        /// </summary>
        public void ShowFloorMesh()
        {
            group.Children[2] = mGeometryGP;
        }

        /// <summary>
        /// Hides the FloorMesh (by replacing it with a temporary Transparent GeometryModel3D) in the PathDisplayViewport.
        /// </summary>
        public void HideFloorMesh()
        {
            group.Children[2] = new GeometryModel3D(new MeshGeometry3D(), new DiffuseMaterial(Brushes.Transparent));
        }

        /*
        private void Grid_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            camera.Position = new Point3D(camera.Position.X, camera.Position.Y, camera.Position.Z - e.Delta / 250D);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            camera.Position = new Point3D(camera.Position.X, camera.Position.Y, 5);
            mGeometryGP.Transform = new Transform3DGroup();
            //mGeometry.Transform = new Transform3DGroup();
        }

        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {

                if (mDown)
                {
                    Point pos = Mouse.GetPosition(this.PathDisplayViewport);
                    Point actualPos = new Point(pos.X - this.PathDisplayViewport.ActualWidth / 2, this.PathDisplayViewport.ActualHeight / 2 - pos.Y);
                    double dx = actualPos.X - mLastPos.X, dy = actualPos.Y - mLastPos.Y;

                    double mouseAngle = 0;

                    //unlock rotation
                    if (!rotateLock)
                    {
                        if (dx != 0 && dy != 0)
                        {
                            mouseAngle = System.Math.Asin(System.Math.Abs(dy) / System.Math.Sqrt(System.Math.Pow(dx, 2) + System.Math.Pow(dy, 2)));
                            if (dx < 0 && dy > 0) mouseAngle += System.Math.PI / 2;
                            else if (dx < 0 && dy < 0) mouseAngle += System.Math.PI;
                            else if (dx > 0 && dy < 0) mouseAngle += System.Math.PI * 1.5;
                        }
                        else if (dx == 0 && dy != 0) mouseAngle = System.Math.Sign(dy) > 0 ? System.Math.PI / 2 : System.Math.PI * 1.5;
                        else if (dx != 0 && dy == 0) mouseAngle = System.Math.Sign(dx) > 0 ? 0 : System.Math.PI;
                    }
                    else
                    {
                        //lock rotation
                        if (dx != 0) mouseAngle = System.Math.Sign(dx) > 0 ? 0 : System.Math.PI;
                    }

                    double axisAngle = mouseAngle + System.Math.PI / 2;

                    Vector3D axis = new Vector3D(System.Math.Cos(axisAngle) * 4, System.Math.Sin(axisAngle) * 4, 0);

                    double rotation = 0.01 * System.Math.Sqrt(System.Math.Pow(dx, 2) + System.Math.Pow(dy, 2));

                    Transform3DGroup group = mGeometryGP.Transform as Transform3DGroup;
                    Transform3DGroup group1 = mGeometry.Transform as Transform3DGroup;
                    QuaternionRotation3D r = new QuaternionRotation3D(new Quaternion(axis, rotation * 180 / System.Math.PI));
                    group.Children.Add(new RotateTransform3D(r));
                    group1.Children.Add(new RotateTransform3D(r));

                    mLastPos = actualPos;
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
            {
                return;
            }
            mDown = true;
            Point pos = Mouse.GetPosition(this.PathDisplayViewport);
            mLastPos = new Point(pos.X - this.PathDisplayViewport.ActualWidth / 2, this.PathDisplayViewport.ActualHeight / 2 - pos.Y);
        }

        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            mDown = false;
        }
        */
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
