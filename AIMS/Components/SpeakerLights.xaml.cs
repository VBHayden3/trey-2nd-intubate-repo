﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for SpeakerLights.xaml
    /// </summary>
    public partial class SpeakerLights : UserControl
    {
        #region Dependency Properties

        public static readonly DependencyProperty NumLightsProperty =
            DependencyProperty.Register("NumLights", typeof(int), typeof(SpeakerLights), new PropertyMetadata(21, NumLightsChangedCallback));

        public static readonly DependencyProperty LeftScoreProperty =
            DependencyProperty.Register("LeftScore", typeof(double), typeof(SpeakerLights), new PropertyMetadata(0.0, LeftScoreChangedCallback));

        public static readonly DependencyProperty RightScoreProperty =
            DependencyProperty.Register("RightScore", typeof(double), typeof(SpeakerLights), new PropertyMetadata(0.0, RightScoreChangedCallback));

        #endregion

        #region Dependency Accessors

        public int NumLights
        {
            get { return (int)GetValue(NumLightsProperty); }
            set { SetValue(NumLightsProperty, value); }
        }

        public double LeftScore
        {
            get { return (double)GetValue(LeftScoreProperty); }
            set { SetValue(LeftScoreProperty, value); }
        }

        public double RightScore
        {
            get { return (double)GetValue(RightScoreProperty); }
            set { SetValue(RightScoreProperty, value); }
        }

        #endregion

        #region Dependency Callbacks

        private static void NumLightsChangedCallback(object sender, DependencyPropertyChangedEventArgs e)
        {
            SpeakerLights sl = sender as SpeakerLights;

            sl.MainGrid.Children.Clear();
            sl.MainGrid.ColumnDefinitions.Clear();

            // Settings that will be reused for each light
            sl.lights = new Border[sl.NumLights];

            Thickness margin = new Thickness(3);
            CornerRadius cornerRadius = new CornerRadius(5);
            GridLength width = new GridLength(1, GridUnitType.Star);

            // Add the left lights
            for (int i = 0; i < sl.NumLights/2; i++)
            {
                Border light = new Border();
                light.Margin = margin;
                light.CornerRadius = cornerRadius;
                light.Name = "Light" + i;
                sl.lights[i] = light;

                sl.setBackground(i, i < (sl.LeftScore * sl.NumLights));

                ColumnDefinition columnDefinition = new ColumnDefinition();
                columnDefinition.Width = width;

                sl.MainGrid.ColumnDefinitions.Add(columnDefinition);
                sl.MainGrid.Children.Add(light);
                Grid.SetColumn(light, i);
            }

            // Add the Transparent Block

            // Add Transparent Block
            {
                Border light = new Border();
                light.Margin = margin;
                light.CornerRadius = cornerRadius;
                light.Name = "CenterLight";


                light.Background = (Brush)sl.Resources["Transparent"];

                ColumnDefinition columnDefinition = new ColumnDefinition();
                columnDefinition.Width = width;
                sl.MainGrid.ColumnDefinitions.Add(columnDefinition);

                columnDefinition = new ColumnDefinition();
                columnDefinition.Width = width;
                sl.MainGrid.ColumnDefinitions.Add(columnDefinition);

                sl.MainGrid.Children.Add(light);
                Grid.SetColumnSpan(light, 2);
                Grid.SetColumn(light, sl.NumLights / 2);

            }

            // Add the right lights
            for (int i = sl.NumLights / 2; i < sl.NumLights; i++)
            {
                Border light = new Border();
                light.Margin = margin;
                light.CornerRadius = cornerRadius;
                light.Name = "Light" + i;
                sl.lights[i] = light;

                sl.setBackground(i, i < (sl.RightScore * sl.NumLights));

                ColumnDefinition columnDefinition = new ColumnDefinition();
                columnDefinition.Width = width;

                sl.MainGrid.ColumnDefinitions.Add(columnDefinition);
                sl.MainGrid.Children.Add(light);
                Grid.SetColumn(light, i+2);
            }
        }

        private static void LeftScoreChangedCallback(object sender, DependencyPropertyChangedEventArgs e)
        {
            SpeakerLights sl = sender as SpeakerLights;

            for (int i = 0; i < sl.NumLights/2; i++)
            {
                sl.setBackground(sl.NumLights/2 - 1 - i, (i < ((sl.NumLights/2) * (sl.LeftScore))));
            }
        }

        private static void RightScoreChangedCallback(object sender, DependencyPropertyChangedEventArgs e)
        {
            SpeakerLights sl = sender as SpeakerLights;

            for (int i = sl.NumLights/2; i < sl.NumLights; i++)
            {
                sl.setBackground(i, i < (sl.NumLights/2 * (1 + sl.RightScore)));
            }
        }

        #endregion

        private Border[] lights;

        #region Cunstructors
        public SpeakerLights()
        {
            InitializeComponent();

            lights = new Border[NumLights];

            // Settings that will be reused for each light
            Thickness margin = new Thickness(2);
            CornerRadius cornerRadius = new CornerRadius(5);
            GridLength width = new GridLength(1, GridUnitType.Star);
            Border light;

            // Add the left lights
            for (int i = 0; i < NumLights; i++)
            {
                light = new Border();
                light.Margin = margin;
                light.CornerRadius = cornerRadius;
                light.Name = "Light" + i;
                lights[i] = light;

                setBackground(i, false);

                ColumnDefinition columnDefinition = new ColumnDefinition();
                columnDefinition.Width = width;
                
                MainGrid.ColumnDefinitions.Add(columnDefinition);
                MainGrid.Children.Add(light);
                Grid.SetColumn(light, i);
            }

            // Add Transparent Block
            {
                light = new Border();
                light.Margin = margin;
                light.CornerRadius = cornerRadius;
                light.Name = "CenterLight";


                light.Background = (Brush)Resources["Transparent"];

                ColumnDefinition columnDefinition = new ColumnDefinition();
                columnDefinition.Width = width;
                MainGrid.ColumnDefinitions.Add(columnDefinition);

                columnDefinition = new ColumnDefinition();
                columnDefinition.Width = width;
                MainGrid.ColumnDefinitions.Add(columnDefinition);

                MainGrid.Children.Add(light);
                Grid.SetColumnSpan(light, 2);
                Grid.SetColumn(light, NumLights/2);

            }


            // Add Right Half
            for (int i = NumLights/2; i < NumLights; i++)
            {
                light = new Border();
                light.Margin = margin;
                light.CornerRadius = cornerRadius;
                light.Name = "Light" + i;
                lights[i] = light;

                setBackground(i, false);

                ColumnDefinition columnDefinition = new ColumnDefinition();
                columnDefinition.Width = width;

                MainGrid.ColumnDefinitions.Add(columnDefinition);
                MainGrid.Children.Add(light);
                Grid.SetColumn(light, NumLights+2);
            }
        }

        public SpeakerLights(int numLights)
        {
            InitializeComponent();

            this.NumLights = numLights;

            lights = new Border[NumLights];

            // Settings that will be reused for each light
            Thickness margin = new Thickness(5);
            CornerRadius cornerRadius = new CornerRadius(5);
            GridLength width = new GridLength(1, GridUnitType.Star);

            for (int i = 0; i < NumLights/2; i++)
            {
                Border light = new Border();
                light.Margin = margin;
                light.CornerRadius = cornerRadius;
                light.Name = "Light" + i;
                lights[i] = light;

                setBackground(i, false);

                ColumnDefinition columnDefinition = new ColumnDefinition();
                columnDefinition.Width = width;

                MainGrid.ColumnDefinitions.Add(columnDefinition);
                MainGrid.Children.Add(light);
                Grid.SetColumn(light, i);
            }
        }
        #endregion
        
        public void setRightScore(double score)
        {
            this.LeftScore = score;
        }

        public void setLeftScore(double score)
        {
            this.RightScore = score;
        }

        /// <summary>
        /// Sets the background color of the lights
        /// </summary>
        /// <param name="index"></param>
        /// <param name="isLit"></param>
        private void setBackground(int index, bool isLit)
        {
            if (isLit)
            {
                if (index < (NumLights + 1) / 6)
                    lights[index].Background = (Brush)Resources["GreenLit"];
                else if (index < (2 * NumLights + 5) / 6)
                    lights[index].Background = (Brush)Resources["YellowLit"];
                else if (index < (3 * NumLights + 3) / 6)
                    lights[index].Background = (Brush)Resources["RedLit"];
                else if (index < (4 * NumLights + 2) / 6)
                    lights[index].Background = (Brush)Resources["RedLit"];
                else if (index < (5 * NumLights + 4) / 6)
                    lights[index].Background = (Brush)Resources["YellowLit"];
                else
                    lights[index].Background = (Brush)Resources["GreenLit"];

            }
            else
            {
                if (index < (NumLights + 1) / 6)
                    lights[index].Background = (Brush)Resources["GreenUnlit"];
                else if (index < (2 * NumLights + 5) / 6)
                    lights[index].Background = (Brush)Resources["YellowUnlit"];
                else if (index < (3 * NumLights + 3) / 6)
                    lights[index].Background = (Brush)Resources["RedUnlit"];
                else if (index < (4 * NumLights + 2) / 6)
                    lights[index].Background = (Brush)Resources["RedUnlit"];
                else if (index < (5 * NumLights + 4) / 6)
                    lights[index].Background = (Brush)Resources["YellowUnlit"];
                else
                    lights[index].Background = (Brush)Resources["GreenUnlit"];
            }
        }
    }
}
