﻿/****************************************************************************
 * 
 *  Class: StageImage
 *  Author: Mitchel Weate
 *  
 *  StageImage displays an image with the application's border style. It also
 *  overlays up to 2 target points. When the target points are modified
 *  the user must call targetsUpdated() so that the StageImage will redraw itself.
 *   
 * ***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for StageImage.xaml
    /// </summary>
    public partial class StageImage : UserControl
    {
        #region Dependency properties

        private static DependencyProperty StageBackgroundProperty =
            DependencyProperty.Register("StageBackground", typeof(Brush), typeof(StageImage), new PropertyMetadata(null, StageImageChangedCallback));

        #endregion

        #region Dependency accessors

        public Brush StageBackground
        {
            get { return (Brush)GetValue(StageBackgroundProperty); }
            set { SetValue(StageBackgroundProperty, value); }
        }

        #endregion

        #region Dependency callbacks

        private static void StageImageChangedCallback(object sender, DependencyPropertyChangedEventArgs e)
        {
            StageImage stageImage = (StageImage)sender;

            stageImage.Border.Background = (Brush)(e.NewValue);
        }

        #endregion

        private StageImageOverlay.Target[] targets;

        public StageImage()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();
            }
        }

        public StageImageOverlay.Target[] getTargets()
        {
            return Overlay.getTargets();
        }

        public void targetsUpdated()
        {
            Overlay.InvalidateVisual();
        }
    }
}
