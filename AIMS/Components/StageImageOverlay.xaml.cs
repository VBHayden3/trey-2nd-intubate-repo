﻿/****************************************************************************
 * 
 *  Class: StageImageOverlay
 *  Author: Mitchel Weate
 *  
 *  StageImageOverlay provides an area for StageImage's targets to be drawn.
 *   
 * ***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for StageImageOverlay.xaml
    /// </summary>
    public partial class StageImageOverlay : UserControl
    {
        private Target[] targets;

        public StageImageOverlay()
        {
            InitializeComponent();

            targets = new Target[2];
            for (int i = 0; i < 2; i++)
            {
                targets[i] = new Target();
            }
        }

        public Target[] getTargets()
        {
            return targets;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            for (int i = 0; i < 2; i++)
            {
                if (targets[i].isVisible == true)
                {
                    drawingContext.DrawEllipse(targets[i].background, null,
                        new Point(targets[i].center.X * this.ActualWidth, targets[i].center.Y * this.ActualHeight),
                        targets[i].radius, targets[i].radius);

                    drawingContext.DrawEllipse(targets[i].foreground, null,
                        new Point(targets[i].center.X * this.ActualWidth, targets[i].center.Y * this.ActualHeight),
                        targets[i].radius, targets[i].radius);

                }
            }
        }

        public class Target
        {
            public Brush foreground;
            public Brush background;
            public Point center;
            public int radius;
            public bool isVisible;

            public Target()
            {
                this.foreground = null;
                this.background = null;
                this.center = new Point();
                this.radius = 0;
                this.isVisible = false;
            }
        }
    }
}
