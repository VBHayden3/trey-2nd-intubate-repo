﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for StartTaskVerificationOverlay.xaml
    /// </summary>
    public partial class StartTaskVerificationOverlay : UserControl
    {
        Page parentPage;
        private TaskMode taskMode;

        public TaskMode TaskMode { get { return taskMode; } set { taskMode = value; UpdateTaskModeVerificationText(); } }

        public StartTaskVerificationOverlay()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();
                Loaded += StartTaskVerificationOverlay_Loaded;
            }
        }

        private void StartTaskVerificationOverlay_Loaded(object sender, EventArgs e)
        {
            parentPage = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

            /*this.Height = parentPage.ActualHeight;
            this.Width = parentPage.ActualWidth;*/

            TaskMode = TaskMode.Practice;
        }

        private Task GetTaskFromFlowIndex()
        {
            Task task = Task.None;
            task = ((App)App.Current).CurrentTask;
            
            StudentDashboard sd = parentPage as StudentDashboard;
            
            if (sd != null)
            {
                if (sd.flow != null)
                {
                    switch (sd.flow.Index)
                    {
                        case 0:
                            task = Task.Intubation;
                            break;
                        case 1:
                            task = Task.VerticalLift;
                            break;
                        case 2:
                            task = Task.LateralTransfer;
                            break;
                        case 3:
                            task = Task.CPR;
                            break;
                        case 4:
                            task = Task.SafePatientHandling;
                            break;
                    }
                }
            }
            
            
            return task;
        }

        private void StartButton_Clicked(object sender, RoutedEventArgs e)
        {
            StartTask();
        }

        public void StartTask()
        {
            //MessageBox.Show("Starting Task");
            //Finalize the TaskMode in the static app (this will be read again once the task is navigated to (just below))
            ((App)App.Current).CurrentTaskMode = taskMode;
            //Finalize the Task in the current App from the Flow Control.
            ((App)App.Current).CurrentTask = GetTaskFromFlowIndex();

            //Navigate to the selected task
            switch (((App)App.Current).CurrentTask)
            {
                case Task.Intubation:
                {
                    parentPage.NavigationService.Navigate(new System.Uri(@"../Tasks/Intubation/IntubationPage.xaml", System.UriKind.Relative));
                    break;
                }
                case Task.VerticalLift:
                {
                    parentPage.NavigationService.Navigate(new System.Uri(@"../Tasks/Vertical Lift/UI/VerticalLiftPage.xaml", System.UriKind.Relative));
                    break;
                }
                case Task.LateralTransfer:
                {
                    parentPage.NavigationService.Navigate(new System.Uri(@"../Tasks/Lateral Transfer/LateralTransferPage.xaml", System.UriKind.Relative));
                    break;
                }
                case Task.CPR:
                {
                    parentPage.NavigationService.Navigate(new System.Uri(@"../Tasks/Cpr/UI/CprPage.xaml", System.UriKind.Relative));
                    break;
                }
            }
        }

        private void ExitButton_Clicked(object sender, RoutedEventArgs e)
        {
            ExitOverlay();
        }

        public void ShowOverlay(TaskMode taskmode, Task task)
        {
            ((App)(App.Current)).CurrentTask = task;
            ((App)App.Current).CurrentTaskMode = taskmode;
            //MessageBox.Show(String.Format("Task is {0}.", ((App)App.Current).CurrentTask.ToString()));

            this.TaskMode = taskmode;

            StudentDashboard sd = parentPage as StudentDashboard;

            if (sd != null)
            {
                Storyboard storyboard = new Storyboard();

                DoubleAnimation myDoubleAnimation = new DoubleAnimation();
                myDoubleAnimation.From = 0;
                myDoubleAnimation.To = 20;
                myDoubleAnimation.Name = "Blur";
                myDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(0.25));
                myDoubleAnimation.AutoReverse = false;
                myDoubleAnimation.FillBehavior = FillBehavior.Stop;

                sd.RegisterName(myDoubleAnimation.Name, myDoubleAnimation);

                storyboard.Children.Clear();

                storyboard.Children.Add(myDoubleAnimation);
                Storyboard.SetTargetName(myDoubleAnimation, "BlurEffect");
                Storyboard.SetTargetProperty(myDoubleAnimation, new PropertyPath(System.Windows.Media.Effects.BlurEffect.RadiusProperty));

                sd.UnregisterName(myDoubleAnimation.Name);

                storyboard.Completed += delegate { sd.BlurEffect.Radius = 20; };

                storyboard.Begin(sd);
            }

            this.Visibility = Visibility.Visible;
            this.IsEnabled = true;
        }
        
        public void ExitOverlay()
        {
            StudentDashboard sd = parentPage as StudentDashboard;

            if (sd != null)
            {
                Storyboard storyboard = new Storyboard();

                DoubleAnimation myDoubleAnimation = new DoubleAnimation();
                myDoubleAnimation.From = 20;
                myDoubleAnimation.To = 0;
                myDoubleAnimation.Name = "Unblur";
                myDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(0.25));
                myDoubleAnimation.AutoReverse = false;
                myDoubleAnimation.FillBehavior = FillBehavior.Stop;

                sd.RegisterName(myDoubleAnimation.Name, myDoubleAnimation);

                storyboard.Children.Clear();

                storyboard.Children.Add(myDoubleAnimation);
                Storyboard.SetTargetName(myDoubleAnimation, "BlurEffect");
                Storyboard.SetTargetProperty(myDoubleAnimation, new PropertyPath(System.Windows.Media.Effects.BlurEffect.RadiusProperty));

                sd.UnregisterName(myDoubleAnimation.Name);

                storyboard.Completed += delegate { sd.BlurEffect.Radius = 0; };

                storyboard.Begin(sd);
            }

            this.IsEnabled = false;
            this.Visibility = Visibility.Hidden;
            
            //parentPage.NavigationService.Navigate(new System.Uri(@"../StudentDashboard.xaml", System.UriKind.Relative));
        }

        private DependencyObject GetDependencyObjectFromVisualTree(DependencyObject startObject, Type type)
        {
            DependencyObject parent = startObject;

            while (parent != null)
            {
                if (type.IsInstanceOfType(parent))
                    break;

                else
                    parent = VisualTreeHelper.GetParent(parent);
            }

            return parent;
        }

        private void UpdateTaskModeVerificationText()
        {
            switch ( this.TaskMode )
            {
                case TaskMode.Practice:
                    this.StartTaskTextBlock.Text = "Are you ready to begin your practice?";
                    break;
                case TaskMode.Test:
                    this.StartTaskTextBlock.Text = "Are you ready to begin your test?";
                    break;
            }
        }
    }
}
