﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Npgsql;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for StudentMessageDisplay.xaml
    /// </summary>
    public partial class StudentMessageDisplay : UserControl
    {
        Page parentPage;

        private ObservableCollection<Message> messagesList = new ObservableCollection<Message>();

        public ObservableCollection<Message> MessagesList
        {
            get { return messagesList; }
            set
            {
                messagesList = value;

                OnPropertyChanged(this, "MessagesList");
            }
        }

        #region NotifyPropertyChanged
        // Declare the PropertyChanged event
        public event PropertyChangedEventHandler PropertyChanged;

        // OnPropertyChanged will raise the PropertyChanged event passing the
        // source property that is being updated.
        private void OnPropertyChanged(object sender, string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                PropertyChanged(sender, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        public StudentMessageDisplay()
        {
            parentPage = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();

                Loaded +=StudentMessageDisplay_Loaded;
            }
        }

        private void StudentMessageDisplay_Loaded(object sender, EventArgs e)
        {
            parentPage = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

            if (parentPage as StudentDashboard != null)
            {
                StudentDashboard sd = parentPage as StudentDashboard;
                sd.BlurEffect.Radius = 0;
            }
        }
     
        /// <summary>
        /// Shows and enables the Results Overlay, blurring the background.
        /// </summary>
        /// <param name="result"></param>
        public void ShowMessages( List<Message> msgList )
        {
            this.Visibility = System.Windows.Visibility.Visible;
            this.IsEnabled = true;

            MessagesList.Clear();

            if (msgList != null)
            {               
                foreach (Message msg in msgList)
                {
                    MessagesList.Add(msg);
                }
            }

            messagesDisplay.ItemsSource = MessagesList;
        }

        
        private void ExitClicked(object sender, RoutedEventArgs e)
        {
            this.Visibility = System.Windows.Visibility.Hidden;
            this.IsEnabled = false;

            parentPage = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

            if (parentPage as StudentDashboard != null)
            {
                StudentDashboard tp = parentPage as StudentDashboard;
                tp.BlurEffect.Radius = 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="startObject"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private DependencyObject GetDependencyObjectFromVisualTree(DependencyObject startObject, Type type)
        {
            DependencyObject parent = startObject;

            while (parent != null)
            {
                if (type.IsInstanceOfType(parent))
                    break;

                else
                    parent = VisualTreeHelper.GetParent(parent);
            }

            return parent;
        } 
    }
}

