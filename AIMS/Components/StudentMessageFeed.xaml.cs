﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for StudentMessageFeed.xaml
    /// </summary>
    public partial class StudentMessageFeed : UserControl
    {
        List<Message> messages = new List<Message>();

        public StudentMessageFeed()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();
                Loaded += StudentMessageFeed_Loaded;
            }
        }

        public void StudentMessageFeed_Loaded(object sender, EventArgs e)
        {
            messages = RetreiveMessagesForStudent(((App)App.Current).CurrentStudent);

            foreach( Message message in messages )
            {
                this.MessageFeedListBox.Items.Add( message );
            }
        }

        private List<Message> RetreiveMessagesForStudent(Student student)
        {
            if (student == null)
                return new List<Message>();

            List<Message> messages = Message.FindMessagesForStudentID(student.ID);

            return messages;
        }
    }
}
