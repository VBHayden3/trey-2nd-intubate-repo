﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Npgsql;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for StudentMessages.xaml
    /// </summary>
    public partial class StudentMessages : UserControl
    {
        private List<Result> studentResultList = new List<Result>();
        private List<Message> mssgList = new List<Message>();
        private List<int> mssgIndexList = new List<int>();
        Page parentPage;


        public StudentMessages()
        {
            parentPage = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();

                Loaded +=StudentMessages_Loaded;
            }
        }

        private void StudentMessages_Loaded(object sender, EventArgs e)
        {
        }
     
        /// <summary>
        /// Shows and enables the Results Overlay, blurring the background.
        /// </summary>
        /// <param name="result"></param>
        public void ShowMessages()
        {
           PopulateMssgButtonList();
       
           this.Visibility = System.Windows.Visibility.Visible;
           this.IsEnabled = true;
        }

        /// <summary>
        /// Fills the button list with buttons representing each StageScore object from the Result object
        /// passed in as the parameter. If existing buttons exist, they will first be cleared out.
        /// </summary>
        /// <param name="result"></param>
        public void PopulateMssgButtonList( )
        {
            // Clear out old buttons before re-populating StageScore button list.
            StepButtonsPanel.Items.Clear();

            if (DatabaseManager.Connection.State != System.Data.ConnectionState.Open)
            {
                DatabaseManager.OpenConnection();
            }

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;

            command.CommandText = "SELECT * FROM messages";

            NpgsqlDataReader sqlresult = command.ExecuteReader();

            mssgList = null;
            mssgList = new List<Message>();
            mssgIndexList = null;
            mssgIndexList = new List<int>();

            int uid;
            int courseid;
            int toid;
            int fromid;
            string subjectText;
            string bodyText;
            bool read;
            DateTime dt;

            try
            {
                while (sqlresult.Read())
                {
                    uid = sqlresult.GetInt32(sqlresult.GetOrdinal("uid"));
                    courseid = sqlresult.GetInt32(sqlresult.GetOrdinal("courseid"));
                    toid = sqlresult.GetInt32(sqlresult.GetOrdinal("toid"));
                    fromid = sqlresult.GetInt32(sqlresult.GetOrdinal("fromid"));
                    subjectText = sqlresult.GetString(sqlresult.GetOrdinal("subject"));
                    bodyText = sqlresult.GetString(sqlresult.GetOrdinal("body"));
                    read = sqlresult.GetBoolean(sqlresult.GetOrdinal("hasbeenread"));
                    dt = sqlresult.GetDateTime(sqlresult.GetOrdinal("datesent"));

                    Course cid = new Course();
                    //cid = Course.GetCourseFromCourseID(courseid, false);

                    Doctor did = new Doctor();
                    //did = Doctor.GetDoctorFromDoctorID(fromid, false);

                    Student sid = new Student();
                    //sid = Student.GetStudentFromStudentID(toid, false);

                    Message mssg = new Message(uid, sid, did, cid, subjectText, bodyText, read, dt);
                   
                    if (mssg != null)    // if the message
                    {
                        mssgList.Add(mssg); // add it to the list of messages.
                        mssgIndexList.Add(uid);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Messages were not successfully pulled from Database.");
                return;
            }

            DatabaseManager.CloseConnection();

            // Repopulate the results stage button list from the passed in result.
            for (int i = 0; i < mssgList.Count; i++)
            {
                System.Windows.Controls.Button bttn = new Button();
                bttn.Uid = i.ToString();
                //int score = (int)(100 * result.StageScores[i].score);
                //bttn.Click += new RoutedEventHandler(bttn_Click);    //StageButtonClicked();
                bttn.Content = mssgList[i].Subject.ToString();
                if (mssgList[i].HasBeenRead == true)
                {
                    bttn.Background = Brushes.Gray;
                }
                StepButtonsPanel.Items.Add(bttn);
            }
        }




        /// <summary>
        /// Event handler attached to every StageScore button. When clicked, we get the StageScore object
        /// at the index of the button within the Result object's StageScore collection. We then populate 
        /// the Results Component with information from that StageScore object (Name, number, time, score, 
        /// scenario effect, etc).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mssgButton_Click(object sender, RoutedEventArgs e)
        {
            Button clicked = e.OriginalSource as Button;
            clicked.Background = Brushes.Gray;

            findMssgInformation( Int32.Parse( clicked.Uid ) );
        }

        public void findMssgInformation(int index)
        {
            if (mssgList != null)
            {
                if (mssgList!= null)
                {
                    if (index >= 0 && index < mssgList.Count)
                    {
                        showMssgInformation(mssgList[index]);
                    }
                    else
                    {
                        MessageBox.Show(String.Format("Index {0} is outside the bounds of the Message collection."));
                    }
                }
                else
                {
                    MessageBox.Show("The linked message consists of no information.");
                }
            }
            else
            {
                MessageBox.Show("No message object has been linked to display.");
            }
        }

        private void showMssgInformation(Message mssg)
        {
            if (mssg != null)
            {
                this.SubjectText.Text = String.IsNullOrWhiteSpace(mssg.Subject) ? "No detailed information is available " : mssg.Subject;
                this.IdText.Text  = mssg.From.ToString();
                this.DateText.Text = mssg.DateSent.ToString();
                this.BodyText.Text = String.IsNullOrWhiteSpace(mssg.Body) ? "No detailed information is available" : mssg.Body;

                DatabaseManager.OpenConnection();
                NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
                command.CommandType = System.Data.CommandType.Text;
                command.CommandText = "UPDATE messages SET hasbeenread = :hasbeenread WHERE uid = :uid;";

                command.Parameters.Add("hasbeenread", NpgsqlTypes.NpgsqlDbType.Boolean).Value = true;
                command.Parameters.Add("uid", NpgsqlTypes.NpgsqlDbType.Integer).Value = mssg.ID;

                int enq = command.ExecuteNonQuery();

                DatabaseManager.CloseConnection();
            }
            else
            {
                MessageBox.Show("The message data does not exist.");
            }
        }

       

        private void ExitClicked(object sender, RoutedEventArgs e)
        {
            this.Visibility = System.Windows.Visibility.Hidden;
            this.IsEnabled = false;

            parentPage = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

            if (parentPage as StudentDashboard != null)
            {
                StudentDashboard tp = parentPage as StudentDashboard;
                tp.BlurEffect.Radius = 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="startObject"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private DependencyObject GetDependencyObjectFromVisualTree(DependencyObject startObject, Type type)
        {
            DependencyObject parent = startObject;

            while (parent != null)
            {
                if (type.IsInstanceOfType(parent))
                    break;

                else
                    parent = VisualTreeHelper.GetParent(parent);
            }

            return parent;
        } 
    }
}

