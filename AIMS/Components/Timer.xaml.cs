﻿/****************************************************************************
 * 
 *  Class: Timer
 *  Author: Mitchel Weate
 *  
 *  Timer will act as a stopwatch that can save a best time. saveTime() will first 
 *  check if the timer is better than the previous best, and only save it if it is.
 *  When checking for a best time, Stop() should be called first.
 *   
 * ***************************************************************************/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for Timer.xaml
    /// </summary>
    public partial class Timer : UserControl
    {
        public delegate void TimerResetEventHandler(object sender);
        public event TimerResetEventHandler TimerReset;

        private DispatcherTimer dispatcherTimer;
        private Stopwatch stopwatch;
        private TimeSpan bestTime;

        public Timer()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();
                
                dispatcherTimer = new DispatcherTimer();
                dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 40);
                dispatcherTimer.Tick += dispatchTimer_Tick;

                stopwatch = new Stopwatch();
            
                this.Loaded += TimerLoaded;
            }

            
        }

        void TimerLoaded(object sender, RoutedEventArgs e)
        {
        }

        // Get the best time
        public TimeSpan getBestTime()
        {
            return this.bestTime;
        }

        // Get the current time
        public TimeSpan getCurentTime()
        {
            return this.stopwatch.Elapsed;
        }

        // Start timing. Also starts the dispatch timer so that the component can be redrawn
        public void Start()
        {
            stopwatch.Start();
            dispatcherTimer.Start();
        }

        // Stops timing. The dispatch timer is stoped so that the component doesn't keep getting redrawn
        public void Stop()
        {
            stopwatch.Stop();
            dispatcherTimer.Stop();
        }

        // Sets the timer back to zero and notifies any listeners that the timer has been reset.
        public void Reset()
        {
            stopwatch.Reset();
            dispatcherTimer.Stop();

            setCurrentTime();

            if (TimerReset != null)
                TimerReset(this);

        }

        // Sets the PreviousTimer TextBlock. Must be called before Reset.
        public void saveTime()
        {
            this.CurrentTime.Text = String.Format("{0:00}:{1:00}:{2:00}",
                stopwatch.Elapsed.Minutes, stopwatch.Elapsed.Seconds, stopwatch.Elapsed.Milliseconds / 10);

            if (bestTime.TotalMilliseconds > stopwatch.Elapsed.TotalMilliseconds || bestTime.TotalMilliseconds == 0)
            {
                bestTime = stopwatch.Elapsed;

                this.BestTime.Text = String.Format("{0:00}:{1:00}:{2:00}",
                    bestTime.Minutes, bestTime.Seconds, bestTime.Milliseconds / 10);
            }
            
        }

        // Sets the CurrentTime TextBlock and updates the visual.
        private void dispatchTimer_Tick(object sender, EventArgs e)
        {
            setCurrentTime();

            this.InvalidateVisual();
        }

        // Resets the timer when the button is clicked.
        private void ResetButtonClick(object sender, RoutedEventArgs e)
        {
            this.Reset();
        }

        // Sets the CurrentTime TextBlock
        private void setCurrentTime()
        {
            this.CurrentTime.Text = String.Format("{0:00}:{1:00}:{2:00}",
                stopwatch.Elapsed.Minutes, stopwatch.Elapsed.Seconds, stopwatch.Elapsed.Milliseconds / 10);
        }
    }
}
