﻿/****************************************************************************
 * 
 *  Class: TrackedJoint
 *  Author: Mitchel Weate
 *  
 *  TrackedJoint is used to specify brushes and a size for the SkeletonOverlay.
 *   
 * ***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using Microsoft.Kinect;

namespace AIMS.Components
{
    /// <summary>
    /// A Tracked joint for use with the SkeletonOverlay. A tracked Joint
    /// Contains 2 brushes and a jointID.
    /// </summary>
    public class TrackedJoint
    {
        public Brush background {get; set;}
        public Brush foreground {get; set;}
        public JointType joint {get; set;}

        public TrackedJoint(JointType joint, Brush background, Brush foreground)
        {
            this.joint = joint;
            this.background = background;
            this.foreground = foreground;
        }
    }
}
