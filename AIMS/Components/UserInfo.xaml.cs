﻿/****************************************************************************
 * 
 *  Class: UserInfo
 *  Author: Mitchel Weate
 *  
 *  UserInfo displays a logo, user name, and organization name.
 *   
 * ***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for UserInfo.xaml
    /// </summary>
    public partial class UserInfo : UserControl
    {
        public UserInfo()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();

                this.UserName.Text = String.Format("{0} {1}", ((App)App.Current).CurrentUserFirstName, ((App)App.Current).CurrentUserLastName);
                this.Organization.Text = ((App)App.Current).CurrentUserOrganizationName;
            }
        }
    }
}
