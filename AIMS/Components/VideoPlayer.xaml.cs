﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace AIMS.Components
{
    /// <summary>
    /// Interaction logic for videoPlayer.xaml
    /// </summary>
    public partial class VideoPlayer : UserControl
    {
        private enum PlayMode { Play, Pause, Closed };

        private PlayMode playmode = PlayMode.Closed;

        private MediaElement mediaElement;
        public MediaElement MediaElement { get { return mediaElement; } set { mediaElement = value; } }

        // Anim variables
        private DoubleAnimation widthShowAnim;
        private DoubleAnimation heightShowAnim;
        private DoubleAnimation widthHideAnim;
        private DoubleAnimation heightHideAnim;
        private Storyboard showStoryboard;
        private Storyboard hideStoryboard;
        private VisualBrush visualBrush;

        public VideoPlayer()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();

                mediaElement = new MediaElement();
                mediaElement.MediaEnded += video_MediaEnded;
                mediaElement.MediaFailed += video_MediaFailed;
                mediaElement.LoadedBehavior = MediaState.Manual;
                this.Video.Background = new VisualBrush(mediaElement);

                this.Loaded += loaded;
            }
        }

        private void loaded(object sender, RoutedEventArgs e)
        {
            // Set up the animations
            widthShowAnim = new DoubleAnimation();
            widthShowAnim.Name = "WidthShowAnim";
            widthShowAnim.From = 0.0;
            widthShowAnim.To = this.ActualWidth;
            widthShowAnim.Duration = new Duration(TimeSpan.FromSeconds(0.5));
            widthShowAnim.AutoReverse = false;

            if( this.FindName(widthShowAnim.Name) != null )
                this.UnregisterName(widthShowAnim.Name);
            
            this.RegisterName(widthShowAnim.Name, widthShowAnim);

            widthHideAnim = new DoubleAnimation();
            widthHideAnim.Name = "WidthHideAnim";
            widthHideAnim.From = this.ActualWidth;
            widthHideAnim.To = 0.0;
            widthHideAnim.Duration = widthShowAnim.Duration;
            widthHideAnim.AutoReverse = false;

            if( this.FindName(widthHideAnim.Name) != null )
                this.UnregisterName(widthHideAnim.Name);
            this.RegisterName(widthHideAnim.Name, widthHideAnim);

            heightShowAnim = new DoubleAnimation();
            heightShowAnim.Name = "HeightShowAnim";
            heightShowAnim.From = 0.0;
            heightShowAnim.To = this.ActualHeight;
            heightShowAnim.Duration = widthShowAnim.Duration;
            heightShowAnim.AutoReverse = false;
            
            if( this.FindName(this.heightShowAnim.Name) != null )
                this.UnregisterName(heightShowAnim.Name);

                this.RegisterName(heightShowAnim.Name, heightShowAnim);

            heightHideAnim = new DoubleAnimation();
            heightHideAnim.Name = "HeightHideAnim";
            heightHideAnim.From = this.ActualHeight;
            heightHideAnim.To = 0.0;
            heightHideAnim.Duration = widthShowAnim.Duration;
            heightHideAnim.AutoReverse = false;
            

            if( this.FindName(heightHideAnim.Name) != null )
                this.UnregisterName(heightHideAnim.Name);

            this.RegisterName(heightHideAnim.Name, heightHideAnim);

            // Set up storyboard for showing the video pane
            showStoryboard = new Storyboard();
            showStoryboard.Completed += showStoryBoardCompleted;

            showStoryboard.Children.Add(widthShowAnim);
            Storyboard.SetTargetName(widthShowAnim, Video.Name);
            Storyboard.SetTargetProperty(widthShowAnim, new PropertyPath(Border.WidthProperty));

            showStoryboard.Children.Add(heightShowAnim);
            Storyboard.SetTargetName(heightShowAnim, Video.Name);
            Storyboard.SetTargetProperty(heightShowAnim, new PropertyPath(Border.HeightProperty));

            // Set up the storyboard for hiding the video pane
            hideStoryboard = new Storyboard();
            hideStoryboard.Completed += hideStoryBoardCompleted;

            hideStoryboard.Children.Add(widthHideAnim);
            Storyboard.SetTargetName(widthHideAnim, Video.Name);
            Storyboard.SetTargetProperty(widthHideAnim, new PropertyPath(Border.WidthProperty));

            hideStoryboard.Children.Add(heightHideAnim);
            Storyboard.SetTargetName(heightHideAnim, Video.Name);
            Storyboard.SetTargetProperty(heightHideAnim, new PropertyPath(Border.HeightProperty));
        }

        public void setVideoSource(Uri uri)
        {
            mediaElement.Source = uri;
        }

        public void startVideo(object sender, RoutedEventArgs e)
        {

            this.Visibility = System.Windows.Visibility.Visible;

            widthShowAnim.To = this.ActualWidth;
            widthHideAnim.From = this.ActualWidth;

            heightShowAnim.To = this.ActualHeight;
            heightHideAnim.From = this.ActualHeight;

            // Resize the component to take up the whole screen
            showStoryboard.Begin(this);
        }

        private void showStoryBoardCompleted(object sender, EventArgs e)
        {
            playmode = PlayMode.Play;
            mediaElement.Play();
        }

        private void hideStoryBoardCompleted(object sender, EventArgs e)
        {
            //this.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void video_MediaEnded(object sender, RoutedEventArgs e)
        {
            CloseVideo();
        }

        private void video_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(String.Format("VideoPlayer - Media Failed: {0}", e.ErrorException));

            CloseVideo();
        }

        /// <summary>
        /// Stops the video (resetting it to be played again from the begining), sets the playmode to closed, and begins the hideStoreboard animation.
        /// </summary>
        public void CloseVideo()
        {
            if (playmode != PlayMode.Closed)
            {
                playmode = PlayMode.Closed;
                mediaElement.Stop();
                mediaElement.Close();

                // start the storyboard to hide the component
                hideStoryboard.Begin(this);
            }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            CloseVideo();
        }
    }
}
