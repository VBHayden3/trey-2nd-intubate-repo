﻿/****************************************************************************
 * 
 *  Class: ZoneOverlay
 *  Author: Mitchel Weate
 *  
 *  ZoneOverlay is used to paint tracked joints over the colored image.
 *  TrackedJoint is used to specify which joints should be drawn as well as
 *  how they should be drawn.  
 *   
 * ***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;

namespace AIMS.Components
{
    /// <summary>
    /// Skeleton overlay will display specified joints for the skeleton that is passed in.
    /// </summary>
    public partial class ZoneOverlay : UserControl
    {
        int colorWidth = 1920;
        int colorHeight = 1080;

        public static readonly DependencyProperty ShowZoneOverlaysProperty =
            DependencyProperty.Register("ShowZoneOverlays", typeof(bool), typeof(ZoneOverlay), new PropertyMetadata(false, OverlaySettingChanged));
        
        public static readonly DependencyProperty ShowZoneTextOverlaysProperty =
            DependencyProperty.Register("ShowZoneTextOverlays", typeof(bool), typeof(ZoneOverlay), new PropertyMetadata(false, OverlaySettingChanged));

        public static readonly DependencyProperty ShowZonesAsOutlinesProperty =
            DependencyProperty.Register("ShowZonesAsOutlines", typeof(bool), typeof(ZoneOverlay), new PropertyMetadata(false, OverlaySettingChanged));

        public bool ShowZoneOverlays
        {
            get { return (bool)GetValue(ShowZoneOverlaysProperty); }
            set { SetValue(ShowZoneOverlaysProperty, value);}
        }

        public bool ShowZoneTextOverlays
        { 
            get { return (bool)GetValue(ShowZoneTextOverlaysProperty); }
            set { SetValue(ShowZoneTextOverlaysProperty, value); }
        }

        public bool ShowZonesAsOutlines
        {
            get { return (bool)GetValue(ShowZonesAsOutlinesProperty); }
            set { SetValue(ShowZonesAsOutlinesProperty, value); }
        }

        private List<Zone> trackedZones;

        private readonly SolidColorBrush greenOpaqueBrush = new SolidColorBrush(Color.FromArgb(130, 0, 255, 0));
        private readonly SolidColorBrush redOpaqueBrush = new SolidColorBrush(Color.FromArgb(130, 255, 0, 0)); 
        private readonly SolidColorBrush purpleOpaqueBrush = new SolidColorBrush(Color.FromArgb(65, 128, 0, 255));

        private readonly SolidColorBrush greenBrush = new SolidColorBrush(Color.FromArgb(255, 0, 255, 0));
        private readonly SolidColorBrush redBrush = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
        private readonly SolidColorBrush purpleBrush = new SolidColorBrush(Color.FromArgb(255, 128, 0, 255));

        public ZoneOverlay()
        {
            InitializeComponent();
            trackedZones = new List<Zone>();
        }
        
        public void UpdateZones()
        {
            this.InvalidateVisual();
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            //System.Diagnostics.Debug.WriteLine("ZoneOverlay -> OnRender()");

            base.OnRender(drawingContext);

            double scaleX = ActualWidth / colorWidth;
            double scaleY = ActualHeight / colorHeight;

            double thickness = 1.25;

            // all dimensions need to be resized to 1920 from 640
            double sizeMultiplier = 1.0;

            if (ShowZoneOverlays)
            {
                //System.Diagnostics.Debug.WriteLine("ZoneOverlay -> OnRender() showing zone overlays...");

                for (int i = 0; i < trackedZones.Count; i++)
                {
                    Pen pen;
                    SolidColorBrush brush;

                    // Only draw Active zones.
                    if (trackedZones[i].Active)
                    {
                        if (trackedZones[i].GetType() == typeof(PolyZone))
                        {
                            Console.WriteLine("Trying to display polyzone");
                        }

                        // Figure out how to display the polygon
                        if (ShowZonesAsOutlines)
                        {
                            pen = new Pen(redBrush, thickness);

                            brush = new SolidColorBrush(Colors.Transparent);

                        }
                        else
                        {
                            if (trackedZones[i].IsWithinZone && trackedZones[i].IsImproperZone)
                            {
                                brush = redOpaqueBrush;
                                pen = new Pen(redBrush, thickness);
                            }
                            else if (trackedZones[i].IsWithinZone && !trackedZones[i].IsImproperZone)
                            {
                                brush = greenOpaqueBrush;
                                pen = new Pen(greenBrush, thickness);
                            }
                            else
                            {
                                brush = purpleOpaqueBrush;
                                pen = new Pen(purpleBrush, thickness);
                            }
                        }

                        // Build the drawing visual
                        PathGeometry geometry = new PathGeometry();
                        geometry.FillRule = FillRule.EvenOdd;
                        Point start = new Point(trackedZones[i].Points[0].X * scaleX * sizeMultiplier, trackedZones[i].Points[0].Y * scaleY * sizeMultiplier);
                        System.Diagnostics.Debug.WriteLine(
                            "ZoneOverlay -> " + 
                            trackedZones[i].Name + 
                            " TopLeft XY:" + 
                            String.Format("{0},{1}", (int)start.X, (int)start.Y));
                        LineSegment[] segments = new LineSegment[trackedZones[i].Points.Count - 1];
                        for (int j = 1; j < trackedZones[i].Points.Count; j++)
                        {
                            segments[j - 1] = new LineSegment(new Point(trackedZones[i].Points[j].X * scaleX * sizeMultiplier, trackedZones[i].Points[j].Y * scaleY * sizeMultiplier), true);
                        }
                        PathFigure figure = new PathFigure(start, segments, true);
                        geometry.Figures.Add(figure);

                        drawingContext.DrawGeometry(brush, pen, geometry);

                            //drawingContext.DrawText( new FormattedText(trackedZones[i].ToString(), System.Globalization.CultureInfo.CurrentUICulture, System.Windows.FlowDirection.LeftToRight, new Typeface("Arial"), 10.0, Brushes.White), new Point(trackedZones[i].X * xscale, trackedZones[i].Y * yscale));
                        // Add text if ZoneTextOverlay on
                        if (this.ShowZoneTextOverlays)
                        {
                            //System.Diagnostics.Debug.WriteLine("ZoneOverlay -> ShowZoneTextOverlays at " + trackedZones[i].X + "x" + trackedZones[i].Y);
                            drawingContext.DrawText(new FormattedText(trackedZones[i].ToString(), System.Globalization.CultureInfo.CurrentCulture,
                                System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Bold,
                                FontStretches.Condensed), 14.0, Brushes.White), new Point(trackedZones[i].X * scaleX * sizeMultiplier, trackedZones[i].Y * scaleY * sizeMultiplier));
                        }
                    }
                }
            }
        }

        public void ClearZones()
        {
            trackedZones.Clear();
        }

        public void AddZone(Zone zone)
        {
            trackedZones.Add(zone);
        }

        public void RemoveZone(Zone zone)
        {
            if (trackedZones.Contains(zone))
            {
                trackedZones.Remove(zone);
            }
        }

        private static void OverlaySettingChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            ZoneOverlay overlay = sender as ZoneOverlay;

            if (sender != null)
            {
                overlay.InvalidateVisual();
            }
        }

        // Converts the point from the dictionary into pixel coordinates so that it can be drawn.
        private Point Get2DPosition(Point point)
        {
            point.X = point.X * this.RenderSize.Width;
            point.Y = point.Y * this.RenderSize.Height;

            System.Diagnostics.Debug.WriteLine("ZoneOverlay -> Get2DPosition Point XY:" + String.Format("{0},{1}", point.X, point.Y));

            return point;
        }

        // Converts the point from the dictionary into pixel coordinates so that it can be drawn.
        private Point Get2DPosition(Joint joint)
        {
            ColorSpacePoint colorPoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(joint.Position);

            Point point = new Point();
            point.X = (double)colorPoint.X / KinectManager.ColorWidth;
            point.Y = (double)colorPoint.Y / KinectManager.ColorHeight;

            point.X = point.X * this.RenderSize.Width;
            point.Y = point.Y * this.RenderSize.Height;

            System.Diagnostics.Debug.WriteLine("ZoneOverlay -> Get2DPosition Joint XY:" + String.Format("{0}{1}", point.X, point.Y));
            return point;
        }
    }
}
