﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace AIMS.Components
{
    public class ZoomBorder : Border
    {
        private UIElement child = null;
        private Point origin;
        private Point start;

        private bool dragging = false;

        public bool Dragging { get { return dragging; } }   // Exposed dragging feild in a property which allows other controls to determine if a drag is currently occuring or not. This is useful in MouseUp and MouseMove events.

        public double ZoomMin
        {
            get { return (double)GetValue(ZoomMinProperty); }
            set { SetValue(ZoomMinProperty, value); }
        }
        public static readonly DependencyProperty ZoomMinProperty =
            DependencyProperty.Register("ZoomMin", typeof(double), typeof(ColorTrackerOverlay), new PropertyMetadata(1.0, null));

        public double ZoomMax
        {
            get { return (double)GetValue(ZoomMaxProperty); }
            set { SetValue(ZoomMaxProperty, value); }
        }
        public static readonly DependencyProperty ZoomMaxProperty =
            DependencyProperty.Register("ZoomMax", typeof(double), typeof(ColorTrackerOverlay), new PropertyMetadata(5.0, null));

        public double Zoom
        {
            get { return (double)GetValue(ZoomProperty); }
            set { SetValue(ZoomProperty, value); }
        }
        public static readonly DependencyProperty ZoomProperty =
            DependencyProperty.Register("Zoom", typeof(double), typeof(ColorTrackerOverlay), new PropertyMetadata(1.0, null));

        public double ZoomInterval
        {
            get { return (double)GetValue(ZoomIntervalProperty); }
            set { SetValue(ZoomIntervalProperty, value); }
        }
        public static readonly DependencyProperty ZoomIntervalProperty =
            DependencyProperty.Register("ZoomInterval", typeof(double), typeof(ColorTrackerOverlay), new PropertyMetadata(0.2, null));

        public double PanThreshold
        {
            get { return (double)GetValue(PanThresholdProperty); }
            set { SetValue(PanThresholdProperty, value); }
        }
        public static readonly DependencyProperty PanThresholdProperty =
            DependencyProperty.Register("PanThreshold", typeof(double), typeof(ColorTrackerOverlay), new PropertyMetadata(5.0, null));

        private TranslateTransform GetTranslateTransform(UIElement element)
        {
            return (TranslateTransform)((TransformGroup)element.RenderTransform)
              .Children.First(tr => tr is TranslateTransform);
        }

        private ScaleTransform GetScaleTransform(UIElement element)
        {
            return (ScaleTransform)((TransformGroup)element.RenderTransform)
              .Children.First(tr => tr is ScaleTransform);
        }

        public override UIElement Child
        {
            get { return base.Child; }
            set
            {
                if (value != null && value != this.Child)
                    this.Initialize(value);
                base.Child = value;
            }
        }

        public void Initialize(UIElement element)
        {
            this.child = element;

            if (child != null)
            {
                TransformGroup group = new TransformGroup();
                ScaleTransform st = new ScaleTransform();
                group.Children.Add(st);
                TranslateTransform tt = new TranslateTransform();
                group.Children.Add(tt);
                child.RenderTransform = group;
                child.RenderTransformOrigin = new Point(0.0, 0.0);
                this.MouseWheel += child_MouseWheel;
                this.MouseLeftButtonDown += child_MouseLeftButtonDown;
                this.MouseLeftButtonUp += child_MouseLeftButtonUp;
                this.MouseMove += child_MouseMove;
                this.PreviewMouseRightButtonDown += new MouseButtonEventHandler(child_PreviewMouseRightButtonDown);
                this.PreviewMouseUp += new MouseButtonEventHandler(ZoomBorder_MouseUp);
            }
        }


        public void Reset()
        {
            if (child != null)
            {
                // reset zoom
                var st = GetScaleTransform(child);
                st.ScaleX = 1.0;
                st.ScaleY = 1.0;

                // reset pan
                var tt = GetTranslateTransform(child);
                tt.X = 0.0;
                tt.Y = 0.0;
            }
        }

        #region Child Events

        private void child_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (child != null)
            {
                var st = GetScaleTransform(child);
                var tt = GetTranslateTransform(child);

                double zoom = e.Delta > 0 ? ZoomInterval : -ZoomInterval;

                if (e.Delta > 0 && (st.ScaleX >= ZoomMax || st.ScaleY >= ZoomMax))
                    return;
                if (e.Delta < 0 && (st.ScaleX <= ZoomMin || st.ScaleY <= ZoomMin))
                    return;

                Point relative = e.GetPosition(child);
                double abosuluteX;
                double abosuluteY;

                abosuluteX = relative.X * st.ScaleX + tt.X;
                abosuluteY = relative.Y * st.ScaleY + tt.Y;

                Zoom = st.ScaleX + zoom;

                st.ScaleX = Zoom;
                st.ScaleY = Zoom;

                tt.X = abosuluteX - relative.X * st.ScaleX;
                tt.Y = abosuluteY - relative.Y * st.ScaleY;
            }
        }

        public void ZoomInFromCenter()
        {
            if (child != null)
            {
                var st = GetScaleTransform(child);
                var tt = GetTranslateTransform(child);

                double zoom = -ZoomInterval;

                if (st.ScaleX <= ZoomMin || st.ScaleY <= ZoomMin)
                    return;

                Point relative = new Point(0, 0);
                double abosuluteX;
                double abosuluteY;

                abosuluteX = relative.X * st.ScaleX + tt.X;
                abosuluteY = relative.Y * st.ScaleY + tt.Y;

                Zoom = st.ScaleX + zoom;

                st.ScaleX = Zoom;
                st.ScaleY = Zoom;

                tt.X = abosuluteX - relative.X * st.ScaleX;
                tt.Y = abosuluteY - relative.Y * st.ScaleY;
            }
        }

        public void ZoomOutFromCenter()
        {
            if (child != null)
            {
                var st = GetScaleTransform(child);
                var tt = GetTranslateTransform(child);

                double zoom = ZoomInterval;

                if (st.ScaleX >= ZoomMax || st.ScaleY >= ZoomMax)
                    return;

                Point relative = new Point(0, 0);
                double abosuluteX;
                double abosuluteY;

                abosuluteX = relative.X * st.ScaleX + tt.X;
                abosuluteY = relative.Y * st.ScaleY + tt.Y;

                Zoom = st.ScaleX + zoom;

                st.ScaleX = Zoom;
                st.ScaleY = Zoom;

                tt.X = abosuluteX - relative.X * st.ScaleX;
                tt.Y = abosuluteY - relative.Y * st.ScaleY;
            }
        }

        private void child_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (child != null)
            {
                var tt = GetTranslateTransform(child);
                start = e.GetPosition(this);
                origin = new Point(tt.X, tt.Y);
                child.CaptureMouse();
            }
        }

        private void child_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (child != null)
            {
                child.ReleaseMouseCapture();
                this.Cursor = Cursors.Arrow;

                dragging = false;   // end drag
            }
        }

        void child_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            //this.Reset();
        }

        void ZoomBorder_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Middle)
            {
                if (!this.Dragging)    // Add support to reset scale and translation to the MiddleMouseButton.
                    this.Reset();
            }
        }

        private void child_MouseMove(object sender, MouseEventArgs e)
        {
            if (child != null)
            {
                if (child.IsMouseCaptured)
                {
                    Vector v = start - e.GetPosition(this);

                    if (System.Math.Abs(v.X) > this.PanThreshold || System.Math.Abs(v.Y) > this.PanThreshold)  // Adds a minimum movement requirement to reduce unintentional drags when clicking. If the dragging has already begun, don't check threshold.
                    {
                        if (!dragging)
                        {
                            this.Cursor = Cursors.Hand;
                            dragging = true;    // start the drag.
                        }

                        var tt = GetTranslateTransform(child);
                        tt.X = origin.X - v.X;
                        tt.Y = origin.Y - v.Y;
                    }
                    
                }
            }
        }
        #endregion
    }
}