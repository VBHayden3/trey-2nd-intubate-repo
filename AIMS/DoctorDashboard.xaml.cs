﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIMS
{
    /// <summary>
    /// Interaction logic for DoctorDashboard.xaml
    /// </summary>
    public partial class DoctorDashboard : Page
    {
        private List<Course> courseList;
        public List<Course> CourseList { get { return courseList; } set { courseList = value; } }
        private int selectedCourseIndex = 0;
        public int SelectedCourseIndex { get { return selectedCourseIndex; } }

        public DoctorDashboard()
        {
            InitializeComponent();

            Loaded += DoctorDashboard_Loaded;
            Unloaded += DoctorDashboard_Unloaded;
        }

        private void DoctorDashboard_Loaded(object sender, RoutedEventArgs e)
        {
            PopulateCourseButtonList();
        }

        private void DoctorDashboard_Unloaded(object sender, RoutedEventArgs e)
        {

        }

        private void ResultsButton_Click(object sender, RoutedEventArgs e)
        {
            SetCurrentlyShownClassOverlay(this.DoctorCourseResultsWindow);
        }

        private void AssignmentsButton_Click(object sender, RoutedEventArgs e)
        {
            SetCurrentlyShownClassOverlay(this.AssignmentsWindow);
        }

        private void RosterButton_Click(object sender, RoutedEventArgs e)
        {
            SetCurrentlyShownClassOverlay(this.DoctorCourseRosterWindow);
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void LogoutButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void classButton_Click(object sender, RoutedEventArgs e)
        {
            Button clicked = e.OriginalSource as Button;

            //MessageBox.Show(clicked.ToString() + " clicked.");

            // Set the selected course index to the index of the button within the CourseButtonsPanel.
            this.selectedCourseIndex = this.CourseButtonsPanel.Items.IndexOf(clicked);

            // Update the dashboard's information feilds with course information at the selected course index.
            this.UpdateDashboardCourseInformation(courseList[selectedCourseIndex]);
        }

        /// <summary>
        /// Fills the course buttons list with courses which link with the currently logged in doctor.
        /// </summary>
        /// <param name="result"></param>
        public void PopulateCourseButtonList()
        {
            if (((App)App.Current).CurrentDoctor == null)
                return;

            // Get all courses pertaining to this doctor.
            courseList = ((App)App.Current).CurrentDoctor.GetCoursesNoRoster();

            if (courseList.Count > 0)
            {
                // Clear out old buttons, as well as the NoCoursesFoundTextBlock, before re-populating StageScore button list.
                CourseButtonsPanel.Items.Clear();

                // Repopulate the results stage button list from the passed in result.
                for (int i = 0; i < courseList.Count; i++)
                {
                    System.Windows.Controls.Button bttn = new Button();
                    bttn.Uid = courseList[i].ID.ToString();
                    bttn.Content = courseList[i].Name;
                    CourseButtonsPanel.Items.Add(bttn);
                }

                UpdateDashboardCourseInformation(courseList[0]);
            }
            else
            {
                // Leave the NoCoursesFoundTextBlock as the only child item. Then also open up the AddCoursesComponent.
                this.AddClassWindow.ShowOverlay();
            }
        }

        public void UpdateDashboardCourseInformation( Course course )
        {
            if (course != null)
            {
                SelectedClassTitleText.Text = course.Name;

                FillRosterTable(course.Roster);
            }
        }

        public void FillRosterTable(Roster roster)
        {
            if (roster != null)
            {

            }
        }

        private void SendMessageButton_Click(object sender, RoutedEventArgs e)
        {
            SetCurrentlyShownClassOverlay(this.SendMessageOverlay);
        }

        private Course GetCurrentlySelectedCourse()
        {
            return courseList[selectedCourseIndex];
        }

        private Student GetCurrentlySelectedStudent()
        {
            Student student = null;

            // NOT YET IMPLEMENTED
            student = Student.GetStudentFromStudentID(1, true); // TEMPORARY! Creates a Student object filled with the details of the #1 student in our database (John Doe of Account "test").

            return student;
        }

        private void ShowSendMessageOverlay( Student toStudent, Course relatedCourse )
        {
            this.SendMessageOverlay.ShowOverlay( toStudent, relatedCourse );
        }

        private void AddClassButton_Click(object sender, RoutedEventArgs e)
        {
            SetCurrentlyShownClassOverlay(this.AddClassWindow);
        }

        public void SetCurrentlyShownClassOverlay(UserControl control)
        {
            if (control == null || courseList == null)
                return;

            if (control == this.AddClassWindow)
            {
                // Cancel the other overlays
                this.SendMessageOverlay.CancelOverlay();
                this.DoctorCourseResultsWindow.CancelOverlay();
                this.DoctorCourseRosterWindow.CancelOverlay();
                this.AssignmentsWindow.CancelOverlay();
               this.DoctorIntubationStatsView.CancelOverlay();

                // Show the overlay
                this.AddClassWindow.ShowOverlay();
            }
            else if (control == this.SendMessageOverlay)
            {
                // Cancel the other overlays
                this.AddClassWindow.CancelOverlay();
                this.DoctorCourseResultsWindow.CancelOverlay();
                this.DoctorCourseRosterWindow.CancelOverlay();
                this.AssignmentsWindow.CancelOverlay();
               this.DoctorIntubationStatsView.CancelOverlay();

                // Show the overlay
                ShowSendMessageOverlay(GetCurrentlySelectedStudent(), GetCurrentlySelectedCourse());
            }
            else if (control == this.DoctorCourseResultsWindow)
            {
                // Cancel the other overlays
                this.SendMessageOverlay.CancelOverlay();
                this.AddClassWindow.CancelOverlay();
                this.DoctorCourseRosterWindow.CancelOverlay();
                this.AssignmentsWindow.CancelOverlay();
               this.DoctorIntubationStatsView.CancelOverlay();

                // Show the overlay
                if (courseList.Count > 0 && System.Math.Max(0, selectedCourseIndex) < courseList.Count)
                {
                    this.DoctorCourseResultsWindow.ShowOverlay(courseList[selectedCourseIndex]);  // currently selected course
                   this.DoctorIntubationStatsView.ShowOverlay(GetCurrentlySelectedStudent());
                }
                else
                {
                    MessageBox.Show("This button has no effect because there is no currently selected course.");
                }
            }
            else if (control == this.DoctorCourseRosterWindow)
            {
                // Cancel the other overlays
                this.SendMessageOverlay.CancelOverlay();
                this.AddClassWindow.CancelOverlay();
                this.DoctorCourseResultsWindow.CancelOverlay();
                this.AssignmentsWindow.CancelOverlay();
               this.DoctorIntubationStatsView.CancelOverlay();

                // Show the overlay
                if (courseList.Count > 0 && System.Math.Max(0, selectedCourseIndex) < courseList.Count)
                {
                    this.DoctorCourseRosterWindow.ShowOverlay(courseList[selectedCourseIndex]);  // currently selected course
                }
                else
                {
                    MessageBox.Show("This button has no effect because there is no currently selected course.");
                }
            }
            else if (control == this.AssignmentsWindow)
            {
                // Cancel the other overlays
                this.SendMessageOverlay.CancelOverlay();
                this.AddClassWindow.CancelOverlay();
                this.DoctorCourseResultsWindow.CancelOverlay();
                this.DoctorCourseRosterWindow.CancelOverlay();
               this.DoctorIntubationStatsView.CancelOverlay();

                // Show the overlay
                this.AssignmentsWindow.ShowOverlay();
            }
        }
    }
}
