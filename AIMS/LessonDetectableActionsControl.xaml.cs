﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AIMS.Watchers;
using System.Collections.ObjectModel;

namespace AIMS
{
    /// <summary>
    /// Interaction logic for LessonDetectableActionsControl.xaml
    /// </summary>
    public partial class LessonDetectableActionsControl : UserControl
    {
        private DetectableActionCollection actionCollection;
        public DetectableActionCollection ActionCollection { get { return actionCollection; } private set { actionCollection = value; } }

        public ObservableCollection<DetectableActionGroup> ActionGroups { get { return ActionCollection == null ? null : ActionCollection.ActionGroups; } }

        public LessonDetectableActionsControl()
        {
            InitializeComponent();

            this.DataContext = this;
        }

        public void SetActionCollection( DetectableActionCollection actionCollection )
        {
            if (this.actionCollection != null)
            {
                this.actionCollection = null;
            }

            this.ActionCollection = actionCollection;

            this.SetupActionCollectionVisual();

            this.listview.ItemsSource = this.ActionCollection.ActionGroups;
            //this.treeview.ItemsSource = this.ActionCollection.ActionGroups;

           // this.UpdateActionCollectionDetails();
        }

        public void SetupActionCollectionVisual()
        {
            /*
            if (this.ActionCollection != null)
            {
                foreach (DetectableActionGroup group in this.ActionCollection.ActionGroups)
                {
                    if (group != null)
                    {
                        Expander groupExpander = new Expander();
                        groupExpander.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                        groupExpander.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Stretch;
                        groupExpander.IsExpanded = true;
                        groupExpander.Background = App.Current.Resources["AIMSBlueBrush"] as SolidColorBrush;

                        TextBlock groupNameTB = new TextBlock();
                        groupNameTB.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                        groupNameTB.Text = group.Name;
                        groupExpander.Header = groupNameTB;

                        StackPanel groupPanel = new StackPanel();
                        groupPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                        groupExpander.Content = groupPanel;

                        if (group.DetectableActionCategories != null)
                        {
                            foreach (DetectableActionCategory category in group.DetectableActionCategories)
                            {
                                Expander categoryExpander = new Expander();
                                categoryExpander.Background = (category.Detected ? Brushes.Lime : App.Current.Resources["AIMSLightPurpleBrush"] as SolidColorBrush);
                                TextBlock categoryNameTB = new TextBlock();
                                categoryNameTB.Text = category.Name;
                                categoryNameTB.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                                categoryExpander.Header = categoryNameTB;
                                StackPanel categoryPanel = new StackPanel();
                                categoryPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                                categoryExpander.Content = categoryPanel;
                                groupPanel.Children.Add(categoryExpander);

                                if (category.ProperActions != null)
                                {
                                    foreach (DetectableAction proper in category.ProperActions)
                                    {
                                        Border properBorder = new Border();
                                        properBorder.Background = (proper.Detected ? DetectableActionCategory.ProperTriggerBrush : DetectableActionCategory.ProperUntriggerBrush);
                                        TextBlock properNameTB = new TextBlock();
                                        properNameTB.Text = proper.Name;
                                        properNameTB.TextAlignment= TextAlignment.Center;
                                        properBorder.Child = properNameTB;
                                        categoryPanel.Children.Add(properBorder);
                                    }
                                }

                                if (category.ImproperActions != null)
                                {
                                    foreach (DetectableAction improper in category.ImproperActions)
                                    {
                                        Border improperBorder = new Border();
                                        improperBorder.Background = (improper.Detected ? DetectableActionCategory.InproperTriggerBrush : DetectableActionCategory.InproperUntriggerBrush);
                                        TextBlock improperNameTB = new TextBlock();
                                        improperNameTB.Text = improper.Name;
                                        improperNameTB.TextAlignment = TextAlignment.Center;
                                        improperBorder.Child = improperNameTB;
                                        categoryPanel.Children.Add(improperBorder);
                                    }
                                }

                                if (category.Inactions != null)
                                {
                                    foreach (DetectableAction inaction in category.Inactions)
                                    {
                                        Border inactionBorder = new Border();
                                        inactionBorder.Background = (inaction.Detected ? DetectableActionCategory.InactionTriggerBrush : DetectableActionCategory.InactionUntriggerBrush);
                                        TextBlock inactionNameTB = new TextBlock();
                                        inactionNameTB.Text = inaction.Name;
                                        inactionNameTB.TextAlignment = TextAlignment.Center;
                                        inactionBorder.Child = inactionNameTB;
                                        categoryPanel.Children.Add(inactionBorder);
                                    }
                                }
                            }
                        }

                        ActionsWrapPanel.Children.Add(groupExpander);
                    }
                }
            }*/
        }

        public void UpdateActionCollectionDetails()
        {
            /*
            if (this.ActionCollection != null)
            {
                foreach (DetectableActionGroup group in this.ActionCollection.ActionGroups)
                {
                    if (group != null)
                    {
                        Expander groupExpander = new Expander();
                        groupExpander.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                        groupExpander.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Stretch;
                        groupExpander.IsExpanded = true;
                        groupExpander.Background = App.Current.Resources["AIMSBlueBrush"] as SolidColorBrush;

                        TextBlock groupNameTB = new TextBlock();
                        groupNameTB.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                        groupNameTB.Text = group.Name;
                        groupExpander.Header = groupNameTB;

                        StackPanel groupPanel = new StackPanel();
                        groupPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                        groupExpander.Content = groupPanel;

                        if (group.DetectableActionCategories != null)
                        {
                            foreach (DetectableActionCategory category in group.DetectableActionCategories)
                            {
                                Expander categoryExpander = new Expander();
                                categoryExpander.Background = (category.Detected ? Brushes.Lime : App.Current.Resources["AIMSLightPurpleBrush"] as SolidColorBrush);
                                TextBlock categoryNameTB = new TextBlock();
                                categoryNameTB.Text = category.Name;
                                categoryNameTB.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                                categoryExpander.Header = categoryNameTB;
                                StackPanel categoryPanel = new StackPanel();
                                categoryPanel.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                                categoryExpander.Content = categoryPanel;
                                groupPanel.Children.Add(categoryExpander);

                                if (category.ProperActions != null)
                                {
                                    foreach (DetectableAction proper in category.ProperActions)
                                    {
                                        Border properBorder = new Border();
                                        properBorder.Background = (proper.Detected ? DetectableActionCategory.ProperTriggerBrush : DetectableActionCategory.ProperUntriggerBrush);
                                        TextBlock properNameTB = new TextBlock();
                                        properNameTB.Text = proper.Name;
                                        properNameTB.TextAlignment = TextAlignment.Center;
                                        properBorder.Child = properNameTB;
                                        categoryPanel.Children.Add(properBorder);
                                    }
                                }

                                if (category.ImproperActions != null)
                                {
                                    foreach (DetectableAction improper in category.ImproperActions)
                                    {
                                        Border improperBorder = new Border();
                                        improperBorder.Background = (improper.Detected ? DetectableActionCategory.InproperTriggerBrush : DetectableActionCategory.InproperUntriggerBrush);
                                        TextBlock improperNameTB = new TextBlock();
                                        improperNameTB.Text = improper.Name;
                                        improperNameTB.TextAlignment = TextAlignment.Center;
                                        improperBorder.Child = improperNameTB;
                                        categoryPanel.Children.Add(improperBorder);
                                    }
                                }

                                if (category.Inactions != null)
                                {
                                    foreach (DetectableAction inaction in category.Inactions)
                                    {
                                        Border inactionBorder = new Border();
                                        inactionBorder.Background = (inaction.Detected ? DetectableActionCategory.InactionTriggerBrush : DetectableActionCategory.InactionUntriggerBrush);
                                        TextBlock inactionNameTB = new TextBlock();
                                        inactionNameTB.Text = inaction.Name;
                                        inactionNameTB.TextAlignment = TextAlignment.Center;
                                        inactionBorder.Child = inactionNameTB;
                                        categoryPanel.Children.Add(inactionBorder);
                                    }
                                }
                            }
                        }

                        ActionsWrapPanel.Children.Add(groupExpander);
                    }
                }
            }

            this.InvalidateVisual();*/
        }
    }
}
