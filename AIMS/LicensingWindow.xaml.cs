﻿using Reprise;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Diagnostics;
using System.Timers;

namespace AIMS
{
    /// <summary>
    /// Interaction logic for LicensingWindow.xaml
    /// </summary>
    public partial class LicensingWindow : Window, INotifyPropertyChanged
    {
        private string status = "";
        public string Status { get { return status; } set { if (status == value) status = value; else { status = value; NotifyPropertyChanged("Status"); } } }

        [Obfuscation(Feature = "virtualization", Exclude = false)]
        private readonly TimeSpan OnlineValidationFrequency = TimeSpan.FromDays(30.0);

        // Keep track of the LoginPage that  created us
        private LoginPage loginPage;

        // Should we automatically advance to the dashboard if a  license is found?
        private bool autoAdvance = true;

        private bool hasProductLicense = false;
        private bool hasTrialLicense = false;
        private bool hasTimeLicense = false;

        // Background worker for license validation
        private BackgroundWorker licenseWorker;

        public LicensingWindow(LoginPage caller)
        {
            InitializeComponent();

            this.loginPage = caller;
            this.DataContext = this;

            // Hide this window
            this.Visibility = Visibility.Hidden;
            this.BusyBar.Visibility = Visibility.Hidden;

            // Disable the Continue Button
            this.ContinueButton.IsEnabled = false;

            // Initialize our worker
            if (licenseWorker == null)
            {
                // Declare the  instance and assign its  callbacks
                licenseWorker = new BackgroundWorker();
                licenseWorker.WorkerReportsProgress = true;
                licenseWorker.WorkerSupportsCancellation = true;
                licenseWorker.DoWork += licenseWorker_DoWork;
                licenseWorker.ProgressChanged += licenseWorker_ProgressChanged;
                licenseWorker.RunWorkerCompleted += licenseWorker_RunWorkerCompleted;
            }
        }

        [Obfuscation(Feature = "virtualization", Exclude = false)]
        void LicensingWindow_Loaded(object sender, RoutedEventArgs e)
        {
            // Nothing
        }

        //  Start the Worker
        public void StartLicenseCheck(string manualLicense = null)
        {
            if (licenseWorker.IsBusy != true)
            {
                // Show the  window when we start
                this.Visibility = Visibility.Visible;
                this.BusyBar.Visibility = Visibility.Visible;

                // Start the asynchronous operation.
                licenseWorker.RunWorkerAsync(manualLicense);

                // Disable the Continue Button
                this.ContinueButton.IsEnabled = false;
            }
        }

        private void CancelLicenseCheck()
        {
            if (licenseWorker.WorkerSupportsCancellation == true)
            {
                // Cancel the asynchronous operation.
                licenseWorker.CancelAsync();
            }
        }

        // This event handler updates the progress.
        private void licenseWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // Change the perceived progress
            this.ProgressBar.Value = e.ProgressPercentage;

            // Check if we got a string
            if (e.UserState != null)
            {
                StatusTB.Text = e.UserState.ToString();
            }
        }

        // This event handler deals with the results of the background operation.
        private void licenseWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // Stop showing the busybar
            this.BusyBar.Visibility = Visibility.Hidden;

            // Check if we got any errors
            if (e.Cancelled == true)
            {
                // We must have cancelled the process
                StatusTB.Text = "License Check cancelled!";
            }
            else if (e.Error != null)
            {
                // In the weird situation there was an error, print it
                StatusTB.Text = String.Format("ERROR: {0}, {1}", e.Error.Source, e.Error.Message);
            }
            else if (e.Result != null)
            {
                // If this result isn't true (although this should never happen), break off
                if ((bool)e.Result != true)
                    return;

                // Check if we should advance
                if (autoAdvance)
                {
                    // Move this to the dashboard
                    this.Visibility = Visibility.Hidden;
                    this.Close();
                    this.loginPage.GoToDashboard();
                }
                else
                {
                    // Just enable the continue button
                    this.ContinueButton.IsEnabled = true;
                }
            }
        }

        // This event handler is where the time-consuming work is done.
        //
        // THIS IS (ALMOST WORD FOR WORD) ValidateLicenses() from CPR
        private void licenseWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // Reference the worker executing this block
            BackgroundWorker worker = sender as BackgroundWorker;

            // Keep track of whether or not to advance to dashboard
            bool shouldAdvance = false;

            // Report
            worker.ReportProgress(10, "Attempting license validation...");

            try
            {
                // Get the license file directory.
                string dir = GetLicenseFolderLocation();
                
                // Init RLM to look into 
                ((App)App.Current).rh = RLM.rlm_init(dir, dir, null);

                // Report
                worker.ReportProgress(20, "Checking license directory ...");
                System.Threading.Thread.Sleep(100);

                int trialStat = RLM.RLM_EL_NOPRODUCT;
                int productStat = RLM.RLM_EL_NOPRODUCT;
                int timeStat = RLM.RLM_EL_NOPRODUCT;

                // Report
                worker.ReportProgress(30, "Checking for valid license ...");
                System.Threading.Thread.Sleep(100);

                // Validate Trial License
                hasTrialLicense = ValidateTrialLicense(true, out trialStat);

                // Validate Product License
                hasProductLicense = ValidateProductLicense(true, out productStat);

                // Report
                worker.ReportProgress(35);

                // If we have a trial license, continue
                if (hasTrialLicense)
                {
                    SetStatus(String.Format("Trial valid for {0} {1}.", ((App)App.Current).license_days_remaining, ((App)App.Current).license_days_remaining == 1 ? "day" : "days"));

                    ((App)App.Current).HasTrialLicense = hasTrialLicense;

                    // Report
                    worker.ReportProgress(40);
                    System.Threading.Thread.Sleep(100);

                    shouldAdvance = true;
                }

                // Report
                worker.ReportProgress(50, "Checking for non-trial product license ... ");
                System.Threading.Thread.Sleep(100);

                // Check for  a product license
                if (hasProductLicense)
                {
                    Debug.WriteLine("Product license valid.");

                    // Report
                    worker.ReportProgress(90, "Product license validated.");
                    System.Threading.Thread.Sleep(100);

                    shouldAdvance = true;
                }

                // If we didn't find either...
                if (!shouldAdvance)
                {
                    worker.ReportProgress(80, "Could not find non-trial license ...");
                    System.Threading.Thread.Sleep(100);

                    // No validated product license.
                    string error = RLM.marshalToString(RLM.rlm_errstring(((App)App.Current).lic, ((App)App.Current).rh, new byte[RLM.RLM_ERRSTRING_MAX]));
                    Debug.WriteLine(String.Format("Product license not valid. [{0}]", error));

                    string outputMessage;

                    if (productStat == -1)
                    {
                        outputMessage = String.Format("Product is not licensed.", productStat);
                    }
                    else if (productStat == -22)
                    {
                        outputMessage = "Product license in-use by another running instance.";
                    }
                    else if (productStat == -1030)
                    {
                        outputMessage = "Product license has been disabled.";
                    }
                    else
                    {
                        outputMessage = String.Format("Product license failed to validate. [{0}]", productStat);
                    }

                    worker.ReportProgress(90, outputMessage);
                    System.Threading.Thread.Sleep(100);
                }

                // Report
                worker.ReportProgress(95);
                System.Threading.Thread.Sleep(100);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(e.ToString());
                worker.ReportProgress(10, ex.Source + " :: " + ex.ToString());
            }

            if (shouldAdvance)
            {
                // Reports finished
                worker.ReportProgress(100, "Advancing to Dashboard ... ");

                // Add a parameter to the RunWorkerCompletedArgs
                e.Result = true;
            }
        }

        private void SetStatus(string status)
        {

        }

        /// <summary>
        /// Enables UI for product activation.
        /// </summary>
        private void RevertToActivationGrid()
        {

        }

        [Obfuscation(Feature = "virtualization", Exclude = false)]
        private bool ValidateProductLicense(bool validateAgainstActProServer, out int stat)
        {
            Debug.WriteLine(AIMS.Properties.Settings.Default.LastOnlineProductValidationTime.ToString());

            stat = RLM.RLM_EL_NOPRODUCT;
            bool hasProductLicense = false;

            const string productLicName = "aims_core_cpr_product";

            // Handle Networking Issues Here... 
            int reattemptMax = 5;
            bool hasNetworkIssue = false;

            for (int reattemptCnt = 0; reattemptCnt < reattemptMax; reattemptCnt++)
            {
                if (hasProductLicense)
                    break;

                if (reattemptCnt > 0)
                {
                    if (!hasNetworkIssue)
                    {
                        Debug.Print("NETWORK NETWORK NETWORK NETWORK NETWORK NETWORK NETWORK NETWORK NETWORK ");
                        break;
                    }  
                }

                // Checkout the product license.
                IntPtr licensePointer = RLM.rlm_checkout(((App)App.Current).rh, productLicName, "1.0", 1);     // Store handle in App.

                ((App)App.Current).lic = licensePointer;

                stat = RLM.rlm_license_stat(licensePointer);

                if (stat == 0)
                {
                    int expDays = RLM.rlm_license_exp_days(licensePointer);

                    if (expDays >= 0)
                    {
                        hasProductLicense = true;
                        ((App)App.Current).license_days_remaining = expDays;
                        hasNetworkIssue = false;
                    }

                    hasProductLicense = true;

                    string licenseOptions = RLM.marshalToString(RLM.rlm_license_options(licensePointer));

                    // License options false unless set to true.
                    ((App)App.Current).HasAEDLicense = false;
                    ((App)App.Current).HasCheckPulseLicense = false;

                    Debug.WriteLine("License Options: " + licenseOptions);

                    if (!string.IsNullOrEmpty(licenseOptions))
                    {
                        licenseOptions = licenseOptions.ToLowerInvariant();

                        if (licenseOptions.Contains("requestaed"))
                        {
                            ((App)App.Current).HasAEDLicense = true;
                        }

                        if (licenseOptions.Contains("checkpulse"))
                        {
                            ((App)App.Current).HasCheckPulseLicense = true;
                        }
                    }

                    break;
                }
                else if (IsNetworkError(stat))
                {
                    hasNetworkIssue = true;

                    SetStatus(String.Format("Network Issue Detected. Reattempting: {0} of {1}", reattemptCnt + 1, reattemptMax));

                    Debug.WriteLine("Network error incurred. [" + RLM.marshalToString(RLM.rlm_errstring(licensePointer, ((App)App.Current).rh, new byte[RLM.RLM_ERRSTRING_MAX])) + "] - Attempt #" + (reattemptCnt + 1) + " of " + reattemptMax);
                }
                else
                {
                    hasNetworkIssue = false;

                    break;
                }

                if (reattemptCnt >= reattemptMax)
                {
                    if (hasProductLicense)
                    {
                        SetStatus("");
                    }
                    else if (hasNetworkIssue)
                    {
                        SetStatus("Networking issues. Please try again soon.");
                    }
                    else
                    {
                        SetStatus("");
                    }
                }
            }

            if (hasProductLicense && validateAgainstActProServer)
            {
                // Validate the product license on the activation pro server.
                IntPtr hostidHandle = RLM.rlm_license_hostid(((App)App.Current).lic);
                IntPtr akeyHandle = RLM.rlm_license_akey(((App)App.Current).lic);

                bool validatedLicense = false;
                hasNetworkIssue = false;

                for (int reattemptCnt = 0; reattemptCnt < reattemptMax; reattemptCnt++)
                {

                    if (reattemptCnt > 0)
                    {
                        if (!hasNetworkIssue)
                            break;
                    }

                    stat = RLM.rlm_act_keyvalid(((App)App.Current).rh, "http://hostedactivation.com", RLM.marshalToString(akeyHandle), RLM.marshalToString(hostidHandle));

                    if (stat == 0)
                    {
                        validatedLicense = true;

                        hasNetworkIssue = false;

                        break;
                    }
                    else if (IsNetworkError(stat))
                    {
                        hasNetworkIssue = true;

                        SetStatus(String.Format("Network Issue Detected. Reattempting: {0} of {1}", reattemptCnt + 1, reattemptMax));

                        Debug.WriteLine("Network error incurred. [" + RLM.marshalToString(RLM.rlm_errstring(((App)App.Current).lic, ((App)App.Current).rh, new byte[RLM.RLM_ERRSTRING_MAX])) + "] - Attempt #" + (reattemptCnt + 1) + " of " + reattemptMax);
                    }
                    else
                    {
                        hasNetworkIssue = false;

                        validatedLicense = false;

                        break;
                    }
                }

                if (hasProductLicense)
                {
                    // Local license is valid.
                    if (validatedLicense)
                    {
                        // Update the last validation time.
                        AIMS.Properties.Settings.Default.LastOnlineProductValidationTime = DateTime.Now;
                    }
                    else if (hasNetworkIssue)
                    {
                        // Bypass the license validation if the license has been validated at least recently.
                        if (DateTime.Now - AIMS.Properties.Settings.Default.LastOnlineProductValidationTime < OnlineValidationFrequency)
                        {
                            // Allow the product license to proceed with only a valid local license.
                            Debug.WriteLine(String.Format("Bypassing license validation because license has been validated recently online. ({0} is less than {1})", DateTime.Now - Properties.Settings.Default.LastOnlineProductValidationTime, OnlineValidationFrequency));
                        }
                        else
                        {
                            hasProductLicense = false;
                            SetStatus("Networking issues. Please try again soon.");
                            Debug.WriteLine(String.Format("License validation offline bypass failed. ({0} isn't less than {1})", DateTime.Now - Properties.Settings.Default.LastOnlineProductValidationTime, OnlineValidationFrequency));
                        }
                    }
                    else
                    {
                        // If license validation failed because of something other than a networking issue, then we'll prevent them.
                        hasProductLicense = false;
                    }
                }
                else
                {
                    // No valid local license.
                }
            }

            return hasProductLicense;
        }

        [Obfuscation(Feature = "virtualization", Exclude = false)]
        private bool ValidateTimeLicense(bool validateAgainstActProServer, out int stat)
        {
            Debug.WriteLine(AIMS.Properties.Settings.Default.LastOnlineTimeValidationTime.ToString());

            stat = RLM.RLM_EL_NOPRODUCT;
            bool hasTimeLicense = false;

            const string timeLicName = "aims_core_cpr_time";

            // Handle Networking Issues Here... 
            int reattemptMax = 5;
            bool hasNetworkIssue = false;

            for (int reattemptCnt = 0; reattemptCnt < reattemptMax; reattemptCnt++)
            {
                if (hasTimeLicense)
                    break;

                if (reattemptCnt > 0)
                {
                    if (!hasNetworkIssue)
                        break;
                }

                // Checkout the time license.
                ((App)App.Current).timeLic = RLM.rlm_checkout(((App)App.Current).rh, timeLicName, "1.0", 1);    // Store handle in App.

                stat = RLM.rlm_license_stat(((App)App.Current).timeLic);

                if (stat == 0)
                {
                    int expDays = RLM.rlm_license_exp_days(((App)App.Current).timeLic);

                    if (expDays >= 0)
                    {
                        hasTimeLicense = true;
                        ((App)App.Current).license_days_remaining = expDays;
                        hasNetworkIssue = false;
                    }

                    break;
                }
                else if (IsNetworkError(stat))
                {
                    hasNetworkIssue = true;

                    SetStatus(String.Format("Network Issue Detected. Reattempting: {0} of {1}", reattemptCnt + 1, reattemptMax));

                    Debug.WriteLine("Network error incurred. [" + RLM.marshalToString(RLM.rlm_errstring(((App)App.Current).timeLic, ((App)App.Current).rh, new byte[RLM.RLM_ERRSTRING_MAX])) + "] - Attempt #" + (reattemptCnt + 1) + " of " + reattemptMax);
                }
                else
                {
                    hasNetworkIssue = false;

                    break;
                }
            }

            if (hasTimeLicense && validateAgainstActProServer)
            {
                // Validate the time license on the activation pro server.
                IntPtr hostidHandle = RLM.rlm_license_hostid(((App)App.Current).timeLic);
                IntPtr akeyHandle = RLM.rlm_license_akey(((App)App.Current).timeLic);
                stat = RLM.rlm_act_keyvalid(((App)App.Current).rh, "http://hostedactivation.com", RLM.marshalToString(akeyHandle), RLM.marshalToString(hostidHandle));

                hasNetworkIssue = false;
                bool validatedLicense = false;

                for (int reattemptCnt = 0; reattemptCnt < reattemptMax; reattemptCnt++)
                {
                    if (validatedLicense)
                        break;

                    if (reattemptCnt > 0)
                    {
                        if (!hasNetworkIssue)
                            break;
                    }

                    stat = RLM.rlm_act_keyvalid(((App)App.Current).rh, "http://hostedactivation.com", RLM.marshalToString(akeyHandle), RLM.marshalToString(hostidHandle));

                    if (stat == 0)
                    {
                        validatedLicense = true;

                        hasNetworkIssue = false;

                        break;
                    }
                    else if (IsNetworkError(stat))
                    {
                        hasNetworkIssue = true;

                        SetStatus(String.Format("Network Issue Detected. Reattempting: {0} of {1}", reattemptCnt + 1, reattemptMax));

                        Debug.WriteLine("Network error incurred. [" + RLM.marshalToString(RLM.rlm_errstring(((App)App.Current).timeLic, ((App)App.Current).rh, new byte[RLM.RLM_ERRSTRING_MAX])) + "] - Attempt #" + (reattemptCnt + 1) + " of " + reattemptMax);
                    }
                    else
                    {
                        hasNetworkIssue = false;

                        validatedLicense = false;

                        break;
                    }
                }

                if (!validatedLicense)
                {
                    if (stat == RLM.RLM_ACT_NO_KEY) // -1002
                    {
                        // This time, try to validate the license using the akey of the *parent product license*.
                        // This is a required step due to the fact that in a multi-product definition in RLM, only the primary (in our case, the product) license
                        // has a link to the akey.
                        akeyHandle = RLM.rlm_license_akey(((App)App.Current).lic);

                        for (int reattemptCnt = 0; reattemptCnt < reattemptMax; reattemptCnt++)
                        {
                            if (validatedLicense)
                                break;

                            if (reattemptCnt > 0)
                            {
                                if (!hasNetworkIssue)
                                    break;
                            }

                            stat = RLM.rlm_act_keyvalid(((App)App.Current).rh, "http://hostedactivation.com", RLM.marshalToString(akeyHandle), RLM.marshalToString(hostidHandle));

                            if (stat == 0)
                            {
                                validatedLicense = true;

                                hasNetworkIssue = false;

                                break;
                            }
                            else if (IsNetworkError(stat))
                            {
                                hasNetworkIssue = true;

                                SetStatus(String.Format("Network Issue Detected. Reattempting: {0} of {1}", reattemptCnt + 1, reattemptMax));

                                Debug.WriteLine("Network error incurred. [" + RLM.marshalToString(RLM.rlm_errstring(((App)App.Current).lic, ((App)App.Current).rh, new byte[RLM.RLM_ERRSTRING_MAX])) + "] - Attempt #" + (reattemptCnt + 1) + " of " + reattemptMax);
                            }
                            else
                            {
                                hasNetworkIssue = false;

                                validatedLicense = false;

                                break;
                            }
                        }
                    }
                }

                if (hasTimeLicense)
                {
                    // Local license is valid.
                    if (validatedLicense)
                    {
                        // Update the last validation time.
                        AIMS.Properties.Settings.Default.LastOnlineTimeValidationTime = DateTime.Now;
                    }
                    else if (hasNetworkIssue)
                    {
                        // Bypass the license validation if the license has been validated at least recently.
                        if (DateTime.Now - AIMS.Properties.Settings.Default.LastOnlineTimeValidationTime < OnlineValidationFrequency)
                        {
                            // Allow the time license to proceed with only a valid local license.
                            Debug.WriteLine(String.Format("Bypassing license validation because license has been validated recently online. ({0} is less than {1})", DateTime.Now - Properties.Settings.Default.LastOnlineTimeValidationTime, OnlineValidationFrequency));
                        }
                        else
                        {
                            hasTimeLicense = false;
                            SetStatus("Networking issues. Please try again soon.");
                            Debug.WriteLine(String.Format("License validation offline bypass failed. ({0} isn't less than {1})", DateTime.Now - Properties.Settings.Default.LastOnlineTimeValidationTime, OnlineValidationFrequency));
                        }
                    }
                    else
                    {
                        // If license validation failed because of something other than a networking issue, then we'll prevent them.
                        hasTimeLicense = false;
                    }
                }
                else
                {
                    // No valid local license.
                }
            }

            return hasTimeLicense;
        }

        [Obfuscation(Feature = "virtualization", Exclude = false)]
        private bool ValidateTrialLicense(bool validateAgainstActProServer, out int stat)
        {
            Debug.WriteLine(Properties.Settings.Default.LastOnlineTrialValidationTime.ToString());

            stat = RLM.RLM_EL_NOPRODUCT;

            bool hasTrialLicense = false;

            bool hasNetworkIssue = false;

            const string trialLicName = "aims_core_cpr_trial";

            // Handle Networking Issues Here... 
            int reattemptMax = 5;

            for (int reattemptCnt = 0; reattemptCnt < reattemptMax; reattemptCnt++)
            {
                if (hasTrialLicense)
                    break;

                if (reattemptCnt > 0)
                {
                    if (!hasNetworkIssue)
                        break;
                }

                // Checkout the trial license.
                ((App)App.Current).lic = RLM.rlm_checkout(((App)App.Current).rh, trialLicName, "1.0", 1);    // Store handle in App.

                stat = RLM.rlm_license_stat(((App)App.Current).lic);

                if (stat == 0)
                {
                    int expDays = RLM.rlm_license_exp_days(((App)App.Current).lic);

                    if (expDays >= 0)
                    {
                        hasTrialLicense = true;
                        ((App)App.Current).license_days_remaining = expDays;
                        hasNetworkIssue = false;
                    }

                    break;
                }
                else if (IsNetworkError(stat))
                {
                    hasNetworkIssue = true;

                    SetStatus(String.Format("Network Issue Detected. Reattempting: {0} of {1}", reattemptCnt + 1, reattemptMax));

                    Debug.WriteLine("Network error incurred. [" + RLM.marshalToString(RLM.rlm_errstring(((App)App.Current).lic, ((App)App.Current).rh, new byte[RLM.RLM_ERRSTRING_MAX])) + "] - Attempt #" + (reattemptCnt + 1) + " of " + reattemptMax);
                }
                else
                {
                    hasNetworkIssue = false;

                    break;
                }
            }

            if (hasTrialLicense && validateAgainstActProServer)
            {
                // Validate the trial license on the activation pro server.
                IntPtr hostidHandle = RLM.rlm_license_hostid(((App)App.Current).lic);
                IntPtr akeyHandle = RLM.rlm_license_akey(((App)App.Current).lic);

                hasNetworkIssue = false;
                bool validatedLicense = false;

                for (int reattemptCnt = 0; reattemptCnt < reattemptMax; reattemptCnt++)
                {
                    if (reattemptCnt > 0)
                    {
                        if (!hasNetworkIssue)
                            break;
                    }

                    stat = RLM.rlm_act_keyvalid(((App)App.Current).rh, "http://hostedactivation.com", RLM.marshalToString(akeyHandle), RLM.marshalToString(hostidHandle));

                    if (stat == 0)
                    {
                        hasTrialLicense = true;

                        validatedLicense = true;

                        break;
                    }
                    else if (IsNetworkError(stat))
                    {
                        hasNetworkIssue = true;

                        SetStatus(String.Format("Network Issue Detected. Reattempting: {0} of {1}", reattemptCnt + 1, reattemptMax));

                        Debug.WriteLine("Network error incurred. [" + RLM.marshalToString(RLM.rlm_errstring(((App)App.Current).lic, ((App)App.Current).rh, new byte[RLM.RLM_ERRSTRING_MAX])) + "] - Attempt #" + (reattemptCnt + 1) + " of " + reattemptMax);
                    }
                    else
                    {
                        hasNetworkIssue = false;

                        validatedLicense = false;

                        break;
                    }
                }

                if (hasTrialLicense)
                {
                    // Local license is valid.
                    if (validatedLicense)
                    {
                        // Update the last validation time.
                        Properties.Settings.Default.LastOnlineTrialValidationTime = DateTime.Now;
                    }
                    else if (hasNetworkIssue)
                    {
                        // Bypass the license validation if the license has been validated at least recently.
                        if (DateTime.Now - Properties.Settings.Default.LastOnlineTrialValidationTime < OnlineValidationFrequency)
                        {
                            // Allow the trial license to proceed with only a valid local license.
                            Debug.WriteLine(String.Format("Bypassing license validation because license has been validated recently online. ({0} is less than {1})", DateTime.Now - Properties.Settings.Default.LastOnlineTrialValidationTime, OnlineValidationFrequency));
                        }
                        else
                        {
                            hasTrialLicense = false;
                            SetStatus("Networking issues. Please try again soon.");
                            Debug.WriteLine(String.Format("License validation offline bypass failed. ({0} isn't less than {1})", DateTime.Now - Properties.Settings.Default.LastOnlineTrialValidationTime, OnlineValidationFrequency));
                        }
                    }
                    else
                    {
                        // If license validation failed because of something other than a networking issue, then we'll prevent them.
                        hasTrialLicense = false;
                    }
                }
                else
                {
                    // No valid local license.
                }
            }

            return hasTrialLicense;
        }

        [Obfuscation(Feature = "virtualization", Exclude = false)]
        private static int checkstat(IntPtr rh, IntPtr lic, string name)
        {
            int stat;
            byte[] errstring = new byte[RLM.RLM_ERRSTRING_MAX];

            stat = RLM.rlm_license_stat(lic);

            if (stat == 0)
            {
                Debug.WriteLine(String.Format("Checkout of {0} license OK.", name));
            }
            else
            {
                Debug.WriteLine(String.Format("Error checking out {0} license.", name));
                Debug.WriteLine(String.Format("{0}", RLM.rlm_errstring(lic, rh, errstring)));
            }

            return (stat);
        }

        /// <summary>
        /// Attempt activation (license creation based off the passed in activation key).
        /// </summary>
        /// <param name="rh"></param>
        /// <param name="name">The product name on the ActivationPro server that the activation key coorelates to.</param>
        /// <param name="actkey">The activation key to attempt.</param>
        /// <returns></returns>
        [Obfuscation(Feature = "virtualization", Exclude = false)]
        private int doactivation(IntPtr rh, string name, string actkey)
        {

            char[] license = new char[3 * RLM.RLM_MAX_LINE + 1];    // Allow for HOST, ISV, and LICENSE.
            int stat = RLM.RLM_EH_READ_NOLICENSE;
            IntPtr act_handle = IntPtr.Zero;

            // Check the license folder for license files.
            string dir = GetLicenseFolderLocation();

            // Calling rlm_init sets up the RLM connection by telling RLM where it should look for licenses and such.
            IntPtr handle = RLM.rlm_init(dir, dir, null);

            ((App)App.Current).rh = handle;

            // Note: Perhaps it's best to offer to activate, than do it automatically. (They might not want to pin the activation to this hostid/machine, or they may not want to start the license yet).

            // Note: We need them to input the Activation Key.
            byte[] lic = new byte[3 * RLM.RLM_MAX_LINE + 1];
            byte[] prod = new byte[RLM.RLM_MAX_PRODUCT];
            byte[] version = new byte[RLM.RLM_MAX_VER];
            byte[] upgradeVer = new byte[RLM.RLM_MAX_VER];
            int licType = 0, dateBased = 0;

            stat = RLM.rlm_act_info(handle, "http://hostedactivation.com/", actkey, prod, version, ref dateBased, ref licType, upgradeVer);

            Debug.WriteLine("rlm_act_info returns " + stat);

            switch (stat)
            {
                case 0:
                    {
                        System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
                        String product = enc.GetString(prod);
                        String ver = enc.GetString(version);
                        String upVer = enc.GetString(upgradeVer);
                        Debug.WriteLine("rlm_act_info returns product " + product +
                            " version " + ver +
                            " date based " + dateBased +
                            " license type " + licType +
                            " upgrade version " + upVer);

                        stat = RLM.rlm_act_request(handle, "http://hostedactivation.com/", "hcs", actkey, "", "", 1, "", lic);

                        switch (stat)
                        {
                            case 0:
                                {
                                    FileInfo fi = new FileInfo(GetLicenseFileLocation());

                                    // Create a writer, ready to add entries to the file.

                                    StreamWriter sw = fi.AppendText();

                                    string lictext = enc.GetString(lic);

                                    int lastIndex = lictext.LastIndexOf("\n\n");

                                    lictext = lictext.Substring(0, lastIndex);  // Only write the data w/o all of the \n\n\n\n\n\n's.

                                    sw.WriteLine(lictext);  // Write the license definition.

                                    sw.WriteLine(); // Write line terminator so that additional liceses are created on the next line.

                                    SetStatus("Activation successful.");

                                    // Output the license to debug
                                    Debug.WriteLine(lictext);

                                    // Cleanup the stream writer.
                                    sw.Flush();
                                    sw.Close();
                                    sw.Dispose();
                                    sw = null;

                                    RLM.rlm_set_active(((App)App.Current).rh);

                                    break;
                                }
                            case 1: // ISV
                                {
                                    goto case 0;

                                }
                            case -1005:
                                {
                                    Debug.WriteLine("Activation key already in use.");
                                    SetStatus("Activation key already in use.");
                                    break;
                                }
                            case -1017:
                                {
                                    Debug.WriteLine("Activation key has expiried.");
                                    SetStatus("Activation key has expired.");
                                    break;
                                }
                            case -1021:
                                {
                                    Debug.WriteLine("Activation key not valid.");
                                    SetStatus("Activation key not valid.");
                                    break;
                                }
                            default:
                                {

                                    Debug.WriteLine("Activation stat = " + RLM.marshalToString(RLM.rlm_errstring(((App)App.Current).lic, ((App)App.Current).rh, new byte[RLM.RLM_ERRSTRING_MAX])));

                                    if (IsNetworkError(stat))
                                    {
                                        SetStatus("A network error has occurred.");
                                    }
                                    else
                                    {
                                        SetStatus("Activation error #" + stat);
                                    }

                                    break;
                                }
                        }

                        break;
                    }
                case -1021:
                    {
                        Debug.WriteLine("Activation key not valid.");
                        SetStatus("Activation key not valid.");
                        break;
                    }
                case -1030:
                    {
                        Debug.WriteLine("Activation key disabled.");
                        SetStatus("Activation key disabled.");
                        break;
                    }
                default:
                    {

                        Debug.WriteLine("Activation stat = " + RLM.marshalToString(RLM.rlm_errstring(((App)App.Current).lic, ((App)App.Current).rh, new byte[RLM.RLM_ERRSTRING_MAX])));

                        if (IsNetworkError(stat))
                        {
                            SetStatus("A network error has occurred.");
                        }
                        else
                        {
                            SetStatus("Activation error #" + stat);
                        }

                        break;
                    }
            }

            RLM.rlm_close(handle);

            return stat;
        }

        // Get the complete list of products that can be checked out and
        // show a few details of each
        [Obfuscation(Feature = "virtualization", Exclude = false)]
        private static void showProducts(IntPtr handle)
        {
            IntPtr products = RLM.rlm_products(handle, null, null);

            if (products == (IntPtr)0)
            {
                Debug.WriteLine("rlm_products failed");
                return;
            }
            RLM.rlm_product_first(products);
            do
            {
                Debug.WriteLine(RLM.marshalToString(RLM.rlm_product_name(products)));
                Debug.WriteLine(" version " + RLM.marshalToString(RLM.rlm_product_ver(products)));
                Debug.WriteLine(" expiration " + RLM.marshalToString(RLM.rlm_product_exp(products)));
            } while (RLM.rlm_product_next(products) >= 0);
            RLM.rlm_products_free(products);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        [Obfuscation(Feature = "virtualization", Exclude = false)]
        private void ActivateButton_Click(object sender, RoutedEventArgs e)
        {
            SubmitActivationText();
        }

        [Obfuscation(Feature = "virtualization", Exclude = false)]
        private void ContinueButton_Click(object sender, RoutedEventArgs e)
        {
            // Prevent use of Snoop to Show/Enable the button to bypass license validation.
            if (hasTrialLicense || (hasProductLicense && hasTimeLicense))
            {
                // Move this to the dashboard
                this.Visibility = Visibility.Hidden;
                this.Close();
                this.loginPage.GoToDashboard();
            }
            else
            {
                RevertToActivationGrid();
            }
        }

        [Obfuscation(Feature = "virtualization", Exclude = false)]
        private void ContinueToMainWindow(double delayInSeconds = 0)
        {
            // Save current settings (last validation time, etc).
            App.SaveSettings();

            // if (hasTrialLicense || (hasProductLicense && hasTimeLicense)) // These must be validated, prevent snoop bypassing.
            //{
            if (delayInSeconds > 0)
            {
                LoginPage loginPage = null;

                //mainwindow.Activate();
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    System.Diagnostics.Debug.WriteLine("Navigating to main window...");

                    // Switch over to the MainWindow instead of the LicensingWindow.
                    loginPage = new LoginPage();

                    loginPage.GoToDashboard();

                    // Close the current window.
                    this.Close();
                }));
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Navigating to main window...");

                LoginPage loginPage = null;

                // Switch over to the MainWindow instead of the LicensingWindow.
                loginPage = new LoginPage();

                loginPage.GoToDashboard();

                // Close the current window.
                this.Close();
            }
            //}
        }

        /// <summary>
        /// Checks the passed in stat code to determine whether or not it represents a corrosponding network error.
        /// </summary>
        /// <param name="stat"></param>
        /// <returns></returns>
        [Obfuscation(Feature = "virtualization", Exclude = false)]
        private static bool IsNetworkError(int stat)
        {
            if (stat == 0)
                return false;

            // Below is a list of all of the networking related errors given by RLM. Note that the entire list can be found in RLMInterop.cs within our project's solution explorer.
            if (stat == RLM.RLM_EH_CANTCONNECT_URL || //-132 
                stat == RLM.RLM_EH_NET_INIT || //-103 
                stat == RLM.RLM_EH_NET_WERR || //-104 
                stat == RLM.RLM_EH_NET_RERR || //-105 
                stat == RLM.RLM_EH_BAD_HTTP || //-136 
                stat == RLM.RLM_EH_SERVER_REJECT || //-142
                stat == RLM.RLM_EH_NO_BROADCAST_RESP || //-167 
                stat == RLM.RLM_EL_SERVER_BADRESP || //-16 
                stat == RLM.RLM_EL_COMM_ERROR || //-17 
                stat == RLM.RLM_EL_SERVER_DOWN || //-20 
                stat == RLM.RLM_EL_TIMEDOUT || //-24 
                stat == RLM.RLM_EL_INQUEUE || //-25 
                stat == RLM.RLM_EL_CANTRECONNECT || //-56 
                stat == RLM.RLM_ACT_SERVER_ERROR) //-1019
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Submit the text in the current ProductActivationKey textbox to activate.
        /// </summary>
        [Obfuscation(Feature = "virtualization", Exclude = false)]
        private void SubmitActivationText()
        {
            if (!String.IsNullOrWhiteSpace(ProductActivationKeyTB.Text))
            {
                int stat = doactivation(((App)App.Current).rh, null, ProductActivationKeyTB.Text);

                if (stat == 0 || stat == 1)
                {
                    Debug.Print("VALID VALID VALID VALID VALID VALID VALID VALID VALID ");
                    this.CancelLicenseCheck();
                    this.StartLicenseCheck();
                }
                else
                {
                    Debug.Print("BAD BAD BAD BAD BAD BAD BAD BAD BAD BAD BAD BAD BAD BAD BAD BAD BAD ");
                }
            }
        }

        [Obfuscation(Feature = "virtualization", Exclude = false)]
        private void ProductActivationKeyTB_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    {
                        SubmitActivationText();
                        break;
                    }
            }
        }

        [Obfuscation(Feature = "virtualization", Exclude = false)]
        private string GetLicenseFileLocation()
        {
            return GetLicenseFolderLocation() + @"\license.lic";
        }

        [Obfuscation(Feature = "virtualization", Exclude = false)]
        private string GetLicenseFolderLocation()
        {
            return App.GetProgramDataDirectory();
        }
    }
}
