﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Security;
using Npgsql;
using System.Data;
using System.Windows.Interop;
using System.Windows.Threading;
// 4902-6049-1917-4297
namespace AIMS
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        // Create a hidden instance of a Licensing Window
        private LicensingWindow licenseWindow;

        public LoginPage()
        {
            InitializeComponent();

            Loaded += LoginPage_Loaded;

            userNameBox.Focus();

            CheckCapsLock();
            CheckUserNameEmpty();
            CheckPasswordEmpty();

            // Create our license window, supply this instance as a... parent of sorts
            this.licenseWindow = new LicensingWindow(this);
        }

        /// <summary>
        /// Disabled DirectX effects - which can cause bluescreening if the driver is buggy.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInitialized(EventArgs e)
        {
            var hwndSource = PresentationSource.FromVisual(this) as HwndSource;

            if (hwndSource != null)
            {
                hwndSource.CompositionTarget.RenderMode = RenderMode.SoftwareOnly;
            }

            base.OnInitialized(e);
        }

        /// <summary>
        /// Loaded event for the Login Page.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoginPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (KinectManager.SensorChooser != null)
            {
                this.SensorChooserUI = KinectManager.SensorChooser; // Attach the sensor chooser.
            }
        }

        /// <summary>
        /// Logic for when the user login "Submit" button has been clicked.
        /// 
        /// - Checks user input against database feilds to detect proper login username/password.
        /// - If correct -> sign into the dashboard and display their stats.
        /// - If incorrect -> relaunch login screen, with a warning of incorrect username and/or password.
        /// </summary>
        public void SubmitPasswordButtonClicked(Object sender, RoutedEventArgs args)
        {
            /* Logic Flow:
             * 1. Take inputed username and check it against the database.
             * 2. If an entry in the database is setup, check to see if the password is the same as the entered password.
             * 3. If not, log the login attempt, and return them back to the LoginPage with a fail message.
             * 4. Assuming they got in, load up user-end-dashboard for that user.
             */

            //// TREY: Debug to just skip everything when checking licensing code
            //PasswordSuccess();

            userNameBox.Focus();    // re-focus on the username input box

            string submitted_username = userNameBox.Text;
            string submitted_password = passwordBox.Password;

            incorrectPasswordWarningLabel.Visibility = Visibility.Collapsed;

            if (CheckUserNameEmpty())
                return;
            if (CheckPasswordEmpty())
                return;

            //string query = String.Format( "SELECT uid FROM accounts WHERE username='{0}'", submitted_username );

            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand(); //new NpgsqlCommand(query, DatabaseManager.Connection);

            command.CommandType = CommandType.Text;
            command.CommandText = @"SELECT uid FROM accounts WHERE username = :username AND  password = :password;";
            command.Parameters.Add("username", NpgsqlTypes.NpgsqlDbType.Text).Value = submitted_username;
            command.Parameters.Add("password", NpgsqlTypes.NpgsqlDbType.Text).Value = submitted_password;

            int id = 0;

            try
            {
                id = (int)command.ExecuteScalar();  // read the selected id
            }
            catch (Exception ex)
            {

            }

            DatabaseManager.CloseConnection();

            Account acct = Account.GetAccountFromAccountID(id, true);  // get the account from the database, true = close the database connection

            if (acct == null)   // no account was found
            {
                AccountFailed();    // signal account failed
                return;
            }

            ((App)App.Current).CurrentAccount = acct;   // store the account object in the currently running App so that it can be accessed from anywhere within the application.

            if (!submitted_password.Equals(acct.Password))  // passwords do not match
            {
                PasswordFailed();   // signal failed password
                return;
            }
            else
            {
                PasswordSuccess();  // signal successful password
            }
        }

        private void PasswordChangedHandler(Object sender, RoutedEventArgs args)
        {
            CheckCapsLock();
            CheckUserNameEmpty();
            CheckPasswordEmpty();
            incorrectPasswordWarningLabel.Visibility = Visibility.Collapsed;
        }

        private void UserNameChangedHandler(Object sender, RoutedEventArgs args)
        {
            CheckCapsLock();
            CheckUserNameEmpty();
        }

        private void CheckCapsLock()
        {
            if (Console.CapsLock)   // returns true if CapsLock key is on.
            {
                // Warn that the caps lock key is enabled.

                capsLockWarningLabel.Content = "* Caps Lock enabled";
                capsLockWarningLabel.Visibility = Visibility.Visible;
            }
            else
            {
                capsLockWarningLabel.Content = "";
                capsLockWarningLabel.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Checks if the password box was left empty, or if it is simply white space.
        /// </summary>
        /// <returns></returns>
        private bool CheckPasswordEmpty()
        {
            if (String.IsNullOrWhiteSpace(passwordBox.Password))
            {
                emptyPasswordWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                emptyPasswordWarningLabel.Visibility = Visibility.Collapsed;
                return false;
            }
        }

        /// <summary>
        /// Checks if the username box was left empty, or if it is simply white space.
        /// </summary>
        /// <returns></returns>
        private bool CheckUserNameEmpty()
        {
            if (String.IsNullOrWhiteSpace(userNameBox.Text))
            {
                emptyUserNameWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                emptyUserNameWarningLabel.Visibility = Visibility.Collapsed;
                return false;
            }
        }

        /// <summary>
        /// The password login attempt was a failure.
        /// </summary>
        private void PasswordFailed()
        {
            // register a login attempt.
            // return to the LoginScreen.
            passwordBox.Password = "";  // reset the password
            emptyPasswordWarningLabel.Visibility = Visibility.Collapsed;
            incorrectPasswordWarningLabel.Visibility = Visibility.Visible;
            userNameBox.Focus();
        }

        /// <summary>
        /// The account was unable to be found.
        /// </summary>
        private void AccountFailed()
        {
            passwordBox.Password = "";  // reset the password;
            userNameBox.Text = "";  // reset the username
            userNameBox.Focus();
        }

        /// <summary>
        /// The password was successful, log in the user.
        /// </summary>
        private void PasswordSuccess()
        {
            // TREY:
            //
            // WILL PUT LICENSING CHECKS HERE AS AN INTERMEDIATE STEP BEFORE LETTING THE  USER  LOG IN
            //
            // FOLLOWING SUCCESSFUL PASSWORD, CHECK WITH THE LICENSING SERVER\
            // We're manipulating GUI elements, so need to dispatch this function
            if (licenseWindow.Visibility == Visibility.Visible)
            {
                return;
            }

            Dispatcher.BeginInvoke((Action)(() =>
            {
                // Show the  window
                licenseWindow.Show();

                // Check if this license is valid, dispatched function
                licenseWindow.StartLicenseCheck();
            }));
        }

        public void GoToDashboard()
        {
            UpdateAccountLastLoginInfo();

            if (((App)App.Current).CurrentAccount.ChangePasswordNextLogin)  // Check if the user needs to change their password.
            {
                ShowPasswordChangeOverlay();    // Show password change overlay
            }
            else
            {
                switch (((App)App.Current).CurrentAccount.AccessLevel)
                {
                    case AccessLevel.User:
                        {
                            ((App)App.Current).CurrentStudent = Student.GetStudentFromAccountID(((App)App.Current).CurrentAccount.ID, true);
                            //MessageBox.Show(String.Format("Welcome {0} {1}. \r\rOrganization: \r{2} \r\rAccount: \r{3}", ((App)App.Current).CurrentStudent.FirstName, ((App)App.Current).CurrentStudent.LastName, ((App)App.Current).CurrentStudent.Organization, ((App)App.Current).CurrentStudent.Account));

                            GoToStudentDashboard();

                            break;
                        }
                    case AccessLevel.Doctor:
                        {
                            //MessageBox.Show(String.Format("Logged in a doctor account #{0}.", ((App)App.Current).CurrentAccount.ID));
                            ((App)App.Current).CurrentDoctor = Doctor.GetDoctorFromAccountID(((App)App.Current).CurrentAccount.ID, true);
                            //MessageBox.Show(String.Format("Welcome {0} {1}. \r\rOrganization: \r{2} \r\rAccount: \r{3}", ((App)App.Current).CurrentDoctor.FirstName, ((App)App.Current).CurrentDoctor.LastName, ((App)App.Current).CurrentDoctor.Organization, ((App)App.Current).CurrentDoctor.Account));

                            GoToDoctorDashboard();

                            break;
                        }
                    case AccessLevel.Administrator:
                        {
                            ((App)App.Current).CurrentAdministrator = Administrator.GetAdministratorFromAccountID(((App)App.Current).CurrentAccount.ID, true);
                            //MessageBox.Show(String.Format("Welcome {0} {1}. \r\rOrganization: \r{2} \r\rAccount: \r{3}", ((App)App.Current).CurrentAdministrator.FirstName, ((App)App.Current).CurrentAdministrator.LastName, ((App)App.Current).CurrentAdministrator.Organization, ((App)App.Current).CurrentAdministrator.Account));

                            GoToAdministratorDashboard();

                            break;
                        }
                }
            }
        }

        /// <summary>
        /// Updates the last login date, and last login IP for the logging-in user.
        /// </summary>
        private void UpdateAccountLastLoginInfo()
        {
            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = CommandType.Text;
            command.CommandText = "UPDATE accounts SET lastlogin = CURRENT_TIMESTAMP, lastloginip = inet_client_addr() WHERE uid = :accountid";
            command.Parameters.Add("datenow", NpgsqlTypes.NpgsqlDbType.Timestamp).Value = DateTime.Now;
            command.Parameters.Add("accountid", NpgsqlTypes.NpgsqlDbType.Integer).Value = ((App)App.Current).CurrentAccount.ID;

            if (command.ExecuteNonQuery() == 0)
            {
                // Error creating Last Login Information.
            }

            DatabaseManager.CloseConnection();
        }

        /// <summary>
        /// Navigate to the student dashboard.
        /// </summary>
        private void GoToStudentDashboard()
        {
            System.Uri uri = new System.Uri(@"../MainPage3.xaml", System.UriKind.Relative);
            NavigationService.Navigate(uri);
        }

        /// <summary>
        /// Navigate to the doctor dashboard.
        /// </summary>
        private void GoToDoctorDashboard()
        {
            System.Uri uri = new System.Uri(@"../DoctorDashboard.xaml", System.UriKind.Relative);
            NavigationService.Navigate(uri);
        }

        /// <summary>
        /// Navigate to the administrator dashboard.
        /// </summary>
        private void GoToAdministratorDashboard()
        {
            System.Uri uri = new System.Uri(@"../DevTools.xaml", System.UriKind.Relative);
            NavigationService.Navigate(uri);
        }

        /// <summary>
        /// Button handler, checks for Caps Lock and checks if the Enter button has been hit.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.CapsLock)  // CapsLock key toggles on/off the caps lock warning message.
            {
                CheckCapsLock();
            }
            if (e.Key == Key.Enter) // Enter key submits the current password input.
            {
                SubmitPasswordButtonClicked( null, null );
            }
        }

        // Updating
        private static Action EmptyDelegate = delegate() { };

        // Update the GUI
        private void UpdateVisual()
        {
            this.InvalidateVisual();
            this.Dispatcher.Invoke(DispatcherPriority.Render, EmptyDelegate);
        }

        /// <summary>
        /// Shows the password change overlay, bluring the background and disabling background input.
        /// </summary>
        private void ShowPasswordChangeOverlay()
        {
            // We're manipulating GUI elements, so need to dispatch this function
            Dispatcher.BeginInvoke((Action)(() =>
            {
                this.userNameBox.IsEnabled = false;     // disable the username text box
                this.passwordBox.IsEnabled = false;     // disable the password box
                this.SignInButton.IsEnabled = false;    // disable the sign-in button

                // refresh
                UpdateVisual();

                // show the password change overlay
                this.BlurEffect.Radius = 25;
                this.PasswordChangeOverlay.Visibility = Visibility.Visible;
                this.PasswordChangeOverlay.IsEnabled = true;
                this.PasswordChangeOverlay.CurrentPasswordPasswordBox.Focus();    // focus on the first password box

                UpdateVisual();
            }));
        }

        private void loginAnimate(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: Add event handler implementation here.
        }


        // *****************************************************************************
        // ************************** Forgot Password Overlay **************************
        // *****************************************************************************


        // *************** EVENT HANDLERS ***************

        private void RegisterButtonClicked(object sender, RoutedEventArgs e)
        {
            // navigate to user account creation page
            System.Uri uri = new System.Uri(@"../UserAccountCreation.xaml", System.UriKind.Relative);
            NavigationService.Navigate(uri);
        }

        private void ForogtPasswordButtonClicked(object sender, RoutedEventArgs e)
        {
            this.ForgotPasswordOverlay.Show();
            /*
            FindPassWordBorder.Visibility = Visibility.Visible;
            Username2Box.Text = userNameBox.Text;
            CheckUsername2Empty();
            CheckUsername2Invalid();*/
        }

        /*
        private void CancelButtonClicked(object sender, RoutedEventArgs e)
        {
            FindPassWordBorder.Visibility = Visibility.Collapsed;
        }

        private void SubmitButtonClicked(object sender, RoutedEventArgs e)
        {
            FindPassWordBorder.Visibility = Visibility.Collapsed;
        }
   
        private void Username2ChangedHandler(object sender, RoutedEventArgs e)
        {
            CheckUsername2Empty();
            CheckUsername2Invalid();
        }

        private void SecurityAnswerChangedHandler(object sender, RoutedEventArgs e)
        {
            CheckSecurityAnswerEmpty();
            CheckSecurityAnswerInvalid();
        }


        // *************** FUNCTIONS ***************

        /// <summary>
        /// Checks if the username2 box was left empty, or if it is simply white space.
        /// </summary>
        /// <returns></returns>
        private bool CheckUsername2Empty()
        {
            if (String.IsNullOrWhiteSpace(Username2Box.Text))
            {
                emptyUsername2WarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                emptyUsername2WarningLabel.Visibility = Visibility.Collapsed;
                return false;
            }
        }

        /// <summary>
        /// Checks if the user exists in the database
        /// </summary>
        /// <returns></returns>
        private bool CheckUsername2Invalid()
        {
            if (!CheckUsername2Empty())
            {
                DatabaseManager.OpenConnection();
                NpgsqlCommand command = DatabaseManager.Connection.CreateCommand(); //new NpgsqlCommand(query, DatabaseManager.Connection);

                command.CommandType = CommandType.Text;
                command.CommandText = @"SELECT uid FROM accounts WHERE username = :username;";
                command.Parameters.Add("username", NpgsqlTypes.NpgsqlDbType.Text).Value = Username2Box.Text;

                int id = 0;

                try
                {
                    id = (int)command.ExecuteScalar();  // read the selected id
                }
                catch
                {
                    Username2InvalidWarningLabel.Visibility = Visibility.Visible;
                    return true;
                    // no account with that username/password is in our database
                }

                DatabaseManager.CloseConnection();

                Account acct = Account.GetAccountFromAccountID(id, true);  // get the account from the database, true = close the database connection
                ((App)App.Current).CurrentAccount = acct;   // store the account object in the currently running App so that it can be accessed from anywhere within the application.
                SecurityQuestion.Text = acct.SecurityQuestion;

                Username2InvalidWarningLabel.Visibility = Visibility.Collapsed;
                return false;
            }
            else
            {
                Username2InvalidWarningLabel.Visibility = Visibility.Collapsed;
                return false;
            }
        }

        /// <summary>
        /// Checks if the security answer box was left empty, or if it is simply white space.
        /// </summary>
        /// <returns></returns>
        private bool CheckSecurityAnswerEmpty()
        {
            if (String.IsNullOrWhiteSpace(SecurityAnswerBox.Text))
            {
                emptySecurityAnswerWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                emptySecurityAnswerWarningLabel.Visibility = Visibility.Collapsed;
                return false;
            }
        }

        /// <summary>
        /// Checks if the security answer entered matches the one stored in the database.
        /// </summary>
        /// <returns></returns>
        private bool CheckSecurityAnswerInvalid()
        {
            if (!CheckUsername2Empty() && !CheckUsername2Invalid() && !CheckSecurityAnswerEmpty())
            {
                if (SecurityAnswerBox.Text == ((App)App.Current).CurrentAccount.SecurityAnswer)
                {
                    SecurityAnswerInvalidWarningLabel.Visibility = Visibility.Collapsed;
                    return false;
                }
                else
                {
                    SecurityAnswerInvalidWarningLabel.Visibility = Visibility.Visible;
                    return true;
                }
            }
            else
            {
                SecurityAnswerInvalidWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
        }
         */
    }
}
