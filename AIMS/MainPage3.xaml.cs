﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using AIMS.Speech;
using System.Runtime.InteropServices;
using Microsoft.Kinect;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Controls.DataVisualization.Charting.Primitives;
using System.Data;
using System.Windows.Threading;
using Npgsql;
using AIMS.Components;
using AIMS.Tasks;
using AIMS.Tasks.LateralTransfer;
using AIMS.Tasks.Cpr;
using AIMS.Tasks.VerticalLift;
using AIMS.Tasks.Intubation;


namespace AIMS
{	
	public partial class MainPage3 : INotifyPropertyChanged
	{
		//MAIN PAGE DEPENDENCIES AND STARTUP VARIABLES, AND RESULTS STUFF

        // OLD Charts, Results and Overview tab (Copied from StudentResultsPage student results)
		private DataSet ds = new DataSet();
        private DataTable dt = new DataTable();

        private int displayIndex = 0;
        DispatcherTimer timer;

        private List<Result> studentResultList = new List<Result>();
        private List<StageScore> stageScoreList = new List<StageScore>();
        private List<int> stageScoreIndexList = new List<int>();

        List<List<KeyValuePair<DateTime, int>>> chartValueLists = new List<List<KeyValuePair<DateTime, int>>>();

        // Results and Charts
        private ObservableRangeCollection<LateralTransferResult> lateralTransferResults;
        private ObservableRangeCollection<CprResult> cprResults;
        private ObservableRangeCollection<VerticalLiftResult> verticalLiftResults;
        private ObservableRangeCollection<IntubationResult> intubationResults;

        private ObservableRangeCollection<TaskResult> resultsCollection = new ObservableRangeCollection<TaskResult>();
        public ObservableRangeCollection<TaskResult> ResultsCollection { get { return resultsCollection; } set { resultsCollection = value; } }
		
        
        // these following variables are for the Overview Tab
		private double goalhittotal = 95;
		public double GoalHitTotal
		{
			get { return goalhittotal; }
			set
			{
				if( goalhittotal == value )
					goalhittotal = value;
				else
				{
					goalhittotal = value;
					NotifyPropertyChanged("GoalHitTotal");
				}	
			}
		}

        private double totaldays = 189;
		public double TotalDays
		{
			get { return totaldays; }
			set
			{
				if( totaldays == value )
					totaldays = value;
				else
				{
					totaldays = value;
					NotifyPropertyChanged("TotalDays");
				}	
			}
		}	
		
		private double totalsessions = 4;
		public double TotalSessions
		{
			get { return totalsessions; }
			set
			{
				if( totalsessions == value )
					totalsessions = value;
				else
				{
					totalsessions = value;
					NotifyPropertyChanged("TotalSessions");
				}
			}
		}	
		
		private TimeSpan activetime = TimeSpan.FromSeconds(25.0);
		public TimeSpan ActiveTime
		{
			get { return activetime; }
			set
			{
				if( activetime == value )
					activetime = value;
				else
				{
					activetime = value;
					NotifyPropertyChanged("ActiveTime");					
				}
					
			}
		}
		
		private double totaltaskaverage = .91*100;
		public double TotalTaskAverage
		{
			get { return totaltaskaverage; }
			set
			{
				if( totaltaskaverage == value )
					totaltaskaverage = value;
				else
				{
					totaltaskaverage = value;
					NotifyPropertyChanged("TotalTaskAverage");
				}	
			}
		}	
		
		//These Following variables are for the Results Tab
			//These Following variables are for Intubation Results
		private double accuracy = 70.0;
		public double Accuracy
		{
			get { return accuracy; }
			set
			{
				if( accuracy == value )
					accuracy = value;
				else
				{
					accuracy = value;
					NotifyPropertyChanged("Accuracy");
				}
					
			}
		}
		
		private int totalActions = 3;
		public int TotalActions
		{
			get { return totalActions; }
			set
			{
				if( totalActions == value )
					totalActions = value;
				else
				{
					totalActions = value;
					NotifyPropertyChanged("TotalActions");
				}
					
			}
		}
		
		private int maxActions = 5;
		public int MaxActions
		{
			get { return maxActions; }
			set
			{
				if( maxActions == value )
					maxActions = value;
				else
				{
					maxActions = value;
					NotifyPropertyChanged("MaxActions");
				}
					
			}
		}
		
		public double ActionsPercent
		{
			get
			{
				return ( this.MaxActions == 0 ) ? 0 : ((double)this.TotalActions)/this.MaxActions *100;
			}
		}
		
		private TimeSpan totalTime = TimeSpan.FromSeconds(25.0);
		public TimeSpan TotalTime
		{
			get { return totalTime; }
			set
			{
				if( totalTime == value )
					totalTime = value;
				else
				{
					totalTime = value;
					NotifyPropertyChanged("TotalTime");
					NotifyPropertyChanged("TimePercent");
				}
					
			}
		}
		
		private TimeSpan maxTime = TimeSpan.FromSeconds(30.0);
		public TimeSpan MaxTime
		{
			get { return maxTime; }
			set
			{
				if( maxTime == value )
					maxTime = value;
				else
				{
					maxTime = value;
					NotifyPropertyChanged("MaxTime");
					NotifyPropertyChanged("TimePercent");
				}
					
			}
		}
		
		public double TimePercent
		{
			get
			{
				return ( this.MaxTime.TotalSeconds == 0 ) ? 0 : this.TotalTime.TotalSeconds/this.MaxTime.TotalSeconds *100;
			}
		}
		
		public double TimeReverse
		{
			get
			{
				return 100-(( this.MaxTime.TotalSeconds == 0 ) ? 0 : this.TotalTime.TotalSeconds/this.MaxTime.TotalSeconds *100);
			}
		}
		
		private double intubationGrade = 0.95*100;
		public double IntubationGrade
		{
			get { return intubationGrade; }
			set
			{
				if ( intubationGrade == value )
					 intubationGrade = value;
				else
				{
					 intubationGrade = value;
					 NotifyPropertyChanged("IntubationGrade");
				}	
			}
		}
			//These Following variables are for Lateral Transfer Results
		private double handR = 0.85*100;
		public double HandR
		{
			get { return handR; }
			set
			{
				if( handR == value )
					handR = value;
				else
				{
					handR = value;
					NotifyPropertyChanged("HandR");
					NotifyPropertyChanged("HandAverage");
				}	
			}
		}
		
		private double handL = 0.63*100;
		public double HandL
		{
			get { return handL; }
			set
			{
				if( handL == value )
					handL = value;
				else
				{
					handL = value;
					NotifyPropertyChanged("HandL");
					NotifyPropertyChanged("HandAverage");
				}	
			}
		}
		
		
		public double HandAverage
		{
			get { return (handR + handL)*0.5; }
		}	
		
		
		private double wristR = 0.69*100;
		public double WristR
		{
			get { return wristR; }
			set
			{
				if( wristR == value )
					wristR = value;
				else
				{
					wristR = value;
					NotifyPropertyChanged("WristR");
					NotifyPropertyChanged("WristAverage");
				}	
			}
		}
		
		private double wristL = 0.75*100;
		public double WristL
		{
			get { return wristL; }
			set
			{
				if( wristL == value )
					wristL = value;
				else
				{
					wristL = value;
					NotifyPropertyChanged("WristL");
					NotifyPropertyChanged("WristAverage");
				}	
			}
		}
		
		
		public double WristAverage
		{
			get { return ( wristR + wristL )*0.5; }
			
		}
		
		
		private double elbowR = 0.71*100;
		public double ElbowR
		{
			get { return elbowR; }
			set
			{
				if( elbowR == value )
					elbowR = value;
				else
				{
					elbowR = value;
					NotifyPropertyChanged("ElbowR");
					NotifyPropertyChanged("ElbowAverage");
				}	
			}
		}
		
		private double elbowL = 0.83*100;
		public double ElbowL
		{
			get { return elbowL; }
			set
			{
				if( elbowL == value )
					elbowL = value;
				else
				{
					elbowL = value;
					NotifyPropertyChanged("ElbowL");
					NotifyPropertyChanged("ElbowAverage");
				}	
			}
		}
		
		
		public double ElbowAverage
		{
			get { return ( elbowR + elbowL )*0.5; }
			
		}
		
		
		private double shoulderR = 0.83*100;
		public double ShoulderR
		{
			get { return shoulderR; }
			set
			{
				if( shoulderR == value )
					shoulderR = value;
				else
				{
					shoulderR = value;
					NotifyPropertyChanged("ShoulderR");
					NotifyPropertyChanged("ShoulderAverage");
				}	
			}
		}
		
		private double shoulderL = 0.95*100;
		public double ShoulderL
		{
			get { return shoulderL; }
			set
			{
				if( shoulderL == value )
					shoulderL = value;
				else
				{
					shoulderL = value;
					NotifyPropertyChanged("ShoulderL");
					NotifyPropertyChanged("ShoulderAverage");
				}	
			}
		}
		
		
		public double ShoulderAverage
		{
			get { return ( shoulderR + shoulderL )*0.5; }
			
		}
		
		private double lateralTransGrade = 0.95*100;
		public double LateralTransGrade
		{
			get { return lateralTransGrade; }
			set
			{
				if ( lateralTransGrade == value )
					 lateralTransGrade = value;
				else
				{
					 lateralTransGrade = value;
					 NotifyPropertyChanged("LateralTransGrade");
				}	
			}
		}
			//These Following variables are for CPR
		
			//These Following variables are for Verticle lift
		
		
		public event PropertyChangedEventHandler PropertyChanged;
		
        private void NotifyPropertyChanged( string propertyName )
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
		
		
		/*
        public static readonly DependencyProperty KinectSensorManagerProperty = DependencyProperty.Register("KinectSensorManager", typeof(KinectSensorManager), typeof(StudentDashboard), new PropertyMetadata(null));

        public KinectSensorManager KinectSensorManager
        {
            get { return (KinectSensorManager)GetValue(KinectSensorManagerProperty); }
            set { SetValue(KinectSensorManagerProperty, value); }
        }
        */


        private Dictionary<string, int> taskFlowIndex = new Dictionary<string, int>();

        // Since the timer resolution defaults to about 10ms precisely, we need to
        // increase the resolution to get framerates above between 50fps with any
        // consistency.
        [DllImport("Winmm.dll", EntryPoint = "timeBeginPeriod")]
        private static extern int TimeBeginPeriod(uint period);


        public MainPage3()
		{
			this.DataContext = this;
			
			if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();
                Loaded += this.MainPage3_Loaded;
                Unloaded += this.MainPage3_Unloaded;

                IntubationScrollViewer.ScrollChanged += (o, e) => this.UpdateIntubationPagingButtonState();
                CPRScrollViewer.ScrollChanged += (o, e) => this.UpdateCPRPagingButtonState();
                LateralTransferScrollViewer.ScrollChanged += (o, e) => this.UpdateLateralTransferPagingButtonState();
                VerticalLiftScrollViewer.ScrollChanged += (o, e) => this.UpdateVerticalLiftPagingButtonState();
            }
		}

		private void MainPage3_Loaded(object sender, EventArgs e)
        {

            //this.SensorChooserUI.KinectSensorChooser = KinectManager.SensorChooser;

            if (KinectManager.Kinect != null)   // check if kinect exists
            {
                KinectManager.KinectSpeechCommander.SetModuleGrammars(new Uri("pack://application:,,,/AIMS;component/StudentDashboardSpeechGrammar.xml"),
                                                                        "activeListen", "defaultListen");
                KinectManager.KinectSpeechCommander.SpeechRecognized += new EventHandler<Speech.SpeechCommanderEventArgs>(KinectSpeechCommander_SpeechRecognized);

                kinectRegion.KinectSensor = KinectManager.Kinect;
            }
            else
            {
                MessageBox.Show("Kinect is not connected."); // notify user if kinect isn't set
            }

            if (((App)App.Current).CurrentStudent != null)
            {
                string fullname = "";

                string first = "";
                string last = "";

                first = ((App)App.Current).CurrentStudent.FirstName;
                last = ((App)App.Current).CurrentStudent.LastName;

                if (!String.IsNullOrWhiteSpace(first))
                    fullname += first;

                if (!String.IsNullOrWhiteSpace(last))
                {
                    if (!String.IsNullOrWhiteSpace(first))
                        fullname += " ";

                    fullname += last;
                }

                this.WelcomeTxt.Text = fullname;  // Set the name of the currently logged in student.

                this.FirstNameTB.Text = first;
                this.LastNameTB.Text = last;
            }

            //courses = RetreiveCoursesForStudentFromCourseIDs(((App)App.Current).CurrentStudent);

            //LoadIntubationStatsheetForCurrentStudent();

            //this.IntubationStatsheetComponent.ShowOverlay(((App)App.Current).CurrentStudent);
            tasking.Visibility = Visibility.Collapsed;
            messages.Visibility = Visibility.Collapsed;
            results.Visibility = Visibility.Collapsed;
            overview.Visibility = Visibility.Collapsed;
            /*
            StageItems.Children.Add(kbcalibration);
            StageItems.Children.Add(kbtilthead);
            StageItems.Children.Add(kbpickup);
            StageItems.Children.Add(kbinsertlar);
            StageItems.Children.Add(kbvisairway);
            StageItems.Children.Add(kbgrabtube);
            StageItems.Children.Add(kbinserttube);
            StageItems.Children.Add(kbremovelar);
            StageItems.Children.Add(kbremovesty);
            StageItems.Children.Add(kbsyringe);
            StageItems.Children.Add(kbpump);
             */

            //flow.Cache = new ThumbnailManager();    // create the flow cache (enhances re-loading preformance).
            //Load(@"C:\Users\eric.wilkinson\My Documents\Visual Studio 2010\Projects\AIMS_Updating2\AIMS_Updating\AIMS\Assets\Graphics\Dashboard Graphics"); // load the images for the coverflow (THIS WILL NEED TO CHANGE TO USE CLASS IMAGES)
        }

        //C:\Users\eric.wilkinson\Documents\Visual Studio 2010\Projects\AIMS_Updating2\AIMS_Updating\AIMS\Assets\Graphics\Intubation_trim.png

        private void MainPage3_Unloaded(object sender, RoutedEventArgs e)
        {
            // Handle unloading of student dashboard components here
            if (KinectManager.Kinect != null)   // if the kinect was never there, then no handlers were ever attached to begin with.
            {
                KinectManager.KinectSpeechCommander.SpeechRecognized -= KinectSpeechCommander_SpeechRecognized;
            }
        }
		
		//-----------------------------------------------------------------------------------------
		//KINECT SPEECH PROCESSING
		
		#region Kinect Speech processing
        private void KinectSpeechCommander_SpeechRecognized(object sender, Speech.SpeechCommanderEventArgs e)
        {
            switch (e.Command)
            {
                case "PAUSE":
                    OnSaid_Pause();
                    break;
                case "RESUME":
                    OnSaid_Resume();
                    break;
                case "LOGOUT":
                    OnSaid_Logout();
                    break;
                case "CPR":
                    OnSaid_CPR();
                    break;
                case "INTUBATION":
                    OnSaid_Intubation();
                    break;
                case "VERTICALLIFT":
                    OnSaid_VerticalLift();
                    break;
                case "PRACTICE":
                    OnSaid_Practice();
                    break;
                case "RESULTS":
                    OnSaid_Results();
                    break;
                case "TEST":
                    OnSaid_Test();
                    break;
                case "END_VIDEO":
                    OnSaid_EndVideo();
                    break;
                default:
                    break;
            }
        }

        private void OnSaid_Pause()
        {
            //SpeechDetectedTextBlock.Text = String.Format("Speech Detected: {0}", "Pause");
        }

        private void OnSaid_Resume()
        {
            //SpeechDetectedTextBlock.Text = String.Format("Speech Detected: {0}", "Resume");
        }

        private void OnSaid_Logout()
        {
            //SpeechDetectedTextBlock.Text = String.Format("Speech Detected: {0}", "Logout");

            //this.NavigationHeaderBar.Logout();
        }

        private void OnSaid_Test()
        {
            //SpeechDetectedTextBlock.Text = String.Format("Speech Detected: Do Task - {0}", "Test");

            //ShowStartTaskVerificationOverlay(TaskMode.Test);  // blurs the page, and displays the task verification overlay
            //NavigateToTestVerificationPage();
        }

        private void OnSaid_Practice()
        {
            //SpeechDetectedTextBlock.Text = String.Format("Speech Detected: Do Task - {0}", "Practice");

            //ShowStartTaskVerificationOverlay(TaskMode.Practice);  // blurs the page, and displays the task verification overlay
            //NavigateToPracticeVerificationPage();
        }

        private void OnSaid_Results()
        {
            //SpeechDetectedTextBlock.Text = String.Format("Speech Detected: Do Task - {0}", "Results");

            //NavigateToResultsPage();
        }

        private void OnSaid_Intubation()
        {
            //SpeechDetectedTextBlock.Text = String.Format("Speech Detected: Do Task - {0}", "Intubation");

            //flow.Index = 0; // Index of the Intubation button within the Coverflow (first)
        }

        private void OnSaid_CPR()
        {
            //SpeechDetectedTextBlock.Text = String.Format("Speech Detected: Do Task - {0}", "CPR");

            //flow.Index = 1; // Index of the Intubation button within the Coverflow (first)
        }

        private void OnSaid_VerticalLift()
        {
            //SpeechDetectedTextBlock.Text = String.Format("Speech Detected: Do Task - {0}", "Vertical Lift");

            //flow.Index = 2; // Index of the Intubation button within the Coverflow (first)
        }


        private void OnSaid_EndVideo()
        {
            //this.VideoPlayer.CloseVideo();
        }

        /*
        private void EnableAecChecked(object sender, RoutedEventArgs e)
        {
            var enableAecCheckBox = (CheckBox)sender;
            this.UpdateEchoCancellation(enableAecCheckBox);
        }

        private void UpdateEchoCancellation(CheckBox aecCheckBox)
        {
            this.mySpeechRecognizer.EchoCancellationMode = aecCheckBox.IsChecked != null && aecCheckBox.IsChecked.Value
                ? EchoCancellationMode.CancellationAndSuppression
                : EchoCancellationMode.None;
        }*/

        #endregion Kinect Speech processing
		
		
		//-------------------------------------------------------------------------------------------------
		//GENERAL MAINPAGE FUNCTIONS
		
		private void MyAccount_Click(object sender, RoutedEventArgs e)
		{
			NavigationService.Navigate(new System.Uri(@"../AccountSettings.xaml", System.UriKind.Relative));
		}
		
		//------------------------------------------------------------------------------------------
		//TASK SELECTION TAB
		
		//Button Logic for task selection
		private int currentSelectedTask = 1;
		
		private void Intubation_Selected_Click(object sender, RoutedEventArgs e)
		{
			ChangeTaskSelected(1);
        }

        private void VerticalLift_Selected_Click(object sender, RoutedEventArgs e)
        {
            ChangeTaskSelected(2);
        }

        private void LateralTransfer_Selected_Click(object sender, RoutedEventArgs e)
        {
            ChangeTaskSelected(3);
        }
		
		private void CPR_Selected_Click(object sender, RoutedEventArgs e)
		{
			ChangeTaskSelected(4);
		}
		
		private void ChangeTaskSelected(int mode)
		{
			ChangeTaskPic(mode);
			ChangeTaskTitle(mode);
			ChangeTaskOverviewText(mode);
			ChangeTaskRequirements(mode);
			currentSelectedTask = mode;

            EnableDisableButtons(mode);
		}

        private void EnableDisableButtons(int mode)
        {
            switch (mode)
            {
                case 1:
                    this.PracticeButton.IsEnabled = true;
                    this.TestButton.IsEnabled = true;
                    break;
                case 2:
                    this.PracticeButton.IsEnabled = false;
                    this.TestButton.IsEnabled = false;
                    break;
                case 3:
                    this.PracticeButton.IsEnabled = false;
                    this.TestButton.IsEnabled = false;
                    break;
                case 4:
                    this.PracticeButton.IsEnabled = false;
                    this.TestButton.IsEnabled = false;
                    break;
            }
        }

		private void ChangeTaskPic(int mode)
		{
			if(mode == 1)
			{
				TaskImage.Source = new BitmapImage(new Uri("../../Tasks/Intubation/Graphics/Intubation008.png", UriKind.Relative));
			}
			else if(mode == 2)
			{
                TaskImage.Source = new BitmapImage(new Uri("../../Tasks/VerticalLift/Graphics/VerticalLift003.png", UriKind.Relative));
			}
            else if (mode == 3)
			{
                TaskImage.Source = new BitmapImage(new Uri("../../Tasks/Lateral Transfer/Graphics/LateralTransfer003.png", UriKind.Relative));
			}
            else if (mode == 4)
			{
                TaskImage.Source = new BitmapImage(new Uri("../../Tasks/CPR/Graphics/CPR008.png", UriKind.Relative));
			}
		}
		
		private void ChangeTaskTitle(int mode)
		{
			if(mode == 1)
			{
				taskName.Text = "Intubation";
			}
            else if (mode == 2)
			{
				taskName.Text = "Vertical Lift";
			}
            else if (mode == 3)
			{
				taskName.Text = "Lateral Transfer";
			}
            else if (mode == 4)
			{
				taskName.Text = "CPR";
			}
		}
		
		private void ChangeTaskOverviewText(int mode)
		{
			if(mode == 1)
			{
				taskOverviewText1.Text = "Your patient's airway has collapsed, failed, or has become otherwise blocked.";
                taskOverviewText2.Text = "You must use the tools before you: a laryngoscope, a syrnge, an endotracheal tube, and a bag valve mask, to get them breathing again.";
			}
            else if (mode == 2)
			{
				taskOverviewText1.Text = "Learn how to safely lift objects to prevent injury to yourself.";
				taskOverviewText2.Text = "";
			}
            else if (mode == 3)
			{
				taskOverviewText1.Text = "Your patient needs to be transferred to a new bed to be moved to another location.";
				taskOverviewText2.Text = "Your team must move the patient without harm to a new bed or stretcher.";
			}
            else if (mode == 4)
			{
				taskOverviewText1.Text = "Your patient has gone into cardiac arrest. You must pump their heart for them until they stabilize.";
				taskOverviewText2.Text = "";
			}
		}
		
		
		private void ChangeTaskRequirements(int mode)
		{
			
		}
		
		//makes visible a learning page based on the currently selected task
		private void Learning_Button_Click(object sender, RoutedEventArgs e)
		{
			
			//send which procedure that learning needs to display
			if(currentSelectedTask == 1)
			{
				TaskingGrid.Visibility = System.Windows.Visibility.Hidden;
				LearningGrid.Visibility = System.Windows.Visibility.Visible;
			}
			
			if(currentSelectedTask == 2)
			{
				TaskingGrid.Visibility = System.Windows.Visibility.Hidden;
				LearningGrid_VerticalLift.Visibility = System.Windows.Visibility.Visible;
			}
			
			if(currentSelectedTask == 3)
			{
				TaskingGrid.Visibility = System.Windows.Visibility.Hidden;
				LearningGrid_LateralTransfer.Visibility = System.Windows.Visibility.Visible;
			}
			
			if(currentSelectedTask == 4)
			{
				TaskingGrid.Visibility = System.Windows.Visibility.Hidden;
				LearningGrid_CPR.Visibility = System.Windows.Visibility.Visible;
			}
			
			
			//switch to task from learning
			//TaskingGrid.Visibility = System.Windows.Visibility.Hidden;
			//LearningGrid.Visibility = System.Windows.Visibility.Visible;
			//LoadLearningPage();
		}
		
		//Returns to the taking page
		private void Return_Button_Click(object sender, RoutedEventArgs e)
		{
			TaskingGrid.Visibility = System.Windows.Visibility.Visible;
			LearningGrid.Visibility = System.Windows.Visibility.Hidden;
			LearningGrid_LateralTransfer.Visibility = System.Windows.Visibility.Hidden;
			LearningGrid_VerticalLift.Visibility = System.Windows.Visibility.Hidden;
			LearningGrid_CPR.Visibility = System.Windows.Visibility.Hidden;
			VideoPlayer.CloseVideo();
			VideoPlayer1.CloseVideo();
			VideoPlayer2.CloseVideo();
			VideoPlayer3.CloseVideo();
		}
		
		//---------------------------------------------------------------------------------------------
        //TASK SELECTION, GETTING TO TASKS.
		
		
		private void Test_Button_Click( object sender, RoutedEventArgs e)
		{
			TaskVerificationGrid.Visibility = System.Windows.Visibility.Visible;
		}
		
		private void Practice_Button_Click(object sender, RoutedEventArgs e)
		{
			TaskVerificationGrid.Visibility = System.Windows.Visibility.Visible;
		}
		
		private void Start_Button_Click(object sender, RoutedEventArgs e)
		{				
			TaskVerificationGrid.Visibility = System.Windows.Visibility.Collapsed;

			switch (currentSelectedTask)
            {
                case 1:
                {
                    NavigationService.Navigate(new System.Uri(@"../Tasks/Intubation/IntubationPage.xaml", System.UriKind.Relative));
                    break;
                }
                case 2:
                {
                    NavigationService.Navigate(new System.Uri(@"../Tasks/VerticalLift/UI/VerticalLiftPage.xaml", System.UriKind.Relative));
                    break;
                }
                case 3:
                {
                    NavigationService.Navigate(new System.Uri(@"../Tasks/Lateral Transfer/LateralTransferPage.xaml", System.UriKind.Relative));
                    break;
                }
				case 4:
                {
                    NavigationService.Navigate(new System.Uri(@"../Tasks/CPR/UI/CPRPage.xaml", System.UriKind.Relative));
                    break;
                }
            }
		}
		
		private void Cancel_Button_Click(object sender, RoutedEventArgs e)
		{
			TaskVerificationGrid.Visibility = System.Windows.Visibility.Hidden;
		}
		
		//-------------------------------------------------------------------------------------------------
		//LEARNING TAB
		
		
		
		/* code for filling in learning page info based on selected procedure
		- will have to implement later for easier integration of new tasks
		
		private AIMS.Tasks.Intubation.Intubation lesson = null;
		
		private void LoadLearningPage()
		{
			//intubation
			if(currentSelectedTask == 1)
			{
				fillLearning_Intubation();
			}
			
			else
				MessageBox.Show("LoadLearningPage not working");
		}
		
		private void fillLearning_Intubation()
		{
			StageItems.Children.Clear();
			foreach (Stage stage in lesson.Stages)
			{
				Button bi = new Button();
				bi.Content = stage.Name;
				bi.Click += this.stage_button_click;
				
			}
		}
		
		
		private void stage_button_click(object sender, RoutedEventArgs e)
		{
			//grab the button that was clicked, find out where it is in the stack panel
			Button srcBttn = e.Source as Button;
			int location = StageItems.Children.IndexOf(srcBttn);
			
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			
			//how do i grab a specific stage from lesson?
			//Stage tempStage = lesson.Stages;
			//t1.Text = tempStage.Instruction;
			
			t1.Text = "clicked";
			LearningInfoSP.Children.Clear();
			LearningInfoSP.Children.Add(t1);
			
			/*
			string currentLesson = lesson.Name;
			string path1,path2, stageNum;
			
			if(location < 10)
				stageNum = "0" + location;
			
			path1 = "../../Tasks/" + currentLesson + "/Videos/Stage 0" + stageNum + ".wmv";
			
			this.VideoPlayer.setVideoSource(new Uri(path1, UriKind.Relative));
			this.VideoPlayer.startVideo(this, new RoutedEventArgs());
			
			path2 = "../../Assets/Graphics/Stage Images/" + lessonName + "/" + lessonName + "0" + stageNum + ".png";
			LearningImage.Source = new BitmapImage(new Uri(path2, UriKind.Relative));
			
		}
		*/
		
		//-----------------------------------------------------------------------------------------------
		//LEARNING PAGE INTUBATION
		
		private void stage000_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must calibrate with AIMS by raising your right hand and placing your left hand on the mannequin's face.";
			LearningInfoSP.Children.Clear();
			LearningInfoSP.Children.Add(t1);
			this.VideoPlayer.setVideoSource(new Uri("Tasks/Intubation/Videos/Stage 000.wmv", UriKind.Relative));
			this.VideoPlayer.startVideo(this, new RoutedEventArgs());
			LearningImage.Source = new BitmapImage(new Uri("../../Tasks/Intubation/Graphics/Intubation000.png", UriKind.Relative));
			LStageName.Text = "Calibration";
		}
		
		
		private void stage001_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must tilt the patient's head back to be properly intubated.";
			LearningInfoSP.Children.Clear();
			LearningInfoSP.Children.Add(t1);
			this.VideoPlayer.setVideoSource(new Uri("Tasks/Intubation/Videos/Stage 001.wmv", UriKind.Relative));
			this.VideoPlayer.startVideo(this, new RoutedEventArgs());
			LearningImage.Source = new BitmapImage(new Uri("../../Tasks/Intubation/Graphics/Intubation001.png", UriKind.Relative));
			LStageName.Text = "Tilt Head";
		}
		
		private void stage002_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must pick up the laryngoscope and lock it into position.";
			LearningInfoSP.Children.Clear();
			LearningInfoSP.Children.Add(t1);
			this.VideoPlayer.setVideoSource(new Uri("Tasks/Intubation/Videos/Stage 002.wmv", UriKind.Relative));
			this.VideoPlayer.startVideo(this, new RoutedEventArgs());
			LearningImage.Source = new BitmapImage(new Uri("../../Tasks/Intubation/Graphics/Intubation002.png", UriKind.Relative));
			LStageName.Text = "Pick Up Lscope";
		}
		
		private void stage003_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must Insert the laryngoscope into the right side of the mouth. Make sure not to hit the teeth.";
			LearningInfoSP.Children.Clear();
			LearningInfoSP.Children.Add(t1);
			this.VideoPlayer.setVideoSource(new Uri("Tasks/Intubation/Videos/Stage 003.wmv", UriKind.Relative));
			this.VideoPlayer.startVideo(this, new RoutedEventArgs());
			LearningImage.Source = new BitmapImage(new Uri("../../Tasks/Intubation/Graphics/Intubation003.png", UriKind.Relative));
			LStageName.Text = "Insert Lscope";
		}

		private void stage004_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must bend down to view the vocal chords.";
			LearningInfoSP.Children.Clear();
			LearningInfoSP.Children.Add(t1);
			this.VideoPlayer.setVideoSource(new Uri("Tasks/Intubation/Videos/Stage 004.wmv", UriKind.Relative));
			this.VideoPlayer.startVideo(this, new RoutedEventArgs());
			LearningImage.Source = new BitmapImage(new Uri("../../Tasks/Intubation/Graphics/Intubation004.png", UriKind.Relative));
			LStageName.Text = "Visualize Airway";
		}

		private void stage005_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must pick up the Endotracheal Tube.";
			LearningInfoSP.Children.Clear();
			LearningInfoSP.Children.Add(t1);
			this.VideoPlayer.setVideoSource(new Uri("Tasks/Intubation/Videos/Stage 005.wmv", UriKind.Relative));
			this.VideoPlayer.startVideo(this, new RoutedEventArgs());
			LearningImage.Source = new BitmapImage(new Uri("../../Tasks/Intubation/Graphics/Intubation005.png", UriKind.Relative));
			LStageName.Text = "Grab Tube";
		}

		private void stage006_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must insert the Endotracheal tube into the patient's trachea until it is just beyond the vocal chords.";
			LearningInfoSP.Children.Clear();
			LearningInfoSP.Children.Add(t1);
			this.VideoPlayer.setVideoSource(new Uri("Tasks/Intubation/Videos/Stage 006.wmv", UriKind.Relative));
			this.VideoPlayer.startVideo(this, new RoutedEventArgs());
			LearningImage.Source = new BitmapImage(new Uri("../../Tasks/Intubation/Graphics/Intubation006.png", UriKind.Relative));
			LStageName.Text = "Insert Tube";
		}

		private void stage007_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must use your right hand to stabilize the tube while removing the laryngoscope with your left. ";
			LearningInfoSP.Children.Clear();
			LearningInfoSP.Children.Add(t1);
			this.VideoPlayer.setVideoSource(new Uri("Tasks/Intubation/Videos/Stage 007.wmv", UriKind.Relative));
			this.VideoPlayer.startVideo(this, new RoutedEventArgs());
			LearningImage.Source = new BitmapImage(new Uri("../../Tasks/Intubation/Graphics/Intubation007.png", UriKind.Relative));
			LStageName.Text = "Remove Lscope";
		}

		private void stage008_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must stabilize the tube with your left hand and use your right hand to remove the stylet from the tube and set it down";
			LearningInfoSP.Children.Clear();
			LearningInfoSP.Children.Add(t1);
			this.VideoPlayer.setVideoSource(new Uri("Tasks/Intubation/Videos/Stage 008.wmv", UriKind.Relative));
			this.VideoPlayer.startVideo(this, new RoutedEventArgs());
			LearningImage.Source = new BitmapImage(new Uri("../../Tasks/Intubation/Graphics/Intubation008.png", UriKind.Relative));
			LStageName.Text = "Remove Stylet";
		}

		private void stage009_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must pick up the syringe with your right hand and compress it to inflate the balloon cuff. This will keep the tube from slipping out of position.";
			LearningInfoSP.Children.Clear();
			LearningInfoSP.Children.Add(t1);
			this.VideoPlayer.setVideoSource(new Uri("Tasks/Intubation/Videos/Stage 009.wmv", UriKind.Relative));
			this.VideoPlayer.startVideo(this, new RoutedEventArgs());
			LearningImage.Source = new BitmapImage(new Uri("Tasks/Intubation/Graphics/Intubation009.png", UriKind.Relative));
			LStageName.Text = "Syringe";
		}

		private void stage010_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must Insert the laryngoscope into the right side of the mouth. Make sure not to hit the teeth.";
			LearningInfoSP.Children.Clear();
			LearningInfoSP.Children.Add(t1);
			this.VideoPlayer.setVideoSource(new Uri("Tasks/Intubation/Videos/Stage 010.wmv", UriKind.Relative));
			this.VideoPlayer.startVideo(this, new RoutedEventArgs());
			LearningImage.Source = new BitmapImage(new Uri("../../Tasks/Intubation/Graphics/Intubation010.png", UriKind.Relative));
			LStageName.Text = "Pump Balloon";
		}
		
		
		//-----------------------------------------------------------------------------------------------
		//LEARNING PAGE VERTICAL LIFT
		
		private void stage200_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must stand in front of the object and raise your right hand to calibrate.";
			LearningInfoSP3.Children.Clear();
			LearningInfoSP3.Children.Add(t1);
			this.VideoPlayer3.setVideoSource(new Uri("Tasks/VerticalLift/Videos/VertLift 000.wmv", UriKind.Relative));
			this.VideoPlayer3.startVideo(this, new RoutedEventArgs());
			LearningImage3.Source = new BitmapImage(new Uri("../../Tasks/VerticalLift/Graphics/VerticalLift000.png", UriKind.Relative));
			LStageName3.Text = "Calibration";
		}
		
		private void stage201_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must squat down, keeping your back straight to pick up the object.";
			LearningInfoSP3.Children.Clear();
			LearningInfoSP3.Children.Add(t1);
			this.VideoPlayer3.setVideoSource(new Uri("Tasks/VerticalLift/Videos/VertLift 001.wmv", UriKind.Relative));
			this.VideoPlayer3.startVideo(this, new RoutedEventArgs());
			LearningImage3.Source = new BitmapImage(new Uri("../../Tasks/VerticalLift/Graphics/VerticalLift001.png", UriKind.Relative));
			LStageName3.Text = "Squat";
		}
		
		private void stage202_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must grab the object and lift while keeping your back straight.";
			LearningInfoSP3.Children.Clear();
			LearningInfoSP3.Children.Add(t1);
			this.VideoPlayer3.setVideoSource(new Uri("Tasks/VerticalLift/Videos/VertLift 002.wmv", UriKind.Relative));
			this.VideoPlayer3.startVideo(this, new RoutedEventArgs());
			LearningImage3.Source = new BitmapImage(new Uri("../../Tasks/VerticalLift/Graphics/VerticalLift002.png", UriKind.Relative));
			LStageName3.Text = "Stand";
		}
		
		private void stage203_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must squat back down and put down the object.";
			LearningInfoSP3.Children.Clear();
			LearningInfoSP3.Children.Add(t1);
			this.VideoPlayer3.setVideoSource(new Uri("Tasks/VerticalLift/Videos/VertLift 003.wmv", UriKind.Relative));
			this.VideoPlayer3.startVideo(this, new RoutedEventArgs());
			LearningImage3.Source = new BitmapImage(new Uri("../../Tasks/VerticalLift/Graphics/VerticalLift003.png", UriKind.Relative));
			LStageName3.Text = "Put Down";
		}

		//-----------------------------------------------------------------------------------------------
		//LEARNING PAGE LATERAL TRANSFER
		
		private void stage300_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must calibrate by standing behind the patient and raising your right hand.";
			LearningInfoSP2.Children.Clear();
			LearningInfoSP2.Children.Add(t1);
			this.VideoPlayer2.setVideoSource(new Uri("Tasks/Lateral Transfer/Videos/LatTrans_000.wmv", UriKind.Relative));
			this.VideoPlayer2.startVideo(this, new RoutedEventArgs());
			LearningImage2.Source = new BitmapImage(new Uri("../../Tasks/Lateral Transfer/Graphics/LateralTransfer000.png", UriKind.Relative));
			LStageName2.Text = "Calibration";
		}

		private void stage301_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must lean over the bed and grasp the sheet under the patient.";
			LearningInfoSP2.Children.Clear();
			LearningInfoSP2.Children.Add(t1);
			this.VideoPlayer2.setVideoSource(new Uri("Tasks/Lateral Transfer/Videos/LatTrans_001.wmv", UriKind.Relative));
			this.VideoPlayer2.startVideo(this, new RoutedEventArgs());
			LearningImage2.Source = new BitmapImage(new Uri("../../Tasks/Lateral Transfer/Graphics/LateralTransfer001.png", UriKind.Relative));
			LStageName2.Text = "Prepare Movement";
		}
			
		private void stage302_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must work in unison in preparing to pull; Either by counting down or 'Ready,Set,Pull'.";
			LearningInfoSP2.Children.Clear();
			LearningInfoSP2.Children.Add(t1);
			this.VideoPlayer2.setVideoSource(new Uri("Tasks/Lateral Transfer/Videos/LatTrans_002.wmv", UriKind.Relative));
			this.VideoPlayer2.startVideo(this, new RoutedEventArgs());
			LearningImage2.Source = new BitmapImage(new Uri("../../Tasks/Lateral Transfer/Graphics/LateralTransfer002.png", UriKind.Relative));
			LStageName2.Text = "Count Down";
		}
			
		private void stage303_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must move the patient onto the new bed.";
			LearningInfoSP2.Children.Clear();
			LearningInfoSP2.Children.Add(t1);
			this.VideoPlayer2.setVideoSource(new Uri("Tasks/Lateral Transfer/Videos/LatTrans_003.wmv", UriKind.Relative));
			this.VideoPlayer2.startVideo(this, new RoutedEventArgs());
			LearningImage2.Source = new BitmapImage(new Uri("../../Tasks/Lateral Transfer/Graphics/LateralTransfer003.png", UriKind.Relative));
			LStageName2.Text = "Move Patient";
		}
		//-----------------------------------------------------------------------------------------------
		//LEARNING PAGE CPR
		
		private void stage400_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must calibrate by kneeling behind the mannequin, placing your left hand in the center of the chest, and raising your right hand.";
			LearningInfoSP1.Children.Clear();
			LearningInfoSP1.Children.Add(t1);
			this.VideoPlayer1.setVideoSource(new Uri("Tasks/Cpr/Videos/CPR 000.wmv", UriKind.Relative));
			this.VideoPlayer1.startVideo(this, new RoutedEventArgs());
			LearningImage1.Source = new BitmapImage(new Uri("../../Tasks/Cpr/Graphics/CPR000.png", UriKind.Relative));
			LStageName1.Text = "Calibration";
		}
		private void stage401_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must check for a response from the patient";
			LearningInfoSP1.Children.Clear();
			LearningInfoSP1.Children.Add(t1);
			this.VideoPlayer1.setVideoSource(new Uri("Tasks/Cpr/Videos/CPR 001.wmv", UriKind.Relative));
			this.VideoPlayer1.startVideo(this, new RoutedEventArgs());
			LearningImage1.Source = new BitmapImage(new Uri("../../Tasks/Cpr/Graphics/CPR001.png", UriKind.Relative));
			LStageName1.Text = "Check Patient";
		}
		private void stage402_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must check the patient's pulse.";
			LearningInfoSP1.Children.Clear();
			LearningInfoSP1.Children.Add(t1);
			this.VideoPlayer1.setVideoSource(new Uri("Tasks/Cpr/Videos/CPR 002.wmv", UriKind.Relative));
			this.VideoPlayer1.startVideo(this, new RoutedEventArgs());
			LearningImage1.Source = new BitmapImage(new Uri("../../Tasks/Cpr/Graphics/CPR003.png", UriKind.Relative));
			LStageName1.Text = "Check Pulse";
		}
		private void stage403_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must tell AIMI to call 911.";
			LearningInfoSP1.Children.Clear();
			LearningInfoSP1.Children.Add(t1);
			this.VideoPlayer1.setVideoSource(new Uri("Tasks/Cpr/Videos/CPR 003.wmv", UriKind.Relative));
			this.VideoPlayer1.startVideo(this, new RoutedEventArgs());
			LearningImage1.Source = new BitmapImage(new Uri("../../Tasks/Cpr/Graphics/CPR004.png", UriKind.Relative));
			LStageName1.Text = "Call 911";
		}
		private void stage404_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must tell AIMI to get a defibrulator";
			LearningInfoSP1.Children.Clear();
			LearningInfoSP1.Children.Add(t1);
			this.VideoPlayer1.setVideoSource(new Uri("Tasks/Cpr/Videos/CPR 004.wmv", UriKind.Relative));
			this.VideoPlayer1.startVideo(this, new RoutedEventArgs());
			LearningImage1.Source = new BitmapImage(new Uri("../../Tasks/Cpr/Graphics/CPR005.png", UriKind.Relative));
			LStageName1.Text = "Call for Tools";
		}
		private void stage405_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must place your hands in the correct position on the chest.";
			LearningInfoSP1.Children.Clear();
			LearningInfoSP1.Children.Add(t1);
			this.VideoPlayer1.setVideoSource(new Uri("Tasks/Cpr/Videos/CPR 005.wmv", UriKind.Relative));
			this.VideoPlayer1.startVideo(this, new RoutedEventArgs());
			LearningImage1.Source = new BitmapImage(new Uri("../../Tasks/Cpr/Graphics/CPR006.png", UriKind.Relative));
			LStageName1.Text = "Place Hands";
		}
		private void stage406_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must make sure your shoulders are directly over your hands, with arms straight down and locked.";
			LearningInfoSP1.Children.Clear();
			LearningInfoSP1.Children.Add(t1);
			this.VideoPlayer1.setVideoSource(new Uri("Tasks/Cpr/Videos/CPR 006.wmv", UriKind.Relative));
			this.VideoPlayer1.startVideo(this, new RoutedEventArgs());
			LearningImage1.Source = new BitmapImage(new Uri("../../Tasks/Cpr/Graphics/CPR007.png", UriKind.Relative));
			LStageName1.Text = "Body Position";
		}
		private void stage407_click(object sender, RoutedEventArgs e)
		{
			TextBlock t1 = new TextBlock();
			t1.TextWrapping = TextWrapping.Wrap;
			t1.Padding = new Thickness(10);
			t1.Text = "In this stage you must push down on the chest 2 inches and pump the chest for 2 minutes.";
			LearningInfoSP1.Children.Clear();
			LearningInfoSP1.Children.Add(t1);
			this.VideoPlayer1.setVideoSource(new Uri("Tasks/Cpr/Videos/CPR 007.wmv", UriKind.Relative));
			this.VideoPlayer1.startVideo(this, new RoutedEventArgs());
			LearningImage1.Source = new BitmapImage(new Uri("../../Tasks/Cpr/Graphics/CPR008.png", UriKind.Relative));
			LStageName1.Text = "Pump Chest";
		}


    //-------------------------------------------------------------------------------------------------------------------
    // RESULTS (OVERVIEW) TAB
    //-------------------------------------------------------------------------------------------------------------------

        private TaskResult selectedResult;
        public TaskResult SelectedResult
        {
            get { return selectedResult; }
            set
            {
                if (selectedResult != value)
                {
                    selectedResult = value;

                    this.ResultsListView.ScrollIntoView(selectedResult);
                    
                    NotifyPropertyChanged("SelectedResult");

                    this.ShowTaskDetails();
                }
            }
        }

        private void ShowTaskDetails()
        {
            if( SelectedResult is IntubationResult )
            {
                IntubationResult tmpIntRslt = selectedResult as IntubationResult;
                IntubationResult.RetrieveImages(ref tmpIntRslt);
                this.TestResultsIntubation.ShowResults(SelectedResult, false );
            }
            else if( this.SelectedResult is CprResult )
            {
                CprResult tmpCprRslt = selectedResult as CprResult;
                CprResult.RetrieveImages(ref tmpCprRslt);
                this.TestResultsCPR.ShowResults(SelectedResult, false);
            }
            else if (this.SelectedResult is LateralTransferResult)
            {
                LateralTransferResult tmpLatTrnRslt = selectedResult as LateralTransferResult;
                LateralTransferResult.RetrieveImages(ref tmpLatTrnRslt);
                this.TestResultsLateralTransfer.ShowResults(SelectedResult, false);
            }
            else
            {
                // None Selected.
            }
        }


        private ComboBoxItem selectedModuleType;
        public ComboBoxItem SelectedModuleType
        {
            get { return selectedModuleType; }
            set
            {
                if (selectedModuleType != value)
                {
                    selectedModuleType = value;
                    this.SetResultsCollection(selectedModuleType.Content.ToString());
                    NotifyPropertyChanged("SelectedModuleType");
                }
            }
        }


        //private void AreaSeries_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    //TaskResult result = (TaskResult)e.AddedItems[0];
        //    //System.Windows.MessageBox.Show("Result UID = " + result.UID.ToString() + ",  Score = " + result.Score.ToString() + "\nSubmissionTime = " + result.SubmissionTime.ToString() + "\nIndex = " + result.Index.ToString());

        //    AreaSeries areaSeries = sender as AreaSeries;
        //    TaskResult selectedResult = areaSeries.SelectedItem as TaskResult;

        //    if (selectedResult != null)
        //    {
        //        System.Windows.MessageBox.Show("Result UID = " + selectedResult.UID.ToString() + ",  Score = " + selectedResult.Score.ToString() + "\nSubmissionTime = " + selectedResult.SubmissionTime.ToString() + "\nIndex = " + selectedResult.Index.ToString());
        //    } 
        //}
		
        //private void ModuleTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    ComboBoxItem cbi = ((sender as ComboBox).SelectedItem as ComboBoxItem);
        //    this.SetResultsCollection(cbi.Content.ToString());
        //}


        private void SetResultsCollection(string taskType)
        {
            if (taskType.Contains("Lateral Transfer"))
            {
                if (this.lateralTransferResults == null)
                    this.lateralTransferResults = this.CreateLateralTransferResults();

                this.resultsCollection.ReplaceRange(this.lateralTransferResults);
            }
            else if (taskType.Contains("CPR"))
            {
                if (this.cprResults == null)
                    this.cprResults = this.CreateCPRResults();

                this.resultsCollection.ReplaceRange(this.cprResults);
            }
            else if (taskType.Contains("Vertical Lift"))
            {
                if (this.verticalLiftResults == null)
                    this.verticalLiftResults = this.CreateVerticalLiftResults();

                this.resultsCollection.ReplaceRange(this.verticalLiftResults);
            }
            else if (taskType.Contains("Intubation"))
            {
                if (this.intubationResults == null)
                    this.intubationResults = this.CreateIntubationResults();

                this.resultsCollection.ReplaceRange(this.intubationResults);
            }
        }


        private ObservableRangeCollection<LateralTransferResult> CreateLateralTransferResults()
        {
            ObservableRangeCollection<LateralTransferResult> results = new ObservableRangeCollection<LateralTransferResult>();
            DataTable dataTable;
            int resultid = 0;
            int index = 0;

            dataTable = this.CreateResultsDatatable(((App)App.Current).CurrentStudent.ID, "taskresults", "uid", "LateralTransfer", "submissiontime");

            foreach (DataRow row in dataTable.Rows)
            {
                resultid = (int)row["uid"];
                results.Add(LateralTransferResult.RetrieveResult(resultid, false));
                results[index].Index = ++index;
            }

            return results;
        }


        private ObservableRangeCollection<CprResult> CreateCPRResults()
        {
            ObservableRangeCollection<CprResult> results = new ObservableRangeCollection<CprResult>();
            DataTable dataTable;
            int resultid = 0;
            int index = 0;

            dataTable = this.CreateResultsDatatable(((App)App.Current).CurrentStudent.ID, "taskresults", "uid", "CPR", "submissiontime");

            foreach (DataRow row in dataTable.Rows)
            {
                resultid = (int)row["uid"];
                results.Add(CprResult.RetrieveResult(resultid, false));
                results[index].Index = ++index;
            }

            return results;
        }


        private ObservableRangeCollection<VerticalLiftResult> CreateVerticalLiftResults()
        {
            ObservableRangeCollection<VerticalLiftResult> results = new ObservableRangeCollection<VerticalLiftResult>();
            DataTable dataTable;
            int resultid = 0;
            int index = 0;

            dataTable = this.CreateResultsDatatable(((App)App.Current).CurrentStudent.ID, "taskresults", "uid", "VerticalLift", "submissiontime");

            foreach (DataRow row in dataTable.Rows)
            {
                resultid = (int)row["uid"];
                results.Add(VerticalLiftResult.RetrieveResult(resultid, false));
                results[index].Index = ++index;
            }

            return results;
        }


        private ObservableRangeCollection<IntubationResult> CreateIntubationResults()
        {
            ObservableRangeCollection<IntubationResult> results = new ObservableRangeCollection<IntubationResult>();
            DataTable dataTable;
            int resultid = 0;
            int index = 0;

            dataTable = this.CreateResultsDatatable(((App)App.Current).CurrentStudent.ID, "taskresults", "uid", "Intubation", "submissiontime");

            foreach (DataRow row in dataTable.Rows)
            {
                resultid = (int)row["uid"];
                results.Add(IntubationResult.RetrieveResult(resultid, false));
                results[index].Index = ++index;
            }

            return results;
        }

        // Create an existing Student's Task Results DataTable from the database
        private DataTable CreateResultsDatatable(int studentID, string tableName, string columnName, string taskType, string orderBy)
        {
            DataSet dataSet = new DataSet();
            DataTable dataTable = new DataTable();

            DatabaseManager.OpenConnection();

            NpgsqlDataAdapter da = new NpgsqlDataAdapter("SELECT " + columnName + " FROM " + tableName + " WHERE studentid=" + studentID + " AND tasktype='" + taskType + "' ORDER BY " + orderBy, DatabaseManager.Connection);
            da.Fill(dataSet);
            dataTable = dataSet.Tables[0];

            DatabaseManager.CloseConnection();

            return dataTable;
        }
        
        //
        // OLD Charts, Results and Overview tab (Copied from StudentResultsPage student results)
        //
        private void OverviewChart_DataPoint_MouseClick(object sender, MouseEventArgs e)
        {
            DataPoint dp = sender as DataPoint;

            if (dp != null)
            {
                e.Handled = true;   // handle the exception

                //MessageBox.Show(String.Format("x: {0}, y: {1}", dp.IndependentValue, dp.DependentValue));
            }
        }
		
		 /// <summary>
        /// Delayed timer that populates the graphs.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_Tick(object sender, EventArgs e)
        {
            
            OverviewChart.Visibility = Visibility.Visible;       // Show the overall chart.
            timer.Stop();
        }
		
		private void FillResultChartsFromDatabase()
        {
            try
            {
                dt = CreateDataTable(); // builds a data table using static columns, uses DB data to populate, and sets our "dt" datatable value to that table
            }
            catch (Exception msg)
            {
                Console.WriteLine(msg.ToString());
                //MessageBox.Show(msg.ToString());
            }
        }

        

        /// <summary>
        /// This method is used to pull out the byte[] from the database, and deserialize it. 
        /// This can then be used to populate the singleChart for the current "displayIndex".
        /// </summary>
        private void deserializeStageScoreFromDatabase()
        {
            
            DatabaseManager.CloseConnection();

            if (DatabaseManager.Connection.State != ConnectionState.Open)
            {
                DatabaseManager.OpenConnection();
            }

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandText = "SELECT uid, resultid, stagescore FROM student_results_stage_scores WHERE resultid = :resultnumber";
           //not sure what this is used for
			// command.Parameters.Add("resultnumber", NpgsqlTypes.NpgsqlDbType.Integer).Value = resultnumber;

            NpgsqlDataReader sqlresult = command.ExecuteReader();

            stageScoreList = null;
            stageScoreList = new List<StageScore>();
            stageScoreIndexList = null;
            stageScoreIndexList = new List<int>();

            int uid;
            int resultid;
            byte[] stagescore = null;

            try
            {
                while (sqlresult.Read())
                {
                    uid = sqlresult.GetInt32(sqlresult.GetOrdinal("uid"));
                    resultid = sqlresult.GetInt32(sqlresult.GetOrdinal("resultid"));
                    stagescore = (byte[])sqlresult.GetValue(sqlresult.GetOrdinal("stagescore"));

                    StageScore stageScore = null;

                    try
                    {
                        stageScore = ByteArrayToObject(stagescore) as StageScore;   // deserialize the stageScore byteArray data read from the db.
                    }
                    catch
                    {
                        //MessageBox.Show(exception.ToString());  // error deserializing..
                    }

                    if( stageScore != null )    // if the stage score exists
                    {
                        stageScoreList.Add(stageScore); // add it to the list of stage scores.
                        stageScoreIndexList.Add(uid);
                    }
                }
            }
            catch( Exception)
            {
                //MessageBox.Show( e.ToString(), "StageScore was not successfully pulled from Database.");
                return;
            }

            DatabaseManager.CloseConnection();

            // Build out the singleChart using this data list
            BuildOverviewResultChart(stageScoreList);
        }
		
		/// <summary>
        /// This method is called when a DataPoint on the line chart is clicked, it finds the index of the clicked point, checks if there is a screenshot,
        /// and then finds the linked stagescoreID number. With this number, the method recalls deserializeScreenShotFromDatabase - this
        /// time passing in the int value of the linked stagescoreID.
        /// </summary>
        /// <param name="dp"></param>
        private void deserializeScreenShotFromDatabase( KeyValuePair<int, double> pair )
        {
            int index = pair.Key - 1;   // key's represet stage score # -> they are 1-5 (for example) rather than 0-4, so we subtract 1  to get proper 0-based index value.

            StageScore stagescore = stageScoreList[index];
            int stagescoreid = stageScoreIndexList[index];

            if (stagescore.HasScreenshot)
            {
                deserializeScreenShotFromDatabase(stagescoreid);
            }
            else
            {
                //MessageBox.Show("StageScore has no screenshot");
            }
        }
		
		/// <summary>
        /// This method is used to access the database and retreive the screenshot binary data linked to the passed stagescoreID#.
        /// The method retreives the binary data, deserializes it into a screenshot object, and then makes a call to draw the 3D screenshot
        /// into the 3DViewer, using the newly re-constructed screenshot object.
        /// </summary>
        /// <param name="stagescoreID"></param>
        private void deserializeScreenShotFromDatabase( int stagescoreID )
        {
            DatabaseManager.CloseConnection();

            if (DatabaseManager.Connection.State != ConnectionState.Open)
            {
                DatabaseManager.OpenConnection();
            }

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandText = String.Format("SELECT uid, stagescoreid, coloranddepthdata FROM student_results_3dscreenshots WHERE stagescoreid = :scoreID");
            command.Parameters.Add("scoreID", NpgsqlTypes.NpgsqlDbType.Integer).Value = stagescoreID;

            NpgsqlDataReader sqlresult = command.ExecuteReader();

            int uid;
            int stagescoreid;
            byte[] coloranddepthdata = null;

            try
            {
                while (sqlresult.Read())
                {
                    uid = sqlresult.GetInt32(sqlresult.GetOrdinal("uid"));
                    stagescoreid = sqlresult.GetInt32(sqlresult.GetOrdinal("stagescoreid"));
                    coloranddepthdata = (byte[])sqlresult.GetValue(sqlresult.GetOrdinal("coloranddepthdata"));

                    StageScore.Screenshot screenshot = null;

                    try
                    {
                        screenshot = ByteArrayToObject(coloranddepthdata) as StageScore.Screenshot;   // deserialize the stageScore byteArray data read from the db.
                    }
                    catch (Exception)
                    {
                        //MessageBox.Show(exception.ToString());  // error deserializing..
                    }

                    if (screenshot != null)    // if the stage score exists
                    {
                        
                    }
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(e.ToString(), "StageScore was not successfully pulled from Database.");
                return;
            }

            DatabaseManager.CloseConnection();
        }
		
		private void BuildOverviewResultChart( List<StageScore> stageScoreList )
        {
            List<KeyValuePair<int, double>> values = new List<KeyValuePair<int, double>>(); // list of key value pairs (int = stage#, double = score )

            foreach (StageScore stageScore in stageScoreList)
            {
                values.Add( new KeyValuePair<int, double>( stageScore.StageNumber + 1, stageScore.Score * 100 ) );
                ////MessageBox.Show(String.Format("New keyValuePair added: stage: {0}, score: {1}", stageScore.StageNumber + 1, stageScore.Score * 100));
            }

            try
            {
                (OverviewChart.Series[0] as DataPointSeries).ItemsSource = null;
                (OverviewChart.Series[0] as DataPointSeries).ItemsSource = values;
				OverviewChart.UpdateLayout();

                ////MessageBox.Show("SinglesChart updated");
            }
            catch (Exception ex)
            {
                ////MessageBox.Show(ex.ToString());
                Console.WriteLine(ex.ToString());
            }

            /*DataPoint firstDP = ( singleChart.Series[0] as DataPointSeries )*/
            deserializeScreenShotFromDatabase(0);
        }
		
		/// <summary>
        /// Function to get object from byte array
        /// </summary>
        /// <param name="_ByteArray">byte array to get object</param>
        /// <returns>object</returns>
        public object ByteArrayToObject(byte[] _ByteArray)
        {
            try
            {
                // convert byte array to memory stream
                System.IO.MemoryStream _MemoryStream = new System.IO.MemoryStream(_ByteArray);

                // create new BinaryFormatter
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter _BinaryFormatter
                            = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                // set memory stream position to starting point
                _MemoryStream.Position = 0;

                // Deserializes a stream into an object graph and return as a object.
                return _BinaryFormatter.Deserialize(_MemoryStream);
            }
            catch (Exception _Exception)
            {
                // Error
                Console.WriteLine("Exception caught in process: {0}", _Exception.ToString());
                ////MessageBox.Show(_Exception.ToString());
            }

            // Error occured, return null
            return null;
        }
		
		private DataTable CreateDataTable()
        {
            DataTable dataTable = new DataTable();

            DataColumn myDataColumn;

            myDataColumn = new DataColumn();
            myDataColumn.DataType = Type.GetType("System.Int32");
            myDataColumn.ColumnName = "uid";
            dataTable.Columns.Add(myDataColumn);


            myDataColumn = new DataColumn();
            myDataColumn.DataType = Type.GetType("System.Int32");
            myDataColumn.ColumnName = "#";
            dataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = Type.GetType("System.DateTime");
            myDataColumn.ColumnName = "Date";
            dataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = Type.GetType("System.Boolean");
            myDataColumn.ColumnName = "Passed";
            dataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = Type.GetType("System.Double");
            myDataColumn.ColumnName = "Total Score";
            dataTable.Columns.Add(myDataColumn);

            ////MessageBox.Show( String.Format( "CurrentUserID: {0}, CurrentTask#: {1}", ((App)App.Current).CurrentUserID, (int)((App)App.Current).CurrentTask ) );
            NpgsqlCommand command = new NpgsqlCommand( String.Format( "select uid,date,passed,totalscore FROM student_results where studentid='{0}' AND tasktype='{1}' ORDER BY uid", ((App)App.Current).CurrentStudent.ID, (int)((App)App.Current).CurrentTask ), DatabaseManager.Connection);
            DatabaseManager.OpenConnection();
            NpgsqlDataReader reader = command.ExecuteReader();

            int indx = 1;

            while ( reader.Read() )
            {
                DataRow row = dataTable.NewRow();

                row[0] = reader.GetInt32( reader.GetOrdinal("uid") );   // this row is hidden, but holds valuable information (the id# of the current result).
                row[1] = indx++;
                row[2] = reader.GetDateTime( reader.GetOrdinal("date") );
                row[3] = reader.GetBoolean( reader.GetOrdinal("passed") );
                row[4] = reader.GetDouble( reader.GetOrdinal("totalscore") );

                dataTable.Rows.Add(row);
            }

            reader.Dispose();

            DatabaseManager.CloseConnection();
            //ResultsDataGrid.DataContext = dataTable.DefaultView;
                        
            return dataTable;
        }

        private void RESULTS_Click(object sender, RoutedEventArgs e)
        {

        }

        private void overviewbutton_Click(object sender, RoutedEventArgs e)
        {
            overview.IsSelected = true;
            tasking.IsSelected = false;
            messages.IsSelected = false;
            results.IsSelected = false;
            e.Handled = true;
        }

        private void resultsbutton_Click(object sender, RoutedEventArgs e)
        {
            overview.IsSelected = false;
            tasking.IsSelected = false;
            messages.IsSelected = false;
            results.IsSelected = true;
            e.Handled = true;
        }

        private void messagesbutton_Click(object sender, RoutedEventArgs e)
        {
            overview.IsSelected = false;
            tasking.IsSelected = false;
            messages.IsSelected = true;
            results.IsSelected = false;
            e.Handled = true;
        }

        private void taskingbutton_Click(object sender, RoutedEventArgs e)
        {
            overview.IsSelected = false;
            tasking.IsSelected = true;
            messages.IsSelected = false;
            results.IsSelected = false;
            e.Handled = true;
        }

        #region Scroll Up/Down Hover Buttons

            #region Intubation
        public static readonly DependencyProperty IntubationPageUpEnabledProperty = DependencyProperty.Register(
            "IntubationPageUpEnabled", typeof(bool), typeof(MainPage3), new PropertyMetadata(false));

        public static readonly DependencyProperty IntubationPageDownEnabledProperty = DependencyProperty.Register(
            "IntubationPageDownEnabled", typeof(bool), typeof(MainPage3), new PropertyMetadata(false));

        private const double ScrollErrorMargin = 0.001;

        private const int PixelScrollByAmount = 20;

        /// <summary>
        /// CLR Property Wrappers for PageUpEnabledProperty
        /// </summary>
        public bool IntubationPageUpEnabled
        {
            get
            {
                return (bool)GetValue(IntubationPageUpEnabledProperty);
            }

            set
            {
                this.SetValue(IntubationPageUpEnabledProperty, value);
            }
        }

        /// <summary>
        /// CLR Property Wrappers for PageDownEnabledProperty
        /// </summary>
        public bool IntubationPageDownEnabled
        {
            get
            {
                return (bool)GetValue(IntubationPageDownEnabledProperty);
            }

            set
            {
                this.SetValue(IntubationPageDownEnabledProperty, value);
            }
        }

        /// <summary>
        /// Change button state depending on scroll viewer position
        /// </summary>
        private void UpdateIntubationPagingButtonState()
        {
            this.IntubationPageUpEnabled = IntubationScrollViewer.VerticalOffset > ScrollErrorMargin;

            this.IntubationPageDownEnabled = IntubationScrollViewer.VerticalOffset < IntubationScrollViewer.ScrollableHeight - ScrollErrorMargin;
        }

        private void IntubationPageUpButtonClick(object sender, RoutedEventArgs e)
        {
            IntubationScrollViewer.ScrollToVerticalOffset(IntubationScrollViewer.VerticalOffset - PixelScrollByAmount);
        }

        private void IntubationPageDownButtonClick(object sender, RoutedEventArgs e)
        {
            IntubationScrollViewer.ScrollToVerticalOffset(IntubationScrollViewer.VerticalOffset + PixelScrollByAmount);
        }
        #endregion

            #region CPR
            public static readonly DependencyProperty CPRPageUpEnabledProperty = DependencyProperty.Register(
                "CPRPageUpEnabled", typeof(bool), typeof(MainPage3), new PropertyMetadata(false));

            public static readonly DependencyProperty CPRPageDownEnabledProperty = DependencyProperty.Register(
                "CPRPageDownEnabled", typeof(bool), typeof(MainPage3), new PropertyMetadata(false));

            /// <summary>
            /// CLR Property Wrappers for PageUpEnabledProperty
            /// </summary>
            public bool CPRPageUpEnabled
            {
                get
                {
                    return (bool)GetValue(CPRPageUpEnabledProperty);
                }

                set
                {
                    this.SetValue(CPRPageUpEnabledProperty, value);
                }
            }

            /// <summary>
            /// CLR Property Wrappers for PageDownEnabledProperty
            /// </summary>
            public bool CPRPageDownEnabled
            {
                get
                {
                    return (bool)GetValue(CPRPageDownEnabledProperty);
                }

                set
                {
                    this.SetValue(CPRPageDownEnabledProperty, value);
                }
            }

            /// <summary>
            /// Change button state depending on scroll viewer position
            /// </summary>
            private void UpdateCPRPagingButtonState()
            {
                this.CPRPageUpEnabled = CPRScrollViewer.VerticalOffset > ScrollErrorMargin;

                this.CPRPageDownEnabled = CPRScrollViewer.VerticalOffset < CPRScrollViewer.ScrollableHeight - ScrollErrorMargin;
            }

            private void CPRPageUpButtonClick(object sender, RoutedEventArgs e)
            {
                CPRScrollViewer.ScrollToVerticalOffset(CPRScrollViewer.VerticalOffset - PixelScrollByAmount);
            }

            private void CPRPageDownButtonClick(object sender, RoutedEventArgs e)
            {
                CPRScrollViewer.ScrollToVerticalOffset(CPRScrollViewer.VerticalOffset + PixelScrollByAmount);
            }
            #endregion

            #region LateralTransfer
            public static readonly DependencyProperty LateralTransferPageUpEnabledProperty = DependencyProperty.Register(
                "LateralTransferPageUpEnabled", typeof(bool), typeof(MainPage3), new PropertyMetadata(false));

            public static readonly DependencyProperty LateralTransferPageDownEnabledProperty = DependencyProperty.Register(
                "LateralTransferPageDownEnabled", typeof(bool), typeof(MainPage3), new PropertyMetadata(false));

            /// <summary>
            /// CLR Property Wrappers for PageUpEnabledProperty
            /// </summary>
            public bool LateralTransferPageUpEnabled
            {
                get
                {
                    return (bool)GetValue(LateralTransferPageUpEnabledProperty);
                }

                set
                {
                    this.SetValue(LateralTransferPageUpEnabledProperty, value);
                }
            }

            /// <summary>
            /// CLR Property Wrappers for PageDownEnabledProperty
            /// </summary>
            public bool LateralTransferPageDownEnabled
            {
                get
                {
                    return (bool)GetValue(LateralTransferPageDownEnabledProperty);
                }

                set
                {
                    this.SetValue(LateralTransferPageDownEnabledProperty, value);
                }
            }

            /// <summary>
            /// Change button state depending on scroll viewer position
            /// </summary>
            private void UpdateLateralTransferPagingButtonState()
            {
                this.LateralTransferPageUpEnabled = LateralTransferScrollViewer.VerticalOffset > ScrollErrorMargin;

                this.LateralTransferPageDownEnabled = LateralTransferScrollViewer.VerticalOffset < LateralTransferScrollViewer.ScrollableHeight - ScrollErrorMargin;
            }

            private void LateralTransferPageUpButtonClick(object sender, RoutedEventArgs e)
            {
                LateralTransferScrollViewer.ScrollToVerticalOffset(LateralTransferScrollViewer.VerticalOffset - PixelScrollByAmount);
            }

            private void LateralTransferPageDownButtonClick(object sender, RoutedEventArgs e)
            {
                LateralTransferScrollViewer.ScrollToVerticalOffset(LateralTransferScrollViewer.VerticalOffset + PixelScrollByAmount);
            }

            #endregion

            #region VerticalLift
            public static readonly DependencyProperty VerticalLiftPageUpEnabledProperty = DependencyProperty.Register(
                "VerticalLiftPageUpEnabled", typeof(bool), typeof(MainPage3), new PropertyMetadata(false));

            public static readonly DependencyProperty VerticalLiftPageDownEnabledProperty = DependencyProperty.Register(
                "VerticalLiftPageDownEnabled", typeof(bool), typeof(MainPage3), new PropertyMetadata(false));

            /// <summary>
            /// CLR Property Wrappers for PageUpEnabledProperty
            /// </summary>
            public bool VerticalLiftPageUpEnabled
            {
                get
                {
                    return (bool)GetValue(VerticalLiftPageUpEnabledProperty);
                }

                set
                {
                    this.SetValue(VerticalLiftPageUpEnabledProperty, value);
                }
            }

            /// <summary>
            /// CLR Property Wrappers for PageDownEnabledProperty
            /// </summary>
            public bool VerticalLiftPageDownEnabled
            {
                get
                {
                    return (bool)GetValue(VerticalLiftPageDownEnabledProperty);
                }

                set
                {
                    this.SetValue(VerticalLiftPageDownEnabledProperty, value);
                }
            }

            /// <summary>
            /// Change button state depending on scroll viewer position
            /// </summary>
            private void UpdateVerticalLiftPagingButtonState()
            {
                this.VerticalLiftPageUpEnabled = VerticalLiftScrollViewer.VerticalOffset > ScrollErrorMargin;

                this.VerticalLiftPageDownEnabled = VerticalLiftScrollViewer.VerticalOffset < VerticalLiftScrollViewer.ScrollableHeight - ScrollErrorMargin;
            }

            private void VerticalLiftPageUpButtonClick(object sender, RoutedEventArgs e)
            {
                VerticalLiftScrollViewer.ScrollToVerticalOffset(VerticalLiftScrollViewer.VerticalOffset - PixelScrollByAmount);
            }

            private void VerticalLiftPageDownButtonClick(object sender, RoutedEventArgs e)
            {
                VerticalLiftScrollViewer.ScrollToVerticalOffset(VerticalLiftScrollViewer.VerticalOffset + PixelScrollByAmount);
            }
            #endregion

        #endregion

        /*
		private void OverviewChart_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AreaSeries areaseries = sender as AreaSeries;

            if (e == null || sender == null || areaseries == null || e.AddedItems == null)
                return;

            //MessageBox.Show(e.AddedItems[0].ToString(), e.AddedItems[0].GetType().ToString());

            if (areaseries != null)
            {
                if (e.AddedItems.Count > 0 && e.AddedItems[0] is KeyValuePair<int, double>)
                {
                    //deserializeScreenShotFromDatabase((KeyValuePair<int, double>)(e.AddedItems[0]));
                    displayIndex = ((KeyValuePair<int, double>)(e.AddedItems[0])).Key - 1;
                    
                    try
                    {
                        ResultsDataGrid.SelectedItem = (ResultsDataGrid.Items == null ? null : ResultsDataGrid.Items[displayIndex]);
                        ResultsDataGrid.UpdateLayout();
                        ResultsDataGrid.ScrollIntoView(ResultsDataGrid.SelectedItem);
                    }
                    catch
                    {
                        ////MessageBox.Show(String.Format("EXCEPTION: Could not jump to row index {0}.", displayIndex));
                    }

                    deserializeStageScoreFromDatabase();

                    e.Handled = true;
                }
            }
        } */
	
	
		
	//---------------------------------------------------------------------------------------------------------------------
	//RESULTS TAB
		
		
		
    }
}