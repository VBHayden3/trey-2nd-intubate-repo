﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;

namespace AIMS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            /* Note:
             * We'll listen to the Esc key, and navigate to FrontPage.xaml. This will allow CprPage or any other page to be able to get back to FrontPage by pressing Esc.
             */
            try
            {
                if (e.Handled)
                    return;

                switch (e.Key)
                {
                    case System.Windows.Input.Key.Escape:
                        {
                            if (this.PageFrame != null)
                            {
                                // Navigate to the FrontPage (if we're not already there).
                                if (!(this.PageFrame.Content is LoginPage))
                                {
                                    this.PageFrame.Navigate(new Uri("/AIMS;component/LoginPage.xaml", UriKind.RelativeOrAbsolute));
                                }
                            }
                            break;
                        }
                    case System.Windows.Input.Key.F1:
                        {
                            if (this.PageFrame != null)
                            {
                                if (this.PageFrame != null)
                                {
                                    // Navigate to the DevTools page (if we're not already there).
                                    if (!(this.PageFrame.Content is DevTools))
                                    {
                                        this.PageFrame.Navigate(new Uri("/AIMS;component/DevTools.xaml", UriKind.RelativeOrAbsolute));
                                    }
                                }
                            }
                            break;
                        }
                }
            }
            catch (Exception exc) { Debug.Print("MainWindow Window_KeyUp() ERROR: {0}", exc.Message); }
        }
    }
}
