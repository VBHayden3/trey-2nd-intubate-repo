﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIMS.AIMS_Math
{
    class Vector3f
    {
        public float X;
        public float Y;
        public float Z;

        public Vector3f(Vector3f vector)
        {
            this.X = vector.X;
            this.Y = vector.Y;
            this.Z = vector.Z;
        }

        public Vector3f(float X, float Y, float Z)
        {
            this.X = X;
            this.Y = Y;
            this.Z = Z;
        }

        public Vector3f() : this(0.0f, 0.0f, 0.0f) { }

        public void normalize()
        {
            float magnitude = Magnitude(this);

            X = X / magnitude;
            Y = Y / magnitude;
            Z = Z / magnitude;
        }

        #region static functions

        public static float Magnitude(Vector3f vector)
        {
            float magnitude = vector.X * vector.X + vector.Y * vector.Y + vector.Z * vector.Z;

            return (float)System.Math.Sqrt(magnitude);
        }

        /// <summary>
        /// Finds the dot product between two vectors
        /// </summary>
        /// <param name="v1">First vector</param>
        /// <param name="v2">Second vector</param>
        /// <returns></returns>
        public static float Dot(Vector3f v1, Vector3f v2)
        {
            return (v1.X * v2.X) + (v1.Y * v2.Y) + (v1.Z * v2.Z);
        }
        /// <summary>
        /// Finds the angle in degrees between two non-normalized vectors
        /// </summary>
        /// <param name="v1">First vector</param>
        /// <param name="v2">Second vector</param>
        /// <returns></returns>
        public static float Angle(Vector3f v1, Vector3f v2)
        {
            double temp = Dot(v1, v2);
            temp = temp / (Magnitude(v1) * Magnitude(v2));

            temp = System.Math.Acos(temp);

            temp = temp * 180 / System.Math.PI;

            return (float)temp;
        }
        #endregion
    }
}
