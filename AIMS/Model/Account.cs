﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using AIMS;
using Npgsql;
using System.Windows;


namespace AIMS
{
    /// <summary>
    /// Class from which all people derive from. Stores important user-bound information such as account name, password, assignments, grades, etc.
    /// </summary>
    [Serializable()]
    public class Account
    {
        public enum ConnectionState
        {
            Connected,
            Disconnected
        }

        private int id = -1;
        private string username;
        private string password;
        private DateTime creationDate;
        private bool changePasswordNextLogin;
        private AccessLevel accessLevel = AccessLevel.User;
        private string email;
        private string securityQuestion;
        private string securityAnswer;

        public int ID { get { return id; } set { id = value; } }
        public string Username { get { return username; } set { username = value; } }
        public string Password { get { return password; } set { password = value; } }
        public DateTime CreationDate { get { return creationDate; } set { creationDate = value; } }
        public bool ChangePasswordNextLogin { get { return changePasswordNextLogin; } set { changePasswordNextLogin = value; } }
        public AccessLevel AccessLevel { get { return accessLevel; } set { accessLevel = value; } }
        public string Email { get { return email; } set { email = value; } }
        public string SecurityQuestion { get { return securityQuestion; } set { securityQuestion = value; } }
        public string SecurityAnswer { get { return securityAnswer; } set { securityAnswer = value; } }

        public Account()
        {
        }

        public Account(string username, string password, AccessLevel accesslevel, string email, string securityQuestion, string securityAnswer) 
            : this( -1, username, password, DateTime.Now, true, accesslevel, email, securityQuestion, securityAnswer )
        {
        }

        public Account(int id, string username, string password, DateTime creationdate, bool changepasswordnextlogin, AccessLevel accesslevel, string email,
            string securityQuestion, string securityAnswer)
        {
            this.ID = id;
            this.Username = username;
            this.Password = password;
            this.CreationDate = creationdate;
            this.ChangePasswordNextLogin = changepasswordnextlogin;
            this.AccessLevel = accesslevel;
            this.Email = email;
            this.SecurityQuestion = securityQuestion;
            this.SecurityAnswer = securityAnswer;
        }

        public override string ToString()
        {
            return String.Format( "ID#: {0} Username: \"{1}\", AccessLevel: {2}", this.ID, this.Username, this.AccessLevel.ToString() );
        }

        public static Account GetAccountFromAccountID(int accountID, bool closeConnection)
        {
            if (accountID <= 0)
                return null;

            Account account = null;

            if (DatabaseManager.Connection.State != System.Data.ConnectionState.Open)
            {
                DatabaseManager.OpenConnection();
            }

            string query = String.Format("SELECT uid, username, password, creationdate, changepasswordnextlogin, accesslevel, email, securityquestion, securityanswer FROM accounts WHERE uid='{0}'", accountID);

            NpgsqlCommand command = new NpgsqlCommand(query, DatabaseManager.Connection);

            NpgsqlDataReader reader = command.ExecuteReader();

            int id = -1;
            string username = null;
            string password = null;
            DateTime creationdate = DateTime.MinValue;
            bool changepasswordnextlogin = false;
            int accesslevel = 0;
            string email = null;
            string securityquestion = null;
            string securityanswer = null;
            
            while (reader.Read())
            {
                id = reader.GetInt32(reader.GetOrdinal("uid"));
                username = reader.GetString(reader.GetOrdinal("username"));
                password = reader.GetString(reader.GetOrdinal("password"));
                creationdate = reader.GetTimeStamp(reader.GetOrdinal("creationdate"));
                changepasswordnextlogin = reader.GetBoolean(reader.GetOrdinal("changepasswordnextlogin"));
                accesslevel = (int)reader.GetInt16(reader.GetOrdinal("accesslevel"));
                try
                {
                    email = reader.GetString(reader.GetOrdinal("email"));
                }
                catch (Exception)
                {
                    
                }
                try
                {
                    securityquestion = reader.GetString(reader.GetOrdinal("securityquestion"));
                }
                catch (Exception)
                {

                }
                try
                {
                    securityanswer = reader.GetString(reader.GetOrdinal("securityanswer"));
                }
                catch (Exception)
                {

                } 
            }
            reader.Dispose();
            account = new Account(id, username, password, creationdate, changepasswordnextlogin, (AccessLevel)accesslevel, email, securityquestion, securityanswer);

            if (closeConnection)
                DatabaseManager.CloseConnection();
            return account;
        }

        /// <summary>
        /// Creates and Account, stores it in the Database, and returns the Account object.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public static Account CreateAccount( string username, string password, AccessLevel accesslevel, DateTime creationdate, DateTime expirationdate, bool changepasswordnextlogin, string email, 
            string securityquestion, string securityanswer, Organization organization )
        {
            Account acct = null;

            if (String.IsNullOrWhiteSpace(username))
            {
                return null;
            }

            DatabaseManager.OpenConnection();
            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = @"INSERT INTO accounts ( Username, Password, AccessLevel, CreationDate, ExpirationDate, ChangePasswordNextLogin, OrganizationID, Email, SecurityQuestion, SecurityAnswer) VALUES ( @Username, @Password, @AccessLevel, @CreationDate, @ExpirationDate::date, @ChangePasswordNextLogin, @OrganizationID, @Email, @SecurityQuestion, @SecurityAnswer); SELECT uid FROM accounts WHERE username=:username;";

            command.Parameters.Add("@Username", NpgsqlTypes.NpgsqlDbType.Text).Value = username;
            command.Parameters.Add("@Password", NpgsqlTypes.NpgsqlDbType.Text).Value = password;
            command.Parameters.Add("@AccessLevel", NpgsqlTypes.NpgsqlDbType.Smallint).Value = (int)accesslevel;
            command.Parameters.Add("@CreationDate", NpgsqlTypes.NpgsqlDbType.Timestamp).Value = creationdate;
            command.Parameters.Add("@ExpirationDate", NpgsqlTypes.NpgsqlDbType.Date).Value = expirationdate;
            command.Parameters.Add("@ChangePasswordNextLogin", NpgsqlTypes.NpgsqlDbType.Boolean).Value = changepasswordnextlogin;
            command.Parameters.Add("@OrganizationID", NpgsqlTypes.NpgsqlDbType.Integer).Value = organization.ID;
            command.Parameters.Add("@Email", NpgsqlTypes.NpgsqlDbType.Text).Value = ( String.IsNullOrWhiteSpace( email ) ? "" : email );
            command.Parameters.Add("@SecurityQuestion", NpgsqlTypes.NpgsqlDbType.Text).Value = (String.IsNullOrWhiteSpace(securityquestion) ? "" : securityquestion);
            command.Parameters.Add("@SecurityAnswer", NpgsqlTypes.NpgsqlDbType.Text).Value = (String.IsNullOrWhiteSpace(securityanswer) ? "" : securityanswer);
            command.Parameters.Add("username", NpgsqlTypes.NpgsqlDbType.Text).Value = username;


            int id = -1;

            try
            {
               id = (int)command.ExecuteScalar();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
            }

            DatabaseManager.CloseConnection();

            acct = Account.GetAccountFromAccountID(id, true);

            return acct;
        }

        /// <summary>
        /// Update Account, updates it in the Database, and returns the Account object.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public static void UpdateAccount(string email, string password, string securityquestion, string securityanswer)
        {
            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            // The command updates the password, and also updates the flag in the DB so they don't need to update their password again.
            command.CommandText = "UPDATE accounts SET email = :email, password = :password, securityquestion = :securityquestion, securityanswer = :securityanswer WHERE uid = :uid;";

            command.Parameters.Add("email", NpgsqlTypes.NpgsqlDbType.Text).Value = email;
            command.Parameters.Add("password", NpgsqlTypes.NpgsqlDbType.Text).Value = password;
            command.Parameters.Add("securityquestion", NpgsqlTypes.NpgsqlDbType.Text).Value = securityquestion;
            command.Parameters.Add("securityanswer", NpgsqlTypes.NpgsqlDbType.Text).Value = securityanswer;
            command.Parameters.Add("uid", NpgsqlTypes.NpgsqlDbType.Smallint).Value = ((App)App.Current).CurrentStudent.Account.ID;

            int enq = command.ExecuteNonQuery();

            DatabaseManager.CloseConnection();

            // Update the current account object with the new details.
            ((App)App.Current).CurrentStudent.Account.Email = email;
            ((App)App.Current).CurrentStudent.Account.Password = password; ;
            ((App)App.Current).CurrentStudent.Account.SecurityQuestion = securityquestion;
            ((App)App.Current).CurrentStudent.Account.SecurityAnswer = securityanswer;
        }

        private static string CreateTempPassword(string username)
        {
            string password;

            password = username;    // TEMPORARY! The password is defaulted to your username, atm. Will soon use email service, w/ randomly generated password.

            return password;
        }
    }
}
