﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using AIMS.Model;

namespace AIMS
{
    [Serializable()]
    public class Administrator
    {
        private int id;
        private Account account;
        private string firstName;
        private string lastName;
        private Organization organization;
        private AdministratorSettings userSettings;

        public int ID { get { return id; } set { id = value; } }
        public Account Account { get { return account; } set { account = value; } }
        public string FirstName { get { return firstName; } set { firstName = value; } }
        public string LastName { get { return lastName; } set { lastName = value; } }
        public Organization Organization { get { return organization; } set { organization = value; } }

        public AdministratorSettings UserSettings
        {
            get
            {
                if (userSettings == null)
                    userSettings = new AdministratorSettings(this);
                return userSettings;
            }
        }

        public Administrator()
        {
            id = -1;
            account = null;
            firstName = null;
            lastName = null;
            organization = null;
        }

        public Administrator(int id, Account account, string firstname, string lastname, Organization organization)
        {
            this.ID = id;
            this.Account = account;
            this.FirstName = firstname;
            this.LastName = lastname;
            this.Organization = organization;
        }

        public static Administrator GetAdministratorFromAdministratorID(int administratorID, bool closeConnection)
        {
            if (administratorID <= 0)
                return null;

            Administrator administrator = null;

            if (DatabaseManager.Connection.State != System.Data.ConnectionState.Open)
            {
                DatabaseManager.OpenConnection();
            }

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;

            command.CommandText = "SELECT uid, accountid, firstname, lastname, organizationid FROM administrators WHERE uid = :administratorid;";
            command.Parameters.Add("administratorid", NpgsqlTypes.NpgsqlDbType.Integer).Value = administratorID;

            NpgsqlDataReader reader = command.ExecuteReader();

            // Temporary feilds
            int id = -1;
            int accountid = -1;
            string firstname = null;
            string lastname = null;
            int organizationid = -1;

            while (reader.Read())
            {
                id = reader.GetInt32(reader.GetOrdinal("uid"));
                accountid = reader.GetInt32(reader.GetOrdinal("accountid"));
                firstname = reader.GetString(reader.GetOrdinal("firstname"));
                lastname = reader.GetString(reader.GetOrdinal("lastname"));
                organizationid = reader.GetInt32(reader.GetOrdinal("organizationid"));
            }

            reader.Dispose();

            administrator = new Administrator(id, Account.GetAccountFromAccountID(accountid, false), firstname, lastname, Organization.GetOrganizationFromOrganizationID(organizationid, false));

            if (closeConnection)
                DatabaseManager.CloseConnection();

            return administrator;
        }

        public static Administrator GetAdministratorFromAccountID(int accountID, bool closeConnection)
        {
            if (accountID <= 0)
                return null;

            Administrator administrator = null;

            if (DatabaseManager.Connection.State != System.Data.ConnectionState.Open)
            {
                DatabaseManager.OpenConnection();
            }

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "SELECT uid, accountid, firstname, lastname, organizationid FROM administrators WHERE accountid = :accountid;";
            command.Parameters.Add("accountid", NpgsqlTypes.NpgsqlDbType.Integer).Value = accountID;

            NpgsqlDataReader reader = command.ExecuteReader();

            // Temporary feilds
            int id = -1;
            int accountid = -1;
            string firstname = null;
            string lastname = null;
            int organizationid = -1;

            while (reader.Read())
            {
                id = reader.GetInt32(reader.GetOrdinal("uid"));
                accountid = reader.GetInt32(reader.GetOrdinal("accountid"));
                firstname = reader.GetString(reader.GetOrdinal("firstname"));
                lastname = reader.GetString(reader.GetOrdinal("lastname"));
                organizationid = reader.GetInt32(reader.GetOrdinal("organizationid"));
            }

            reader.Dispose();

            administrator = new Administrator(id, Account.GetAccountFromAccountID(accountid, false), firstname, lastname, Organization.GetOrganizationFromOrganizationID(organizationid, false));

            if (closeConnection)
                DatabaseManager.CloseConnection();

            return administrator;
        }
    }
}
