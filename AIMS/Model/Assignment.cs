﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;

namespace AIMS
{
    public enum AssignmentType
    {
        None = 0,
        Practice = 1,
        Assessment = 2,
    }

    public class Assignment
    {
        private int assignmentID = -1;
        private int courseID = -1;
        private string name = null;
        private string description = null;
        private DateTime startDate = DateTime.MinValue;
        private DateTime endDate = DateTime.MaxValue;
        private AssignmentType assignmentType = AssignmentType.None;
        private Task task = Task.None;

        public int AssignmentID { get { return assignmentID; } set { assignmentID = value; } }
        public int CourseID { get { return courseID; } set { courseID = value; } }
        public string Name { get { return name; } set { name = value; } }
        public string Description { get { return description; } set { description = value; } }
        public DateTime StartDate { get { return startDate; } set { startDate = value; } }
        public DateTime EndDate { get { return endDate; } set { endDate = value; } }
        public Task Task { get { return task; } set { task = value; } }
        public AssignmentType AssignmentType { get { return assignmentType; } set { assignmentType = value; } }

        public Assignment()
        {
        }

        public Assignment( int id, int courseid, DateTime startdate, DateTime enddate, string name, string description, AssignmentType assignmenttype, Task task)
        {
            this.assignmentID = id;
            this.courseID = courseid;
            this.name = name;
            this.description = description;
            this.startDate = startdate;
            this.endDate = enddate;
            this.assignmentType = assignmenttype;
            this.task = task;
        }

        /// <summary>
        /// Recreates a course object using data stored in the database.
        /// </summary>
        /// <param name="courseID"></param>
        /// <returns></returns>
        public static Assignment GetAssignmentFromAssignmentID(int assignmentID)
        {
            if (assignmentID <= 0)
                return null;

            Assignment assignment = null;

            DatabaseManager.OpenConnection();

            string query = String.Format("SELECT uid, courseid, startdate, duedate, name, description, taskmode, tasktype FROM course_assignments WHERE uid='{0}'", assignmentID);

            NpgsqlCommand command = new NpgsqlCommand(query, DatabaseManager.Connection);

            NpgsqlDataReader reader = command.ExecuteReader();

            // Temporary feilds
            int id = -1;
            int courseid = -1;
            DateTime startdate = DateTime.MinValue;
            DateTime enddate = DateTime.MinValue;
            string name = null;
            string description = null;
            int taskmodeint = 0;
            int taskint = 0;

            while (reader.Read())
            {
                id = reader.GetInt32(reader.GetOrdinal("uid"));
                courseid = reader.GetInt32(reader.GetOrdinal("courseid"));
                startdate = reader.GetTimeStamp(reader.GetOrdinal("startdate"));
                enddate = reader.GetTimeStamp(reader.GetOrdinal("duedate"));
                name = reader.GetString(reader.GetOrdinal("name"));
                description = reader.GetString(reader.GetOrdinal("description"));
                taskmodeint = reader.GetInt32(reader.GetOrdinal("taskmode"));
                taskint = reader.GetInt32(reader.GetOrdinal("tasktype"));
            }

            reader.Dispose();

            assignment = new Assignment(id, courseid, startdate, enddate, name, description, (AssignmentType)taskmodeint, (Task)taskint);

            DatabaseManager.CloseConnection();

            return assignment;
        }

        public static List<Assignment> GetAssignmentsForCourseID( int courseID )
        {
            if (courseID <= 0)
                return null;

            List<Assignment> assignments = new List<Assignment>();

            DatabaseManager.OpenConnection();
            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "SELECT uid, name, description, taskmode, tasktype, startdate, duedate FROM course_assignments WHERE courseid=:courseid;";
            command.Parameters.Add("courseid", NpgsqlTypes.NpgsqlDbType.Integer).Value = courseID;

            NpgsqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                assignments.Add(new Assignment(reader.GetInt32(reader.GetOrdinal("uid")), courseID, reader.GetDateTime(reader.GetOrdinal("startdate")), reader.GetDateTime(reader.GetOrdinal("duedate")), reader.GetString(reader.GetOrdinal("name")), reader.GetString(reader.GetOrdinal("description")), (AssignmentType)reader.GetInt32(reader.GetOrdinal("taskmode")), (Task)reader.GetInt32(reader.GetOrdinal("tasktype"))));
            }

            reader.Dispose();

            DatabaseManager.CloseConnection();

            return assignments;
        }
    }
}
