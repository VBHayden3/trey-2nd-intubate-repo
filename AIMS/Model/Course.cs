﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using System.Windows;

namespace AIMS
{
    public class Course
    {
        private int id = -1;
        private Doctor doctor;
        private Roster roster;
        private DateTime startDate;
        private DateTime endDate;
        private string name;
        private string description;
        private bool hasIntubation = false;
        private bool hasVerticalLift = false;
        private bool hasCPR = false;

        public int ID { get { return id; } set { id = value; } }
        public Doctor Doctor { get { return doctor; } set { doctor = value; } }
        public Roster Roster { get { return roster; } set { roster = value; } }
        public DateTime StartDate { get { return startDate; } set { startDate = value; } }
        public DateTime EndDate { get { return endDate; } set { endDate = value; } }
        public string Name { get { return name; } set { name = value; } }
        public string Description { get { return description; } set { description = value; } }
        public bool HasIntubation { get { return hasIntubation; } set { hasIntubation = value; } }
        public bool HasCPR { get { return hasCPR; } set { hasCPR = value; } }
        public bool HasVerticalLift { get { return hasVerticalLift; } set { hasVerticalLift = value; } }

        public Course()
        {
        }

        public Course(int id, Doctor doctor, DateTime startdate, DateTime enddate, string name, string description, bool hasintubation, bool hasverticallift, bool hascpr) : this( id, doctor, null, startdate, enddate, name, description, hasintubation, hasverticallift, hascpr )
        {
        }

        public Course(int id, Doctor doctor, Roster roster, DateTime startdate, DateTime enddate, string name, string description, bool hasintubation, bool hasverticallift, bool hascpr)
        {
            this.ID = id;
            this.Doctor = doctor;
            this.Roster = roster;
            this.StartDate = startdate;
            this.EndDate = enddate;
            this.Name = name;
            this.Description = description;
            this.hasIntubation = hasintubation;
            this.hasVerticalLift = hasverticallift;
            this.hasCPR = hascpr;
        }

        /// <summary>
        /// Recreates a course object using data stored in the database.
        /// </summary>
        /// <param name="courseID"></param>
        /// <returns></returns>
        public static Course GetCourseFromCourseID(int courseID)
        {
            if (courseID <= 0)
                return null;

            Course course = null;

            DatabaseManager.OpenConnection();
        
            string query = String.Format("SELECT uid, doctorid, startdate, enddate, name, description, hasintubation, hasverticallift, hascpr FROM courses WHERE uid='{0}'", courseID);

            NpgsqlCommand command = new NpgsqlCommand(query, DatabaseManager.Connection);

            NpgsqlDataReader reader = command.ExecuteReader();

            // Temporary feilds
            int id = -1;
            int doctorid = -1;
            DateTime startdate = DateTime.MinValue;
            DateTime enddate = DateTime.MinValue;
            string name = null;
            string description = null;
            bool hasintubation = false;
            bool hasverticallift = false;
            bool hascpr = false;

            while (reader.Read())
            {
                id = reader.GetInt32(reader.GetOrdinal("uid"));
                doctorid = reader.GetInt32(reader.GetOrdinal("doctorid"));
                startdate = reader.GetTimeStamp(reader.GetOrdinal("startdate"));
                enddate = reader.GetTimeStamp(reader.GetOrdinal("enddate"));
                name = reader.GetString(reader.GetOrdinal("name"));
                description = reader.GetString(reader.GetOrdinal("description"));
                hasintubation = reader.GetBoolean(reader.GetOrdinal("hasintubation"));
                hasverticallift = reader.GetBoolean(reader.GetOrdinal("hasverticallift"));
                hascpr = reader.GetBoolean(reader.GetOrdinal("hascpr"));
            }

            reader.Dispose();

            course = new Course(id, Doctor.GetDoctorFromDoctorID(doctorid, false), Roster.GetRosterFromCourseID(id), startdate, enddate, name, description, hasintubation, hasverticallift, hascpr);

            DatabaseManager.CloseConnection();

            return course;
        }

        /// <summary>
        /// Creates a new course in the database.
        /// </summary>
        /// <param name="doctorid"></param>
        /// <param name="startdate"></param>
        /// <param name="enddate"></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="hasintubation"></param>
        /// <param name="hasverticallift"></param>
        /// <param name="hascpr"></param>
        /// <returns></returns>
        public static Course CreateCourseNoRoster( int doctorid, DateTime startdate, DateTime enddate, string name, string description, bool hasintubation, bool hasverticallift, bool hascpr)
        {
            if (doctorid <= 0)
            {
                return null;
            }

            Course course = null;

            DatabaseManager.CloseConnection();  // must close connection.

            if (DatabaseManager.Connection.State != System.Data.ConnectionState.Open)
            {
                DatabaseManager.OpenConnection();
            }

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "INSERT INTO courses (doctorid, startdate, enddate, name, description, hasintubation, hasverticallift, hascpr) VALUES ( :doctorid, :startdate, :enddate, :name, :description, :hasintubation, :hasverticallift, :hascpr ); SELECT uid FROM courses WHERE doctorid = :doctorid AND startdate = :startdate AND enddate = :enddate AND name = :name AND description = :description AND hasintubation = :hasintubation AND hasverticallift = :hasverticallift AND hascpr = :hascpr";
            command.Parameters.Add("doctorid", NpgsqlTypes.NpgsqlDbType.Integer).Value = doctorid;
            command.Parameters.Add("startdate", NpgsqlTypes.NpgsqlDbType.Timestamp).Value = startdate;
            command.Parameters.Add("enddate", NpgsqlTypes.NpgsqlDbType.Timestamp).Value = enddate;
            command.Parameters.Add("name", NpgsqlTypes.NpgsqlDbType.Text).Value = name;
            command.Parameters.Add("description", NpgsqlTypes.NpgsqlDbType.Text).Value = description;
            command.Parameters.Add("hasintubation", NpgsqlTypes.NpgsqlDbType.Boolean).Value = hasintubation;
            command.Parameters.Add("hasverticallift", NpgsqlTypes.NpgsqlDbType.Boolean).Value = hasverticallift;
            command.Parameters.Add("hascpr", NpgsqlTypes.NpgsqlDbType.Boolean).Value = hascpr;

            int courseid = -1;

            try
            {
                courseid = (int)command.ExecuteScalar();
            }
            catch( Exception e )
            {
                MessageBox.Show(e.ToString());
            }

            DatabaseManager.CloseConnection();

            if( courseid > 0 )
                course = Course.GetCourseFromCourseID(courseid);

            return course;
        }
    }
}