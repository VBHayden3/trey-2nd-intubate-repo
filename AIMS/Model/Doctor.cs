﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using AIMS.Model;

namespace AIMS
{
    [Serializable()]
    public class Doctor
    {
        private int id;
        private Account account;
        private string firstName;
        private string lastName;
        private Organization organization;
        private DoctorSettings userSettings;

        public int ID { get { return id; } set { id = value; } }
        public Account Account { get { return account; } set { account = value; } }
        public string FirstName { get { return firstName; } set { firstName = value; } }
        public string LastName { get { return lastName; } set { lastName = value; } }
        public Organization Organization { get { return organization; } set { organization = value; } }

        public DoctorSettings UserSettings
        {
            get
            {
                if (userSettings == null)
                    userSettings = new DoctorSettings(this);
                return userSettings;
            }
        }

        public Doctor()
        {
            id = -1;
            account = null;
            firstName = null;
            lastName = null;
            organization = null;
        }

        public Doctor(int id, Account account, string firstname, string lastname, Organization organization)
        {
            this.ID = id;
            this.Account = account;
            this.FirstName = firstname;
            this.LastName = lastname;
            this.Organization = organization;
        }

        public static Doctor GetDoctorFromDoctorID(int doctorID, bool closeConnection)
        {
            if (doctorID <= 0)
                return null;

            Doctor doctor = null;

            if (DatabaseManager.Connection.State != System.Data.ConnectionState.Open)
            {
                DatabaseManager.OpenConnection();
            }

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;

            command.CommandText = "SELECT uid, accountid, firstname, lastname, organizationid FROM doctors WHERE uid = :doctorid;";
            command.Parameters.Add("doctorid", NpgsqlTypes.NpgsqlDbType.Integer).Value = doctorID;

            NpgsqlDataReader reader = command.ExecuteReader();

            // Temporary feilds
            int id = -1;
            int accountid = -1;
            string firstname = null;
            string lastname = null;
            int organizationid = -1;

            while (reader.Read())
            {
                id = reader.GetInt32(reader.GetOrdinal("uid"));
                accountid = reader.GetInt32(reader.GetOrdinal("accountid"));
                firstname = reader.GetString(reader.GetOrdinal("firstname"));
                lastname = reader.GetString(reader.GetOrdinal("lastname"));
                organizationid = reader.GetInt32(reader.GetOrdinal("organizationid"));
            }

            reader.Dispose();

            doctor = new Doctor(id, Account.GetAccountFromAccountID(accountid, false), firstname, lastname, Organization.GetOrganizationFromOrganizationID(organizationid, false));

            if (closeConnection)
                DatabaseManager.CloseConnection();

            return doctor;
        }

        public static Doctor GetDoctorFromAccountID(int accountID, bool closeConnection)
        {
            if (accountID <= 0)
                return null;

            Doctor doctor = null;

            if (DatabaseManager.Connection.State != System.Data.ConnectionState.Open)
            {
                DatabaseManager.OpenConnection();
            }

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;

            command.CommandText = "SELECT uid, accountid, firstname, lastname, organizationid FROM doctors WHERE accountid = :accountid;";
            command.Parameters.Add("accountid", NpgsqlTypes.NpgsqlDbType.Integer).Value = accountID;

            NpgsqlDataReader reader = command.ExecuteReader();

            // Temporary feilds
            int id = -1;
            int accountid = -1;
            string firstname = null;
            string lastname = null;
            int organizationid = -1;

            while (reader.Read())
            {
                id = reader.GetInt32(reader.GetOrdinal("uid"));
                accountid = reader.GetInt32(reader.GetOrdinal("accountid"));
                firstname = reader.GetString(reader.GetOrdinal("firstname"));
                lastname = reader.GetString(reader.GetOrdinal("lastname"));
                organizationid = reader.GetInt32(reader.GetOrdinal("organizationid"));
            }

            reader.Dispose();

            doctor = new Doctor(id, Account.GetAccountFromAccountID(accountid, false), firstname, lastname, Organization.GetOrganizationFromOrganizationID(organizationid, false));

            if (closeConnection)
                DatabaseManager.CloseConnection();

            return doctor;
        }

        public List<Course> GetCoursesNoRoster()
        {
            return GetCoursesNoRoster(true);
        }

        public List<Course> GetCoursesNoRoster(bool closeConnection)
        {
            List<Course> courses = new List<Course>();

            if (DatabaseManager.Connection.State != System.Data.ConnectionState.Open)
            {
                DatabaseManager.OpenConnection();
            }

            string query = String.Format("SELECT uid, startdate, enddate, name, description, hasintubation, hasverticallift, hascpr FROM courses WHERE doctorid='{0}'", ((App)App.Current).CurrentDoctor.ID);

            NpgsqlCommand command = new NpgsqlCommand(query, DatabaseManager.Connection);

            NpgsqlDataReader reader = command.ExecuteReader();

            // Temporary feilds
            int id = -1;
            DateTime startdate = DateTime.MinValue;
            DateTime enddate = DateTime.MinValue;
            string name = null;
            string description = null;
            bool hasintubation = false;
            bool hasverticallift = false;
            bool hascpr = false;

            while (reader.Read())
            {
                id = reader.GetInt32(reader.GetOrdinal("uid"));
                startdate = reader.GetTimeStamp(reader.GetOrdinal("startdate"));
                enddate = reader.GetTimeStamp(reader.GetOrdinal("enddate"));
                name = reader.GetString(reader.GetOrdinal("name"));
                description = reader.GetString(reader.GetOrdinal("description"));
                hasintubation = reader.GetBoolean(reader.GetOrdinal("hasintubation"));
                hasverticallift = reader.GetBoolean(reader.GetOrdinal("hasverticallift"));
                hascpr = reader.GetBoolean(reader.GetOrdinal("hascpr"));

                courses.Add(new Course(id, ((App)App.Current).CurrentDoctor, startdate, enddate, name, description, hasintubation, hasverticallift, hascpr));
            }

            reader.Dispose();

            if (closeConnection)
                DatabaseManager.CloseConnection();

            return courses;
        }
    }
}
