﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;

namespace AIMS
{
    [Serializable()]
    public class Message
    {
        private int id = -1;
        private Student to;
        private Doctor from;
        private Course course;
        private string subject;
        private string body;
        private bool hasBeenRead = false;
        private DateTime dateSent = DateTime.Now;
        private int toid;
        private int fromid;
        private int courseid;

        public int ID { get { return id; } set { id = value; } }
        public int ToID { get { return toid; } set { toid = value; } }
        public int FromID { get { return fromid; } set { fromid = value; } }
        public int CourseID { get { return courseid; } set { courseid = value; } }
        public Student To { get { return to; } set { to = value; } }
        public Doctor From { get { return from; } set { from = value; } }
        public Course Course { get { return course; } set { course = value; } }
        public string Subject { get { return subject; } set { subject = value; } }
        public string Body { get { return body; } set { body = value; } }
        public DateTime DateSent { get { return dateSent; } set { dateSent = value; } }
        public bool HasBeenRead { get { return hasBeenRead; } set { hasBeenRead = value; } }

        public Message()
        {
        }

        public Message(int id, Student to, Doctor from, Course course, string subject, string body, bool hasBeenRead, DateTime dateSent)
        {
            this.id = id;
            this.to = to;
            this.from = from;
            this.course = course;
            this.subject = subject;
            this.body = body;
            this.hasBeenRead = hasBeenRead;
            this.dateSent = dateSent;
        }

        public Message(int id, int toid, int fromid, int courseid, string subject, string body, bool hasbeenread, DateTime datesent)
        {
            this.id = id;
            this.toid = toid;
            this.fromid = fromid;
            this.courseid = courseid;
            this.subject = subject;
            this.body = body;
            this.hasBeenRead = hasbeenread;
            this.dateSent = datesent;
        }

        public Message(int id, int toid, Doctor from, int courseid, string subject, string body, bool hasbeenread, DateTime datesent)
        {
            this.id = id;
            this.toid = toid;
            this.from = from;
            this.courseid = courseid;
            this.subject = subject;
            this.body = body;
            this.hasBeenRead = hasbeenread;
            this.dateSent = datesent;
        }

        public Message(int id, int courseid, int studentID, Doctor doctor, string subject, string body, bool hasbeenread, DateTime datesent)
        {
            this.id = id;
            this.courseid = courseid;
            this.from = doctor;
            this.toid = studentID;            
            this.subject = subject;
            this.body = body;
            this.hasBeenRead = hasbeenread;
            this.dateSent = datesent;
        }

        public static Message GetMessageFromMessageID(int messageID, bool closeConnection)
        {
            if (messageID <= 0)
                return null;

            Message message = null;

            if (DatabaseManager.Connection.State != System.Data.ConnectionState.Open)
            {
                DatabaseManager.OpenConnection();
            }

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;

            command.CommandText = "SELECT uid, courseid, toid, fromid, subject, body, datesent, hasbeenread FROM messages WHERE uid = :messageid;";
            command.Parameters.Add("messageid", NpgsqlTypes.NpgsqlDbType.Integer).Value = messageID;

            NpgsqlDataReader reader = command.ExecuteReader();

            // Temporary feilds
            int id = -1;
            int courseid = -1;
            int toid = -1;
            int fromid = -1;
            string subject = null;
            string body = null;
            DateTime datesent = DateTime.MinValue;
            bool hasbeenread = false;

            while (reader.Read())
            {
                id = reader.GetInt32(reader.GetOrdinal("uid"));
                courseid = reader.GetInt32(reader.GetOrdinal("courseid"));
                toid = reader.GetInt32(reader.GetOrdinal("toid"));
                fromid = reader.GetInt32(reader.GetOrdinal("fromid"));
                subject = reader.GetString(reader.GetOrdinal("subject"));
                body = reader.GetString(reader.GetOrdinal("body"));
                datesent = reader.GetTimeStamp(reader.GetOrdinal("datesent"));
                hasbeenread = reader.GetBoolean(reader.GetOrdinal("hasbeenread"));
            }

            reader.Dispose();

            if (id > 0)
            {
                message = new Message(id, toid, Doctor.GetDoctorFromDoctorID(fromid, false), courseid, subject, body, hasbeenread, datesent );
            }

            if (closeConnection)
                DatabaseManager.CloseConnection();

            return message;
        }

        public static Message GetMessageFullObjectFromMessageID(int messageID, bool closeConnection)
        {
            if (messageID <= 0)
                return null;

            Message message = null;

            if (DatabaseManager.Connection.State != System.Data.ConnectionState.Open)
            {
                DatabaseManager.OpenConnection();
            }

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;

            command.CommandText = "SELECT uid, courseid, toid, fromid, subject, body, datesent, hasbeenread FROM messages WHERE uid = :messageid;";
            command.Parameters.Add("messageid", NpgsqlTypes.NpgsqlDbType.Integer).Value = messageID;

            NpgsqlDataReader reader = command.ExecuteReader();

            // Temporary feilds
            int id = -1;
            int courseid = -1;
            int toid = -1;
            int fromid = -1;
            string subject = null;
            string body = null;
            DateTime datesent = DateTime.MinValue;
            bool hasbeenread = false;

            while (reader.Read())
            {
                id = reader.GetInt32(reader.GetOrdinal("uid"));
                courseid = reader.GetInt32(reader.GetOrdinal("courseid"));
                toid = reader.GetInt32(reader.GetOrdinal("toid"));
                fromid = reader.GetInt32(reader.GetOrdinal("fromid"));
                subject = reader.GetString(reader.GetOrdinal("subject"));
                body = reader.GetString(reader.GetOrdinal("body"));
                datesent = reader.GetTimeStamp(reader.GetOrdinal("datesent"));
                hasbeenread = reader.GetBoolean(reader.GetOrdinal("hasbeenread"));
            }

            reader.Dispose();

            if (id > 0)
            {
                message = new Message(id, Student.GetStudentFromStudentID(toid, true), Doctor.GetDoctorFromDoctorID(fromid, true), Course.GetCourseFromCourseID(courseid), subject, body, hasbeenread, datesent);
            }

            if (closeConnection)
                DatabaseManager.CloseConnection();

            return message;
        }

        public static List<Message> FindMessagesForStudentID( int studentID )
        {
            if( studentID <= 0 )
                return new List<Message>();

            List<Message> messages = new List<Message>();

            DatabaseManager.OpenConnection();
            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "SELECT uid, courseid, toid, fromid, subject, body, datesent, hasbeenread FROM messages WHERE toid = :studentid";
            command.Parameters.Add("studentid", NpgsqlTypes.NpgsqlDbType.Integer).Value = studentID;

            NpgsqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                messages.Add( new Message( reader.GetInt32(reader.GetOrdinal("uid")), reader.GetInt32(reader.GetOrdinal("courseid")), studentID, reader.GetInt32(reader.GetOrdinal("fromid")), reader.GetString(reader.GetOrdinal("subject")), reader.GetString(reader.GetOrdinal("body")), reader.GetBoolean(reader.GetOrdinal("hasbeenread")), reader.GetDateTime( reader.GetOrdinal( "datesent" ) ) ) );
            }

            reader.Dispose();

            DatabaseManager.CloseConnection();

            foreach (Message message in messages)
            {
                message.From = Doctor.GetDoctorFromDoctorID(message.fromid, true);
            }

            return messages;
        }
    }
}
