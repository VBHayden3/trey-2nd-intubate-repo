﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;

namespace AIMS
{
    [Serializable()]
    public class Organization
    {
        public enum Organizations
        {
            None = 0,
            SimIS,
            EVMS
        }

        private int id;
        private string name;
        private Package package;
        //private Organizations organization;

        public int ID { get { return id; } set { id = value; } }
        public string Name { get { return name; } set { name = value; } }
        public Package Package { get { return package; } set { package = value; } }
        //public Organizations Organization { get { return organization; } set { organization = value; } }

        public Organization()
        {
            id = -1;
            name = null;
            package = null;
            //organization = Organizations.None;
        }

        public Organization(int id, string name, Package package)
        {
            this.ID = id;
            this.Name = name;
            this.Package = package;
        }

        public override string ToString()
        {
            return String.Format("ID#: {0} Name: \"{0}\"", this.ID, this.Name);
        }

        public static Organization GetOrganizationFromOrganizationID(int organizationID, bool closeConnection)
        {
            if (organizationID <= 0)
                return null;

            Organization organization = null;

            if (DatabaseManager.Connection.State != System.Data.ConnectionState.Open)
            {
                DatabaseManager.OpenConnection();
            }

            string query = String.Format("SELECT uid, name, packageid FROM organizations WHERE uid='{0}'", organizationID);

            NpgsqlCommand command = new NpgsqlCommand(query, DatabaseManager.Connection);

            NpgsqlDataReader reader = command.ExecuteReader();

            // Temporary feilds
            int id = -1;
            string name = null;
            int packageid = 0;

            while (reader.Read())
            {
                id = reader.GetInt32(reader.GetOrdinal("uid"));
                name = reader.GetString(reader.GetOrdinal("name"));
                packageid = reader.GetInt32(reader.GetOrdinal("packageid"));
            }

            reader.Dispose();

            organization = new Organization( id, name, Package.GetPackageFromPackageID( packageid, false ) );

            if (closeConnection)
                DatabaseManager.CloseConnection();

            return organization;
        }
    }
}
