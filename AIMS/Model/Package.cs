﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;

namespace AIMS
{
    [Serializable()]
    public class Package
    {
        public enum TaskOptions
        {
            None = 0,
            All,
            IntubationOnly,
            VerticalLiftOnly,
            LateralMovementOnly,
            IntubationAndVerticalLift,
            IntubationAndLateralMovement,
            VerticalLiftAndLateralMovement
        }

        private int id;
        private string name;
        private string description;
        private TaskOptions taskoption;

        public int ID { get { return id; } set { id = value; } }
        public string Name { get { return name; } set { name = value; } }
        public string Description { get { return description; } set { description = value; } }
        public TaskOptions TaskOption { get { return taskoption; } set { taskoption = value; } }

        public Package()
        {
            id = -1;
            name = null;
            description = null;
            taskoption = TaskOptions.None;
        }

        public Package(int id, string name, string description, TaskOptions taskoption)
        {
            this.ID = id;
            this.Name = name;
            this.Description = description;
            this.TaskOption = taskoption;
        }

        public static Package GetPackageFromPackageID(int packageID, bool closeConnection)
        {
            if (packageID <= 0)
                return null;

            Package package = null;

            if (DatabaseManager.Connection.State != System.Data.ConnectionState.Open)
            {
                DatabaseManager.OpenConnection();
            }

            string query = String.Format("SELECT uid, name, description, availabletasks FROM packages WHERE uid='{0}'", packageID);

            NpgsqlCommand command = new NpgsqlCommand(query, DatabaseManager.Connection);

            NpgsqlDataReader reader = command.ExecuteReader();

            // Temporary feilds
            int id = -1;
            string name = null;
            string description = null;
            int availabletasks = 0;

            while (reader.Read())
            {
                id = reader.GetInt32(reader.GetOrdinal("uid"));
                name = reader.GetString(reader.GetOrdinal("name"));
                description = reader.GetString(reader.GetOrdinal("description"));
                availabletasks = reader.GetInt32(reader.GetOrdinal("availabletasks"));
            }

            reader.Dispose();

            package = new Package( id, name, description, (TaskOptions)availabletasks );

            if (closeConnection)
                DatabaseManager.CloseConnection();

            return package;
        }
    }
}
