﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using System.Windows;

namespace AIMS
{
    [Serializable()]
    public class Roster
    {
        private int id = -1;
        private Course course = null;
        private int courseid = -1;
        private List<Student> students = null;
        private List<int> studentIDs = null;

        public int ID { get { return id; } set { id = value; } }
        public Course Course { get { return course; } set { course = value; } }
        public int CourseID { get { return courseid; } set { courseid = value; } }
        public List<Student> Students { get { return students; } set { students = value; } }
        public List<int> StudentIDs { get { return studentIDs; } set { studentIDs = value; } }

        public Roster()
        {
        }

        public Roster(int id, Course course, List<Student> students)
        {
            this.ID = id;
            this.Course = course;
            this.Students = students;
        }

        public Roster(int id, Course course, List<int> studentids)
        {
            this.ID = id;
            this.Course = course;
            this.StudentIDs = studentids;
        }

        public Roster(int id, int courseid, List<int> studentids)
        {
            this.ID = id;
            this.Course = null;
            this.CourseID = courseid;
            this.StudentIDs = studentids;
        }

        public Roster(int id, int courseid, List<Student> students)
        {
            this.ID = id;
            this.Course = null;
            this.CourseID = courseid;
            this.Students = students;
        }

        public static Roster GetRosterFromRosterID(int rosterID)
        {
            if (rosterID <= 0)
                return null;

            Roster roster = null;

            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "SELECT uid, courseid, studentlist FROM rosters WHERE uid=:rosterid";

            command.Parameters.Add("rosterid", NpgsqlTypes.NpgsqlDbType.Integer).Value = rosterID;

            NpgsqlDataReader reader = command.ExecuteReader();

            // Temporary feilds
            int id = -1;
            int courseid = -1;
            int[] studentids = new int[0];

            while ( reader.Read() )
            {
                id = reader.GetInt32(reader.GetOrdinal("uid"));
                courseid = reader.GetInt32( reader.GetOrdinal("courseid") );
                studentids = reader.GetValue( reader.GetOrdinal( "studentlist" ) ) as int[];
            }

            reader.Dispose();

            roster = new Roster( id, courseid, studentids.ToList<int>() );

            DatabaseManager.CloseConnection();

            return roster;
        }

        /// <summary>
        /// Function to get object from byte array
        /// </summary>
        /// <param name="_ByteArray">byte array to get object</param>
        /// <returns>object</returns>
        public static object ByteArrayToObject(byte[] _ByteArray)
        {
            if (_ByteArray == null || _ByteArray.Length == 0)
                return null;

            try
            {
                // convert byte array to memory stream
                System.IO.MemoryStream _MemoryStream = new System.IO.MemoryStream(_ByteArray);

                // create new BinaryFormatter
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter _BinaryFormatter
                            = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                // set memory stream position to starting point
                _MemoryStream.Position = 0;

                // Deserializes a stream into an object graph and return as a object.
                return _BinaryFormatter.Deserialize(_MemoryStream);
            }
            catch (Exception _Exception)
            {
                // Error
                Console.WriteLine("Exception caught in process: {0}", _Exception.ToString());
                MessageBox.Show( "Exception Caught in Roster Deser" + _Exception.ToString());
            }

            // Error occured, return null
            return null;
        }

        public static Roster GetRosterFromCourseID(int courseID)
        {
            if (courseID <= 0)
                return null;

            Roster roster = null;

            DatabaseManager.CloseConnection();  // must close connection.

            if (DatabaseManager.Connection.State != System.Data.ConnectionState.Open)
            {
                DatabaseManager.OpenConnection();
            }

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "SELECT uid, courseid, studentlist FROM rosters WHERE courseid=:courseid";

            command.Parameters.Add("courseid", NpgsqlTypes.NpgsqlDbType.Integer).Value = courseID;

            NpgsqlDataReader reader = command.ExecuteReader();

            // Temporary feilds
            int id = -1;
            int courseid = -1;
            int[] studentlist = null;

            while ( reader.Read() )
            {
                id = reader.GetInt32(reader.GetOrdinal("uid"));
                courseid = reader.GetInt32( reader.GetOrdinal("courseid") );
                studentlist = reader.GetValue( reader.GetOrdinal( "studentlist" ) ) as int[];
            }

            reader.Dispose();

            roster = new Roster(id, courseid/*Course.GetCourseFromCourseID( courseid, false )*/, studentlist == null ? null : studentlist.ToList<int>());

            DatabaseManager.CloseConnection();

            return roster;
        }

        public static Roster CreateRosterLinkedToCourse( int courseID, List<Student> students )
        {
            if (courseID <= 0)
                return null;

            Roster roster = null;

            DatabaseManager.CloseConnection();  // must close connection.

            if (DatabaseManager.Connection.State != System.Data.ConnectionState.Open)
            {
                DatabaseManager.OpenConnection();
            }

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "INSERT INTO rosters (courseid, studentlist) VALUES ( :courseid, :studentlist ); SELECT uid FROM rosters WHERE courseid = :courseid";
            command.Parameters.Add("studentlist", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Integer).Value = GetStudentIDArray(students);
            command.Parameters.Add("courseid", NpgsqlTypes.NpgsqlDbType.Integer).Value = courseID;
            int rosterid = -1;

            try
            {
                rosterid = (int)command.ExecuteScalar();
            }
            catch( Exception e )
            {
                MessageBox.Show(e.ToString());
            }
            
            DatabaseManager.CloseConnection();

            roster = Roster.GetRosterFromRosterID(rosterid);

            return roster;
        }

        public static Roster CreateRosterLinkedToCourse(int courseID, List<int> studentIDs)
        {
            if (courseID <= 0)
                return null;

            Roster roster = null;

            DatabaseManager.CloseConnection();  // must close connection.

            if (DatabaseManager.Connection.State != System.Data.ConnectionState.Open)
            {
                DatabaseManager.OpenConnection();
            }

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "INSERT INTO rosters (courseid, studentlist) VALUES ( :courseid, :studentlist ); SELECT uid FROM rosters WHERE courseid = :courseid";
            command.Parameters.Add("studentlist", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Integer).Value = GetStudentIDArray(studentIDs);
            command.Parameters.Add("courseid", NpgsqlTypes.NpgsqlDbType.Integer).Value = courseID;
            int rosterid = -1;

            try
            {
                rosterid = (int)command.ExecuteScalar();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }

            DatabaseManager.CloseConnection();

            roster = Roster.GetRosterFromRosterID(rosterid);

            return roster;
        }

        public static byte[] ObjectToByteArray(object _Object)
        {
            try
            {
                // create new memory stream
                System.IO.MemoryStream _MemoryStream = new System.IO.MemoryStream();

                // create new BinaryFormatter
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter _BinaryFormatter
                            = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                // Serializes an object, or graph of connected objects, to the given stream.
                _BinaryFormatter.Serialize(_MemoryStream, _Object);

                // convert stream to byte array and return
                return _MemoryStream.ToArray();
            }
            catch (Exception _Exception)
            {
                // Error
                Console.WriteLine("Exception caught in process: {0}", _Exception.ToString());
                MessageBox.Show(_Exception.ToString());
            }

            // Error occured, return null
            return null;
        }

        private static int[] GetStudentIDArray( List<Student> students)
        {
            int[] list = null;

            if (students != null)
            {
                list = new int[students.Count];

                for( int i = 0; i < students.Count; i++ )
                {
                    list[i] = students[i].ID;
                }
            }

            return list;
        }

        private static int[] GetStudentIDArray(List<int> studentIDs)
        {
            int[] list = null;

            if (studentIDs != null)
            {
                list = new int[studentIDs.Count];

                for (int i = 0; i < studentIDs.Count; i++)
                {
                    list[i] = studentIDs[i];
                }
            }

            return list;
        }
    }
}
