﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using System.Windows;

using AIMS.Tasks;
using AIMS.Model;

namespace AIMS
{
    [Serializable()]
    public class Student
    {
        // An intubation statsheet for the student, this is attached once it's loaded at login.
        public LessonStatsheet IntubationStatsheet { get; set; }
        public LessonStatsheet CatheterInsertionStatsheet { get; set; }
        public LessonStatsheet IVInsertionStatsheet { get; set; }

        private int id;
        private Account account;
        private string firstName;
        private string lastName;
        private Organization organization;
        private List<int> enrolledCourseIDs;
        private StudentSettings userSettings;

        public int ID { get { return id; } set { id = value; } }
        public Account Account { get { return account; } set { account = value; } }
        public string FirstName { get { return firstName; } set { firstName = value; } }
        public string LastName { get { return lastName; } set { lastName = value; } }
        public Organization Organization { get { return organization; } set { organization = value; } }
        public List<int> EnrolledCourseIDs { get { return enrolledCourseIDs; } set { enrolledCourseIDs = value; } }

        public StudentSettings UserSettings
        {
            get
            {
                if (userSettings == null)
                    userSettings = new StudentSettings(this);
                return userSettings;
            }
        }

        public Student()
        {
            id = -1;
            account = null;
            firstName = null;
            lastName = null;
            organization = null;
            enrolledCourseIDs = new List<int>();
        }

        public Student(int id, Account account, string firstname, string lastname, Organization organization, List<int> enrolledcourseids)
        {
            this.ID = id;
            this.Account = account;
            this.FirstName = firstname;
            this.LastName = lastname;
            this.Organization = organization;
            this.EnrolledCourseIDs = enrolledcourseids;
        }

        public static Student GetStudentFromStudentID(int studentID, bool closeConnection)
        {
            if (studentID <= 0)
                return null;

            Student student = null;

            if (DatabaseManager.Connection.State != System.Data.ConnectionState.Open)
            {
                DatabaseManager.OpenConnection();
            }

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;

            command.CommandText = "SELECT uid, accountid, firstname, lastname, organizationid, enrolledcourseids FROM students WHERE uid = :studentid;";
            command.Parameters.Add("studentid", NpgsqlTypes.NpgsqlDbType.Integer).Value = studentID;

            NpgsqlDataReader reader = command.ExecuteReader();

            // Temporary feilds
            int id = -1;
            int accountid = -1;
            string firstname = null;
            string lastname = null;
            int organizationid = -1;
            int[] enrolledcourseids = null;

            while (reader.Read())
            {
                id = reader.GetInt32(reader.GetOrdinal("uid"));
                accountid = reader.GetInt32(reader.GetOrdinal("accountid"));
                firstname = reader.GetString(reader.GetOrdinal("firstname"));
                lastname = reader.GetString(reader.GetOrdinal("lastname"));
                organizationid = reader.GetInt32(reader.GetOrdinal("organizationid"));
                enrolledcourseids = reader.GetValue(reader.GetOrdinal("enrolledcourseids")) as int[];
            }

            reader.Dispose();

            if (id > 0)
            {
                student = new Student(id, Account.GetAccountFromAccountID(accountid, false), firstname, lastname, Organization.GetOrganizationFromOrganizationID(organizationid, false), enrolledcourseids.ToList<int>());
            }

            if (closeConnection)
                DatabaseManager.CloseConnection();
            
            return student;
        }

        public static Student GetStudentFromAccountID(int accountID, bool closeConnection)
        {
            if (accountID <= 0)
                return null;

            Student student = null;

            if (DatabaseManager.Connection.State != System.Data.ConnectionState.Open)
            {
                DatabaseManager.OpenConnection();
            }

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;

            command.CommandText = "SELECT uid, accountid, firstname, lastname, organizationid, enrolledcourseids FROM students WHERE accountid = :accountid;";
            command.Parameters.Add("accountid", NpgsqlTypes.NpgsqlDbType.Integer).Value = accountID;

            NpgsqlDataReader reader = command.ExecuteReader();

            // Temporary feilds
            int id = -1;
            int accountid = -1;
            string firstname = null;
            string lastname = null;
            int organizationid = -1;
            int[] enrolledcourseids = null;

            while (reader.Read())
            {
                id = reader.GetInt32(reader.GetOrdinal("uid"));
                accountid = reader.GetInt32(reader.GetOrdinal("accountid"));
                firstname = reader.GetString(reader.GetOrdinal("firstname"));
                lastname = reader.GetString(reader.GetOrdinal("lastname"));
                organizationid = reader.GetInt32(reader.GetOrdinal("organizationid"));
                enrolledcourseids = reader.GetValue(reader.GetOrdinal("enrolledcourseids")) as int[];
            }

            reader.Dispose();

            if (id > 0) // Only create a student from the ID, if it was found. Else, it will remain null.
            {
                student = new Student(id, Account.GetAccountFromAccountID(accountid, false), firstname, lastname, Organization.GetOrganizationFromOrganizationID(organizationid, false), enrolledcourseids.ToList<int>());
            }

            if (closeConnection)
                DatabaseManager.CloseConnection();

            return student;
        }

        public static Student CreateStudentLinkedToAccount(int accountID)
        {
            if (accountID <= 0)
            {
                return null;
            }

            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = @"INSERT INTO students ( accountid, firstname, lastname, organizationid, enrolledcourseids) VALUES (@accountid, @firstname, @lastname, @organizationid, @enrolledcourseids); SELECT uid FROM students WHERE accountid=:accountid;";

            command.Parameters.Add("@accountid", NpgsqlTypes.NpgsqlDbType.Integer).Value = accountID;
            command.Parameters.Add("@firstname", NpgsqlTypes.NpgsqlDbType.Text).Value = "";
            command.Parameters.Add("@lastname", NpgsqlTypes.NpgsqlDbType.Text).Value = "";
            command.Parameters.Add("@organizationid", NpgsqlTypes.NpgsqlDbType.Integer).Value = ((App)App.Current).CurrentDoctor.Organization.ID;
            command.Parameters.Add("accountid", NpgsqlTypes.NpgsqlDbType.Integer).Value = accountID;

            int id = -1;

            try
            {
                id = (int)command.ExecuteScalar();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
            }

            DatabaseManager.CloseConnection();

            return Student.GetStudentFromStudentID(id, true);
        }

        public static Student CreateStudentLinkedToAccount(int accountID, String firstName, String lastName, int organization)
        {
            if (accountID <= 0)
            {
                return null;
            }

            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = @"INSERT INTO students ( accountid, firstname, lastname, organizationid, enrolledcourseids) VALUES (@accountid, @firstname, @lastname, @organizationid, @enrolledcourseids); SELECT uid FROM students WHERE accountid=:accountid;";

            command.Parameters.Add("@accountid", NpgsqlTypes.NpgsqlDbType.Integer).Value = accountID;
            command.Parameters.Add("@firstname", NpgsqlTypes.NpgsqlDbType.Text).Value = firstName;
            command.Parameters.Add("@lastname", NpgsqlTypes.NpgsqlDbType.Text).Value = lastName;
            command.Parameters.Add("@organizationid", NpgsqlTypes.NpgsqlDbType.Integer).Value = organization;
            int[] enrolledcourseids = new int[0];
            command.Parameters.Add("@enrolledcourseids", NpgsqlTypes.NpgsqlDbType.Array | NpgsqlTypes.NpgsqlDbType.Integer).Value = enrolledcourseids;
            command.Parameters.Add("accountid", NpgsqlTypes.NpgsqlDbType.Integer).Value = accountID;

            int id = -1;

            try
            {
                id = (int)command.ExecuteScalar();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
            }

            DatabaseManager.CloseConnection();

            return Student.GetStudentFromStudentID(id, true);
        }

        public static void UpdateStudentLinkedToAccount(String firstName, String lastName)
        {
            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            // The command updates the password, and also updates the flag in the DB so they don't need to update their password again.
            command.CommandText = "UPDATE students SET firstname = :firstname, lastname = :lastname  WHERE accountid = :accountid;";

            command.Parameters.Add("firstname", NpgsqlTypes.NpgsqlDbType.Text).Value = firstName;
            command.Parameters.Add("lastname", NpgsqlTypes.NpgsqlDbType.Text).Value = lastName;
            command.Parameters.Add("accountid", NpgsqlTypes.NpgsqlDbType.Smallint).Value = ((App)App.Current).CurrentAccount.ID;

            int enq = command.ExecuteNonQuery();

            DatabaseManager.CloseConnection();

            // Update the information on the currently logged in student object.
            ((App)App.Current).CurrentStudent.FirstName = firstName;
            ((App)App.Current).CurrentStudent.LastName = lastName;
        }
    }
}
