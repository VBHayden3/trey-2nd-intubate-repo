﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;

namespace AIMS.Model
{
    public abstract class UserSettings
    {
        #region Properties

        private int id;
        public int ID { get { return this.id; } set { this.id = value; } }

        #endregion Properties


        #region Constructors

        public UserSettings()
        {
        }

        #endregion Constructors
    
    }


    public class StudentSettings : UserSettings
    {
        #region Properties

        private string setting1;
        private string setting2;
        private string setting3;

        public string Setting1
        {
            get
            {
                return this.setting1;
            }
            set
            {
                if (this.setting1 != value)
                {
                    this.setting1 = value;
                    this.Update("setting1", value);
                }
            }
        }

        public string Setting2
        {
            get
            {
                return this.setting2;
            }
            set
            {
                if (this.setting2 != value)
                {
                    this.setting2 = value;
                    this.Update("setting2", value);
                }
            }
        }

        public string Setting3
        {
            get
            {
                return this.setting3;
            }
            set
            {
                if (this.setting3 != value)
                {
                    this.setting3 = value;
                    this.Update("setting3", value);
                }
            }
        }

        private Student student;
        public Student Student { get { return this.student; } set { this.student = value; } }

        #endregion Properties


        #region Constructors

        public StudentSettings(Student student)
        {
            this.student = student;
            this.setting1 = null;
            this.setting2 = null;
            this.setting3 = null;
            this.Init(student.ID);
        }

        #endregion Constructors


        #region Methods

        private void Init(int studentID)
        {
            if (!(this.Read(studentID)))
            {
                // New Student Created, add new default settings
                this.Create(studentID);
            }
        }


        // Read(Get) an existing Student's settings from the database
        private bool Read(int studentID)
        {
            bool found = false;

            DatabaseManager.OpenConnection();
            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "SELECT * FROM student_settings WHERE studentid=" + studentID;

            NpgsqlDataReader reader = command.ExecuteReader();

            try
            {
                if (reader.HasRows)
                {
                    // Get exisiting settings
                    found = true;
                    while (reader.Read())
                    {
                        base.ID = reader.GetInt32(reader.GetOrdinal("id"));
                        this.setting1 = reader.GetString(reader.GetOrdinal("setting1"));
                        this.setting2 = reader.GetString(reader.GetOrdinal("setting2"));
                        this.setting3 = reader.GetString(reader.GetOrdinal("setting3"));
                    }
                    System.Diagnostics.Debug.WriteLine("StudentSettings::Read Setting1 = " + this.setting1);
                    System.Diagnostics.Debug.WriteLine("StudentSettings::Read Setting2 = " + this.setting2);
                    System.Diagnostics.Debug.WriteLine("StudentSettings::Read Setting3 = " + this.setting3);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                System.Windows.MessageBox.Show(exception.Message);
            }

            reader.Dispose();
            DatabaseManager.CloseConnection();

            return found;
        }


        // Create(Add) new Student settings to the database for a new Student
        private void Create(int studentID)
        {
            string defaultSetting = "Default Setting";
            
            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            //command.CommandText = @"INSERT INTO student_settings (studentid, setting1, setting2, setting3) VALUES (@studentid, @setting1, @setting2, @setting3);  SELECT id FROM student_settings WHERE studentid=:studentID;";
            command.CommandText = "INSERT INTO student_settings (studentid, setting1, setting2, setting3) VALUES (:studentid, :setting1, :setting2, :setting3) RETURNING id;";

            command.Parameters.Add("studentid", NpgsqlTypes.NpgsqlDbType.Integer).Value = studentID;
            // Defaults here or use column defaults in the database?
            command.Parameters.Add("setting1", NpgsqlTypes.NpgsqlDbType.Text).Value = defaultSetting;
            command.Parameters.Add("setting2", NpgsqlTypes.NpgsqlDbType.Text).Value = defaultSetting;
            command.Parameters.Add("setting3", NpgsqlTypes.NpgsqlDbType.Text).Value = defaultSetting;

            // Set properties
            this.setting1 = defaultSetting;
            this.setting2 = defaultSetting;
            this.setting3 = defaultSetting;
            //this.setting1 = command.Parameters["setting1"].Value.ToString();
            //this.setting1 = command.Parameters[2].Value.ToString();
            //this.setting2 = command.Parameters[3].Value.ToString();
            //this.setting3 = command.Parameters[4].Value.ToString();
            
            try
            {
                // Get PK Autonumber
                base.ID = (int)command.ExecuteScalar();
                //this.ID = (int)command.ExecuteScalar();
                System.Diagnostics.Debug.WriteLine("StudentSettings::Create ID(PK) = " + this.ID);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                System.Windows.MessageBox.Show(exception.ToString());
            }
            System.Diagnostics.Debug.WriteLine("StudentSettings::Create Setting1 = " + this.setting1);
            System.Diagnostics.Debug.WriteLine("StudentSettings::Create Setting2 = " + this.setting2);
            System.Diagnostics.Debug.WriteLine("StudentSettings::Create Setting3 = " + this.setting3);

            DatabaseManager.CloseConnection();
        }


        // Update a setting to the database
        private void Update(string columnName, string value)
        {
            DatabaseManager.OpenConnection();
            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            
            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "UPDATE student_settings SET " + columnName + " = :value WHERE id = :id";

            command.Parameters.Add("value", NpgsqlTypes.NpgsqlDbType.Text).Value = value;
            command.Parameters.Add("id", NpgsqlTypes.NpgsqlDbType.Integer).Value = base.ID;

            try
            {
                int enq = command.ExecuteNonQuery();
                System.Diagnostics.Debug.WriteLine("StudentSettings::Update ExecuteNonQuery Int = " + enq);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                System.Windows.MessageBox.Show(exception.ToString());
            }

            System.Diagnostics.Debug.WriteLine("StudentSettings::Update Column Name = " + columnName);
            System.Diagnostics.Debug.WriteLine("StudentSettings::Update Column Value = " + value);
            System.Diagnostics.Debug.WriteLine("StudentSettings::Update id = " + base.ID);

            DatabaseManager.CloseConnection();
        }


        // Delete settings from the database
        // DO NOT USE until the system supports deleting accounts from the database
        public void Delete(int studentID)
        {
            DatabaseManager.OpenConnection();
            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "DELETE FROM student_settings WHERE studentid = :studentid AND id = :id";

            command.Parameters.Add("studentid", NpgsqlTypes.NpgsqlDbType.Integer).Value = studentID;
            command.Parameters.Add("id", NpgsqlTypes.NpgsqlDbType.Integer).Value = base.ID;

            try
            {
                int enq = command.ExecuteNonQuery();
                System.Diagnostics.Debug.WriteLine("StudentSettings::Update ExecuteNonQuery Int = " + enq);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                System.Windows.MessageBox.Show(exception.ToString());
            }

            System.Diagnostics.Debug.WriteLine("StudentSettings::Delete id = " + base.ID);
            System.Diagnostics.Debug.WriteLine("StudentSettings::Delete studentID = " + studentID);

            DatabaseManager.CloseConnection();
        }

        #endregion Methods

    } //End StudentSettings Class


    public class DoctorSettings : UserSettings
    {
        #region Properties

        private string setting1;
        private string setting2;
        private string setting3;

        public string Setting1
        {
            get
            {
                return this.setting1;
            }
            set
            {
                if (this.setting1 != value)
                {
                    this.setting1 = value;
                    this.Update("setting1", value);
                }
            }
        }

        public string Setting2
        {
            get
            {
                return this.setting2;
            }
            set
            {
                if (this.setting2 != value)
                {
                    this.setting2 = value;
                    this.Update("setting2", value);
                }
            }
        }

        public string Setting3
        {
            get
            {
                return this.setting3;
            }
            set
            {
                if (this.setting3 != value)
                {
                    this.setting3 = value;
                    this.Update("setting3", value);
                }
            }
        }

        private Doctor doctor;
        public Doctor Doctor { get { return this.doctor; } set { this.doctor = value; } }

        #endregion Properties


        #region Constructors

        public DoctorSettings(Doctor doctor)
        {
            this.doctor = doctor;
            this.setting1 = null;
            this.setting2 = null;
            this.setting3 = null;
            this.Init(doctor.ID);
        }

        #endregion Constructors


        #region Methods

        private void Init(int doctorID)
        {
            if (!(this.Read(doctorID)))
            {
                // New Doctor Created, add new default settings
                this.Create(doctorID);
            }
        }


        // Read(Get) an existing Doctor's settings from the database
        private bool Read(int doctorID)
        {
            bool found = false;

            DatabaseManager.OpenConnection();
            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "SELECT * FROM doctor_settings WHERE doctorid=" + doctorID;

            NpgsqlDataReader reader = command.ExecuteReader();

            try
            {
                if (reader.HasRows)
                {
                    // Get exisiting settings
                    found = true;
                    while (reader.Read())
                    {
                        base.ID = reader.GetInt32(reader.GetOrdinal("id"));
                        this.setting1 = reader.GetString(reader.GetOrdinal("setting1"));
                        this.setting2 = reader.GetString(reader.GetOrdinal("setting2"));
                        this.setting3 = reader.GetString(reader.GetOrdinal("setting3"));
                    }
                    System.Diagnostics.Debug.WriteLine("DoctorSettings::Read Setting1 = " + this.setting1);
                    System.Diagnostics.Debug.WriteLine("DoctorSettings::Read Setting2 = " + this.setting2);
                    System.Diagnostics.Debug.WriteLine("DoctorSettings::Read Setting3 = " + this.setting3);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                System.Windows.MessageBox.Show(exception.Message);
            }

            reader.Dispose();
            DatabaseManager.CloseConnection();

            return found;
        }


        // Create(Add) new Doctor settings to the database for a new Doctor
        private void Create(int doctorID)
        {
            string defaultSetting = "Default Setting";
            
            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            //command.CommandText = "INSERT INTO doctor_settings (doctorid, setting1, setting2, setting3) VALUES (:doctorid, :setting1, :setting2, :setting3);  SELECT id FROM doctor_settings WHERE doctorid=:doctorID;";
            command.CommandText = "INSERT INTO doctor_settings (doctorid, setting1, setting2, setting3) VALUES (:doctorid, :setting1, :setting2, :setting3) RETURNING id;";

            command.Parameters.Add("doctorid", NpgsqlTypes.NpgsqlDbType.Integer).Value = doctorID;
            // Defaults here or use column defaults in the database?
            command.Parameters.Add("setting1", NpgsqlTypes.NpgsqlDbType.Text).Value = defaultSetting;
            command.Parameters.Add("setting2", NpgsqlTypes.NpgsqlDbType.Text).Value = defaultSetting;
            command.Parameters.Add("setting3", NpgsqlTypes.NpgsqlDbType.Text).Value = defaultSetting;

            // Set properties
            this.setting1 = defaultSetting;
            this.setting2 = defaultSetting;
            this.setting3 = defaultSetting;
            
            try
            {
                // Get PK Autonumber
                base.ID = (int)command.ExecuteScalar();
                System.Diagnostics.Debug.WriteLine("DoctorSettings::Create ID(PK) = " + this.ID);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                System.Windows.MessageBox.Show(exception.ToString());
            }
            System.Diagnostics.Debug.WriteLine("DoctorSettings::Create Setting1 = " + this.setting1);
            System.Diagnostics.Debug.WriteLine("DoctorSettings::Create Setting2 = " + this.setting2);
            System.Diagnostics.Debug.WriteLine("DoctorSettings::Create Setting3 = " + this.setting3);

            DatabaseManager.CloseConnection();
        }


        // Update a setting to the database
        private void Update(string columnName, string value)
        {
            DatabaseManager.OpenConnection();
            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            
            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "UPDATE doctor_settings SET " + columnName + " = :value WHERE id = :id";

            command.Parameters.Add("value", NpgsqlTypes.NpgsqlDbType.Text).Value = value;
            command.Parameters.Add("id", NpgsqlTypes.NpgsqlDbType.Integer).Value = base.ID;

            try
            {
                int enq = command.ExecuteNonQuery();
                System.Diagnostics.Debug.WriteLine("DoctorSettings::Update ExecuteNonQuery Int = " + enq);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                System.Windows.MessageBox.Show(exception.ToString());
            }

            System.Diagnostics.Debug.WriteLine("DoctorSettings::Update Column Name = " + columnName);
            System.Diagnostics.Debug.WriteLine("DoctorSettings::Update Column Value = " + value);
            System.Diagnostics.Debug.WriteLine("DoctorSettings::Update id = " + base.ID);

            DatabaseManager.CloseConnection();
        }


        // Delete settings from the database
        // DO NOT USE until the system supports deleting accounts from the database
        public void Delete(int doctorID)
        {
            DatabaseManager.OpenConnection();
            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "DELETE FROM doctor_settings WHERE doctorid = :doctorid AND id = :id";

            command.Parameters.Add("doctorid", NpgsqlTypes.NpgsqlDbType.Integer).Value = doctorID;
            command.Parameters.Add("id", NpgsqlTypes.NpgsqlDbType.Integer).Value = base.ID;

            try
            {
                int enq = command.ExecuteNonQuery();
                System.Diagnostics.Debug.WriteLine("DoctorSettings::Update ExecuteNonQuery Int = " + enq);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                System.Windows.MessageBox.Show(exception.ToString());
            }

            System.Diagnostics.Debug.WriteLine("DoctorSettings::Delete id = " + base.ID);
            System.Diagnostics.Debug.WriteLine("DoctorSettings::Delete doctorID = " + doctorID);

            DatabaseManager.CloseConnection();
        }

        #endregion Methods

    } //End DoctorSettings Class


    public class AdministratorSettings : UserSettings
    {
        #region Properties

        private string setting1;
        private string setting2;
        private string setting3;

        public string Setting1
        {
            get
            {
                return this.setting1;
            }
            set
            {
                if (this.setting1 != value)
                {
                    this.setting1 = value;
                    this.Update("setting1", value);
                }
            }
        }

        public string Setting2
        {
            get
            {
                return this.setting2;
            }
            set
            {
                if (this.setting2 != value)
                {
                    this.setting2 = value;
                    this.Update("setting2", value);
                }
            }
        }

        public string Setting3
        {
            get
            {
                return this.setting3;
            }
            set
            {
                if (this.setting3 != value)
                {
                    this.setting3 = value;
                    this.Update("setting3", value);
                }
            }
        }

        private Administrator administrator;
        public Administrator Administrator { get { return this.administrator; } set { this.administrator = value; } }

        #endregion Properties


        #region Constructors

        public AdministratorSettings(Administrator administrator)
        {
            this.administrator = administrator;
            this.setting1 = null;
            this.setting2 = null;
            this.setting3 = null;
            this.Init(administrator.ID);
        }

        #endregion Constructors


        #region Methods

        private void Init(int administratorID)
        {
            if (!(this.Read(administratorID)))
            {
                // New Administrator Created, add new default settings
                this.Create(administratorID);
            }
        }


        // Read(Get) an existing Administrator's settings from the database
        private bool Read(int administratorID)
        {
            bool found = false;

            DatabaseManager.OpenConnection();
            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "SELECT * FROM administrator_settings WHERE administratorid=" + administratorID;

            NpgsqlDataReader reader = command.ExecuteReader();

            try
            {
                if (reader.HasRows)
                {
                    // Get exisiting settings
                    found = true;
                    while (reader.Read())
                    {
                        base.ID = reader.GetInt32(reader.GetOrdinal("id"));
                        this.setting1 = reader.GetString(reader.GetOrdinal("setting1"));
                        this.setting2 = reader.GetString(reader.GetOrdinal("setting2"));
                        this.setting3 = reader.GetString(reader.GetOrdinal("setting3"));
                    }
                    System.Diagnostics.Debug.WriteLine("AdministratorSettings::Read Setting1 = " + this.setting1);
                    System.Diagnostics.Debug.WriteLine("AdministratorSettings::Read Setting2 = " + this.setting2);
                    System.Diagnostics.Debug.WriteLine("AdministratorSettings::Read Setting3 = " + this.setting3);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                System.Windows.MessageBox.Show(exception.Message);
            }

            reader.Dispose();
            DatabaseManager.CloseConnection();

            return found;
        }


        // Create(Add) new Administrator settings to the database for a new Administrator
        private void Create(int administratorID)
        {
            string defaultSetting = "Default Setting";
            
            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "INSERT INTO administrator_settings (administratorid, setting1, setting2, setting3) VALUES (:administratorid, :setting1, :setting2, :setting3) RETURNING id;";

            command.Parameters.Add("administratorid", NpgsqlTypes.NpgsqlDbType.Integer).Value = administratorID;
            // Defaults here or use column defaults in the database?
            command.Parameters.Add("setting1", NpgsqlTypes.NpgsqlDbType.Text).Value = defaultSetting;
            command.Parameters.Add("setting2", NpgsqlTypes.NpgsqlDbType.Text).Value = defaultSetting;
            command.Parameters.Add("setting3", NpgsqlTypes.NpgsqlDbType.Text).Value = defaultSetting;

            // Set properties
            this.setting1 = defaultSetting;
            this.setting2 = defaultSetting;
            this.setting3 = defaultSetting;
            
            try
            {
                // Get PK Autonumber
                base.ID = (int)command.ExecuteScalar();
                System.Diagnostics.Debug.WriteLine("AdministratorSettings::Create ID(PK) = " + this.ID);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                System.Windows.MessageBox.Show(exception.ToString());
            }
            System.Diagnostics.Debug.WriteLine("AdministratorSettings::Create Setting1 = " + this.setting1);
            System.Diagnostics.Debug.WriteLine("AdministratorSettings::Create Setting2 = " + this.setting2);
            System.Diagnostics.Debug.WriteLine("AdministratorSettings::Create Setting3 = " + this.setting3);

            DatabaseManager.CloseConnection();
        }


        // Update a setting to the database
        private void Update(string columnName, string value)
        {
            DatabaseManager.OpenConnection();
            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            
            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "UPDATE administrator_settings SET " + columnName + " = :value WHERE id = :id";

            command.Parameters.Add("value", NpgsqlTypes.NpgsqlDbType.Text).Value = value;
            command.Parameters.Add("id", NpgsqlTypes.NpgsqlDbType.Integer).Value = base.ID;

            try
            {
                int enq = command.ExecuteNonQuery();
                System.Diagnostics.Debug.WriteLine("AdministratorSettings::Update ExecuteNonQuery Int = " + enq);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                System.Windows.MessageBox.Show(exception.ToString());
            }

            System.Diagnostics.Debug.WriteLine("AdministratorSettings::Update Column Name = " + columnName);
            System.Diagnostics.Debug.WriteLine("AdministratorSettings::Update Column Value = " + value);
            System.Diagnostics.Debug.WriteLine("AdministratorSettings::Update id = " + base.ID);

            DatabaseManager.CloseConnection();
        }


        // Delete settings from the database
        // DO NOT USE until the system supports deleting accounts from the database
        public void Delete(int administratorID)
        {
            DatabaseManager.OpenConnection();
            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "DELETE FROM administrator_settings WHERE administratorid = :administratorid AND id = :id";

            command.Parameters.Add("administratorid", NpgsqlTypes.NpgsqlDbType.Integer).Value = administratorID;
            command.Parameters.Add("id", NpgsqlTypes.NpgsqlDbType.Integer).Value = base.ID;

            try
            {
                int enq = command.ExecuteNonQuery();
                System.Diagnostics.Debug.WriteLine("AdministratorSettings::Update ExecuteNonQuery Int = " + enq);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                System.Windows.MessageBox.Show(exception.ToString());
            }

            System.Diagnostics.Debug.WriteLine("AdministratorSettings::Delete id = " + base.ID);
            System.Diagnostics.Debug.WriteLine("AdministratorSettings::Delete administratorID = " + administratorID);

            DatabaseManager.CloseConnection();
        }

        #endregion Methods

    } //End AdministratorSettings Class


} //End Namespace
