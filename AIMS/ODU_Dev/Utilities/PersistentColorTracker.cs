﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Media.Imaging;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Windows.Threading;
using System.Windows;
using System.Threading;

namespace AIMS.ODU_Dev.Utilities
{
    public class PersistentColorTracker : IDisposable
    {
        int width = 1920;
        int height = 1080;

        const int COEFFICIENT_OF_PROXIMITY = 5;    // How close the blobs can be to be considered encumbused.
        const int COEFFICIENT_OF_MOTION = 15;       // How much motion/noise must be detected in order to convey actual motion detected.
        const int MAXIMUM_OVERLAP_COUNT = 300;      // How many blobs can be encumbused within a single blob.
        const int MINIMUM_OVERLAP_COUNT = 5;        // How few blobs can be encumbused within a single blob.
        const int MAXIMUM_POINTS_STORED_COUNT = 300;    // How many points can be stored before needing to be cleared.
        const int STRETCH_FACTOR = 100;

        private int allCurrentBlobsContourInformationIndexer = 0;
        private List<ContourInformation> dummy = new List<ContourInformation>();
        private List<List<ContourInformation>> allPastBlobsContourInformation = new List<List<ContourInformation>>();

        private Bitmap imageToProcess;  // Bitmap of the image which is being processed. This allows it to be visually represented on the screen.
        private Image<Bgr, Byte> globalImage;   // Emgu.CV.Image object representing the raw data of the image.
        private BitmapSource bitmapSource;
        private MCvFont normalFont = new MCvFont(Emgu.CV.CvEnum.FONT.CV_FONT_HERSHEY_COMPLEX, 1.0, 1.0);    // Font used when writing on the image.

        private Stopwatch updateDelayStopwatch = new Stopwatch();
        private DispatcherTimer dispatcherTimer = new DispatcherTimer();

        private Dispatcher linkedDispatcher;
        /// <summary> 
        /// Allows the persistent color tracker to output it's processed image to a linked image control.. if it exists. 
        /// This can be null, and the PersistentColorTracker will simply run without any visual representation of its findings.
        /// </summary>
        private System.Windows.Controls.Image linkedImageControl;

        public Dispatcher LinkedDispatcher { get { return linkedDispatcher; } set { linkedDispatcher = value; } }
        public System.Windows.Controls.Image LinkedImageControl { get { return linkedImageControl; } set { linkedImageControl = value; } }

        public PersistentColorTracker()
        {
        }

        public void WipeClean()
        {
            this.StopTracking();
            this.ClearTrackedObjects();
        }

        public void StopTracking()
        {
            updateDelayStopwatch.Reset();

            if (this.dispatcherTimer != null)
            {
                this.dispatcherTimer.Stop();
                this.dispatcherTimer = null;
            }

            // Unplug from the AllDataReady event of the KinectManager.
            KinectManager.AllDataReady -= this.KinectManager_AllDataReady;
        }

        public void ClearTrackedObjects()
        {
            foreach (List<ContourInformation> list in this.allPastBlobsContourInformation)
            {
                if (list != null)
                {
                    foreach (ContourInformation contourInfo in list)
                    {
                        if (contourInfo != null)
                            contourInfo.WipeClean();
                    }
                }
            }
        }

        public void Dispose()
        {
            WipeClean();
        }

        /// <summary>
        /// An event handler which attaches into the KinectManager's AllDataReady event.
        /// The AllDataReady event triggers after each of the color, depth, and skeletal frames have been processed and stored.
        /// This is important because it gives us access to the stored mapped color/depth data.
        /// 
        /// In PersistentColorTracker, we use this event to update the current image on which we will do our frame processing on.
        /// </summary>
        private void KinectManager_AllDataReady()
        {
            Bitmap image = ImageToBitmap(KinectManager.MappedColorData, width, height, new Rect(0, 0, width, height));
            this.imageToProcess = image;
        }

        delegate void updateCallback(string tekst);

        private void OnTimedEvent(object source, EventArgs e)
        {
            UpdateElement(null);
        }

        private void UpdateElement(string tekst)
        {
            // Check to see if we can access the thread containing our linked output Image control.
            if (linkedImageControl != null && linkedImageControl.Dispatcher != null && !linkedImageControl.Dispatcher.CheckAccess())
            {
                // If we can't Access the thread of our linked output Image control, then put another call to this method into the queue to try again.
                if (this.linkedDispatcher != null)
                {
                    // we can only make the re-queue callback if the linkedDispatcher exists.
                    updateCallback uCallBack = new updateCallback(UpdateElement);
                    this.linkedDispatcher.Invoke(uCallBack, tekst);
                }
            }
            else
            {
                #region block is strictly for testing

                BlobInformation[] bucketOfBlobs = {
                    new BlobInformation(140, 181, 253, 255, "RED"),  
                    new BlobInformation(27,37, 152, 252, "YELLOW")
                    //new BlobInformation(140, 181, 253, 255, "RED"),     
                    //new BlobInformation(27, 30, 200, 252, "YELLOW"),
                    //new BlobInformation(95,	144, 251, 253, "BLUE")
                    //new BlobInformation(24, 84, 195, 210, "YELLOW")
                    };


                //Prerequisite for Persistence - start
                bucketOfBlobs[0].DETECT_MOTION_FILTER_FLAG = false;
                bucketOfBlobs[1].DETECT_MOTION_FILTER_FLAG = true;

                bucketOfBlobs[0].MAXIMUM_DEPTH_MILLIMETERS = 3219.2f;
                bucketOfBlobs[1].MAXIMUM_DEPTH_MILLIMETERS = 3219.2f;
                //Prerequisite for Persistence - end

                ProcessFrame(this.imageToProcess, bucketOfBlobs);
                #endregion
            }
        }

        public void StartTracking()
        {
            // Plug into the AllDataReady event of the KinectManager.
            KinectManager.AllDataReady += this.KinectManager_AllDataReady;

            updateDelayStopwatch.Start();

            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(OnTimedEvent);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 5);
            dispatcherTimer.Start();
        }

        public int getDepth(float x = 0, float y = 0)
        {
            int depthInMillimeters = this.getDepthInfo(x, y);

            int depthInches = (int)(depthInMillimeters * 0.0393700787);
            int depthFt = depthInches / 12;
            depthInches = depthInches % 12;

            return depthInMillimeters;
        }

        public int getDepthInfo(float x = 0, float y = 0)
        {
            if (KinectManager.DepthFrameData != null && KinectManager.DepthFrameData.Length > 0)
            {
                return KinectManager.GetDepthValue((int) x, (int) y);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Breaks up the contour into 4 quadrants, and checks the depth position at:
        /// 1. The center of the entire contour.
        /// 2. The center of each of the 4 quadrants of the contour.
        /// 
        /// The first non-zero depth value is out'ed in depthMillimeters.
        /// Additionally, all 5 depth values are also out'ed in the depthInfoArray.
        /// </summary>
        /// <param name="contours"></param>
        /// <param name="depthInfoArray"></param>
        /// <param name="depthMillimeters"></param>
        private void getDepth(Contour<System.Drawing.Point> contours, out float[] depthInfoArray, out float depthMillimeters)
        {
            //size 5 because of depth information is collected from center and center of 4 quadrants of the contour
            depthInfoArray = new float[5];

            int centerX = (contours.BoundingRectangle.Left + contours.BoundingRectangle.Right) / 2;
            int centerY = (contours.BoundingRectangle.Top + contours.BoundingRectangle.Bottom) / 2;

            int firstQuadrantCenterX = (contours.BoundingRectangle.Left + centerX) / 2;
            int firstQuadrantCenterY = (contours.BoundingRectangle.Top + centerY) / 2;

            int secondQuadrantCenterX = (contours.BoundingRectangle.Right + centerX) / 2;
            int thirdQuadrantCenterY = (contours.BoundingRectangle.Bottom + centerY) / 2;

            // It's a square, so X or Y values with match twice - no need to calculate each.

            float depthInMillimeters = 0;
            depthMillimeters = 0;

            depthInMillimeters = getDepth(centerX, centerY);
            depthInfoArray[0] = depthInMillimeters;

            depthInMillimeters = getDepth(firstQuadrantCenterX, firstQuadrantCenterY);
            depthInfoArray[1] = depthInMillimeters;

            depthInMillimeters = getDepth(secondQuadrantCenterX, firstQuadrantCenterY);
            depthInfoArray[2] = depthInMillimeters;

            depthInMillimeters = getDepth(firstQuadrantCenterX, thirdQuadrantCenterY);
            depthInfoArray[3] = depthInMillimeters;

            depthInMillimeters = getDepth(secondQuadrantCenterX, thirdQuadrantCenterY);
            depthInfoArray[4] = depthInMillimeters;

            // return the first non-0 depth value (if there is one) starting with the contour's center's depth.
            for (int i = 0; i < depthInfoArray.Length; i++)
            {
                if (depthInfoArray[i] > 0)
                {
                    depthMillimeters = depthInfoArray[i];
                    break;
                }
            }
        }

        public void ProcessFrame(Bitmap image, BlobInformation[] blobsToTrack, Boolean filterColorsFlag = true)
        {
            //ConsoleColor[] blobColorsForConsole is purely cosmetic and may be removed
            if (image == null || blobsToTrack.Length == 0)
            {
                return;
            }

            List<ContourInformation>[] allCurrentBlobsContourInformation = new List<ContourInformation>[blobsToTrack.Length];
            for (int i = 0; i < allCurrentBlobsContourInformation.Length; i++)
            {
                allCurrentBlobsContourInformation[i] = new List<ContourInformation>();
            }

            List<System.Drawing.Point> itemsFoundList = new List<System.Drawing.Point>();

            Image<Bgr, Byte> originalImage = new Image<Bgr, byte>(image);
            Image<Gray, Byte> canny;
            Image<Gray, Byte> frame1 = null;
            Image<Gray, Byte> smoothImg = null;
            globalImage = new Image<Bgr, byte>(image);

            int commonArraySize0 = blobsToTrack.Length;
            //allPreviousPositionTime = new int[commonArraySize0];

            for (int x = 0; x < blobsToTrack.Length; x++)
            {
                if (filterColorsFlag)
                {
                    #region filter out unwanted colors

                    Image<Hsv, Byte> hsvimg = globalImage.Convert<Hsv, Byte>();
                    Image<Gray, Byte>[] channels = hsvimg.Split();
                    Image<Gray, Byte> imghue = channels[0];
                    Image<Gray, Byte> imgval = channels[2];
                    Image<Gray, Byte> imgsat = channels[1];

                    //filter out all but "the color you want
                    Image<Gray, byte> huefilter = imghue.InRange(new Gray(blobsToTrack[x].MINIMUM_HUE), new Gray(blobsToTrack[x].MAXIMUM_HUE));
                    Image<Gray, byte> satfilter = imgsat.InRange(new Gray(blobsToTrack[x].MINIMUM_SATURATION), new Gray(blobsToTrack[x].MAXIMUM_SATURATION));
                    //huefilter = huefilter.SmoothGaussian(17);                                                            
                    //now and the two to get the parts of the imaged that are colored and above some brightness.
                    Image<Gray, byte> detimg = huefilter.And(satfilter);
                    smoothImg = detimg.SmoothGaussian(5, 5, 1.5, 1.5);
                    canny = smoothImg.Canny(new Gray(0).Intensity, new Gray(255).Intensity);

                    #endregion
                }
                else
                {
                    frame1 = globalImage.Convert<Gray, Byte>();
                    //find Canny edges 
                    canny = frame1.Canny(new Gray(255).Intensity, new Gray(255).Intensity);
                }

                int itemCount = 0;
                Contour<System.Drawing.Point> contours = canny.FindContours();
                System.Drawing.Point coordinate = new System.Drawing.Point();

                for (; contours != null; contours = contours.HNext)
                {
                    #region filter out objects less than size 8
                    if (contours.Area > blobsToTrack[x].MAXIMUM_CONTOUR_AREA)
                    {

                        float[] depthInfoArray;
                        float depthMillimeters;

                        getDepth(contours, out depthInfoArray, out depthMillimeters);


                        #region filter out objects out of range
                        if (blobsToTrack[x].DEPTH_RANGE_FILTER_FLAG)
                        {
                            if (depthMillimeters != 0)
                            {
                                if (depthMillimeters < blobsToTrack[x].MAXIMUM_DEPTH_MILLIMETERS)
                                {
                                    //Console.ForegroundColor = blobColorsForConsole[x];
                                    coordinate = new System.Drawing.Point(contours.BoundingRectangle.X, contours.BoundingRectangle.Y);
                                    itemsFoundList.Add(coordinate);
                                    //canny.Draw(contours.BoundingRectangle, new Gray(255), 1);
                                    //globalImage.Draw(contours.BoundingRectangle, new Bgr(), 1);

                                    //prerequisite for motion tracking
                                    if (blobsToTrack[x].DETECT_MOTION_FILTER_FLAG)
                                    {
                                        //transfer Contour data to allRedBlobsContourInformation
                                        ContourInformation temp = new ContourInformation(
                                                                                            contours.BoundingRectangle.Location.X,
                                                                                            contours.BoundingRectangle.Location.Y,
                                                                                            contours.BoundingRectangle.Width,
                                                                                            contours.BoundingRectangle.Height,
                                                                                            blobsToTrack[x].BLOB_COLOR
                                                                                        );

                                        allCurrentBlobsContourInformation[x].Add(temp);

                                        //to draw the smaller bounding boxes
                                        //globalImage.Draw(new System.Drawing.Rectangle(contours.BoundingRectangle.Location.X, contours.BoundingRectangle.Location.Y, contours.BoundingRectangle.Width, contours.BoundingRectangle.Height), new Bgr(0, 255, 102), 1);

                                        itemCount++;
                                    }


                                }
                            }

                        }
                        #endregion
                    }
                    #endregion
                }

                summarizeCloseContours2(ref allCurrentBlobsContourInformation);

                //to ensure allCurrentBlobsContourInformation.Length = allPastBlobsContourInformation.Count
                if (allPastBlobsContourInformation.Count < allCurrentBlobsContourInformation.Length)
                {
                    for (int i = 0; i < allCurrentBlobsContourInformation.Length; i++)
                    {
                        allPastBlobsContourInformation.Add(dummy);
                    }
                }

                //Console.WriteLine(allPastBlobsContourInformation.Count);
                updatePreviousXY(ref allPastBlobsContourInformation);

                #region test for persistence
                //reset tracked flags

                for (int i = 0; i < allPastBlobsContourInformation.Count; i++)
                {
                    for (int j = 0; j < allPastBlobsContourInformation[i].Count; j++)
                    {
                        if (allPastBlobsContourInformation[i][j] != null)
                        {
                            allPastBlobsContourInformation[i][j].IS_TRACKED_FLAG = false;
                            allPastBlobsContourInformation[i][j].IS_MOVING_FLAG = false;
                        }
                    }
                }

                //for motion detection

                //the order of these function calls is crucial since OVERLAP_COUNT may be changes by testForPersistence()
                initializePreviousOVERLAP_COUNT(ref allPastBlobsContourInformation);
                testForPersistence(ref allCurrentBlobsContourInformation, ref allPastBlobsContourInformation);
                #endregion

                #region test motion/tagging

                for (int i = 0; i < allPastBlobsContourInformation.Count; i++)
                {
                    for (int j = 0; j < allPastBlobsContourInformation[i].Count; j++)
                    {
                        if (allPastBlobsContourInformation[i][j] != null)
                        {
                            //comment this if statement to test clean up
                            if (allPastBlobsContourInformation[i][j].IS_TRACKED_FLAG)
                            {

                                globalImage.Draw(new System.Drawing.Rectangle(allPastBlobsContourInformation[i][j].X, allPastBlobsContourInformation[i][j].Y, allPastBlobsContourInformation[i][j].Width, allPastBlobsContourInformation[i][j].Height), new Bgr(0, 255, 0), 2);
                                //globalImage.Draw("TAG/OC: " + allPastBlobsContourInformation[i][j].CONTOUR_IDENTITY.COLOR_INDEX + "," + allPastBlobsContourInformation[i][j].CONTOUR_IDENTITY.CONTOUR_ID + "/" + allPastBlobsContourInformation[i][j].OVERLAP_COUNT, ref normalFont, new System.Drawing.Point(allPastBlobsContourInformation[i][j].X, allPastBlobsContourInformation[i][j].Y-30), new Bgr(79, 255, 49));
                                globalImage.Draw("TAG: " + allPastBlobsContourInformation[i][j].CONTOUR_IDENTITY.CONTOUR_ID, ref normalFont, new System.Drawing.Point(allPastBlobsContourInformation[i][j].X, allPastBlobsContourInformation[i][j].Y), new Bgr(50, 0, 255));
                                //globalImage.Draw("OC: " + allPastBlobsContourInformation[i][j].OVERLAP_COUNT, ref normalFont, new System.Drawing.Point(allPastBlobsContourInformation[i][j].X, allPastBlobsContourInformation[i][j].Y), new Bgr(79, 255, 49));
                                //globalImage.Draw("Life: " + allPastBlobsContourInformation[i][j].LIFE, ref normalFont, new System.Drawing.Point(allPastBlobsContourInformation[i][j].X, allPastBlobsContourInformation[i][j].Y), new Bgr(79, 255, 49));

                                //globalImage.Draw("X/pX: " + allPastBlobsContourInformation[i][j].X + "/" + allPastBlobsContourInformation[i][j].PREVIOUS_X, ref normalFont, new System.Drawing.Point(allPastBlobsContourInformation[i][j].X, allPastBlobsContourInformation[i][j].Y), new Bgr(79, 255, 49));
                                //globalImage.Draw("X,Y: " + allPastBlobsContourInformation[i][j].X + "," + allPastBlobsContourInformation[i][j].Y, ref normalFont, new System.Drawing.Point(allPastBlobsContourInformation[i][j].X, allPastBlobsContourInformation[i][j].Y), new Bgr(79, 255, 49));
                                //globalImage.Draw("pY: " + allPastBlobsContourInformation[i][j].PREVIOUS_Y, ref normalFont, new System.Drawing.Point(allPastBlobsContourInformation[i][j].X, allPastBlobsContourInformation[i][j].Y), new Bgr(79, 255, 49));
                                //globalImage.Draw("Y: " + allPastBlobsContourInformation[i][j].Y, ref normalFont, new System.Drawing.Point(allPastBlobsContourInformation[i][j].X, allPastBlobsContourInformation[i][j].Y), new Bgr(79, 255, 49));
                                //globalImage.Draw("Mem: " + allPastBlobsContourInformation[i][j].IS_GROUP_MEMBER, ref normalFont, new System.Drawing.Point(allPastBlobsContourInformation[i][j].X, allPastBlobsContourInformation[i][j].Y), new Bgr(79, 255, 49));
                                //globalImage.Draw("Vel: " + allPastBlobsContourInformation[i][j].VELOCITY, ref normalFont, new System.Drawing.Point(allPastBlobsContourInformation[i][j].X, allPastBlobsContourInformation[i][j].Y), new Bgr(79, 255, 49));
                                //globalImage.Draw("TAG/Life: " + allPastBlobsContourInformation[i][j].CONTOUR_IDENTITY.CONTOUR_ID + "/" + allPastBlobsContourInformation[i][j].LIFE, ref normalFont, new System.Drawing.Point(allPastBlobsContourInformation[i][j].X, allPastBlobsContourInformation[i][j].Y), new Bgr(50, 0, 255));
                                //globalImage.Draw("Storage: " + allPastBlobsContourInformation[i][j].STORAGE, ref normalFont, new System.Drawing.Point(allPastBlobsContourInformation[i][j].X, allPastBlobsContourInformation[i][j].Y), new Bgr(50, 0, 255));

                                List<ContourInformation> PastColorBlobsContourInformation = allPastBlobsContourInformation[i];
                                checkForMotion(ref PastColorBlobsContourInformation);
                                allPastBlobsContourInformation[i] = PastColorBlobsContourInformation;

                                bool showPathAlways = true;

                                if (showPathAlways)
                                {
                                    System.Drawing.Point[] previousPoints = ContourInformation.toPointArray(allPastBlobsContourInformation[i][j].PREVIOUS_XY);

                                    if (previousPoints != null)
                                    {
                                        globalImage.DrawPolyline(previousPoints, false, new Bgr(0, 255, 255), 1);
                                    }
                                }
                                /*else */
                                if (allPastBlobsContourInformation[i][j].IS_MOVING_FLAG)
                                {
                                    if (allPastBlobsContourInformation[i][j].OVERLAP_COUNT > MINIMUM_OVERLAP_COUNT)
                                    {
                                        globalImage.Draw("MOTION", ref normalFont, new System.Drawing.Point(allPastBlobsContourInformation[i][j].X, allPastBlobsContourInformation[i][j].Y - 25), new Bgr(0, 0, 255));

                                        System.Drawing.Point point0 = new System.Drawing.Point(allPastBlobsContourInformation[i][j].X, allPastBlobsContourInformation[i][j].Y);
                                        System.Drawing.Point point1 = new System.Drawing.Point(allPastBlobsContourInformation[i][j].PREVIOUS_X, allPastBlobsContourInformation[i][j].PREVIOUS_Y);
                                        globalImage.Draw(new LineSegment2D(point0, point1), new Bgr(0, 255, 255), 1);

                                        System.Drawing.Point[] previousPoints = ContourInformation.toPointArray(allPastBlobsContourInformation[i][j].PREVIOUS_XY);

                                        if (previousPoints != null)
                                        {
                                            globalImage.DrawPolyline(previousPoints, false, new Bgr(0, 255, 255), 1);
                                        }

                                        //Console.WriteLine("vel: " + allPastBlobsContourInformation[i][j].VELOCITY);
                                        //Console.Beep(4000, 25);
                                    }
                                }

                            }
                        }
                    }
                }

                deleteObsoletePastContours(ref allPastBlobsContourInformation);

                #endregion

                #region testing multiple color generalization

                for (int i = 0; i < allCurrentBlobsContourInformation.Length; i++)
                {
                    if (allCurrentBlobsContourInformation[i] != null)
                    {
                        for (int j = 0; j < allCurrentBlobsContourInformation[i].Count; j++)
                        {
                            if (allCurrentBlobsContourInformation[i][j] != null)
                            {
                                //Console.WriteLine("itemCount " + itemCount + ", " + allCurrentBlobsContourInformation[i][j].NAME_OF_COLOR + " Count: " + allCurrentBlobsContourInformation[i].Count);
                                globalImage.Draw(new System.Drawing.Rectangle(allCurrentBlobsContourInformation[i][j].X, allCurrentBlobsContourInformation[i][j].Y, allCurrentBlobsContourInformation[i][j].Width, allCurrentBlobsContourInformation[i][j].Height), new Bgr(250, 0, 0), 1);
                                //globalImage.Draw("ID: " + allCurrentBlobsContourInformation[i][j].CONTOUR_ID.COLOR_ID, ref normalFont, new System.Drawing.Point(allCurrentBlobsContourInformation[i][j].X, allCurrentBlobsContourInformation[i][j].Y), new Bgr(79, 255, 49));
                                //Console.WriteLine(j + "ID: " + allCurrentBlobsContourInformation[i][j].CONTOUR_ID.COLOR_ID);
                            }
                        }
                    }
                }

                #endregion
            }

            //bitmapSource = ToBitmapSource(globalImage);//i = ToBitmapSource(canny);
            bitmapSource = ToBitmapSource2(globalImage);//i = ToBitmapSource(canny);

            if (bitmapSource != null)
            {
                if (linkedImageControl != null)
                {
                    linkedImageControl.Source = bitmapSource;
                }
            }
        }

        public void updatePreviousXY(ref List<List<ContourInformation>> allPastBlobsContourInformation)
        {

            if (updateDelayStopwatch.Elapsed.Milliseconds % ContourInformation.DELAY_IN_MILLISECONDS == 0)
            {
                for (int i = 0; i < allPastBlobsContourInformation.Count; i++)
                {
                    for (int j = 0; j < allPastBlobsContourInformation[i].Count; j++)
                    {
                        if (allPastBlobsContourInformation[i][j] != null)
                        {
                            //remove later
                            allPastBlobsContourInformation[i][j].PREVIOUS_X = allPastBlobsContourInformation[i][j].X;
                            allPastBlobsContourInformation[i][j].PREVIOUS_Y = allPastBlobsContourInformation[i][j].Y;

                            allPastBlobsContourInformation[i][j].PREVIOUS_TIME_MILLISECONDS = updateDelayStopwatch.Elapsed.Milliseconds;
                            //if a different decay rate time (milliseconds) is needed, remove next line from here and use same if condition with a different constant
                            allPastBlobsContourInformation[i][j].LIFE -= allPastBlobsContourInformation[i][j].CONTOUR_DECAY_GRADIENT;
                        }
                    }
                }
            }

        }

        public void initializePreviousOVERLAP_COUNT(ref List<List<ContourInformation>> allPastBlobsContourInformation)
        {
            for (int i = 0; i < allPastBlobsContourInformation.Count; i++)
            {
                for (int j = 0; j < allPastBlobsContourInformation[i].Count; j++)
                {
                    if (allPastBlobsContourInformation[i][j] != null)
                    {
                        allPastBlobsContourInformation[i][j].PREVIOUS_OVERLAP_COUNT = allPastBlobsContourInformation[i][j].OVERLAP_COUNT;
                    }
                }
            }
        }

        public void deleteObsoletePastContours(ref List<List<ContourInformation>> allPastBlobsContourInformation)
        {

            for (int i = 0; i < allPastBlobsContourInformation.Count; i++)
            {
                for (int j = 0; j < allPastBlobsContourInformation[i].Count; j++)
                {
                    if (allPastBlobsContourInformation[i][j] != null)
                    {
                        if (allPastBlobsContourInformation[i][j].OVERLAP_COUNT == allPastBlobsContourInformation[i][j].PREVIOUS_OVERLAP_COUNT && allPastBlobsContourInformation[i][j].IS_TRACKED_FLAG == false)
                        {
                            if (allPastBlobsContourInformation[i][j].LIFE <= allPastBlobsContourInformation[i][j].MINIMUM_CONTOUR_LIFE)
                            {
                                allPastBlobsContourInformation[i].Remove(allPastBlobsContourInformation[i][j]);
                            }
                        }
                    }

                }
            }


        }

        public void tagContours(ref List<List<ContourInformation>> listOfListOfContourInfo, int Xref = 0, int Yref = 0)
        {

            List<List<int>> differences = new List<List<int>>();


            for (int i = 0; i < listOfListOfContourInfo.Count; i++)
            {

                differences.Add(new List<int>());


                for (int j = 0; j < listOfListOfContourInfo[i].Count; j++)
                {
                    //System.Drawing.Point p0 = new System.Drawing.Point(listOfListOfContourInfo[i][j].X, listOfListOfContourInfo[i][j].Y);
                    //System.Drawing.Point p1 = new System.Drawing.Point(Xref, Yref);

                    //int temp = (int)distanceBetween2Points(p0, p1);

                    if (listOfListOfContourInfo[i][j] != null)
                    {
                        int temp = listOfListOfContourInfo[i][j].X;
                        listOfListOfContourInfo[i][j].STORAGE = temp;
                        differences[i].Add(temp);

                    }

                }
            }

            //implicit is the fact that differences.Count = listOfListOfPastContourInfo.Count
            for (int i = 0; i < differences.Count; i++)
            {
                differences[i].Sort();
                for (int j = 0; j < differences[i].Count; j++)
                {
                    int index = differences[i].IndexOf(listOfListOfContourInfo[i][j].STORAGE);

                    listOfListOfContourInfo[i][j].CONTOUR_IDENTITY = new ContourInformation.ID(i, index);
                }
            }




        }

        public void testForPersistence(ref List<ContourInformation>[] allCurrentBlobsContourInformation, ref List<List<ContourInformation>> allPastBlobsContourInformation)
        {

            //note that allPastBlobsContourInformation is global, thus preserved beyond here
            //precondition: allCurrentBlobsContourInformation.Length = allPastBlobsContourInformation.Count


            //check if the number of objects available match that which was set to track. For example track 2 red objects exclusively
            for (int x = 0; x < allCurrentBlobsContourInformation.Length; x++)
            {
                if (allCurrentBlobsContourInformation[x].Count > 0)
                {
                    if (allCurrentBlobsContourInformation[x][0].MAXIMUM_NUMBER_OF_OBJECTS_TO_TRACK == allCurrentBlobsContourInformation[x].Count)
                    {

                        List<ContourInformation> sortedallCurrentBlobsContourInformation = new List<ContourInformation>();
                        List<ContourInformation> sortedAllPastColorBlobsContourInformation = new List<ContourInformation>();
                        Console.WriteLine();

                        for (int i = 0; i < allCurrentBlobsContourInformation.Length; i++)
                        {
                            if (allCurrentBlobsContourInformation[i].Count > allPastBlobsContourInformation[i].Count)
                            {
                                #region Case 1

                                try
                                {

                                    sortContourInfo(allCurrentBlobsContourInformation[i], out sortedallCurrentBlobsContourInformation);
                                    sortContourInfo(allPastBlobsContourInformation[i], out sortedAllPastColorBlobsContourInformation);
                                    allPastBlobsContourInformation[i] = sortedAllPastColorBlobsContourInformation;

                                    int j;
                                    for (j = 0; j < sortedAllPastColorBlobsContourInformation.Count; j++)
                                    {

                                        //ensure tag is unique
                                        if (allPastBlobsContourInformation[i][j].IS_TAGGED_FLAG == false)
                                        {
                                            //Console.Beep(37,25);
                                            int contourID = (int)updateDelayStopwatch.ElapsedMilliseconds;// / 1000;
                                            allPastBlobsContourInformation[i][j].CONTOUR_IDENTITY = new ContourInformation.ID(i, contourID);
                                            allPastBlobsContourInformation[i][j].IS_TAGGED_FLAG = true;
                                        }

                                        allPastBlobsContourInformation[i][j].IS_TRACKED_FLAG = true;
                                        allPastBlobsContourInformation[i][j].OVERLAP_COUNT++;

                                        if (allPastBlobsContourInformation[i][j].OVERLAP_COUNT > MAXIMUM_OVERLAP_COUNT)
                                        {
                                            allPastBlobsContourInformation[i][j].OVERLAP_COUNT = MINIMUM_OVERLAP_COUNT;
                                        }


                                        allPastBlobsContourInformation[i][j].resetLife();

                                        allPastBlobsContourInformation[i][j].X = sortedallCurrentBlobsContourInformation[j].X;
                                        allPastBlobsContourInformation[i][j].Y = sortedallCurrentBlobsContourInformation[j].Y;
                                        //note allPastBlobsContourInformation[i][j].PREVIOUS_XY grows fast
                                        allPastBlobsContourInformation[i][j].PREVIOUS_XY.Add(new System.Drawing.Point(allPastBlobsContourInformation[i][j].X, allPastBlobsContourInformation[i][j].Y));
                                    }

                                    while (j < allCurrentBlobsContourInformation[i].Count)
                                    {
                                        allPastBlobsContourInformation[i].Add(sortedallCurrentBlobsContourInformation[j]);
                                        j++;
                                    }


                                }
                                catch (Exception ex)
                                {
                                }


                                #endregion
                            }
                            else if (allCurrentBlobsContourInformation[i].Count == allPastBlobsContourInformation[i].Count)
                            {

                                #region Case 2

                                try
                                {
                                    ////careful, note that when j is used this line should follow the sortContourInfo(): allCurrentBlobsContourInformation[i] = sortedallCurrentBlobsContourInformation for example
                                    sortContourInfo(allCurrentBlobsContourInformation[i], out sortedallCurrentBlobsContourInformation);
                                    sortContourInfo(allPastBlobsContourInformation[i], out sortedAllPastColorBlobsContourInformation);
                                    allPastBlobsContourInformation[i] = sortedAllPastColorBlobsContourInformation;

                                    for (int j = 0; j < sortedallCurrentBlobsContourInformation.Count; j++)
                                    {

                                        //ensure tag is unique
                                        if (allPastBlobsContourInformation[i][j].IS_TAGGED_FLAG == false)
                                        {
                                            //Console.Beep(37,25);
                                            int contourID = (int)updateDelayStopwatch.ElapsedMilliseconds;// / 1000;
                                            allPastBlobsContourInformation[i][j].CONTOUR_IDENTITY = new ContourInformation.ID(i, contourID);
                                            allPastBlobsContourInformation[i][j].IS_TAGGED_FLAG = true;
                                        }


                                        allPastBlobsContourInformation[i][j].IS_TRACKED_FLAG = true;
                                        allPastBlobsContourInformation[i][j].OVERLAP_COUNT++;

                                        if (allPastBlobsContourInformation[i][j].OVERLAP_COUNT > MAXIMUM_OVERLAP_COUNT)
                                        {
                                            allPastBlobsContourInformation[i][j].OVERLAP_COUNT = MINIMUM_OVERLAP_COUNT;
                                        }


                                        allPastBlobsContourInformation[i][j].resetLife();


                                        //because null exception can be thrown here

                                        allPastBlobsContourInformation[i][j].X = sortedallCurrentBlobsContourInformation[j].X;
                                        allPastBlobsContourInformation[i][j].Y = sortedallCurrentBlobsContourInformation[j].Y;
                                        //note allPastBlobsContourInformation[i][j].PREVIOUS_XY grows fast
                                        allPastBlobsContourInformation[i][j].PREVIOUS_XY.Add(new System.Drawing.Point(allPastBlobsContourInformation[i][j].X, allPastBlobsContourInformation[i][j].Y));

                                    }
                                }
                                catch (Exception ex)
                                {
                                }

                                #endregion
                            }
                            else
                            {
                                //allCurrentBlobsContourInformation[i].Count < allPastBlobsContourInformation[i].Count
                                #region Case 3
                                //try
                                //{

                                //    sortContourInfo(allCurrentBlobsContourInformation[i], out sortedallCurrentBlobsContourInformation);
                                //    sortContourInfo(allPastBlobsContourInformation[i], out sortedAllPastColorBlobsContourInformation);
                                //    allPastBlobsContourInformation[i] = sortedAllPastColorBlobsContourInformation;

                                //    for (int j = 0; j < allCurrentBlobsContourInformation[i].Count; j++)
                                //    {
                                //        if (allPastBlobsContourInformation[i][j].IS_TAGGED_FLAG == false)
                                //        {
                                //            Console.Beep();
                                //            int contourID = (int)updateDelayStopwatch.ElapsedMilliseconds;// / 1000;
                                //            allPastBlobsContourInformation[i][j].CONTOUR_IDENTITY = new ContourInformation.ID(i, contourID);
                                //            allPastBlobsContourInformation[i][j].IS_TAGGED_FLAG = true;
                                //        }

                                //        allPastBlobsContourInformation[i][j].IS_TRACKED_FLAG = true;
                                //        allPastBlobsContourInformation[i][j].OVERLAP_COUNT++;

                                //        if (allPastBlobsContourInformation[i][j].OVERLAP_COUNT > MAXIMUM_OVERLAP_COUNT)
                                //        {
                                //            allPastBlobsContourInformation[i][j].OVERLAP_COUNT = MINIMUM_OVERLAP_COUNT;
                                //        }


                                //        allPastBlobsContourInformation[i][j].resetLife();

                                //        allPastBlobsContourInformation[i][j].X = allCurrentBlobsContourInformation[i][j].X;
                                //        allPastBlobsContourInformation[i][j].Y = allCurrentBlobsContourInformation[i][j].Y;
                                //    }




                                //}
                                //catch (Exception ex)
                                //{
                                //}


                                #endregion
                            }




                        }


                    }
                }
            }


        }

        public void sortContourInfo(List<ContourInformation>[] arrayOfListOfContourInfo, out List<ContourInformation>[] sortedArrayOfListOfContourInfo, int Xref = 0, int Yref = 0)
        {
            ContourInformation[][] tempResult = new ContourInformation[arrayOfListOfContourInfo.Length][];
            List<int>[] differences = new List<int>[arrayOfListOfContourInfo.Length];
            sortedArrayOfListOfContourInfo = new List<ContourInformation>[arrayOfListOfContourInfo.Length];

            try
            {
                for (int i = 0; i < arrayOfListOfContourInfo.Length; i++)
                {
                    sortedArrayOfListOfContourInfo[i] = new List<ContourInformation>();
                    differences[i] = new List<int>();
                    tempResult[i] = new ContourInformation[arrayOfListOfContourInfo[i].Count];
                    for (int j = 0; j < arrayOfListOfContourInfo[i].Count; j++)
                    {

                        //System.Drawing.Point p0 = new System.Drawing.Point(arrayOfListOfContourInfo[i][j].X, arrayOfListOfContourInfo[i][j].Y);
                        //System.Drawing.Point p1 = new System.Drawing.Point(Xref, Yref);

                        //int temp = (int)distanceBetween2Points(p0, p1);

                        int temp;
                        if (arrayOfListOfContourInfo[i][j] != null)
                        {
                            temp = arrayOfListOfContourInfo[i][j].X;
                        }
                        else
                        {
                            temp = -1;
                        }

                        arrayOfListOfContourInfo[i][j].STORAGE = temp;
                        differences[i].Add(temp);

                    }
                }


                for (int i = 0; i < arrayOfListOfContourInfo.Length; i++)
                {
                    differences[i].Sort();

                    for (int j = 0; j < arrayOfListOfContourInfo[i].Count; j++)
                    {
                        int index = differences[i].IndexOf(arrayOfListOfContourInfo[i][j].STORAGE);
                        tempResult[i][index] = arrayOfListOfContourInfo[i][j];
                    }
                }

                for (int i = 0; i < tempResult.Length; i++)
                {
                    for (int j = 0; j < tempResult[i].Length; j++)
                    {
                        sortedArrayOfListOfContourInfo[i].Add(tempResult[i][j]);

                    }
                }
            }
            catch (Exception ex)
            {
            }

        }

        public void sortContourInfo(List<ContourInformation> arrayOfListOfContourInfo, out List<ContourInformation> sortedArrayOfListOfContourInfo, int startFrom = 0, int Xref = 0, int Yref = 0)
        {
            ContourInformation[] tempResult = new ContourInformation[arrayOfListOfContourInfo.Count];
            List<int> differences = new List<int>();
            sortedArrayOfListOfContourInfo = new List<ContourInformation>();

            try
            {
                for (int i = startFrom; i < arrayOfListOfContourInfo.Count; i++)
                {


                    //System.Drawing.Point p0 = new System.Drawing.Point(arrayOfListOfContourInfo[i].X, arrayOfListOfContourInfo[i].Y);
                    //System.Drawing.Point p1 = new System.Drawing.Point(Xref, Yref);

                    //int temp = (int)distanceBetween2Points(p0, p1);

                    int temp;
                    if (arrayOfListOfContourInfo[i] != null)
                    {
                        temp = arrayOfListOfContourInfo[i].X;
                    }
                    else
                    {
                        temp = -1;
                    }

                    arrayOfListOfContourInfo[i].STORAGE = temp;
                    differences.Add(temp);



                }

                differences.Sort();

                for (int i = 0; i < arrayOfListOfContourInfo.Count; i++)
                {

                    int index = differences.IndexOf(arrayOfListOfContourInfo[i].STORAGE);
                    //this happens when an item which was not part of the sort is considered (because it was considered previously)
                    if (index == -1)
                    {
                        index = i;
                    }
                    tempResult[index] = arrayOfListOfContourInfo[i];

                }

                for (int i = 0; i < tempResult.Length; i++)
                {
                    sortedArrayOfListOfContourInfo.Add(tempResult[i]);
                }
            }
            catch (Exception ex)
            {
            }

        }

        public void sortContourInfo(List<List<ContourInformation>> listOfListOfPastContourInfo, out List<List<ContourInformation>> sortedListOfListOfPastContourInfo, int Xref = 0, int Yref = 0)
        {
            List<List<int>> differences = new List<List<int>>();
            sortedListOfListOfPastContourInfo = new List<List<ContourInformation>>();
            ContourInformation[][] tempResult = new ContourInformation[listOfListOfPastContourInfo.Count][];

            try
            {

                for (int i = 0; i < listOfListOfPastContourInfo.Count; i++)
                {
                    tempResult[i] = new ContourInformation[listOfListOfPastContourInfo[i].Count];
                    differences.Add(new List<int>());
                    sortedListOfListOfPastContourInfo.Add(new List<ContourInformation>());

                    for (int j = 0; j < listOfListOfPastContourInfo[i].Count; j++)
                    {


                        //System.Drawing.Point p0 = new System.Drawing.Point(listOfListOfPastContourInfo[i][j].X, listOfListOfPastContourInfo[i][j].Y);
                        //System.Drawing.Point p1 = new System.Drawing.Point(Xref, Yref);

                        //int temp = (int)distanceBetween2Points(p0, p1);

                        int temp;
                        if (listOfListOfPastContourInfo[i][j] != null)
                        {
                            temp = listOfListOfPastContourInfo[i][j].X;
                        }
                        else
                        {
                            temp = -1;
                        }


                        listOfListOfPastContourInfo[i][j].STORAGE = temp;
                        differences[i].Add(temp);


                    }
                }

                //implicit is the fact that differences.Count = listOfListOfPastContourInfo.Count
                for (int i = 0; i < differences.Count; i++)
                {
                    differences[i].Sort();
                    for (int j = 0; j < differences[i].Count; j++)
                    {
                        int index = differences[i].IndexOf(listOfListOfPastContourInfo[i][j].STORAGE);
                        tempResult[i][index] = listOfListOfPastContourInfo[i][j];
                    }
                }


                for (int i = 0; i < tempResult.Length; i++)
                {
                    for (int j = 0; j < tempResult[i].Length; j++)
                    {
                        sortedListOfListOfPastContourInfo[i].Add(tempResult[i][j]);
                    }
                }
            }
            catch (Exception ex)
            {

            }


        }

        public void checkForMotion(ref List<ContourInformation> allPastBlobsContourInformation)
        {
            for (int i = 0; i < allPastBlobsContourInformation.Count; i++)
            {
                if (allPastBlobsContourInformation[i] != null)
                {

                    if (allPastBlobsContourInformation[i].PREVIOUS_X + allPastBlobsContourInformation[i].PREVIOUS_Y != 0)
                    {

                        System.Drawing.Point pointA = new System.Drawing.Point(allPastBlobsContourInformation[i].PREVIOUS_X, allPastBlobsContourInformation[i].PREVIOUS_Y);
                        System.Drawing.Point pointB = new System.Drawing.Point(allPastBlobsContourInformation[i].X, allPastBlobsContourInformation[i].Y);
                        double distanceBetweenPreviousAndPresentPosition = distanceBetween2Points(pointA, pointB);


                        //globalImage.Draw("distance: " + distanceBetweenPreviousAndPresentPosition, ref normalFont, new System.Drawing.Point(allPastBlobsContourInformation[i].X, allPastBlobsContourInformation[i].Y - 30), new Bgr(0, 0, 255));
                        if (distanceBetweenPreviousAndPresentPosition > COEFFICIENT_OF_MOTION)
                        {
                            allPastBlobsContourInformation[i].IS_MOVING_FLAG = true;
                            int presentPositionTime = updateDelayStopwatch.Elapsed.Milliseconds;

                            allPastBlobsContourInformation[i].VELOCITY = (float)distanceBetweenPreviousAndPresentPosition / System.Math.Abs(presentPositionTime - allPastBlobsContourInformation[i].PREVIOUS_TIME_MILLISECONDS);


                            //globalImage.Draw("MOTION", ref normalFont, new System.Drawing.Point(allPastYellowBlobsContourInformation[i].X, allPastYellowBlobsContourInformation[i].Y-25), new Bgr(0, 0, 255));
                            //Console.Beep(4000, 25);
                            //Console.WriteLine("diff: " + distanceBetween2Points(pointA, pointB));
                        }
                    }
                }
            }
        }

        //old version checkForMotion()
        //public void checkForMotion(ref List<ContourInformation>[] contourInformationOfAllColors)
        //{


        //    MCvFont f = new MCvFont(FONT.CV_FONT_HERSHEY_COMPLEX, 1.5, 1.5);
        //    //iterate through all colors in the contourInformationOfAllColors
        //    for (int c = 0; c < contourInformationOfAllColors.Length; c++)
        //    {
        //        if (contourInformationOfAllColors[c] != null)
        //        {

        //            for (int i = 0; i < contourInformationOfAllColors[c].Count; i++)
        //            {
        //                //Part 1
        //                //detected object
        //                //Console.WriteLine("Before Objects Count: " + itemCount + ", Objects Count: " + qualifiedYellowBlobsContourInformation.Count);
        //                globalImage.Draw(new System.Drawing.Rectangle(contourInformationOfAllColors[c][i].X, contourInformationOfAllColors[c][i].Y, contourInformationOfAllColors[c][i].Width, contourInformationOfAllColors[c][i].Height), new Bgr(250, 0, 0), 1);
        //                //img.Draw("Object " + i + ":" + contourInformationOfAllColors[c][i].X + "," + contourInformationOfAllColors[c][i].Y, ref f, new System.Drawing.Point(contourInformationOfAllColors[c][i].X, contourInformationOfAllColors[c][i].Y), new Bgr(79, 79, 49));



        //                //Part 2
        //                System.Drawing.Point presentPosition =
        //                   new System.Drawing.Point(
        //                                               contourInformationOfAllColors[c][i].X,
        //                                               contourInformationOfAllColors[c][i].Y
        //                                           );

        //                LineSegment2D line = new LineSegment2D(previousPosition, presentPosition);
        //                int presentPositionTime = updateDelayStopwatch.Elapsed.Milliseconds;

        //                if (distanceBetween2Points(previousPosition, presentPosition) >= COEFFICIENT_OF_PROXIMITY + COEFFICIENT_OF_PROXIMITY_TOLERANCE)
        //                {
        //                    try
        //                    {
        //                        float velocity = (float)(distanceBetween2Points(previousPosition, presentPosition) / Math.Abs(presentPositionTime - allPreviousPositionTime[c]));
        //                        //this velocity is used to filter out possible outliers, since they are sporadic, they tend to have high velocities
        //                        if (velocity <= COEFFICIENT_OF_MOTION)
        //                        {
        //                            allVelocityCount[c]++;

        //                            //motion detected object
        //                            globalImage.Draw("Motion " + allVelocityCount[c], ref f, presentPosition, new Bgr(255, 0, 0));
        //                            contourInformationOfAllColors[c][i].IS_MOVING_FLAG = true;
        //                            contourInformationOfAllColors[c][i].VELOCITY = velocity;




        //                            //img.Draw(line, new Bgr(0, 255, 0), 10);
        //                        }
        //                        else
        //                        {
        //                            contourInformationOfAllColors[c][i].IS_MOVING_FLAG = false;
        //                            contourInformationOfAllColors[c][i].VELOCITY = 0;
        //                            allVelocityCount[c] = 0;
        //                        }

        //                    }
        //                    catch (Exception divideByZero)
        //                    {

        //                    }
        //                }


        //                if (updateDelayStopwatch.Elapsed.Milliseconds % DELAY_IN_MILLISECONDS == 0)
        //                {
        //                    allPreviousPositionTime[c] = updateDelayStopwatch.Elapsed.Milliseconds;
        //                    previousPosition = new System.Drawing.Point(contourInformationOfAllColors[c][i].X, contourInformationOfAllColors[c][i].Y);
        //                }
        //            }
        //        }
        //    }



        //}

        public ContourInformation stretchContour(ContourInformation oldContourInfo, int factor = STRETCH_FACTOR)
        {

            //generalize with formula 
            //verify - start
            //if factor=1, newContourInfo is 2 times in size
            //if factor=2, newContourInfo is 9 times in size
            //if factor=3, newContourInfo is 16 times in size
            //if factor=4, newContourInfo is 25 times in size
            //verify - end


            System.Drawing.Point topLeft = new System.Drawing.Point(oldContourInfo.Left - factor, oldContourInfo.Top - factor);
            System.Drawing.Point topRight = new System.Drawing.Point(oldContourInfo.Right + factor, oldContourInfo.Top - factor);

            System.Drawing.Point bottomLeft = new System.Drawing.Point(oldContourInfo.Left - factor, oldContourInfo.Bottom + factor);
            System.Drawing.Point bottomRight = new System.Drawing.Point(oldContourInfo.Right + factor, oldContourInfo.Bottom + factor);

            int width = (int)distanceBetween2Points(bottomLeft, bottomRight);
            int height = (int)distanceBetween2Points(bottomRight, topRight);

            ContourInformation newContourInfo = new ContourInformation(topLeft.X, topLeft.Y, width, height, oldContourInfo.NAME_OF_COLOR);

            return newContourInfo;
        }

        /// <summary>
        /// is contourInfoA circumscribed by contourInfoB: is contourA contourInfoB? or is contourInfoB pregnant with contourInfoA
        /// </summary>
        /// <param name="contourInfoA"></param>
        /// <param name="insideContourB"></param>
        /// <returns></returns>
        public Boolean IsContourCircumscribed(ContourInformation contourInfoA, ContourInformation contourInfoB)
        {
            if (
                    contourInfoA.Top >= contourInfoB.Top && contourInfoA.Bottom <= contourInfoB.Bottom &&
                    contourInfoA.Left >= contourInfoB.Left && contourInfoA.Right <= contourInfoB.Right
               )
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public int IsContourCircumscribed(ContourInformation contourInfoA, List<ContourInformation> historyOfPastContours, ref List<ContourInformation> PastOverlap)
        {

            int howManyContoursAreCircumscribed = 0;
            for (int i = 0; i < historyOfPastContours.Count; i++)
            {

                if (
                        contourInfoA.Top >= historyOfPastContours[i].Top && contourInfoA.Bottom <= historyOfPastContours[i].Bottom &&
                        contourInfoA.Left >= historyOfPastContours[i].Left && contourInfoA.Right <= historyOfPastContours[i].Right
                   )
                {
                    PastOverlap.Add(historyOfPastContours[i]);
                    howManyContoursAreCircumscribed++;
                }
            }



            return howManyContoursAreCircumscribed;
        }

        public void summarizeCloseContours(ref List<ContourInformation> contourInformationList, ref List<ContourInformation>[] allCurrentBlobsContourInformation)
        {

            int previousSize = 0;
            while (previousSize != contourInformationList.Count)
            {
                previousSize = contourInformationList.Count;

                //compare atleast 2
                if (contourInformationList.Count >= 2)
                {
                    for (int i = 0; i < contourInformationList.Count; i++)
                    {
                        //compare atleast 2
                        if (contourInformationList.Count >= 2)
                        {
                            for (int j = 0; j < contourInformationList.Count; j++)
                            {
                                if (j != i)
                                {

                                    //divide by zero check
                                    try
                                    {
                                        if (contourInformationList[i].Height == 0)
                                        {
                                            contourInformationList[i].Height = 1;
                                        }

                                        if (contourInformationList[j].Height == 0)
                                        {
                                            contourInformationList[j].Height = 1;
                                        }

                                        //check if blob center is very close to another blob center
                                        System.Drawing.Point centerPointA =
                                            new System.Drawing.Point(
                                                                        (contourInformationList[i].Left + contourInformationList[i].Right) / 2,
                                                                        (contourInformationList[i].Top + contourInformationList[i].Bottom) / 2
                                                                    );

                                        System.Drawing.Point centerPointB =
                                            new System.Drawing.Point(
                                                                        (contourInformationList[j].Left + contourInformationList[j].Right) / 2,
                                                                        (contourInformationList[j].Top + contourInformationList[j].Bottom) / 2
                                                                    );
                                        //this is incorrect
                                        //System.Drawing.Point centerPointB =
                                        //    new System.Drawing.Point(
                                        //                                (contourInformationList[j].Width) / 2,
                                        //                                (contourInformationList[j].Height) / 2
                                        //                            );

                                        //these blobs are very close and are most likely the same object
                                        if (distanceBetween2Points(centerPointA, centerPointB) <= COEFFICIENT_OF_PROXIMITY)
                                        {
                                            ContourInformation summaryOf2Rectangles = summaryOfRectangles(contourInformationList[i], contourInformationList[j]);

                                            //for diagnostics
                                            //LineSegment2D line = new LineSegment2D(centerPointA, centerPointB);
                                            //img.Draw(line, new Bgr(0, 255, 0), 10);

                                            contourInformationList.Remove(contourInformationList[i]);
                                            contourInformationList.Remove(contourInformationList[j]);

                                            contourInformationList.Add(summaryOf2Rectangles);

                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                    }





                                }
                            }
                        }
                    }
                }
            }



            allCurrentBlobsContourInformation[allCurrentBlobsContourInformationIndexer] = contourInformationList;
            allCurrentBlobsContourInformationIndexer++;
            allCurrentBlobsContourInformationIndexer = allCurrentBlobsContourInformationIndexer % allCurrentBlobsContourInformation.Length;
        }

        public void summarizeCloseContours2(ref List<ContourInformation>[] allCurrentBlobsContourInformation)
        {


            //for allCurrentBlobsContourInformation
            for (int x = 0; x < allCurrentBlobsContourInformation.Length; x++)
            {

                int previousSize = 0;
                if (allCurrentBlobsContourInformation[x] != null)
                {
                    while (previousSize != allCurrentBlobsContourInformation[x].Count)
                    {
                        previousSize = allCurrentBlobsContourInformation[x].Count;

                        //compare atleast 2
                        if (allCurrentBlobsContourInformation[x].Count >= 2)
                        {
                            for (int i = 0; i < allCurrentBlobsContourInformation[x].Count; i++)
                            {
                                //compare atleast 2
                                if (allCurrentBlobsContourInformation[x].Count >= 2)
                                {
                                    for (int j = 0; j < allCurrentBlobsContourInformation[x].Count; j++)
                                    {
                                        if (j != i)
                                        {

                                            //divide by zero check
                                            try
                                            {
                                                if (allCurrentBlobsContourInformation[x][i].Height == 0)
                                                {
                                                    allCurrentBlobsContourInformation[x][i].Height = 1;
                                                }

                                                if (allCurrentBlobsContourInformation[x][j].Height == 0)
                                                {
                                                    allCurrentBlobsContourInformation[x][j].Height = 1;
                                                }

                                                //check if blob center is very close to another blob center
                                                System.Drawing.Point centerPointA =
                                                    new System.Drawing.Point(
                                                                                (allCurrentBlobsContourInformation[x][i].Left + allCurrentBlobsContourInformation[x][i].Right) / 2,
                                                                                (allCurrentBlobsContourInformation[x][i].Top + allCurrentBlobsContourInformation[x][i].Bottom) / 2
                                                                            );

                                                System.Drawing.Point centerPointB =
                                                    new System.Drawing.Point(
                                                                                (allCurrentBlobsContourInformation[x][j].Left + allCurrentBlobsContourInformation[x][j].Right) / 2,
                                                                                (allCurrentBlobsContourInformation[x][j].Top + allCurrentBlobsContourInformation[x][j].Bottom) / 2
                                                                            );
                                                //this is incorrect
                                                //System.Drawing.Point centerPointB =
                                                //    new System.Drawing.Point(
                                                //                                (allCurrentBlobsContourInformation[x][j].Width) / 2,
                                                //                                (allCurrentBlobsContourInformation[x][j].Height) / 2
                                                //                            );

                                                //these blobs are very close and are most likely the same object
                                                if (distanceBetween2Points(centerPointA, centerPointB) <= COEFFICIENT_OF_PROXIMITY)
                                                {
                                                    ContourInformation summaryOf2Rectangles = summaryOfRectangles(allCurrentBlobsContourInformation[x][i], allCurrentBlobsContourInformation[x][j]);

                                                    //for diagnostics
                                                    //LineSegment2D line = new LineSegment2D(centerPointA, centerPointB);
                                                    //img.Draw(line, new Bgr(0, 255, 0), 10);

                                                    allCurrentBlobsContourInformation[x].Remove(allCurrentBlobsContourInformation[x][i]);
                                                    allCurrentBlobsContourInformation[x].Remove(allCurrentBlobsContourInformation[x][j]);

                                                    allCurrentBlobsContourInformation[x].Add(summaryOf2Rectangles);

                                                }

                                            }
                                            catch (Exception ex)
                                            {
                                            }





                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }



        }

        //obsolete tagContourInfos()
        //public void tagContourInfos(ref List<ContourInformation> contourInformationList)
        //{
        //    if (contourInformationList.Count == 0)
        //    {
        //        return;
        //    }

        //    List<int> allContourInfoXvaluesArray = new List<int>();

        //    for (int i = 0; i < contourInformationList.Count; i++)
        //    {
        //        allContourInfoXvaluesArray.Add(contourInformationList[i].X);              
        //        //globalImage.Draw("X " + contourInformationOfAllColors[i][j].X, ref bigFont, new System.Drawing.Point(contourInformationOfAllColors[i][j].X, contourInformationOfAllColors[i][j].Y), new Bgr(250, 0, 0));
        //    }

        //    allContourInfoXvaluesArray.Sort();

        //    for (int i = 0; i < contourInformationList.Count; i++)
        //    {
        //        int colorIndex = contourInformationList[i].CONTOUR_ID.COLOR_INDEX;
        //        int colorID = allContourInfoXvaluesArray.IndexOf(contourInformationList[i].X);

        //        ContourInformation.ID id = new ContourInformation.ID(colorIndex, colorID);
        //        contourInformationList[i].CONTOUR_ID = id;

        //        //globalImage.Draw(new System.Drawing.Rectangle(contourInformationList[i].X, contourInformationList[i].Y, contourInformationList[i].Width, contourInformationList[i].Height), new Bgr(250, 0, 0), 1);
        //        //globalImage.Draw("ID " , ref f, new System.Drawing.Point(contourInformationList[i].X, contourInformationList[i].Y+25), new Bgr(79, 255, 49));
        //    }




        //}

        public float inchesToFeet(float inches)
        {
            return inches * 0.0833333f;
        }

        public double distanceBetween2Points(System.Drawing.Point point1, System.Drawing.Point point2)
        {
            return System.Math.Sqrt(((point2.X - point1.X) * (point2.X - point1.X)) + ((point2.Y - point1.Y) * (point2.Y - point1.Y)));
        }

        private ContourInformation summaryOfRectangles(ContourInformation contoursAInformation, ContourInformation contoursBInformation)
        {
            int newLeftmostX = 0;
            int newTopmostY = 0;

            int newWidth = 0;
            int newHeight = 0;

            if (contoursAInformation.X < contoursBInformation.X)
            {
                newLeftmostX = contoursAInformation.X;
            }
            else
            {
                newLeftmostX = contoursBInformation.X;
            }

            if (contoursAInformation.Y < contoursBInformation.Y)
            {
                newTopmostY = contoursAInformation.Y;
            }
            else
            {
                newTopmostY = contoursBInformation.Y;
            }

            //summarize width
            if (contoursBInformation.Left >= contoursAInformation.Left && contoursBInformation.Right >= contoursAInformation.Right)
            {
                newWidth = System.Math.Abs(contoursBInformation.Right - contoursAInformation.Right) + contoursAInformation.Width;
            }
            else if (contoursBInformation.Left >= contoursAInformation.Left && contoursBInformation.Right <= contoursAInformation.Right)
            {
                newWidth = contoursAInformation.Width;
            }
            else if (contoursBInformation.Left <= contoursAInformation.Left && contoursBInformation.Right >= contoursAInformation.Right)
            {
                newWidth = contoursBInformation.Width;
            }
            else if (contoursBInformation.Left <= contoursAInformation.Left && contoursBInformation.Right <= contoursAInformation.Right)
            {
                newWidth = System.Math.Abs(contoursBInformation.Left - contoursAInformation.Left) + contoursAInformation.Width;
            }


            //summarize height
            if (contoursBInformation.Top <= contoursAInformation.Top && contoursBInformation.Bottom >= contoursAInformation.Bottom)
            {
                newHeight = contoursBInformation.Height;
            }
            else if (contoursBInformation.Top >= contoursAInformation.Top && contoursBInformation.Bottom >= contoursAInformation.Bottom)
            {
                newHeight = System.Math.Abs(contoursAInformation.Bottom - contoursBInformation.Bottom) + contoursAInformation.Height;
            }
            else if (contoursBInformation.Top <= contoursAInformation.Top && contoursBInformation.Bottom <= contoursAInformation.Bottom)
            {
                newHeight = System.Math.Abs(contoursBInformation.Top - contoursAInformation.Top) + contoursAInformation.Height;
            }
            else if (contoursBInformation.Top >= contoursAInformation.Top && contoursBInformation.Bottom <= contoursAInformation.Bottom)
            {
                newHeight = contoursAInformation.Height;
            }

            ContourInformation summaryOf2Rectangles = new ContourInformation(newLeftmostX, newTopmostY, newWidth, newHeight, contoursAInformation.NAME_OF_COLOR);

            return summaryOf2Rectangles;

        }


        /// <summary>
        /// Convert an IImage to a WPF BitmapSource. The result can be used in the Set Property of Image.Source
        /// </summary>
        /// <param name="image">The Emgu CV Image</param>
        /// <returns>The equivalent BitmapSource</returns>
        /// 

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);

        private BitmapSource ToBitmapSource(IImage image)
        {
            using (System.Drawing.Bitmap source = image.Bitmap)
            {
                IntPtr ptr = source.GetHbitmap(); //obtain the Hbitmap

                BitmapSource bs = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                    ptr,
                    IntPtr.Zero,
                    Int32Rect.Empty,
                    System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());

                DeleteObject(ptr); //release the HBitmap


                return bs;
            }
        }

        private static BitmapSource ToBitmapSource2(IImage image)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                image.Bitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Png);

                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.StreamSource = new System.IO.MemoryStream(stream.ToArray());
                bitmap.EndInit();

                return bitmap;
            }
        }

        public static Bitmap ImageToBitmap(byte[] colorData, int width, int height, Rect region)
        {
            byte[] pixeldata = new byte[colorData.Length];
            // Image.CopyPixelDataTo(pixeldata);
            for (int i = 0; i < colorData.Length; i++)
            {
                pixeldata[i] = colorData[i];
            }

            Bitmap bmap = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppRgb);

            System.Drawing.Imaging.BitmapData bmapdata = bmap.LockBits(new System.Drawing.Rectangle(0, 0, width, height),
                                                                          System.Drawing.Imaging.ImageLockMode.WriteOnly,
                                                                          bmap.PixelFormat);
            IntPtr ptr = bmapdata.Scan0;
            System.Runtime.InteropServices.Marshal.Copy(pixeldata, 0, ptr, colorData.Length);
            bmap.UnlockBits(bmapdata);
            return bmap;
        }
    }

    public class ContourInformation
    {
        private int X_ = 0;
        private int Y_ = 0;

        private int width_ = 0;
        private int height_ = 0;

        private String nameOfColor_;

        #region for Past ContourInformation
        //consider extending ContourInformation to derive stretchContour especially if there is an important benefit to gain
        //else this works fine

        private ID contourID = new ID(-1, -1);

        public struct ID
        {
            public int COLOR_INDEX;
            public int CONTOUR_ID;

            public ID(int colorIndex, int contourID)
            {
                COLOR_INDEX = colorIndex;
                CONTOUR_ID = contourID;
            }
        }

        private int previousX = 0;
        private int previousY = 0;
        //if necessary functions that cap the size of the list
        private List<System.Drawing.Point> previousXYList = new List<System.Drawing.Point>();

        //private int previousXYCursor = 0;
        private int previousTimeInMilliseconds = 0;
        private int storage = -1;                       //used for temporary storage

        private ContourInformation pastContour;
        private Boolean isTrackedFlag = false;
        private Boolean isMovingFlag = false;
        private Boolean isTaggedFlag = false;


        private float velocity_ = 0;

        private int overlapCount_ = 0;
        private int previousOverlapCount = 0;

        //MAXIMUM_CONTOUR_LIFE should not be too small else good Past contours will be killed too soon, 
        //the higher this value, the better the algorithm and the longer obsolete Past contours are kept
        //const int MAXIMUM_CONTOUR_LIFE = 2000;
        //const int MAXIMUM_CONTOUR_LIFE = 10000;
        const int MAXIMUM_CONTOUR_LIFE = 20000;
        const int MINIMUM_CONTOUR_LIFE_ = 0;
        const int CONTOUR_DECAY_GRADIENT_ = 1;

        const int DELAY_IN_MILLISECONDS_ = 4;             //4
        int MAXIMUM_NUMBER_OF_OBJECTS_TO_TRACK_ = 2;

        private int life;

        #endregion


        public ContourInformation(int x = 0, int y = 0, int width = 0, int height = 0, String nameOfColor = "COLOR")
        {
            X_ = x;
            Y_ = y;

            width_ = width;
            height_ = height;

            nameOfColor_ = nameOfColor.ToUpper();

            life = MAXIMUM_CONTOUR_LIFE;


        }

        public int X
        {
            get { return X_; }
            set { X_ = value; }
        }

        public int Y
        {
            get { return Y_; }
            set { Y_ = value; }
        }

        public int Width
        {
            get { return width_; }
            set { width_ = value; }
        }

        public int Height
        {
            get { return height_; }
            set { height_ = value; }
        }

        public int Left
        {
            get { return X_; }
        }

        public int Right
        {
            get { return width_ + X_; }
        }

        public int Top
        {
            get { return Y_; }
        }

        public int Bottom
        {
            get { return height_ + Y_; }
        }

        public String NAME_OF_COLOR
        {
            get { return nameOfColor_; }
            set { nameOfColor_ = value; }
        }

        public Boolean IS_MOVING_FLAG
        {
            get { return isMovingFlag; }
            set { isMovingFlag = value; }
        }

        public Boolean IS_TAGGED_FLAG
        {
            get { return isTaggedFlag; }
            set { isTaggedFlag = value; }
        }


        public float VELOCITY
        {
            get { return velocity_; }
            set { velocity_ = value; }
        }

        public ID CONTOUR_IDENTITY
        {
            get { return contourID; }
            set { contourID = value; }
        }


        #region for Past ContourInformation

        public int PREVIOUS_X
        {
            get { return previousX; }
            set { previousX = value; }
        }

        public int PREVIOUS_Y
        {
            get { return previousY; }
            set { previousY = value; }
        }

        public List<System.Drawing.Point> PREVIOUS_XY
        {
            get { return previousXYList; }
            set { previousXYList = value; }
        }

        //precondition: size of fx array >= 1
        //public static float[] GetSlopes(PointF[] fx)
        //{

        //    if (fx == null)
        //    {
        //        //Error, precondition not met
        //        return null;
        //    }

        //    float[] slopes = null;
        //    if (slopes == null)
        //    {
        //        slopes = new float[fx.Length];
        //        PointF previousPoint = new PointF(-99999, -99999);
        //        int count = 0;

        //        // loop through each point of the function and use the
        //        // adjacent point to determine the next slope.
        //        foreach (PointF point in fx)
        //        {
        //            if ((int)previousPoint.X != -99999)
        //            {
        //                // calculate slope
        //                float slope = (point.Y - previousPoint.Y) / (point.X - previousPoint.X);
        //                slopes[count] = slope;
        //                count++;
        //            }

        //            previousPoint = point;
        //        }

        //    }
        //    // return the set of slopes
        //    return slopes;

        //}

        public static System.Drawing.Point[] toPointArray(List<System.Drawing.Point> listOfPoints)
        {

            if (listOfPoints != null)
            {
                System.Drawing.Point[] values = new System.Drawing.Point[listOfPoints.Count];

                for (int i = 0; i < listOfPoints.Count; i++)
                {
                    values[i] = listOfPoints[i];
                }

                return values;
            }
            else
            {
                return null;
            }
        }

        //public static System.Drawing.PointF[] toPointFArray(System.Drawing.Point[] arrayOfPoints)
        //{
        //    if (arrayOfPoints != null)
        //    {
        //        System.Drawing.PointF[] values = new PointF[arrayOfPoints.Length];

        //        for (int i = 0; i < arrayOfPoints.Length; i++)
        //        {
        //            values[i] = arrayOfPoints[i];
        //        }

        //        return values;
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}

        public int PREVIOUS_TIME_MILLISECONDS
        {
            get { return previousTimeInMilliseconds; }
            set { previousTimeInMilliseconds = value; }
        }

        public int STORAGE
        {
            get { return storage; }
            set { storage = value; }
        }

        public ContourInformation PAST_CONTOUR
        {
            get { return pastContour; }
            set { pastContour = value; }
        }

        public Boolean IS_TRACKED_FLAG
        {
            get { return isTrackedFlag; }
            set { isTrackedFlag = value; }
        }


        public int OVERLAP_COUNT
        {
            get { return overlapCount_; }
            set { overlapCount_ = value; }
        }

        public int PREVIOUS_OVERLAP_COUNT
        {
            get { return previousOverlapCount; }
            set { previousOverlapCount = value; }
        }

        public int LIFE
        {
            get { return life; }
            set { life = value; }
        }

        public void resetLife(int maximumLife = MAXIMUM_CONTOUR_LIFE)
        {
            life = MAXIMUM_CONTOUR_LIFE;
        }

        public int MINIMUM_CONTOUR_LIFE
        {
            get { return MINIMUM_CONTOUR_LIFE_; }
        }

        public int CONTOUR_DECAY_GRADIENT
        {
            get { return CONTOUR_DECAY_GRADIENT_; }
        }

        public static int DELAY_IN_MILLISECONDS
        {
            get { return DELAY_IN_MILLISECONDS_; }
        }

        public int MAXIMUM_NUMBER_OF_OBJECTS_TO_TRACK
        {
            get { return MAXIMUM_NUMBER_OF_OBJECTS_TO_TRACK_; }
            set { MAXIMUM_NUMBER_OF_OBJECTS_TO_TRACK_ = value; }
        }


        public void WipeClean()
        {
            this.previousTimeInMilliseconds = 0;
            this.previousOverlapCount = 0;
            this.previousX = 0;
            this.previousY = 0;

            if (this.previousXYList != null && this.previousXYList.Count > 0)
            {
                this.previousXYList.Clear();
            }

            this.storage = 0;
            this.contourID = new ID(-1, -1);
            this.height_ = 0;
            this.width_ = 0;
            this.velocity_ = 0;
            this.pastContour = null;
            this.nameOfColor_ = null;
            this.overlapCount_ = 0;
            this.MAXIMUM_NUMBER_OF_OBJECTS_TO_TRACK_ = 0;
            this.life = 0;
            this.isTrackedFlag = false;
            this.isTaggedFlag = false;
            this.isMovingFlag = false;

        }
        #endregion

    }

    public class BlobInformation
    {
        private float minimumHue;
        private float maximumHue;

        private float minimumSaturation;
        private float maximumSaturation;

        //private float minimumValue;
        //private float maximumValue;

        private float maxDepthMillimeters;

        private int maximumContourArea;

        private Boolean depthRangeFilterFlag;
        private Boolean detectMotionFlag;

        private String blobColor_;


        public BlobInformation(float minHue, float maxHue, float minSat, float maxSat /*,float minVal, float maxVal*/, String blobColor = "COLOR", int maxDepthInMillimeters = 1524, int maxContourArea = 8, Boolean dpthRangeFilterFlag = true, Boolean motionFlag = false)
        {
            minimumHue = minHue;
            maximumHue = maxHue;

            minimumSaturation = minSat;
            maximumSaturation = maxSat;

            //minimumValue = minVal;
            //maximumValue = maxVal;

            maxDepthMillimeters = maxDepthInMillimeters;

            maximumContourArea = maxContourArea;

            depthRangeFilterFlag = dpthRangeFilterFlag;

            detectMotionFlag = motionFlag;
            blobColor_ = blobColor.ToUpper();
        }

        #region accessors

        public float MINIMUM_HUE
        {
            get { return minimumHue; }
            set { minimumHue = value; }
        }

        public float MAXIMUM_HUE
        {
            get { return maximumHue; }
            set { maximumHue = value; }
        }

        public float MINIMUM_SATURATION
        {
            get { return minimumSaturation; }
            set { minimumSaturation = value; }
        }

        public float MAXIMUM_SATURATION
        {
            get { return maximumSaturation; }
            set { maximumSaturation = value; }
        }

        //public float MINIMUM_VALUE
        //{
        //    get { return minimumValue; }
        //    set { minimumValue = value; }
        //}

        //public float MAXIMUM_VALUE
        //{
        //    get { return maximumValue; }
        //    set { maximumValue = value; }
        //}

        public float MAXIMUM_DEPTH_MILLIMETERS
        {
            get { return maxDepthMillimeters; }
            set { maxDepthMillimeters = value; }
        }

        public int MAXIMUM_CONTOUR_AREA
        {
            get { return maximumContourArea; }
            set { maximumContourArea = value; }
        }

        public Boolean DEPTH_RANGE_FILTER_FLAG
        {
            get { return depthRangeFilterFlag; }
            set { depthRangeFilterFlag = value; }
        }

        public Boolean DETECT_MOTION_FILTER_FLAG
        {
            get { return detectMotionFlag; }
            set { detectMotionFlag = value; }
        }

        public String BLOB_COLOR
        {
            get { return blobColor_; }
            set { blobColor_ = value; }
        }


        #endregion


    }
}
