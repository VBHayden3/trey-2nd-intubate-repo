﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AIMS.Assets.Lessons2;
using System.ComponentModel;

namespace AIMS.Pages
{
    /// <summary>
    /// Interaction logic for ColorCalibrationPage.xaml
    /// </summary>
    public partial class ColorCalibrationPage : Page, INotifyPropertyChanged
    {
        /// <summary>
        /// List of all available ColorRange's from the XML file.
        /// </summary>
        private ObservableRangeCollection<ColorTracker.ColorRange> colorRanges = new ObservableRangeCollection<ColorTracker.ColorRange>();
        public ObservableRangeCollection<ColorTracker.ColorRange> ColorRanges
        {
            get { return colorRanges; }
            set
            {
                if (colorRanges != value)
                {
                    colorRanges = value;
                }
                else
                {
                    colorRanges = value;
                    NotifyPropertyChanged(this, "ColorRanges");
                }
            }
        }
        /// <summary>
        /// List of all specialObjects from the XML file.
        /// </summary>
        private ObservableRangeCollection<SpecialObject> specialObjects = new ObservableRangeCollection<SpecialObject>();
        public ObservableRangeCollection<SpecialObject> SpecialObjects
        {
            get { return specialObjects; }
            set
            {
                if (specialObjects != value)
                {
                    specialObjects = value;
                }
                else
                {
                    specialObjects = value;
                    NotifyPropertyChanged(this, "SpecialObjects");
                }
            }
        }

        

        public ColorCalibrationPage()
        {
            InitializeComponent();

            colorRanges.Add(ColorTracker.YellowTape);
            colorRanges.Add(ColorTracker.RedTape);
            colorRanges.Add(ColorTracker.GreenTape);
            colorRanges.Add(ColorTracker.PinkTape);

            specialObjects.Add(new SpecialObject("Laryngoscope", ColorTracker.YellowTape));
            specialObjects.Add(new SpecialObject("Endotracheal Tube", ColorTracker.PinkTape));
        }


        #region Notify Property Changed
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(object sender, string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(sender, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
