﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using System.ComponentModel;
using Microsoft.Kinect;
using System.Reflection;

namespace AIMS
{
    /// <summary>
    /// Interaction logic for PathComparisonPage.xaml
    /// </summary>
    public partial class PathComparisonPage : Page, INotifyPropertyChanged
    {
        private PathComparison pathComparison;
        public PathComparison PathComparison
        { 
            get { return pathComparison; } 
            set 
            {
                if (pathComparison == value)
                    pathComparison = value;
                else
                {
                    pathComparison = value;
                    NotifyPropertyChanged("PathComparison");

                    if (pathComparison != null)
                    {
                        // Set the ranges.
                        this.RangeStartTime = pathComparison.RecordingStartTime;
                        this.RangeEndTime = pathComparison.RecordingEndTime;
                    }
                }
            } 
        }

        private bool useRangeClamp = true;
        public bool UseRangeClamp
        {
            get { return useRangeClamp; }
            set
            {
                if (useRangeClamp == value)
                    useRangeClamp = value;
                else
                {
                    useRangeClamp = value;
                    NotifyPropertyChanged("UseRangeClamp");
                }
            }
        }

        private bool usePathSmoothing = true;
        public bool UsePathSmoothing
        {
            get { return usePathSmoothing; }
            set
            {
                if (usePathSmoothing == value)
                    usePathSmoothing = value;
                else
                {
                    usePathSmoothing = value;
                    NotifyPropertyChanged("UsePathSmoothing");
                }
            }
        }

        private int pathSmoothingValue = 1;
        public int PathSmoothingValue
        {
            get { return pathSmoothingValue; }
            set
            {
                if (pathSmoothingValue == value)
                    pathSmoothingValue = value;
                else
                {
                    pathSmoothingValue = value;
                    NotifyPropertyChanged("PathSmoothingValue");
                }
            }
        }

        private bool recording = false;
        public bool Recording
        {
            get { return recording; }
            set
            {
                if (recording == value)
                    recording = value;
                else
                {
                    recording = value;
                    NotifyPropertyChanged("Recording");
                }
            }
        }

        private bool smoothBeforeClamp = true;
        public bool SmoothBeforeClamp
        {
            get { return smoothBeforeClamp; }
            set
            {
                if (smoothBeforeClamp == value)
                    smoothBeforeClamp = value;
                else
                {
                    smoothBeforeClamp = value;
                    NotifyPropertyChanged("SmoothBeforeClamp");
                }
            }
        }

        private DateTime rangeStartTime = DateTime.MinValue;
        public DateTime RangeStartTime
        {
            get { return rangeStartTime; }
            set
            {
                if (rangeStartTime == value)
                    rangeStartTime = value;
                else
                {
                    rangeStartTime = value;
                    NotifyPropertyChanged("RangeStartTime");
                }
            }
        }

        private DateTime rangeEndTime = DateTime.MaxValue;
        public DateTime RangeEndTime
        {
            get { return rangeEndTime; }
            set
            {
                if (rangeEndTime == value)
                    rangeEndTime = value;
                else
                {
                    rangeEndTime = value;
                    NotifyPropertyChanged("RangeEndTime");
                }
            }
        }

        private WriteableBitmap colorBitmap = new WriteableBitmap(640, 480, 96, 96, PixelFormats.Bgr32, null);

        public PathComparisonPage()
        {
            InitializeComponent();
            ColorDisplay.Source = colorBitmap;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = this;

            KinectManager.NumStickySkeletons = 2;
            KinectManager.AllDataReady += new AllDataReadyEventHandler(KinectManager_AllDataReady);

            BuildSolid();
        }

        public List<Color> SimpleColorList
        {
            get
            {
                List<Color> colors = new List<Color> 
                { 
                    #region Add All From Colors.*;
                    Colors.AliceBlue, 
                    Colors.AntiqueWhite,
                    Colors.Aqua,
                    Colors.Aquamarine,
                    Colors.Azure,
                    Colors.Beige,
                    Colors.Bisque,
                    Colors.Black,
                    Colors.BlanchedAlmond,
                    Colors.Blue,
                    Colors.BlueViolet,
                    Colors.Brown,
                    Colors.BurlyWood,
                    Colors.CadetBlue,
                    Colors.Chartreuse,
                    Colors.Chocolate,
                    Colors.Coral,
                    Colors.CornflowerBlue,
                    Colors.Cornsilk,
                    Colors.Crimson,
                    Colors.Cyan,
                    Colors.DarkBlue,
                    Colors.DarkCyan,
                    Colors.DarkGoldenrod,
                    Colors.DarkGray,
                    Colors.DarkGreen,
                    Colors.DarkKhaki,
                    Colors.DarkMagenta,
                    Colors.DarkOliveGreen,
                    Colors.DarkOrange,
                    Colors.DarkOrchid,
                    Colors.DarkRed,
                    Colors.DarkSalmon,
                    Colors.DarkSeaGreen,
                    Colors.DarkSlateBlue,
                    Colors.DarkSlateGray,
                    Colors.DarkTurquoise,
                    Colors.DarkViolet,
                    Colors.DeepPink,
                    Colors.DeepSkyBlue,
                    Colors.DimGray,
                    Colors.DodgerBlue,
                    Colors.Firebrick,
                    Colors.FloralWhite,
                    Colors.ForestGreen,
                    Colors.Fuchsia,
                    Colors.Gainsboro,
                    Colors.GhostWhite,
                    Colors.Gold,
                    Colors.Goldenrod,
                    Colors.Gray,
                    Colors.Green,
                    Colors.GreenYellow,
                    Colors.Honeydew,
                    Colors.HotPink,
                    Colors.IndianRed,
                    Colors.Indigo,
                    Colors.Ivory,
                    Colors.Khaki,
                    Colors.Lavender,
                    Colors.LavenderBlush,
                    Colors.LawnGreen,
                    Colors.LemonChiffon,
                    Colors.LightBlue,
                    Colors.LightCoral,
                    Colors.LightCyan,
                    Colors.LightGoldenrodYellow,
                    Colors.LightGray,
                    Colors.LightGreen,
                    Colors.LightPink,
                    Colors.LightSalmon,
                    Colors.LightSeaGreen,
                    Colors.LightSkyBlue,
                    Colors.LightSlateGray,
                    Colors.LightSteelBlue,
                    Colors.LightYellow,
                    Colors.Lime,
                    Colors.LimeGreen,
                    Colors.Linen,
                    Colors.Magenta,
                    Colors.Maroon,
                    Colors.MediumAquamarine,
                    Colors.MediumBlue,
                    Colors.MediumOrchid,
                    Colors.MediumPurple,
                    Colors.MediumSeaGreen,
                    Colors.MediumSlateBlue,
                    Colors.MediumSpringGreen,
                    Colors.MediumTurquoise,
                    Colors.MediumVioletRed,
                    Colors.MidnightBlue,
                    Colors.MintCream,
                    Colors.MistyRose,
                    Colors.Moccasin,
                    Colors.NavajoWhite,
                    Colors.Navy,
                    Colors.OldLace,
                    Colors.Olive,
                    Colors.OliveDrab,
                    Colors.Orange,
                    Colors.OrangeRed,
                    Colors.Orchid,
                    Colors.PaleGoldenrod,
                    Colors.PaleGreen,
                    Colors.PaleTurquoise,
                    Colors.PaleVioletRed,
                    Colors.PapayaWhip,
                    Colors.PeachPuff,
                    Colors.Peru,
                    Colors.Pink,
                    Colors.Plum,
                    Colors.PowderBlue,
                    Colors.Purple,
                    Colors.Red,
                    Colors.RosyBrown,
                    Colors.RoyalBlue,
                    Colors.SaddleBrown,
                    Colors.Salmon,
                    Colors.SandyBrown,
                    Colors.SeaGreen,
                    Colors.SeaShell,
                    Colors.Sienna,
                    Colors.Silver,
                    Colors.SkyBlue,
                    Colors.SlateBlue,
                    Colors.SlateGray,
                    Colors.Snow,
                    Colors.SpringGreen,
                    Colors.SteelBlue,
                    Colors.Tan,
                    Colors.Teal,
                    Colors.Thistle,
                    Colors.Tomato,
                    Colors.Transparent,
                    Colors.Turquoise,
                    Colors.Violet,
                    Colors.Wheat,
                    Colors.White,
                    Colors.WhiteSmoke,
                    Colors.Yellow,
                    Colors.YellowGreen
                    #endregion
                };

                return colors;
            }
        }

        void KinectManager_AllDataReady()
        {
            // Update the colorView
            colorBitmap.WritePixels(new Int32Rect(0, 0, 640, 480), KinectManager.ColorData, 640 * 4, 0);

            // Update the SkeletonOverlay.
            this.SkeletonOverlay.UpdateSkeleton();
        }

        private bool showFloor = true;
        public bool ShowFloor
        {
            get { return showFloor; }
            set
            {
                if (showFloor == value)
                    showFloor = value;
                else
                {
                    showFloor = value;
                    NotifyPropertyChanged("ShowFloor");                    
                }

                if (showFloor)
                {
                    ShowFloorMesh();
                }
                else
                {
                    HideFloorMesh();
                }
            }
        }

        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(sender, e);
            }
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            this.NotifyPropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        private Model3DGroup mGeometry = new Model3DGroup();
        private Model3DGroup mGeometryGP;
        private bool mDown;
        private Point mLastPos;
        private static bool rotateLock = false;
        private int[] goodSync;
        private int[] badSync;

        List<List<Point3D[]>> all3dTransformedPoints;
        
        private Color[] colListJoint;

        public Color[] COL_LIST_JOINT
        {
            get { return colListJoint; }
            set { colListJoint = value; }
        }

        /// <summary>
        /// Draws the paths of both skeletons from the PathComparison using the passed parameters.
        /// 
        /// Called by "RedrawPaths()" which determines start/stop time and smoothing value based on current options.
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="smoothingValue"></param>
        private void DrawPaths( DateTime startTime, DateTime endTime, int smoothingValue, bool smoothBeforeClamp )
        {
            if (this.PathComparison == null)    // No PathComparison to draw.
                return;

            int count = group.Children.Count;

            while (group.Children.Count > 3)    // Remove the old drawn paths from the model group, leaving the first 3 (ambient light, directional light, and floor mesh) elements.
            {
                group.Children.RemoveAt(group.Children.Count - 1);
            }

            mGeometry.Children.Clear();

            mGeometry.Transform = mGeometryGP.Transform;

            if (this.PathComparison.PathA != null)
            {
                ComparablePath pA = this.PathComparison.PathA;

                if (pA.ShowPath)
                {
                    this.DrawPath(pA, startTime, endTime, smoothBeforeClamp, smoothingValue);
                }
            }
        }

        private void DrawPath(JointPath path, DateTime startTime, DateTime endTime, bool smoothBeforeClamp, int smoothingValue = 1)
        {
            if (path == null) 
                return;

            if (path.PathPoints == null) 
                return;

            DiffuseMaterial dm = new DiffuseMaterial(new SolidColorBrush(path.PathColor));

            //#region Scale to Viewport Coordinate Space
            //for (int i = 0; i < path.PathPoints.Count; i++)
            //{
            //    path.PathPoints[i].Location = new Point3D(path.PathPoints[i].Location.X/* / 640 * 1000*/, path.PathPoints[i].Location.Y/* / 480 * 1000*/, /*path.PathPoints[i].Location.Z*/ (path.PathPoints[i].Location.Z));
            //}
            //#endregion

            /*
            #region Rotate and Transform
            for (int i = 0; i < path.PathPoints.Count; i++)
            {
                path.PathPoints[i].Location = new Point3D(( path.PathPoints[i].Location.X / 100 ) * 100, (10 - (path.PathPoints[i].Location.Y / 100)) * 100, (10 - (path.PathPoints[i].Location.Z / 100)) * 100);
            }
            #endregion
            */

            #region Draw/Connect Points
            MeshGeometry3D mesh = new MeshGeometry3D();

            #region Add Points
            //AddPoints(ref mesh, Points, thickness);
            double thickness = 0.0025;

            if (smoothBeforeClamp)
            {
                #region Smooth, then Clamp, then Draw.

                List<JointPath.PathPoint> smoothedPathPoints = new List<JointPath.PathPoint>();

                if (smoothingValue > 1)
                {
                    for (int i = 0; i < path.PathPoints.Count; i++)
                    {
                        int cnt = 0;
                        double totalX = 0;
                        double totalY = 0;
                        double totalZ = 0;

                        for (int j = smoothingValue; j >= 0; j--)
                        {
                            if (i - j >= 0) // Average the previous (#)x of points based on the smoothing value.
                            {
                                cnt++;

                                totalX += path.PathPoints[i - j].Location.X;
                                totalY += path.PathPoints[i - j].Location.Y;
                                totalZ += path.PathPoints[i - j].Location.Z;
                            }
                        }

                        if (cnt > 0)
                        {
                            Point3D avgP3D = new Point3D(totalX / cnt, totalY / cnt, totalZ / cnt);
                            smoothedPathPoints.Add(new JointPath.PathPoint(avgP3D, path.PathPoints[i].Timestamp));
                        }
                    }
                }
                else
                {
                    smoothedPathPoints = path.PathPoints.ToList();
                }

                // CLAMP THE PATHS TO TIME RANGE:
                List<JointPath.PathPoint> clampedPathPoints = smoothedPathPoints.Where(o => (o.Timestamp >= startTime && o.Timestamp <= endTime)).ToList();


                if (clampedPathPoints != null && clampedPathPoints.Count > 0)
                {
                    mesh.Positions.Add(new Point3D(clampedPathPoints[0].Location.X, clampedPathPoints[0].Location.Y, clampedPathPoints[0].Location.Z));
                    mesh.Positions.Add(new Point3D(clampedPathPoints[0].Location.X + thickness, clampedPathPoints[0].Location.Y, clampedPathPoints[0].Location.Z));
                    mesh.Positions.Add(new Point3D(clampedPathPoints[0].Location.X + thickness, clampedPathPoints[0].Location.Y - thickness, clampedPathPoints[0].Location.Z));
                    mesh.Positions.Add(new Point3D(clampedPathPoints[0].Location.X, clampedPathPoints[0].Location.Y - thickness, clampedPathPoints[0].Location.Z));
                    for (int i = 0; i < clampedPathPoints.Count - 1; i++)
                    {
                        #region Joint Points
                        //joinPoints(ref mesh, Points[i], Points[++i], thickness);
                        mesh.Positions.Add(new Point3D(clampedPathPoints[i + 1].Location.X, clampedPathPoints[i + 1].Location.Y, clampedPathPoints[i + 1].Location.Z));
                        mesh.Positions.Add(new Point3D(clampedPathPoints[i + 1].Location.X + thickness, clampedPathPoints[i + 1].Location.Y, clampedPathPoints[i + 1].Location.Z));
                        mesh.Positions.Add(new Point3D(clampedPathPoints[i + 1].Location.X + thickness, clampedPathPoints[i + 1].Location.Y - thickness, clampedPathPoints[i + 1].Location.Z));
                        mesh.Positions.Add(new Point3D(clampedPathPoints[i + 1].Location.X, clampedPathPoints[i + 1].Location.Y - thickness, clampedPathPoints[i + 1].Location.Z));

                        int k = mesh.Positions.Count - 8;

                        //Front face
                        miniBuildFace(ref mesh, 0 + k, 1 + k, 2 + k);
                        miniBuildFace(ref mesh, 0 + k, 2 + k, 3 + k);

                        //Back face
                        miniBuildFace(ref mesh, 4 + k, 5 + k, 6 + k);
                        miniBuildFace(ref mesh, 4 + k, 6 + k, 7 + k);

                        //Right face
                        miniBuildFace(ref mesh, 3 + k, 2 + k, 5 + k);
                        miniBuildFace(ref mesh, 3 + k, 5 + k, 4 + k);

                        //Top face
                        miniBuildFace(ref mesh, 0 + k, 3 + k, 7 + k);
                        miniBuildFace(ref mesh, 0 + k, 7 + k, 4 + k);

                        //Left face
                        miniBuildFace(ref mesh, 4 + k, 5 + k, 1 + k);
                        miniBuildFace(ref mesh, 4 + k, 1 + k, 0 + k);

                        //Bottom face
                        miniBuildFace(ref mesh, 2 + k, 1 + k, 5 + k);
                        miniBuildFace(ref mesh, 2 + k, 5 + k, 6 + k);
                        #endregion
                    }
                }
                #endregion
            }
            else
            {
                #region Clamp, then Smooth, then Draw.
                // CLAMP THE PATHS TO TIME RANGE:
                List<JointPath.PathPoint> clampedPathPoints = path.PathPoints.Where(o => (o.Timestamp >= startTime && o.Timestamp <= endTime)).ToList();

                if (smoothingValue > 1)
                {
                    List<JointPath.PathPoint> smoothedPathPoints = new List<JointPath.PathPoint>();

                    for (int i = 0; i < clampedPathPoints.Count; i++)
                    {
                        int cnt = 0;
                        double totalX = 0;
                        double totalY = 0;
                        double totalZ = 0;

                        for (int j = smoothingValue; j >= 0; j--)
                        {
                            if (i - j >= 0) // Average the previous (#)x of points based on the smoothing value.
                            {
                                cnt++;

                                totalX += clampedPathPoints[i - j].Location.X;
                                totalY += clampedPathPoints[i - j].Location.Y;
                                totalZ += clampedPathPoints[i - j].Location.Z;
                            }
                        }

                        if (cnt > 0)
                        {
                            Point3D avgP3D = new Point3D(totalX / cnt, totalY / cnt, totalZ / cnt);
                            smoothedPathPoints.Add(new JointPath.PathPoint(avgP3D, clampedPathPoints[i].Timestamp));
                        }
                    }

                    clampedPathPoints.Clear();
                    clampedPathPoints = smoothedPathPoints;
                }


                if (clampedPathPoints != null && clampedPathPoints.Count > 0)
                {
                    mesh.Positions.Add(new Point3D(clampedPathPoints[0].Location.X, clampedPathPoints[0].Location.Y, clampedPathPoints[0].Location.Z));
                    mesh.Positions.Add(new Point3D(clampedPathPoints[0].Location.X + thickness, clampedPathPoints[0].Location.Y, clampedPathPoints[0].Location.Z));
                    mesh.Positions.Add(new Point3D(clampedPathPoints[0].Location.X + thickness, clampedPathPoints[0].Location.Y - thickness, clampedPathPoints[0].Location.Z));
                    mesh.Positions.Add(new Point3D(clampedPathPoints[0].Location.X, clampedPathPoints[0].Location.Y - thickness, clampedPathPoints[0].Location.Z));
                    for (int i = 0; i < clampedPathPoints.Count - 1; i++)
                    {
                        #region Joint Points
                        //joinPoints(ref mesh, Points[i], Points[++i], thickness);
                        mesh.Positions.Add(new Point3D(clampedPathPoints[i + 1].Location.X, clampedPathPoints[i + 1].Location.Y, clampedPathPoints[i + 1].Location.Z));
                        mesh.Positions.Add(new Point3D(clampedPathPoints[i + 1].Location.X + thickness, clampedPathPoints[i + 1].Location.Y, clampedPathPoints[i + 1].Location.Z));
                        mesh.Positions.Add(new Point3D(clampedPathPoints[i + 1].Location.X + thickness, clampedPathPoints[i + 1].Location.Y - thickness, clampedPathPoints[i + 1].Location.Z));
                        mesh.Positions.Add(new Point3D(clampedPathPoints[i + 1].Location.X, clampedPathPoints[i + 1].Location.Y - thickness, clampedPathPoints[i + 1].Location.Z));

                        int k = mesh.Positions.Count - 8;

                        //Front face
                        miniBuildFace(ref mesh, 0 + k, 1 + k, 2 + k);
                        miniBuildFace(ref mesh, 0 + k, 2 + k, 3 + k);

                        //Back face
                        miniBuildFace(ref mesh, 4 + k, 5 + k, 6 + k);
                        miniBuildFace(ref mesh, 4 + k, 6 + k, 7 + k);

                        //Right face
                        miniBuildFace(ref mesh, 3 + k, 2 + k, 5 + k);
                        miniBuildFace(ref mesh, 3 + k, 5 + k, 4 + k);

                        //Top face
                        miniBuildFace(ref mesh, 0 + k, 3 + k, 7 + k);
                        miniBuildFace(ref mesh, 0 + k, 7 + k, 4 + k);

                        //Left face
                        miniBuildFace(ref mesh, 4 + k, 5 + k, 1 + k);
                        miniBuildFace(ref mesh, 4 + k, 1 + k, 0 + k);

                        //Bottom face
                        miniBuildFace(ref mesh, 2 + k, 1 + k, 5 + k);
                        miniBuildFace(ref mesh, 2 + k, 5 + k, 6 + k);
                        #endregion
                    }
                }
                #endregion
            }
            #endregion
            #endregion

            //mGeometry = new GeometryModel3D(mesh, diffuseMaterial);
            //mGeometry.Transform = new Transform3DGroup();

            mGeometry.Children.Add(new GeometryModel3D(mesh, dm));

            group.Children.Add(mGeometry);
        }

        private void miniBuildFace(ref MeshGeometry3D mesh, int one, int two, int three)
        {
            mesh.TriangleIndices.Add(one);
            mesh.TriangleIndices.Add(two);
            mesh.TriangleIndices.Add(three);

            mesh.TriangleIndices.Add(one);
            mesh.TriangleIndices.Add(three);
            mesh.TriangleIndices.Add(two);

        }

        private void rotateAndTransform3D(ref Point3D[] Points)
        {
            for (int i = 0; i < Points.Length; i++)
            {
                Points[i].X = Points[i].X / 100;
                Points[i].Y = 10 - (Points[i].Y / 100);
                Points[i].Z = 10 - (Points[i].Z / 100);
            }
        }

        #region Input Events


        private static readonly int width = 640;
        private static readonly int height = 480;
        private static readonly double scale = 1.0;
        private static readonly double depthThreshold = 2.0;

        private double depthScale = 0.0025;
        private short maxDepth = 0;

        // Variables for camera positioning
        double r = 4.0;
        double theta = 0.0;
        double phi = 0.0;

        Point3D lookAt = new Point3D(0, 0, 0);
        Point3D cameraOffset = new Point3D(0, 0, 5);   // offset from lookAt

        // Variables for mouse input
        Point mouseStart = new Point();
        int mouseWheelMin = 15000000;

        private void UserControl_KeyDown(object sender, KeyEventArgs e)
        {
            Key key = e.Key;

            double R;       // the radius of the circle cut along the xz plane
            switch (key)
            {
                case Key.Right:
                    // Orbit right
                    theta += 0.01;

                    //Find the Y value
                    cameraOffset.Y = r * System.Math.Sin(phi);

                    // Find the radius along the xz plane at the specified Y
                    R = r * System.Math.Cos(phi);

                    // Find the X and Z values
                    cameraOffset.X = R * System.Math.Sin(theta);
                    cameraOffset.Z = R * System.Math.Cos(theta);

                    this.camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                    this.camera.LookDirection = new Vector3D(lookAt.X - this.camera.Position.X, lookAt.Y - this.camera.Position.Y, lookAt.Z - this.camera.Position.Z);

                    break;
                case Key.Left:
                    // Orbit left
                    theta -= 0.01;

                    //Find the Y value
                    cameraOffset.Y = r * System.Math.Sin(phi);

                    // Find the radius along the xz plane at the specified Y
                    R = r * System.Math.Cos(phi);

                    // Find the X and Z values
                    cameraOffset.X = R * System.Math.Sin(theta);
                    cameraOffset.Z = R * System.Math.Cos(theta);

                    this.camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                    this.camera.LookDirection = new Vector3D(lookAt.X - this.camera.Position.X, lookAt.Y - this.camera.Position.Y, lookAt.Z - this.camera.Position.Z);

                    break;
                case Key.Up:
                    /*
                    // Orbit up
                    phi += 0.01;

                    // Clamp phi between -pi/2 and pi/2
                    phi = Math.Min(Math.PI / 2 - 0.001, phi);
                    phi = Math.Max(-Math.PI / 2 + 0.001, phi);

                    //Find the Y value
                    cameraOffset.Y = r * Math.Sin(phi);

                    // Find the radius along the xz plane at the specified Y
                    R = r * Math.Cos(phi);

                    // Find the X and Z values
                    cameraOffset.X = R * Math.Sin(theta);
                    cameraOffset.Z = R * Math.Cos(theta);
                    
                    this.Camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                    this.Camera.LookDirection = new Vector3D(lookAt.X - this.Camera.Position.X,
                                                                lookAt.Y - this.Camera.Position.Y,
                                                                lookAt.Z - this.Camera.Position.Z);
                     */
                    depthScale += 0.001;

                    break;
                case Key.Down:
                    // Orbit down
                    phi -= 0.01;

                    // Clamp phi between -pi/2 and pi/2
                    phi = System.Math.Min(System.Math.PI / 2 - 0.001, phi);
                    phi = System.Math.Max(-System.Math.PI / 2 + 0.001, phi);

                    //Find the Y value
                    cameraOffset.Y = r * System.Math.Sin(phi);

                    // Find the radius along the xz plane at the specified Y
                    R = r * System.Math.Cos(phi);

                    // Find the X and Z values
                    cameraOffset.X = R * System.Math.Sin(theta);
                    cameraOffset.Z = R * System.Math.Cos(theta);

                    this.camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                    this.camera.LookDirection = new Vector3D(lookAt.X - this.camera.Position.X, lookAt.Y - this.camera.Position.Y, lookAt.Z - this.camera.Position.Z);

                    break;
                case Key.Space:
                    // Reset the camera to the start position
                    r = 10.0;
                    theta = 0.0;
                    phi = 0.0;
                    lookAt.X = 3.2;
                    lookAt.Y = 2.4;
                    lookAt.Z = 0.0;

                    cameraOffset.X = 0.0;
                    cameraOffset.Y = 0.0;
                    cameraOffset.Z = 10.0;

                    this.camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                    this.camera.LookDirection = new Vector3D(lookAt.X - this.camera.Position.X,
                                                                lookAt.Y - this.camera.Position.Y,
                                                                lookAt.Z - this.camera.Position.Z);

                    break;
            }
        }

        private void UserControl_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            mouseStart = e.GetPosition(null);
        }

        private void UserControl_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (System.Math.Abs(e.Delta) < mouseWheelMin && e.Delta != 0)
                mouseWheelMin = System.Math.Abs(e.Delta);

            int ticks = e.Delta / mouseWheelMin;

            Vector3D cameraDirection = new Vector3D(cameraOffset.X, cameraOffset.Y, cameraOffset.Z);
            cameraDirection.Normalize();

            // Make sure that the radius will stay above 0
            // Distance traveled 
            if (r - (ticks * 0.1 * 1.732) > 0.1)
            {
                cameraOffset.X -= (0.1 * ticks) * cameraDirection.X;
                cameraOffset.Y -= (0.1 * ticks) * cameraDirection.Y;
                cameraOffset.Z -= (0.1 * ticks) * cameraDirection.Z;

                // Find the new radius
                r = System.Math.Sqrt(cameraOffset.X * cameraOffset.X + cameraOffset.Y * cameraOffset.Y + cameraOffset.Z * cameraOffset.Z);
            }

            this.camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
            this.camera.LookDirection = new Vector3D(lookAt.X - this.camera.Position.X,
                                                        lookAt.Y - this.camera.Position.Y,
                                                        lookAt.Z - this.camera.Position.Z);

            e.Handled = true;
        }

        private void UserControl_PreviewMouseMove(object sender, MouseEventArgs e)
        {

            Point currentPosition = e.GetPosition(null);
            Vector diff = new Vector(currentPosition.X - mouseStart.X, currentPosition.Y - mouseStart.Y);

            // If dragging with the left mouse button, orbit the camera
            if (e.LeftButton == MouseButtonState.Pressed &&
                e.RightButton == MouseButtonState.Released &&
                (System.Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                System.Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                mouseStart = currentPosition;

                phi += 0.005 * diff.Y;
                theta -= 0.005 * diff.X;

                // Clamp phi between -pi/2 and pi/2
                phi = System.Math.Min(System.Math.PI / 2 - 0.001, phi);
                phi = System.Math.Max(-System.Math.PI / 2 + 0.001, phi);

                //Find the Y value
                cameraOffset.Y = r * System.Math.Sin(phi);

                // Find the radius along the xz plane at the specified Y
                double R = r * System.Math.Cos(phi);

                // Find the X and Z values
                cameraOffset.X = R * System.Math.Sin(theta);
                cameraOffset.Z = R * System.Math.Cos(theta);

                this.camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                this.camera.LookDirection = new Vector3D(lookAt.X - this.camera.Position.X,
                                                            lookAt.Y - this.camera.Position.Y,
                                                            lookAt.Z - this.camera.Position.Z);
            }
            // Pan the camera on right click or mouswheel click
            else if (e.LeftButton == MouseButtonState.Released &&
                (e.RightButton == MouseButtonState.Pressed ||
                e.MiddleButton == MouseButtonState.Pressed) &&
                (System.Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                System.Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                mouseStart = currentPosition;

                double deltaY = 0.005 * diff.Y;
                double deltaX = -0.005 * diff.X;

                // Vectors for determining which direction the pan should move the camera
                Vector3D viewDirection = new Vector3D(this.camera.LookDirection.X, this.camera.LookDirection.Y, this.camera.LookDirection.Z);
                Vector3D upDirection = new Vector3D(0, 1, 0);
                Vector3D xDirection;
                Vector3D yDirection;

                // Normalize the view vector so all following vecotrs will be unit vectors
                viewDirection.Normalize();

                // Move along the camera's x direction
                xDirection = Vector3D.CrossProduct(viewDirection, upDirection);
                lookAt.X += xDirection.X * deltaX;
                lookAt.Y += xDirection.Y * deltaX;
                lookAt.Z += xDirection.Z * deltaX;

                // Move along the camera's y direction
                yDirection = Vector3D.CrossProduct(xDirection, viewDirection);
                lookAt.X += yDirection.X * deltaY;
                lookAt.Y += yDirection.Y * deltaY;
                lookAt.Z += yDirection.Z * deltaY;

                this.camera.Position = new Point3D(lookAt.X + cameraOffset.X, lookAt.Y + cameraOffset.Y, lookAt.Z + cameraOffset.Z);
                this.camera.LookDirection = new Vector3D(lookAt.X - this.camera.Position.X,
                                                            lookAt.Y - this.camera.Position.Y,
                                                            lookAt.Z - this.camera.Position.Z);
            }
        }

        #endregion
        /*
        public void ComparePath3d(ref List<List<AIMS.JointPath.PathPoint>> all3dPoints)
        {
            if (all3dPoints != null)
            {
                BuildSolid();

                all3dTransformedPoints = new List<List<Point3D[]>>();
                
                for (int i = 0; i < all3dPoints.Count; i++)
                {
                    List<Point3D[]> tempList = new List<Point3D[]>();
                    if (all3dPoints[i] != null)
                    {
                        for (int j = 0; j < all3dPoints[i].Count; j++)
                        {
                            Point3D[] temp = draw3dPoints(all3dPoints[i][j].PayLoadJoint.Points3D, new DiffuseMaterial(new SolidColorBrush(all3dPoints[i][j].PayLoadJoint.Color)));
                            tempList.Add(temp);
                        }
                    }
                    else
                    {
                        tempList.Add(null);
                    }
                    all3dTransformedPoints.Add(tempList);
                    tempList = null;
                }


                if (all3dPoints.Count == 2)
                {
                    goodSync = new int[all3dPoints[0].Count];
                    badSync = new int[all3dPoints[0].Count];
                    for (int j = 0; j < all3dPoints[0].Count; j++)
                    {
                        comparePath(all3dTransformedPoints[0][j], all3dTransformedPoints[1][j], ref goodSync[j], ref badSync[j]);
                        Console.WriteLine(all3dPoints[0][j].PayLoadJoint.Label.ToString().Substring(0, all3dPoints[0][j].PayLoadJoint.Label.ToString().Length - 1));
                        Console.WriteLine("\n===========================================");
                        Console.WriteLine("\n Good Sync = " + goodSync[j].ToString() + "\n Bad Sync = " + badSync[j].ToString() + "\n\n");
                    }
                }

                //if (all3dPoints[0].Count == 2)
                //{
                //    goodSync = new int[1];
                //    badSync = new int[1];
                //    //for (int j = 0; j < all3dPoints[0].Count; j++)
                //    //{
                //    comparePath(all3dTransformedPoints[0][0], all3dTransformedPoints[0][1], ref goodSync[0], ref badSync[0]);
                //    Console.WriteLine(all3dPoints[0][0].PayLoadJoint.Label.ToString().Substring(0, all3dPoints[0][0].PayLoadJoint.Label.ToString().Length - 1));
                //    Console.WriteLine("\n===========================================");
                //    Console.WriteLine("\n Good Sync = " + goodSync[0].ToString() + "\n Bad Sync = " + badSync[0].ToString() + "\n\n");
                //    //}
                //}

                //resetting
                
                for (int i = 0; i < all3dPoints.Count; i++)
                {
                    if (all3dPoints[i] != null)
                    {
                        for (int j = 0; j < all3dPoints[i].Count; j++)
                        {
                            all3dPoints[i][j].PayLoadJoint.Points3D = null;
                        }
                    }
                }
                
            }
        }*/

        public Point3D[] draw3dPoints(List<Point3D> all3dPointsList, DiffuseMaterial diffuseMaterial)
        {
            if (all3dPointsList == null)
            {
                return null;
            }

            if (all3dPointsList.Count < 30)
            {
                return null;
            }

            #region experiment test lagrange 3d

            Point3D[] points3d0 = new Point3D[all3dPointsList.Count - 30];   

            for (int i = 15, j = 0; i < all3dPointsList.Count - 15; i++, j++)
                points3d0[j] = all3dPointsList[i];

            all3dPointsList = null;
            all3dPointsList = new List<Point3D>();
            int smallestX = 1000;
            int largestX = 0;
            double scaleZ;

            scaleZ = change3D(ref points3d0);

            for (int i = 0; i < points3d0.Length; i++)
            {
                points3d0[i].Z = (points3d0[i].Z - 90) * 10;   // 90 was scaleZ
                all3dPointsList.Add(points3d0[i]);
                if (points3d0[i].X < smallestX) smallestX = (int)points3d0[i].X;
                if (points3d0[i].X > largestX) largestX = (int)points3d0[i].X;

            }

            List<double> XPoints = new List<double>();
            List<double> YPoints = new List<double>();
            List<double> ZPoints = new List<double>();
            
            rotateAndTransform3D(ref points3d0);
                
            ConnectPoints(points3d0, diffuseMaterial, 0.1);

            return points3d0;
                #endregion            
        }

        private void comparePath(Point3D[] p0, Point3D[] p1, ref int good, ref int bad)
        {
            List<double> t0Slopes = null;
            List<double> t1Slopes = null;
            // block for comparing
            
            t0Slopes = LagrangeInterpolation.get3dSlope(p0);
            
            t1Slopes = LagrangeInterpolation.get3dSlope(p1);

            good = 0;
            bad = 0;
            LagrangeInterpolation.comparePathSlopes(t0Slopes, t1Slopes, ref good, ref bad);
        }
 

        private void ConnectPoints(Point3D[] Points, DiffuseMaterial diffuseMaterial, double thickness)
        {
            MeshGeometry3D mesh = new MeshGeometry3D();         

            AddPoints(ref mesh, Points, thickness);

            //mGeometry = new GeometryModel3D(mesh, diffuseMaterial);
            //mGeometry.Transform = new Transform3DGroup();

            mGeometry.Children.Add(new GeometryModel3D(mesh, diffuseMaterial));
            mGeometry.Transform = new Transform3DGroup();
            group.Children.Add(mGeometry);
        }

        private void AddPoints(ref MeshGeometry3D mesh, Point3D[] Points, double thickness)
        {
            mesh.Positions.Add(new Point3D(Points[0].X, Points[0].Y, Points[0].Z));
            mesh.Positions.Add(new Point3D(Points[0].X + thickness, Points[0].Y, Points[0].Z));
            mesh.Positions.Add(new Point3D(Points[0].X + thickness, Points[0].Y - thickness, Points[0].Z));
            mesh.Positions.Add(new Point3D(Points[0].X, Points[0].Y - thickness, Points[0].Z));
            for (int i = 0; i < Points.Length - 1; )
            {
                joinPoints(ref mesh, Points[i], Points[++i], thickness);
            }

        }

        private void joinPoints(ref MeshGeometry3D mesh, Point3D firstPoint, Point3D secondPoint, double thickness)
        {
            mesh.Positions.Add(new Point3D(secondPoint.X, secondPoint.Y, secondPoint.Z));
            mesh.Positions.Add(new Point3D(secondPoint.X + thickness, secondPoint.Y, secondPoint.Z));
            mesh.Positions.Add(new Point3D(secondPoint.X + thickness, secondPoint.Y - thickness, secondPoint.Z));
            mesh.Positions.Add(new Point3D(secondPoint.X, secondPoint.Y - thickness, secondPoint.Z));

            int k = mesh.Positions.Count - 8;

            //Front face
            miniBuildFace(ref mesh, 0 + k, 1 + k, 2 + k);
            miniBuildFace(ref mesh, 0 + k, 2 + k, 3 + k);

            //Back face
            miniBuildFace(ref mesh, 4 + k, 5 + k, 6 + k);
            miniBuildFace(ref mesh, 4 + k, 6 + k, 7 + k);

            //Right face
            miniBuildFace(ref mesh, 3 + k, 2 + k, 5 + k);
            miniBuildFace(ref mesh, 3 + k, 5 + k, 4 + k);

            //Top face
            miniBuildFace(ref mesh, 0 + k, 3 + k, 7 + k);
            miniBuildFace(ref mesh, 0 + k, 7 + k, 4 + k);

            //Left face
            miniBuildFace(ref mesh, 4 + k, 5 + k, 1 + k);
            miniBuildFace(ref mesh, 4 + k, 1 + k, 0 + k);

            //Bottom face
            miniBuildFace(ref mesh, 2 + k, 1 + k, 5 + k);
            miniBuildFace(ref mesh, 2 + k, 5 + k, 6 + k);

        }       
        /*
        private void miniBuildFace(ref MeshGeometry3D mesh, int one, int two, int three)
        {
            mesh.TriangleIndices.Add(one);
            mesh.TriangleIndices.Add(two);
            mesh.TriangleIndices.Add(three);

            mesh.TriangleIndices.Add(one);
            mesh.TriangleIndices.Add(three);
            mesh.TriangleIndices.Add(two);

        }*/

        private void BuildSolid()
        {
            mGeometryGP = new Model3DGroup();

            BuildFace(1, 3, 6, 2, Colors.Black);
        }

        private void BuildFace(int one, int two, int three, int four, Color col)
        {
            MeshGeometry3D mesh = new MeshGeometry3D();

            mesh.Positions.Add(new Point3D(-1, 3, -1));
            mesh.Positions.Add(new Point3D(-1, -1, -1));
            mesh.Positions.Add(new Point3D(3, -1, -1));
            mesh.Positions.Add(new Point3D(-1, -1, 3));
            mesh.Positions.Add(new Point3D(-1, 3, 3));
            mesh.Positions.Add(new Point3D(3, 3, -1));
            mesh.Positions.Add(new Point3D(3, -1, 3));

            miniBuildFace(ref mesh, one, two, three);
            miniBuildFace(ref mesh, one, three, four);

            Brush br = new SolidColorBrush(col);
            br.Opacity = 1;//original 0.2

            // Geometry creation
            mGeometryGP.Children.Add(new GeometryModel3D(mesh, new DiffuseMaterial(br)));
            mGeometryGP.Transform = new Transform3DGroup();
            group.Children.Add(mGeometryGP);

        }

        /// <summary>
        /// Shows the FloorMesh in the PathDisplayViewport.
        /// </summary>
        public void ShowFloorMesh()
        {
            group.Children[2] = mGeometryGP;
        }

        /// <summary>
        /// Hides the FloorMesh (by replacing it with a temporary Transparent GeometryModel3D) in the PathDisplayViewport.
        /// </summary>
        public void HideFloorMesh()
        {
            group.Children[2] = new GeometryModel3D(new MeshGeometry3D(), new DiffuseMaterial(Brushes.Transparent));
        }
        
        private void Grid_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            camera.Position = new Point3D(camera.Position.X, camera.Position.Y, camera.Position.Z - e.Delta / 250D);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            camera.Position = new Point3D(camera.Position.X, camera.Position.Y, 5);
            mGeometryGP.Transform = new Transform3DGroup();
            //mGeometry.Transform = new Transform3DGroup();
        }

        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {

                if (mDown)
                {
                    Point pos = Mouse.GetPosition(this.PathDisplayViewport);
                    Point actualPos = new Point(pos.X - this.PathDisplayViewport.ActualWidth / 2, this.PathDisplayViewport.ActualHeight / 2 - pos.Y);
                    double dx = actualPos.X - mLastPos.X, dy = actualPos.Y - mLastPos.Y;

                    double mouseAngle = 0;

                    //unlock rotation
                    if (!rotateLock)
                    {
                        if (dx != 0 && dy != 0)
                        {
                            mouseAngle = System.Math.Asin(System.Math.Abs(dy) / System.Math.Sqrt(System.Math.Pow(dx, 2) + System.Math.Pow(dy, 2)));
                            if (dx < 0 && dy > 0) mouseAngle += System.Math.PI / 2;
                            else if (dx < 0 && dy < 0) mouseAngle += System.Math.PI;
                            else if (dx > 0 && dy < 0) mouseAngle += System.Math.PI * 1.5;
                        }
                        else if (dx == 0 && dy != 0) mouseAngle = System.Math.Sign(dy) > 0 ? System.Math.PI / 2 : System.Math.PI * 1.5;
                        else if (dx != 0 && dy == 0) mouseAngle = System.Math.Sign(dx) > 0 ? 0 : System.Math.PI;
                    }
                    else
                    {
                        //lock rotation
                        if (dx != 0) mouseAngle = System.Math.Sign(dx) > 0 ? 0 : System.Math.PI;
                    }

                    double axisAngle = mouseAngle + System.Math.PI / 2;

                    Vector3D axis = new Vector3D(System.Math.Cos(axisAngle) * 4, System.Math.Sin(axisAngle) * 4, 0);

                    double rotation = 0.01 * System.Math.Sqrt(System.Math.Pow(dx, 2) + System.Math.Pow(dy, 2));

                    Transform3DGroup group = mGeometryGP.Transform as Transform3DGroup;
                    Transform3DGroup group1 = mGeometry.Transform as Transform3DGroup;
                    QuaternionRotation3D r = new QuaternionRotation3D(new Quaternion(axis, rotation * 180 / System.Math.PI));
                    group.Children.Add(new RotateTransform3D(r));
                    group1.Children.Add(new RotateTransform3D(r));

                    mLastPos = actualPos;
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
            {
                return;
            }
            mDown = true;
            Point pos = Mouse.GetPosition(this.PathDisplayViewport);
            mLastPos = new Point(pos.X - this.PathDisplayViewport.ActualWidth / 2, this.PathDisplayViewport.ActualHeight / 2 - pos.Y);
        }

        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            mDown = false;
        }
        /*
        private void rotateAndTransform3D(ref Point3D[] Points )
        {
            for (int i = 0; i < Points.Length; i++ )
            {
                Points[i].X = Points[i].X / 100;
                Points[i].Y = 10 - (Points[i].Y / 100);
                Points[i].Z = 10 - (Points[i].Z / 100);
            }
        }
        */

        private double change3D(ref Point3D[] Points)
        {
            List<Point3D> PointsList = Points.ToList();

            double compZ = 10000;

            for (int i = 0; i < Points.Length; i++)
            {
                Points[i].X = Points[i].X / 640 * 1000;  
                Points[i].Y = Points[i].Y / 480 * 1000;

                if (compZ > Points[i].Z) 
                    compZ = Points[i].Z;
            }

            return compZ;
        }

        private void rotateAxes(ref Point3D[] Points)
        {
            for (int i = 0; i < Points.Length; i++)
            {
                Points[i].X = Points[i].X * 1000;
                Points[i].Y = (1 - Points[i].Y ) * 1000;
                Points[i].Z = (1 - Points[i].Z) * 1000;
            }
        }

        private void chklock_click(object sender, RoutedEventArgs e)
        {
            rotateLock = true;
        }

        private void chk_lock_Unchecked(object sender, RoutedEventArgs e)
        {
            rotateLock = false;
        }

        private void StartRecordButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.PathComparison == null)
            {
                if (KinectManager.StickySkeletonIDs.Length >= 2)
                {
                    bool foundSkeletons = false;
                    int skel1id = 0;
                    int skel2id = 0;

                    for (int i = 0; i < KinectManager.StickySkeletonIDs.Length && !foundSkeletons; i++)
                    {
                        if (KinectManager.GetSkeletonProfile(KinectManager.StickySkeletonIDs[i], true) != null)
                        {
                            if (skel1id == 0)
                            {
                                skel1id = KinectManager.StickySkeletonIDs[i].Value;
                            }
                            else if (skel2id == 0)
                            {
                                skel2id = KinectManager.StickySkeletonIDs[i].Value;
                                foundSkeletons = true;
                            }
                        }
                    }
                    this.PathComparison = new PathComparison();

                    this.PathComparison.SetSkeleton1(skel1id);
                    this.PathComparison.SetSkeleton2(skel2id);
                }

                this.PathComparison.StartRecording();
            }
            else
            {
                this.PathComparison.Restart();
            }
        }

        private void StopRecordButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.PathComparison != null)
            {
                this.PathComparison.StopRecording();

                this.RangeEndTime = this.PathComparison.RecordingEndTime;
                this.RangeStartTime = this.PathComparison.RecordingStartTime;
            }
        }

        private void DrawPathsButton_Click(object sender, RoutedEventArgs e)
        {
            RedrawPaths();
        }

        private void ResetPathsButton_Click(object sender, RoutedEventArgs e)
        {
            this.PathComparison.WipeClean();
        }

        /// <summary>
        /// Method called whenever an option button which is meant to alter the physical appearance of the Viewport3D. 
        /// 
        /// This will re-draw the visual using the new settings.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DrawOptionButton_Click(object sender, RoutedEventArgs e)
        {
            RedrawPaths();
        }
        
        private void DateRangeSlider_RangeChange(object sender, EventArgs data)
        {
            ReperformAnalysis();
            RedrawPaths();
        }

        private void PathColorCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RedrawPaths();
        }

        private void PathSmoothingSlider_MouseDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;   // Handle to avoid dragging the behind Viewport3D.
        }

        private void PathSmoothingSlider_MouseMove(object sender, MouseEventArgs e)
        {
            e.Handled = true;   // Handle to avoid dragging the behind Viewport3D.
        }

        private void PathSmoothingSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ReperformAnalysis();
            RedrawPaths();
        }

        private void PathSmoothingCB_IsCheckedChanged(object sender, RoutedEventArgs e)
        {
            ReperformAnalysis();
            RedrawPaths();
        }

        private void ClampRangeCB_IsCheckedChanged(object sender, RoutedEventArgs e)
        {
            ReperformAnalysis();
            RedrawPaths();
        }

        private void ReperformAnalysis()
        {
            if( this.PathComparison == null )
                return;

            this.PathComparison.PerformAnalysis( (this.UseRangeClamp ? this.DateRangeSlider.LowerValue : this.PathComparison.RecordingStartTime), (this.UseRangeClamp ?  this.DateRangeSlider.UpperValue : this.PathComparison.RecordingEndTime ), (this.UsePathSmoothing ? this.PathSmoothingValue : 1 ), this.SmoothBeforeClamp );
        }

        private void RedrawPaths()
        {
            if (this.PathComparison == null)
                return;

            this.DrawPaths((this.UseRangeClamp ? this.DateRangeSlider.LowerValue : this.PathComparison.RecordingStartTime), (this.UseRangeClamp ? this.DateRangeSlider.UpperValue : this.PathComparison.RecordingEndTime), (this.UsePathSmoothing ? this.PathSmoothingValue : 1), this.SmoothBeforeClamp);
        }

        private void SmoothBeforeClampCB_IsCheckedChanged(object sender, RoutedEventArgs e)
        {
            this.ReperformAnalysis();
            this.RedrawPaths();
        }

        private void SnapMinimumButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.DateRangeSlider != null)
            {
                this.DateRangeSlider.LowerValue = this.DateRangeSlider.Minimum;
            }
        }

        private void SnapMaximumButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.DateRangeSlider != null)
            {
                this.DateRangeSlider.UpperValue = this.DateRangeSlider.Maximum;
            }
        }
    }


    /// <summary>
    /// Class which holds an instance of a PathComparison between two ComparableSkeletons.
    /// </summary>
    public class PathComparison : INotifyPropertyChanged
    {
        private ComparableSkeleton skeleton1;
        public ComparableSkeleton Skeleton1
        {
            get { return skeleton1; }
            private set
            {
                if (skeleton1 == value)
                    skeleton1 = value;
                else
                {
                    skeleton1 = value;
                    NotifyPropertyChanged("Skeleton1");
                }
            }
        }
        private ComparableSkeleton skeleton2;
        public ComparableSkeleton Skeleton2
        {
            get { return skeleton2; }
            private set
            {
                if (skeleton2 == value)
                    skeleton2 = value;
                else
                {
                    skeleton2 = value;
                    NotifyPropertyChanged("Skeleton2");
                }
            }
        }

        private DateTime recordingStartTime;
        public DateTime RecordingStartTime
        {
            get { return recordingStartTime; }
            set
            {
                if (recordingStartTime == value)
                    recordingStartTime = value;
                else
                {
                    recordingStartTime = value;
                    NotifyPropertyChanged("RecordingStartTime");
                }
            }
        }
        private DateTime recordingEndTime;
        public DateTime RecordingEndTime
        {
            get { return recordingEndTime; }
            set
            {
                if (recordingEndTime == value)
                    recordingEndTime = value;
                else
                {
                    recordingEndTime = value;
                    NotifyPropertyChanged("RecordingEndTime");
                }
            }
        }

        public TimeSpan RecordingLength { get { return recordingEndTime - recordingStartTime; } }

        private bool recording = false;
        public bool Recording
        {
            get { return recording; }
            set
            {
                if (recording == value)
                    recording = value;
                else
                {
                    recording = value;
                    NotifyPropertyChanged("Recording");
                }

            }
        }

        #region Syncs
        private double headGoodSync;
        public double HeadGoodSync
        {
            get { return headGoodSync; }
            set
            {
                if (headGoodSync == value)
                    headGoodSync = value;
                else
                {
                    headGoodSync = value;
                    NotifyPropertyChanged("HeadGoodSync");
                }
            }
        }
        private double headBadSync;
        public double HeadBadSync
        {
            get { return headBadSync; }
            set
            {
                if (headBadSync == value)
                    headBadSync = value;
                else
                {
                    headBadSync = value;
                    NotifyPropertyChanged("HeadBadSync");
                }
            }
        }
        private double neckGoodSync;
        public double NeckGoodSync
        {
            get { return neckGoodSync; }
            set
            {
                if (neckGoodSync == value)
                    neckGoodSync = value;
                else
                {
                    neckGoodSync = value;
                    NotifyPropertyChanged("NeckGoodSync");
                }
            }
        }
        private double neckBadSync;
        public double NeckBadSync
        {
            get { return neckBadSync; }
            set
            {
                if (neckBadSync == value)
                    neckBadSync = value;
                else
                {
                    neckBadSync = value;
                    NotifyPropertyChanged("NeckBadSync");
                }
            }
        }
        private double shoulderCenterGoodSync;
        public double ShoulderCenterGoodSync
        {
            get { return shoulderCenterGoodSync; }
            set
            {
                if (shoulderCenterGoodSync == value)
                    shoulderCenterGoodSync = value;
                else
                {
                    shoulderCenterGoodSync = value;
                    NotifyPropertyChanged("ShoulderCenterGoodSync");
                }
            }
        }
        private double shoulderCenterBadSync;
        public double ShoulderCenterBadSync
        {
            get { return shoulderCenterBadSync; }
            set
            {
                if (shoulderCenterBadSync == value)
                    shoulderCenterBadSync = value;
                else
                {
                    shoulderCenterBadSync = value;
                    NotifyPropertyChanged("ShoulderCenterBadSync");
                }
            }
        }
        private double shoulderRightGoodSync;
        public double ShoulderRightGoodSync
        {
            get { return shoulderRightGoodSync; }
            set
            {
                if (shoulderRightGoodSync == value)
                    shoulderRightGoodSync = value;
                else
                {
                    shoulderRightGoodSync = value;
                    NotifyPropertyChanged("ShoulderRightGoodSync");
                }
            }
        }
        private double shoulderRightBadSync;
        public double ShoulderRightBadSync
        {
            get { return shoulderRightBadSync; }
            set
            {
                if (shoulderRightBadSync == value)
                    shoulderRightBadSync = value;
                else
                {
                    shoulderRightBadSync = value;
                    NotifyPropertyChanged("ShoulderRightBadSync");
                }
            }
        }
        private double shoulderLeftGoodSync;
        public double ShoulderLeftGoodSync
        {
            get { return shoulderLeftGoodSync; }
            set
            {
                if (shoulderLeftGoodSync == value)
                    shoulderLeftGoodSync = value;
                else
                {
                    shoulderLeftGoodSync = value;
                    NotifyPropertyChanged("ShoulderLeftGoodSync");
                }
            }
        }
        private double shoulderLeftBadSync;
        public double ShoulderLeftBadSync
        {
            get { return shoulderLeftBadSync; }
            set
            {
                if (shoulderLeftBadSync == value)
                    shoulderLeftBadSync = value;
                else
                {
                    shoulderLeftBadSync = value;
                    NotifyPropertyChanged("ShoulderLeftBadSync");
                }
            }
        }
        private double elbowRightGoodSync;
        public double ElbowRightGoodSync
        {
            get { return elbowRightGoodSync; }
            set
            {
                if (elbowRightGoodSync == value)
                    elbowRightGoodSync = value;
                else
                {
                    elbowRightGoodSync = value;
                    NotifyPropertyChanged("ElbowRightGoodSync");
                }
            }
        }
        private double elbowRightBadSync;
        public double ElbowRightBadSync
        {
            get { return elbowRightBadSync; }
            set
            {
                if (elbowRightBadSync == value)
                    elbowRightBadSync = value;
                else
                {
                    elbowRightBadSync = value;
                    NotifyPropertyChanged("ElbowRightBadSync");
                }
            }
        }
        private double elbowLeftGoodSync;
        public double ElbowLeftGoodSync
        {
            get { return elbowLeftGoodSync; }
            set
            {
                if (elbowLeftGoodSync == value)
                    elbowLeftGoodSync = value;
                else
                {
                    elbowLeftGoodSync = value;
                    NotifyPropertyChanged("ElbowLeftGoodSync");
                }
            }
        }
        private double elbowLeftBadSync;
        public double ElbowLeftBadSync
        {
            get { return elbowLeftBadSync; }
            set
            {
                if (elbowLeftBadSync == value)
                    elbowLeftBadSync = value;
                else
                {
                    elbowLeftBadSync = value;
                    NotifyPropertyChanged("ElbowLeftBadSync");
                }
            }
        }
        private double wristRightGoodSync;
        public double WristRightGoodSync
        {
            get { return wristRightGoodSync; }
            set
            {
                if (wristRightGoodSync == value)
                    wristRightGoodSync = value;
                else
                {
                    wristRightGoodSync = value;
                    NotifyPropertyChanged("WristRightGoodSync");
                }
            }
        }
        private double wristRightBadSync;
        public double WristRightBadSync
        {
            get { return wristRightBadSync; }
            set
            {
                if (wristRightBadSync == value)
                    wristRightBadSync = value;
                else
                {
                    wristRightBadSync = value;
                    NotifyPropertyChanged("WristRightBadSync");
                }
            }
        }
        private double wristLeftGoodSync;
        public double WristLeftGoodSync
        {
            get { return wristLeftGoodSync; }
            set
            {
                if (wristLeftGoodSync == value)
                    wristLeftGoodSync = value;
                else
                {
                    wristLeftGoodSync = value;
                    NotifyPropertyChanged("WristLeftGoodSync");
                }
            }
        }
        private double wristLeftBadSync;
        public double WristLeftBadSync
        {
            get { return wristLeftBadSync; }
            set
            {
                if (wristLeftBadSync == value)
                    wristLeftBadSync = value;
                else
                {
                    wristLeftBadSync = value;
                    NotifyPropertyChanged("WristLeftBadSync");
                }
            }
        }
        private double handRightGoodSync;
        public double HandRightGoodSync
        {
            get { return handRightGoodSync; }
            set
            {
                if (handRightGoodSync == value)
                    handRightGoodSync = value;
                else
                {
                    handRightGoodSync = value;
                    NotifyPropertyChanged("HandRightGoodSync");
                }
            }
        }
        private double handRightBadSync;
        public double HandRightBadSync
        {
            get { return handRightBadSync; }
            set
            {
                if (handRightBadSync == value)
                    handRightBadSync = value;
                else
                {
                    handRightBadSync = value;
                    NotifyPropertyChanged("HandRightBadSync");
                }
            }
        }
        private double handLeftGoodSync;
        public double HandLeftGoodSync
        {
            get { return handLeftGoodSync; }
            set
            {
                if (handLeftGoodSync == value)
                    handLeftGoodSync = value;
                else
                {
                    handLeftGoodSync = value;
                    NotifyPropertyChanged("HandLeftGoodSync");
                }
            }
        }
        private double handLeftBadSync;
        public double HandLeftBadSync
        {
            get { return handLeftBadSync; }
            set
            {
                if (handLeftBadSync == value)
                    handLeftBadSync = value;
                else
                {
                    handLeftBadSync = value;
                    NotifyPropertyChanged("HandLeftBadSync");
                }
            }
        }
        private double spineGoodSync;
        public double SpineGoodSync
        {
            get { return spineGoodSync; }
            set
            {
                if (spineGoodSync == value)
                    spineGoodSync = value;
                else
                {
                    spineGoodSync = value;
                    NotifyPropertyChanged("SpineGoodSync");
                }
            }
        }
        private double spineBadSync;
        public double SpineBadSync
        {
            get { return spineBadSync; }
            set
            {
                if (spineBadSync == value)
                    spineBadSync = value;
                else
                {
                    spineBadSync = value;
                    NotifyPropertyChanged("SpineBadSync");
                }
            }
        }
        private double hipCenterGoodSync;
        public double HipCenterGoodSync
        {
            get { return hipCenterGoodSync; }
            set
            {
                if (hipCenterGoodSync == value)
                    hipCenterGoodSync = value;
                else
                {
                    hipCenterGoodSync = value;
                    NotifyPropertyChanged("HipCenterGoodSync");
                }
            }
        }
        private double hipCenterBadSync;
        public double HipCenterBadSync
        {
            get { return hipCenterBadSync; }
            set
            {
                if (hipCenterBadSync == value)
                    hipCenterBadSync = value;
                else
                {
                    hipCenterBadSync = value;
                    NotifyPropertyChanged("HipCenterBadSync");
                }
            }
        }
        private double hipRightGoodSync;
        public double HipRightGoodSync
        {
            get { return hipRightGoodSync; }
            set
            {
                if (hipRightGoodSync == value)
                    hipRightGoodSync = value;
                else
                {
                    hipRightGoodSync = value;
                    NotifyPropertyChanged("HipRightGoodSync");
                }
            }
        }
        private double hipRightBadSync;
        public double HipRightBadSync
        {
            get { return hipRightBadSync; }
            set
            {
                if (hipRightBadSync == value)
                    hipRightBadSync = value;
                else
                {
                    hipRightBadSync = value;
                    NotifyPropertyChanged("HipRightBadSync");
                }
            }
        }
        private double hipLeftGoodSync;
        public double HipLeftGoodSync
        {
            get { return hipLeftGoodSync; }
            set
            {
                if (hipLeftGoodSync == value)
                    hipLeftGoodSync = value;
                else
                {
                    hipLeftGoodSync = value;
                    NotifyPropertyChanged("HipLeftGoodSync");
                }
            }
        }
        private double hipLeftBadSync;
        public double HipLeftBadSync
        {
            get { return hipLeftBadSync; }
            set
            {
                if (hipLeftBadSync == value)
                    hipLeftBadSync = value;
                else
                {
                    hipLeftBadSync = value;
                    NotifyPropertyChanged("HipLeftBadSync");
                }
            }
        }
        private double kneeRightGoodSync;
        public double KneeRightGoodSync
        {
            get { return kneeRightGoodSync; }
            set
            {
                if (kneeRightGoodSync == value)
                    kneeRightGoodSync = value;
                else
                {
                    kneeRightGoodSync = value;
                    NotifyPropertyChanged("KneeRightGoodSync");
                }
            }
        }
        private double kneeRightBadSync;
        public double KneeRightBadSync
        {
            get { return kneeRightBadSync; }
            set
            {
                if (kneeRightBadSync == value)
                    kneeRightBadSync = value;
                else
                {
                    kneeRightBadSync = value;
                    NotifyPropertyChanged("KneeRightBadSync");
                }
            }
        }
        private double kneeLeftGoodSync;
        public double KneeLeftGoodSync
        {
            get { return kneeLeftGoodSync; }
            set
            {
                if (kneeLeftGoodSync == value)
                    kneeLeftGoodSync = value;
                else
                {
                    kneeLeftGoodSync = value;
                    NotifyPropertyChanged("KneeLeftGoodSync");
                }
            }
        }
        private double kneeLeftBadSync;
        public double KneeLeftBadSync
        {
            get { return kneeLeftBadSync; }
            set
            {
                if (kneeLeftBadSync == value)
                    kneeLeftBadSync = value;
                else
                {
                    kneeLeftBadSync = value;
                    NotifyPropertyChanged("KneeLeftBadSync");
                }
            }
        }
        private double ankleRightGoodSync;
        public double AnkleRightGoodSync
        {
            get { return ankleRightGoodSync; }
            set
            {
                if (ankleRightGoodSync == value)
                    ankleRightGoodSync = value;
                else
                {
                    ankleRightGoodSync = value;
                    NotifyPropertyChanged("AnkleRightGoodSync");
                }
            }
        }
        private double ankleRightBadSync;
        public double AnkleRightBadSync
        {
            get { return ankleRightBadSync; }
            set
            {
                if (ankleRightBadSync == value)
                    ankleRightBadSync = value;
                else
                {
                    ankleRightBadSync = value;
                    NotifyPropertyChanged("AnkleRightBadSync");
                }
            }
        }
        private double ankleLeftGoodSync;
        public double AnkleLeftGoodSync
        {
            get { return ankleLeftGoodSync; }
            set
            {
                if (ankleLeftGoodSync == value)
                    ankleLeftGoodSync = value;
                else
                {
                    ankleLeftGoodSync = value;
                    NotifyPropertyChanged("AnkleLeftGoodSync");
                }
            }
        }
        private double ankleLeftBadSync;
        public double AnkleLeftBadSync
        {
            get { return ankleLeftBadSync; }
            set
            {
                if (ankleLeftBadSync == value)
                    ankleLeftBadSync = value;
                else
                {
                    ankleLeftBadSync = value;
                    NotifyPropertyChanged("AnkleLeftBadSync");
                }
            }
        }
        private double footRightGoodSync;
        public double FootRightGoodSync
        {
            get { return footRightGoodSync; }
            set
            {
                if (footRightGoodSync == value)
                    footRightGoodSync = value;
                else
                {
                    footRightGoodSync = value;
                    NotifyPropertyChanged("FootRightGoodSync");
                }
            }
        }
        private double footRightBadSync;
        public double FootRightBadSync
        {
            get { return footRightBadSync; }
            set
            {
                if (footRightBadSync == value)
                    footRightBadSync = value;
                else
                {
                    footRightBadSync = value;
                    NotifyPropertyChanged("FootRightBadSync");
                }
            }
        }
        private double footLeftGoodSync;
        public double FootLeftGoodSync
        {
            get { return footLeftGoodSync; }
            set
            {
                if (footLeftGoodSync == value)
                    footLeftGoodSync = value;
                else
                {
                    footLeftGoodSync = value;
                    NotifyPropertyChanged("FootLeftGoodSync");
                }
            }
        }
        private double footLeftBadSync;
        public double FootLeftBadSync
        {
            get { return footLeftBadSync; }
            set
            {
                if (footLeftBadSync == value)
                    footLeftBadSync = value;
                else
                {
                    footLeftBadSync = value;
                    NotifyPropertyChanged("FootLeftBadSync");
                }
            }
        }
        #endregion

        public PathComparison()
        {
            KinectManager.AllDataReady += KinectManager_AllDataReady;
        }

        /// <summary>
        /// Set a SkeletonProfile as the skeleton to compare against.
        /// 
        /// This function creates a ComparableSkeleton and sets it as the Comparison's Skeleton1.
        /// 
        /// Specify which Joints to track, otherwise all are tracked.
        /// </summary>
        /// <param name="skeleton"></param>
        /// <param name="trackHead"></param>
        /// <param name="trackShoulderCenter"></param>
        /// <param name="trackShoulderRight"></param>
        /// <param name="trackShoulderLeft"></param>
        /// <param name="trackElbowRight"></param>
        /// <param name="trackElbowLeft"></param>
        /// <param name="trackWristRight"></param>
        /// <param name="trackWristLeft"></param>
        /// <param name="trackHandRight"></param>
        /// <param name="trackHandLeft"></param>
        /// <param name="trackSpine"></param>
        /// <param name="trackHipCenter"></param>
        /// <param name="trackHipRight"></param>
        /// <param name="trackHipLeft"></param>
        /// <param name="trackKneeRight"></param>
        /// <param name="trackKneeLeft"></param>
        /// <param name="trackAnkleRight"></param>
        /// <param name="trackAnkleLeft"></param>
        /// <param name="trackFootRight"></param>
        /// <param name="trackFootLeft"></param>
        public void SetSkeleton1(SkeletonProfile skeleton, bool trackHead = true, bool trackShoulderCenter = true, bool trackShoulderRight = true, bool trackShoulderLeft = true, bool trackElbowRight = true, bool trackElbowLeft = true, bool trackWristRight = true, bool trackWristLeft = true, bool trackHandRight = true, bool trackHandLeft = true, bool trackSpine = true, bool trackHipCenter = true, bool trackHipRight = true, bool trackHipLeft = true, bool trackKneeRight = true, bool trackKneeLeft = true, bool trackAnkleRight = true, bool trackAnkleLeft = true, bool trackFootRight = true, bool trackFootLeft = true)
        {
            this.SetSkeleton1(skeleton.CurrentTrackingID.Value, trackHead, trackShoulderCenter, trackShoulderRight, trackShoulderLeft, trackElbowRight, trackElbowLeft, trackWristRight, trackWristLeft, trackHandRight, trackHandLeft, trackSpine, trackHipCenter, trackHipRight, trackHipLeft, trackKneeRight, trackKneeLeft, trackAnkleRight, trackAnkleLeft, trackFootRight, trackFootLeft);
        }

        public void SetSkeleton1(int skeletonID, bool trackHead = true, bool trackShoulderCenter = true, bool trackShoulderRight = true, bool trackShoulderLeft = true, bool trackElbowRight = true, bool trackElbowLeft = true, bool trackWristRight = true, bool trackWristLeft = true, bool trackHandRight = true, bool trackHandLeft = true, bool trackSpine = true, bool trackHipCenter = true, bool trackHipRight = true, bool trackHipLeft = true, bool trackKneeRight = true, bool trackKneeLeft = true, bool trackAnkleRight = true, bool trackAnkleLeft = true, bool trackFootRight = true, bool trackFootLeft = true)
        {
            ComparableSkeleton skele = new ComparableSkeleton(skeletonID);
            skele.Color = Colors.Orange;

            skele.TrackHead = trackHead;
            skele.TrackShoulderCenter = trackShoulderCenter;
            skele.TrackShoulderRight = trackShoulderRight;
            skele.TrackShoulderLeft = trackShoulderLeft;
            skele.TrackElbowRight = trackElbowRight;
            skele.TrackElbowLeft = trackElbowLeft;
            skele.TrackWristRight = trackWristRight;
            skele.TrackWristLeft = trackWristLeft;
            skele.TrackHandRight = trackHandRight;
            skele.TrackHandLeft = trackHandLeft;
            skele.TrackSpine = trackSpine;
            skele.TrackHipCenter = trackHipCenter;
            skele.TrackHipRight = trackHipRight;
            skele.TrackHipLeft = trackHipLeft;
            skele.TrackKneeRight = trackKneeRight;
            skele.TrackKneeLeft = trackKneeLeft;
            skele.TrackAnkleRight = trackAnkleRight;
            skele.TrackAnkleLeft = trackAnkleLeft;
            skele.TrackFootRight = trackFootRight;
            skele.TrackFootLeft = trackFootLeft;

            this.Skeleton1 = skele;
        }

        /// <summary>
        /// Set a SkeletonProfile as the skeleton to compare with.
        /// 
        /// This function creates a ComparableSkeleton and sets it as the Comparison's Skeleton2.
        /// 
        /// Specify which Joints to track, otherwise all are tracked.
        /// </summary>
        /// <param name="skeleton"></param>
        /// <param name="trackHead"></param>
        /// <param name="trackShoulderCenter"></param>
        /// <param name="trackShoulderRight"></param>
        /// <param name="trackShoulderLeft"></param>
        /// <param name="trackElbowRight"></param>
        /// <param name="trackElbowLeft"></param>
        /// <param name="trackWristRight"></param>
        /// <param name="trackWristLeft"></param>
        /// <param name="trackHandRight"></param>
        /// <param name="trackHandLeft"></param>
        /// <param name="trackSpine"></param>
        /// <param name="trackHipCenter"></param>
        /// <param name="trackHipRight"></param>
        /// <param name="trackHipLeft"></param>
        /// <param name="trackKneeRight"></param>
        /// <param name="trackKneeLeft"></param>
        /// <param name="trackAnkleRight"></param>
        /// <param name="trackAnkleLeft"></param>
        /// <param name="trackFootRight"></param>
        /// <param name="trackFootLeft"></param>
        public void SetSkeleton2(SkeletonProfile skeleton, bool trackHead = true, bool trackShoulderCenter = true, bool trackShoulderRight = true, bool trackShoulderLeft = true, bool trackElbowRight = true, bool trackElbowLeft = true, bool trackWristRight = true, bool trackWristLeft = true, bool trackHandRight = true, bool trackHandLeft = true, bool trackSpine = true, bool trackHipCenter = true, bool trackHipRight = true, bool trackHipLeft = true, bool trackKneeRight = true, bool trackKneeLeft = true, bool trackAnkleRight = true, bool trackAnkleLeft = true, bool trackFootRight = true, bool trackFootLeft = true)
        {
            this.SetSkeleton2(skeleton.CurrentTrackingID.Value, trackHead, trackShoulderCenter, trackShoulderRight, trackShoulderLeft, trackElbowRight, trackElbowLeft, trackWristRight, trackWristLeft, trackHandRight, trackHandLeft, trackSpine, trackHipCenter, trackHipRight, trackHipLeft, trackKneeRight, trackKneeLeft, trackAnkleRight, trackAnkleLeft, trackFootRight, trackFootLeft);
        }

        public void SetSkeleton2(int skeletonID, bool trackHead = true, bool trackShoulderCenter = true, bool trackShoulderRight = true, bool trackShoulderLeft = true, bool trackElbowRight = true, bool trackElbowLeft = true, bool trackWristRight = true, bool trackWristLeft = true, bool trackHandRight = true, bool trackHandLeft = true, bool trackSpine = true, bool trackHipCenter = true, bool trackHipRight = true, bool trackHipLeft = true, bool trackKneeRight = true, bool trackKneeLeft = true, bool trackAnkleRight = true, bool trackAnkleLeft = true, bool trackFootRight = true, bool trackFootLeft = true)
        {
            ComparableSkeleton skele = new ComparableSkeleton(skeletonID);
            skele.Color = Colors.Red;

            skele.TrackHead = trackHead;
            skele.TrackShoulderCenter = trackShoulderCenter;
            skele.TrackShoulderRight = trackShoulderRight;
            skele.TrackShoulderLeft = trackShoulderLeft;
            skele.TrackElbowRight = trackElbowRight;
            skele.TrackElbowLeft = trackElbowLeft;
            skele.TrackWristRight = trackWristRight;
            skele.TrackWristLeft = trackWristLeft;
            skele.TrackHandRight = trackHandRight;
            skele.TrackHandLeft = trackHandLeft;
            skele.TrackSpine = trackSpine;
            skele.TrackHipCenter = trackHipCenter;
            skele.TrackHipRight = trackHipRight;
            skele.TrackHipLeft = trackHipLeft;
            skele.TrackKneeRight = trackKneeRight;
            skele.TrackKneeLeft = trackKneeLeft;
            skele.TrackAnkleRight = trackAnkleRight;
            skele.TrackAnkleLeft = trackAnkleLeft;
            skele.TrackFootRight = trackFootRight;
            skele.TrackFootLeft = trackFootLeft;

            this.Skeleton2 = skele;
        }

        private void KinectManager_AllDataReady()
        {
            // Update with the KinectManager's AllDataReady event. (15 times a second).
            DoRecordingUpdate();
        }

        private void DoRecordingUpdate()
        {
            if (Recording)
            {
                if (skeleton1 != null && skeleton1.SkeletonID != null)
                {
                    skeleton1.TrackPaths(); // continue to build the path for skeleton1
                }


                if (skeleton2 != null && skeleton2.SkeletonID != null)
                {
                    skeleton2.TrackPaths();  // continue to build the path for skeleton2
                }
            }
        }

        public void StartRecording()
        {
            if (!Recording) // Before recording has begun.
            {
                if (skeleton1 == null || skeleton1.SkeletonID == null)
                {
                    // No skeleton found, cannot record joint positions.
                    return;
                }

                if (skeleton2 == null || skeleton2.SkeletonID == null)
                {
                    // No skeleton found, cannot record joint positions.
                    return;
                }

                this.RecordingStartTime = DateTime.Now;

                Recording = true;

                System.Diagnostics.Debug.WriteLine(String.Format("{0:MM/dd/yyyy hh:mm:ss.fff tt}:: Recording Began", this.RecordingStartTime));
            }
        }

        public void StopRecording()
        {
            if (Recording)
            {
                Recording = false;

                this.RecordingEndTime = DateTime.Now;

                System.Diagnostics.Debug.WriteLine(String.Format("{0:MM/dd/yyyy hh:mm:ss.fff tt}:: Recording Ended. ({1} seconds elapsed)", this.RecordingEndTime, this.RecordingLength.TotalSeconds));

                //PerformAnalysis( this.RecordingStartTime, this.RecordingEndTime );
            }
        }

        public void PerformAnalysis( DateTime start, DateTime end, int smoothingValue, bool smoothBeforeClamp )
        {
            double good = 0;
            double bad = 0;

            if (this.Skeleton1 == null || this.Skeleton2 == null)
                return;

            LagrangeInterpolation.ComparePaths(ClampAndSmooth(this.Skeleton1.HeadPath, start, end, smoothingValue, smoothBeforeClamp), ClampAndSmooth(this.Skeleton2.HeadPath, start, end, smoothingValue, smoothBeforeClamp), out good, out bad);
            this.HeadGoodSync = good;
            this.HeadBadSync = bad;

            //System.Diagnostics.Debug.WriteLine(String.Format("Head:: Good: {0}, Bad: {1}", this.HeadGoodSync, this.HeadBadSync));

            LagrangeInterpolation.ComparePaths(ClampAndSmooth(this.Skeleton1.ShoulderCenterPath, start, end, smoothingValue, smoothBeforeClamp), ClampAndSmooth(this.Skeleton2.ShoulderCenterPath, start, end, smoothingValue, smoothBeforeClamp), out good, out bad);
            this.ShoulderCenterGoodSync = good;
            this.ShoulderCenterBadSync = bad;

            //System.Diagnostics.Debug.WriteLine(String.Format("ShoulderCenter:: Good: {0}, Bad: {1}", this.ShoulderCenterGoodSync, this.ShoulderCenterBadSync));

            LagrangeInterpolation.ComparePaths(ClampAndSmooth(this.Skeleton1.ShoulderRightPath, start, end, smoothingValue, smoothBeforeClamp), ClampAndSmooth(this.Skeleton2.ShoulderRightPath, start, end, smoothingValue, smoothBeforeClamp), out good, out bad);
            this.ShoulderRightGoodSync = good;
            this.ShoulderRightBadSync = bad;

            //System.Diagnostics.Debug.WriteLine(String.Format("ShoulderRight:: Good: {0}, Bad: {1}", this.ShoulderRightGoodSync, this.ShoulderRightBadSync));

            LagrangeInterpolation.ComparePaths(ClampAndSmooth(this.Skeleton1.ShoulderLeftPath, start, end, smoothingValue, smoothBeforeClamp), ClampAndSmooth(this.Skeleton2.ShoulderLeftPath, start, end, smoothingValue, smoothBeforeClamp), out good, out bad);
            this.ShoulderLeftGoodSync = good;
            this.ShoulderLeftBadSync = bad;

            //System.Diagnostics.Debug.WriteLine(String.Format("ShoulderLeft:: Good: {0}, Bad: {1}", this.ShoulderLeftGoodSync, this.ShoulderLeftBadSync));

            LagrangeInterpolation.ComparePaths(ClampAndSmooth(this.Skeleton1.ElbowRightPath, start, end, smoothingValue, smoothBeforeClamp), ClampAndSmooth(this.Skeleton2.ElbowRightPath, start, end, smoothingValue, smoothBeforeClamp), out good, out bad);
            this.ElbowRightGoodSync = good;
            this.ElbowRightBadSync = bad;

            //System.Diagnostics.Debug.WriteLine(String.Format("ElbowRight:: Good: {0}, Bad: {1}", this.ElbowRightGoodSync, this.ElbowRightBadSync));

            LagrangeInterpolation.ComparePaths(ClampAndSmooth(this.Skeleton1.ElbowLeftPath, start, end, smoothingValue, smoothBeforeClamp), ClampAndSmooth(this.Skeleton2.ElbowLeftPath, start, end, smoothingValue, smoothBeforeClamp), out good, out bad);
            this.ElbowLeftGoodSync = good;
            this.ElbowLeftBadSync = bad;

            //System.Diagnostics.Debug.WriteLine(String.Format("ElbowLeft:: Good: {0}, Bad: {1}", this.ElbowLeftGoodSync, this.ElbowLeftBadSync));

            LagrangeInterpolation.ComparePaths(ClampAndSmooth(this.Skeleton1.WristRightPath, start, end, smoothingValue, smoothBeforeClamp), ClampAndSmooth(this.Skeleton2.WristRightPath, start, end, smoothingValue, smoothBeforeClamp), out good, out bad);
            this.WristRightGoodSync = good;
            this.WristRightBadSync = bad;

            //System.Diagnostics.Debug.WriteLine(String.Format("WristRight:: Good: {0}, Bad: {1}", this.WristRightGoodSync, this.WristRightBadSync));

            LagrangeInterpolation.ComparePaths(ClampAndSmooth(this.Skeleton1.WristLeftPath, start, end, smoothingValue, smoothBeforeClamp), ClampAndSmooth(this.Skeleton2.WristLeftPath, start, end, smoothingValue, smoothBeforeClamp), out good, out bad);
            this.WristLeftGoodSync = good;
            this.WristLeftBadSync = bad;

            //System.Diagnostics.Debug.WriteLine(String.Format("WristLeft:: Good: {0}, Bad: {1}", this.WristLeftGoodSync, this.WristLeftBadSync));

            LagrangeInterpolation.ComparePaths(ClampAndSmooth(this.Skeleton1.HandRightPath, start, end, smoothingValue, smoothBeforeClamp), ClampAndSmooth(this.Skeleton2.HandRightPath, start, end, smoothingValue, smoothBeforeClamp), out good, out bad);
            this.HandRightGoodSync = good;
            this.HandRightBadSync = bad;

            //System.Diagnostics.Debug.WriteLine(String.Format("HandRight:: Good: {0}, Bad: {1}", this.HandRightGoodSync, this.HandRightBadSync));

            LagrangeInterpolation.ComparePaths(ClampAndSmooth(this.Skeleton1.HandLeftPath, start, end, smoothingValue, smoothBeforeClamp), ClampAndSmooth(this.Skeleton2.HandLeftPath, start, end, smoothingValue, smoothBeforeClamp), out good, out bad);
            this.HandLeftGoodSync = good;
            this.HandLeftBadSync = bad;

            //System.Diagnostics.Debug.WriteLine(String.Format("HandLeft:: Good: {0}, Bad: {1}", this.HandLeftGoodSync, this.HandLeftBadSync));

            LagrangeInterpolation.ComparePaths(ClampAndSmooth(this.Skeleton1.SpinePath, start, end, smoothingValue, smoothBeforeClamp), ClampAndSmooth(this.Skeleton2.SpinePath, start, end, smoothingValue, smoothBeforeClamp), out good, out bad);
            this.SpineGoodSync = good;
            this.SpineBadSync = bad;

            //System.Diagnostics.Debug.WriteLine(String.Format("Spine:: Good: {0}, Bad: {1}", this.SpineGoodSync, this.SpineBadSync));

            LagrangeInterpolation.ComparePaths(ClampAndSmooth(this.Skeleton1.HipCenterPath, start, end, smoothingValue, smoothBeforeClamp), ClampAndSmooth(this.Skeleton2.HipCenterPath, start, end, smoothingValue, smoothBeforeClamp), out good, out bad);
            this.HipCenterGoodSync = good;
            this.HipCenterBadSync = bad;

            //System.Diagnostics.Debug.WriteLine(String.Format("HipCenter:: Good: {0}, Bad: {1}", this.HipCenterGoodSync, this.HipCenterBadSync));

            LagrangeInterpolation.ComparePaths(ClampAndSmooth(this.Skeleton1.HipRightPath, start, end, smoothingValue, smoothBeforeClamp), ClampAndSmooth(this.Skeleton2.HipRightPath, start, end, smoothingValue, smoothBeforeClamp), out good, out bad);
            this.HipRightGoodSync = good;
            this.HipRightBadSync = bad;

            //System.Diagnostics.Debug.WriteLine(String.Format("HipRight:: Good: {0}, Bad: {1}", this.HipRightGoodSync, this.HipRightBadSync));

            LagrangeInterpolation.ComparePaths(ClampAndSmooth(this.Skeleton1.HipLeftPath, start, end, smoothingValue, smoothBeforeClamp), ClampAndSmooth(this.Skeleton2.HipLeftPath, start, end, smoothingValue, smoothBeforeClamp), out good, out bad);
            this.HipLeftGoodSync = good;
            this.HipLeftBadSync = bad;

            //System.Diagnostics.Debug.WriteLine(String.Format("HipLeft:: Good: {0}, Bad: {1}", this.HipLeftGoodSync, this.HipLeftBadSync));

            LagrangeInterpolation.ComparePaths(ClampAndSmooth(this.Skeleton1.KneeRightPath, start, end, smoothingValue, smoothBeforeClamp), ClampAndSmooth(this.Skeleton2.KneeRightPath, start, end, smoothingValue, smoothBeforeClamp), out good, out bad);
            this.KneeRightGoodSync = good;
            this.KneeRightBadSync = bad;

            //System.Diagnostics.Debug.WriteLine(String.Format("KneeRight:: Good: {0}, Bad: {1}", this.KneeRightGoodSync, this.KneeRightBadSync));

            LagrangeInterpolation.ComparePaths(ClampAndSmooth(this.Skeleton1.KneeLeftPath, start, end, smoothingValue, smoothBeforeClamp), ClampAndSmooth(this.Skeleton2.KneeLeftPath, start, end, smoothingValue, smoothBeforeClamp), out good, out bad);
            this.KneeLeftGoodSync = good;
            this.KneeLeftBadSync = bad;

            //System.Diagnostics.Debug.WriteLine(String.Format("KneeLeft:: Good: {0}, Bad: {1}", this.KneeLeftGoodSync, this.KneeLeftBadSync));

            LagrangeInterpolation.ComparePaths(ClampAndSmooth(this.Skeleton1.AnkleRightPath, start, end, smoothingValue, smoothBeforeClamp), ClampAndSmooth(this.Skeleton2.AnkleRightPath, start, end, smoothingValue, smoothBeforeClamp), out good, out bad);
            this.AnkleRightGoodSync = good;
            this.AnkleRightBadSync = bad;

            //System.Diagnostics.Debug.WriteLine(String.Format("AnkleRight:: Good: {0}, Bad: {1}", this.AnkleRightGoodSync, this.AnkleRightBadSync));

            LagrangeInterpolation.ComparePaths(ClampAndSmooth(this.Skeleton1.AnkleLeftPath, start, end, smoothingValue, smoothBeforeClamp), ClampAndSmooth(this.Skeleton2.AnkleLeftPath, start, end, smoothingValue, smoothBeforeClamp), out good, out bad);
            this.AnkleLeftGoodSync = good;
            this.AnkleLeftBadSync = bad;

            //System.Diagnostics.Debug.WriteLine(String.Format("AnkleLeft:: Good: {0}, Bad: {1}", this.AnkleLeftGoodSync, this.AnkleLeftBadSync));

            LagrangeInterpolation.ComparePaths(ClampAndSmooth(this.Skeleton1.FootRightPath, start, end, smoothingValue, smoothBeforeClamp), ClampAndSmooth(this.Skeleton2.FootRightPath, start, end, smoothingValue, smoothBeforeClamp), out good, out bad);
            this.FootRightGoodSync = good;
            this.FootRightBadSync = bad;

            //System.Diagnostics.Debug.WriteLine(String.Format("FootRight:: Good: {0}, Bad: {1}", this.FootRightGoodSync, this.FootRightBadSync));

            LagrangeInterpolation.ComparePaths(ClampAndSmooth(this.Skeleton1.FootLeftPath, start, end, smoothingValue, smoothBeforeClamp), ClampAndSmooth(this.Skeleton2.FootLeftPath, start, end, smoothingValue, smoothBeforeClamp), out good, out bad);
            this.FootLeftGoodSync = good;
            this.FootLeftBadSync = bad;

            //System.Diagnostics.Debug.WriteLine(String.Format("FootLeft:: Good: {0}, Bad: {1}", this.FootLeftGoodSync, this.FootLeftBadSync));
        }

        private JointPath ClampAndSmooth(JointPath originalPath, DateTime startTime, DateTime endTime, int smoothingValue, bool smoothBeforeClamp)
        {
            if( smoothBeforeClamp )
                return ClampToTimeRange(SmoothPath(originalPath, smoothingValue), startTime, endTime);  // Smooth First, then Clamp.
            
            return SmoothPath(ClampToTimeRange(originalPath, startTime, endTime), smoothingValue);    // Clamp First, then Smooth.
        }

        public JointPath ClampToTimeRange( JointPath originalPath, DateTime startTime, DateTime endTime )
        {
            if (originalPath == null)
                return null;

            JointPath clamped = new JointPath( originalPath.PathColor, originalPath.JointType );
            
            foreach( AIMS.JointPath.PathPoint p in originalPath.PathPoints.Where( o => ( o.Timestamp >= startTime && o.Timestamp <= endTime ) ).ToList() )
            {
                clamped.PathPoints.Add(p);
            }

            return clamped;
        }

        public JointPath SmoothPath(JointPath originalPath, int smoothingValue)
        {
            if (originalPath == null)
                return null;

            JointPath smoothed = new JointPath(originalPath.PathColor, originalPath.JointType);

            if (smoothingValue > 1)
            {
                List<JointPath.PathPoint> smoothedPathPoints = new List<JointPath.PathPoint>();

                for (int i = 0; i < originalPath.PathPoints.Count; i++)
                {
                    int cnt = 0;
                    double totalX = 0;
                    double totalY = 0;
                    double totalZ = 0;

                    for (int j = smoothingValue; j >= 0; j--)
                    {
                        if (i - j >= 0) // Average the previous (#)x of points based on the smoothing value.
                        {
                            cnt++;

                            totalX += originalPath.PathPoints[i - j].Location.X;
                            totalY += originalPath.PathPoints[i - j].Location.Y;
                            totalZ += originalPath.PathPoints[i - j].Location.Z;
                        }
                    }

                    if (cnt > 0)
                    {
                        Point3D avgP3D = new Point3D(totalX / cnt, totalY / cnt, totalZ / cnt);
                        smoothedPathPoints.Add(new JointPath.PathPoint(avgP3D, originalPath.PathPoints[i].Timestamp));
                    }
                }

                smoothed.PathPoints.AddRange( smoothedPathPoints );
            }
            else
            {
                smoothed.PathPoints = originalPath.PathPoints;
            }

            return smoothed;
        }

        public void Restart()
        {

        }

        public void WipeClean()
        {
            this.Skeleton1 = null;
            this.Skeleton2 = null;
            this.Recording = false;
            this.RecordingEndTime = DateTime.MaxValue;
            this.RecordingStartTime = DateTime.MinValue;
        }

        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(sender, e);
            }
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            this.NotifyPropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }

    public class ComparableSkeleton : INotifyPropertyChanged
    {
        #region Show/Hide Skeleton
        private bool showSkeleton = true;
        /// <summary>
        /// Whether or not to show the Skeleton in the comparison.
        /// </summary>
        public bool ShowSkeleton
        {
            get { return showSkeleton; }
            set
            {
                if (showSkeleton == value)
                    showSkeleton = value;
                else
                {
                    showSkeleton = value;
                    NotifyPropertyChanged("ShowSkeleton");
                }
            }
        }
        #endregion

        #region Show/Hide JointPaths
        private bool showHead = true;
        public bool ShowHead
        {
            get { return showHead; }
            set
            {
                if (showHead == value)
                    showHead = value;
                else
                {
                    showHead = value;
                    NotifyPropertyChanged("ShowHead");
                }
            }
        }
        private bool showNeck = true;
        public bool ShowNeck
        {
            get { return showNeck; }
            set
            {
                if (showNeck == value)
                    showNeck = value;
                else
                {
                    showNeck = value;
                    NotifyPropertyChanged("ShowNeck");
                }
            }
        }
        private bool showShoulderCenter = true;
        public bool ShowShoulderCenter
        {
            get { return showShoulderCenter; }
            set
            {
                if (showShoulderCenter == value)
                    showShoulderCenter = value;
                else
                {
                    showShoulderCenter = value;
                    NotifyPropertyChanged("ShowShoulderCenter");
                }
            }
        }
        private bool showShoulderRight = true;
        public bool ShowShoulderRight
        {
            get { return showShoulderRight; }
            set
            {
                if (showShoulderRight == value)
                    showShoulderRight = value;
                else
                {
                    showShoulderRight = value;
                    NotifyPropertyChanged("ShowShoulderRight");
                }
            }
        }
        private bool showShoulderLeft = true;
        public bool ShowShoulderLeft
        {
            get { return showShoulderLeft; }
            set
            {
                if (showShoulderLeft == value)
                    showShoulderLeft = value;
                else
                {
                    showShoulderLeft = value;
                    NotifyPropertyChanged("ShowShoulderLeft");
                }
            }
        }
        private bool showElbowRight = true;
        public bool ShowElbowRight
        {
            get { return showElbowRight; }
            set
            {
                if (showElbowRight == value)
                    showElbowRight = value;
                else
                {
                    showElbowRight = value;
                    NotifyPropertyChanged("ShowElbowRight");
                }
            }
        }
        private bool showElbowLeft = true;
        public bool ShowElbowLeft
        {
            get { return showElbowLeft; }
            set
            {
                if (showElbowLeft == value)
                    showElbowLeft = value;
                else
                {
                    showElbowLeft = value;
                    NotifyPropertyChanged("ShowElbowLeft");
                }
            }
        }
        private bool showWristRight = true;
        public bool ShowWristRight
        {
            get { return showWristRight; }
            set
            {
                if (showWristRight == value)
                    showWristRight = value;
                else
                {
                    showWristRight = value;
                    NotifyPropertyChanged("ShowWristRight");
                }
            }
        }
        private bool showWristLeft = true;
        public bool ShowWristLeft
        {
            get { return showWristLeft; }
            set
            {
                if (showWristLeft == value)
                    showWristLeft = value;
                else
                {
                    showWristLeft = value;
                    NotifyPropertyChanged("ShowWristLeft");
                }
            }
        }
        private bool showHandRight = true;
        public bool ShowHandRight
        {
            get { return showHandRight; }
            set
            {
                if (showHandRight == value)
                    showHandRight = value;
                else
                {
                    showHandRight = value;
                    NotifyPropertyChanged("ShowHandRight");
                }
            }
        }
        private bool showHandLeft = true;
        public bool ShowHandLeft
        {
            get { return showHandLeft; }
            set
            {
                if (showHandLeft == value)
                    showHandLeft = value;
                else
                {
                    showHandLeft = value;
                    NotifyPropertyChanged("ShowHandLeft");
                }
            }
        }
        private bool showSpine = true;
        public bool ShowSpine
        {
            get { return showSpine; }
            set
            {
                if (showSpine == value)
                    showSpine = value;
                else
                {
                    showSpine = value;
                    NotifyPropertyChanged("ShowSpine");
                }
            }
        }
        private bool showHipCenter = true;
        public bool ShowHipCenter
        {
            get { return showHipCenter; }
            set
            {
                if (showHipCenter == value)
                    showHipCenter = value;
                else
                {
                    showHipCenter = value;
                    NotifyPropertyChanged("ShowHipCenter");
                }
            }
        }
        private bool showHipRight = true;
        public bool ShowHipRight
        {
            get { return showHipRight; }
            set
            {
                if (showHipRight == value)
                    showHipRight = value;
                else
                {
                    showHipRight = value;
                    NotifyPropertyChanged("ShowHipRight");
                }
            }
        }
        private bool showHipLeft = true;
        public bool ShowHipLeft
        {
            get { return showHipLeft; }
            set
            {
                if (showHipLeft == value)
                    showHipLeft = value;
                else
                {
                    showHipLeft = value;
                    NotifyPropertyChanged("ShowHipLeft");
                }
            }
        }
        private bool showKneeRight = true;
        public bool ShowKneeRight
        {
            get { return showKneeRight; }
            set
            {
                if (showKneeRight == value)
                    showKneeRight = value;
                else
                {
                    showKneeRight = value;
                    NotifyPropertyChanged("ShowKneeRight");
                }
            }
        }
        private bool showKneeLeft = true;
        public bool ShowKneeLeft
        {
            get { return showKneeLeft; }
            set
            {
                if (showKneeLeft == value)
                    showKneeLeft = value;
                else
                {
                    showKneeLeft = value;
                    NotifyPropertyChanged("ShowKneeLeft");
                }
            }
        }
        private bool showAnkleRight = true;
        public bool ShowAnkleRight
        {
            get { return showAnkleRight; }
            set
            {
                if (showAnkleRight == value)
                    showAnkleRight = value;
                else
                {
                    showAnkleRight = value;
                    NotifyPropertyChanged("ShowAnkleRight");
                }
            }
        }
        private bool showAnkleLeft = true;
        public bool ShowAnkleLeft
        {
            get { return showAnkleLeft; }
            set
            {
                if (showAnkleLeft == value)
                    showAnkleLeft = value;
                else
                {
                    showAnkleLeft = value;
                    NotifyPropertyChanged("ShowAnkleLeft");
                }
            }
        }
        private bool showFootRight = true;
        public bool ShowFootRight
        {
            get { return showFootRight; }
            set
            {
                if (showFootRight == value)
                    showFootRight = value;
                else
                {
                    showFootRight = value;
                    NotifyPropertyChanged("ShowFootRight");
                }
            }
        }
        private bool showFootLeft = true;
        public bool ShowFootLeft
        {
            get { return showFootLeft; }
            set
            {
                if (showFootLeft == value)
                    showFootLeft = value;
                else
                {
                    showFootLeft = value;
                    NotifyPropertyChanged("ShowFootLeft");
                }
            }
        }
        #endregion

        #region Track/Untrack JointPaths
        private bool trackHead = true;
        public bool TrackHead
        {
            get { return trackHead; }
            set
            {
                if (trackHead == value)
                    trackHead = value;
                else
                {
                    trackHead = value;
                    NotifyPropertyChanged("TrackHead");
                }
            }
        }
        private bool trackNeck = true;
        public bool TrackNeck
        {
            get { return trackNeck; }
            set
            {
                if (trackNeck == value)
                    trackNeck = value;
                else
                {
                    trackNeck = value;
                    NotifyPropertyChanged("TrackNeck");
                }
            }
        }
        private bool trackShoulderCenter = true;
        public bool TrackShoulderCenter
        {
            get { return trackShoulderCenter; }
            set
            {
                if (trackShoulderCenter == value)
                    trackShoulderCenter = value;
                else
                {
                    trackShoulderCenter = value;
                    NotifyPropertyChanged("TrackShoulderCenter");
                }
            }
        }
        private bool trackShoulderRight = true;
        public bool TrackShoulderRight
        {
            get { return trackShoulderRight; }
            set
            {
                if (trackShoulderRight == value)
                    trackShoulderRight = value;
                else
                {
                    trackShoulderRight = value;
                    NotifyPropertyChanged("TrackShoulderRight");
                }
            }
        }
        private bool trackShoulderLeft = true;
        public bool TrackShoulderLeft
        {
            get { return trackShoulderLeft; }
            set
            {
                if (trackShoulderLeft == value)
                    trackShoulderLeft = value;
                else
                {
                    trackShoulderLeft = value;
                    NotifyPropertyChanged("TrackShoulderLeft");
                }
            }
        }
        private bool trackElbowRight = true;
        public bool TrackElbowRight
        {
            get { return trackElbowRight; }
            set
            {
                if (trackElbowRight == value)
                    trackElbowRight = value;
                else
                {
                    trackElbowRight = value;
                    NotifyPropertyChanged("TrackElbowRight");
                }
            }
        }
        private bool trackElbowLeft = true;
        public bool TrackElbowLeft
        {
            get { return trackElbowLeft; }
            set
            {
                if (trackElbowLeft == value)
                    trackElbowLeft = value;
                else
                {
                    trackElbowLeft = value;
                    NotifyPropertyChanged("TrackElbowLeft");
                }
            }
        }
        private bool trackWristRight = true;
        public bool TrackWristRight
        {
            get { return trackWristRight; }
            set
            {
                if (trackWristRight == value)
                    trackWristRight = value;
                else
                {
                    trackWristRight = value;
                    NotifyPropertyChanged("TrackWristRight");
                }
            }
        }
        private bool trackWristLeft = true;
        public bool TrackWristLeft
        {
            get { return trackWristLeft; }
            set
            {
                if (trackWristLeft == value)
                    trackWristLeft = value;
                else
                {
                    trackWristLeft = value;
                    NotifyPropertyChanged("TrackWristLeft");
                }
            }
        }
        private bool trackHandRight = true;
        public bool TrackHandRight
        {
            get { return trackHandRight; }
            set
            {
                if (trackHandRight == value)
                    trackHandRight = value;
                else
                {
                    trackHandRight = value;
                    NotifyPropertyChanged("TrackHandRight");
                }
            }
        }
        private bool trackHandLeft = true;
        public bool TrackHandLeft
        {
            get { return trackHandLeft; }
            set
            {
                if (trackHandLeft == value)
                    trackHandLeft = value;
                else
                {
                    trackHandLeft = value;
                    NotifyPropertyChanged("TrackHandLeft");
                }
            }
        }
        private bool trackSpine = true;
        public bool TrackSpine
        {
            get { return trackSpine; }
            set
            {
                if (trackSpine == value)
                    trackSpine = value;
                else
                {
                    trackSpine = value;
                    NotifyPropertyChanged("TrackSpine");
                }
            }
        }
        private bool trackHipCenter = true;
        public bool TrackHipCenter
        {
            get { return trackHipCenter; }
            set
            {
                if (trackHipCenter == value)
                    trackHipCenter = value;
                else
                {
                    trackHipCenter = value;
                    NotifyPropertyChanged("TrackHipCenter");
                }
            }
        }
        private bool trackHipRight = true;
        public bool TrackHipRight
        {
            get { return trackHipRight; }
            set
            {
                if (trackHipRight == value)
                    trackHipRight = value;
                else
                {
                    trackHipRight = value;
                    NotifyPropertyChanged("TrackHipRight");
                }
            }
        }
        private bool trackHipLeft = true;
        public bool TrackHipLeft
        {
            get { return trackHipLeft; }
            set
            {
                if (trackHipLeft == value)
                    trackHipLeft = value;
                else
                {
                    trackHipLeft = value;
                    NotifyPropertyChanged("TrackHipLeft");
                }
            }
        }
        private bool trackKneeRight = true;
        public bool TrackKneeRight
        {
            get { return trackKneeRight; }
            set
            {
                if (trackKneeRight == value)
                    trackKneeRight = value;
                else
                {
                    trackKneeRight = value;
                    NotifyPropertyChanged("TrackKneeRight");
                }
            }
        }
        private bool trackKneeLeft = true;
        public bool TrackKneeLeft
        {
            get { return trackKneeLeft; }
            set
            {
                if (trackKneeLeft == value)
                    trackKneeLeft = value;
                else
                {
                    trackKneeLeft = value;
                    NotifyPropertyChanged("TrackKneeLeft");
                }
            }
        }
        private bool trackAnkleRight = true;
        public bool TrackAnkleRight
        {
            get { return trackAnkleRight; }
            set
            {
                if (trackAnkleRight == value)
                    trackAnkleRight = value;
                else
                {
                    trackAnkleRight = value;
                    NotifyPropertyChanged("TrackAnkleRight");
                }
            }
        }
        private bool trackAnkleLeft = true;
        public bool TrackAnkleLeft
        {
            get { return trackAnkleLeft; }
            set
            {
                if (trackAnkleLeft == value)
                    trackAnkleLeft = value;
                else
                {
                    trackAnkleLeft = value;
                    NotifyPropertyChanged("TrackAnkleLeft");
                }
            }
        }
        private bool trackFootRight = true;
        public bool TrackFootRight
        {
            get { return trackFootRight; }
            set
            {
                if (trackFootRight == value)
                    trackFootRight = value;
                else
                {
                    trackFootRight = value;
                    NotifyPropertyChanged("TrackFootRight");
                }
            }
        }
        private bool trackFootLeft = true;
        public bool TrackFootLeft
        {
            get { return trackFootLeft; }
            set
            {
                if (trackFootLeft == value)
                    trackFootLeft = value;
                else
                {
                    trackFootLeft = value;
                    NotifyPropertyChanged("TrackFootLeft");
                }
            }
        }
        #endregion

        #region JointPaths
        private JointPath headPath;
        public JointPath HeadPath
        {
            get { return headPath; }
            set
            {
                if (headPath == value)
                    headPath = value;
                else
                {
                    headPath = value;
                    NotifyPropertyChanged("HeadPath");
                }
            }
        }
        private JointPath neckPath;
        public JointPath NeckPath
        {
            get { return neckPath; }
            set
            {
                if (neckPath == value)
                    neckPath = value;
                else
                {
                    neckPath = value;
                    NotifyPropertyChanged("NeckPath");
                }
            }
        }
        private JointPath shoulderCenterPath;
        public JointPath ShoulderCenterPath
        {
            get { return shoulderCenterPath; }
            set
            {
                if (shoulderCenterPath == value)
                    shoulderCenterPath = value;
                else
                {
                    shoulderCenterPath = value;
                    NotifyPropertyChanged("ShoulderCenterPath");
                }
            }
        }
        private JointPath shoulderRightPath;
        public JointPath ShoulderRightPath
        {
            get { return shoulderRightPath; }
            set
            {
                if (shoulderRightPath == value)
                    shoulderRightPath = value;
                else
                {
                    shoulderRightPath = value;
                    NotifyPropertyChanged("ShoulderRightPath");
                }
            }
        }
        private JointPath shoulderLeftPath;
        public JointPath ShoulderLeftPath
        {
            get { return shoulderLeftPath; }
            set
            {
                if (shoulderLeftPath == value)
                    shoulderLeftPath = value;
                else
                {
                    shoulderLeftPath = value;
                    NotifyPropertyChanged("ShoulderLeftPath");
                }
            }
        }
        private JointPath elbowRightPath;
        public JointPath ElbowRightPath
        {
            get { return elbowRightPath; }
            set
            {
                if (elbowRightPath == value)
                    elbowRightPath = value;
                else
                {
                    elbowRightPath = value;
                    NotifyPropertyChanged("ElbowRightPath");
                }
            }
        }
        private JointPath elbowLeftPath;
        public JointPath ElbowLeftPath
        {
            get { return elbowLeftPath; }
            set
            {
                if (elbowLeftPath == value)
                    elbowLeftPath = value;
                else
                {
                    elbowLeftPath = value;
                    NotifyPropertyChanged("ElbowLeftPath");
                }
            }
        }
        private JointPath wristRightPath;
        public JointPath WristRightPath
        {
            get { return wristRightPath; }
            set
            {
                if (wristRightPath == value)
                    wristRightPath = value;
                else
                {
                    wristRightPath = value;
                    NotifyPropertyChanged("WristRightPath");
                }
            }
        }
        private JointPath wristLeftPath;
        public JointPath WristLeftPath
        {
            get { return wristLeftPath; }
            set
            {
                if (wristLeftPath == value)
                    wristLeftPath = value;
                else
                {
                    wristLeftPath = value;
                    NotifyPropertyChanged("WristLeftPath");
                }
            }
        }
        private JointPath handRightPath;
        public JointPath HandRightPath
        {
            get { return handRightPath; }
            set
            {
                if (handRightPath == value)
                    handRightPath = value;
                else
                {
                    handRightPath = value;
                    NotifyPropertyChanged("HandRightPath");
                }
            }
        }
        private JointPath handLeftPath;
        public JointPath HandLeftPath
        {
            get { return handLeftPath; }
            set
            {
                if (handLeftPath == value)
                    handLeftPath = value;
                else
                {
                    handLeftPath = value;
                    NotifyPropertyChanged("HandLeftPath");
                }
            }
        }
        private JointPath spinePath;
        public JointPath SpinePath
        {
            get { return spinePath; }
            set
            {
                if (spinePath == value)
                    spinePath = value;
                else
                {
                    spinePath = value;
                    NotifyPropertyChanged("SpinePath");
                }
            }
        }
        private JointPath hipCenterPath;
        public JointPath HipCenterPath
        {
            get { return hipCenterPath; }
            set
            {
                if (hipCenterPath == value)
                    hipCenterPath = value;
                else
                {
                    hipCenterPath = value;
                    NotifyPropertyChanged("HipCenterPath");
                }
            }
        }
        private JointPath hipRightPath;
        public JointPath HipRightPath
        {
            get { return hipRightPath; }
            set
            {
                if (hipRightPath == value)
                    hipRightPath = value;
                else
                {
                    hipRightPath = value;
                    NotifyPropertyChanged("HipRightPath");
                }
            }
        }
        private JointPath hipLeftPath;
        public JointPath HipLeftPath
        {
            get { return hipLeftPath; }
            set
            {
                if (hipLeftPath == value)
                    hipLeftPath = value;
                else
                {
                    hipLeftPath = value;
                    NotifyPropertyChanged("HipLeftPath");
                }
            }
        }
        private JointPath kneeRightPath;
        public JointPath KneeRightPath
        {
            get { return kneeRightPath; }
            set
            {
                if (kneeRightPath == value)
                    kneeRightPath = value;
                else
                {
                    kneeRightPath = value;
                    NotifyPropertyChanged("KneeRightPath");
                }
            }
        }
        private JointPath kneeLeftPath;
        public JointPath KneeLeftPath
        {
            get { return kneeLeftPath; }
            set
            {
                if (kneeLeftPath == value)
                    kneeLeftPath = value;
                else
                {
                    kneeLeftPath = value;
                    NotifyPropertyChanged("KneeLeftPath");
                }
            }
        }
        private JointPath ankleRightPath;
        public JointPath AnkleRightPath
        {
            get { return ankleRightPath; }
            set
            {
                if (ankleRightPath == value)
                    ankleRightPath = value;
                else
                {
                    ankleRightPath = value;
                    NotifyPropertyChanged("AnkleRightPath");
                }
            }
        }
        private JointPath ankleLeftPath;
        public JointPath AnkleLeftPath
        {
            get { return ankleLeftPath; }
            set
            {
                if (ankleLeftPath == value)
                    ankleLeftPath = value;
                else
                {
                    ankleLeftPath = value;
                    NotifyPropertyChanged("AnkleLeftPath");
                }
            }
        }
        private JointPath footRightPath;
        public JointPath FootRightPath
        {
            get { return footRightPath; }
            set
            {
                if (footRightPath == value)
                    footRightPath = value;
                else
                {
                    footRightPath = value;
                    NotifyPropertyChanged("FootRightPath");
                }
            }
        }
        private JointPath footLeftPath;
        public JointPath FootLeftPath
        {
            get { return footLeftPath; }
            set
            {
                if (footLeftPath == value)
                    footLeftPath = value;
                else
                {
                    footLeftPath = value;
                    NotifyPropertyChanged("FootLeftPath");
                }
            }
        }
        #endregion

        private ObservableRangeCollection<JointPath> jointPaths = new ObservableRangeCollection<JointPath>();
        public ObservableRangeCollection<JointPath> JointPaths
        {
            get { return jointPaths; }
            set { jointPaths = value; }
        }

        private int? skeletonID = null;
        public int? SkeletonID
        {
            get { return skeletonID; }
            set
            {
                if( skeletonID == value )
                    skeletonID = value;
                else
                {
                    skeletonID = value;
                    NotifyPropertyChanged("SkeletonID");
                }
            }
        }

        private Color color;
        public Color Color
        {
            get { return color; }
            set
            {
                if (color == value)
                    color = value;
                else
                {
                    color = value;
                    NotifyPropertyChanged("Color");
                }
            }
        }

        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(sender, e);
            }
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            this.NotifyPropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        /// <summary>
        /// ComparableSkeleton is one of two which are compared against eachother in the 3D Path Comparison.
        /// 
        /// Each skeleton has multiple JointPaths which is made up of many PathPoints over time.
        /// 
        /// The Skeleton's JointPaths can be compared to eachother if both exist.
        /// 
        /// 
        /// * Pass in a SkeletonProfile upon instantiating, and the ComparableSkeleton will store the profileID.
        /// </summary>
        /// <param name="profile"></param>
        public ComparableSkeleton( SkeletonProfile profile )
        {
            if (profile != null && profile.CurrentTrackingID != null)
            {
                SkeletonID = profile.CurrentTrackingID;
            }
        }

        /// <summary>
        /// ComparableSkeleton is one of two which are compared against eachother in the 3D Path Comparison.
        /// 
        /// Each skeleton has multiple JointPaths which is made up of many PathPoints over time.
        /// 
        /// The Skeleton's JointPaths can be compared to eachother if both exist.
        /// 
        /// 
        /// * Pass in an int representing the SkeletonID of a SkeletonProfile when instantiating, and the COmparableSkeleton will store it as the tracked profile id.
        /// </summary>
        /// <param name="SkeletonID"></param>
        public ComparableSkeleton(int skeletonID)
        {
            SkeletonID = skeletonID;
        }

        /// <summary>
        /// Track all joints who's Track{x} = true.
        /// ( ie. Track the head by setting TrackHead = true prior to calling TrackPaths )
        /// </summary>
        public void TrackPaths()
        {
            // Find the Skeleton.
            SkeletonProfile profile = KinectManager.GetSkeletonProfile(this.SkeletonID, true);

            if (profile == null)
            {
                // No Skeleton Found.
                return;
            }

            #region Update/Track Each Joint.
            if (this.TrackHead)
            {
                if (HeadPath == null)
                {
                    HeadPath = new JointPath(this.Color, JointType.Head);
                }

                HeadPath.TrackJoint( profile );
            }
            if (this.TrackShoulderCenter)
            {
                if (ShoulderCenterPath == null)
                {
                    ShoulderCenterPath = new JointPath(this.Color, JointType.ShoulderCenter);
                }

                ShoulderCenterPath.TrackJoint(profile);
            }
            if (this.TrackShoulderRight)
            {
                if (ShoulderRightPath == null)
                {
                    ShoulderRightPath = new JointPath(this.Color, JointType.ShoulderRight);
                }

                ShoulderRightPath.TrackJoint(profile);
            }
            if (this.TrackShoulderLeft)
            {
                if (ShoulderLeftPath == null)
                {
                    ShoulderLeftPath = new JointPath(this.Color, JointType.ShoulderLeft);
                }

                ShoulderLeftPath.TrackJoint(profile);
            }
            if (this.TrackElbowRight)
            {
                if (ElbowRightPath == null)
                {
                    ElbowRightPath = new JointPath(this.Color, JointType.ElbowRight);
                }

                ElbowRightPath.TrackJoint(profile);
            }
            if (this.TrackElbowLeft)
            {
                if (ElbowLeftPath == null)
                {
                    ElbowLeftPath = new JointPath(this.Color, JointType.ElbowLeft);
                }

                ElbowLeftPath.TrackJoint(profile);
            }
            if (this.TrackWristRight)
            {
                if (WristRightPath == null)
                {
                    WristRightPath = new JointPath(this.Color, JointType.WristRight);
                }

                WristRightPath.TrackJoint(profile);
            }
            if (this.TrackWristLeft)
            {
                if (WristLeftPath == null)
                {
                    WristLeftPath = new JointPath(this.Color, JointType.WristLeft);
                }

                WristLeftPath.TrackJoint(profile);
            }
            if (this.TrackHandRight)
            {
                if (HandRightPath == null)
                {
                    HandRightPath = new JointPath(this.Color, JointType.HandRight);
                }

                HandRightPath.TrackJoint(profile);
            }
            if (this.TrackHandLeft)
            {
                if (HandLeftPath == null)
                {
                    HandLeftPath = new JointPath(this.Color, JointType.HandLeft);
                }

                HandLeftPath.TrackJoint(profile);
            }
            if (this.TrackSpine)
            {
                if (SpinePath == null)
                {
                    SpinePath = new JointPath(this.Color, JointType.SpineMid);
                }

                SpinePath.TrackJoint(profile);
            }
            if (this.TrackHipCenter)
            {
                if (HipCenterPath == null)
                {
                    HipCenterPath = new JointPath(this.Color, JointType.SpineBase);
                }

                HipCenterPath.TrackJoint(profile);
            }
            if (this.TrackHipRight)
            {
                if (HipRightPath == null)
                {
                    HipRightPath = new JointPath(this.Color, JointType.HipRight);
                }

                HipRightPath.TrackJoint(profile);
            }
            if (this.TrackHipLeft)
            {
                if (HipLeftPath == null)
                {
                    HipLeftPath = new JointPath(this.Color, JointType.HipLeft);
                }

                HipLeftPath.TrackJoint(profile);
            }
            if (this.TrackKneeRight)
            {
                if (KneeRightPath == null)
                {
                    KneeRightPath = new JointPath(this.Color, JointType.KneeRight);
                }

                KneeRightPath.TrackJoint(profile);
            }
            if (this.TrackKneeLeft)
            {
                if (KneeLeftPath == null)
                {
                    KneeLeftPath = new JointPath(this.Color, JointType.KneeLeft);
                }

                KneeLeftPath.TrackJoint(profile);
            }
            if (this.TrackAnkleRight)
            {
                if (AnkleRightPath == null)
                {
                    AnkleRightPath = new JointPath(this.Color, JointType.AnkleRight);
                }

                AnkleRightPath.TrackJoint(profile);
            }
            if (this.TrackAnkleLeft)
            {
                if (AnkleLeftPath == null)
                {
                    AnkleLeftPath = new JointPath(this.Color, JointType.AnkleLeft);
                }

                AnkleLeftPath.TrackJoint(profile);
            }
            if (this.TrackFootRight)
            {
                if (FootRightPath == null)
                {
                    FootRightPath = new JointPath(this.Color, JointType.FootRight);
                }

                FootRightPath.TrackJoint(profile);
            }
            if (this.TrackFootLeft)
            {
                if (FootLeftPath == null)
                {
                    FootLeftPath = new JointPath(this.Color, JointType.FootLeft);
                }

                FootLeftPath.TrackJoint(profile);
            }
            #endregion
        }
    }

    public class JointPath : INotifyPropertyChanged
    {
        private ObservableRangeCollection<PathPoint> pathPoints = new ObservableRangeCollection<PathPoint>();
        public ObservableRangeCollection<PathPoint> PathPoints { get { return pathPoints; } set { pathPoints = value; } }

        private JointType jointType;
        public JointType JointType { get { return jointType; } set { jointType = value; } }

        private Color pathColor;
        public Color PathColor 
        { 
            get { return pathColor; } 
            set 
            {
                if (pathColor == value)
                    pathColor = value;
                else
                {
                    pathColor = value;
                    NotifyPropertyChanged("PathColor");
                }
            } 
        }

        public JointPath( Color color, JointType jointtype )
        {
            this.JointType = jointtype;
            this.PathColor = color;
        }

        public void TrackJoint( SkeletonProfile profile )
        {
            if (profile == null)
                return; // No Skeleton Profile.

            JointProfile jp = profile.JointProfiles.FirstOrDefault(o => o.JointType == this.JointType);

            if (jp == null)
                return; // No Joint On Skeleton Profile.

            this.PathPoints.Add(new PathPoint(new Point3D(jp.PreviousPosition.X, jp.PreviousPosition.Y, jp.PreviousPosition.Z), DateTime.Now));
            this.PathPoints.Add(new PathPoint(new Point3D( jp.CurrentPosition.X, jp.CurrentPosition.Y, jp.CurrentPosition.Z ), DateTime.Now));
        }

        /// <summary>
        /// Represents a single data point of a path with a Point3D location, and a DateTime timestamp for sorting/filtering.
        /// </summary>
        public class PathPoint
        {
            private Point3D location = new Point3D();
            public Point3D Location { get { return location; } set { location = value; } }

            DateTime timestamp = new DateTime();
            public DateTime Timestamp { get { return timestamp; } set { timestamp = value; } }

            public PathPoint(Point3D loc, DateTime timestamp)
            {
                this.Location = loc;
                this.Timestamp = timestamp;
            }
        }

        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(sender, e);
            }
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            this.NotifyPropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }

    public class ComparablePath : INotifyPropertyChanged
    {
        public enum PathTypes
        {
            Skeletal,
            Color
        }

        private bool clampDepth = false;
        private int minDepth_MM = 0;
        private int maxDepth_MM = 3000;

        private Int32Rect colorRegion = new Int32Rect(0, 0, 640, 480);
        public Int32Rect ColorRegion
        {
            get { return colorRegion; }
            set
            {
                if (colorRegion == value)
                    colorRegion = value;
                else
                {
                    colorRegion = value;
                    NotifyPropertyChanged("ColorRegion");
                }
            }
        }

        private AIMS.Assets.Lessons2.ColorTracker.ColorRange colorRange = AIMS.Assets.Lessons2.ColorTracker.GetColorRange("yellow");

        public AIMS.Assets.Lessons2.ColorTracker.ColorRange ColorRange
        {
            get { return colorRange; }
            set
            {
                if (colorRange == value)
                    colorRange = value;
                else
                {
                    colorRange = value;
                    NotifyPropertyChanged("ColorRange");
                }
            }
        }

        private ObservableRangeCollection<PathPoint> pathPoints = new ObservableRangeCollection<PathPoint>();
        public ObservableRangeCollection<PathPoint> PathPoints { get { return pathPoints; } set { pathPoints = value; } }

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                if (name == value)
                    name = value;
                else
                {
                    name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        private PathTypes pathType;
        public PathTypes PathType
        {
            get { return pathType; }
            set
            {
                if (pathType == value)
                    pathType = value;
                else
                {
                    pathType = value;
                    NotifyPropertyChanged("PathType");
                }
            }
        }

        private Color color;
        public Color Color
        {
            get { return color; }
            set
            {
                if (color == value)
                    color = value;
                else
                {
                    color = value;
                    NotifyPropertyChanged("Color");
                }
            }
        }

        public ComparablePath(Color color, JointType jointtype, PathTypes pathtype)
        {
            this.JointType = jointtype;
            this.Color = color;
        }

        public void TrackJoint(SkeletonProfile profile)
        {
            if (profile == null)
                return; // No Skeleton Profile.

            JointProfile jp = profile.JointProfiles.FirstOrDefault(o => o.JointType == this.JointType);

            if (jp == null)
                return; // No Joint On Skeleton Profile.

            this.PathPoints.Add(new PathPoint(new Point3D(jp.PreviousPosition.X, jp.PreviousPosition.Y, jp.PreviousPosition.Z), DateTime.Now));
            this.PathPoints.Add(new PathPoint(new Point3D(jp.CurrentPosition.X, jp.CurrentPosition.Y, jp.CurrentPosition.Z), DateTime.Now));
        }

        /// <summary>
        /// Represents a single data point of a path with a Point3D location, and a DateTime timestamp for sorting/filtering.
        /// </summary>
        public class PathPoint
        {
            private Point3D location = new Point3D();
            public Point3D Location { get { return location; } set { location = value; } }

            DateTime timestamp = new DateTime();
            public DateTime Timestamp { get { return timestamp; } set { timestamp = value; } }

            public PathPoint(Point3D loc, DateTime timestamp)
            {
                this.Location = loc;
                this.Timestamp = timestamp;
            }
        }

        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(sender, e);
            }
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            this.NotifyPropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }

    public class LagrangeInterpolation
    {
        private struct Operators
        {
            public List<double> Operatos;
        }

        /// <summary>
        /// Langrage Interpolation Function
        /// Precondition: smallestXPosition less than largestXPosition
        /// </summary>
        /// <param name="pointList"> Is the original list with X and Y points </param>
        /// <param name="XPoints"> has the interpolated X points </param>
        /// <param name="YPoints"> has the interpolated Y points  </param>
        /// <param name="YPoints"> has the interpolated Z points  </param>
        /// <param name="lagrangeCoordinates"> has the interpolated XYZ points </param>
        /// <param name="smallestXPosition"></param>
        /// <param name="largestXPosition"></param>
        /// <returns> 0 - error, else 1 </returns>
        public static int fitLagrange(List<Point3D> pointList, ref List<double> XPoints, ref List<double> YPoints, ref List<double> ZPoints, out Point3D[] lagrangeCoordinates, int smallestXPosition = 0, int largestXPosition = 1000)
        {
            lagrangeCoordinates = null;
            if (pointList.Count < 1)
            {
                return 0;
            }

            if (largestXPosition > smallestXPosition)
            {
                lagrangeCoordinates = new Point3D[largestXPosition - smallestXPosition];
                XPoints = new List<double>();
                YPoints = new List<double>();
                ZPoints = new List<double>();
            }
            else
            {
                //error
                return 0;
            }

            List<Operators> OpList;
            OpList = new List<Operators>();

            try
            {
                if (pointList.Count > 0)
                {
                    //compute lagrange operator for each X coordinate
                    for (int x = smallestXPosition; x <= largestXPosition; x++)
                    {
                        //list of double to hold the Lagrange operators
                        List<double> L = new List<double>();
                        //Init the list with 1's
                        for (int i = 0; i < pointList.Count; i++)
                        {
                            L.Add(1);
                        }
                        for (int i = 0; i < L.Count; i++)
                        {
                            for (int k = 0; k < pointList.Count; k++)
                            {
                                if (i != k)
                                    L[i] *= (double) ((x - pointList[k].X) / (pointList[i].X - pointList[k].X));
                            }
                        }
                        Operators o = new Operators();
                        o.Operatos = L;
                        OpList.Add(o);
                        XPoints.Add(x);

                    }

                    //Computing the Polynomial P(x) which is y in our curve
                    foreach (Operators O in OpList)
                    {
                        double y = 0;
                        for (int i = 0; i < pointList.Count; i++)
                        {
                            y += (double) (O.Operatos[i] * pointList[i].Y);
                        }

                        YPoints.Add(y);
                    }

                    //****************************************************
                    foreach (Operators O in OpList)
                    {
                        double z = 0;
                        for (int i = 0; i < pointList.Count; i++)
                        {
                            z += (double) (O.Operatos[i] * pointList[i].Z);//****
                        }

                        ZPoints.Add(z);
                    }

                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }

            //populate lagrange coordinate
            for (int i = 0; i < lagrangeCoordinates.Length; i++)
            {
                if (Double.IsInfinity(YPoints[i]) || Double.IsNaN(YPoints[i]) || Double.IsInfinity(ZPoints[i]) || Double.IsNaN(ZPoints[i]))
                {
                    
                }
                else
                {
                    lagrangeCoordinates[i] = new Point3D(XPoints[i], YPoints[i], ZPoints[i]);
                }
                
            }

            return 1;
        }

        public static double get3dSlope(Point3D point3d0, Point3D point3d1)
        {
            //double deltaZ = (double) System.Math.Abs(point3d1.Z - point3d0.Z);
           
            //Point p0 = new Point((double)point3d0.X, (double)point3d0.Y);
            //Point p1 = new Point((double)point3d1.X, (double)point3d1.Y);

            //double distanceXY = (double) distanceBetween2Points(p0, p1);

            //double slope = deltaZ / distanceXY;

            //return slope;

            //double deltaY = (double)System.Math.Abs(point3d1.Y - point3d0.Y);
            double deltaY = point3d1.Y - point3d0.Y;    // Non-Absolute-Value to ensure slopes move in the same direction..

            Point p0 = new Point((double)point3d0.X, (double)point3d0.Z);
            Point p1 = new Point((double)point3d1.X, (double)point3d1.Z);

            double distanceXZ = (double)distanceBetween2Points(p0, p1);
            /*
            if (distanceXZ < 0.3 && deltaY < 0.3)
                return -99999;*/
            
            double slope = 0;

            try
            {
                slope = deltaY / distanceXZ;
            }
            catch (Exception)
            {
            }

            return slope;
        }

        public static double distanceBetween2Points(Point point1, Point point2)
        {
            return (double)System.Math.Sqrt(((point2.X - point1.X) * (point2.X - point1.X)) + ((point2.Y - point1.Y) * (point2.Y - point1.Y)));
        }

        /// <summary>
        /// Calculates slopes in 3d plane based on formula
        /// </summary>
        /// <param name="points3d"></param>
        /// <returns></returns>
        public static List<double> get3dSlope(Point3D[] points3d)
        {
            if (points3d == null)
            {
                //Error, precondition not met
                return null;
            }

            List<double> slopes = null;
            if (slopes == null)
            {
                slopes = new List<double>();
                Point3D previousPoint = new Point3D(-99999, -99999, -99999);
                int count = 0;

                // loop through each point of the function and use the
                // adjacent point to determine the next slope.
                //foreach (Point3D point in points3d)
                for (int i = 15; i < points3d.Length - 15; i++)
                {
                    if ((int)previousPoint.X != -99999)
                    {
                        // calculate slope
                        //double slope = (point.Y - previousPoint.Y) / (point.X - previousPoint.X);
                        double slope = get3dSlope(points3d[i], previousPoint);
                        if (slope == -99999) continue;
                        slopes.Add(slope);
                        count++;
                    }

                    previousPoint = points3d[i];
                }

            }
            // return the set of slopes
            return slopes;
        }

        /// <summary>
        /// Gets the smallest and largest X values
        /// </summary>
        /// <param name="pathCoordinates"> Original list </param>
        /// <param name="smallestXValue"> smallest X-Value of pathCoordinates </param>
        /// <param name="largestXValue"> largest X-Value of pathCoordinates </param>
        public  static void getSmallestAndLargestXValue(List<Point3D> pathCoordinates, ref double smallestXValue, ref double largestXValue)
        {
            if (pathCoordinates.Count < 1)
            {
                return;
            }

            double[] xValues = new double[pathCoordinates.Count];

            for (int i = 0; i < xValues.Length; i++)
            {
                xValues[i] = (double) pathCoordinates[i].X;
            }

            Array.Sort(xValues);
            smallestXValue = xValues[0];
            largestXValue = xValues[xValues.Length - 1];
        }

        public static void comparePathSlopes(List<double> subjectASlopes, List<double> subjectBSlopes, ref int goodSyncCount, ref int badSyncCount, double LOCAL_COEFFICIENT_OF_CORRECT_SYNC = 0.5/*0.285F*/)
        {
            int lengthOfComparisonA = 0;
            int lengthOfComparisonB = 0;
            bool flag = false;
            if (subjectASlopes.Count < subjectBSlopes.Count)
            {
                lengthOfComparisonA = subjectASlopes.Count - 1;
            }
            else
            {
                lengthOfComparisonB = subjectBSlopes.Count - 1;
            }

            if (lengthOfComparisonA != 0)
            {
                if (lengthOfComparisonA >= subjectBSlopes.Count/2)
                    flag = true;
            }
            else
            {
                if (lengthOfComparisonB >= subjectASlopes.Count/2)
                    flag = true;
            }

            //later, comparePathSlopes will have parameters that specify start and end points
            goodSyncCount = 0;
            badSyncCount = 0;

            //subjectASlopes.Length-1 because the last slope values for A and B are always 0
            if (flag)
            {
                int comp;
                if(lengthOfComparisonA>lengthOfComparisonB) comp = lengthOfComparisonA;
                else comp = lengthOfComparisonB;
                 
                for (int i = 0; i < comp ; i++)
                {
                    double difference = subjectASlopes[i] - subjectBSlopes[i];
                    if (System.Math.Abs(difference) <= LOCAL_COEFFICIENT_OF_CORRECT_SYNC)
                    {
                        goodSyncCount++;
                    }
                    else
                    {
                        badSyncCount++;
                    }
                }
            }
        }

        private void comparePath(Point3D[] p0, Point3D[] p1, ref int good, ref int bad)
        {
            List<double> t0Slopes = null;
            List<double> t1Slopes = null;
            // block for comparing

            t0Slopes = LagrangeInterpolation.get3dSlope(p0);

            t1Slopes = LagrangeInterpolation.get3dSlope(p1);

            good = 0;
            bad = 0;
            LagrangeInterpolation.comparePathSlopes(t0Slopes, t1Slopes, ref good, ref bad);
        }

        public static void ComparePaths(JointPath a, JointPath b, out double percentGoodSync, out double percentBadSync)
        {
            percentGoodSync = 0;
            percentBadSync = 0;

            if (a == null || b == null)
                return;

            List<double> slopesA = LagrangeInterpolation.Get3DSlopes(a);
            List<double> slopesB = LagrangeInterpolation.Get3DSlopes(b);


            int numGood = 0;
            int numBad = 0;

            LagrangeInterpolation.comparePathSlopes(slopesA, slopesB, ref numGood, ref numBad);

            int total = numGood + numBad;

            percentGoodSync = ((double)numGood) / total;
            percentBadSync = ((double)numBad) / total;
        }

        public static List<double> Get3DSlopes(JointPath path)
        {
            if (path == null || path.PathPoints == null)
            {
                //Error, precondition not met
                return null;
            }

            List<double> slopes = null;

            if (slopes == null)
            {
                slopes = new List<double>();

                Point3D previousPoint = new Point3D(-99999, -99999, -99999);

                int count = 0;

                // loop through each point of the function and use the
                // adjacent point to determine the next slope.
                //foreach (Point3D point in points3d)
                for (int i = 0; i < path.PathPoints.Count; i++)
                {
                    if ((int)previousPoint.X != -99999)
                    {
                        // calculate slope
                        //double slope = (point.Y - previousPoint.Y) / (point.X - previousPoint.X);
                        double slope = get3dSlope(path.PathPoints[i].Location, previousPoint);

                        if (slope == -99999)
                            continue;

                        slopes.Add(slope);

                        count++;
                    }

                    previousPoint = path.PathPoints[i].Location;
                }
            }

            // return the set of slopes
            return slopes;
        }
    }
}
