﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using System.IO;
using AIMS.Tasks;
using Npgsql;

namespace AIMS.Pages
{
    /// <summary>
    /// Interaction logic for UserDashboard.xaml
    /// </summary>
    public partial class UserDashboard : Page
    {
        /*
        public static readonly DependencyProperty KinectSensorManagerProperty = DependencyProperty.Register( "KinectSensorManager", typeof(KinectSensorManager), typeof(UserDashboard), new PropertyMetadata(null));

        public KinectSensorManager KinectSensorManager
        {
            get { return (KinectSensorManager)GetValue(KinectSensorManagerProperty); }
            set { SetValue(KinectSensorManagerProperty, value); }
        }
        */

        private Dictionary<string, int> taskFlowIndex = new Dictionary<string, int>();

        // Since the timer resolution defaults to about 10ms precisely, we need to
        // increase the resolution to get framerates above between 50fps with any
        // consistency.
        [DllImport("Winmm.dll", EntryPoint = "timeBeginPeriod")]
        private static extern int TimeBeginPeriod(uint period);

        private List<Message> messages;
        private List<Message> unreadMessages = new List<Message>();
        private List<Course> courses;
        private List<Assignment> assignments;

        public List<Message> Messages { get { return messages; } set { messages = value; } }
        public List<Message> UnreadMessages { get { return unreadMessages; } set { unreadMessages = value; } }

        public int UnreadMessageCount
        {
            get
            {
                if (unreadMessages == null)
                    return 0;
                else
                {
                    return unreadMessages.Count;
                }
            }
        }

        public UserDashboard()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                InitializeComponent();
                Loaded += this.UserDashboard_Loaded;
                Unloaded += this.UserDashboard_Unloaded;
            }
        }

        private void UserDashboard_Loaded(object sender, EventArgs e)
        {
            if (KinectManager.Kinect != null)   // check if kinect exists
            {
                flow.KinectSensor = KinectManager.Kinect;   // set the kinect of the flow control
                KinectManager.KinectSpeechCommander.SetModuleGrammars(new Uri("pack://application:,,,/AIMS;component/UserDashboardSpeechGrammar.xml"),
                                                                        "activeListen", "defaultListen");
                KinectManager.KinectSpeechCommander.SpeechRecognized += new EventHandler<Speech.SpeechCommanderEventArgs>(KinectSpeechCommander_SpeechRecognized);
                //KinectManager.Kinect.SkeletonFrameReady += this.Kinect_SkeletonFrameReady;  // add handler for skeletonframeready (used to control hands-free cursor).
                //handCursor.Click += new RoutedEventHandler(handCursor_Click);

                //KinectManager.SkeletonDataReady += this.KinectSkeletonDataReady;
            }
            else
            {
                MessageBox.Show("Kinect is not connected."); // notify user if kinect isn't set
            }

            this.BlurEffect.Radius = 0; // reset blur radius to 0

            FindUnreadMessageCount();   // update the message count
            //FindClasses();
            //FindAssignments();

            assignments = RetreiveAssignmentsForStudent( ((App)App.Current).CurrentStudent );
            messages = RetreiveMessagesForStudent(((App)App.Current).CurrentStudent);

            foreach (Message message in messages)
            {
                if (message != null)
                {
                    if (!message.HasBeenRead)
                    {
                        unreadMessages.Add(message);
                    }
                }
            }

            courses = RetreiveCoursesForStudentFromCourseIDs(((App)App.Current).CurrentStudent);

            LoadIntubationStatsheetForCurrentStudent();

            this.IntubationStatsheetComponent.ShowOverlay(((App)App.Current).CurrentStudent);

            flow.Cache = new ThumbnailManager();    // create the flow cache (enhances re-loading preformance).

            LoadFlowImages();
        }

        //C:\Users\eric.wilkinson\Documents\Visual Studio 2010\Projects\AIMS_Updating2\AIMS_Updating\AIMS\Assets\Graphics\Intubation_trim.png

        private void UserDashboard_Unloaded(object sender, RoutedEventArgs e)
        {
            // Handle unloading of student dashboard components here
            if (KinectManager.Kinect != null)   // if the kinect was never there, then no handlers were ever attached to begin with.
            {
                KinectManager.KinectSpeechCommander.SpeechRecognized -= KinectSpeechCommander_SpeechRecognized;
                //KinectManager.Kinect.SkeletonFrameReady -= this.Kinect_SkeletonFrameReady;  // unplug the skeletalframeready handler
                //handCursor.Click -= new RoutedEventHandler(handCursor_Click);   // unplug the cursor mouse click handler

                //KinectManager.SkeletonDataReady -= this.KinectSkeletonDataReady;
            }
        }

        #region Kinect Speech processing
        private void KinectSpeechCommander_SpeechRecognized(object sender, Speech.SpeechCommanderEventArgs e)
        {
            switch (e.Command)
            {
                case "PAUSE":
                    OnSaid_Pause();
                    break;
                case "RESUME":
                    OnSaid_Resume();
                    break;
                case "LOGOUT":
                    OnSaid_Logout();
                    break;
                case "CPR":
                    OnSaid_CPR();
                    break;
                case "INTUBATION":
                    OnSaid_Intubation();
                    break;
                case "VERTICALLIFT":
                    OnSaid_VerticalLift();
                    break;
                case "PRACTICE":
                    OnSaid_Practice();
                    break;
                case "RESULTS":
                    OnSaid_Results();
                    break;
                case "TEST":
                    OnSaid_Test();
                    break;
                case "STARTFROMOVERLAY":
                    OnSaid_StartFromOverlay();
                    break;
                case "EXITOVERLAY":
                    OnSaid_ExitOverlay();
                    break;
                default:
                    break;
            }
        }

        private void OnSaid_Pause()
        {
            //SpeechDetectedTextBlock.Text = String.Format("Speech Detected: {0}", "Pause");
        }

        private void OnSaid_Resume()
        {
            //SpeechDetectedTextBlock.Text = String.Format("Speech Detected: {0}", "Resume");
        }

        private void OnSaid_Logout()
        {
            //SpeechDetectedTextBlock.Text = String.Format("Speech Detected: {0}", "Logout");

            this.NavigationHeaderBar.Logout();
        }

        private void OnSaid_Test()
        {
            //SpeechDetectedTextBlock.Text = String.Format("Speech Detected: Do Task - {0}", "Test");

            ShowStartTaskVerificationOverlay(TaskMode.Test);  // blurs the page, and displays the task verification overlay
            //NavigateToTestVerificationPage();
        }

        private void OnSaid_Practice()
        {
            //SpeechDetectedTextBlock.Text = String.Format("Speech Detected: Do Task - {0}", "Practice");

            ShowStartTaskVerificationOverlay(TaskMode.Practice);  // blurs the page, and displays the task verification overlay
            //NavigateToPracticeVerificationPage();
        }

        private void OnSaid_Results()
        {
            //SpeechDetectedTextBlock.Text = String.Format("Speech Detected: Do Task - {0}", "Results");

            NavigateToResultsPage();
        }

        private void OnSaid_Intubation()
        {
            //SpeechDetectedTextBlock.Text = String.Format("Speech Detected: Do Task - {0}", "Intubation");

            flow.Index = 0; // Index of the Intubation button within the Coverflow (first)
        }

        private void OnSaid_CPR()
        {
            //SpeechDetectedTextBlock.Text = String.Format("Speech Detected: Do Task - {0}", "CPR");

            flow.Index = 1; // Index of the Intubation button within the Coverflow (first)
        }

        private void OnSaid_VerticalLift()
        {
            //SpeechDetectedTextBlock.Text = String.Format("Speech Detected: Do Task - {0}", "Vertical Lift");

            flow.Index = 2; // Index of the Intubation button within the Coverflow (first)
        }

        private void OnSaid_StartFromOverlay()
        {
            //SpeechDetectedTextBlock.Text = String.Format("Speech Detected: {0}", "Start Task (from overlay)");

            if( this.StartTaskVerificationOverlay.IsVisible )
            {
                this.StartTaskVerificationOverlay.StartTask();  // start the task
            }
        }

        private void OnSaid_ExitOverlay()
        {
            //SpeechDetectedTextBlock.Text = String.Format("Speech Detected: {0}", "Exit Overlay");

            if (this.StartTaskVerificationOverlay.IsVisible)
            {
                this.StartTaskVerificationOverlay.ExitOverlay();    // close out the overlay
            }
        }

        /*
        private void EnableAecChecked(object sender, RoutedEventArgs e)
        {
            var enableAecCheckBox = (CheckBox)sender;
            this.UpdateEchoCancellation(enableAecCheckBox);
        }

        private void UpdateEchoCancellation(CheckBox aecCheckBox)
        {
            this.mySpeechRecognizer.EchoCancellationMode = aecCheckBox.IsChecked != null && aecCheckBox.IsChecked.Value
                ? EchoCancellationMode.CancellationAndSuppression
                : EchoCancellationMode.None;
        }*/

        #endregion Kinect Speech processing

        private class FileInfoComparer : IComparer<FileInfo>
        {
            #region IComparer<FileInfo> Membres
            public int Compare(FileInfo x, FileInfo y)
            {
                return string.Compare(x.FullName, y.FullName);
            }
            #endregion
        }

        #region Handlers
        private void DoKeyDown(Key key)
        {
            switch (key)
            {
                case Key.Right:
                    flow.GoToNext();
                    break;
                case Key.Left:
                    flow.GoToPrevious();
                    break;
                case Key.PageUp:
                    flow.GoToNextPage();
                    break;
                case Key.PageDown:
                    flow.GoToPreviousPage();
                    break;
            }
            // if (flow.Index != Convert.ToInt32(slider.Value))
            //slider.Value = flow.Index;
        }
   
        private void Page_KeyDown(object sender, KeyEventArgs e)
        {
            DoKeyDown(e.Key);
        }
        private void slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //flow.Index = Convert.ToInt32(slider.Value);
        }

        #endregion
        #region Private stuff
        public void LoadFlowImages()
        {
            /*
            var imageDir = new DirectoryInfo(imagePath);
            var images = new List<FileInfo>(imageDir.GetFiles("*.png"));
            images.Sort(new FileInfoComparer());
            foreach (FileInfo f in images)
                flow.Add(Environment.MachineName, f.FullName);*/

            bool hasintubation = false;
            bool hascpr = false;
            bool hasverticallift = false;

            // Cycle through the student's enrolled courses and determine if they have access to each of the lessons.
            foreach (Course course in courses)
            {
                if (course.HasIntubation)
                {
                    hasintubation = true;
                }
                if (course.HasCPR)
                {
                    hascpr = true;
                }
                if (course.HasVerticalLift)
                {
                    hasverticallift = true;
                }
            }

            // If any of the flag were set by the list of courses enrolled, add that lesson's representative image to the coverflow.
            if (hasintubation)
            {
                flow.Add(null, "Assets/Graphics/Dashboard Graphics/Intubation_trim.png");
                //flow.Add(Environment.MachineName, new Uri(@"pack://application:,,,/Assets/Graphics/Intubation_trim.png", UriKind.Absolute).LocalPath);
            }
            if (hascpr)
            {
                flow.Add(null, "Assets/Graphics/Dashboard Graphics/CPR_trim.png");
                //flow.Add(Environment.MachineName, new Uri(@"pack://application:,,,/Assets/Graphics/CPR_trim.png", UriKind.Absolute).LocalPath);
            }
            if (hasverticallift)
            {
                flow.Add(null, "Assets/Graphics/Dashboard Graphics/VerticalLift.png");
            }
        }
        #endregion

        private void PracticeButton_Clicked(object sender, RoutedEventArgs e)
        {
            ShowStartTaskVerificationOverlay(TaskMode.Practice);  // blurs the page, and displays the task verification overlay
        }

        private void TestButton_Clicked(object sender, RoutedEventArgs e)
        {
            ShowStartTaskVerificationOverlay( TaskMode.Test );  // blurs the page, and displays the task verification overlay with Test text
        }

        private void ResultsButton_Clicked(object sender, RoutedEventArgs e)
        {
            NavigateToResultsPage();
        }

        private void MessagesButton_Clicked(object sender, RoutedEventArgs e)
        {
            ShowStudentMessagesOverlay();
        }

        private void ShowStudentMessagesOverlay()
        {
            //this.BlurEffect.Radius = 20;
            StudentMessageDisplay.ShowMessages( messages );
            //StudentMessagesOverlay.ShowMessages();
        }

        private void LoadIntubationStatsheetForCurrentStudent()
        {
            if (((App)App.Current).CurrentStudent == null)
                return;

            LessonStatsheet statsheet = null;

            //statsheet = IntubationStatsheet.GetIntubationStatsheetFromStudentID( ((App)App.Current).CurrentStudent.ID );  // attempts to find an intubation statsheet but will NOT create a new one if none is found.
            statsheet = LessonStatsheet.GetOrCreateStatsheetForStudent(((App)App.Current).CurrentStudent.ID, LessonType.Intubation);   // attempts to find an intubation statsheet, and will also attempt to create a new one if none is found.
            
            
            if (statsheet == null)
            {
                // No statsheet was found for the student.
                //MessageBox.Show("No intubation statsheet was found for the current student.");
            }
            else
            {
                // The statsheet was found for the student.
                //MessageBox.Show("An intubation statsheet was found for the current student.");

                ((App)App.Current).CurrentStudent.IntubationStatsheet = statsheet;  // Attach the intubation statsheet to the student for further usage throughout this session.
            }

        }

        private void ShowStartTaskVerificationOverlay( TaskMode taskmode )
        {
            //MessageBox.Show(String.Format( "Flow index is {0}", flow.Index.ToString()));

            Task task = Task.None;

            switch (flow.Index)
            {
                case 0:
                {
                    task = Task.Intubation; break;
                }
                case 1:
                {
                    task = Task.VerticalLift; break;
                }
                case 2:
                {
                    task = Task.CPR; break;
                }
            }

            this.StartTaskVerificationOverlay.ShowOverlay(taskmode, task);
        }

        /// <summary>
        /// Navigates to the results page of the currently selected task, for the current user.
        /// </summary>
        private void NavigateToResultsPage()
        {
            try
            {
                switch ( flow.Index )
                {
                    case 0:
                    {
                        ( (App)(App.Current) ).CurrentTask = Task.Intubation;

                        NavigationService.Navigate( new System.Uri( @"../StudentResultsPage.xaml", System.UriKind.Relative ) );

                        break;
                    }
                    case 1:
                    {
                        ( (App)(App.Current) ).CurrentTask = Task.VerticalLift;

                        NavigationService.Navigate( new System.Uri( @"../StudentResultsPage.xaml", System.UriKind.Relative ) );
                        
                        break;
                    }
                    case 2:
                    {
                        ( (App)(App.Current) ).CurrentTask = Task.CPR;

                        NavigationService.Navigate( new System.Uri( @"../StudentResultsPage.xaml", System.UriKind.Relative ) );

                        break;
                    }
                }
            }
            catch
            {
                MessageBox.Show( "Could not navigate to index of currently selected task on Flow Component." );
            }
        }

        private void FindUnreadMessageCount()
        {
            if ( ( (App)App.Current ).CurrentStudent == null )
            {
                MessageBox.Show( "No student is currently logged in to retreive unread messages for." );
                return;
            }

            if ( DatabaseManager.Connection.State != System.Data.ConnectionState.Open )
            {
                DatabaseManager.OpenConnection();
            }

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "SELECT COUNT(*) FROM messages WHERE toid = :toid AND hasbeenread = :hasbeenread";

            command.Parameters.Add( "toid", NpgsqlTypes.NpgsqlDbType.Integer ).Value = ( (App)App.Current ).CurrentStudent.ID;
            command.Parameters.Add( "hasbeenread", NpgsqlTypes.NpgsqlDbType.Boolean ).Value = false;

            Int64 res = ( Int64 )command.ExecuteScalar();

            //MessageBox.Show(String.Format("You have {0} new messages", res <= 0 ? "no" : res.ToString()));

            UnreadMessageCountTextBlock.Text = System.Math.Max( 0, res ).ToString();    // set the number on the counter text (in the messages button). 
            UnreadMessageCountTextBlock.Visibility = ( res <= 0 ? System.Windows.Visibility.Hidden : System.Windows.Visibility.Visible );

            DatabaseManager.CloseConnection();
        }

        private List<Course> RetreiveCoursesForStudentFromCourseIDs( Student student )
        {
            if ( student == null ) 
                return new List<Course>();

            List<Course> courses = new List<Course>();

            foreach ( int courseID in student.EnrolledCourseIDs )
            {
                Course course = Course.GetCourseFromCourseID( courseID );

                if ( course != null )
                {
                    courses.Add( course );
                }
            }

            return courses;
        }

        private List<Assignment> RetreiveAssignmentsForStudent( Student student )
        {
            if ( student == null )
                return new List<Assignment>();

            List<Assignment> assignments = new List<Assignment>();

            foreach ( int courseID in student.EnrolledCourseIDs )
            {
                List<Assignment> asignmnts = Assignment.GetAssignmentsForCourseID( courseID );

                if ( asignmnts != null )
                {
                    assignments.AddRange( asignmnts );
                }
            }

            return assignments;
        }

        private List<Message> RetreiveMessagesForStudent( Student student )
        {
            if ( student == null )
                return new List<Message>();

            List<Message> messages = Message.FindMessagesForStudentID( student.ID );

            return messages;
        }

        /*
        private void SidePanelExpansionButton_Click(object sender, RoutedEventArgs e)
        {
            if (SidePanelContentGrid.Visibility == System.Windows.Visibility.Collapsed)
            {
                SidePanelContentGrid.Visibility = System.Windows.Visibility.Visible;
                (sender as Button).Content = "<";
                SidePanelContentGrid.Focus();
            }
            else
            {
                SidePanelContentGrid.Visibility = System.Windows.Visibility.Collapsed;
                (sender as Button).Content = ">";
                SidePanelContentGrid.Focus();
            }
        }

        private void HeaderButtonsListBox_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                HeaderButtonsListBox.SelectedIndex = 0;
            }
            catch
            {
            }
        }

        private void ProceduralTaskButtonsListBox_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {

            }
            catch
            {
            }
        }

        private void ProceduralTaskButtonsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            try
            {
                switch (ProceduralTaskButtonsListBox.SelectedIndex)
                {
                    case 0:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    case 1:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    case 2:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    case 3:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    case 4:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    default:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                }
            }
            catch
            {
            }
        }

        private void HeaderButtonsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (HeaderButtonsListBox.SelectedIndex == -1)
            {
                return;
            }

            try
            {
                switch (HeaderButtonsListBox.SelectedIndex)
                {
                    case 0: // Home

                        break;
                    case 1: // Procedural Tasks
                        //HeaderButtonsListBox.SelectedIndex = -1;    // Deselect
                        ProceduralTaskButtonsListBox.SelectedIndex = -1;    // Deselect
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Visible;
                        break;
                    case 2: // Safe Patient Handling

                        break;
                    case 3: // Virtual Patient

                        break;
                    case 4: // Messages
                        ShowStudentMessagesOverlay();
                        break;
                    default:

                        break;
                }
            }
            catch
            {
            }
        }

        private void ProceduralTaskButtonsListBox_MouseLeave(object sender, MouseEventArgs e)
        {
            ProceduralTaskButtonsListBox.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void AssessmentsButton_Clicked(object sender, RoutedEventArgs e)
        {

        }

        private void TopPanelExpansionButton_Click(object sender, RoutedEventArgs e)
        {
            if (TopPanelContentGrid.Visibility == System.Windows.Visibility.Collapsed)
            {
                TopPanelContentGrid.Visibility = System.Windows.Visibility.Visible;
                (sender as Button).Content = "^";
                TopPanelContentGrid.Focus();
            }
            else
            {
                TopPanelContentGrid.Visibility = System.Windows.Visibility.Collapsed;
                (sender as Button).Content = "v";
                TopPanelContentGrid.Focus();
            }
        }*/
    }
}
