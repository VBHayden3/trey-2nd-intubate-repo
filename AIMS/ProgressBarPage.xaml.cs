﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace AIMS
{
	public partial class ProgressBarPage : Page, INotifyPropertyChanged
	{
		private double accuracy = 70.0;
		public double Accuracy
		{
			get { return accuracy; }
			set
			{
				if( accuracy == value )
					accuracy = value;
				else
				{
					accuracy = value;
					NotifyPropertyChanged("Accuracy");
				}
					
			}
		}
		
		private int totalActions = 3;
		public int TotalActions
		{
			get { return totalActions; }
			set
			{
				if( totalActions == value )
					totalActions = value;
				else
				{
					totalActions = value;
					NotifyPropertyChanged("TotalActions");
				}
					
			}
		}
		
		private int maxActions = 5;
		public int MaxActions
		{
			get { return maxActions; }
			set
			{
				if( maxActions == value )
					maxActions = value;
				else
				{
					maxActions = value;
					NotifyPropertyChanged("MaxActions");
				}
					
			}
		}
		
		public double ActionsPercent
		{
			get
			{
				return ( this.MaxActions == 0 ) ? 0 : ((double)this.TotalActions)/this.MaxActions *100;
			}
		}
		
		private TimeSpan totalTime = TimeSpan.FromSeconds(25.0);
		public TimeSpan TotalTime
		{
			get { return totalTime; }
			set
			{
				if( totalTime == value )
					totalTime = value;
				else
				{
					totalTime = value;
					NotifyPropertyChanged("TotalTime");
					NotifyPropertyChanged("TimePercent");
				}
					
			}
		}
		
		private TimeSpan maxTime = TimeSpan.FromSeconds(30.0);
		public TimeSpan MaxTime
		{
			get { return maxTime; }
			set
			{
				if( maxTime == value )
					maxTime = value;
				else
				{
					maxTime = value;
					NotifyPropertyChanged("MaxTime");
					NotifyPropertyChanged("TimePercent");
				}
					
			}
		}
		
		public double TimePercent
		{
			get
			{
				return ( this.MaxTime.TotalSeconds == 0 ) ? 0 : this.TotalTime.TotalSeconds/this.MaxTime.TotalSeconds *100;
			}
		}
		
		public ProgressBarPage()
		{
			// Required to initialize variables
			InitializeComponent();
			
			this.DataContext = this;
		}
		
		public event PropertyChangedEventHandler PropertyChanged;
		
        private void NotifyPropertyChanged( string propertyName )
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
	}
}