﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Npgsql;
using System.Data;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Controls.DataVisualization.Charting.Primitives;
using System.IO;
using System.ComponentModel;
using System.Windows.Threading;
using AIMS.Speech;

namespace AIMS
{
    /// <summary>
    /// Interaction logic for StudentResultsPage.xaml
    /// </summary>
    public partial class StudentResultsPage : Page
    {
        private DataSet ds = new DataSet();
        private DataTable dt = new DataTable();

        //private byte[] colorData = new byte[640 * 480 * 4];
        //private short[] depthData = new short[640 * 480];

        private int displayIndex = 0;
        DispatcherTimer timer;

        private List<Result> studentResultList = new List<Result>();
        private List<StageScore> stageScoreList = new List<StageScore>();
        private List<int> stageScoreIndexList = new List<int>();

        List<List<KeyValuePair<DateTime, int>>> chartValueLists = new List<List<KeyValuePair<DateTime, int>>>();

        public StudentResultsPage()
        {
            InitializeComponent();

            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                Loaded += ResultsPage_Loaded;
            }
        }

        private void singleChart_DataPoint_MouseClick(object sender, MouseEventArgs e)
        {
            DataPoint dp = sender as DataPoint;

            if (dp != null)
            {
                e.Handled = true;   // handle the exception

                //MessageBox.Show(String.Format("x: {0}, y: {1}", dp.IndependentValue, dp.DependentValue));
            }
        }

        private void ResultsPage_Loaded(object sender, EventArgs e)
        {
            FillResultChartsFromDatabase(); // Fill initial chart

            // Create a timer, for 1 second, this allows the initial chart time to complete.. else there are *sometimes* conflicts w/ the chart populating, and creating the graphs using chart information.
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1.0);
            timer.Tick += timer_Tick;
            timer.Start();

            if (KinectManager.KinectSpeechCommander != null)
            {
                KinectManager.KinectSpeechCommander.SetModuleGrammars(new Uri("pack://application:,,,/AIMS;component/StudentResultsSpeechGrammar.xml"),
                                                        "activeListen", "defaultListen");
            }
        }

        private void ResultsPage_Unloaded(object sender, EventArgs e)
        {
            if (timer != null)
            {
                timer.Stop();
                timer = null;
            }

            if (KinectManager.KinectSpeechCommander != null)
            {
                KinectManager.KinectSpeechCommander.SpeechRecognized -= this.KinectSpeechCommander_SpeechRecognized;
            }
        }
        
        #region Kinect Speech processing
        private void KinectSpeechCommander_SpeechRecognized(object sender, SpeechCommanderEventArgs e)
        {
            switch (e.Command)
            {
                case "LOGOUT":
                    OnSaid_Logout();
                    break;
                case "HOME":
                    OnSaid_Home();
                    break;
                case "NEXT":
                    OnSaid_Next();
                    break;
                case "LAST":
                    OnSaid_Last();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// "Logout" was said, logout and navigate to the login page.
        /// </summary>
        private void OnSaid_Logout()
        {
            //this.NavigationHeaderBar.Logout();
        }

        /// <summary>
        /// "Home" was said, navigate to the user's home dashboard.
        /// </summary>
        private void OnSaid_Home()
        {
            //this.NavigationHeaderBar.GoHome();
        }

        /// <summary>
        /// Determine if the user meant NextResult or NextStageScore.
        /// Navigate appropriately.
        /// </summary>
        private void OnSaid_Next()
        {
            NextResult();   // TEMP - only goes to next result, doesn't yet try next stage score
        }

        /// <summary>
        /// Determine if the user meant LastResult or LastStageScore.
        /// Navigate appropriately.
        /// </summary>
        private void OnSaid_Last()
        {
            PreviousResult();   // TEMP - only goes to last result, doesn't yet try last stage score
        }
        #endregion Kinect Speech processing


        /// <summary>
        /// Delayed timer that populates the graphs.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_Tick(object sender, EventArgs e)
        {
            if (ResultsDataGrid.HasItems)
            {
                ResultsDataGrid.SelectedValuePath = "uid";
                ResultsDataGrid.SelectedItem = ResultsDataGrid.Items[0];
                deserializeStageScoreFromDatabase();
                ResultsDataGrid.Columns[0].Visibility = Visibility.Hidden;  // Hide the UID column.
            }

            ResultsDataGrid.Focus();
            ResultsDataGrid.BeginEdit();

            showOverallChart(); // creates the overall chart, and begins the singles chart.

            ResultsDataGrid.Visibility = Visibility.Visible;    // Show the chart.
            overallChart.Visibility = Visibility.Visible;       // Show the overall chart.
            singleChart.Visibility = Visibility.Visible;        // Show the singles chart.
            _3DViewer.Visibility = Visibility.Visible;          // Show the 3d viewer.

            timer.Stop();
        }


        private void FillResultChartsFromDatabase()
        {
            try
            {
                dt = CreateDataTable(); // builds a data table using static columns, uses DB data to populate, and sets our "dt" datatable value to that table
            }
            catch (Exception msg)
            {
                Console.WriteLine(msg.ToString());
                //MessageBox.Show(msg.ToString());
            }
        }

        /*
         
        /// <summary>
        /// This method reads temporary data stored in c:/temp/color and c:/temp/depth,
        /// and uses these datas to draw the screenshot in the 3DViewer.
        /// </summary>
        private void draw3DPanel()
        {
            
            // Load the color data
            using (FileStream fs = File.Open(@"c:/temp/color", FileMode.Open))
            {
                fs.Read(colorData, 0, 640 * 480 * 4);
            }

            // Load the depth data
            using (FileStream fs = File.Open(@"c:/temp/depth", FileMode.Open))
            {
                using (BinaryReader br = new BinaryReader(fs))
                {
                    for (int i = 0; i < (640 * 480); i++)
                    {
                        depthData[i] = br.ReadInt16();
                    }
                }
            }

            draw3DPanel(colorData, depthData);
        }

        */

        /// <summary>
        /// Pulls the colorData and depthData byte/short arrays out of the screenshot, and then makes the call
        /// to draw3DPanel with passing these two arrays in as parameters.
        /// </summary>
        /// <param name="screenshot"></param>
        private void draw3DPanel( StageScore.Screenshot screenshot )
        {
            if( screenshot != null )
            {
                draw3DPanel(screenshot.colorData, screenshot.depthData);
            }
        }

        /// <summary>
        /// This method sets the color and depth data for the 3DViewer panel.
        /// </summary>
        private void draw3DPanel( byte[] colorData, ushort[] depthData )
        {
            _3DViewer.setImages(colorData, depthData);  // draw the screenshot into the 3DViewer using the color and depth data
        }

        /// <summary>
        /// This method is used to pull out the byte[] from the database, and deserialize it. 
        /// This can then be used to populate the singleChart for the current "displayIndex".
        /// </summary>
        private void deserializeStageScoreFromDatabase()
        {
            if (ResultsDataGrid.SelectedValue == null)
                return; // no selected value on datagrid, therefore cannot create a graph to represent that value..
            
            // Verify that the result number exists in the database.
            int resultnumber = (int)ResultsDataGrid.SelectedValue;
            

            ////MessageBox.Show("Selected Value set to " + resultnumber);

            DatabaseManager.CloseConnection();

            if (DatabaseManager.Connection.State != ConnectionState.Open)
            {
                DatabaseManager.OpenConnection();
            }

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandText = "SELECT uid, resultid, stagescore FROM student_results_stage_scores WHERE resultid = :resultnumber";
            command.Parameters.Add("resultnumber", NpgsqlTypes.NpgsqlDbType.Integer).Value = resultnumber;

            NpgsqlDataReader sqlresult = command.ExecuteReader();

            stageScoreList = null;
            stageScoreList = new List<StageScore>();
            stageScoreIndexList = null;
            stageScoreIndexList = new List<int>();

            int uid;
            int resultid;
            byte[] stagescore = null;

            try
            {
                while (sqlresult.Read())
                {
                    uid = sqlresult.GetInt32(sqlresult.GetOrdinal("uid"));
                    resultid = sqlresult.GetInt32(sqlresult.GetOrdinal("resultid"));
                    stagescore = (byte[])sqlresult.GetValue(sqlresult.GetOrdinal("stagescore"));

                    StageScore stageScore = null;

                    try
                    {
                        stageScore = ByteArrayToObject(stagescore) as StageScore;   // deserialize the stageScore byteArray data read from the db.
                    }
                    catch
                    {
                        //MessageBox.Show(exception.ToString());  // error deserializing..
                    }

                    if( stageScore != null )    // if the stage score exists
                    {
                        stageScoreList.Add(stageScore); // add it to the list of stage scores.
                        stageScoreIndexList.Add(uid);
                    }
                }
            }
            catch( Exception e )
            {
                //MessageBox.Show( e.ToString(), "StageScore was not successfully pulled from Database.");
                return;
            }

            DatabaseManager.CloseConnection();

            // Build out the singleChart using this data list
            BuildSingleResultChart(stageScoreList);
        }

        /// <summary>
        /// This method is called when a DataPoint on the line chart is clicked, it finds the index of the clicked point, checks if there is a screenshot,
        /// and then finds the linked stagescoreID number. With this number, the method recalls deserializeScreenShotFromDatabase - this
        /// time passing in the int value of the linked stagescoreID.
        /// </summary>
        /// <param name="dp"></param>
        private void deserializeScreenShotFromDatabase( KeyValuePair<int, double> pair )
        {
            int index = pair.Key - 1;   // key's represet stage score # -> they are 1-5 (for example) rather than 0-4, so we subtract 1  to get proper 0-based index value.

            StageScore stagescore = stageScoreList[index];
            int stagescoreid = stageScoreIndexList[index];

            if (stagescore.HasScreenshot)
            {
                deserializeScreenShotFromDatabase(stagescoreid);
            }
            else
            {
                //MessageBox.Show("StageScore has no screenshot");
            }
        }

        /// <summary>
        /// This method is used to access the database and retreive the screenshot binary data linked to the passed stagescoreID#.
        /// The method retreives the binary data, deserializes it into a screenshot object, and then makes a call to draw the 3D screenshot
        /// into the 3DViewer, using the newly re-constructed screenshot object.
        /// </summary>
        /// <param name="stagescoreID"></param>
        private void deserializeScreenShotFromDatabase( int stagescoreID )
        {
            DatabaseManager.CloseConnection();

            if (DatabaseManager.Connection.State != ConnectionState.Open)
            {
                DatabaseManager.OpenConnection();
            }

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandText = String.Format("SELECT uid, stagescoreid, coloranddepthdata FROM student_results_3dscreenshots WHERE stagescoreid = :scoreID");
            command.Parameters.Add("scoreID", NpgsqlTypes.NpgsqlDbType.Integer).Value = stagescoreID;

            NpgsqlDataReader sqlresult = command.ExecuteReader();

            int uid;
            int stagescoreid;
            byte[] coloranddepthdata = null;

            try
            {
                while (sqlresult.Read())
                {
                    uid = sqlresult.GetInt32(sqlresult.GetOrdinal("uid"));
                    stagescoreid = sqlresult.GetInt32(sqlresult.GetOrdinal("stagescoreid"));
                    coloranddepthdata = (byte[])sqlresult.GetValue(sqlresult.GetOrdinal("coloranddepthdata"));

                    StageScore.Screenshot screenshot = null;

                    try
                    {
                        screenshot = ByteArrayToObject(coloranddepthdata) as StageScore.Screenshot;   // deserialize the stageScore byteArray data read from the db.
                    }
                    catch (Exception exception)
                    {
                        //MessageBox.Show(exception.ToString());  // error deserializing..
                    }

                    if (screenshot != null)    // if the stage score exists
                    {
                        draw3DPanel(screenshot);    // make the call to draw the screenshot into the 3DViewer
                    }
                }
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.ToString(), "StageScore was not successfully pulled from Database.");
                return;
            }

            DatabaseManager.CloseConnection();
        }

        private void BuildSingleResultChart( List<StageScore> stageScoreList )
        {
            List<KeyValuePair<int, double>> values = new List<KeyValuePair<int, double>>(); // list of key value pairs (int = stage#, double = score )

            foreach (StageScore stageScore in stageScoreList)
            {
                values.Add( new KeyValuePair<int, double>( stageScore.StageNumber + 1, stageScore.Score * 100 ) );
                ////MessageBox.Show(String.Format("New keyValuePair added: stage: {0}, score: {1}", stageScore.StageNumber + 1, stageScore.Score * 100));
            }

            try
            {
                (singleChart.Series[0] as DataPointSeries).ItemsSource = null;
                (singleChart.Series[0] as DataPointSeries).ItemsSource = values;
                singleChart.UpdateLayout();

                ////MessageBox.Show("SinglesChart updated");
            }
            catch (Exception ex)
            {
                ////MessageBox.Show(ex.ToString());
                Console.WriteLine(ex.ToString());
            }

            /*DataPoint firstDP = ( singleChart.Series[0] as DataPointSeries )*/
            deserializeScreenShotFromDatabase(0);
        }

        /// <summary>
        /// Function to get object from byte array
        /// </summary>
        /// <param name="_ByteArray">byte array to get object</param>
        /// <returns>object</returns>
        public object ByteArrayToObject(byte[] _ByteArray)
        {
            try
            {
                // convert byte array to memory stream
                System.IO.MemoryStream _MemoryStream = new System.IO.MemoryStream(_ByteArray);

                // create new BinaryFormatter
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter _BinaryFormatter
                            = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                // set memory stream position to starting point
                _MemoryStream.Position = 0;

                // Deserializes a stream into an object graph and return as a object.
                return _BinaryFormatter.Deserialize(_MemoryStream);
            }
            catch (Exception _Exception)
            {
                // Error
                Console.WriteLine("Exception caught in process: {0}", _Exception.ToString());
                ////MessageBox.Show(_Exception.ToString());
            }

            // Error occured, return null
            return null;
        }

    
        private void showOverallChart()
        {
            List<KeyValuePair<int, double>> values = new List<KeyValuePair<int, double>>();

            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    values.Add(new KeyValuePair<int, double>((int)row[1], (double)row[4] * 100));
                }
            }

            try
            {
                (overallChart.Series[0] as DataPointSeries).ItemsSource = null;
                (overallChart.Series[0] as DataPointSeries).ItemsSource = values;
                overallChart.UpdateLayout();
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.ToString());
                Console.WriteLine(ex.ToString());
            }
        }

        private void PreviousResult()
        {
            displayIndex--;

            if (displayIndex < 0)
                displayIndex = dt.Rows.Count - 1;

            try
            {
                ResultsDataGrid.SelectedItem = (ResultsDataGrid.Items == null ? null : ResultsDataGrid.Items[displayIndex]);
            }
            catch
            {
                ////MessageBox.Show(String.Format("EXCEPTION: Could not jump to row index {0}.", displayIndex));
            }
            ResultsDataGrid.UpdateLayout();
            ResultsDataGrid.ScrollIntoView(ResultsDataGrid.SelectedItem);

            deserializeStageScoreFromDatabase();
            //showSingleChart(displayIndex);
        }

        private void ResultsScrollUp_Clicked(object sender, RoutedEventArgs e)
        {
            PreviousResult();
        }

        private void ResultsScrollDown_Clicked(object sender, RoutedEventArgs e)
        {
            NextResult();
        }

        private void NextResult()
        {
            displayIndex++;

            if (displayIndex > dt.Rows.Count - 1)
                displayIndex = 0;

            try
            {
                ResultsDataGrid.SelectedItem = (ResultsDataGrid.Items == null ? null : ResultsDataGrid.Items[displayIndex]);
                ResultsDataGrid.UpdateLayout();
                ResultsDataGrid.ScrollIntoView(ResultsDataGrid.SelectedItem);
            }
            catch
            {
                //MessageBox.Show(String.Format("EXCEPTION: Could not jump to row index {0}.", displayIndex));
            }

            deserializeStageScoreFromDatabase();
            //((AreaDataPoint)(overallChart.Series[displayIndex]));
            // showSingleChart(displayIndex);
        }

        private static bool RequestFocusChange(FocusNavigationDirection direction)
        {
            var focused = Keyboard.FocusedElement as UIElement;
            if (focused != null)
            {
                focused.MoveFocus(new TraversalRequest(direction));
                Keyboard.FocusedElement.Focus();
                return true;
            }
            return false;
        }

        private void MoveFocusForward_Executed(object target, ExecutedRoutedEventArgs e)
        {
            RequestFocusChange(FocusNavigationDirection.Down);
        }

        private void MoveFocusBack_Executed(object target, ExecutedRoutedEventArgs e)
        {
            RequestFocusChange(FocusNavigationDirection.Right);
        }

        private DataTable CreateDataTable()
        {
            DataTable dataTable = new DataTable();

            DataColumn myDataColumn;

            myDataColumn = new DataColumn();
            myDataColumn.DataType = Type.GetType("System.Int32");
            myDataColumn.ColumnName = "uid";
            dataTable.Columns.Add(myDataColumn);


            myDataColumn = new DataColumn();
            myDataColumn.DataType = Type.GetType("System.Int32");
            myDataColumn.ColumnName = "#";
            dataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = Type.GetType("System.DateTime");
            myDataColumn.ColumnName = "Date";
            dataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = Type.GetType("System.Boolean");
            myDataColumn.ColumnName = "Passed";
            dataTable.Columns.Add(myDataColumn);

            myDataColumn = new DataColumn();
            myDataColumn.DataType = Type.GetType("System.Double");
            myDataColumn.ColumnName = "Total Score";
            dataTable.Columns.Add(myDataColumn);

            ////MessageBox.Show( String.Format( "CurrentUserID: {0}, CurrentTask#: {1}", ((App)App.Current).CurrentUserID, (int)((App)App.Current).CurrentTask ) );
            NpgsqlCommand command = new NpgsqlCommand( String.Format( "select uid,date,passed,totalscore FROM student_results where studentid='{0}' AND tasktype='{1}' ORDER BY uid", ((App)App.Current).CurrentStudent.ID, (int)((App)App.Current).CurrentTask ), DatabaseManager.Connection);
            DatabaseManager.OpenConnection();
            NpgsqlDataReader reader = command.ExecuteReader();

            int indx = 1;

            while ( reader.Read() )
            {
                DataRow row = dataTable.NewRow();

                row[0] = reader.GetInt32( reader.GetOrdinal("uid") );   // this row is hidden, but holds valuable information (the id# of the current result).
                row[1] = indx++;
                row[2] = reader.GetDateTime( reader.GetOrdinal("date") );
                row[3] = reader.GetBoolean( reader.GetOrdinal("passed") );
                row[4] = reader.GetDouble( reader.GetOrdinal("totalscore") );

                dataTable.Rows.Add(row);
            }

            reader.Dispose();

            DatabaseManager.CloseConnection();
            ResultsDataGrid.DataContext = dataTable.DefaultView;
                        
            return dataTable;
        }

        private void AreaSeries_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AreaSeries areaseries = sender as AreaSeries;

            if (e == null || sender == null || areaseries == null || e.AddedItems == null)
                return;

            if (areaseries != null)
            {
                if (e.AddedItems.Count > 0 && e.AddedItems[0] is KeyValuePair<int, double>)
                {
                    deserializeScreenShotFromDatabase((KeyValuePair<int, double>)(e.AddedItems[0]));
                    e.Handled = true;
                }
            }
        }

        private void ColumnSeries_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ColumnSeries colseries = sender as ColumnSeries;

            if (e == null || sender == null || colseries == null || e.AddedItems == null)
                return;

            if (colseries != null)
            {
                if (e.AddedItems.Count > 0 && e.AddedItems[0] is KeyValuePair<int, double>)
                {
                    deserializeScreenShotFromDatabase((KeyValuePair<int, double>)(e.AddedItems[0]));
                    e.Handled = true;
                }
            }
        }

        private void DataPoint_MouseEnter(object sender, MouseEventArgs e)
        {
            ToolTip tt = ToolTipService.GetToolTip(sender as DependencyObject) as ToolTip;
            tt.IsOpen = true;
        }

        private void DataPoint_MouseLeave(object sender, MouseEventArgs e)
        {
            ToolTip tt = ToolTipService.GetToolTip(sender as DependencyObject) as ToolTip;
            tt.IsOpen = false;
        }

        private void ResultsDataGrid_SelectedCellsChanged(object sender, Microsoft.Windows.Controls.SelectedCellsChangedEventArgs e)
        {
            if (ResultsDataGrid.SelectedIndex != -1)
            {
                int olddisplayindex = displayIndex;

                displayIndex = ResultsDataGrid.SelectedIndex;

                if( olddisplayindex != displayIndex )
                    deserializeStageScoreFromDatabase();    // only refresh the stage score chart IF the display index is different then what it used to be...
            }
        }

        private void OverallAreaSeries_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AreaSeries areaseries = sender as AreaSeries;

            if (e == null || sender == null || areaseries == null || e.AddedItems == null)
                return;

            //MessageBox.Show(e.AddedItems[0].ToString(), e.AddedItems[0].GetType().ToString());

            if (areaseries != null)
            {
                if (e.AddedItems.Count > 0 && e.AddedItems[0] is KeyValuePair<int, double>)
                {
                    //deserializeScreenShotFromDatabase((KeyValuePair<int, double>)(e.AddedItems[0]));
                    displayIndex = ((KeyValuePair<int, double>)(e.AddedItems[0])).Key - 1;
                    
                    try
                    {
                        ResultsDataGrid.SelectedItem = (ResultsDataGrid.Items == null ? null : ResultsDataGrid.Items[displayIndex]);
                        ResultsDataGrid.UpdateLayout();
                        ResultsDataGrid.ScrollIntoView(ResultsDataGrid.SelectedItem);
                    }
                    catch
                    {
                        ////MessageBox.Show(String.Format("EXCEPTION: Could not jump to row index {0}.", displayIndex));
                    }

                    deserializeStageScoreFromDatabase();

                    e.Handled = true;
                }
            }
        } 
    }
}
