﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Media3D;
using System.Media;
using System.ComponentModel;
using AIMS.ODU_Dev.Utilities;
using AIMS.Watchers;

namespace AIMS.Tasks.Cpr
{
    public class Cpr : Lesson
    {
        // Cpr Lesson Properties / Fields
        private CprPage cprPage;
        public CprPage CprPage { get { return this.cprPage; } }

        private SoundPlayer nextStageSound;
        public SoundPlayer NextStageSound { get { return this.nextStageSound; } set { this.nextStageSound = value; } }

        private SoundPlayer stageTimeoutSound;
        public SoundPlayer StageSound { get { return this.stageTimeoutSound; } set { this.stageTimeoutSound = value; } }

        private Point3D manikinChinLocation;
        public Point3D ManikinChinLocation { get { return this.manikinChinLocation; } set { this.manikinChinLocation = value; } }

        private Point3D manikinSideLocation;
        public Point3D ManikinSideLocation { get { return this.manikinSideLocation; } set { this.manikinSideLocation = value; } }

        private List<BaseWatcher> lessonWatchers;
        public List<BaseWatcher> LessonWatchers { get { return this.lessonWatchers; } set { this.lessonWatchers = value; } }

        private PersistentColorTracker persistentColorTracker;
        public PersistentColorTracker PersistentColorTracker { get { return persistentColorTracker; } set { persistentColorTracker = value; } }

        private CprResult cprResults;
        private int compressionImagesFinished = 0;
        private CprCompressions compressionStage;

        public CprCompressions CompressionStage { get { return compressionStage; } set { compressionStage = value; } }

        // Amount of good compressions per minute.
        public const double GOOD_CPR_RATE = 100;
        public const double GOAL_CPR_DEPTH = 0.0508; // Two inches in meters.
        public const double BAD_CPR_DEPTH_HIGH = 0.1143; // 4.5 inches in meters. //0.08255; // 3.25 inches in meters.
        public const double BAD_CPR_DEPTH_LOW = 0.04191; // 1.65 inches in meters.

        private DetectableActionCollection detectableActions;
        public DetectableActionCollection DetectableActions { get { return detectableActions; } }

        public enum StageNumber
        {
            Calibration,
            VocalInteractions,
            CheckPulse,
            Compressions
        };

        // Constructor
        public Cpr( CprPage page, TaskMode taskmode, MasteryLevel masterylevel )
        {
            this.cprPage = page;
            TaskMode = taskmode;
            MasteryLevel = masterylevel;

            Name = "Cpr";
            Description = "Cardiopulmonary Resuscitation ( CPR )";

            Stages = new List<Stage>();
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            Paused = false;
            Running = false;
            CurrentStageIndex = 0;
            ImagePath = "/Tasks/Cpr/Graphics/CPR_Calibration.png";

            TargetTime = TimeSpan.FromSeconds(300.0);

            lessonWatchers = new List<BaseWatcher>();

            nextStageSound = new System.Media.SoundPlayer(Properties.Resources.Ding);
            // Load the sound.
            nextStageSound.LoadAsync();

            stageTimeoutSound = new System.Media.SoundPlayer(Properties.Resources.Ding);
            // Load the sound.
            stageTimeoutSound.LoadAsync();

            // Need a direct accessor to compressionStage
            this.compressionStage = new CprCompressions(this, 4, 10) { ScoreWeight = 0.80, StageNumber = (int)StageNumber.Compressions };

            // Load all of the stages used in the lesson.
            Stages.Add(new CprCalibrationStage(this) { ScoreWeight = 0.00, StageNumber = (int)StageNumber.Calibration });
            Stages.Add(new CprVocalInteractions(this) { ScoreWeight = 0.05, StageNumber = (int)StageNumber.VocalInteractions });
            Stages.Add(new CprCheckPulse(this) { ScoreWeight = 0.15, StageNumber = (int)StageNumber.CheckPulse });
            Stages.Add(this.compressionStage);

            this.CompressionStage.CompressionDetected += CompressionStage_CompressionDetected;

            this.Complete = false;
            Stopwatch = new System.Diagnostics.Stopwatch();

            CreateDetectableActions();

            this.CprPage.CprCompressionUI.CompressionSets = this.CompressionStage.CompressionSets;
            this.CprPage.CprCompressionUI.CompressionPart = this.CompressionStage.ReusedCompressionPart;
        }

        /// <summary>
        /// Handle the firing of the subscribed event which indicates a compression has been detected by the CompressionStage.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void CompressionStage_CompressionDetected(object sender, CprCompressions.CompressionDetectedEventArgs e)
        {
            if (e.Compression == null || !e.Compression.Valid || e.Compression.Score < 0.5)  // Must be a valid compression to force stage swapping.
                return;

            // If the compressions have begun and the lesson is still at either check pulse or vocal interaction stage, those stages need to be ended and the lesson needs to focus only on the compression stage.
            switch (CurrentStageIndex)
            {
                case (int)StageNumber.VocalInteractions:
                    // End this stage to stop it's listening and move along.
                    this.Stages[CurrentStageIndex].EndStage(false);

                    break;
                case (int)StageNumber.CheckPulse:
                    // End this stage to stop it's listening and move along.
                    this.Stages[CurrentStageIndex].EndStage(false);

                    break;
                case (int)StageNumber.Compressions:

                    // We're already on the compression stage.
                    break;
            }
        }

        private void CreateDetectableActions()
        {
            detectableActions = new DetectableActionCollection("All CPR Actions");

            // USER:
            DetectableActionGroup userActions = new DetectableActionGroup("User");

            DetectableActionCategory user_BecomeSticky = new DetectableActionCategory(userActions, "Become Sticky");
            DetectableAction user_BecameSticky = new DetectableAction("Became Sticky");
            DetectableAction user_NoSticky = new DetectableAction("No Sticky");
            DetectableAction user_CantSticky = new DetectableAction("Can't Sticky");
            user_BecomeSticky.AddProperAction(user_BecameSticky);
            user_BecomeSticky.AddInaction(user_NoSticky);
            user_BecomeSticky.AddInaction(user_CantSticky);

            userActions.AddActionCategory(user_BecomeSticky);   // add become sticky category to group.

            detectableActions.AddGroup(userActions);

            // CHECK PULSE:
            DetectableActionGroup pulseCheck = new DetectableActionGroup("Pulse Check");

            DetectableAction pulseCheck_NoPulseCheck = new DetectableAction("No Pulse Check");    // added into each of the pulse check action categories

            DetectableActionCategory pulseCheck_HandPlacement = new DetectableActionCategory(pulseCheck, "Hand Placement");
            DetectableAction pulseCheck_ClosestSidePulseCheck = new DetectableAction("Placed Closest Side");
            DetectableAction pulseCheck_FurthestSidePulseCheck = new DetectableAction("Placed Furthest Side");
            pulseCheck_HandPlacement.AddInaction(pulseCheck_NoPulseCheck);
            pulseCheck_HandPlacement.AddProperAction(pulseCheck_ClosestSidePulseCheck);
            pulseCheck_HandPlacement.AddImproperAction(pulseCheck_FurthestSidePulseCheck);

            DetectableActionCategory pulseCheck_HandUsed = new DetectableActionCategory(pulseCheck, "Hand Used");
            DetectableAction pulseCheck_NearestHandUsed = new DetectableAction("Nearest Hand Used");
            DetectableAction pulseCheck_FurthestHandUsed = new DetectableAction("Furthest Hand Used");
            pulseCheck_HandUsed.AddInaction(pulseCheck_NoPulseCheck);
            pulseCheck_HandUsed.AddProperAction(pulseCheck_NearestHandUsed);
            pulseCheck_HandUsed.AddImproperAction(pulseCheck_FurthestHandUsed);

            DetectableActionCategory pulseCheck_Duration = new DetectableActionCategory(pulseCheck, "Duration");
            DetectableAction pulseCheck_TooShortDuration = new DetectableAction("Too Short");
            DetectableAction pulseCheck_ProperDuration = new DetectableAction("Proper");
            DetectableAction pulseCheck_TooLongDuration = new DetectableAction("Too Long");
            pulseCheck_Duration.AddInaction(pulseCheck_NoPulseCheck);
            pulseCheck_Duration.AddProperAction(pulseCheck_ProperDuration);
            pulseCheck_Duration.AddImproperAction(pulseCheck_TooShortDuration);
            pulseCheck_Duration.AddImproperAction(pulseCheck_TooLongDuration);

            pulseCheck.AddActionCategory(pulseCheck_HandPlacement); // add check pulse category to group.
            pulseCheck.AddActionCategory(pulseCheck_HandUsed); // add check pulse category to group.
            pulseCheck.AddActionCategory(pulseCheck_Duration); // add check pulse category to group.

            detectableActions.AddGroup(pulseCheck);

            // VOCAL INTERACTIONS:
            DetectableActionGroup vocalInteractions = new DetectableActionGroup("Vocal Interactions");

            DetectableActionCategory vocalInteraction_AreYouOK = new DetectableActionCategory( vocalInteractions, "Are You Okay?");
            DetectableAction vocalInteraction_AskedIfOK = new DetectableAction("Asked");
            DetectableAction vocalInteraction_NotAskedIfOK = new DetectableAction("Not Asked");
            DetectableAction vocalInteraction_LateAskedIfOK = new DetectableAction("Asked Late");
            vocalInteraction_AreYouOK.AddProperAction(vocalInteraction_AskedIfOK);
            vocalInteraction_AreYouOK.AddInaction(vocalInteraction_NotAskedIfOK);
            vocalInteraction_AreYouOK.AddImproperAction(vocalInteraction_LateAskedIfOK);

            vocalInteractions.AddActionCategory(vocalInteraction_AreYouOK); // add category to group.

            DetectableActionCategory vocalInteraction_CallFor911 = new DetectableActionCategory(vocalInteractions, "Call For 911");
            DetectableAction vocalInteraction_CalledFor911 = new DetectableAction("Called For 911");
            DetectableAction vocalInteraction_NeverCalledFor911 = new DetectableAction("Never Called For 911");
            DetectableAction vocalInteraction_LateCallFor911 = new DetectableAction("Late Call For 911");
            vocalInteraction_CallFor911.AddProperAction(vocalInteraction_CalledFor911);
            vocalInteraction_CallFor911.AddInaction(vocalInteraction_NeverCalledFor911);
            vocalInteraction_CallFor911.AddImproperAction(vocalInteraction_LateCallFor911);

            vocalInteractions.AddActionCategory(vocalInteraction_CallFor911); // add category to group.

            DetectableActionCategory vocalInteraction_CallForAED = new DetectableActionCategory(vocalInteractions, "Call For An AED");
            DetectableAction vocalInteraction_CalledForAED = new DetectableAction("Called For An AED");
            DetectableAction vocalInteraction_NeverCalledForAED = new DetectableAction("Never Called For An AED");
            DetectableAction vocalInteraction_LateCallForAED = new DetectableAction("Late Call For An AED");
            vocalInteraction_CallForAED.AddProperAction(vocalInteraction_CalledForAED);
            vocalInteraction_CallForAED.AddInaction(vocalInteraction_NeverCalledForAED);
            vocalInteraction_CallForAED.AddImproperAction(vocalInteraction_LateCallForAED);

            vocalInteractions.AddActionCategory(vocalInteraction_CallForAED); // add category to group.

            detectableActions.AddGroup(vocalInteractions);

            // COMPRESSIONS: 
            // Build a compression action group for each compression set.
            for (int i = 0; i < this.compressionStage.NUM_SETS; i++)
            {
                DetectableActionGroup compressions = new DetectableActionGroup(String.Format( "Compression Set {0}", i));

                DetectableActionCategory compressions_Overall = new DetectableActionCategory(compressions, "Overall");
                DetectableAction compressions_OverallCorrect = new DetectableAction("Correctly Performed");
                DetectableAction compressions_OverallIncorrect = new DetectableAction("Incorrectly Performed");
                DetectableAction compressions_OverallNone = new DetectableAction("Not Performed");
                compressions_Overall.AddProperAction(compressions_OverallCorrect);
                compressions_Overall.AddImproperAction(compressions_OverallIncorrect);
                compressions_Overall.AddInaction(compressions_OverallNone);

                DetectableActionCategory compressions_Depth = new DetectableActionCategory(compressions, "Compression Depth");
                DetectableAction compressions_GoodDepth = new DetectableAction("Depth Within Range");
                DetectableAction compressions_LowDepth = new DetectableAction("Depth Too Low");
                DetectableAction compressions_NoDepth = new DetectableAction("No Depth");
                compressions_Depth.AddProperAction(compressions_GoodDepth);
                compressions_Depth.AddImproperAction(compressions_LowDepth);
                compressions_Depth.AddInaction(compressions_NoDepth);

                DetectableActionCategory compressions_HandPlacement = new DetectableActionCategory(compressions, "Hand Placement");
                DetectableAction compressions_HandsOverChest = new DetectableAction("Hands Over Chest");
                DetectableAction compressions_IncorrectHandPlacement = new DetectableAction("Incorrect Hand Placement");
                compressions_HandPlacement.AddProperAction(compressions_HandsOverChest);
                compressions_HandPlacement.AddImproperAction(compressions_IncorrectHandPlacement);

                DetectableActionCategory compressions_LockedArms = new DetectableActionCategory(compressions, "Lock Arms");
                DetectableAction compressions_ArmsLocked = new DetectableAction("Arms Locked");
                DetectableAction compressions_ArmsUnlocked = new DetectableAction("Arms Unlocked");
                compressions_LockedArms.AddProperAction(compressions_ArmsLocked);
                compressions_LockedArms.AddImproperAction(compressions_ArmsUnlocked);

                DetectableActionCategory compressions_ShouldersOverChest = new DetectableActionCategory(compressions, "Shoulder Positioning");
                DetectableAction compressions_DirectlyOverChest = new DetectableAction("Directly Over Chest");
                DetectableAction compressions_NotDirectlyOverChest = new DetectableAction("Not Directly Over Chest");  // todo: too far forward, too far back
                //DetectableAction compressions_ShouldersTooFarForward = new DetectableAction("Too Far Forward");
                //DetectableAction compressions_ShouldersTooFarBack = new DetectableAction("Too Far Back");
                compressions_ShouldersOverChest.AddProperAction(compressions_DirectlyOverChest);
                compressions_ShouldersOverChest.AddImproperAction(compressions_NotDirectlyOverChest);
                //compressions_ShouldersOverChest.AddImproperAction(compressions_ShouldersTooFarForward);
                //compressions_ShouldersOverChest.AddImproperAction(compressions_ShouldersTooFarBack);

                DetectableActionCategory compressions_RepSpeed = new DetectableActionCategory(compressions, "Reps Per Minute");
                DetectableAction compressions_ProperRepSpeed = new DetectableAction("Proper Rep Rate");
                DetectableAction compressions_SlowRepSpeed = new DetectableAction("Slow Rep Rate");
                DetectableAction compressions_FastRepSpeed = new DetectableAction("Fast Rep Rate");
                compressions_RepSpeed.AddImproperAction(compressions_SlowRepSpeed);
                compressions_RepSpeed.AddImproperAction(compressions_FastRepSpeed);
                compressions_RepSpeed.AddProperAction(compressions_ProperRepSpeed);

                compressions.AddActionCategory(compressions_Overall);
                compressions.AddActionCategory(compressions_Depth);
                compressions.AddActionCategory(compressions_HandPlacement);
                compressions.AddActionCategory(compressions_LockedArms);
                compressions.AddActionCategory(compressions_ShouldersOverChest);
                compressions.AddActionCategory(compressions_RepSpeed);

                detectableActions.AddGroup(compressions);   // add compression group to collection.
            }

            if (this.CprPage != null)
            {
                this.CprPage.LessonDetectableActionsControl.SetActionCollection(detectableActions);
            }
        }

        public override void StartLesson()
        {
            if (!KinectManager.HasKinect)   // Kinect sensor does not exist, abort the lesson.
            {
                this.AbortLesson();
            }
            else
            {
                this.compressionStage.ImageFinished += Cpr_ImageFinished;

                try
                {
                    KinectManager.AllDataReady += KinectManager_AllDataReady;
                }
                catch
                {
                }
                try
                {
                    KinectManager.SensorNotReady += KinectManager_SensorNotReady;
                }
                catch
                {
                }

       //     Microsoft.Kinect.TransformSmoothParameters smoothParams = new Microsoft.Kinect.TransformSmoothParameters() { Smoothing = 0.02f, Correction = 0.5f, Prediction = 0.1f, JitterRadius = 0.1f, MaxDeviationRadius = 0.15f };

                KinectManager.NumStickySkeletons = 1;

                this.CurrentStageIndex = 0;

                if (this.cprPage!= null)
                {
                    if (this.cprPage.flow.Count == 0)
	                {
                        this.cprPage.flow.Cache = new ThumbnailManager();    // create the flow cache (enhances re-loading preformance).

                        foreach (Stage stage in this.Stages)
                            this.cprPage.flow.Add(null, stage.ImagePath);
                    }
                    this.cprPage.flow.Index = this.CurrentStageIndex;
                }
                // Check if the Kinect is using the smoothing parameters CPR would like to use.
          /*      if (KinectManager.Kinect.SkeletonStream.SmoothParameters != smoothParams)
                {
                    KinectManager.Kinect.SkeletonStream.Disable();
                    KinectManager.Kinect.SkeletonStream.Enable(smoothParams);
                } */

                this.cprResults = new CprResult();

                this.StartTime = DateTime.Now;

                this.Stopwatch.Start();

                this.UpdateLesson();
            }
        }

        private void KinectManager_AllDataReady()
        {
            this.UpdateLesson();
        }

        private void KinectManager_SensorNotReady()
        {
            if (!this.Complete)
            {
                this.AbortLesson();
            }
        }

        public override void UpdateLesson()
        {
            this.CprPage.skeletonOverlay.UpdateSkeleton();
            //this.CprPage.manikinHeadCrosshairOverlay.UpdateManikinHeadCrosshair();
            this.CprPage.zoneOverlay.UpdateZones();

            if (!this.Complete)
            {
                // Update the stage specific watchers.
                foreach (Stage stage in this.Stages)
                {
                    if (stage != null)
                    {
                        // For each stage, go through and update the state of all watchers relevant to which lesson is current.
                        // It is up to the stage itself to identify which watcher is relevant at various positionings of the current stage.
                        stage.UpdateWatcherStates(true, true, true); // ** BPC ** update to new version after Stage class changed.
                    }
                }

                // Go through and update the state of all lesson-wide watchers.
                foreach (BaseWatcher watcher in lessonWatchers)
                {
                    watcher.UpdateWatcher();
                }

                // Update CurrentStage specifically ( not just watchers, UI too etc. )
                if (this.CurrentStageIndex < this.Stages.Count)
                    this.Stages[this.CurrentStageIndex].UpdateStage();

                if (this.CurrentStageIndex > 0 && this.CurrentStageIndex < this.CompressionStage.StageNumber)
                    this.CompressionStage.UpdateStage();    // update the compression stage anytime after calibration and before the compression stage is the "current stage".
            }
        }

        /// <summary>
        /// Called by Stages belonging to this lesson when they complete. Handles CurrentStage flow based on which stage finished.
        /// </summary>
        /// <param name="stage">Stage which just ended 'this.lesson.StageComplete(this)' within ending Stage.</param>
        public void StageComplete(Stage stage, bool forcedEnd = false)
        {
            // Handles stage flow. Updates currentstageindex.
            if (this.CurrentStageIndex < this.Stages.Count)
            {
                // Current stage finished
                if (stage == this.Stages[this.CurrentStageIndex])
                {
                    // Update CurrentStage to next stage.
                    this.CurrentStageIndex++;

                    // When Calibration Stage finishes, start VocalInteraction then set CurrentStage to next stage.
                    if (stage.StageNumber == (int)StageNumber.Calibration)
                    {
                        this.Stages[this.CurrentStageIndex].UpdateStage();
                        this.CurrentStageIndex++;
                    }
                }

                // Update the flow on the CprPage
                if (this.CurrentStageIndex < this.cprPage.flow.Count)
                    this.cprPage.flow.Index = this.CurrentStageIndex;
            }

            // Used to store an image of the current scene.
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                KinectImage ki = (KinectImage)args.Argument;
                ki = KinectImage.TakeKinectSnapshot(true, true);
            };

            // Logic for when stages complete ( whether they were the current stage or not )
            double vocalInteractScore = 0;
            double checkPulseScore = 0;
            double compressionScore = 0;

            // Logic for completed stages, whether they are current or not.
            switch ((StageNumber)stage.StageNumber)
            {
                case StageNumber.Calibration:
                    try
                    {
                        worker.RunWorkerAsync(this.cprResults.CalibrationImage);
                    }
                    catch (Exception exc)
                    {
                        System.Diagnostics.Debug.Print("Cpr StageComplete Calibration ERROR : {0}", exc.Message);
                    }

                    this.CompressionStage.SetupStage(); // call to setup the stage after calibration has occured (they've found the calibration point and the user - compressions could technically begin anytime past here).

                    break;
                case StageNumber.VocalInteractions:
                    try
                    {
                        this.cprResults.AskedForAED = ((CprVocalInteractions)stage).AskedForAED;
                        this.cprResults.AskedFor911 = ((CprVocalInteractions)stage).AskedFor911;
                        this.cprResults.CheckedResponsiveness = ((CprVocalInteractions)stage).CheckedResponsiveness;

                        // set VocalInteractions Score ( used to compute total score ) and feedback
                        if (this.cprResults.AskedForAED && this.cprResults.AskedFor911 && this.cprResults.CheckedResponsiveness)
                        {
                            this.cprResults.OverallFeedback.Add(65);    // The pateint was asked if they were okay, AIMI was told to call 911, and AIMI was told to get a defibrillator. Good communication was established.
                            vocalInteractScore = 1.0;
                        }
                        else
                        {
                            if (this.cprResults.AskedForAED)
                                vocalInteractScore += (1.0 / 3)/* * 100*/;
                            else
                                this.cprResults.OverallFeedback.Add(64);    //AIMI was not told to get a defibrillator. A defibrillator may be necessary if CPR alone does not resuscitate the patient.

                            if (this.cprResults.AskedFor911)
                                vocalInteractScore += (1.0 / 3)/* * 100*/;
                            else
                                this.cprResults.OverallFeedback.Add(63);    // AIMI was never told to call 911. Trained assistance should be notified immediately.

                            if (this.cprResults.CheckedResponsiveness)
                                vocalInteractScore += (1.0 / 3)/* * 100*/;
                            else
                                this.cprResults.OverallFeedback.Add(62);    // The patient was not asked if they were okay. It is important to establish that the patient is indeed in need of assistance.
                        }
                        this.cprResults.VocalInteractionScore = vocalInteractScore;
                    }
                    catch (Exception exc)
                    {
                        System.Diagnostics.Debug.Print("Cpr StageComplete VocalInteraction ERROR: {0}", exc.Message);
                    }
                    break;
                case StageNumber.CheckPulse:
                    try
                    {
                        CprCheckPulse checkPulseStage = (CprCheckPulse)stage;

                        this.cprResults.CheckedPulse = checkPulseStage.CheckPulse;

                        worker.RunWorkerAsync(this.cprResults.CheckPulseImage);

                        checkPulseScore = 1.0;

                        switch (this.cprResults.CheckedPulse)
                        {
                            case CheckPulse.No:
                            {
                                checkPulseScore = 0.0;
                                this.cprResults.OverallFeedback.Add(66);    //The patient's pulse was not checked. CPR should only be performed if the patient's pulse is absent.
                                break;
                            }
                            case CheckPulse.Incorrectly:
                            {
                                double handUsedScore = checkPulseStage.ClosestHandUsed ? 1 : 0;
                                double handPlacementScore = checkPulseStage.HandPlacedNearestSide ? 1 : 0;
                                double durationScore = checkPulseStage.CheckPulseDuration.TotalMilliseconds < CprCheckPulse.CheckPulseMinTime ? checkPulseStage.CheckPulseDuration.TotalMilliseconds / CprCheckPulse.CheckPulseMinTime  // Too Short, Scale Back Score Based Upon Time Away From Target (min).
                                    : checkPulseStage.CheckPulseDuration.TotalMilliseconds > CprCheckPulse.CheckPulseMaxTime ? (System.Math.Min(checkPulseStage.CheckPulseDuration.TotalMilliseconds / CprCheckPulse.CheckPulseMaxTime, 1) - 1) // Too Long, Scale back score based upon time away from target (max). Clamp the score between 0 and 1.
                                    : 1;    // The check pulse duration falls properly within the min and max range.

                                checkPulseScore = handUsedScore * 0.25 + handPlacementScore * 0.25 + durationScore * 0.5;   // Assign weights to the various values, the sum of the weights must equal 1.

                                this.cprResults.OverallFeedback.Add(67);    // The patients pulse was checked, ensuring that it was absent.
                                break;
                            }
                            case CheckPulse.Correctly:
                            {
                                checkPulseScore = 1.0;

                                this.cprResults.OverallFeedback.Add(67);    // The patients pulse was checked, ensuring that it was absent.
                                break;
                            }
                        }

                        this.cprResults.CheckPulseScore = checkPulseScore;
                    }
                    catch (Exception exc)
                    {
                        System.Diagnostics.Debug.Print("Cpr StageComplete CheckPulse ERROR: {0}", exc.Message);
                    }

                    this.CompressionStage.ShowZones();  // Replace the currently drawn zones with ones from the compression stage (which has been running silently in the background).

                    break;
                case StageNumber.Compressions:
                    try
                    {
                        // Find the VocalInteractions stage and end it if it hasn't already.
                        foreach (Stage stage_loop in this.Stages)
                        {
                            if (stage_loop.StageNumber == (int)StageNumber.VocalInteractions && stage_loop.IsSetup && !stage_loop.Complete)
                                stage_loop.EndStage();
                        }

                        //List<CprCompressions.Compression>[] compressions = ((CprCompressions)stage).Compressions;
                        ObservableRangeCollection<CprCompressions.CompressionSet> compressionsets = ((CprCompressions)stage).CompressionSets;
                        if (compressionsets != null)
                        {
                            // Totals required to generate averages across all compression sets.
                            double totalDepth = 0;
                            double totalRate = 0;
                            double totalBodyMech = 0;
                            int totalNumberOfValid = 0;

                            // Iterate over compression sets.
                            for (int i = 0; i < compressionsets.Count; i++)
                            {
                                double avgDepth = compressionsets[i].AverageValidDepth;
                                double avgRate = compressionsets[i].RepsPerMinute;
                                double avgArmsLocked = compressionsets[i].AverageValidArmsLockedScore;
                                double avgShouldersOverManikin = compressionsets[i].AverageValidShoulderOverManikinScore;
                                double avgScore = compressionsets[i].AverageValidScore;
                                int numValid = compressionsets[i].ValidCompressionCount;

                                if (this.cprResults != null)
                                {
                                    CprCompressionSetResult setresult = new CprCompressionSetResult(compressionsets[i]);

                                    // Add Feedback to Compression Set.
                                    if (avgArmsLocked < 0.85)
                                        setresult.FeedbackIDs.Add(70);  // Your elbows did not remain locked while performing compressions.
                                    else
                                        setresult.FeedbackIDs.Add(71);  // Your elbows stayed straight while performing compressions.

                                    if (avgShouldersOverManikin < 0.75)
                                    {
                                        setresult.FeedbackIDs.Add(69);  // Your shoulders were not positioned directly over the patient's chest.
                                    }
                                    else
                                        setresult.FeedbackIDs.Add(68);  // Your shoulders were correctly positioned directly over the patient's chest.

                                    if (avgRate < Cpr.GOOD_CPR_RATE)
                                        setresult.FeedbackIDs.Add(76);  // Rate of compressions was less than 100 compressions per minute.
                                    else
                                        setresult.FeedbackIDs.Add(75);  // Rate of compressions was at least 100 compressions per miute.

                                    if (avgDepth < Cpr.BAD_CPR_DEPTH_LOW)
                                        setresult.FeedbackIDs.Add(74);  // The depth of compressions, on average, were less than two inches.
                                    else if (avgDepth > Cpr.BAD_CPR_DEPTH_HIGH)
                                        setresult.FeedbackIDs.Add(73);  // During compressions, your hands were lifted above the patient's chest.
                                    else
                                        setresult.FeedbackIDs.Add(72);  // Compressions on average reached a depth of at least two inches.

                                    this.cprResults.AddCompressionSet(setresult);

                                    totalDepth += avgDepth * numValid;
                                    totalRate += avgRate;
                                    totalBodyMech += avgArmsLocked + avgShouldersOverManikin;
                                    totalNumberOfValid += numValid;
                                    compressionScore += ((avgRate / GOOD_CPR_RATE) > 1) ? avgScore : (avgScore * (avgRate / GOOD_CPR_RATE));
                                }
                            }

                            this.cprResults.AverageDepth = totalDepth / totalNumberOfValid/* / compressions.Length*/;
                            this.cprResults.AverageRate = totalRate / compressionsets.Count;
                            this.cprResults.AverageBodyMech = (totalBodyMech / (compressionsets.Count * 2))/* * 100*/; // Convert from percent to value

                            compressionScore /= compressionsets.Count;
                            /*compressionScore *= 100*/; // Convert from percent to value
                        }

                        this.cprResults.Score = (0.10 * this.cprResults.VocalInteractionScore) + 
                            (0.15 * this.cprResults.CheckPulseScore) + (0.75 * compressionScore);

                        this.EndLesson();
                    }
                    catch (Exception exc)
                    {
                        System.Diagnostics.Debug.Print("Cpr StageComplete Compressions ERROR; {0}", exc.Message);
                    }
                    break;
            }
        }

        // Set images in the cprResults as they come in from the compressions stage.
        private void Cpr_ImageFinished(object source, CprCompressions.CompressionSetImageEventArgs e)
        {
            if (e.Image != null && e.ImageIndx < this.CompressionStage.CompressionSets.Count)
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    this.CompressionStage.CompressionSets[e.ImageIndx].Image = e.Image;
                }));
            }

            try
            {
                if (++this.compressionImagesFinished >= this.compressionStage.CompressionSetImages.Length)
                {
                    this.Dispatcher.Invoke((Action)(() =>
                    {
                        this.EndLesson();
                    }));
                }
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print("Cpr Cpr_ImageFinished ERROR : {0}", exc.Message);
            }
        }

        public void PlayNextStageSound()
        {
            nextStageSound.Play();
        }

        public void PlayStageTimeoutSound()
        {
            stageTimeoutSound.Play();
        }

        public override void ResetLesson()
        {
            this.WipeClean();
            this.StartLesson();
        }

        public override void EndLesson()
        {
            try
            {
                System.Diagnostics.Debug.WriteLine(String.Format("EndStage Called. this.compressionImagesFinished = {0}, this.compressionStage.CompressionSetImages.Length = {1}", this.compressionImagesFinished, this.compressionStage.CompressionSetImages.Length));

                // If not finished collecting compression stage images, exit.
                if (this.compressionImagesFinished < this.compressionStage.CompressionSetImages.Length - 1)
                    return;

                // Set the images for the Compressions Stage.
                for (int i = 0; i < this.compressionStage.CompressionSetImages.Length; i++)
                {
                    this.cprResults.CompressionSets[i].Image = this.compressionStage.CompressionSetImages[i];
                    System.Diagnostics.Debug.Print("STORING COMPRESSION IMAGE : {0}", i);
                }

                this.EndTime = DateTime.Now;

                // Update the rest of the known CprResults fields.
                this.cprResults.StartTime = this.StartTime;
                this.cprResults.EndTime = this.EndTime;
                this.cprResults.MasteryLevel = this.MasteryLevel;
                this.cprResults.TaskMode = this.TaskMode;
                this.cprResults.StudentID = ((App)App.Current).CurrentStudent.ID;

                this.Complete = true;
                this.Running = false;

                this.Stopwatch.Stop();

                List<CprCompressions.Compression>[] compressions = this.compressionStage.Compressions;

                for (int i = 0; i < compressions.Length; i++)
                {
                    if (compressions[i].Count > 0)  // if the inner compressions exist
                    {
                        for (int j = 0; j < compressions[i].Count; j++) // for each of the inner compressions
                        {
                            // output the compression details.
                            CprCompressions.Compression comp = compressions[i][j];

                            System.Diagnostics.Debug.WriteLine(String.Format("Set: {0}, Index: {1}, Score: {2}, AL: {3}, SOM: {4}, Depth: {5} (~{6}\"), PHP: {7}, Time: {8:hh:mm:ss}", i, j, comp.Score, comp.ArmsLocked, comp.ShouldersOverManikin, comp.Depth, comp.DepthInInches, comp.ProperHandPlacement, comp.Time));
                        }

                        System.Diagnostics.Debug.Print("CompressionSet: {0}, depth: {1:0.000} ~ {2:0.000}\", rate: {3}/min, score: {4}, handplacementscore: {5}, armslockedscore: {6}, chestovermanikinscore: {7}, num valid compressions: {8}",this.compressionStage.CompressionSets[i].SetNumber,this.compressionStage.CompressionSets[i].AverageValidDepth,this.compressionStage.CompressionSets[i].AverageValidDepthInches,this.compressionStage.CompressionSets[i].RepsPerMinute,this.compressionStage.CompressionSets[i].AverageValidScore,this.compressionStage.CompressionSets[i].AverageValidHandPlacementScore,this.compressionStage.CompressionSets[i].AverageValidArmsLockedScore,this.compressionStage.CompressionSets[i].AverageValidShoulderOverManikinScore,this.compressionStage.CompressionSets[i].ValidCompressionCount);
                    }
                }

                // Display results on CprPage
                if (this.CprPage != null)
                    this.CprPage.ShowResults(this.cprResults);
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print("Cpr EndLesson ERROR: {0}", exc.Message);
            }
        }

        public override void AbortLesson()
        {
            // Stop calls to update the lesson.
            KinectManager.AllDataReady -= KinectManager_AllDataReady;

            // Stop the Overlays that use the Kinect sensor.
            this.CprPage.skeletonOverlay.ShowBoneOverlays = false;
            this.CprPage.skeletonOverlay.ShowJointOverlays = false;
            this.CprPage.skeletonOverlay.ShowJointTextOverlays = false;

            // Pause and cleanout the lesson.
            this.PauseLesson();
            this.WipeClean();

            // Go back to the Main Page.  This will trigger CprPage Unloaded
            if (this.CprPage != null)
                this.CprPage.GoHome();
        }

        public override void PauseLesson()
        {
            this.Paused = true;
            this.Stopwatch.Stop();

            if (this.Stages != null && this.Stages.Count > this.CurrentStageIndex)
            {
                this.Stages[this.CurrentStageIndex].PauseStage();
            }
        }

        public override void OnTutorialVideoRequest()
        {
            this.PauseLesson();

            // Show the stage help video
            this.CprPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Cpr/Videos/Full_Cpr.wmv", UriKind.Relative));
            this.CprPage.VideoPlayer.startVideo(this, new RoutedEventArgs());

            this.CprPage.VideoPlayer.MediaElement.MediaEnded += this.TutorialVideo_MediaEnded;
            this.CprPage.VideoPlayer.MediaElement.MediaFailed += this.TutorialVideo_MediaFailed;
        }

        private void TutorialVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeLesson();

            this.CprPage.VideoPlayer.MediaElement.MediaEnded -= this.TutorialVideo_MediaEnded;
            this.CprPage.VideoPlayer.MediaElement.MediaFailed -= this.TutorialVideo_MediaFailed;
        }

        private void TutorialVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeLesson();

            this.CprPage.VideoPlayer.MediaElement.MediaEnded -= this.TutorialVideo_MediaEnded;
            this.CprPage.VideoPlayer.MediaElement.MediaFailed -= this.TutorialVideo_MediaFailed;
        }

        public override void ResumeLesson()
        {
            //throw new NotImplementedException();
            this.Paused = false;
            this.Stopwatch.Start();

            if (this.Stages != null && this.Stages.Count > this.CurrentStageIndex)
            {
                this.Stages[this.CurrentStageIndex].ResumeStage();
            }
        }

        public override void WipeClean()
        {
            compressionImagesFinished = 0;  // reset number of compressionImagesFinished.

            if (this.DetectableActions != null)
            {
                this.DetectableActions.ResetActions();  // reset the state of all detectable actions.
            }

            this.cprPage.zoneOverlay.ClearZones();
            AIMS.Assets.Lessons2.ColorTracker.Images.Clear();

            if (this.Stages != null)
            {
                foreach (Stage stage in this.Stages)
                {
                    stage.WipeClean();
                }
            }

            this.manikinChinLocation = new Point3D();
            this.manikinSideLocation = new Point3D();

            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.Paused = false;
            this.Running = false;
            this.CurrentStageIndex = 0;
            this.Complete = false;

            if (Result != null)
            {
                Result.WipeClean();
                this.Result = null;
                this.Result = new Result();
            }

            this.Stopwatch.Reset();

            KinectManager.AllDataReady -= KinectManager_AllDataReady;

            this.compressionStage.ImageFinished -= Cpr_ImageFinished;

            KinectManager.SensorNotReady -= KinectManager_SensorNotReady;

            
            if (this.CprPage != null)
            {
                this.CprPage.zoneOverlay.ClearZones();
                this.CprPage.InstructionTextBlock.Text = "";
                this.CprPage.StageInfoTextBlock.Text = "";
            }
        }
    }
}
