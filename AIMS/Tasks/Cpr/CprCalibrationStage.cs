﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Microsoft.Kinect;
using System.Windows;
using System.Windows.Media.Media3D;
using AIMS.Assets.Lessons2;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using AIMS.Tasks;

namespace AIMS.Tasks.Cpr
{
    public class CprCalibrationStage : Stage
    {
        #region Private Fields And Constants

        private Cpr lesson;
        private Point3D manikinChinLocation;
        private Point3D manikinSideLocation;
        // StickySkeleton used by Stage from KinectManager
        private Body stickySkeleton;
        // Zones to look for manikin face and side locations around left hand.
        private RecZone zoneBelowLeftHand, zoneLeftOfLeftHand, zoneRightOfLeftHand;
        // Zones to look for manikin face and side locations around right hand.
        private RecZone zoneBelowRightHand, zoneLeftOfRightHand, zoneRightOfRightHand;
        // Watches for the left hand to be above the manikinSideLocation an in-line with the manikinFaceLocation.
        private Watcher LeftHandCalibratingWatcher;
        // Watches for the right hand to be above the manikinSideLocation an in-line with the manikinFaceLocation.
        private Watcher RightHandCalibratingWatcher;

        private Watcher manikinSideDetectionWatcher;
        private Watcher manikinChinLeftWatcher;
        private Watcher manikinChinRightWatcher;

        private RecZone manikinSideZone;
        private RecZone manikinChinLeftZone;
        private RecZone manikinChinRightZone;

        private bool manikinSideFound = false;
        private bool manikinChinFound = false;

        // Amount of time necessary for the manikin's colors to be within a zone before they are recognized.
        private const double TIME_WITHIN_ZONE = 250;
        private const double TOTAL_TIME_WITHIN_ZONE = 1000;

        #endregion

        #region Public Properties

        /// <summary>
        /// Location of the Manikin's chin.
        /// </summary>
        public Point3D ManikinChinLocation { get { return this.manikinChinLocation; } }
        /// <summary>
        /// Location of the Manikin's side.
        /// </summary>
        public Point3D ManikinSideLocation { get { return this.manikinSideLocation; } }

        #endregion

        // Constructor
        public CprCalibrationStage( Cpr lesson )
        {
            this.lesson = lesson;

            this.CurrentStageWatchers = new List<BaseWatcher>();
            this.PeripheralStageWatchers = new List<BaseWatcher>();
            this.OutlyingStageWatchers = new List<BaseWatcher>();

            Name = "Calibration";
            Instruction = "Place one hand on the manikin's sternum between the nipples and the other in the air.";
            TargetTime = TimeSpan.MaxValue; // No target time for calibration...
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Tasks/Cpr/Graphics/CPR000.png";
            ScoreWeight = 0;    // Calibration stage is not weighted
            Zones = new List<Zone>(10);
            Stopwatch = new System.Diagnostics.Stopwatch();
            TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.CanTimeOut = false;
        }

        /// <summary>
        /// Initializes most of the CprCalibrationStage Fields. Rigs everything together.
        /// </summary>
        public override void SetupStage()
        {
            try
            {
                // Update CprPage for this Stage
                this.lesson.CprPage.StageInfoTextBlock.Text = this.Name;
                this.lesson.CprPage.InstructionTextBlock.Text = this.Instruction;
                this.lesson.CprPage.CurrentStageImage.Source = new BitmapImage(new Uri(this.ImagePath, UriKind.Relative));
                this.lesson.CprPage.zoneOverlay.ClearZones();

                // Set default values to fields which get modified.
                this.manikinChinLocation = new Point3D();
                this.manikinSideLocation = new Point3D();

                // Create Zones and Watchers
                this.SetupZones();
                this.CreateStageWatchers();

                // Reset Timers / Clocks / Watches
                this.StartTime = DateTime.Now;
                this.EndTime = DateTime.MaxValue;
                this.Stopwatch.Reset();
                this.Stopwatch.Start();
                this.TimeoutStopwatch.Reset();
                this.TimeoutStopwatch.Start();

                // Reset StageScore
                this.StageScore.WipeClean();
                this.StageScore = null;
                this.StageScore = new StageScore();

                // Done setting up!
                this.IsSetup = true;
                this.Complete = false;
            }
            catch (Exception exc)
            {
                Debug.Print("CprCalibrationStage SetupStage() ERROR: {0}", exc.Message);
            }
        }

        /// <summary>
        /// Sets default values for zones.
        /// </summary>
        private void SetupZones()
        {
            this.Zones.Clear();

            // Setup zones to look for manikin face and side locations around left hand.
            this.zoneBelowLeftHand = new RecZone() 
            {
                Width = 95, // pixels
                Height = 50, // pixels
                Depth = 400, // mm
                IsImproperZone = false,
                DoesTrackJoint = false,
                Active = false,
                Name = "zoneBelowLeftHand"
            };
            this.Zones.Add(this.zoneBelowLeftHand);

            this.zoneLeftOfLeftHand = new RecZone()
            {
                Width = 48, // pixels
                Height = 64, // pixels
                Depth = 400, // mm
                IsImproperZone = false,
                DoesTrackJoint = false,
                Active = false,
                Name = "zoneLeftOfLeftHand"
            };
            this.Zones.Add(this.zoneLeftOfLeftHand);

            this.zoneRightOfLeftHand = new RecZone()
            {
                Width = 48, // pixels
                Height = 64, // pixels
                Depth = 400, // mm
                IsImproperZone = false,
                DoesTrackJoint = false,
                Active = false,
                Name = "zoneRightOfLeftHand"
            };
            this.Zones.Add(this.zoneRightOfLeftHand);

            // Setup zones to look for manikin face and side locations around right hand.
            this.zoneBelowRightHand = new RecZone()
            {
                Width = 95, // pixels
                Height = 50, // pixels
                Depth = 400, // mm
                IsImproperZone = false,
                DoesTrackJoint = false,
                Active = false,
                Name = "zoneBelowRightHand"
            };
            this.Zones.Add(this.zoneBelowRightHand);

            this.zoneLeftOfRightHand = new RecZone()
            {
                Width = 48, // pixels
                Height = 64, // pixels
                Depth = 400, // mm
                IsImproperZone = false,
                DoesTrackJoint = false,
                Active = false,
                Name = "zoneLeftOfRightHand"
            };
            this.Zones.Add(this.zoneLeftOfRightHand);

            this.zoneRightOfRightHand = new RecZone()
            {
                Width = 48, // pixels
                Height = 64, // pixels
                Depth = 400, // mm
                IsImproperZone = false,
                DoesTrackJoint = false,
                Active = false,
                Name = "zoneRightOfRightHand"
            };
            this.Zones.Add(this.zoneRightOfRightHand);

            this.manikinSideZone = new RecZone()
            {
                Width = 100,
                Height = 100,
                IsImproperZone = false,
                DoesTrackJoint = false,
                Active = false,
                Name = "Manikin Side"
            };
            this.Zones.Add(this.manikinSideZone);

            this.manikinChinLeftZone = new RecZone()
            {
                Width = 60,
                Height = 60,
                IsImproperZone = false,
                DoesTrackJoint = false,
                Active = false,
                Name = "Manikin Chin Left"
            };
            this.Zones.Add(this.manikinChinLeftZone);

            this.manikinChinRightZone = new RecZone()
            {
                Width = 60,
                Height = 60,
                IsImproperZone = false,
                DoesTrackJoint = false,
                Active = false,
                Name = "Manikin Chin Right"
            };
            this.Zones.Add(this.manikinChinRightZone);


            // Add zones to the lesson page zone overlay.
            foreach (Zone zone in this.Zones)
                this.lesson.CprPage.zoneOverlay.AddZone(zone);
        }

        /// <summary>
        /// Create watcher logic for watchers within this stage.
        /// </summary>
        private void CreateStageWatchers()
        {
            // Clear the list of watchers.
            this.CurrentStageWatchers.Clear();
            this.PeripheralStageWatchers.Clear();
            this.OutlyingStageWatchers.Clear();

            #region LeftHandCalibratingWatcher

            this.LeftHandCalibratingWatcher = new Watcher();
            this.LeftHandCalibratingWatcher.CheckForActionDelegate = () => 
            {
                if (this.Complete)
                    return;

                if (this.stickySkeleton != null)
                {
                    // Must be lowest hand.
                    if (this.stickySkeleton.Joints[JointType.HandLeft].Position.Y > this.stickySkeleton.Joints[JointType.HandRight].Position.Y)
                    {
                        this.zoneBelowLeftHand.Active = false;
                        this.zoneLeftOfLeftHand.Active = false;
                        this.zoneRightOfLeftHand.Active = false;
                        return;
                    }
                    else
                    {
                        this.zoneBelowLeftHand.Active = true;
                        this.zoneLeftOfLeftHand.Active = true;
                        this.zoneRightOfLeftHand.Active = true;
                    }

                    bool manikinSideFound = false;
                    bool manikinChinFound = false;

                    DepthSpacePoint leftHandPos;
                    // Find the leftHandPos in the depth frame.
                    try
                    {
                        leftHandPos = KinectManager.CoordinateMapper.MapCameraPointToDepthSpace(this.stickySkeleton.Joints[JointType.HandLeft].Position);
                    }
                    catch (Exception exc)
                    {
                        System.Diagnostics.Debug.Print("LeftHandCalibratingWatcher ERROR: {0}", exc.Message);
                        return;
                    }

                    // Update zones to check for yellow around hand ( zones move every frame ).
                    // X and Y are in pixels, Z is in millimeters.
                    this.zoneBelowLeftHand.CenterX = leftHandPos.X;
                    this.zoneBelowLeftHand.CenterY = leftHandPos.Y + 90; // Zone below hand.
                    this.zoneBelowLeftHand.CenterZ = this.stickySkeleton.Joints[JointType.HandLeft].Position.Z - 100; // Zone in-front of hand.

                    this.zoneLeftOfLeftHand.CenterX = leftHandPos.X - 55; // Zone to the left of hand.
                    this.zoneLeftOfLeftHand.CenterY = leftHandPos.Y + 45; // Zone below hand.
                    this.zoneLeftOfLeftHand.CenterZ = this.stickySkeleton.Joints[JointType.HandLeft].Position.Z;

                    this.zoneRightOfLeftHand.CenterX = leftHandPos.X + 85; // Zone to the right of hand.
                    this.zoneRightOfLeftHand.CenterY = leftHandPos.Y + 45; // Zone below hand.
                    this.zoneRightOfLeftHand.CenterZ = this.stickySkeleton.Joints[JointType.HandLeft].Position.Z;

                    // Different pieces of tape, hopefully found, around the hand. ( expecting 2 pieces to be found, but checking both sides 
                    // of the manikin for chin ).
                    Point redBelowHand = ColorTracker.find(ColorTracker.GetColorRange("pink", Lighting.none), zoneBelowLeftHand.Rectangle, KinectManager.MappedColorData);
                    Point yellowLeftOfHand = ColorTracker.find(ColorTracker.GetColorRange("green", Lighting.none), zoneLeftOfLeftHand.Rectangle, KinectManager.MappedColorData);
                    Point yellowRightOfHand = ColorTracker.find(ColorTracker.GetColorRange("green", Lighting.none), zoneRightOfLeftHand.Rectangle, KinectManager.MappedColorData);

                    // Not triggered... unless triggered.
                    this.LeftHandCalibratingWatcher.IsTriggered = false;

                    // Check zone below hand.
                    if (!Double.IsNaN(redBelowHand.X) && !Double.IsNaN(redBelowHand.Y))
                    {
                        //int pixelIndex = 640 * (int)redBelowHand.Y + (int)redBelowHand.X;
                        int depth = KinectManager.GetDepthValue((int)redBelowHand.X, (int)redBelowHand.Y);
                        if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                        {
                            // Check if point within zone for >= 2 seconds.
                            this.zoneBelowLeftHand.CheckPointWithinZone(new Point3D(redBelowHand.X, redBelowHand.Y, depth));
                            if (this.zoneBelowLeftHand.TimeWithinZone >= TimeSpan.FromMilliseconds(TIME_WITHIN_ZONE))
                            {
                                this.manikinSideLocation = new Point3D(redBelowHand.X, redBelowHand.Y, depth);
                                manikinSideFound = true;
                            }
                        }
                    }
                    else
                        this.zoneBelowLeftHand.IsWithinZone = false;

                    // Check zone left of hand.
                    if (!Double.IsNaN(yellowLeftOfHand.X) && !Double.IsNaN(yellowLeftOfHand.Y))
                    {
                        //int pixelIndex = 640 * (int)yellowLeftOfHand.Y + (int)yellowLeftOfHand.X;
                        int depth = KinectManager.GetDepthValue((int)yellowLeftOfHand.X, (int)yellowLeftOfHand.Y);
                        if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                        {
                            // Check if point within zone for >= 2 seconds.
                            this.zoneLeftOfLeftHand.CheckPointWithinZone(new Point3D(yellowLeftOfHand.X, yellowLeftOfHand.Y, depth));
                            if (this.zoneLeftOfLeftHand.TimeWithinZone >= TimeSpan.FromMilliseconds(TIME_WITHIN_ZONE))
                            {
                                this.manikinChinLocation = new Point3D(yellowLeftOfHand.X, yellowLeftOfHand.Y, depth);
                                manikinChinFound = true;
                            }
                        }
                    }
                    else
                        this.zoneLeftOfLeftHand.IsWithinZone = false;

                    // Check zone right of hand.
                    if (!Double.IsNaN(yellowRightOfHand.X) && !Double.IsNaN(yellowRightOfHand.Y))
                    {
                        //int pixelIndex = 640 * (int)yellowRightOfHand.Y + (int)yellowRightOfHand.X;
                        int depth = KinectManager.GetDepthValue((int)yellowRightOfHand.X, (int)yellowRightOfHand.Y);
                        if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                        {
                            // Check if point within zone for >= 2 seconds.
                            this.zoneRightOfLeftHand.CheckPointWithinZone(new Point3D(yellowRightOfHand.X, yellowRightOfHand.Y, depth));
                            if (this.zoneRightOfLeftHand.TimeWithinZone >= TimeSpan.FromMilliseconds(TIME_WITHIN_ZONE))
                            {
                                this.manikinChinLocation = new Point3D(yellowRightOfHand.X, yellowRightOfHand.Y, depth);
                                manikinChinFound = true;
                            }
                        }
                    }
                    else
                        this.zoneRightOfLeftHand.IsWithinZone = false;

                    // Watcher Triggered if manikin face and side locations found.
                    if (manikinSideFound && manikinChinFound)
                    {
                        if (this.zoneBelowLeftHand.TotalTimeWithinZone >= TimeSpan.FromMilliseconds(TOTAL_TIME_WITHIN_ZONE)
                            && (this.zoneLeftOfLeftHand.TotalTimeWithinZone >= TimeSpan.FromMilliseconds(TOTAL_TIME_WITHIN_ZONE)
                            || this.zoneRightOfLeftHand.TotalTimeWithinZone >= TimeSpan.FromMilliseconds(TOTAL_TIME_WITHIN_ZONE)))
                        {
                            this.LeftHandCalibratingWatcher.IsTriggered = true;
                            System.Diagnostics.Debug.Print("LEFT HAND CALIBRATING WATCHER TRIGGERED!");
                        }
                    }
                }
            };
            this.CurrentStageWatchers.Add(this.LeftHandCalibratingWatcher);   // Add the watcher to the list of watchers

            #endregion
            
            #region RightHandCalibratingWatcher

            this.RightHandCalibratingWatcher = new Watcher();
            this.RightHandCalibratingWatcher.CheckForActionDelegate = () =>
            {
                if (this.Complete)
                    return;

                if (this.stickySkeleton != null)
                {
                    if (this.stickySkeleton.Joints[JointType.HandRight].Position.Y > this.stickySkeleton.Joints[JointType.HandLeft].Position.Y)
                    {
                        this.zoneBelowRightHand.Active = false;
                        this.zoneLeftOfRightHand.Active = false;
                        this.zoneRightOfRightHand.Active = false;
                        return;
                    }
                    else
                    {
                        this.zoneBelowRightHand.Active = true;
                        this.zoneLeftOfRightHand.Active = true;
                        this.zoneRightOfRightHand.Active = true;
                    }

                    bool manikinSideFound = false;
                    bool manikinChinFound = false;

                    DepthSpacePoint rightHandPos;
                    // Find the rightHandPos in the depth frame.
                    try
                    {
                        rightHandPos = KinectManager.Kinect.CoordinateMapper.MapCameraPointToDepthSpace(this.stickySkeleton.Joints[JointType.HandRight].Position);
                    }
                    catch (Exception exc)
                    {
                        System.Diagnostics.Debug.Print("RightHandCalibratingWatcher ERROR: {0}", exc.Message);
                        return;
                    }

                    // Update zones to check for yellow around hand ( zones move every frame ).
                    // X and Y are in pixels, Z is in millimeters.
                    this.zoneBelowRightHand.CenterX = rightHandPos.X;
                    this.zoneBelowRightHand.CenterY = rightHandPos.Y + 90; // Zone below hand
                    this.zoneBelowRightHand.CenterZ = this.stickySkeleton.Joints[JointType.HandRight].Position.Z - 100; // Zone in-front of hand.

                    this.zoneLeftOfRightHand.CenterX = rightHandPos.X - 95; // Zone to the left of hand.
                    this.zoneLeftOfRightHand.CenterY = rightHandPos.Y + 45; // Zone below hand.
                    this.zoneLeftOfRightHand.CenterZ = this.stickySkeleton.Joints[JointType.HandRight].Position.Z;

                    this.zoneRightOfRightHand.CenterX = rightHandPos.X + 75; // Zone to the right of hand.
                    this.zoneRightOfRightHand.CenterY = rightHandPos.Y + 45; // Zone below hand.
                    this.zoneRightOfRightHand.CenterZ = this.stickySkeleton.Joints[JointType.HandRight].Position.Z;

                    // Different pieces of tape, hopefully found, around the hand. ( expecting 2 pieces to be found, but checking both sides 
                    // of the manikin for chin ).
                    Point redBelowHand = ColorTracker.find(ColorTracker.GetColorRange("pink", Lighting.none), zoneBelowRightHand.Rectangle, KinectManager.MappedColorData);
                    Point yellowLeftOfHand = ColorTracker.find(ColorTracker.GetColorRange("green", Lighting.none), zoneLeftOfRightHand.Rectangle, KinectManager.MappedColorData);
                    Point yellowRightOfHand = ColorTracker.find(ColorTracker.GetColorRange("green", Lighting.none), zoneRightOfRightHand.Rectangle, KinectManager.MappedColorData);

                    // Not triggered... unless triggered.
                    this.RightHandCalibratingWatcher.IsTriggered = false;

                    // Check zone below hand.
                    if (!Double.IsNaN(redBelowHand.X) && !Double.IsNaN(redBelowHand.Y))
                    {
                        //int pixelIndex = 640 * (int)redBelowHand.Y + (int)redBelowHand.X;
                        int depth = KinectManager.GetDepthValue((int)redBelowHand.X, (int)redBelowHand.Y);
                        if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                        {
                            // @todo determine this
                            //int redBelowHandDepth = KinectManager.PixelData[pixelIndex].Depth;
                            int redBelowHandDepth = depth;

                            // Check if point within zone for >= 1 seconds.
                            this.zoneBelowRightHand.CheckPointWithinZone(new Point3D(redBelowHand.X, redBelowHand.Y, depth));
                            if (this.zoneBelowRightHand.TimeWithinZone >= TimeSpan.FromMilliseconds(TIME_WITHIN_ZONE))
                            {
                                this.manikinSideLocation = new Point3D(redBelowHand.X, redBelowHand.Y, depth);
                                manikinSideFound = true;
                            }
                        }
                    }
                    else
                        this.zoneBelowRightHand.IsWithinZone = false;

                    // Check zone left of hand.
                    if (!Double.IsNaN(yellowLeftOfHand.X) && !Double.IsNaN(yellowLeftOfHand.Y))
                    {
                        //int pixelIndex = 640 * (int)yellowLeftOfHand.Y + (int)yellowLeftOfHand.X;
                        int depth = KinectManager.GetDepthValue((int)yellowLeftOfHand.X, (int)yellowLeftOfHand.Y);
                        if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                        {
                            // Check if point within zone for >= 1 seconds.
                            this.zoneLeftOfRightHand.CheckPointWithinZone(new Point3D(yellowLeftOfHand.X, yellowLeftOfHand.Y, depth));
                            if (this.zoneLeftOfRightHand.TimeWithinZone >= TimeSpan.FromMilliseconds(TIME_WITHIN_ZONE))
                            {
                                this.manikinChinLocation = new Point3D(yellowLeftOfHand.X, yellowLeftOfHand.Y, depth);
                                manikinChinFound = true;
                            }
                        }
                    }
                    else
                        this.zoneLeftOfRightHand.IsWithinZone = false;

                    // Check zone right of hand.
                    if (!Double.IsNaN(yellowRightOfHand.X) && !Double.IsNaN(yellowRightOfHand.Y))
                    {
                        //int pixelIndex = 640 * (int)yellowRightOfHand.Y + (int)yellowRightOfHand.X;
                        int depth = KinectManager.GetDepthValue((int)yellowRightOfHand.X, (int)yellowRightOfHand.Y);
                        if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                        {
                            // Check if point within zone for >= 2 seconds.
                            this.zoneRightOfRightHand.CheckPointWithinZone(new Point3D(yellowRightOfHand.X, yellowRightOfHand.Y, depth));
                            if (this.zoneRightOfRightHand.TimeWithinZone >= TimeSpan.FromMilliseconds(TIME_WITHIN_ZONE))
                            {
                                this.manikinChinLocation = new Point3D(yellowRightOfHand.X, yellowRightOfHand.Y, depth);
                                manikinChinFound = true;
                            }
                        }
                    }
                    else
                        this.zoneRightOfRightHand.IsWithinZone = false;

                    // Watcher Triggered if manikin face and side locations found.
                    if (manikinSideFound && manikinChinFound)
                    {
                        if (this.zoneBelowRightHand.TotalTimeWithinZone >= TimeSpan.FromMilliseconds(TOTAL_TIME_WITHIN_ZONE)
                            && (this.zoneLeftOfRightHand.TotalTimeWithinZone >= TimeSpan.FromMilliseconds(TOTAL_TIME_WITHIN_ZONE)
                            || this.zoneRightOfRightHand.TotalTimeWithinZone >= TimeSpan.FromMilliseconds(TOTAL_TIME_WITHIN_ZONE)))
                        {
                            this.RightHandCalibratingWatcher.IsTriggered = true;
                            System.Diagnostics.Debug.Print("RIGHT HAND CALIBRATING WATCHER TRIGGERED!");
                        }
                    }
                }
            };
            this.CurrentStageWatchers.Add(this.RightHandCalibratingWatcher);   // Add the watcher to the list of watchers

            #endregion

            // @todo review this
            /*

            #region ManikinSideDetectionWatcher
            this.manikinSideDetectionWatcher = new Watcher();
            this.manikinSideDetectionWatcher.CheckForActionDelegate = () =>
            {
                if (this.Complete)
                    return;

                if (this.manikinSideFound)  // no need to run unless it hasn't been found.
                    return;

                this.manikinSideDetectionWatcher.IsTriggered = false;   // untriggered - will only re-trigger if required action is detected

                if (this.stickySkeleton != null)
                {
                    DepthImagePoint stickySkeletonLoc = KinectManager.Kinect.CoordinateMapper.MapSkeletonPointToDepthPoint(this.stickySkeleton.Position, KinectManager.LastImageFormat);    // get the position of the stuck skeleton.

                    // Update the location of the side detection zone
                    this.manikinSideZone.CenterX = stickySkeletonLoc.X; // Center the zone on the skeleton, who should be sitting directly at the manikin's chest.
                    this.manikinSideZone.CenterY = (stickySkeletonLoc.Y + 480) / 2; // halfway between the bottom of the screen and the skeleton's Y location.
                    this.manikinSideZone.Height = 480 - stickySkeletonLoc.Y;    // the height should adjust to be the entire distance between the skeleton and the bottom of the screen.

                    if (!this.manikinSideZone.Active)
                        this.manikinSideZone.Active = true;

                    Point found = ColorTracker.find(ColorTracker.GetColorRange("pink"), this.manikinSideZone.Rectangle, KinectManager.MappedColorData);

                    if (double.IsNaN(found.X) || double.IsNaN(found.Y))
                        return;

                    int pixelIndex = 640 * (int)found.Y + (int)found.X;

                    if (KinectManager.PixelData[pixelIndex].IsKnownDepth)    // Depth must be known for the initial point
                    {
                        this.manikinSideLocation = new Point3D(found.X, found.Y, KinectManager.PixelData[pixelIndex].Depth);
                        this.manikinSideDetectionWatcher.IsTriggered = true;

                        this.manikinSideFound = true;

                        this.manikinSideZone.IsWithinZone = true;
                    }
                    else
                    {
                        // depth not known.
                    }
                }
            };
            this.CurrentStageWatchers.Add(this.manikinSideDetectionWatcher);   // Add the watcher to the list of watchers
            #endregion // ManikinSideDetectionWatcher

            #region ManikinChinLeftWatcher
            this.manikinChinLeftWatcher = new Watcher();
            this.manikinChinLeftWatcher.CheckForActionDelegate = () =>
            {
                if (this.Complete)
                    return;

                if (this.manikinChinFound)  // no need to run unless it hasn't been found.
                    return;

                this.manikinChinLeftWatcher.IsTriggered = false;   // untriggered - will only re-trigger if required action is detected

                if (this.stickySkeleton != null)
                {
                    // Update the location of the ChinLeft detection zone
                    this.manikinChinLeftZone.CenterX = this.manikinSideLocation.X - 80; // Center the zone on the skeleton, who should be sitting directly at the manikin's chest.
                    this.manikinChinLeftZone.CenterY = this.manikinSideLocation.Y - 30; // halfway between the bottom of the screen and the skeleton's Y location.

                    if (!this.manikinChinLeftZone.Active)
                        this.manikinChinLeftZone.Active = true;

                    Point found = ColorTracker.find(ColorTracker.GetColorRange("green"), this.manikinChinLeftZone.Rectangle, KinectManager.MappedColorData);

                    if (double.IsNaN(found.X) || double.IsNaN(found.Y))
                        return;

                    int pixelIndex = 640 * (int)found.Y + (int)found.X;

                    if (KinectManager.PixelData[pixelIndex].IsKnownDepth)    // Depth must be known for the initial point
                    {
                        this.manikinChinLocation = new Point3D(found.X, found.Y, KinectManager.PixelData[pixelIndex].Depth);
                        this.manikinChinLeftWatcher.IsTriggered = true;

                        this.manikinChinFound = true;

                        this.manikinChinLeftZone.IsWithinZone = true;
                    }
                    else
                    {
                        // depth not known.
                    }
                }
            };
            this.CurrentStageWatchers.Add(this.manikinChinLeftWatcher);   // Add the watcher to the list of watchers
            #endregion // ManikinChinLeftWatcher

            #region ManikinChinRightWatcher
            this.manikinChinRightWatcher = new Watcher();
            this.manikinChinRightWatcher.CheckForActionDelegate = () =>
            {
                if (this.Complete)
                    return;

                if (this.manikinChinFound)  // no need to run unless it hasn't been found.
                    return;

                this.manikinChinRightWatcher.IsTriggered = false;   // untriggered - will only re-trigger if required action is detected

                if (this.stickySkeleton != null)
                {
                    // Update the location of the ChinRight detection zone
                    this.manikinChinRightZone.CenterX = this.manikinSideLocation.X + 80; // Center the zone on the skeleton, who should be sitting directly at the manikin's chest.
                    this.manikinChinRightZone.CenterY = this.manikinSideLocation.Y - 30; // halfway between the bottom of the screen and the skeleton's Y location.

                    if (!this.manikinChinRightZone.Active)
                        this.manikinChinRightZone.Active = true;

                    Point found = ColorTracker.find(ColorTracker.GetColorRange("green"), this.manikinChinRightZone.Rectangle, KinectManager.MappedColorData);

                    if (double.IsNaN(found.X) || double.IsNaN(found.Y))
                        return;

                    int pixelIndex = 640 * (int)found.Y + (int)found.X;

                    if (KinectManager.PixelData[pixelIndex].IsKnownDepth)    // Depth must be known for the initial point
                    {
                        this.manikinChinLocation = new Point3D(found.X, found.Y, KinectManager.PixelData[pixelIndex].Depth);
                        this.manikinChinRightWatcher.IsTriggered = true;

                        this.manikinChinFound = true;

                        this.manikinChinRightZone.IsWithinZone = true;
                    }
                    else
                    {
                        // depth not known.
                    }
                }
            };
            this.CurrentStageWatchers.Add(this.manikinChinRightWatcher);   // Add the watcher to the list of watchers
            #endregion // ManikinChinRightWatcher

            */
        }

        /// <summary>
        /// Should be called every Frame. Updates all of the stage logic
        /// </summary>
        public override void UpdateStage()
        {
            if (!IsSetup)
                SetupStage();

            if (KinectManager.DepthWidth == 0)
                return;

            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
                this.OnStageTimeout();

            // Question: Should the following two lines be moved into the NumStickySkel == 1 condition block?

            bool hadSticky = (stickySkeleton != null);  // Flag which indicates whether the we previously had a sticky, before unsetting it and re-finding.

            // Important so when sticky skeleton isn't found in frame, don't use last occurance of sticky skeleton.
            this.stickySkeleton = null;

            // Use first occurance found of a sticky skeleton in the StickySkeletonIDs array.
            if (KinectManager.NumStickySkeletons == 1)
            {
                foreach (ulong? stickySkelId in KinectManager.StickySkeletonIDs)
                {
                    if (stickySkelId.HasValue)
                    {
                        this.stickySkeleton = KinectManager.GetSkeleton(stickySkelId.Value, true);

                        // Detect the action if it's really a new skeleton.
                        if( !hadSticky && stickySkeleton != null )
                            this.lesson.DetectableActions.DetectAction("Became Sticky", "Become Sticky", "User", true);   // Detect the Action

                        break;
                    }
                }
            }

            // Turn off the zones if the StickySkeleton is null.
            if (this.stickySkeleton == null)
                foreach (Zone zone in this.Zones)
                    zone.Active = false;

            if (this.manikinSideFound && this.manikinChinFound)
            {
                this.lesson.ManikinChinLocation = this.ManikinChinLocation;
                this.lesson.ManikinSideLocation = this.manikinSideLocation;

                this.EndStage();
                return;
            }

            // Check if the stage completed.
            if (this.stickySkeleton != null && (this.LeftHandCalibratingWatcher.IsTriggered || this.RightHandCalibratingWatcher.IsTriggered))
            {
                // Update Lesson Properties.
                this.lesson.ManikinChinLocation = this.manikinChinLocation;
                this.lesson.ManikinSideLocation = this.manikinSideLocation;
       /*         Debug.Print("ManikinChinLocation: ({0:#.##},{1:#.##},{2:#.##})\nManikinSideLocation: ({3:#.##},{4:#.##},{5:#.##})",
                    this.manikinChinLocation.X, this.manikinChinLocation.Y, this.manikinChinLocation.Z,
                    this.manikinSideLocation.X, this.manikinSideLocation.Y, this.manikinSideLocation.Z); */

                // End the Stage.
                this.EndStage();
            }
        }

        // Placeholder until this function signature is removed from Stage class.
        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            this.UpdateWatcherStates();
        }

        public void UpdateWatcherStates()
        {
            // Only run if stage is setup
            if (!this.IsSetup)
                return;

            int difference = System.Math.Abs(this.lesson.CurrentStageIndex - this.StageNumber);

            // Update Current Watchers
            if (difference == 0)
            {
                foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                {
                    watcher.UpdateWatcher();
                }
            }
            // Update Peripheral Watchers
            if (difference == 1)
            {
                foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                {
                    watcher.UpdateWatcher();
                }
            }
            // Update Outlier Watchers
            if (difference > 1)
            {
                foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                {
                    watcher.UpdateWatcher();
                }
            }
        }

        public override void EndStage( bool playSound = true )
        {
            Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            this.EndTime = DateTime.Now;

            if( playSound )
                lesson.PlayNextStageSound();

            this.lesson.Running = true;

            this.Complete = true;
            this.lesson.StageComplete(this);
        }

        public override void PauseStage()
        {
            this.lesson.Paused = true;
            this.lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.lesson.Paused = false;
            this.lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void ResetStage()
        {
            this.WipeClean();
            this.SetupStage();
        }

        #region Action Functions

        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            //this.PauseStage();
            this.TimedOut = true;
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            this.RequestStageVideo();   // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
        }

        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.lesson.CprPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Cpr/Videos/CPR 000.wmv", UriKind.Relative));
            this.lesson.CprPage.VideoPlayer.startVideo(this, new RoutedEventArgs());

            this.lesson.CprPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.lesson.CprPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.lesson.CprPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.lesson.CprPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.lesson.CprPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.lesson.CprPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        #endregion

        public override void WipeClean()
        {
            manikinSideFound = false;
            manikinChinFound = false;

            manikinSideLocation = new Point3D();
            manikinChinLocation = new Point3D();

            manikinSideDetectionWatcher = null;
            manikinChinLeftWatcher = null;
            manikinChinRightWatcher = null;

            manikinSideZone = null;
            manikinChinLeftZone = null;
            manikinChinRightZone = null;

            this.IsSetup = false;
            this.Complete = false;
            this.TimedOut = false;
        }
    }
}
