﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Microsoft.Kinect;
using System.Windows;
using System.Windows.Media.Media3D;
using AIMS.Assets.Lessons2;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using AIMS.Tasks;

namespace AIMS.Tasks.Cpr
{
    public class CprCheckPulse : Stage
    {
        #region Public Const / ReadOnly Fields

        /// <summary>
        /// Minimum length of time, in milliseconds, to check the pulse.
        /// </summary>
        public const int CheckPulseMinTime = 5000;
        /// <summary>
        /// Maximum length of time, in milliseconds, to check the pulse.
        /// </summary>
        public const int CheckPulseMaxTime = 10000;

        #endregion

        #region Private Fields

        private Cpr lesson;
        // StickySkeleton used by Stage from KinectManager
        private Body stickySkeleton;
        // Zones to look for hands near the manikins neck ( checking pulse, left and right hand zones ).
        private RecZone zoneAtNeckLeftHand, zoneAtNeckRightHand, zoneOnOverlay, zoneAtNeckLeftWrist, zoneAtNeckRightWrist;
        // Watches for the left hand to be near the manikins neck checking pulse ( properly and improperly ).
        private Watcher LeftHandCheckWatcher;
        // Watches for the right hand to be near the manikins neck checking pulse ( properly and improperly ).
        private Watcher RightHandCheckWatcher;
        // Watches for the left wrist to be near the manikins neck checking pulse ( properly and improperly ).
        private Watcher LeftWristCheckWatcher;
        // Watches for the right wrist to be near the manikins neck checking pulse ( properly and improperly ).
        private Watcher RightWristCheckWatcher;

        private Watcher RightHandPlacedNearestSideWatcher;
        private Watcher LeftHandPlacedNearestSideWatcher;
        private Watcher RightHandPlacedFurthestSideWatcher;
        private Watcher LeftHandPlacedFurthestSideWatcher;

        // Stores the status of the pulse being checked. No, Incorrectly, Correctly.
        private CheckPulse checkPulse;
        private TimeSpan checkPulseDuration = TimeSpan.Zero;
        private bool hasCheckedPulse = false;
        private bool closestHandUsed = false;
        private bool handPlacedNearestSide = false;

        #endregion

        /// <summary>
        /// Stores the status of the pulse being checked. No, Incorrectly, Correctly.
        /// </summary>
        public CheckPulse CheckPulse { get { return this.checkPulse; } private set { checkPulse = value; } }
        /// <summary>
        /// Indicates the total duration which the pulse was checked for.
        /// </summary>
        public TimeSpan CheckPulseDuration { get { return checkPulseDuration; } private set { checkPulseDuration = value; } }
        /// <summary>
        /// Indicates whether or not the pulse has been checked.
        /// </summary>
        public bool HasCheckedPulse { get { return hasCheckedPulse; } private set { hasCheckedPulse = value; } }
        /// <summary>
        /// Indicates whether or not the user used their hand closest to the patient's neck to check the pulse.
        /// </summary>
        public bool ClosestHandUsed { get { return closestHandUsed; } private set { closestHandUsed = value; } }
        /// <summary>
        /// Indicates whether or not the user placed their hands on the side of the patient's neck nearest to them, rather than reaching across the patient's neck and checking the pulse on the far side.
        /// </summary>
        public bool HandPlacedNearestSide { get { return handPlacedNearestSide; } private set { handPlacedNearestSide = value; } }

        // Constructor
        public CprCheckPulse(Cpr lesson)
        {
            this.lesson = lesson;

            this.CurrentStageWatchers = new List<BaseWatcher>();
            this.PeripheralStageWatchers = new List<BaseWatcher>();
            this.OutlyingStageWatchers = new List<BaseWatcher>();

            Name = "Check Pulse";
            Instruction = "Place two fingers in to the indentation to the side of the windpipe, in line with where an Adam's apple would/should be.";
            TargetTime = TimeSpan.MaxValue;
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Tasks/Cpr/Graphics/CPR003.png";
            ScoreWeight = 1;
            Zones = new List<Zone>(10);
            Stopwatch = new System.Diagnostics.Stopwatch();
            TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.CanTimeOut = false;
        }

        /// <summary>
        /// Initializes most of the CprCalibrationStage Fields. Rigs everything together.
        /// </summary>
        public override void SetupStage()
        {
            // Update CprPage for this Stage
            this.lesson.CprPage.StageInfoTextBlock.Text = this.Name;
            this.lesson.CprPage.InstructionTextBlock.Text = this.Instruction;
            this.lesson.CprPage.CurrentStageImage.Source = new BitmapImage(new Uri(this.ImagePath, UriKind.Relative));

            // Create Zones and Watchers
            this.SetupZones();
            this.CreateStageWatchers();

            this.checkPulse = CheckPulse.No;

            // Reset Timers / Clocks / Watches
            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.Stopwatch.Reset();
            this.Stopwatch.Start();
            this.TimeoutStopwatch.Reset();
            this.TimeoutStopwatch.Start();

            // Reset StageScore
            this.StageScore.WipeClean();
            this.StageScore = null;
            this.StageScore = new StageScore();

            // Done setting up!
            this.Complete = false;
            this.IsSetup = true;
        }
        
        /// <summary>
        /// Sets default values for zones.
        /// </summary>
        private void SetupZones()
        {
            // Clear any previous zones.
            this.lesson.CprPage.zoneOverlay.ClearZones();
            this.Zones.Clear();

            // Setup zones
            this.zoneAtNeckLeftHand = new RecZone()
            {
                Width = 65, // pixels
                Height = 115, // pixels
                Depth = 350, // mm
                CenterX = this.lesson.ManikinChinLocation.X,
                CenterY = this.lesson.ManikinChinLocation.Y - 10,
                CenterZ = this.lesson.ManikinSideLocation.Z + 130,
                //CenterZ = this.lesson.ManikinChinLocation.Z,
                IsImproperZone = false,
                DoesTrackJoint = false,
                Active = true,
                Name = "zoneAtNeckLeftHand"
            };
            this.Zones.Add(this.zoneAtNeckLeftHand);

            // Setup zones
            this.zoneAtNeckLeftWrist = new RecZone()
            {
                Width = 65, // pixels
                Height = 115, // pixels
                Depth = 350, // mm
                CenterX = this.lesson.ManikinChinLocation.X,
                CenterY = this.lesson.ManikinChinLocation.Y - 10,
                CenterZ = this.lesson.ManikinSideLocation.Z + 130,
                //CenterZ = this.lesson.ManikinChinLocation.Z,
                IsImproperZone = false,
                DoesTrackJoint = false,
                Active = true,
                Name = "zoneAtNeckLeftWrist"
            };
            this.Zones.Add(this.zoneAtNeckLeftWrist);

            this.zoneAtNeckRightHand = new RecZone()
            {
                Width = 65, // pixels
                Height = 115, // pixels
                Depth = 350, // mm
                CenterX = this.lesson.ManikinChinLocation.X,
                CenterY = this.lesson.ManikinChinLocation.Y - 10,
                CenterZ = this.lesson.ManikinSideLocation.Z + 130,
                //CenterZ = this.lesson.ManikinChinLocation.Z,
                IsImproperZone = false,
                DoesTrackJoint = false,
                Active = true,
                Name = "zoneAtNeckRightHand"
            };
            this.Zones.Add(this.zoneAtNeckRightHand);

            this.zoneAtNeckRightWrist = new RecZone()
            {
                Width = 65, // pixels
                Height = 115, // pixels
                Depth = 350, // mm
                CenterX = this.lesson.ManikinChinLocation.X,
                CenterY = this.lesson.ManikinChinLocation.Y - 10,
                CenterZ = this.lesson.ManikinSideLocation.Z + 130,
                //CenterZ = this.lesson.ManikinChinLocation.Z,
                IsImproperZone = false,
                DoesTrackJoint = false,
                Active = true,
                Name = "zoneAtNeckRightWrist"
            };
            this.Zones.Add(this.zoneAtNeckRightWrist);

            this.zoneOnOverlay = new RecZone()
            {
                Width = 65, // pixels
                Height = 115, // pixels
                Depth = 350, // mm
                CenterX = this.lesson.ManikinChinLocation.X,
                CenterY = this.lesson.ManikinChinLocation.Y - 10,
                CenterZ = this.lesson.ManikinSideLocation.Z + 130,
                //CenterZ = this.lesson.ManikinChinLocation.Z,
                IsImproperZone = false,
                DoesTrackJoint = false,
                Active = true,
                Name = "zoneOnOverlay"
            };
            this.Zones.Add(this.zoneOnOverlay);

            // For this module, only add the zoneOnOverlay to the zoneOverlay.
            this.lesson.CprPage.zoneOverlay.AddZone(this.zoneOnOverlay);
        }

        /// <summary>
        /// Create watcher logic for watchers within this stage.
        /// </summary>
        private void CreateStageWatchers()
        {
            // Clear the list of watchers.
            this.CurrentStageWatchers.Clear();
            this.PeripheralStageWatchers.Clear();
            this.OutlyingStageWatchers.Clear();

            #region LeftHandCheckWatcher

            this.LeftHandCheckWatcher = new Watcher();
            this.LeftHandCheckWatcher.CheckForActionDelegate = () =>
            {
                if (this.Complete)
                    return;

                if (this.stickySkeleton != null)
                {
                    DepthSpacePoint leftHandPos;
                    // Find the leftHandPos in the depth frame.
                    try
                    {
                        leftHandPos = KinectManager.CoordinateMapper.MapCameraPointToDepthSpace(this.stickySkeleton.Joints[JointType.HandLeft].Position);
                    }
                    catch (Exception exc)
                    {
                        System.Diagnostics.Debug.Print("LeftHandCheckWatcher ERROR: {0}", exc.Message);
                        return;
                    }

                    // Not triggered... unless triggered.
                    this.LeftHandCheckWatcher.IsTriggered = false;

                    // Check if point within zone for >= 2 seconds.
                    this.zoneAtNeckLeftHand.CheckPointWithinZone(new Point3D(leftHandPos.X, leftHandPos.Y, this.stickySkeleton.Joints[JointType.HandLeft].Position.Z));

                    if (zoneAtNeckLeftHand.IsWithinZone)
                    {
                        this.LeftHandCheckWatcher.IsTriggered = true;
                    }
                    else
                    {
                        this.LeftHandCheckWatcher.IsTriggered = false;
                    }
                }
            };
            this.CurrentStageWatchers.Add(this.LeftHandCheckWatcher);   // Add the watcher to the list of watchers

            #endregion

            #region RightHandCheckWatcher

            this.RightHandCheckWatcher = new Watcher();
            this.RightHandCheckWatcher.CheckForActionDelegate = () =>
            {
                if (this.Complete)
                    return;

                if (this.stickySkeleton != null)
                {
                    DepthSpacePoint rightHandPos;
                    // Find the rightHandPos in the depth frame.
                    try
                    {
                        rightHandPos = KinectManager.CoordinateMapper.MapCameraPointToDepthSpace(this.stickySkeleton.Joints[JointType.HandRight].Position);
                    }
                    catch (Exception exc)
                    {
                        System.Diagnostics.Debug.Print("RightHandCheckWatcher ERROR: {0}", exc.Message);
                        return;
                    }

                    // Not triggered... unless triggered.
                    this.RightHandCheckWatcher.IsTriggered = false;

                    this.zoneAtNeckRightHand.CheckPointWithinZone(new Point3D(rightHandPos.X, rightHandPos.Y, this.stickySkeleton.Joints[JointType.HandRight].Position.Z));

                    if (zoneAtNeckRightHand.IsWithinZone)
                    {
                        this.RightHandCheckWatcher.IsTriggered = true;
                    }
                    else
                    {
                        this.RightHandCheckWatcher.IsTriggered = false;
                    }
                }
            };
            this.CurrentStageWatchers.Add(this.RightHandCheckWatcher);   // Add the watcher to the list of watchers

            #endregion

            #region LeftWristCheckWatcher

            this.LeftWristCheckWatcher = new Watcher();
            this.LeftWristCheckWatcher.CheckForActionDelegate = () =>
            {
                if (this.Complete)
                    return;

                if (this.stickySkeleton != null)
                {
                    DepthSpacePoint leftWristPos;
                    // Find the leftWristPos in the depth frame.
                    try
                    {
                        leftWristPos = KinectManager.CoordinateMapper.MapCameraPointToDepthSpace(this.stickySkeleton.Joints[JointType.WristLeft].Position);
                    }
                    catch (Exception exc)
                    {
                        System.Diagnostics.Debug.Print("LeftWristCheckWatcher ERROR: {0}", exc.Message);
                        return;
                    }

                    // Not triggered... unless triggered.
                    this.LeftWristCheckWatcher.IsTriggered = false;

                    // Check if point within zone for >= 2 seconds.
                    this.zoneAtNeckLeftWrist.CheckPointWithinZone(new Point3D(leftWristPos.X, leftWristPos.Y, this.stickySkeleton.Joints[JointType.WristLeft].Position.Z));

                    if (zoneAtNeckLeftWrist.IsWithinZone)
                    {
                        this.LeftWristCheckWatcher.IsTriggered = true;
                    }
                    else
                    {
                        this.LeftWristCheckWatcher.IsTriggered = false;
                    }
                }
            };
            this.CurrentStageWatchers.Add(this.LeftWristCheckWatcher);   // Add the watcher to the list of watchers

            #endregion

            #region RightWristCheckWatcher

            this.RightWristCheckWatcher = new Watcher();
            this.RightWristCheckWatcher.CheckForActionDelegate = () =>
            {
                if (this.Complete)
                    return;

                if (this.stickySkeleton != null)
                {
                    DepthSpacePoint rightWristPos;
                    // Find the rightWristPos in the depth frame.
                    try
                    {
                        rightWristPos = KinectManager.CoordinateMapper.MapCameraPointToDepthSpace(this.stickySkeleton.Joints[JointType.WristRight].Position);
                    }
                    catch (Exception exc)
                    {
                        System.Diagnostics.Debug.Print("RightWristCheckWatcher ERROR: {0}", exc.Message);
                        return;
                    }

                    // Not triggered... unless triggered.
                    this.RightWristCheckWatcher.IsTriggered = false;

                    // Check if point within zone for >= 2 seconds.
                    this.zoneAtNeckRightWrist.CheckPointWithinZone(new Point3D(rightWristPos.X, rightWristPos.Y, this.stickySkeleton.Joints[JointType.WristRight].Position.Z));

                    if (zoneAtNeckRightWrist.IsWithinZone)
                    {
                        this.RightWristCheckWatcher.IsTriggered = true;
                    }
                    else
                    {
                        this.RightWristCheckWatcher.IsTriggered = false;
                    }
                }
            };
            this.CurrentStageWatchers.Add(this.RightWristCheckWatcher);   // Add the watcher to the list of watchers

            #endregion

            // @todo removed for compiling
            /*

            #region RightHandPlacedNearestSideWatcher
            this.RightHandPlacedNearestSideWatcher = new Watcher();
            this.RightHandPlacedNearestSideWatcher.CheckForActionDelegate = () =>
            {
                if (this.Complete)
                    return;

                // Not triggered... unless triggered.
                this.RightHandPlacedNearestSideWatcher.IsTriggered = false;

                if (this.stickySkeleton != null)
                {
                    DepthSpacePoint rightWristPos;
                    DepthSpacePoint rightHandPos;
                    // Find the rightWristPos in the depth frame.
                    try
                    {
                        rightWristPos = KinectManager.Kinect.CoordinateMapper.MapSkeletonPointToDepthPoint(this.stickySkeleton.Joints[JointType.WristRight].Position,
                            KinectManager.LastImageFormat);
                        rightHandPos = KinectManager.Kinect.CoordinateMapper.MapSkeletonPointToDepthPoint(this.stickySkeleton.Joints[JointType.HandRight].Position,
                            KinectManager.LastImageFormat);
                    }
                    catch (Exception exc)
                    {
                        System.Diagnostics.Debug.Print("RightHandPlacedNearestSideWatcher ERROR: {0}", exc.Message);
                        return;
                    }

                    // Check if the right hand's depth is behind the depth of the manikin's neck.
                    if ( ( this.RightHandCheckWatcher.IsTriggered && this.lesson.ManikinChinLocation.Z < rightHandPos.Depth ) || ( this.RightWristCheckWatcher.IsTriggered && this.lesson.ManikinChinLocation.Z < rightWristPos.Depth ) )
                    {
                        this.RightHandPlacedNearestSideWatcher.IsTriggered = true;
                    }
                }
            };
            this.CurrentStageWatchers.Add(this.RightHandPlacedNearestSideWatcher);   // Add the watcher to the list of watchers
            #endregion

            #region RightHandPlacedFurthestSideWatcher
            this.RightHandPlacedFurthestSideWatcher = new Watcher();
            this.RightHandPlacedFurthestSideWatcher.CheckForActionDelegate = () =>
            {
                if (this.Complete)
                    return;

                // Not triggered... unless triggered.
                this.RightHandPlacedFurthestSideWatcher.IsTriggered = false;

                if (this.stickySkeleton != null)
                {
                    DepthImagePoint rightWristPos;
                    DepthImagePoint rightHandPos;
                    // Find the rightWristPos in the depth frame.
                    try
                    {
                        rightWristPos = KinectManager.Kinect.CoordinateMapper.MapSkeletonPointToDepthPoint(this.stickySkeleton.Joints[JointType.WristRight].Position,
                            KinectManager.LastImageFormat);
                        rightHandPos = KinectManager.Kinect.CoordinateMapper.MapSkeletonPointToDepthPoint(this.stickySkeleton.Joints[JointType.HandRight].Position,
                            KinectManager.LastImageFormat);
                    }
                    catch (Exception exc)
                    {
                        System.Diagnostics.Debug.Print("RightHandPlacedFurthestSideWatcher ERROR: {0}", exc.Message);
                        return;
                    }

                    // Check if the right hand's depth is behind the depth of the manikin's neck.
                    if ( ( this.RightHandCheckWatcher.IsTriggered && this.lesson.ManikinChinLocation.Z > rightHandPos.Depth ) || ( this.RightWristCheckWatcher.IsTriggered && this.lesson.ManikinChinLocation.Z > rightWristPos.Depth ) )
                    {
                        this.RightHandPlacedFurthestSideWatcher.IsTriggered = true;
                    }
                }
            };
            this.CurrentStageWatchers.Add(this.RightHandPlacedFurthestSideWatcher);   // Add the watcher to the list of watchers
            #endregion

            #region LeftHandPlacedNearestSideWatcher
            this.LeftHandPlacedNearestSideWatcher = new Watcher();
            this.LeftHandPlacedNearestSideWatcher.CheckForActionDelegate = () =>
            {
                if (this.Complete)
                    return;

                // Not triggered... unless triggered.
                this.LeftHandPlacedNearestSideWatcher.IsTriggered = false;

                if (this.stickySkeleton != null)
                {
                    DepthImagePoint leftWristPos;
                    DepthImagePoint leftHandPos;
                    // Find the leftWristPos in the depth frame.
                    try
                    {
                        leftWristPos = KinectManager.Kinect.CoordinateMapper.MapSkeletonPointToDepthPoint(this.stickySkeleton.Joints[JointType.WristLeft].Position,
                            KinectManager.LastImageFormat);
                        leftHandPos = KinectManager.Kinect.CoordinateMapper.MapSkeletonPointToDepthPoint(this.stickySkeleton.Joints[JointType.HandLeft].Position,
                            KinectManager.LastImageFormat);
                    }
                    catch (Exception exc)
                    {
                        System.Diagnostics.Debug.Print("LeftHandPlacedNearestSideWatcher ERROR: {0}", exc.Message);
                        return;
                    }

                    // Check if the left hand's depth is behind the depth of the manikin's neck.
                    if ((this.LeftHandCheckWatcher.IsTriggered && this.lesson.ManikinChinLocation.Z < leftHandPos.Depth) || (this.LeftWristCheckWatcher.IsTriggered && this.lesson.ManikinChinLocation.Z < leftWristPos.Depth))
                    {
                        this.LeftHandPlacedNearestSideWatcher.IsTriggered = true;
                    }
                }
            };
            this.CurrentStageWatchers.Add(this.LeftHandPlacedNearestSideWatcher);   // Add the watcher to the list of watchers
            #endregion

            #region LeftHandPlacedFurthestSideWatcher
            this.LeftHandPlacedFurthestSideWatcher = new Watcher();
            this.LeftHandPlacedFurthestSideWatcher.CheckForActionDelegate = () =>
            {
                if (this.Complete)
                    return;

                // Not triggered... unless triggered.
                this.LeftHandPlacedFurthestSideWatcher.IsTriggered = false;

                if (this.stickySkeleton != null)
                {
                    DepthImagePoint leftWristPos;
                    DepthImagePoint leftHandPos;
                    // Find the leftWristPos in the depth frame.
                    try
                    {
                        leftWristPos = KinectManager.Kinect.CoordinateMapper.MapSkeletonPointToDepthPoint(this.stickySkeleton.Joints[JointType.WristLeft].Position,
                            KinectManager.LastImageFormat);
                        leftHandPos = KinectManager.Kinect.CoordinateMapper.MapSkeletonPointToDepthPoint(this.stickySkeleton.Joints[JointType.HandLeft].Position,
                            KinectManager.LastImageFormat);
                    }
                    catch (Exception exc)
                    {
                        System.Diagnostics.Debug.Print("LeftHandPlacedFurthestSideWatcher ERROR: {0}", exc.Message);
                        return;
                    }

                    // Check if the left hand's depth is behind the depth of the manikin's neck.
                    if ((this.LeftHandCheckWatcher.IsTriggered && this.lesson.ManikinChinLocation.Z > leftHandPos.Depth) || (this.LeftWristCheckWatcher.IsTriggered && this.lesson.ManikinChinLocation.Z > leftWristPos.Depth))
                    {
                        this.LeftHandPlacedFurthestSideWatcher.IsTriggered = true;
                    }
                }
            };
            this.CurrentStageWatchers.Add(this.LeftHandPlacedFurthestSideWatcher);   // Add the watcher to the list of watchers
            #endregion

            */
        }

        /// <summary>
        /// Returns whether or not the manikin's head is to the left of the manikin's side.
        /// </summary>
        /// <returns>True if the Head is Left of the Side.</returns>
        private bool IsManikinHeadOnLeftSide()
        {
            if (this.lesson.ManikinSideLocation.X > this.lesson.ManikinChinLocation.X)    // Head---Side---|
                return true;

            return false;   // |---Side---Head
        }

        /// <summary>
        /// Should be called every Frame. Updates all of the stage logic
        /// </summary>
        public override void UpdateStage()
        {
            if (!this.IsSetup)
                this.SetupStage();

            if (KinectManager.DepthWidth == 0)
                return;

            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
                this.OnStageTimeout();

            // Update the zoneOnOverlay based on either of the LeftHand or RightHand zones
            this.zoneOnOverlay.IsWithinZone = (this.zoneAtNeckLeftHand.IsWithinZone || this.zoneAtNeckRightHand.IsWithinZone || this.zoneAtNeckLeftWrist.IsWithinZone || this.zoneAtNeckRightWrist.IsWithinZone);

            // Important so when sticky skeleton isn't found in frame, don't use last occurance of sticky skeleton.
            this.stickySkeleton = null;

            // Use first occurance found of a sticky skeleton in the StickySkeletonIDs array.
            if (KinectManager.NumStickySkeletons == 1)
            {
                foreach (ulong? stickySkelId in KinectManager.StickySkeletonIDs)
                {
                    if (stickySkelId.HasValue)
                    {
                        this.stickySkeleton = KinectManager.GetSkeleton(stickySkelId.Value, true);
                        break;
                    }
                }
            }

            this.CheckPulseDuration = this.zoneOnOverlay.TotalTimeWithinZone;

            // Check if the stage completed.
            if (this.stickySkeleton != null )
            {
                if( this.zoneOnOverlay.IsWithinZone )
                {
                    this.checkPulse = CheckPulse.Correctly; // default to Correctly, unless overridden in the following conditions:

                    if (this.LeftHandCheckWatcher.IsTriggered || this.LeftWristCheckWatcher.IsTriggered)
                    {
                        if (IsManikinHeadOnLeftSide())
                        {
                            if (this.zoneAtNeckRightHand.IsWithinZone || this.zoneAtNeckRightWrist.IsWithinZone)    // Right Zone
                            {
                                this.ClosestHandUsed = false;   // Left hand used on right zone (Furthest)
                                this.lesson.DetectableActions.DetectAction("Furthest Hand Used", "Hand Used", "Pulse Check", true); // improper
                                this.CheckPulse = CheckPulse.Incorrectly;
                            }
                            else if (this.zoneAtNeckLeftHand.IsWithinZone || this.zoneAtNeckLeftWrist.IsWithinZone)  // Left Zone
                            {
                                this.ClosestHandUsed = true;    // Left hand used on left zone (Closest)
                                this.lesson.DetectableActions.DetectAction("Nearest Hand Used", "Hand Used", "Pulse Check", true);    // proper
                            }
                        }
                        else
                        {
                            if (this.zoneAtNeckRightHand.IsWithinZone || this.zoneAtNeckRightWrist.IsWithinZone)    // Right Zone
                            {
                                this.ClosestHandUsed = true;    // Right hand used on left zone (Closest)
                                this.lesson.DetectableActions.DetectAction("Nearest Hand Used", "Hand Used", "Pulse Check", true);    // proper
                            }
                            else if (this.zoneAtNeckLeftHand.IsWithinZone || this.zoneAtNeckLeftWrist.IsWithinZone)  // Left Zone
                            {
                                this.ClosestHandUsed = false;   // Right hand used on left zone (Furthest)
                                this.lesson.DetectableActions.DetectAction("Furthest Hand Used", "Hand Used", "Pulse Check", true); // improper
                                this.CheckPulse = CheckPulse.Incorrectly;
                            }
                        }

                        // Determine whether they're infront of the chin.
                        if (this.LeftHandPlacedNearestSideWatcher.IsTriggered) // Nearest
                        {
                            this.HandPlacedNearestSide = true;
                            this.lesson.DetectableActions.DetectAction("Placed Closest Side", "Hand Placement", "Pulse Check", true);    // proper
                        }
                        else if( this.LeftHandPlacedFurthestSideWatcher.IsTriggered )  // Reaching Across
                        {
                            this.HandPlacedNearestSide = false;
                            this.lesson.DetectableActions.DetectAction("Placed Furthest Side", "Hand Placement", "Pulse Check", true); // improper
                            this.CheckPulse = CheckPulse.Incorrectly;
                        }
                    }
                    else if (this.RightHandCheckWatcher.IsTriggered || this.RightWristCheckWatcher.IsTriggered)    // Right Hand Used.
                    {
                        if (IsManikinHeadOnLeftSide())
                        {
                            if (this.zoneAtNeckRightHand.IsWithinZone || this.zoneAtNeckRightWrist.IsWithinZone)    // Right Zone
                            {
                                this.ClosestHandUsed = false;   // Right hand used on right zone (Furthest)
                                this.lesson.DetectableActions.DetectAction("Furthest Hand Used", "Hand Used", "Pulse Check", true); // improper
                                this.CheckPulse = CheckPulse.Incorrectly;

                            }
                            else if (this.zoneAtNeckLeftHand.IsWithinZone || this.zoneAtNeckLeftWrist.IsWithinZone)  // Left Zone
                            {
                                this.ClosestHandUsed = true;    // Right hand used on left zone (Closest)
                                this.lesson.DetectableActions.DetectAction("Nearest Hand Used", "Hand Used", "Pulse Check", true);    // proper
                            }
                        }
                        else
                        {
                            if (this.zoneAtNeckRightHand.IsWithinZone || this.zoneAtNeckRightWrist.IsWithinZone)    // Right Zone
                            {
                                this.ClosestHandUsed = true;    // Right hand used on right zone (Closest)
                                this.lesson.DetectableActions.DetectAction("Nearest Hand Used", "Hand Used", "Pulse Check", true);    // proper
                            }
                            else if (this.zoneAtNeckLeftHand.IsWithinZone || this.zoneAtNeckLeftWrist.IsWithinZone)  // Left Zone
                            {
                                this.ClosestHandUsed = false;   // Left hand used on right zone (Furthest)
                                this.lesson.DetectableActions.DetectAction("Furthest Hand Used", "Hand Used", "Pulse Check", true); // improper
                                this.CheckPulse = CheckPulse.Incorrectly;
                            }
                        }

                        // Determine whether they're infront of the chin.
                        if (this.RightHandPlacedNearestSideWatcher.IsTriggered) // Nearest
                        {
                            this.HandPlacedNearestSide = true;
                            this.lesson.DetectableActions.DetectAction("Placed Closest Side", "Hand Placement", "Pulse Check", true);    // proper
                        }
                        else if (this.RightHandPlacedFurthestSideWatcher.IsTriggered)  // Reaching Across
                        {
                            this.HandPlacedNearestSide = false;
                            this.lesson.DetectableActions.DetectAction("Placed Furthest Side", "Hand Placement", "Pulse Check", true); // improper
                            this.CheckPulse = CheckPulse.Incorrectly;
                        }
                    }
                }
            }

            if (this.CheckPulseDuration >= TimeSpan.FromMilliseconds(CprCheckPulse.CheckPulseMinTime))
                this.EndStage();
        }

        // Placeholder until this function signature is removed from Stage class.
        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            this.UpdateWatcherStates();
        }

        public void UpdateWatcherStates()
        {
            // Only run if stage is setup
            if (!this.IsSetup)
                return;

            int difference = System.Math.Abs(this.lesson.CurrentStageIndex - this.StageNumber);

            // Update Current Watchers
            if (difference == 0)
            {
                foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                {
                    watcher.UpdateWatcher();
                }
            }
            // Update Peripheral Watchers
            if (difference == 1)
            {
                foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                {
                    watcher.UpdateWatcher();
                }
            }
            // Update Outlier Watchers
            if (difference > 1)
            {
                foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                {
                    watcher.UpdateWatcher();
                }
            }
        }

        public override void EndStage( bool playSound = true )
        {
            this.CheckPulseDuration = this.zoneOnOverlay.TotalTimeWithinZone;

            if (this.CheckPulseDuration <= TimeSpan.FromSeconds(0.1))
            {
                this.HasCheckedPulse = false;
                this.CheckPulse = Tasks.Cpr.CheckPulse.No;
                this.lesson.DetectableActions.DetectAction("No Pulse Check", "Duration", "Pulse Check", true);  // inaction
            }
            else if (this.CheckPulseDuration < TimeSpan.FromMilliseconds(CprCheckPulse.CheckPulseMinTime))
            {
                this.HasCheckedPulse = true;
                this.lesson.DetectableActions.DetectAction("Too Short", "Duration", "Pulse Check", true);  // improper
            }
            else if (this.CheckPulseDuration > TimeSpan.FromMilliseconds(CprCheckPulse.CheckPulseMaxTime))
            {
                this.HasCheckedPulse = true;
                this.lesson.DetectableActions.DetectAction("Too Long", "Duration", "Pulse Check", true);  // improper
            }
            else
            {
                this.HasCheckedPulse = true;
                this.lesson.DetectableActions.DetectAction("Proper", "Duration", "Pulse Check", true);  // proper
            }

            System.Diagnostics.Debug.WriteLine(String.Format("Pulse Checked: {0} ({1})", this.HasCheckedPulse, this.CheckPulseDuration));
            System.Diagnostics.Debug.WriteLine(String.Format("Check Type: {0}", this.CheckPulse));

            Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            this.EndTime = DateTime.Now;

            if( playSound )
                lesson.PlayNextStageSound();

            this.lesson.Running = true;

            this.Complete = true;
            this.lesson.StageComplete(this);
        }

        public override void PauseStage()
        {
            this.lesson.Paused = true;
            this.lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.lesson.Paused = false;
            this.lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void ResetStage()
        {
            this.WipeClean();
            this.SetupStage();
        }

        #region Action Functions
        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            //this.PauseStage();
            this.TimedOut = true;
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            this.RequestStageVideo();   // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
        }

        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.lesson.CprPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Cpr/Videos/CPR 002.wmv", UriKind.Relative));
            this.lesson.CprPage.VideoPlayer.startVideo(this, new RoutedEventArgs());

            this.lesson.CprPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.lesson.CprPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.lesson.CprPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.lesson.CprPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.lesson.CprPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.lesson.CprPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }
        #endregion

        public override void WipeClean()
        {
            this.stickySkeleton = null;

            zoneAtNeckLeftHand = null;
            zoneAtNeckRightHand = null;
            zoneOnOverlay = null;
            zoneAtNeckLeftWrist = null;
            zoneAtNeckRightWrist = null;
            LeftHandCheckWatcher = null;
            RightHandCheckWatcher = null;
            LeftWristCheckWatcher = null;
            RightWristCheckWatcher = null;

            RightHandPlacedNearestSideWatcher = null;
            LeftHandPlacedNearestSideWatcher = null;
            RightHandPlacedFurthestSideWatcher = null;
            LeftHandPlacedFurthestSideWatcher = null;

            checkPulse = Tasks.Cpr.CheckPulse.No;
            checkPulseDuration = TimeSpan.Zero;
            hasCheckedPulse = false;
            closestHandUsed = false;
            handPlacedNearestSide = false;

            this.IsSetup = false;
            this.Complete = false;
            this.TimedOut = false;
        }
    }
}
