﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using System.Windows.Media.Imaging;
using Microsoft.Kinect;
using System.Windows;
using System.ComponentModel;

namespace AIMS.Tasks.Cpr
{
    public class CprCheckResponsiveness : Stage
    {
        private Cpr lesson;
        public Cpr Lesson { get { return lesson; } set { lesson = value; } }

        private bool wasHeadTiltedBack = false;
        private bool refoundHead = false;
        private Point3D newHeadLocation = new Point3D();
        private double initialChinDepth = 0;
        private int yellowCounter = 0;
        private int yellowDepthCounter = 0;

        private bool rightHandOrWristByManikinsChin = false;
        private bool leftHandOrWristByManikinsChin = false;

        public bool RightHandOrWristByManikinsChin { get { return rightHandOrWristByManikinsChin; } set { rightHandOrWristByManikinsChin = value; } }
        public bool LeftHandOrWristByManikinsChin { get { return leftHandOrWristByManikinsChin; } set { leftHandOrWristByManikinsChin = value; } }

        public bool WasHeadTiltedBack { get { return wasHeadTiltedBack; } set { wasHeadTiltedBack = value; } }
        public bool RefoundHead { get { return refoundHead; } set { refoundHead = value; } }
        public Point3D NewHeadLocation { get { return newHeadLocation; } set { newHeadLocation = value; } }
        public double InitialChinDepth { get { return initialChinDepth; } set { initialChinDepth = value; } }
        public int YellowCounter { get { return yellowCounter; } set { yellowCounter = value; } }
        public int YellowDepthCounter { get { return yellowDepthCounter; } set { yellowDepthCounter = value; } }

        private Point3D watcher_FoundManikinHeadPos = new Point3D();
        private int watcher_YellowCounter = 0;
        private int watcher_YellowDepthCounter = 0;
        private bool watcher_FoundChin = false;

        private bool manikinHeadMoved = false;

        public CprCheckResponsiveness( Cpr cpr )
        {
            Lesson = cpr;

            StageNumber = 1;
            Name = "Tilt Head";
            Instruction = "Use both hands to tilt the manikin's head back slightly.";
            TargetTime = TimeSpan.FromSeconds(3.5);
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Assets/Graphics/Cpr/Stage1.png";
            Active = false;
            Complete = false;
            ScoreWeight = 0.20;
            Zones = new List<Zone>();
            IsSetup = false;
            Stopwatch = new System.Diagnostics.Stopwatch();
            this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.CanTimeOut = false;
            this.TimeOutTime = TimeSpan.FromSeconds(10.0);
        }

        public override void SetupStage()
        {
            this.Lesson.CprPage.StageInfoTextBlock.Text = this.Name;
            this.Lesson.CprPage.InstructionTextBlock.Text = this.Instruction;

            // Set the image for the current stage.
            this.Lesson.CprPage.CurrentStageImage.Source = new BitmapImage(new Uri("/Assets/Graphics/Cpr/Cpr_Stage001_640.jpg", UriKind.Relative));

            this.Zones.Clear(); // Clear the stage zone list.

            // Add the right and left zones (where the hands should be).

            // stageZones[0]
            #region Right Wrist Zone
            RecZone rightzonewrist = new RecZone();
            rightzonewrist.X = this.Lesson.ManikinChinLocation.X + 28.6 - (5.5 * 3);
            rightzonewrist.Y = this.Lesson.ManikinChinLocation.Y - 21 - (9.8 * 3);
            rightzonewrist.Z = 0;
            rightzonewrist.Name = "R Wrist";
            rightzonewrist.Width = 5.5 * 6;
            rightzonewrist.Height = 9.8 * 6;
            rightzonewrist.Depth = 0;
            rightzonewrist.AcceptedJointType = JointType.WristRight;
            Zones.Add(rightzonewrist);
            #endregion
            // stageZones[1]
            #region Left Wrist Zone
            RecZone leftzonewrist = new RecZone();
            leftzonewrist.X = this.Lesson.ManikinChinLocation.X - 26.4 - (6.5 * 3);
            leftzonewrist.Y = this.Lesson.ManikinChinLocation.Y - 15 - (9.9 * 3);
            leftzonewrist.Z = 0;
            leftzonewrist.Name = "L Wrist";
            //leftzonewrist.ZoneType = ZoneType.Position;
            leftzonewrist.Width = 6.5 * 6;
            leftzonewrist.Height = 9.9 * 6;
            leftzonewrist.Depth = 0;
            leftzonewrist.AcceptedJointType = JointType.WristLeft;
            Zones.Add(leftzonewrist);
            #endregion

            // stageZones[2]
            #region Right Hand Zone
            RecZone rightzonehand = new RecZone();
            rightzonehand.X = this.Lesson.ManikinChinLocation.X + 28.6 - (5.5 * 3);
            rightzonehand.Y = this.Lesson.ManikinChinLocation.Y - 21 - (9.8 * 3);
            rightzonehand.Z = 0;
            rightzonehand.Name = "R Hand";
            //rightzonehand.ZoneType = ZoneType.Position;
            rightzonehand.Width = 5.5 * 6;
            rightzonehand.Height = 9.8 * 6;
            rightzonehand.Depth = 0;
            rightzonehand.AcceptedJointType = JointType.HandRight;
            Zones.Add(rightzonehand);
            #endregion
            // stageZones[3]
            #region Left Hand Zone
            RecZone leftzonehand = new RecZone();
            leftzonehand.X = this.Lesson.ManikinChinLocation.X - 26.4 - (6.5 * 3);
            leftzonehand.Y = this.Lesson.ManikinChinLocation.Y - 15 - (9.9 * 3);
            leftzonehand.Z = 0;
            leftzonehand.Name = "L Hand";
            //leftzonehand.ZoneType = ZoneType.Position;
            leftzonehand.Width = 6.5 * 6;
            leftzonehand.Height = 9.9 * 6;
            leftzonehand.Depth = 0;
            leftzonehand.AcceptedJointType = JointType.HandLeft;
            Zones.Add(leftzonehand);
            #endregion

            #region Right Hand Zone
            RecZone outsiderightzonehand = new RecZone();
            rightzonehand.X = this.Lesson.ManikinChinLocation.X + 57.2 - (5.5 * 3);
            rightzonehand.Y = this.Lesson.ManikinChinLocation.Y - 21 - (9.8 * 3);
            rightzonehand.Z = 0;
            rightzonehand.Name = "R Hand";
            //rightzonehand.ZoneType = ZoneType.Position;
            rightzonehand.Width = 5.5 * 6;
            rightzonehand.Height = 9.8 * 6;
            rightzonehand.Depth = 0;
            rightzonehand.AcceptedJointType = JointType.HandRight;
            Zones.Add(rightzonehand);
            #endregion
            // stageZones[3]
            #region Left Hand Zone
            RecZone outsideleftzonehand = new RecZone();
            leftzonehand.X = this.Lesson.ManikinChinLocation.X - 52.4 - (6.5 * 3);
            leftzonehand.Y = this.Lesson.ManikinChinLocation.Y - 15 - (9.9 * 3);
            leftzonehand.Z = 0;
            leftzonehand.Name = "L Hand";
            //leftzonehand.ZoneType = ZoneType.Position;
            leftzonehand.Width = 6.5 * 6;
            leftzonehand.Height = 9.9 * 6;
            leftzonehand.Depth = 0;
            leftzonehand.AcceptedJointType = JointType.HandLeft;
            Zones.Add(leftzonehand);
            #endregion

            this.Lesson.CprPage.zoneOverlay.ClearZones();
            // Add the zones into the zoneOverlay
            for (int i = 0; i < Zones.Count - 2; i++)
            {
                this.Lesson.CprPage.zoneOverlay.AddZone(Zones[i]);
            }
            /*
            foreach (Zone zone in this.Zones)
            {
            }*/

            InitialChinDepth = this.Lesson.ManikinChinLocation.Z;

            yellowCounter = 0;

            this.CurrentStageWatchers = new List<BaseWatcher>();
            this.PeripheralStageWatchers = new List<BaseWatcher>();
            this.OutlyingStageWatchers = new List<BaseWatcher>();

            CreateStageWatchers();

            this.StartTime = DateTime.Now;

            this.IsSetup = true;

            this.Stopwatch.Start();

            this.TimeoutStopwatch.Start();
        }

        private void CreateStageWatchers()
        {
            // Clear the list of watchers.
            this.CurrentStageWatchers.Clear();
            this.PeripheralStageWatchers.Clear();
            this.OutlyingStageWatchers.Clear();

            // Add in new watchers.
            Watcher manikinChinPositionChangeWatcher = new Watcher();

            #region WatcherLogic

            // Checks for the manikin's head to change position.
            manikinChinPositionChangeWatcher.CheckForActionDelegate = () =>
            {
                if (!manikinHeadMoved) // Never set to True, so always passes
                {
                    System.Windows.Point p = AIMS.Assets.Lessons2.ColorTracker.find(AIMS.Assets.Lessons2.ColorTracker.YellowTape, new Int32Rect((int)this.Lesson.ManikinChinLocation.X - 10, (int)this.Lesson.ManikinChinLocation.Y - 10, 20, 20), KinectManager.MappedColorData);

                    if (!Double.IsNaN(p.X) && !Double.IsNaN(p.Y))
                    {
                        int zIndex = (int)(p.Y * 640 + p.X);

                        if (KinectManager.PixelData[zIndex].IsKnownDepth)
                        {
                            watcher_FoundManikinHeadPos.Z += KinectManager.PixelData[zIndex].Depth;
                            watcher_YellowDepthCounter++;
                        }
                        else
                        {
                            watcher_FoundManikinHeadPos.Z = 0;
                            watcher_YellowDepthCounter = 0;
                        }

                        watcher_FoundManikinHeadPos.X += p.X;
                        watcher_FoundManikinHeadPos.Y += p.Y;

                        watcher_YellowCounter++;
                    }
                    else
                    {
                        // Color wasn't found this time, reset X, Y, Z, and both counters.
                        watcher_YellowCounter = 0;
                        watcher_FoundManikinHeadPos.X = 0;
                        watcher_FoundManikinHeadPos.Y = 0;

                        watcher_YellowDepthCounter = 0;
                        watcher_FoundManikinHeadPos.Z = 0;

                    }

                    if (watcher_YellowCounter > 1 && watcher_YellowDepthCounter > 1)
                    {
                        // Get the average of the found x's.
                        watcher_FoundManikinHeadPos.X /= watcher_YellowCounter;
                        // Get the average of the found y's.
                        watcher_FoundManikinHeadPos.Y /= watcher_YellowCounter;
                        // Get the average of the found z's.
                        watcher_FoundManikinHeadPos.Z /= watcher_YellowCounter;

                        //System.Diagnostics.Debug.WriteLine(String.Format("ManikinRefoundAt: ({0},{1},{2}).", watcher_FoundManikinHeadPos.X, watcher_FoundManikinHeadPos.Y, watcher_FoundManikinHeadPos.Z));

                        // Set the manikinChinLocation to the newly found point.
                        watcher_FoundChin = true;
                        //MessageBox.Show(String.Format("Depth of chin is {0}mm from the Kinect sensor.", newHeadLocation.Z));

                        // Check for a change in Depth.
                        double deltaDepth = System.Math.Abs(watcher_FoundManikinHeadPos.Z - this.Lesson.ManikinChinLocation.Z);

                        System.Diagnostics.Debug.WriteLine(String.Format("Change in depth is: {0}mm", deltaDepth));

                        if ( deltaDepth > 14 && deltaDepth < 300 )
                        {
                            manikinChinPositionChangeWatcher.IsTriggered = true;
                        }
                        else
                        {
                            manikinChinPositionChangeWatcher.IsTriggered = false;
                            watcher_FoundManikinHeadPos = new Point3D();
                            watcher_YellowCounter = 0;
                            watcher_YellowDepthCounter = 0;
                            watcher_FoundChin = false;
                        }
                    }
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(manikinChinPositionChangeWatcher);

            Watcher rightHandOrWristByChinWatcher = new Watcher();

            #region WatcherLogic
/*
            // Checks for the right hand or wrist to come into close proximity of the manikin's chin location.
            rightHandOrWristByChinWatcher.CheckForActionDelegate = () =>
            {
                SkeletonPoint manikinChinInSkeletalSpace;
                SkeletonPoint rHandLoc;
                SkeletonPoint rWristLoc;
                double rHandDistance;
                double rWristDistance;

                manikinChinInSkeletalSpace = KinectManager.Kinect.CoordinateMapper.MapDepthPointToSkeletonPoint(KinectManager.LastImageFormat, new DepthSpacePoint() { X = (int)this.Lesson.ManikinChinLocation.X, Y = (int)this.Lesson.ManikinChinLocation.Y, Depth = (int)this.Lesson.ManikinChinLocation.Z });

                SkeletonProfile skele = null;

                if (this.Lesson.StuckSkeletonProfileID.HasValue)
                {
                    // Retreive
                    skele = KinectManager.GetSkeletonProfile(this.Lesson.StuckSkeletonProfileID, true);
                }
                if (skele != null)
                {               
                    // Get the right hand and right wrist joint positions out of the profile.
                    rHandLoc = skele.JointProfiles.First(o => o.JointType == JointType.HandRight).CurrentPosition;
                    rWristLoc = skele.JointProfiles.First(o => o.JointType == JointType.WristRight).CurrentPosition;

                    // Distance returned by KinectManager.Distance3D( SkeletalPoint a, SkeletalPoint b) is in SkeletalSpace, and therefore is represented in Meters.
                    rHandDistance = System.Math.Abs(KinectManager.Distance3D(manikinChinInSkeletalSpace, rHandLoc));     // Check the distance between the manikinChin in SkeletalSpace and the right hand. Absolute value is used to negate which point is used as the base.
                    rWristDistance = System.Math.Abs(KinectManager.Distance3D(manikinChinInSkeletalSpace, rWristLoc));   // Check the distance between the manikinChin in the SkeletalSpace and the right wrist. Absolute value is used to negate which point is used as the base.

                    System.Diagnostics.Debug.WriteLine(String.Format("R-Hand to chin:  {0:#}mm", rHandDistance * 1000));
                    //System.Diagnostics.Debug.WriteLine(String.Format("R-Wrist to chin: {0:#}mm @{1}", rWristDistance * 1000, KinectManager.AzimuthAngle(manikinChinInSkeletalSpace, rWristLoc)));

                    double desiredMaxDistance = 0.200;  // 200 meters

                    // Check if the right hand or wrist is found near the chin of the manikin.
                    if (rHandDistance <= desiredMaxDistance || rWristDistance <= desiredMaxDistance)
                    {
                        // If it is within the disiredMaxDistance, flip our trigger switch on.
                        rightHandOrWristByChinWatcher.IsTriggered = true;
                    }
                    else
                    {
                        // If it's outside the desiredMaxDistance, then flip our trigger switch off.
                        rightHandOrWristByChinWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    // If no skeleton was found, then the hand/wrist can't be near the face..
                    rightHandOrWristByChinWatcher.IsTriggered = false;
                }
            };
*/
            #endregion

            this.CurrentStageWatchers.Add(rightHandOrWristByChinWatcher);

            Watcher leftHandOrWristByChinWatcher = new Watcher();

            #region WatcherLogic
/*
            // Checks for the left hand or wrist to come into close proximity of the manikin's chin location.
            leftHandOrWristByChinWatcher.CheckForActionDelegate = () =>
            {
                SkeletonPoint manikinChinInSkeletalSpace;
                SkeletonPoint lHandLoc;
                SkeletonPoint lWristLoc;
                double lHandDistance;
                double lWristDistance;

                manikinChinInSkeletalSpace = KinectManager.Kinect.CoordinateMapper.MapDepthPointToSkeletonPoint(KinectManager.LastImageFormat, new DepthSpacePoint() { X = (int)this.Lesson.ManikinChinLocation.X, Y = (int)this.Lesson.ManikinChinLocation.Y, Depth = (int)this.Lesson.ManikinChinLocation.Z });

                SkeletonProfile skele = null;

                if (this.Lesson.StuckSkeletonProfileID.HasValue)
                {
                    // Retreive
                    skele = KinectManager.GetSkeletonProfile(this.Lesson.StuckSkeletonProfileID, true);
                }
                if (skele != null)
                {
                    // Get the left hand and left wrist joint positions out of the profile.
                    lHandLoc = skele.JointProfiles.First(o => o.JointType == JointType.HandLeft).CurrentPosition;
                    lWristLoc = skele.JointProfiles.First(o => o.JointType == JointType.WristLeft).CurrentPosition;

                    // Distance returned by KinectManager.Distance3D( SkeletalPoint a, SkeletalPoint b) is in SkeletalSpace, and therefore is represented in Meters.
                    lHandDistance = System.Math.Abs(KinectManager.Distance3D(manikinChinInSkeletalSpace, lHandLoc));     // Check the distance between the manikinChin in SkeletalSpace and the left hand. Absolute value is used to negate which point is used as the base.
                    lWristDistance = System.Math.Abs(KinectManager.Distance3D(manikinChinInSkeletalSpace, lWristLoc));   // Check the distance between the manikinChin in the SkeletalSpace and the left wrist. Absolute value is used to negate which point is used as the base.

                    System.Diagnostics.Debug.WriteLine(String.Format("L-Hand to chin:  {0:#}mm", lHandDistance * 1000));
                    //System.Diagnostics.Debug.WriteLine(String.Format("L-Wrist to chin: {0:#}mm @{1}", lWristDistance * 1000, KinectManager.AzimuthAngle(manikinChinInSkeletalSpace, lWristLoc)));

                    double desiredMaxDistance = 0.200;  // 200 mm

                    // Check if the left hand or wrist is found near the chin of the manikin.
                    if (lHandDistance <= desiredMaxDistance || lWristDistance <= desiredMaxDistance)
                    {
                        // If it is within the disiredMaxDistance, flip our trigger switch on.
                        leftHandOrWristByChinWatcher.IsTriggered = true;
                    }
                    else
                    {
                        // If it's outside the desiredMaxDistance, then flip our trigger switch off.
                        leftHandOrWristByChinWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    // If no skeleton was found, then the hand/wrist can't be near the face..
                    leftHandOrWristByChinWatcher.IsTriggered = false;
                }
            };
*/
            #endregion

            this.CurrentStageWatchers.Add(leftHandOrWristByChinWatcher);
        }

        public override void UpdateStage()
        {
            if (!this.IsSetup)
                SetupStage();

            if (KinectManager.ClosestSkeletonProfile == null)
                return;

            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
                this.TimeoutStage();

            #region Update each zone's joint state.
            foreach (Zone zone in this.Zones)
            {
                zone.UpdateJointWithinZone();
            }
            #endregion

            if (this.CurrentStageWatchers[0].IsTriggered)
            {
                if (this.watcher_FoundManikinHeadPos.X != 0 && this.watcher_FoundManikinHeadPos.Y != 0 && this.watcher_FoundManikinHeadPos.Z != 0)
                {
                    this.NewHeadLocation = this.watcher_FoundManikinHeadPos;
                    this.Lesson.ManikinChinLocation = this.NewHeadLocation;

                    // Chin was found with a change in depth within 8-300mm.
                    if (!this.wasHeadTiltedBack)
                    {
                        this.WasHeadTiltedBack = true;
                    }
                }
                else
                {
                    this.watcher_YellowCounter = 0;
                    this.watcher_YellowDepthCounter = 0;
                }

                
            }
            /*else
            {
                if (this.wasHeadTiltedBack)
                {
                    this.WasHeadTiltedBack = false;
                }
            }*/

            if (this.CurrentStageWatchers[1].IsTriggered)
            {
                // The right hand is within close proximity to the manikin's chin location.
                if (!this.RightHandOrWristByManikinsChin)
                {
                    this.RightHandOrWristByManikinsChin = true;
                }
            }
            else
            {
                if (this.RightHandOrWristByManikinsChin)
                {
                    this.RightHandOrWristByManikinsChin = false;
                }
            }

            if (this.CurrentStageWatchers[2].IsTriggered)
            {
                // The left hand is within close proximity to the manikin's chin location.
                if (!this.LeftHandOrWristByManikinsChin)
                {
                    this.LeftHandOrWristByManikinsChin = true;
                }
            }
            else
            {
                if (this.LeftHandOrWristByManikinsChin)
                {
                    this.LeftHandOrWristByManikinsChin = false;
                }
            }

            #region OldLogic
            /*
            // Hands are within the proper position.
            if ((Zones[0].IsWithinZone && Zones[1].IsWithinZone) || (Zones[1].IsWithinZone && Zones[2].IsWithinZone) || (Zones[0].IsWithinZone && Zones[3].IsWithinZone) || (Zones[2].IsWithinZone && Zones[3].IsWithinZone))
            {
                // Check to see if the hands are within the zone for long enough. In this case, 10 ms.
                if ((Zones[0].TimeWithinZone >= TimeSpan.FromMilliseconds(10.0) && Zones[1].TimeWithinZone >= TimeSpan.FromMilliseconds(10.0))      // Wrists
                  || (Zones[2].TimeWithinZone >= TimeSpan.FromMilliseconds(10.0) && Zones[3].TimeWithinZone >= TimeSpan.FromMilliseconds(10.0))     // Hands
                  ||  (Zones[0].TimeWithinZone >= TimeSpan.FromMilliseconds(10.0) && Zones[3].TimeWithinZone >= TimeSpan.FromMilliseconds(10.0))    // Wrists
                  || (Zones[1].TimeWithinZone >= TimeSpan.FromMilliseconds(10.0) && Zones[2].TimeWithinZone >= TimeSpan.FromMilliseconds(10.0)))    // Hands
                {


                    
                    #region refind manikin chin location
                    if (!refoundHead)
                    {
                        System.Windows.Point p = Watchers.Assets.Lessons2.ColorTracker.find(Watchers.Assets.Lessons2.ColorTracker.YellowTape, new Int32Rect((int)this.Lesson.ManikinChinLocation.X - 10, (int)this.Lesson.ManikinChinLocation.Y - 10, 20, 20), KinectManager.MappedColorData);

                        if (!Double.IsNaN(p.X) && !Double.IsNaN(p.Y))
                        {
                            int zIndex = (int)(p.Y * 640 + p.X);

                            if (KinectManager.PixelData[zIndex].IsKnownDepth)
                            {
                                newHeadLocation.Z += KinectManager.PixelData[zIndex].Depth;
                                yellowDepthCounter++;
                            }

                            newHeadLocation.X += p.X;
                            newHeadLocation.Y += p.Y;

                            yellowCounter++;
                        }
                        else
                        {
                            newHeadLocation.Z = 0;
                            newHeadLocation.X = 0;
                            newHeadLocation.Y = 0;

                            yellowDepthCounter = 0;
                            yellowCounter = 0;
                        }

                        if (yellowCounter > 1)
                        {
                            // Get the average of the found x's.
                            newHeadLocation.X /= yellowCounter;
                            // Get the average of the found y's.
                            newHeadLocation.Y /= yellowCounter;

                            if (yellowDepthCounter > 0)
                            {
                                newHeadLocation.Z /= yellowDepthCounter;
                            }
                            else
                            {
                                newHeadLocation.Z = 0;
                            }

                            // Set the manikinChinLocation to the newly found point.
                            refoundHead = true;
                            yellowCounter = 0;
                            yellowDepthCounter = 0;
                            //MessageBox.Show(String.Format("Depth of chin is {0}mm from the Kinect sensor.", newHeadLocation.Z));
                        }
                    }
                    #endregion

                    if (refoundHead)
                    {
                        if (newHeadLocation.Z != InitialChinDepth)
                        {
                            WasHeadTiltedBack = true;
                            this.Lesson.ManikinChinLocation = newHeadLocation;
                        }
                        else
                        {
                            refoundHead = false;
                            newHeadLocation = new Point3D();
                            yellowCounter = 0;
                            yellowDepthCounter = 0;
                        }
                    }
                }
            }
            */
            #endregion

            if (WasHeadTiltedBack && RightHandOrWristByManikinsChin && LeftHandOrWristByManikinsChin)
            {
                this.Complete = true;
                EndStage();
            }
        }

        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }

        public override void EndStage()
        {
            this.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            // Get the current time to signify the time the stage ended.
            EndTime = DateTime.Now;

            //StageScore stagescore = new StageScore();
            // Create a screenshot for the current stage.

            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                StageScore ss = (StageScore)args.Argument;

                //do something
                ss.Screenshot = KinectManager.CreateScreenshot();
                // Set the hasScreenshot variable if the screenshot was created successfully.
                if (ss.Screenshot == null)
                    ss.HasScreenshot = false;
                else ss.HasScreenshot = true;

            };

            worker.RunWorkerAsync(this.StageScore);

            this.StageScore.StageNumber = this.StageNumber;

            this.StageScore.StageName = this.Name;

            CalculateStage1Score(this.StageScore);

            // store how much time was spent in this stage.
            this.StageScore.TimeInStage = this.Stopwatch.Elapsed;

            this.StageScore.Target = this.Instruction;

            this.Lesson.Result.StageScores.Add(this.StageScore);    // Add the this.StageScore to the collection of StageScore objects for this Lesson's Result.

            this.Lesson.CurrentStageIndex++;

            this.Lesson.PlayNextStageSound();
        }

        public void CalculateStage1Score(StageScore stagescore)
        {
            double score = 1;

            if (WasHeadTiltedBack)
            {
                stagescore.Result += "The head was properly tilted back.";
                score = 1;
            }

            stagescore.Score = System.Math.Max(0, System.Math.Min(score, 1)); // clamp the score between 0 and 1, and then set it.
        }

        public override void PauseStage()
        {
            this.Lesson.Paused = true;
            this.Lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.Lesson.Paused = false;
            this.Lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void ResetStage()
        {

        }

        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            //this.PauseStage();
            this.TimedOut = true;
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            this.RequestStageVideo();   // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
        }

        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.Lesson.CprPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Cpr/Videos/01_Tilt_Back_Head.wmv", UriKind.Relative));
            this.Lesson.CprPage.VideoPlayer.startVideo( this, new RoutedEventArgs() );

            this.Lesson.CprPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.Lesson.CprPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.CprPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.CprPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.CprPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.CprPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        public override void WipeClean()
        {
            WasHeadTiltedBack = false;
            RefoundHead = false;
            NewHeadLocation = new Point3D();
            InitialChinDepth = 0;
            YellowCounter = 0;
            YellowDepthCounter = 0;

            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;

            this.IsSetup = false;

            if (this.StageScore != null)
            {
                this.StageScore.WipeClean();
                this.StageScore = null;
                this.StageScore = new StageScore();
            }
            else
            {
                this.StageScore = new StageScore();
            }

            this.Active = false;
            this.Complete = false;

            if (this.Zones != null)
            {
                this.Zones.Clear();
            }
            else
            {
                this.Zones = new List<Zone>();
            }

            if (this.Stopwatch != null)
            {
                this.Stopwatch.Reset();
            }
            else
            {
                this.Stopwatch = new System.Diagnostics.Stopwatch();
            }

            if (this.TimeoutStopwatch != null)
                this.TimeoutStopwatch.Reset();
            else this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();

            this.TimedOut = false;
        }
    }
}
