﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace AIMS.Tasks.Cpr
{
    /// <summary>
    /// Class to hold final representations of a compression set from CPR's CprCompressions stage.
    /// </summary>
    public class CprCompressionSetResult : INotifyPropertyChanged
    {
        #region Private Fields
        // Database Fields: 
        private int? cprResultID;
        private int? imageID;

        // Compression Set Fields:
        private int? setNumber;
        private int? setDurationMiliseconds;
        private KinectImage image;
        private int? validCompressionCount;
        private double? depth;
        private double? depthInches;
        private double? score;
        private double? handPlacementScore;
        private double? lockedArmsScore;
        private double? shouldersOverManikinScore;
        private int? repsPerMinute;
        private List<int> feedbackIDs = new List<int>();

        private ObservableRangeCollection<CprCompressions.Compression> compressions;
        #endregion

        #region Public Properties
        // Database Fields: 
        public int? CprResultID { get { return cprResultID; } set { if (cprResultID == value) cprResultID = value; else { cprResultID = value; NotifyPropertyChanged("CprResultID"); } } }
        public int? ImageID { get { return imageID; } set { if (imageID == value) imageID = value; else { imageID = value; NotifyPropertyChanged("ImageID"); } } }
        public List<int> FeedbackIDs { get { return feedbackIDs; } set { feedbackIDs = value; NotifyPropertyChanged("FeedbackIDs"); } }
        public TaskFeedback[] FeedbackValues { get { return TaskFeedback.GetFeedbackFromIDs( this.feedbackIDs, Task.CPR ); } }

        // Compression Set Fields:
        public int? SetNumber { get { return setNumber; } set { if (setNumber == value) setNumber = value; else { setNumber = value; NotifyPropertyChanged("SetNumber"); } } }
        public int? SetDurationMiliseconds { get { return setDurationMiliseconds; } set { if (setDurationMiliseconds == value) setDurationMiliseconds = value; else { setDurationMiliseconds = value; NotifyPropertyChanged("SetDurationMiliseconds"); } } }
        public KinectImage Image { get { return image; } set { if (image == value) image = value; else { image = value; NotifyPropertyChanged("Image"); } } }
        public int? ValidCompressionCount { get { return validCompressionCount; } set { if (validCompressionCount == value) validCompressionCount = value; else { validCompressionCount = value; NotifyPropertyChanged("ValidCompressionCount"); } } }
        public double? Depth { get { return depth; } set { if (depth == value) depth = value; else { depth = value; NotifyPropertyChanged("Depth"); } } }
        public double? DepthInches { get { return depthInches; } set { if (depthInches == value) depthInches = value; else { depthInches = value; NotifyPropertyChanged("DepthInches"); } } }
        public double? Score { get { return score; } set { if (score == value) score = value; else { score = value; NotifyPropertyChanged("Score"); } } }
        public double? HandPlacementScore { get { return handPlacementScore; } set { if (handPlacementScore == value) handPlacementScore = value; else { handPlacementScore = value; NotifyPropertyChanged("HandPlacementScore"); } } }
        public double? LockedArmsScore { get { return lockedArmsScore; } set { if (lockedArmsScore == value) lockedArmsScore = value; else { lockedArmsScore = value; NotifyPropertyChanged("LockedArmsScore"); } } }
        public double? ShouldersOverManikinScore { get { return shouldersOverManikinScore; } set { if (shouldersOverManikinScore == value) shouldersOverManikinScore = value; else { shouldersOverManikinScore = value; NotifyPropertyChanged("ShouldersOverManikinScore"); } } }
        public int? RepsPerMinute { get { return repsPerMinute; } set { if (repsPerMinute == value) repsPerMinute = value; else { repsPerMinute = value; NotifyPropertyChanged("RepsPerMinute"); } } }

        public ObservableRangeCollection<CprCompressions.Compression> Compressions { get { return compressions; } private set { compressions = value; NotifyPropertyChanged("Compressions"); } }
        #endregion

        #region Constructors
        public CprCompressionSetResult()
        {

        }

        public CprCompressionSetResult( CprCompressions.CompressionSet set )
        {
            this.SetNumber = set.SetNumber;
            this.SetDurationMiliseconds = set.SetLengthMiliseconds;
            this.Image = set.Image;
            this.ValidCompressionCount = set.ValidCompressionCount;
            this.Depth = set.AverageValidDepth;
            this.DepthInches = set.AverageValidDepthInches;
            this.Score = set.AverageValidScore;
            this.HandPlacementScore = set.AverageValidHandPlacementScore;
            this.LockedArmsScore = set.AverageValidArmsLockedScore;
            this.ShouldersOverManikinScore = set.AverageValidShoulderOverManikinScore;
            this.RepsPerMinute = (int)set.RepsPerMinute;
            this.FeedbackIDs = set.FeedbackIDs;

            this.Compressions = set.Compressions;
        }
        #endregion

        #region Public Functions

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
