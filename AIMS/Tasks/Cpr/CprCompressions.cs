﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Microsoft.Kinect;
using System.Windows;
using System.Windows.Media.Media3D;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using System.Timers;
using AIMS.Tasks;
using AIMS.Utilities.Kinect;
using AIMS.Assets.Lessons2;
using System.IO;

namespace AIMS.Tasks.Cpr
{
    public class CprCompressions : Stage, INotifyPropertyChanged
    {
        public enum CompressionState
        {
            None,
            Compressing,
            Decompressing
        };

        #region Readonly's for Compression

        /// <summary>
        /// Amount of Compression Sets to record.
        /// </summary>
        public readonly int NUM_SETS;
        /// <summary>
        /// Length of each compression set in ms.
        /// </summary>
        public readonly int SET_INTERVAL; //ms

        #endregion

        private Stopwatch invalidSkeletonTimer;
        public Stopwatch InvalidSkeletonTimer { get { return invalidSkeletonTimer; } set { invalidSkeletonTimer = value; } }

        private Watcher invalidSkeletonWatcher;
        /// <summary>
        /// Watcher which, when Triggered, indicates the skeleton not fully valid.
        /// </summary>
        public Watcher InvalidSkeletonWatcher { get { return invalidSkeletonWatcher; } set { invalidSkeletonWatcher = value; } }

        /// <summary>
        /// 2D collection of compressions. Array of compression sets length NUM_SETS,
        /// List of compressions in each set collected within the SET_INTERVAL (ms).
        /// </summary>
        public List<Compression>[] Compressions { get { return this.compressions; } }
        /// <summary>
        /// Collection of images, of length NUM_SETS, stored for each compression set.
        /// </summary>
        public KinectImage[] CompressionSetImages { get { return this.compressionSetImages; } }

        public delegate void CompressionSetImageDone(object source, CompressionSetImageEventArgs e);
        public event CompressionSetImageDone ImageFinished;

        private ObservableRangeCollection<CompressionSet> compressionSets;
        public ObservableRangeCollection<CompressionSet> CompressionSets
        {
            get { return compressionSets; }
            set
            {
                if (this.compressionSets != value)
                {
                    this.compressionSets = value;
                    NotifyPropertyChanged("CompressionSets");
                }
                else
                {
                    this.compressionSets = value;
                }
            }
        }

        public class CompressionSetImageEventArgs : EventArgs
        {
            private int imageIndx;
            public int ImageIndx { get { return this.imageIndx; } }

            private KinectImage image = null;
            public KinectImage Image { get { return image; } private set { image = value; } }

            public CompressionSetImageEventArgs(int ImageIndx, KinectImage image)
            {
                this.imageIndx = ImageIndx;
                this.Image = image;
            }
        }

        protected virtual void OnImageDone(CompressionSetImageEventArgs e)
        {
            if (this.ImageFinished != null)
            {
                this.ImageFinished(this, e);
            }
        }

        #region Private Fields

        private Cpr lesson;
        // StickySkeleton used by Stage from KinectManager
        private Body stickySkeleton;

        // ZONEs Initialized in SetupZones()
        // Zone to look for hands in the correct place on the manikin.
        private RecZone zoneHandsOnChest;
        // Zone to see if shoulders are above the manikin within the correct depth.
        private RecZone zoneCheckShoulders;
        // Zones to DEBUG, test size on screen to see if feasible.
        private RecZone zoneTestSizeRht;
        private RecZone zoneTestSizeLft;

        // WATCHERs Initialized in CreateStageWatchers()
        // Watches for the user's hands to be in the correct position above the manikin.
        private Watcher handsOnChestWatcher;
        // Watches for the user's arms to be locked; their elbows are not bent.
        private Watcher armsLockedWatcher;
        // Watches for the user's shoulders and arms to be positioned over the mannekin's chest.
        private Watcher bodyAboveChestWatcher;
        // Watches for CPR compression.
        private Watcher compressionWatcher;
        private Watcher bentArmCompressionWatcher;
        /// <summary>
        /// Adds additional checks when watching for compressions prior to the compression stage.
        ///
        /// This is to further reduce unintentional compressions from triggering the start of the compressions timer.
        /// </summary>
        private Watcher earlyStartCompressionsWatcher;

        // JOINTHISTORY Initialized in SetupJointHistory()
        // Stores collection of specified joints and their history.
        private AIMS.Utilities.Kinect.JointHistory jointHistory;

        private List<Compression>[] compressions;
        private CompressionPart compressionPart;
        private CompressionPart bentArmCompressionPart;
        private CompressionState compressionState;
        private KinectImage[] compressionSetImages;

        private Timer compTimer;
        private int compSetCnt;

        private double distanceBetweenElbows = 0;

        private DateTime lastCompressionTime = DateTime.MinValue;
        #endregion

        #region Compression Detection Event
        public class CompressionDetectedEventArgs : EventArgs
        {
            private Compression compression;
            public Compression Compression { get { return compression; } private set { compression = value; } }

            public CompressionDetectedEventArgs( Compression comp ) : base()
            {
                this.Compression = comp;
            }
        }

        public delegate void CompressionDetectedEventHandler(object sender, CompressionDetectedEventArgs e);

        public event CompressionDetectedEventHandler CompressionDetected;

        public void NotifyCompressionDetected(Compression comp)
        {
            if (this.CompressionDetected != null)
            {
                this.CompressionDetected(this, new CompressionDetectedEventArgs(comp));
            }
        }
        #endregion

        public CompressionPart ReusedCompressionPart { get { return compressionPart; } set { if (this.compressionPart == value) { this.compressionPart = value; } else { this.compressionPart = value; NotifyPropertyChanged("ReusedCompressionPart"); } } }
        public CompressionPart ReusedBentArmCompressionPart { get { return bentArmCompressionPart; } set { if (this.bentArmCompressionPart == value) { this.bentArmCompressionPart = value; } else { this.bentArmCompressionPart = value; NotifyPropertyChanged("ReusedBentArmCompressionPart"); } } }

        /// <summary>
        /// CprCompressions Stage Constructor. Must pass Lesson, can set number of compression sets and length in seconds of each compression set.
        /// </summary>
        /// <param name="Lesson">Lesson which is creating / using this stage.</param>
        /// <param name="NumSets">Number of Compression Sets to record.</param>
        /// <param name="SetInterval">Length in Seconds of each Compression Set.</param>
        public CprCompressions(Cpr Lesson, int NumSets = 8, int SetInterval = 15)
        {
            this.lesson = Lesson;

            this.NUM_SETS = NumSets;
            this.SET_INTERVAL = SetInterval * 1000; // convert from seconds to ms.

            this.CurrentStageWatchers = new List<BaseWatcher>();
            this.PeripheralStageWatchers = new List<BaseWatcher>();
            this.OutlyingStageWatchers = new List<BaseWatcher>();

            Name = "Compressions";
            Instruction = "Place the heel of your hand parallel to the sternum between the nipples and place the heel of your free hand directly over the wrist" +
                            " of your engaged hand. Lock your fingers together. \nShoulders should be directly over your hands with your arms straightened and elbows locked.";
            TargetTime = TimeSpan.MaxValue; // No target time for calibration...
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Tasks/Cpr/Graphics/CPR007.png";
            ScoreWeight = 1;
            Zones = new List<Zone>(10);
            Stopwatch = new System.Diagnostics.Stopwatch();
            TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.CanTimeOut = false;

            this.CompressionSets = new ObservableRangeCollection<CompressionSet>();

            this.ReusedCompressionPart = new CompressionPart();

            this.ReusedBentArmCompressionPart = new CompressionPart();

            this.InvalidSkeletonTimer = new Stopwatch();
        }

        /// <summary>
        /// Initializes most of the CprCalibrationStage Fields. Rigs everything together.
        /// </summary>
        public override void SetupStage()
        {
            System.Diagnostics.Debug.WriteLine("* Setting Up Compression Stage. *");

            // Update CprPage for this Stage
            this.lesson.CprPage.StageInfoTextBlock.Text = this.Name;
            this.lesson.CprPage.InstructionTextBlock.Text = this.Instruction;
            this.lesson.CprPage.CurrentStageImage.Source = new BitmapImage(new Uri(this.ImagePath, UriKind.Relative));
            this.lesson.CprPage.zoneOverlay.ClearZones();

            // Create Zones Watchers and JointHistory
            this.SetupZones();
            this.CreateStageWatchers();
            this.SetupJointHistory();

            // Reset Timers / Clocks / Watches
            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.Stopwatch.Reset();
            this.Stopwatch.Start();
            this.TimeoutStopwatch.Reset();
            this.TimeoutStopwatch.Start();

            // Setup Compression Specific Variables
            this.compressions = new List<Compression>[NUM_SETS];
            for (int i = 0; i < NUM_SETS; i++)
                this.compressions[i] = new List<Compression>();
            this.compressionSetImages = new KinectImage[NUM_SETS];

            this.CompressionSets.Clear();   // clear out the old compression sets

            for( int i = 0; i < NUM_SETS; i++ )
                this.CompressionSets.Add(new CompressionSet(i, this.SET_INTERVAL));

            this.NotifyPropertyChanged("CompressionSets");

            this.compressionState = CompressionState.None;
            this.compTimer = new Timer(SET_INTERVAL) { AutoReset = true };
            this.compTimer.Elapsed += compressionTimer_Elapsed;
            this.compSetCnt = 0;

            // Reset StageScore
            this.StageScore.WipeClean();
            this.StageScore = null;
            this.StageScore = new StageScore();

            // Done setting up!
            this.IsSetup = true;
            this.Complete = false;
        }

        /// <summary>
        /// Initialize ZONEs and set default values
        /// </summary>
        public void SetupZones()
        {
            System.Diagnostics.Debug.WriteLine("* Setting Up Compression Stage - Zones. *");

            this.Zones.Clear();

            // Setup zones to look for mannequin face and side locations around left hand.
            this.zoneHandsOnChest = new RecZone()
            {
                Width = 150, // pixels
                Height = 110, // pixels
                Depth = 350, // mm
                CenterX = this.lesson.ManikinSideLocation.X,
                CenterY = this.lesson.ManikinChinLocation.Y - 55,
                CenterZ = this.lesson.ManikinSideLocation.Z + 130,
                //CenterZ = this.lesson.ManikinChinLocation.Z, TRYING USING SIDE INSTEAD OF CHIN
                IsImproperZone = false,
                DoesTrackJoint = false,
                Active = true,
                Name = "zoneAboveManikinSide"
            };
            this.Zones.Add(this.zoneHandsOnChest);

            this.zoneCheckShoulders = new RecZone()
            {
                Width = 225, // pixels
                Height = 150, // pixels
                Depth = 200, // mm
                CenterX = this.lesson.ManikinSideLocation.X,
                CenterY = this.lesson.ManikinSideLocation.Y - 250,
                Z = this.lesson.ManikinSideLocation.Z - 50,
                IsImproperZone = false,
                /*DoesTrackJoint = true,*/
                Active = true,
                Name = "zoneCheckShoulders"
            };
            this.Zones.Add(this.zoneCheckShoulders);

            this.zoneTestSizeRht = new RecZone()
            {
                Height = 25, // pixels
                Depth = 0, // default 2D zone
                IsImproperZone = false,
                DoesTrackJoint = false,
                Active = false,
                Name = "zoneTestSizeRht"
            };
            this.Zones.Add(this.zoneTestSizeRht);

            this.zoneTestSizeLft = new RecZone()
            {
                Height = 25, // pixels
                Depth = 0, // default 2D zone
                IsImproperZone = false,
                DoesTrackJoint = false,
                Active = false,
                Name = "zoneTestSizeLft"
            };
            this.Zones.Add(this.zoneTestSizeLft);

            // Add zones to the lesson page zone overlay.
            this.ShowZones();
        }

        /// <summary>
        /// Wipes the currently shown zones in the zone overlay, and replaces them with the zones for this stage.
        /// </summary>
        public void ShowZones()
        {
            this.lesson.CprPage.zoneOverlay.ClearZones();

            foreach (Zone zone in this.Zones)
                this.lesson.CprPage.zoneOverlay.AddZone(zone);
        }

        /// <summary>
        /// Initialize Joint History
        /// </summary>
        private void SetupJointHistory()
        {
            System.Diagnostics.Debug.WriteLine("* Setting Up Compression Stage - Joint History. *");

            // Initialize and set HistoryLength
            this.jointHistory = new Utilities.Kinect.JointHistory() { HistoryLength = 15 };

            // Specify joints to track / collect history for.
            this.jointHistory.AddJoint(JointType.ShoulderLeft);
            this.jointHistory.AddJoint(JointType.ShoulderRight);
            this.jointHistory.AddJoint(JointType.ElbowLeft);
            this.jointHistory.AddJoint(JointType.ElbowRight);
            this.jointHistory.AddJoint(JointType.WristLeft);
            this.jointHistory.AddJoint(JointType.WristRight);
        }

        /// <summary>
        /// Create watcher logic for watchers within this stage.
        /// </summary>
        private void CreateStageWatchers()
        {

            System.Diagnostics.Debug.WriteLine("* Setting Up Compression Stage - Watchers. *");

            // Clear the lists of watchers.
            this.CurrentStageWatchers.Clear();
            this.PeripheralStageWatchers.Clear();
            this.OutlyingStageWatchers.Clear();

            #region Invalid Skeleton Watcher

            this.invalidSkeletonWatcher = new Watcher();

            this.invalidSkeletonWatcher.CheckForActionDelegate = () =>
                {
                    if (this.stickySkeleton == null || KinectManager.SingleStickySkeletonProfile == null)
                    {
                        this.invalidSkeletonWatcher.IsTriggered = true;

                        if (!this.InvalidSkeletonTimer.IsRunning)
                            this.InvalidSkeletonTimer.Start();  // Start the timer, a null skeleton isn't a valid skeleton.
                    }
                    else
                    {
                        double leftangle = KinectManager.Angle2D(KinectManager.SingleStickySkeletonProfile.JointProfiles[(int)Microsoft.Kinect.JointType.ShoulderLeft].CurrentPosition, KinectManager.SingleStickySkeletonProfile.JointProfiles[(int)Microsoft.Kinect.JointType.ElbowLeft].CurrentPosition, KinectManager.SingleStickySkeletonProfile.JointProfiles[(int)Microsoft.Kinect.JointType.WristLeft].CurrentPosition);
                        double rightangle = KinectManager.Angle2D(KinectManager.SingleStickySkeletonProfile.JointProfiles[(int)Microsoft.Kinect.JointType.ShoulderRight].CurrentPosition, KinectManager.SingleStickySkeletonProfile.JointProfiles[(int)Microsoft.Kinect.JointType.ElbowRight].CurrentPosition, KinectManager.SingleStickySkeletonProfile.JointProfiles[(int)Microsoft.Kinect.JointType.WristRight].CurrentPosition);

                        System.Diagnostics.Debug.WriteLine(String.Format("Left: {0:0.###} ({1}), Right: {2:0.###} ({3})", leftangle, CprCompressions.IsGoodCompressionAngleLeft(leftangle) ? "Good" : CprCompressions.IsBadCompressionAngleLeft(leftangle) ? "Bad" : !CprCompressions.IsValidCompressionAngleLeft(leftangle) ? "Invalid" : "Valid", rightangle, CprCompressions.IsGoodCompressionAngleRight(rightangle) ? "Good" : CprCompressions.IsBadCompressionAngleRight(rightangle) ? "Bad" : !CprCompressions.IsValidCompressionAngleRight(rightangle) ? "Invalid" : "Valid"));

                        if (!CprCompressions.IsValidCompressionAngleLeft(leftangle) || !CprCompressions.IsValidCompressionAngleRight(rightangle))
                        {
                            // It's an invalid skeleton.
                            this.invalidSkeletonWatcher.IsTriggered = true;

                            if (!this.InvalidSkeletonTimer.IsRunning)
                                this.InvalidSkeletonTimer.Start(); // Start the timer if it's not already started.

                            return;
                        }
                        else
                        {
                            // It's a valid skeleton.
                            this.invalidSkeletonWatcher.IsTriggered = false;

                            if (this.InvalidSkeletonTimer.IsRunning)
                                this.InvalidSkeletonTimer.Reset();  // Reset the timer if it was running.
                        }
                    }
                };

            this.CurrentStageWatchers.Add(this.invalidSkeletonWatcher);
            this.PeripheralStageWatchers.Add(this.invalidSkeletonWatcher);
            this.OutlyingStageWatchers.Add(this.invalidSkeletonWatcher);

            #endregion

            #region handsOnChestWatcher
            this.handsOnChestWatcher = new Watcher();
            this.handsOnChestWatcher.CheckForActionDelegate = () =>
                {
                    if (this.stickySkeleton == null)
                    {
                        this.handsOnChestWatcher.IsTriggered = false;
                        return;
                    }

                    Joint handLeftJoint = this.stickySkeleton.Joints[JointType.HandLeft];
                    Joint handRightJoint = this.stickySkeleton.Joints[JointType.HandRight];
                    Joint wristLeftJoint = this.stickySkeleton.Joints[JointType.WristLeft];
                    Joint wristRightJoint = this.stickySkeleton.Joints[JointType.WristRight];

                    DepthSpacePoint leftHandPos;
                    DepthSpacePoint rightHandPos;
                    DepthSpacePoint leftWristPos;
                    DepthSpacePoint rightWristPos;

                    // Find the left and right hand positions in the depth frame.
                    try
                    {
                        leftHandPos = KinectManager.CoordinateMapper.MapCameraPointToDepthSpace(handLeftJoint.Position);
                        rightHandPos = KinectManager.CoordinateMapper.MapCameraPointToDepthSpace(handRightJoint.Position);
                        leftWristPos = KinectManager.CoordinateMapper.MapCameraPointToDepthSpace(wristLeftJoint.Position);
                        rightWristPos = KinectManager.CoordinateMapper.MapCameraPointToDepthSpace(wristRightJoint.Position);
                    }
                    catch (Exception exc)
                    {
                        System.Diagnostics.Debug.Print("CprBodyPosition ~ handsOnChestWatcher ERROR: {0}", exc.Message);
                        return;
                    }

                    // Not triggered... unless triggered.
                    this.handsOnChestWatcher.IsTriggered = false;

                    // Check to see if the hand (or wrist) of the left and right side are within the proper hand placement zone.
                    bool leftHandOrWristInZone = this.zoneHandsOnChest.IsPoint3DInside(new Point3D(leftHandPos.X, leftHandPos.Y, handLeftJoint.Position.Z)) || this.zoneHandsOnChest.IsPoint3DInside(new Point3D(leftWristPos.X, leftWristPos.Y, wristLeftJoint.Position.Z));
                    bool rightHandOrWristInZone = this.zoneHandsOnChest.IsPoint3DInside(new Point3D(rightHandPos.X, rightHandPos.Y, handRightJoint.Position.Z)) || this.zoneHandsOnChest.IsPoint3DInside(new Point3D(rightWristPos.X, rightWristPos.Y, wristRightJoint.Position.Z));

                    if (leftHandOrWristInZone)
                    {
                        this.ReusedCompressionPart.LeftProperHandPlacementCount++;    // Increment the left count on the compression part.
                        this.ReusedBentArmCompressionPart.LeftProperHandPlacementCount++;    // Increment the left count on the compression part.
                    }

                    if (rightHandOrWristInZone)
                    {
                        this.ReusedCompressionPart.RightProperHandPlacementCount++;    // Increment the right count on the compression part.
                        this.ReusedBentArmCompressionPart.RightProperHandPlacementCount++;    // Increment the right count on the compression part.
                    }

                    if (leftHandOrWristInZone && rightHandOrWristInZone)    // If both are in the zone, light up the zone.
                    {
                        this.zoneHandsOnChest.IsWithinZone = true;

                        // Make sure the hands are on the chest for a quarter of a second before triggering watcher.
                        if (this.zoneHandsOnChest.TimeWithinZone >= TimeSpan.FromMilliseconds(250))
                            this.handsOnChestWatcher.IsTriggered = true;
                    }
                    else
                        this.zoneHandsOnChest.IsWithinZone = false;
                };
            this.CurrentStageWatchers.Add(this.handsOnChestWatcher);   // Add the watcher to the list of watchers
            this.PeripheralStageWatchers.Add(this.handsOnChestWatcher);   // Add the watcher to the list of watchers
            this.OutlyingStageWatchers.Add(this.handsOnChestWatcher);   // Add the watcher to the list of watchers

            #endregion

            #region armsLockedWatcher
            this.armsLockedWatcher = new Watcher();
            this.armsLockedWatcher.CheckForActionDelegate = () =>
                {
                    // Do not check this watcher if the lesson is complete or the sticky skeleton is missing.
                    if (this.stickySkeleton == null)
                    {
                        this.armsLockedWatcher.IsTriggered = false;
                        return;
                    }

                    // For shoulder to elbow check in X dimension. Give shoulder a buffer in meters
                    const double shoulderOffset = 0.045;
                    // For wrist to wrist check in X dimension in meters.
                    const double wristDistanceThresh = 0.28;

                    // Right arm check.
                    Joint shoulderRight = this.stickySkeleton.Joints[JointType.ShoulderRight];
                    Joint wristRight = this.stickySkeleton.Joints[JointType.WristRight];
                    Joint elbowRight = this.stickySkeleton.Joints[JointType.ElbowRight];

                    DepthSpacePoint rightShldr = KinectManager.CoordinateMapper.MapCameraPointToDepthSpace(
                        shoulderRight.Position);
                    DepthSpacePoint rightElbw = KinectManager.CoordinateMapper.MapCameraPointToDepthSpace(
                        elbowRight.Position);

                    this.zoneTestSizeRht.Width = System.Math.Abs(rightElbw.X - rightShldr.X);
                    this.zoneTestSizeRht.Height = System.Math.Abs(rightElbw.Y - rightShldr.Y);
                    this.zoneTestSizeRht.X = rightShldr.X;
                    this.zoneTestSizeRht.Y = rightShldr.Y;
                    this.zoneTestSizeRht.Z = shoulderRight.Position.Z;

                    // Right Arm Locked defaults to TRUE unless any of the joints being checked are NotTracked.
                    bool rightArmLocked = shoulderRight.TrackingState != TrackingState.NotTracked && wristRight.TrackingState != TrackingState.NotTracked &&
                        elbowRight.TrackingState != TrackingState.NotTracked;

                    double leftangle = KinectManager.Angle2D(KinectManager.SingleStickySkeletonProfile.JointProfiles[(int)Microsoft.Kinect.JointType.ShoulderLeft].CurrentPosition, KinectManager.SingleStickySkeletonProfile.JointProfiles[(int)Microsoft.Kinect.JointType.ElbowLeft].CurrentPosition, KinectManager.SingleStickySkeletonProfile.JointProfiles[(int)Microsoft.Kinect.JointType.WristLeft].CurrentPosition);
                    double rightangle = KinectManager.Angle2D(KinectManager.SingleStickySkeletonProfile.JointProfiles[(int)Microsoft.Kinect.JointType.ShoulderRight].CurrentPosition, KinectManager.SingleStickySkeletonProfile.JointProfiles[(int)Microsoft.Kinect.JointType.ElbowRight].CurrentPosition, KinectManager.SingleStickySkeletonProfile.JointProfiles[(int)Microsoft.Kinect.JointType.WristRight].CurrentPosition);

                    if (!CprCompressions.IsGoodCompressionAngleRight(rightangle))
                    {
                        // Right shoulder is below right wrist / elbow, or inside of wrist / elbow.
                        if (shoulderRight.Position.Y < wristRight.Position.Y || shoulderRight.Position.X < wristRight.Position.X ||
                            shoulderRight.Position.Y < elbowRight.Position.Y || shoulderRight.Position.X + shoulderOffset < elbowRight.Position.X) // shoulder + shoulderOffset outside of elbow
                        {
                            rightArmLocked = false;
                            System.Diagnostics.Debug.Print("Right shoulder is below right wrist / elbow, or inside of wrist / elbow.");
                        }

                        // Right wrist is above or outside of right elbow.
                        if (wristRight.Position.X > elbowRight.Position.X || wristRight.Position.Y > elbowRight.Position.Y)
                        {
                            rightArmLocked = false;
                            System.Diagnostics.Debug.Print("Rist wrist is above or outside of right elbow.");
                        }
                    }

                    // Left arm check.
                    Joint shoulderLeft = this.stickySkeleton.Joints[JointType.ShoulderLeft];
                    Joint wristLeft = this.stickySkeleton.Joints[JointType.WristLeft];
                    Joint elbowLeft = this.stickySkeleton.Joints[JointType.ElbowLeft];

                    DepthSpacePoint leftShldr = KinectManager.CoordinateMapper.MapCameraPointToDepthSpace(
                        shoulderLeft.Position);
                    DepthSpacePoint leftElbw = KinectManager.CoordinateMapper.MapCameraPointToDepthSpace(
                        this.stickySkeleton.Joints[JointType.ElbowLeft].Position);

                    this.zoneTestSizeLft.Width = System.Math.Abs(leftElbw.X - leftShldr.X);
                    this.zoneTestSizeLft.Height = System.Math.Abs(leftElbw.Y - leftShldr.Y);
                    this.zoneTestSizeLft.X = leftElbw.X;
                    this.zoneTestSizeLft.Y = leftShldr.Y;
                    this.zoneTestSizeLft.Z = shoulderLeft.Position.Z;

                    // Left Arm Locked defaults to TRUE unless any of the joints being checked are NotTracked.
                    bool leftArmLocked = shoulderLeft.TrackingState != TrackingState.NotTracked && wristLeft.TrackingState != TrackingState.NotTracked &&
                        elbowLeft.TrackingState != TrackingState.NotTracked;

                    if (!CprCompressions.IsGoodCompressionAngleLeft(leftangle))
                    {
                        // Left shoulder is below left wrist / elbow, or inside of wrist / elbow.
                        if (shoulderLeft.Position.Y < wristLeft.Position.Y || shoulderLeft.Position.X > wristLeft.Position.X ||
                            shoulderLeft.Position.Y < elbowLeft.Position.Y || shoulderLeft.Position.X - shoulderOffset > elbowLeft.Position.X) // shoulder + shoulderOffset outside of elbow
                        {
                            leftArmLocked = false;
                            System.Diagnostics.Debug.Print("Left shoulder is below left wrist / elbow, or inside of wrist / elbow.");
                        }

                        // Left wrist is above or outside of left elbow.
                        if (wristLeft.Position.X < elbowLeft.Position.X || wristLeft.Position.Y > elbowLeft.Position.Y)
                        {
                            leftArmLocked = false;
                            System.Diagnostics.Debug.Print("Left wrist is above or outside of left elbow.");
                        }
                    }

                    this.zoneTestSizeRht.IsWithinZone = rightArmLocked;
                    this.zoneTestSizeLft.IsWithinZone = leftArmLocked;

                    double rightArmBendAngle = KinectManager.Angle2D(shoulderRight.Position, elbowRight.Position, wristRight.Position);
                    double leftArmBendAngle = KinectManager.Angle2D(shoulderLeft.Position, elbowLeft.Position, wristLeft.Position);

                    bool goodRightArmBendAngle = CprCompressions.IsGoodCompressionAngleRight(rightArmBendAngle);
                    bool goodLeftArmBendAngle = CprCompressions.IsGoodCompressionAngleLeft(leftArmBendAngle);

                    bool wristsClose = false;
                    // Wrists are not too far away from each other ( another way to check arms bent, wrists end up further away )
                    if (System.Math.Abs(wristRight.Position.X - wristLeft.Position.X) < wristDistanceThresh)
                    {
                        wristsClose = true;
                    }
                    else
                        System.Diagnostics.Debug.Print("WRISTS TOO FAR AWAY!!!!");

                    if (leftArmLocked && goodLeftArmBendAngle && wristsClose)
                        this.ReusedCompressionPart.LeftArmLockedCount++;

                    if (rightArmLocked && goodRightArmBendAngle && wristsClose)
                        this.ReusedCompressionPart.RightArmLockedCount++;

                    // Both arms are locked, and the wrists are close enough to each other.
                    if (rightArmLocked && leftArmLocked && wristsClose)
                        this.armsLockedWatcher.IsTriggered = true;
                    else
                        this.armsLockedWatcher.IsTriggered = false;
                };
            this.CurrentStageWatchers.Add(this.armsLockedWatcher);
            this.PeripheralStageWatchers.Add(this.armsLockedWatcher);
            this.OutlyingStageWatchers.Add(this.armsLockedWatcher);

            #endregion

            #region bodyAboveChestWatcher
            this.bodyAboveChestWatcher = new Watcher();
            this.bodyAboveChestWatcher.CheckForActionDelegate = () =>
                {
                    // Do not check this watcher if the lesson is complete or the sticky skeleton is missing.
                    if (this.stickySkeleton == null)
                    {
                        this.bodyAboveChestWatcher.IsTriggered = false;
                        return;
                    }

                    // Watcher is not triggered, unless triggered...
         //           this.bodyAboveChestWatcher.IsTriggered = false;

                    Joint shoulderRight = this.stickySkeleton.Joints[JointType.ShoulderRight];
                    Joint shoulderLeft = this.stickySkeleton.Joints[JointType.ShoulderLeft];

                    bool leftShoulderWithin = shoulderLeft.TrackingState != TrackingState.NotTracked && this.zoneCheckShoulders.IsJoint3DInside(shoulderLeft);
                    bool rightShoulderWithin = shoulderRight.TrackingState != TrackingState.NotTracked && this.zoneCheckShoulders.IsJoint3DInside(shoulderRight);

                    if (leftShoulderWithin)
                    {
                        this.ReusedCompressionPart.LeftShoulderOverManikinCount++;    // Increment the compressionPart counter for left shoulder.
                        this.ReusedBentArmCompressionPart.LeftShoulderOverManikinCount++;    // Increment the compressionPart counter for left shoulder.
                    }
                    if (rightShoulderWithin)
                    {
                        this.ReusedCompressionPart.RightShoulderOverManikinCount++;   // Increment the compressionPart counter for right shoulder.
                        this.ReusedBentArmCompressionPart.RightShoulderOverManikinCount++;   // Increment the compressionPart counter for right shoulder.
                    }

                    // If the joints within the zone are good, Manually set zone if all of the shoulder joints are in it.
                    if ( leftShoulderWithin && rightShoulderWithin )
                    {
                        this.zoneCheckShoulders.IsWithinZone = true;

                        // If all of the joints have been in the zone for at least a second, trigger the watcher.
                        //if (this.zoneCheckShoulders.TimeWithinZone >= TimeSpan.FromMilliseconds(1000))
                        this.bodyAboveChestWatcher.IsTriggered = true;

                        System.Diagnostics.Debug.WriteLine("Shoulders over manikin.");
                    }
                    else
                    {
                        this.zoneCheckShoulders.IsWithinZone = false;
                        this.bodyAboveChestWatcher.IsTriggered = false;

                        #region Find Shoulder Position Relative To Zone

                        #region Right Shoulder
                        if (rightShoulderWithin)
                        {
                            // Right shoulder is within the zone.
                            System.Diagnostics.Debug.WriteLine("Right shoulder is within the zone");
                        }
                        else
                        {
                            #region Check Z (Depth)
                            if (shoulderRight.Position.Z * 1000 > this.zoneCheckShoulders.Z + this.zoneCheckShoulders.Depth)
                            {
                                // Right shoulder too far behind zone.
                                System.Diagnostics.Debug.WriteLine(String.Format("Right shoulder ({0}) is behind (>) the zone ({1})", shoulderRight.Position.Z * 1000, this.zoneCheckShoulders.Z + this.zoneCheckShoulders.Depth));
                            }
                            else if (shoulderRight.Position.Z * 1000 < this.zoneCheckShoulders.Z)
                            {
                                // Right shoulder too far infront of zone.
                                System.Diagnostics.Debug.WriteLine(String.Format("Right shoulder ({0}) is infront (<) of the zone ({1})", shoulderRight.Position.Z * 1000, this.zoneCheckShoulders.Z));
                            }
                            #endregion
                        }
                        #endregion

                        #region Left Shoulder
                        if (leftShoulderWithin)
                        {
                            // Left shoulder is within the zone.
                            System.Diagnostics.Debug.WriteLine("Left shoulder is within the zone");
                        }
                        else
                        {
                            #region Check Z (Depth)
                            if (shoulderLeft.Position.Z * 1000 > this.zoneCheckShoulders.Z + this.zoneCheckShoulders.Depth)
                            {
                                // Left shoulder too far behind zone.
                                System.Diagnostics.Debug.WriteLine(String.Format("Left shoulder ({0}) is behind (>) the zone ({1})", shoulderLeft.Position.Z * 1000, this.zoneCheckShoulders.Z + this.zoneCheckShoulders.Depth));
                            }
                            else if (shoulderLeft.Position.Z * 1000 < this.zoneCheckShoulders.Z)
                            {
                                // Left shoulder too far infront of zone.
                                System.Diagnostics.Debug.WriteLine(String.Format("Left shoulder ({0}) is infront (<) of the zone ({1})", shoulderLeft.Position.Z * 1000, this.zoneCheckShoulders.Z));
                            }
                            #endregion
                        }
                        #endregion

                        #endregion
                    }

          //          Debug.Print("bodyAboveChestWatcher Triggered: {0}", this.bodyAboveChestWatcher.IsTriggered);
                };
            this.CurrentStageWatchers.Add(this.bodyAboveChestWatcher);
            this.PeripheralStageWatchers.Add(this.bodyAboveChestWatcher);
            this.OutlyingStageWatchers.Add(this.bodyAboveChestWatcher);

            #endregion

            #region compressionWatcher

            this.compressionWatcher = new Watcher();
            this.compressionWatcher.CheckForActionDelegate = () =>
                {
                    System.Diagnostics.Debug.WriteLine("* Checking for compressions. *");

                    if (this.stickySkeleton == null || this.compSetCnt >= NUM_SETS)
                    {
                        this.compressionWatcher.IsTriggered = false;
                        return;
                    }

                    // How far to back to check trend
                    int trendBack = 5;

                    Trend shldrLftTrndY = this.jointHistory.TrendFromLatest(JointType.ShoulderLeft, trendBack).Y;
                    Trend shldrRhtTrndY = this.jointHistory.TrendFromLatest(JointType.ShoulderRight, trendBack).Y;

                    JointExtended[] shoulderL = this.jointHistory[JointType.ShoulderLeft, trendBack];
                    JointExtended[] shoulderR = this.jointHistory[JointType.ShoulderRight, trendBack];

                    double lftMinDepth = double.MaxValue;
                    double rhtMinDepth = double.MaxValue;
                    double lftMaxDepth = double.MinValue;
                    double rhtMaxDepth = double.MinValue;

                    // *** DISCOVER SHOULDER COMPRESSION STATE ***
                    if ((shldrLftTrndY == Trend.Negative || shldrRhtTrndY == Trend.Negative) &&
                            (shldrLftTrndY != Trend.Positive && shldrRhtTrndY != Trend.Positive))
                        this.compressionState = CompressionState.Compressing;

                    else if ((shldrLftTrndY == Trend.Positive || shldrRhtTrndY == Trend.Positive) &&
                            (shldrLftTrndY != Trend.Negative && shldrRhtTrndY != Trend.Negative))
                        this.compressionState = CompressionState.Decompressing;
                        /*

                    else
                        this.compressionState = CompressionState.None;*/

                    this.ReusedCompressionPart.TotalFrames = this.ReusedCompressionPart.TotalFrames + 1; // Increment the Total Frames count on the ReusedCompressionPart.

                    // Look for depth associated with compression state. ( compression depth vs decompression depth )
                    for (int i = 0; i < System.Math.Min(shoulderL.Length, shoulderR.Length); i++)
                    {
                        switch (compressionState)
                        {
                            case CompressionState.Compressing:
                                if (shoulderL[i] != null)
                                {
                                    if (lftMinDepth > shoulderL[i].Position.Y)
                                        lftMinDepth = shoulderL[i].Position.Y;
                                }
                                if (shoulderR[i] != null)
                                {
                                    if (rhtMinDepth > shoulderR[i].Position.Y)
                                        rhtMinDepth = shoulderR[i].Position.Y;
                                }
                                this.ReusedCompressionPart.LeftCompressDepth = lftMinDepth;
                                this.ReusedCompressionPart.RightCompressDepth = rhtMinDepth;
                                break;
                            case CompressionState.Decompressing:
                                if (shoulderL[i] != null)
                                {
                                    if (lftMaxDepth < shoulderL[i].Position.Y)
                                        lftMaxDepth = shoulderL[i].Position.Y;
                                }
                                if (shoulderR[i] != null)
                                {
                                    if (rhtMaxDepth < shoulderR[i].Position.Y)
                                        rhtMaxDepth = shoulderR[i].Position.Y;
                                }
                                this.ReusedCompressionPart.LeftDecompressDepth = lftMaxDepth;
                                this.ReusedCompressionPart.RightDecompressDepth = rhtMaxDepth;
                                break;
                        }
                    }

                    // Add compression if it exists.
                    if (this.ReusedCompressionPart.Depth.HasValue && this.compressionState != CompressionState.Decompressing)
                    {
                        Compression comp = new Compression(this.ReusedCompressionPart.Depth.Value)
                        {
                            RightArmLocked = (this.ReusedCompressionPart.RightArmLockedPercent >= 0.5),
                            LeftArmLocked = (this.ReusedCompressionPart.LeftArmLockedPercent >= 0.5),
                            RightShoulderOverManikin = (this.ReusedCompressionPart.RightShoulderOverManikinPercent >= 0.5),
                            LeftShoulderOverManikin = (this.ReusedCompressionPart.LeftShoulderOverManikinPercent >= 0.5),
                            RightProperHandPlacement = (this.ReusedCompressionPart.RightProperHandPlacementPercent >= 0.5),
                            LeftProperHandPlacement = (this.ReusedCompressionPart.LeftProperHandPlacementPercent >= 0.5),
                            // Store the time between compressions
                            TimeAfterPreviousCompression = this.lastCompressionTime == DateTime.MinValue ? TimeSpan.Zero : DateTime.Now - this.lastCompressionTime
                        };

                        this.lastCompressionTime = comp.Time;

                        this.CompressionSets[compSetCnt].AddCompression(comp);

                        this.NotifyCompressionDetected(comp);   // Alert the event to any listeners.

                        this.compressionWatcher.IsTriggered = true;

                        // Wipe the compressionPart to look for the next compression
                        this.ReusedBentArmCompressionPart.Clear();
                        this.ReusedCompressionPart.Clear();
                    }
                    else
                        this.compressionWatcher.IsTriggered = false;
                };
            this.CurrentStageWatchers.Add(this.compressionWatcher);
            this.PeripheralStageWatchers.Add(this.compressionWatcher);
            this.OutlyingStageWatchers.Add(this.compressionWatcher);

            #endregion

            #region BentArmCompressionWatcher

            this.bentArmCompressionWatcher = new Watcher();
            this.bentArmCompressionWatcher.CheckForActionDelegate = () =>
            {
                System.Diagnostics.Debug.WriteLine("* Checking for bent arm compressions. *");

                if (this.stickySkeleton == null || this.compSetCnt >= NUM_SETS)
                {
                    this.bentArmCompressionWatcher.IsTriggered = false;
                    return;
                }

                // How far to back to check trend
                int trendBack = 5;

                Trend shoulderLeftTrendY = this.jointHistory.TrendFromLatest(JointType.ShoulderLeft, trendBack).Y;
                Trend shoulderRightTrendY = this.jointHistory.TrendFromLatest(JointType.ShoulderRight, trendBack).Y;
                Trend elbowLeftTrendX = this.jointHistory.TrendFromLatest(JointType.ElbowLeft, trendBack).X;
                Trend elbowRightTrendX = this.jointHistory.TrendFromLatest(JointType.ElbowRight, trendBack).X;

                JointExtended[] shoulderL = this.jointHistory[JointType.ShoulderLeft, trendBack];
                JointExtended[] shoulderR = this.jointHistory[JointType.ShoulderRight, trendBack];
                JointExtended[] elbowL = this.jointHistory[JointType.ElbowLeft, trendBack];
                JointExtended[] elbowR = this.jointHistory[JointType.ElbowRight, trendBack];

                double lftMinDepth = double.MaxValue;
                double rhtMinDepth = double.MaxValue;
                double lftMaxDepth = double.MinValue;
                double rhtMaxDepth = double.MinValue;

                double elbowToElbowDistance = KinectManager.Distance3D(this.stickySkeleton.Joints[JointType.ElbowLeft].Position, this.stickySkeleton.Joints[JointType.ElbowRight].Position);

                System.Diagnostics.Debug.WriteLine(String.Format("Elbow-To-Elbow Distance: {0} mm", elbowToElbowDistance));

                if (shoulderRightTrendY == Trend.None || shoulderLeftTrendY == Trend.None)
                {

                    #region *** DISCOVER COMPRESSION STATE ***

                    switch (elbowLeftTrendX)
                    {
                        case Trend.Positive:
                            switch (elbowRightTrendX)
                            {
                                case Trend.Positive:
                                    this.ReusedBentArmCompressionPart.CompressionState = CompressionState.None; // both moving in same direction? bent arm compressions occur when the elbows move in opposite X directions.
                                    break;

                                case Trend.Negative:
                                    this.ReusedBentArmCompressionPart.CompressionState = CompressionState.Compressing;
                                    break;

                                case Trend.None:
                                    this.ReusedBentArmCompressionPart.CompressionState = CompressionState.Compressing;
                                    break;
                            }
                            break;

                        case Trend.Negative:
                            switch (elbowRightTrendX)
                            {
                                case Trend.Positive:
                                    this.ReusedBentArmCompressionPart.CompressionState = CompressionState.Decompressing;
                                    break;

                                case Trend.Negative:
                                    this.ReusedBentArmCompressionPart.CompressionState = CompressionState.None; // both moving in same direction? bent arm compressions occur when the elbows move in opposite X directions.
                                    break;

                                case Trend.None:
                                    this.ReusedBentArmCompressionPart.CompressionState = CompressionState.Decompressing;
                                    break;
                            }
                            break;

                        case Trend.None:
                            switch (elbowRightTrendX)
                            {
                                case Trend.Positive:
                                    this.ReusedBentArmCompressionPart.CompressionState = CompressionState.Compressing;
                                    break;

                                case Trend.Negative:
                                    this.ReusedBentArmCompressionPart.CompressionState = CompressionState.Decompressing;
                                    break;

                                case Trend.None:
                                    this.ReusedBentArmCompressionPart.CompressionState = CompressionState.None; // No movement occuring on either elbow.
                                    break;
                            }
                            break;
                    }

                    /*
                if ((elbowLeftTrendX == Trend.Negative || elbowRightTrendX == Trend.Negative) &&
                        (elbowLeftTrendX != Trend.Positive && elbowRightTrendX != Trend.Positive))
                    this.ReusedBentArmCompressionPart.CompressionState = CompressionState.Compressing;

                else if ((elbowLeftTrendX == Trend.Positive || elbowRightTrendX == Trend.Positive) &&
                        (elbowLeftTrendX != Trend.Negative && elbowRightTrendX != Trend.Negative))
                    this.ReusedBentArmCompressionPart.CompressionState = CompressionState.Decompressing;

                else
                    this.ReusedBentArmCompressionPart.CompressionState = CompressionState.None;
                    */
                    #endregion

                    this.ReusedBentArmCompressionPart.TotalFrames = this.ReusedBentArmCompressionPart.TotalFrames + 1; // Increment the Total Frames count on the ReusedBentArmCompressionPart.

                    // Look for depth associated with compression state. ( compression depth vs decompression depth )
                    for (int i = 0; i < System.Math.Min(elbowL.Length, elbowR.Length); i++)
                    {
                        switch (this.ReusedBentArmCompressionPart.CompressionState)
                        {
                            case CompressionState.Compressing:
                                if (elbowL[i] != null)
                                {
                                    if (lftMinDepth > elbowL[i].Position.Y)
                                        lftMinDepth = elbowL[i].Position.Y;
                                }
                                if (elbowR[i] != null)
                                {
                                    if (rhtMinDepth > elbowR[i].Position.Y)
                                        rhtMinDepth = elbowR[i].Position.Y;
                                }
                                this.ReusedBentArmCompressionPart.LeftCompressDepth = lftMinDepth;
                                this.ReusedBentArmCompressionPart.RightCompressDepth = rhtMinDepth;
                                break;
                            case CompressionState.Decompressing:
                                if (elbowL[i] != null)
                                {
                                    if (lftMaxDepth < elbowL[i].Position.Y)
                                        lftMaxDepth = elbowL[i].Position.Y;
                                }
                                if (elbowR[i] != null)
                                {
                                    if (rhtMaxDepth < elbowR[i].Position.Y)
                                        rhtMaxDepth = elbowR[i].Position.Y;
                                }
                                this.ReusedBentArmCompressionPart.LeftDecompressDepth = lftMaxDepth;
                                this.ReusedBentArmCompressionPart.RightDecompressDepth = rhtMaxDepth;
                                break;
                        }
                    }

                    // Add compression if it exists.
                    if (this.ReusedBentArmCompressionPart.Depth.HasValue && this.ReusedBentArmCompressionPart.CompressionState != CompressionState.Decompressing)
                    {
                        Compression comp = new Compression(this.ReusedBentArmCompressionPart.Depth.Value)
                        {
                            RightArmLocked = (this.ReusedBentArmCompressionPart.RightArmLockedPercent >= 0.5),
                            LeftArmLocked = (this.ReusedBentArmCompressionPart.LeftArmLockedPercent >= 0.5),
                            RightShoulderOverManikin = (this.ReusedBentArmCompressionPart.RightShoulderOverManikinPercent >= 0.5),
                            LeftShoulderOverManikin = (this.ReusedBentArmCompressionPart.LeftShoulderOverManikinPercent >= 0.5),
                            RightProperHandPlacement = (this.ReusedBentArmCompressionPart.RightProperHandPlacementPercent >= 0.5),
                            LeftProperHandPlacement = (this.ReusedBentArmCompressionPart.LeftProperHandPlacementPercent >= 0.5),
                            // Store the time between compressions
                            TimeAfterPreviousCompression = this.lastCompressionTime == DateTime.MinValue ? TimeSpan.Zero : DateTime.Now - this.lastCompressionTime
                        };

                        //this.compressions[compSetCnt].Add(comp);

                        this.lastCompressionTime = comp.Time;

                        this.CompressionSets[compSetCnt].AddCompression(comp);

                        this.ReusedBentArmCompressionPart.Clear();
                        this.ReusedCompressionPart.Clear();

                        this.NotifyCompressionDetected(comp);   // Alert the event to any listeners.

                        this.bentArmCompressionWatcher.IsTriggered = true;

                        /*
                        if (this.compressions[compSetCnt].Count > 0)
                        {
                            Compression tmp = this.compressions[compSetCnt][this.compressions[compSetCnt].Count - 1];
                            System.Diagnostics.Debug.Print("Compression: {4} D: {0:0.##}\" {5:0.00} S:{1} AL:{2} SOM:{3}",
                                tmp.DepthInInches, tmp.Score, tmp.ArmsLocked, tmp.ShouldersOverManikin, this.compressions[compSetCnt].Count, tmp.Depth * 1000);
                        }*/

                        // Wipe the compressionPart to look for the next compression
                        this.ReusedBentArmCompressionPart.Clear();
                    }
                    else
                        this.bentArmCompressionWatcher.IsTriggered = false;
                }
            };
            this.CurrentStageWatchers.Add(this.bentArmCompressionWatcher);
            this.PeripheralStageWatchers.Add(this.bentArmCompressionWatcher);
            this.OutlyingStageWatchers.Add(this.bentArmCompressionWatcher);
            #endregion

            #region EarlyStartCompressionsWatcher

            this.earlyStartCompressionsWatcher = new Watcher();
            this.earlyStartCompressionsWatcher.CheckForActionDelegate = () =>
            {
                System.Diagnostics.Debug.WriteLine("* Checking for early compressions. *");

                this.earlyStartCompressionsWatcher.IsTriggered = false; // default to non-triggered

                if (this.lesson.CurrentStageIndex >= (int)Cpr.StageNumber.Compressions) // this watcher only runs prior to the current stage.
                    return;

                if (this.stickySkeleton == null || this.compSetCnt >= NUM_SETS)
                    return;

                bool compressingWithElbowsAndShoulders = false;
                bool shouldersAreOverManikin = false;

                #region Ensure Elbows and Shoulders are all compressing.

                CompressionState shoulderCompState = CompressionState.None;
                CompressionState elbowCompState = CompressionState.None;

                // How far to back to check trend
                int trendBack = 5;

                Trend elbowLftTrndY = this.jointHistory.TrendFromLatest(JointType.ElbowLeft, trendBack).Y;
                Trend elbowRhtTrndY = this.jointHistory.TrendFromLatest(JointType.ElbowRight, trendBack).Y;

                Trend shoulderLftTrndY = this.jointHistory.TrendFromLatest(JointType.ShoulderLeft, trendBack).Y;
                Trend shoulderRhtTrndY = this.jointHistory.TrendFromLatest(JointType.ShoulderRight, trendBack).Y;

                JointExtended[] elbowL = this.jointHistory[JointType.ElbowLeft, trendBack];
                JointExtended[] elbowR = this.jointHistory[JointType.ElbowRight, trendBack];

                JointExtended[] shoulderL = this.jointHistory[JointType.ShoulderLeft, trendBack];
                JointExtended[] shoulderR = this.jointHistory[JointType.ShoulderRight, trendBack];

                // *** DISCOVER COMPRESSION STATES ***
                if (elbowLftTrndY == Trend.Negative && elbowRhtTrndY == Trend.Negative)
                    elbowCompState = CompressionState.Compressing;

                else if ((elbowLftTrndY == Trend.Positive || elbowRhtTrndY == Trend.Positive) &&
                        (elbowLftTrndY != Trend.Negative && elbowRhtTrndY != Trend.Negative))
                    elbowCompState = CompressionState.Decompressing;

                if (shoulderLftTrndY == Trend.Negative && shoulderRhtTrndY == Trend.Negative)
                    shoulderCompState = CompressionState.Compressing;

                else if ((shoulderLftTrndY == Trend.Positive || shoulderRhtTrndY == Trend.Positive) &&
                        (shoulderLftTrndY != Trend.Negative && shoulderRhtTrndY != Trend.Negative))
                    shoulderCompState = CompressionState.Decompressing;

                // Elbows and Shoulders are both in the same decompression state.
                if (elbowCompState == CompressionState.Decompressing && shoulderCompState == CompressionState.Decompressing)
                {
                    compressingWithElbowsAndShoulders = true;
                }
                #endregion // Ensure Elbows and Shoulders are all compressing.

                #region Ensure the user is over the manikin.
                if (this.bodyAboveChestWatcher.IsTriggered)
                {
                    shouldersAreOverManikin = true;
                }
                #endregion // Ensure the user is over the manikin.

                // Watcher trigger condition:
                if (compressingWithElbowsAndShoulders && shouldersAreOverManikin)
                {
                    this.earlyStartCompressionsWatcher.IsTriggered = true;
                }
            };
            //this.CurrentStageWatchers.Add(this.earlyStartCompressionsWatcher);
            this.PeripheralStageWatchers.Add(this.earlyStartCompressionsWatcher);
            this.OutlyingStageWatchers.Add(this.earlyStartCompressionsWatcher);
            #endregion
        }

        public static bool IsGoodCompressionAngleLeft(double leftangle)
        {
            return leftangle > Compression.MIN_GOOD_ANGLE_LEFT && leftangle < Compression.MAX_GOOD_ANGLE_LEFT;
        }

        public static bool IsBadCompressionAngleLeft(double leftangle)
        {
            return leftangle > Compression.MIN_BAD_ANGLE_LEFT && leftangle < Compression.MAX_BAD_ANGLE_LEFT;
        }

        public static bool IsValidCompressionAngleLeft(double leftangle)
        {
            return !(leftangle > Compression.MIN_INVALID_ANGLE_LEFT && leftangle < Compression.MAX_INVALID_ANGLE_LEFT);
        }

        public static bool IsGoodCompressionAngleRight(double rightangle)
        {
            return rightangle > Compression.MIN_GOOD_ANGLE_RIGHT && rightangle < Compression.MAX_GOOD_ANGLE_RIGHT;
        }

        public static bool IsBadCompressionAngleRight(double rightangle)
        {
            return rightangle > Compression.MIN_BAD_ANGLE_RIGHT && rightangle < Compression.MAX_BAD_ANGLE_RIGHT;
        }

        public static bool IsValidCompressionAngleRight(double rightangle)
        {
            return !(rightangle > Compression.MIN_INVALID_ANGLE_RIGHT && rightangle < Compression.MAX_INVALID_ANGLE_RIGHT);
        }

        /// <summary>
        /// Should be called every Frame. Updates all of the stage logic
        /// </summary>
        public override void UpdateStage()
        {
            if (!IsSetup)
                SetupStage();

            if (this.compSetCnt >= NUM_SETS)
            {
                this.compTimer.Elapsed -= compressionTimer_Elapsed;
                this.compTimer.Stop();
                this.compTimer = null;

                this.EndStage();

                return; // return early.
            }

            if (KinectManager.DepthWidth == 0)
                return;

            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
                this.OnStageTimeout();

            // Important so when sticky skeleton isn't found in frame, don't use last occurance of sticky skeleton.
            this.stickySkeleton = null;
            // Use first occurance found of a sticky skeleton in the StickySkeletonIDs array.
            if (KinectManager.NumStickySkeletons == 1)
            {
                foreach (ulong? stickySkelId in KinectManager.StickySkeletonIDs)
                {
                    if (stickySkelId.HasValue)
                    {
                        // Set found Sticky Skeleton.
                        this.stickySkeleton = KinectManager.GetSkeleton(stickySkelId.Value, true);

                        // Update joint history for tracked joints.
                        this.jointHistory.UpdateJointHistory(this.stickySkeleton);
                        break;
                    }
                }
            }

            // Handle starting and stoping the compressions timer.
            if (this.compTimer != null && !this.compTimer.Enabled && this.compSetCnt < NUM_SETS )
            {
                // Too avoid pre-firing unintentionally, ensure that the compressions collected are within a short time range (they did 3 in 2 seconds, rather than doing 3 over a minute...)
                List<Compression> toRemove = new List<Compression>();

                foreach (Compression comp in this.CompressionSets[0].Compressions)
                {
                    if (comp.Time < (DateTime.Now.Subtract(TimeSpan.FromSeconds(5.0))))
                    {
                        toRemove.Add(comp); // Add to remove list if older than 5 seconds
                    }
                }

                foreach (Compression comp in toRemove)
                {
                    this.CompressionSets[0].Compressions.Remove(comp);  // remove from list
                }

                toRemove.Clear();   // clear temp list..

                // Now that the compressions list has been trimmed down to only compressions which are within the near time range, check to see if the comp timer should pre-fire.
                if ((this.compressionWatcher.IsTriggered && this.earlyStartCompressionsWatcher.IsTriggered && (this.CompressionSets[0].Compressions.Count > 2 && this.CompressionSets[0].AverageValidScore > 0.5) || this.CompressionSets[0].Compressions.Count > 4)    // If real compressions are being found, begin the timer.
                /*|| (this.bentArmCompressionWatcher.IsTriggered && (this.CompressionSets[0].Compressions.Count > 2 && this.CompressionSets[0].AverageValidScore > 0.5) || this.CompressionSets[0].Compressions.Count > 4)*/)  // Otherwise, if we start getting an adequate number of bent-arm compressions, begin the timer.
                {
                    this.CompressionSets[0].ClearCompressions();    // Clear out the compressions in the first set so that the interval is clean prior to collecting it's compressions, and any compressions used to determine whether the user is performing compressions are tossed (these are usually invalidly collected due to the transition getting into compression-ready position).

                    this.compTimer.Start(); // Start the compression collection interval timer.
                }
            }

            if (this.InvalidSkeletonWatcher.IsTriggered && this.InvalidSkeletonTimer.Elapsed.TotalSeconds >= 5.0)  // 5 seconds spent without a valid skeleton.
            {
                // Time out.
                System.Diagnostics.Debug.WriteLine(String.Format("INVALID SKELETON FOR {0:0.###}s", this.InvalidSkeletonTimer.Elapsed.TotalSeconds));

                #region TODO: Implement the effect logic of a skeleton not being tracked for a prolonged period of time.
                Console.Beep();
                this.InvalidSkeletonTimer.Restart();
                #endregion
            }
        }

        // Placeholder until this function signature is removed from Stage class.
        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            this.UpdateWatcherStates();
        }

        public void UpdateWatcherStates()
        {
            // Only run if stage is setup
            if (!this.IsSetup)
                return;

            int difference = this.lesson.CurrentStageIndex - this.StageNumber;

            // Update Current Watchers
            if (difference == 0)
            {
                foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    watcher.UpdateWatcher();
            }
            // Update Peripheral Watchers
            if (difference == 1 || difference == -1) // Runs watchers on stage after this stage.
            {
                foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    watcher.UpdateWatcher();
            }
            // Update Outlier Watchers
            if (difference > 1 || difference < -1)
            {
                foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    watcher.UpdateWatcher();
            }
        }

        public override void EndStage( bool playSound = true )
        {
            this.InvalidSkeletonTimer.Stop();

            Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            this.EndTime = DateTime.Now;
            
            //if( playSound ) lesson.PlayNextStageSound();

            this.lesson.Running = true;

            if( playSound )
                lesson.PlayNextStageSound();

            this.Complete = true;
            this.lesson.StageComplete(this);


        }

        public override void PauseStage()
        {
            this.lesson.Paused = true;
            this.lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.lesson.Paused = false;
            this.lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void ResetStage()
        {
            this.WipeClean();
            this.SetupStage();
        }

        #region Action Functions
        
        // Updates the compression set counter, for which list to store the compression data in.
        private void compressionTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            System.Windows.Application.Current.Dispatcher.Invoke( System.Windows.Threading.DispatcherPriority.Normal,
                (Action)delegate()
                {
                    try
                    {
                        if (compSetCnt < this.CompressionSets.Count)
                        {
                            System.Diagnostics.Debug.Print("CompressionSet: {0}, depth: {1:0.000} ~ {2:0.000}\", rate: {3}/min, score: {4}, handplacementscore: {5}," +
                                                            "armslockedscore: {6}, chestovermanikinscore: {7}, num valid compressions: {8}", 
                                                            this.CompressionSets[compSetCnt].SetNumber, this.CompressionSets[compSetCnt].AverageValidDepth, 
                                                            this.CompressionSets[compSetCnt].AverageValidDepthInches, this.CompressionSets[compSetCnt].RepsPerMinute, 
                                                            this.CompressionSets[compSetCnt].AverageValidScore, 
                                                            this.CompressionSets[compSetCnt].AverageValidHandPlacementScore, 
                                                            this.CompressionSets[compSetCnt].AverageValidArmsLockedScore, 
                                                            this.CompressionSets[compSetCnt].AverageValidShoulderOverManikinScore, 
                                                            this.CompressionSets[compSetCnt].ValidCompressionCount);

                            #region Overall
                            try
                            {
                                if (this.CompressionSets[compSetCnt].AverageValidScore >= 0.95)
                                {
                                    this.lesson.DetectableActions.DetectAction("Correctly Performed", "Overall", String.Format("Compression Set {0}", compSetCnt), true);
                                }
                                else if (this.CompressionSets[compSetCnt].AverageValidScore == 0)
                                {
                                    this.lesson.DetectableActions.DetectAction("Not Performed", "Overall", String.Format("Compression Set {0}", compSetCnt), true);
                                }
                                else
                                {
                                    this.lesson.DetectableActions.DetectAction("Incorrectly Performed", "Overall", String.Format("Compression Set {0}", compSetCnt), true);
                                }
                            }
                            catch (Exception ex)
                            {
                                System.Diagnostics.Debug.WriteLine(ex.ToString());
                            }
                            #endregion

                            #region Compression Depth
                            try
                            {
                                if (this.CompressionSets[compSetCnt].AverageValidDepthInches >= 2.0)
                                {
                                    this.lesson.DetectableActions.DetectAction("Depth Within Range", "Compression Depth", String.Format("Compression Set {0}", compSetCnt), true);
                                }
                                else if (this.CompressionSets[compSetCnt].AverageValidDepthInches == 0)
                                {
                                    this.lesson.DetectableActions.DetectAction("No Depth", "Compression Depth", String.Format("Compression Set {0}", compSetCnt), true);
                                }
                                else
                                {
                                    this.lesson.DetectableActions.DetectAction("Depth Too Low", "Compression Depth", String.Format("Compression Set {0}", compSetCnt), true);
                                }
                            }
                            catch (Exception ex)
                            {
                                System.Diagnostics.Debug.WriteLine(ex.ToString());
                            }
                            #endregion

                            #region Hand Placement
                            try
                            {
                                if (this.CompressionSets[compSetCnt].AverageValidHandPlacementScore <= 0.5)
                                {
                                    this.lesson.DetectableActions.DetectAction("Incorrect Hand Placement", "Hand Placement", String.Format("Compression Set {0}", compSetCnt), true);
                                }
                                else
                                {
                                    this.lesson.DetectableActions.DetectAction("Hands Over Chest", "Hand Placement", String.Format("Compression Set {0}", compSetCnt), true);
                                }
                            }
                            catch (Exception ex)
                            {
                                System.Diagnostics.Debug.WriteLine(ex.ToString());
                            }
                            #endregion

                            #region Lock Arms
                            try
                            {
                                if (this.CompressionSets[compSetCnt].AverageValidArmsLockedScore <= 0.5)
                                {
                                    this.lesson.DetectableActions.DetectAction("Arms Unlocked", "Lock Arms", String.Format("Compression Set {0}", compSetCnt), true);
                                }
                                else
                                {
                                    this.lesson.DetectableActions.DetectAction("Arms Locked", "Lock Arms", String.Format("Compression Set {0}", compSetCnt), true);
                                }
                            }
                            catch (Exception ex)
                            {
                                System.Diagnostics.Debug.WriteLine(ex.ToString());
                            }
                            #endregion

                            #region Shoulder Positioning
                            try
                            {
                                if (this.CompressionSets[compSetCnt].AverageValidShoulderOverManikinScore <= 0.5)
                                {
                                    this.lesson.DetectableActions.DetectAction("Not Directly Over Chest", "Shoulder Positioning", String.Format("Compression Set {0}", compSetCnt), true);
                                }
                                else
                                {
                                    this.lesson.DetectableActions.DetectAction("Directly Over Chest", "Shoulder Positioning", String.Format("Compression Set {0}", compSetCnt), true);
                                }
                            }
                            catch (Exception ex)
                            {
                                System.Diagnostics.Debug.WriteLine(ex.ToString());
                            }
                            #endregion

                            #region Reps Per Minute
                            try
                            {
                                if (this.CompressionSets[compSetCnt].RepsPerMinute < 100)
                                {
                                    this.lesson.DetectableActions.DetectAction("Slow Rep Rate", "Reps Per Minute", String.Format("Compression Set {0}", compSetCnt), true);
                                }
                                else if (this.CompressionSets[compSetCnt].RepsPerMinute > 120)
                                {
                                    this.lesson.DetectableActions.DetectAction("Fast Rep Rate", "Reps Per Minute", String.Format("Compression Set {0}", compSetCnt), true);
                                }
                                else
                                {
                                    this.lesson.DetectableActions.DetectAction("Proper Rep Rate", "Reps Per Minute", String.Format("Compression Set {0}", compSetCnt), true);
                                }
                            }
                            catch (Exception ex)
                            {
                                System.Diagnostics.Debug.WriteLine(ex.ToString());
                            }
                            #endregion
                        }

                        if (compSetCnt < this.compressionSetImages.Length)
                        {
                            BackgroundWorker worker = new BackgroundWorker();

                            worker.DoWork += delegate(object s, DoWorkEventArgs args)
                            {
                                int indx = (int)args.Argument;
                                KinectImage img = KinectImage.TakeKinectSnapshot(true, true);
                                this.compressionSetImages[indx] = img;
                                this.OnImageDone(new CompressionSetImageEventArgs(indx, img));

                                System.Diagnostics.Debug.WriteLine(String.Format("Taking Image: {0}", indx));
                            };

                            worker.RunWorkerAsync(compSetCnt);
                        }

                        this.compSetCnt++;

                        bool groupRates = true;

                        if (groupRates)
                        {
                            #region Calculate Rate Groups
                            int perfectRateMin = 100;
                            int goodRateMin = 65;
                            int poorRateMin = 30;

                            int numPerfect = 0;
                            int numGood = 0;
                            int numPoor = 0;
                            int numBad = 0;

                            for( int j = 0; j < this.CompressionSets.Count; j++ )
                            {
                                int numSetPerfect = 0;
                                int numSetGood = 0;
                                int numSetPoor = 0;
                                int numSetBad = 0;

                                for (int i = 0; i < this.CompressionSets[j].Compressions.Count; i++)
                                {
                                    double elapsed = this.CompressionSets[j].Compressions[i].TimeAfterPreviousCompression.TotalSeconds;

                                    double rpm = 60 / elapsed;

                                    System.Diagnostics.Debug.WriteLine(String.Format("{0} ({1} per minute", elapsed, rpm));

                                    if (rpm > perfectRateMin)
                                    {
                                        numPerfect++;
                                        numSetPerfect++;
                                    }

                                    else if (rpm > goodRateMin)
                                    {
                                        numGood++;
                                        numSetGood++;
                                    }

                                    else if (rpm > poorRateMin)
                                    {
                                        numPoor++;
                                        numSetPoor++;
                                    }

                                    else
                                    {
                                        numBad++;
                                        numSetBad++;
                                    }
                                }

                                System.Diagnostics.Debug.WriteLine(String.Format("Set {1} - Perfect: {0}", numSetPerfect, j));
                                System.Diagnostics.Debug.WriteLine(String.Format("Set {1} - Good: {0}", numSetGood, j));
                                System.Diagnostics.Debug.WriteLine(String.Format("Set {1} - Poor: {0}", numSetPoor, j));
                                System.Diagnostics.Debug.WriteLine(String.Format("Set {1} - Bad: {0}", numSetBad, j));
                            }

                            System.Diagnostics.Debug.WriteLine(String.Format("Perfect: {0}", numPerfect));
                            System.Diagnostics.Debug.WriteLine(String.Format("Good: {0}", numGood));
                            System.Diagnostics.Debug.WriteLine(String.Format("Poor: {0}", numPoor));
                            System.Diagnostics.Debug.WriteLine(String.Format("Bad: {0}", numBad));
                            #endregion
                        }

                        //compTimer.Start();
                    }
                    catch (Exception exc)
                    {
                        System.Diagnostics.Debug.Print("CprCompressions compressionTimer_Elapsed ERROR: {0}", exc.Message);
                    }
                });
        }

        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            //this.PauseStage();
            this.TimedOut = true;
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            this.RequestStageVideo();   // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
        }

        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.lesson.CprPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Cpr/Videos/CPR 007.wmv", UriKind.Relative));
            this.lesson.CprPage.VideoPlayer.startVideo(this, new RoutedEventArgs());

            this.lesson.CprPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.lesson.CprPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.lesson.CprPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.lesson.CprPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.lesson.CprPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.lesson.CprPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        #endregion

        public override void WipeClean()
        {
            this.lastCompressionTime = DateTime.MinValue;

            if (this.InvalidSkeletonTimer != null)
            {
                this.InvalidSkeletonTimer.Reset();
            }

            if (this.ReusedCompressionPart != null)
            {
                this.ReusedCompressionPart.Clear();
            }

            if (this.ReusedBentArmCompressionPart != null)
            {
                this.ReusedBentArmCompressionPart.Clear();
            }

            if( this.compTimer != null )
            {
                this.compTimer.Stop();

                try
                {
                    this.compTimer.Elapsed -= compressionTimer_Elapsed;
                }
                catch
                {
                }
            }

            if (this.CompressionSets != null)
            {
                for (int i = 0; i < this.CompressionSets.Count; i++)
                {
                    if (this.CompressionSets[i] != null)
                        this.CompressionSets[i].ClearCompressions();    // clear out the compression sets.
                }
            }

            this.IsSetup = false;
            this.Complete = false;
            this.TimedOut = false;
        }

        #region Internal Compression Classes
        /// <summary>
        /// Struct which stores what a 'compression' is for CPR.
        /// </summary>
        public class Compression : INotifyPropertyChanged
        {
            #region Constants For Compression Validity And Diagnostics
            // Left - Good Compressions (Arms Locked)
            public const double MIN_GOOD_ANGLE_LEFT = -180;
            public const double MAX_GOOD_ANGLE_LEFT = -135;
            // Left - Bad Compressions (Arms Unlocked)
            public const double MIN_BAD_ANGLE_LEFT = -135;
            public const double MAX_BAD_ANGLE_LEFT = -45;
            // Left - Invalid Compressions (Arms Invalidly Positioned) --> Either incorrectly assumed by the Kinect, or they aren't trying to perform compressions.
            public const double MIN_INVALID_ANGLE_LEFT = -45;
            public const double MAX_INVALID_ANGLE_LEFT = 180;

            // Right - Good Compressions (Arms Locked)
            public const double MIN_GOOD_ANGLE_RIGHT = 135;
            public const double MAX_GOOD_ANGLE_RIGHT = 180;
            // Right - Bad Compressions (Arms Unlocked)
            public const double MIN_BAD_ANGLE_RIGHT = 45;
            public const double MAX_BAD_ANGLE_RIGHT = 135;
            // Right - Invalid Compressions (Arms Invalidly Positioned) --> Either incorrectly assumed by the Kinect, or they aren't trying to perform compressions.
            public const double MIN_INVALID_ANGLE_RIGHT = -180;
            public const double MAX_INVALID_ANGLE_RIGHT = 45;
            #endregion

            #region Private Fields
            private int number = 0;

            private double depth = 0;
            private DateTime time = DateTime.Now;
            private TimeSpan timeAfterPreviousCompression = TimeSpan.Zero;
            private bool leftArmLocked = false;
            private bool rightArmLocked = false;
            private bool leftShldrOverManikin = false;
            private bool rightShldrOverManikin = false;
            private bool leftProperHandPlacement = false;
            private bool rightProperHandPlacement = false;

            #endregion

            #region Public Properties
            public TimeSpan TimeAfterPreviousCompression
            {
                get { return timeAfterPreviousCompression; }
                set
                {
                    if (timeAfterPreviousCompression == value)
                        return;

                    timeAfterPreviousCompression = value;
                    NotifyPropertyChanged("TimeAfterPreviousCompression");
                }
            }

            /// <summary>
            /// Number of the compression in the set. Useful for graphing.
            /// </summary>
            public int Number { get { return number; } set { if( this.number == value ) this.number = value; else { this.number = value; NotifyPropertyChanged("Number"); } } }

            /// <summary>
            /// Score is a value assigned to the compression based upon it's attributes.
            /// </summary>
            public double Score
            {
                get
                {
                    double armslockedscore = 0;
                    double shldrsovermanikinscore = 0;
                    double properhandplacementscore = 0;
                    double depthscore = 0;

                    // Right AND Left = 1; Right OR Left = 0.5; Neither = 0;
                    armslockedscore = rightArmLocked && leftArmLocked ? 1 : rightArmLocked || leftArmLocked ? 0.5 : 0;
                    shldrsovermanikinscore = rightShldrOverManikin && leftShldrOverManikin ? 1 : rightShldrOverManikin || leftShldrOverManikin ? 0.5 : 0;
                    properhandplacementscore = rightProperHandPlacement && leftProperHandPlacement ? 1 : rightProperHandPlacement || leftProperHandPlacement ? 0.5 : 0;

                    if (this.Depth >= Cpr.BAD_CPR_DEPTH_LOW)
                    {
                        if (this.Depth <= Cpr.BAD_CPR_DEPTH_HIGH)   // Depth is in good range.
                        {
                            depthscore = 1; // 100%
                        }
                        else    // Depth too high.
                        {
                            depthscore = 0; // 0% --> It's questionable whether or not to impact depth's actual score, since it was (probably) an invalid reading.
                        }
                    }
                    else
                    {
                        depthscore = System.Math.Max(0, this.Depth / Cpr.GOAL_CPR_DEPTH);    // Scale back depth score until it hits 0, based on the distance away from [goal] target.
                    }

                    return ( armslockedscore + shldrsovermanikinscore + properhandplacementscore + depthscore) / 4;
                }
            }

            /// <summary>
            /// Depth of the compression.
            /// </summary>
            public double Depth
            {
                get { return this.depth; }
                set
                {
                    if (this.depth != value)
                    {
                        this.depth = value;
                        this.NotifyPropertyChanged("Depth");
                        this.NotifyPropertyChanged("DepthInInches");    // Notify the change in DepthInInches too, since it's dependent of Depth.
                    }
                }
            }

            public double DepthInInches { get { return this.depth * 39.3701; } }

            /// <summary>
            /// Time the compression occured (finished).
            /// </summary>
            public DateTime Time
            {
                get { return this.time; }
                set
                {
                    if (this.time != value)
                    {
                        this.time = value;
                        this.NotifyPropertyChanged("Time");
                    }
                    else
                    {
                        this.time = value;
                    }
                }
            }

            /// <summary>
            /// Whether or not the both arms were locked during this compression.
            /// </summary>
            public bool ArmsLocked
            {
                get { return this.leftArmLocked && this.rightArmLocked; }
            }

            /// <summary>
            /// Whether or not the left arm was locked during this compression.
            /// </summary>
            public bool LeftArmLocked
            {
                get { return this.leftArmLocked; }
                set
                {
                    if (this.leftArmLocked != value)
                    {
                        this.leftArmLocked = value;
                        this.NotifyPropertyChanged("LeftArmLocked");

                        this.NotifyPropertyChanged("ArmsLocked");
                    }
                    else
                    {
                        this.leftArmLocked = value;
                    }
                }
            }

            /// <summary>
            /// Whether or not the right arm was locked during this compression.
            /// </summary>
            public bool RightArmLocked
            {
                get { return this.rightArmLocked; }
                set
                {
                    if (this.rightArmLocked != value)
                    {
                        this.rightArmLocked = value;
                        this.NotifyPropertyChanged("RightArmLocked");

                        this.NotifyPropertyChanged("ArmsLocked");
                    }
                    else
                    {
                        this.rightArmLocked = value;
                    }
                }
            }

            /// <summary>
            /// Whether or not both shoulders were above the manikin for this compression.
            /// </summary>
            public bool ShouldersOverManikin
            {
                get
                {
                    return this.rightShldrOverManikin && this.leftShldrOverManikin;
                }
            }

            /// <summary>
            /// Whether or not the left shoulder was above the manikin for this compression.
            /// </summary>
            public bool LeftShoulderOverManikin
            {
                get
                {
                    return this.leftShldrOverManikin;
                }
                set
                {
                    if (this.leftShldrOverManikin != value)
                    {
                        this.leftShldrOverManikin = value;
                        this.NotifyPropertyChanged("LeftShoulderOverManikin");

                        this.NotifyPropertyChanged("ShouldersOverManikin");
                    }
                    else
                    {
                        this.leftShldrOverManikin = value;
                    }
                }
            }

            /// <summary>
            /// Whether or not the right shoulder was above the manikin for this compression.
            /// </summary>
            public bool RightShoulderOverManikin
            {
                get
                {
                    return this.rightShldrOverManikin;
                }
                set
                {
                    if (this.rightShldrOverManikin != value)
                    {
                        this.rightShldrOverManikin = value;
                        this.NotifyPropertyChanged("RightShoulderOverManikin");

                        this.NotifyPropertyChanged("ShouldersOverManikin");
                    }
                    else
                    {
                        this.rightShldrOverManikin = value;
                    }
                }
            }

            /// <summary>
            /// Whether or not the compression had proper hand placement.
            /// </summary>
            public bool ProperHandPlacement
            {
                get { return this.rightProperHandPlacement && this.leftProperHandPlacement; }
            }

            /// <summary>
            /// Whether or not the left hand was properly placed during the compression.
            /// </summary>
            public bool LeftProperHandPlacement
            {
                get { return this.leftProperHandPlacement; }
                set
                {
                    if (this.leftProperHandPlacement != value)
                    {
                        this.leftProperHandPlacement = value;
                        this.NotifyPropertyChanged("LeftProperHandPlacement");

                        this.NotifyPropertyChanged("ProperHandPlacement");
                    }
                    else
                    {
                        this.leftProperHandPlacement = value;
                    }
                }
            }

            /// <summary>
            /// Whether or not the right hand was properly placed during the compression.
            /// </summary>
            public bool RightProperHandPlacement
            {
                get { return this.rightProperHandPlacement; }
                set
                {
                    if (this.rightProperHandPlacement != value)
                    {
                        this.rightProperHandPlacement = value;
                        this.NotifyPropertyChanged("RightProperHandPlacement");

                        this.NotifyPropertyChanged("ProperHandPlacement");
                    }
                    else
                    {
                        this.rightProperHandPlacement = value;
                    }
                }
            }

            public bool Valid
            {
                get { return Compression.IsValid(this); }
            }

            #endregion

            #region Constructor

            public Compression(double Depth)
            {
                this.depth = Depth;
                this.Time = DateTime.Now;
            }

            #endregion

            #region Public Functions

            public override string ToString()
            {
                return String.Format("Score: {0} Depth: {1} {2} Time: {3}", this.Score, this.Depth, this.DepthInInches, this.Time);
            }

            public static bool IsValid( Compression comp )
            {
                if (comp == null)
                    return false;

                if (comp.Depth == 0)    // 0 depth indicates a placeholder compression
                    return false;
                /*
                if (!comp.ArmsLocked)   // Locked arms are required to be valid, otherwise none of the information is reliable.
                    return false;
                 */

                /*
                if (!(comp.LeftArmLocked || comp.RightArmLocked))   // At least one of the arms was locked. Both unlocked arms isn't going to yeild effective results, but we can work with a crippled compression.
                    return false;*/

                // Dev Note: Do we want to include depths which aren't 0, but are out of range? Possibly...
                /*if (comp.Depth < Cpr.BAD_CPR_DEPTH_LOW || comp.Depth > Cpr.BAD_CPR_DEPTH_HIGH)
                    return false;*/

                if (comp.Depth > Cpr.BAD_CPR_DEPTH_HIGH)    // Measurements over the valid range.
                    return false;

                return true;
            }

            #endregion

            public event PropertyChangedEventHandler PropertyChanged;

            public void NotifyPropertyChanged(string propertyName)
            {
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        };

        /// <summary>
        /// Class which holds a set of Compression objects in a central location, and provides a centralized location for analyzing the current set of compressions.
        ///
        /// A CompressionSet includes:
        /// - The set number.
        /// - A KinectImage reflecting the set.
        /// - The
        /// </summary>
        public class CompressionSet : INotifyPropertyChanged
        {
            private int setNumber = 0;
            private int setLengthMiliseconds = 0;
            private ObservableRangeCollection<Compression> compressions = new ObservableRangeCollection<Compression>();
            private KinectImage image = null;
            private List<int> feedbackIDs = new List<int>();

            /// <summary>
            /// KinectImage which represents the compression set.
            /// </summary>
            public KinectImage Image
            {
                get { return image; }
                set
                {
                    if (image != value)
                    {
                        image = value;
                        this.NotifyPropertyChanged("Image");
                    }
                    else
                    {
                        image = value;
                    }
                }
            }

            /// <summary>
            /// Duration of the current set, and used in calculating the Reps per minute.
            /// </summary>
            public int SetLengthMiliseconds
            {
                get { return setLengthMiliseconds; }
                private set
                {
                    if (setLengthMiliseconds != value)
                    {
                        setLengthMiliseconds = value;
                        this.NotifyPropertyChanged("SetLengthMiliseconds");

                        // Because RepsPerMinute relies upon the SetLengthMiliseconds, recalculate RepsPerMinute whenever SetLengthMiliseconds is changed.
                        this.NotifyPropertyChanged("RepsPerMinute");
                    }
                    else
                    {
                        setLengthMiliseconds = value;
                    }
                }
            }

            /// <summary>
            /// Indicates the index of the compression set in a collection of CompressionSet objects.
            /// </summary>
            public int SetNumber
            {
                get { return setNumber; }
                private set
                {
                    if (setNumber != value)
                    {
                        setNumber = value;
                        this.NotifyPropertyChanged("SetNumber");
                    }
                    else
                    {
                        setNumber = value;
                    }
                }
            }

            /// <summary>
            /// Calculates the number of valid compressions currently stored in the compressions collection.
            /// </summary>
            public int ValidCompressionCount
            {
                get
                {
                    if (compressions != null && compressions.Count > 0)
                    {
                        int numValid = 0;

                        for (int i = 0; i < compressions.Count; i++)
                        {
                            if (Compression.IsValid(compressions[i]))
                                numValid++;
                        }

                        return numValid;
                    }

                    return 0;
                }
            }

            /// <summary>
            /// Returns the average depth of all valid compressions included in the compression set.
            /// </summary>
            public double AverageValidDepth
            {
                get
                {
                    if (this.compressions != null)
                    {
                        double depth = 0;
                        int numValid = 0;

                        for (int i = 0; i < compressions.Count; i++)
                        {
                            if (Compression.IsValid(compressions[i]))
                            {
                                depth += compressions[i].Depth;
                                numValid++;
                            }
                        }

                        if (numValid > 0)
                        {
                            return depth / numValid;    // Average depth of all valid compressions.
                        }
                    }

                    return 0;
                }
            }

            /// <summary>
            /// Returns the average depth in inches rather than meters for all valid compressions in the compression set.
            /// </summary>
            public double AverageValidDepthInches
            {
                get
                {
                    if (this.compressions != null)
                    {
                        double depth = 0;
                        int numValid = 0;

                        for (int i = 0; i < compressions.Count; i++)
                        {
                            if (Compression.IsValid(compressions[i]))
                            {
                                depth += compressions[i].DepthInInches;
                                numValid++;
                            }
                        }

                        if (numValid > 0)
                        {
                            return depth / numValid;    // Average depth of all valid compressions.
                        }
                    }

                    return 0;
                }
            }

            /// <summary>
            /// Returns the average score of all valid compressions in the compression set.
            /// </summary>
            public double AverageValidScore
            {
                get
                {
                    if (compressions != null)
                    {
                        double score = 0;
                        int numValid = 0;

                        for (int i = 0; i < compressions.Count; i++)
                        {
                            if (Compression.IsValid(compressions[i]))
                            {
                                score += compressions[i].Score;
                                numValid++;
                            }
                        }

                        if (numValid > 0)
                            return score / numValid;
                    }

                    return 0;
                }
            }

            /// <summary>
            /// Provides the average hand placement score for all valid compressions in the compression set.
            /// </summary>
            public double AverageValidHandPlacementScore
            {
                get
                {
                    if (compressions != null)
                    {
                        double score = 0;
                        int numValid = 0;

                        for (int i = 0; i < compressions.Count; i++)
                        {
                            if (Compression.IsValid(compressions[i]))
                            {
                                score += compressions[i].ProperHandPlacement ? 1 : compressions[i].LeftProperHandPlacement || compressions[i].RightProperHandPlacement ? 0.5 : 0;
                                numValid++;
                            }
                        }

                        if (numValid > 0)
                            return score / numValid;
                    }

                    return 0;
                }
            }

            /// <summary>
            /// Provides the average arms locked score for all valid compressions in the compression set.
            /// </summary>
            public double AverageValidArmsLockedScore
            {
                get
                {
                    if (compressions != null)
                    {
                        double score = 0;
                        int numValid = 0;

                        for (int i = 0; i < compressions.Count; i++)
                        {
                            if (Compression.IsValid(compressions[i]))
                            {
                                score += compressions[i].ArmsLocked ? 1 : compressions[i].LeftArmLocked || compressions[i].RightArmLocked ? 0.5 : 0;
                                numValid++;
                            }
                        }

                        if (numValid > 0)
                            return score / numValid;
                    }

                    return 0;
                }
            }

            /// <summary>
            /// Provides the average shoulders over manikin score for all valid compressions in the compression set.
            /// </summary>
            public double AverageValidShoulderOverManikinScore
            {
                get
                {
                    if (compressions != null)
                    {
                        double score = 0;
                        int numValid = 0;

                        for (int i = 0; i < compressions.Count; i++)
                        {
                            if (Compression.IsValid(compressions[i]))
                            {
                                score += compressions[i].ShouldersOverManikin ? 1 : compressions[i].LeftShoulderOverManikin || compressions[i].RightShoulderOverManikin ? 0.5 : 0;
                                numValid++;
                            }
                        }

                        if (numValid > 0)
                            return score / numValid;
                    }

                    return 0;
                }
            }

            /// <summary>
            /// Calculates the reps per minute proportionate to the current set duration, and the number of valid compressions in the compression set.
            /// </summary>
            public double RepsPerMinute
            {
                get
                {
                    try
                    {
                        return (60000 / (double)this.setLengthMiliseconds) * this.ValidCompressionCount;
                    }
                    catch
                    {
                    }

                    return 0;
                }
            }

            /// <summary>
            /// Exposed getter for the Set's collection of Compression objects.
            /// </summary>
            public ObservableRangeCollection<Compression> Compressions { get { return compressions; } }

            /// <summary>
            /// Exposed getter for the Set's collection of feedback IDs.
            /// </summary>
            public List<int> FeedbackIDs { get { return feedbackIDs; } }

            public CompressionSet(int setNumber, int setLengthMS)
            {
                this.SetNumber = setNumber;
                this.SetLengthMiliseconds = setLengthMS;
            }

            /// <summary>
            /// Used to replace an entire set of compressions.
            /// </summary>
            /// <param name="compressions"></param>
            public void SetCompressions(ObservableRangeCollection<Compression> compressions)
            {
                this.compressions = compressions;

                this.NotifyAllPropertiesChanged();
            }

            public void AddCompression(Compression compression)
            {
                if (compressions == null)
                    compressions = new ObservableRangeCollection<Compression>();

                if (compression != null)
                {
                    compression.Number = compressions.Count;    // give it a number (it's index in the set)

                    // Ensure that only one compression is being added.
                    if (this.compressions.Count > 0)
                    {
                        Compression previous = this.compressions[this.compressions.Count - 1];

                        if (compression.Time.Subtract(previous.Time) < TimeSpan.FromMilliseconds(25))
                        {
                            // compare the two and keep the better one.
                            if (compression.Score > previous.Score)
                            {
                                compression.Number = previous.Number;   // use the previous number since it's being removed.

                                // remove the worse one and add this one.
                                this.compressions.Remove(previous);
                                this.compressions.Add(compression);

                                System.Diagnostics.Debug.WriteLine("Replacing previous compression with current one");
                            }

                            // else, don't add new compression, and keep previous.
                            System.Diagnostics.Debug.WriteLine("Dumping new compression and keeping previous one.");
                        }
                        else
                        {
                            this.compressions.Add(compression);
                        }
                    }
                    else
                    {
                        this.compressions.Add(compression);
                    }
                }

                this.NotifyAllPropertiesChanged();
            }

            public void ClearCompressions()
            {
                if (this.compressions != null)
                    this.compressions.Clear();
                else this.compressions = new ObservableRangeCollection<Compression>();

                this.NotifyPropertyChanged("Compressions");
                this.NotifyAllPropertiesChanged();
            }

            private void NotifyAllPropertiesChanged()
            {
                this.NotifyPropertyChanged("ValidCompressionCount");
                this.NotifyPropertyChanged("AverageValidDepth");
                this.NotifyPropertyChanged("AverageValidDepthInches");
                this.NotifyPropertyChanged("AverageValidScore");
                this.NotifyPropertyChanged("AverageValidHandPlacementScore");
                this.NotifyPropertyChanged("AverageValidShoulderOverManikinScore");
                this.NotifyPropertyChanged("AverageValidArmsLockedScore");
                this.NotifyPropertyChanged("RepsPerMinute");
            }

            public event PropertyChangedEventHandler PropertyChanged;

            public void NotifyPropertyChanged(string propertyName)
            {
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        /// <summary>
        /// Pieces of a compression. Used to build a Compression.
        /// </summary>
        public class CompressionPart : INotifyPropertyChanged
        {
            #region Private Fields

            private double? leftCompressDepth = null;
            private double? rightCompressDepth = null;
            private double? leftDecompressDepth = null;
            private double? rightDecompressDepth = null;

            private int totalFrames = 0;    // How many frames has this part been active/dumped-into (potential Count)

            private int leftProperHandPlacementCount = 0;   // Of the totalFrames, how many of those had proper left hand placement?
            private int rightProperHandPlacementCount = 0;   // Of the totalFrames, how many of those had proper right hand placement?
            private int leftShoulderOverManikinCount = 0;  // Of the totalFrames, how many of those had left shoulder over the manikin?
            private int rightShoulderOverManikinCount = 0;  // Of the totalFrames, how many of those had right shoulder over the manikin?
            private int leftArmLockedCount = 0;    // Of the totalFrames, how many of those had left arm locked?
            private int rightArmLockedCount = 0;    // Of the totalFrames, how many of those had right arm locked?

            private CompressionState compressionState = CompressionState.None;

            #endregion

            #region Public Properties

            /// <summary>
            /// Get or Set the lower part, in Y, of a compression on the left side.
            /// </summary>
            public double? LeftCompressDepth
            {
                get { return this.leftCompressDepth; }
                set
                {
                    if (this.leftCompressDepth != value)
                    {
                        this.leftCompressDepth = value;
                        NotifyPropertyChanged("LeftCompressDepth");

                        // Because LeftDepth relies upon LeftCompressDepth, recalculate LeftDepth.
                        NotifyPropertyChanged("LeftDepth");
                        // Because Depth relies upon LeftDepth, recalculate Depth.
                        NotifyPropertyChanged("Depth");

                        NotifyPropertyChanged("RightLeftMinDepth");
                        NotifyPropertyChanged("RightLeftMaxDepth");
                        NotifyPropertyChanged("RightLeftAvgDepth");
                    }
                    else
                    {
                        this.leftCompressDepth = value;
                    }
                }
            }
            /// <summary>
            /// Get or Set the lower part, in Y, of a compression on the right side.
            /// </summary>
            public double? RightCompressDepth
            {
                get { return this.rightCompressDepth; }
                set
                {
                    if (this.rightCompressDepth != value)
                    {
                        this.rightCompressDepth = value;
                        NotifyPropertyChanged("RightCompressDepth");

                        // Because RightDepth relies upon RightCompressDepth, recalculate RightDepth.
                        NotifyPropertyChanged("RightDepth");
                        // Because Depth relies upon RightDepth, recalculate Depth.
                        NotifyPropertyChanged("Depth");

                        NotifyPropertyChanged("RightLeftMinDepth");
                        NotifyPropertyChanged("RightLeftMaxDepth");
                        NotifyPropertyChanged("RightLeftAvgDepth");
                    }
                    else
                    {
                        this.rightCompressDepth = value;
                    }
                }
            }
            /// <summary>
            /// Get or Set the higher part, in Y, of a compression on the left side.
            /// </summary>
            public double? LeftDecompressDepth
            {
                get { return this.leftDecompressDepth; }
                set
                {
                    if (this.leftDecompressDepth != value)
                    {
                        this.leftDecompressDepth = value;
                        NotifyPropertyChanged("LeftDecompressDepth");

                        // Because LeftDepth relies upon LeftDecompressDepth, recalculate LeftDepth.
                        NotifyPropertyChanged("LeftDepth");
                        // Because Depth relies upon LeftDepth, recalculate Depth.
                        NotifyPropertyChanged("Depth");

                        NotifyPropertyChanged("RightLeftMinDepth");
                        NotifyPropertyChanged("RightLeftMaxDepth");
                        NotifyPropertyChanged("RightLeftAvgDepth");
                    }
                    else
                    {
                        this.leftDecompressDepth = value;
                    }
                }
            }
            /// <summary>
            /// Get or Set the higher part, in Y, of a compression on the right side.
            /// </summary>
            public double? RightDecompressDepth
            {
                get { return this.rightDecompressDepth; }
                set
                {
                    if (this.rightDecompressDepth != value)
                    {
                        this.rightDecompressDepth = value;
                        NotifyPropertyChanged("RightDecompressDepth");

                        // Because RightDepth relies upon RightDecompressDepth, recalculate RightDepth.
                        NotifyPropertyChanged("RightDepth");
                        // Because Depth relies upon RightDepth, recalculate Depth.
                        NotifyPropertyChanged("Depth");

                        NotifyPropertyChanged("RightLeftMinDepth");
                        NotifyPropertyChanged("RightLeftMaxDepth");
                        NotifyPropertyChanged("RightLeftAvgDepth");
                    }
                    else
                    {
                        this.rightDecompressDepth = value;
                    }
                }
            }

            public double? LeftDepth
            {
                get
                {
                    if (this.leftDecompressDepth.HasValue && this.leftCompressDepth.HasValue)
                        return System.Math.Abs(this.leftDecompressDepth.Value - this.leftCompressDepth.Value);

                    else
                        return null;
                }
            }

            /// <summary>
            /// Depth of compression experienced by the right shoulder deduced from compression and decompression depths.
            /// </summary>
            public double? RightDepth
            {
                get
                {
                    if (this.rightDecompressDepth.HasValue && this.rightCompressDepth.HasValue)
                        return System.Math.Abs(this.rightDecompressDepth.Value - this.rightCompressDepth.Value);

                    else
                        return null;
                }
            }

            /// <summary>
            /// Get the depth of a compression deduced from the minimum depth experienced between 
            /// the left and right sides. ( Minimum gives most accurate results )
            /// </summary>
            public double? Depth
            {
                get
                {
                    if (this.RightDepth.HasValue && this.LeftDepth.HasValue)
                        return System.Math.Min(this.RightDepth.Value, this.LeftDepth.Value);

                    else
                    {
                        if (this.RightDepth.HasValue)
                            return RightDepth.Value;

                        else if (this.LeftDepth.HasValue)
                            return LeftDepth.Value;

                        else
                            return null;
                    }
                }
            }

            /// <summary>
            /// Get the average depth between the right and left sides.
            /// </summary>
            public double? RightLeftAvgDepth
            {
                get
                {
                    if (this.RightDepth.HasValue && this.LeftDepth.HasValue)
                        return this.RightDepth.Value + this.LeftDepth.Value / 2;

                    else
                    {
                        if (this.RightDepth.HasValue)
                            return RightDepth.Value;

                        else if (this.LeftDepth.HasValue)
                            return LeftDepth.Value;

                        else
                            return null;
                    }
                }
            }

            /// <summary>
            /// Get the minimum depth between the right and left sides.
            /// </summary>
            public double? RightLeftMinDepth
            {
                get
                {
                    if (this.RightDepth.HasValue && this.LeftDepth.HasValue)
                        return System.Math.Min( this.RightDepth.Value, this.LeftDepth.Value );

                    else
                    {
                        if (this.RightDepth.HasValue)
                            return RightDepth.Value;

                        else if (this.LeftDepth.HasValue)
                            return LeftDepth.Value;

                        else
                            return null;
                    }
                }
            }

            /// <summary>
            /// Get the maximum depth between the right and left sides.
            /// </summary>
            public double? RightLeftMaxDepth
            {
                get
                {
                    if (this.RightDepth.HasValue && this.LeftDepth.HasValue)
                        return System.Math.Max(this.RightDepth.Value, this.LeftDepth.Value);

                    else
                    {
                        if (this.RightDepth.HasValue)
                            return RightDepth.Value;

                        else if (this.LeftDepth.HasValue)
                            return LeftDepth.Value;

                        else
                            return null;
                    }
                }
            }

            /// <summary>
            /// How many frames has this part been active/dumped-into.
            /// </summary>
            public int TotalFrames
            {
                get { return totalFrames; }
                set
                {
                    if (totalFrames != value)
                    {
                        totalFrames = value;
                        NotifyPropertyChanged("TotalFrames");

                        // Recalculate percentages
                        NotifyPropertyChanged("ProperHandPlacementPercent");
                        NotifyPropertyChanged("ShouldersOverManikinPercent");
                        NotifyPropertyChanged("ArmsLockedPercent");
                    }
                    else
                    {
                        totalFrames = value;
                    }

                }
            }

            /// <summary>
            /// How many of the TotalFrames had proper left hand placement.
            /// </summary>
            public int LeftProperHandPlacementCount
            {
                get { return leftProperHandPlacementCount; }
                set
                {
                    if (leftProperHandPlacementCount != value)
                    {
                        leftProperHandPlacementCount = value;
                        NotifyPropertyChanged("LeftProperHandPlacementCount");

                        // Recalculate percentage
                        NotifyPropertyChanged("LeftProperHandPlacementPercent");
                    }
                    else
                    {
                        leftProperHandPlacementCount = value;
                    }

                }
            }

            /// <summary>
            /// How many of the TotalFrames had proper right hand placement.
            /// </summary>
            public int RightProperHandPlacementCount
            {
                get { return rightProperHandPlacementCount; }
                set
                {
                    if (rightProperHandPlacementCount != value)
                    {
                        rightProperHandPlacementCount = value;
                        NotifyPropertyChanged("RightProperHandPlacementCount");

                        // Recalculate percentage
                        NotifyPropertyChanged("RightProperHandPlacementPercent");
                    }
                    else
                    {
                        rightProperHandPlacementCount = value;
                    }

                }
            }

            /// <summary>
            /// How many of the TotalFrames had left shoulder over the manikin.
            /// </summary>
            public int LeftShoulderOverManikinCount
            {
                get { return leftShoulderOverManikinCount; }
                set
                {
                    if (leftShoulderOverManikinCount != value)
                    {
                        leftShoulderOverManikinCount = value;
                        NotifyPropertyChanged("LeftShoulderOverManikinCount");

                        // Recalculate percentage
                        NotifyPropertyChanged("LeftShoulderOverManikinPercent");
                    }
                    else
                    {
                        leftShoulderOverManikinCount = value;
                    }

                }
            }

            /// <summary>
            /// How many of the TotalFrames had right shoulder over the manikin.
            /// </summary>
            public int RightShoulderOverManikinCount
            {
                get { return rightShoulderOverManikinCount; }
                set
                {
                    if (rightShoulderOverManikinCount != value)
                    {
                        rightShoulderOverManikinCount = value;
                        NotifyPropertyChanged("RightShoulderOverManikinCount");

                        // Recalculate percentage
                        NotifyPropertyChanged("RightShoulderOverManikinPercent");
                    }
                    else
                    {
                        rightShoulderOverManikinCount = value;
                    }

                }
            }

            /// <summary>
            /// How many of the TotalFrames had left arm locked.
            /// </summary>
            public int LeftArmLockedCount
            {
                get { return leftArmLockedCount; }
                set
                {
                    if (leftArmLockedCount != value)
                    {
                        leftArmLockedCount = value;
                        NotifyPropertyChanged("LeftArmLockedCount");

                        // Recalculate percentage
                        NotifyPropertyChanged("LeftArmLockedPercent");
                    }
                    else
                    {
                        leftArmLockedCount = value;
                    }

                }
            }

            /// <summary>
            /// How many of the TotalFrames had right arm locked.
            /// </summary>
            public int RightArmLockedCount
            {
                get { return rightArmLockedCount; }
                set
                {
                    if (rightArmLockedCount != value)
                    {
                        rightArmLockedCount = value;
                        NotifyPropertyChanged("RightArmLockedCount");

                        // Recalculate percentage
                        NotifyPropertyChanged("RightArmLockedPercent");
                    }
                    else
                    {
                        rightArmLockedCount = value;
                    }

                }
            }

            /// <summary>
            /// How many of the TotalFrames had proper left hand placement.
            /// </summary>
            public double LeftProperHandPlacementPercent { get { return totalFrames > 0 ? (double)leftProperHandPlacementCount / totalFrames : 0; } }
            /// <summary>
            /// How many of the TotalFrames had proper right hand placement.
            /// </summary>
            public double RightProperHandPlacementPercent { get { return totalFrames > 0 ? (double)rightProperHandPlacementCount / totalFrames : 0; } }
            /// <summary>
            /// How many of the TotalFrames had left shoulder over the manikin.
            /// </summary>
            public double LeftShoulderOverManikinPercent { get { return totalFrames > 0 ? (double)leftShoulderOverManikinCount / totalFrames : 0; } }
            /// <summary>
            /// How many of the TotalFrames had right shoulder over the manikin.
            /// </summary>
            public double RightShoulderOverManikinPercent { get { return totalFrames > 0 ? (double)rightShoulderOverManikinCount / totalFrames : 0; } }
            /// <summary>
            /// How many of the TotalFrames had left arm locked.
            /// </summary>
            public double LeftArmLockedPercent { get { return totalFrames > 0 ? (double)leftArmLockedCount / totalFrames : 0; } }
            /// <summary>
            /// How many of the TotalFrames had right arm locked.
            /// </summary>
            public double RightArmLockedPercent { get { return totalFrames > 0 ? (double)rightArmLockedCount / totalFrames : 0; } }

            public CompressionState CompressionState { get { return compressionState; } set { if (compressionState == value) compressionState = value; else { compressionState = value; NotifyPropertyChanged("CompressionState"); } } }
            #endregion

            #region Public Functions

            /// <summary>
            /// Clear all values.
            /// </summary>
            public void Clear()
            {
                /* We clear using the properties so that the NotifyPropertyChanged events will fire, thus updating anything bound to these values. */
                this.LeftCompressDepth = null;
                this.RightCompressDepth = null;
                this.LeftDecompressDepth = null;
                this.RightDecompressDepth = null;

                this.LeftArmLockedCount = 0;
                this.RightArmLockedCount = 0;
                this.LeftProperHandPlacementCount = 0;
                this.RightProperHandPlacementCount = 0;
                this.LeftShoulderOverManikinCount = 0;
                this.RightShoulderOverManikinCount = 0;

                this.TotalFrames = 0;

                this.CompressionState = CprCompressions.CompressionState.None;
            }

            #endregion

            public event PropertyChangedEventHandler PropertyChanged;

            public void NotifyPropertyChanged(string propertyName)
            {
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        };
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
