﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Microsoft.Kinect;
using System.Windows;
using System.Windows.Media.Media3D;
using AIMS.Assets.Lessons2;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using AIMS.Tasks;

namespace AIMS.Tasks.Cpr
{
    public class CprFindColors : Stage
    {
        #region Stage Fields / Properties

        private Cpr lesson;

        private Body stickySkeleton;

        // Zone for the whole screen :O
        private RecZone zoneScreen;

        // Calibration Stage Watchers
        /// <summary>
        /// Watches for the left hand to be above the manikinSideLocation an in-line with the manikinFaceLocation.
        /// </summary>
        private Watcher LookForColorsWatcher;

        #endregion

        // Constructor
        public CprFindColors( Cpr lesson )
        {
            this.lesson = lesson;

            this.CurrentStageWatchers = new List<BaseWatcher>();
            this.PeripheralStageWatchers = new List<BaseWatcher>();
            this.OutlyingStageWatchers = new List<BaseWatcher>();

            Name = "Finding Colors!";
            Instruction = "Move Colors Around!";
            TargetTime = TimeSpan.MaxValue; // No target time for calibration...
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Tasks/Cpr/Graphics/CPR_Calibration.png";
            ScoreWeight = 0;    // Calibration stage is not weighted
            Zones = new List<Zone>(10);
            Stopwatch = new System.Diagnostics.Stopwatch();
            TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.CanTimeOut = false;
        }

        /// <summary>
        /// Initializes most of the CprCalibrationStage Fields. Rigs everything together.
        /// </summary>
        public override void SetupStage()
        {
            try
            {
                // Update CprPage for this Stage
                this.lesson.CprPage.StageInfoTextBlock.Text = this.Name;
                this.lesson.CprPage.InstructionTextBlock.Text = this.Instruction;
                this.lesson.CprPage.CurrentStageImage.Source = new BitmapImage(new Uri(this.ImagePath, UriKind.Relative));
                this.lesson.CprPage.zoneOverlay.ClearZones();

                // Create Zones and Watchers
                this.SetupZones();
                this.CreateStageWatchers();

                // Reset Timers / Clocks / Watches
                this.StartTime = DateTime.Now;
                this.EndTime = DateTime.MaxValue;
                this.Stopwatch.Reset();
                this.Stopwatch.Start();
                this.TimeoutStopwatch.Reset();
                this.TimeoutStopwatch.Start();

                // Reset StageScore
                this.StageScore.WipeClean();
                this.StageScore = null;
                this.StageScore = new StageScore();

                // Done setting up!
                this.IsSetup = true;
                this.Complete = false;
            }
            catch (Exception exc)
            {
                Debug.Print("CprCalibrationStage SetupStage() ERROR: {0}", exc.Message);
            }
        }

        /// <summary>
        /// Sets default values for zones.
        /// </summary>
        private void SetupZones()
        {
            this.lesson.CprPage.zoneOverlay.ClearZones();
            this.Zones.Clear();

            // Setup zones to look for manikin face and side locations around left hand.
            this.zoneScreen = new RecZone() 
            {
                Width = this.lesson.CprPage.display.ActualWidth * 0.75, // pixels
                Height = this.lesson.CprPage.display.ActualHeight * 0.65, // pixels
                CenterX = this.lesson.CprPage.display.ActualWidth * 0.50,
                CenterY = this.lesson.CprPage.display.ActualHeight * 0.65,
                IsImproperZone = false,
                DoesTrackJoint = false,
                Active = true,
                Name = "zoneScreen"
            };
            this.Zones.Add(this.zoneScreen);

            // Add zones to the lesson page zone overlay.
            foreach (Zone zone in this.Zones)
                this.lesson.CprPage.zoneOverlay.AddZone(zone);
        }

        /// <summary>
        /// Create watcher logic for watchers within this stage.
        /// </summary>
        private void CreateStageWatchers()
        {
            // Clear the list of watchers.
            this.CurrentStageWatchers.Clear();
            this.PeripheralStageWatchers.Clear();
            this.OutlyingStageWatchers.Clear();

            #region LeftHandCalibratingWatcher
            this.LookForColorsWatcher = new Watcher();
            this.LookForColorsWatcher.CheckForActionDelegate = () => 
            {
                if (this.Complete)
                    return;

                // Different pieces of tape, hopefully found, around the hand. ( expecting 2 pieces to be found, but checking both sides 
                // of the manikin for chin ).
                Point Tape = ColorTracker.find(ColorTracker.PinkTape, zoneScreen.Rectangle, KinectManager.MappedColorData);

                // Not triggered... unless triggered.
                this.LookForColorsWatcher.IsTriggered = false;

                // Check for colored tapes within zone.
                if (!Double.IsNaN(Tape.X) && !Double.IsNaN(Tape.Y))
                {
                    this.zoneScreen.CheckPointWithinZone(Tape);
                }
                else
                    this.zoneScreen.IsWithinZone = false;
            };
            this.CurrentStageWatchers.Add(this.LookForColorsWatcher);   // Add the watcher to the list of watchers

            #endregion
        }

        /// <summary>
        /// Should be called every Frame. Updates all of the stage logic
        /// </summary>
        public override void UpdateStage()
        {
            if (!IsSetup)
                SetupStage();

            if (KinectManager.DepthWidth == 0)
                return;

            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
                this.OnStageTimeout();

            // Important so when sticky skeleton isn't found in frame, don't use last occurance of sticky skeleton.
            this.stickySkeleton = null;
            // Use first occurance found of a sticky skeleton in the StickySkeletonIDs array.
            if (KinectManager.NumStickySkeletons == 1)
            {
                foreach (ulong? stickySkelId in KinectManager.StickySkeletonIDs)
                {
                    if (stickySkelId.HasValue)
                    {
                        this.stickySkeleton = KinectManager.GetSkeleton(stickySkelId.Value, true);
                        break;
                    }
                }
            }

            // Check if the stage completed.
            if (this.stickySkeleton != null && this.LookForColorsWatcher.IsTriggered)
            {
                this.EndStage();
            }
        }

        // Placeholder until this function signature is removed from Stage class.
        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            this.UpdateWatcherStates();
        }

        public void UpdateWatcherStates()
        {
            // Only run if stage is setup
            if (!this.IsSetup)
                return;

            int difference = System.Math.Abs(this.lesson.CurrentStageIndex - this.StageNumber);

            // Update Current Watchers
            if (difference == 0)
            {
                foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                {
                    watcher.UpdateWatcher();
                }
            }
            // Update Peripheral Watchers
            if (difference == 1)
            {
                foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                {
                    watcher.UpdateWatcher();
                }
            }
            // Update Outlier Watchers
            if (difference > 1)
            {
                foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                {
                    watcher.UpdateWatcher();
                }
            }
        }

        public override void EndStage( bool playSound = true )
        {
            Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            this.EndTime = DateTime.Now;

            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                StageScore ss = (StageScore)args.Argument;

                ss.ScreenShot = KinectManager.CreateScreenshot();
                // Set the hasScreenshot variable if the screenshot was created successfully.
                if (ss.ScreenShot == null)
                    ss.HasScreenshot = false;
                else ss.HasScreenshot = true;
            };

            worker.RunWorkerAsync(this.StageScore);

            this.StageScore.StageNumber = this.StageNumber;
            this.StageScore.StageName = this.Name;
            this.StageScore.Score = 1.00;

            // store how much time was spent in this stage.
            this.StageScore.TimeInStage = TimeSpan.Zero;

            this.StageScore.Target = this.Instruction;

            this.lesson.Result.StageScores.Add(this.StageScore);

            if( playSound ) lesson.PlayNextStageSound();

            this.lesson.Running = true;

            this.Complete = true;
            this.lesson.StageComplete(this);
        }

        public override void PauseStage()
        {
            this.lesson.Paused = true;
            this.lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.lesson.Paused = false;
            this.lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void ResetStage()
        {
            this.WipeClean();
            this.SetupStage();
        }

        #region Action Functions

        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            //this.PauseStage();
            this.TimedOut = true;
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            this.RequestStageVideo();   // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
        }

        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.lesson.CprPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Cpr/Videos/00_Calibration.wmv", UriKind.Relative));
            this.lesson.CprPage.VideoPlayer.startVideo(this, new RoutedEventArgs());

            this.lesson.CprPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.lesson.CprPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.lesson.CprPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.lesson.CprPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.lesson.CprPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.lesson.CprPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        #endregion

        public override void WipeClean()
        {
            this.IsSetup = false;
            this.Complete = false;
            this.TimedOut = false;
        }
    }
}
