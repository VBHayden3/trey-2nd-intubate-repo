﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace AIMS.Tasks.Cpr
{
    /// <summary>
    /// Result object for the CPR task.
    /// </summary>
    public class CprResult : AIMS.Tasks.TaskResult, INotifyPropertyChanged
    {
        #region Private Constants

        private const string OVERALL_RESULTS_TABLE_NAME = "taskresults";
        private const string CPR_RESULTS_TABLE_NAME = "taskresults_cpr";
        private const string CPR_COMPRESSIONS_RESULTS_TABLE_NAME = "taskresults_cpr_compressions";
        private const string IMAGES_TABLE_NAME = "kinect_images";

        #endregion

        #region Private Fields

        // CprCalibration Stage Fields
        private int? calibrationImageID = null;
        private KinectImage calibrationImage = null;

        // CprVocalInteractions Stage Fields
        private bool askedForAED = false;
        private bool askedFor911 = false;
        private bool checkedResponsiveness = false;
        private double vocalInteractionScore = 0;

        // CprCheckPulse Stage Fields
        private int? checkPulseImageID = null;
        private KinectImage checkPulseImage = null;
        private CheckPulse checkedPulse = CheckPulse.No;
        private double checkPulseScore = 0;
        private TimeSpan checkPulseDuration = TimeSpan.Zero;

        // CprCompressions Stage Fields
        private ObservableRangeCollection<CprCompressionSetResult> compressionSets = new ObservableRangeCollection<CprCompressionSetResult>();
        private double avgDepth = 0;
        private double avgRate = 0;
        private double avgBodyMech = 0;

        // Feedback List
        private List<int> overallFeedback = new List<int>();

        #endregion

        #region Public Properties

        /// <summary>
        /// ID for the Calibration Image in the DB. Should only get set when retrieving from the DB. Null if image was not found.
        /// </summary>
        public int? CalibrationImageID { get { return this.calibrationImageID; } set { if (this.calibrationImageID == value) this.calibrationImageID = value; else { this.calibrationImageID = value; NotifyPropertyChanged("CalibrationImageID"); } } }
        /// <summary>
        /// KinectImage image for the Calibration Stage.
        /// </summary>
        public KinectImage CalibrationImage { get { return this.calibrationImage; } set { if (this.calibrationImage == value) this.calibrationImage = value; else { this.calibrationImage = value; NotifyPropertyChanged("CalibrationImage"); } } }
        /// <summary>
        /// Whether or not user asked for an AED in the Vocal Interactions stage.
        /// </summary>
        public bool AskedForAED { get { return this.askedForAED; } set { if (this.askedForAED == value) this.askedForAED = value; else { this.askedForAED = value; NotifyPropertyChanged("AskedForAED"); } } }
        /// <summary>
        /// Whether or not user asked for 911, or help, in the VocalInteractions stage.
        /// </summary>
        public bool AskedFor911 { get { return this.askedFor911; } set { if (this.askedFor911) this.askedFor911 = value; else { this.askedFor911 = value; NotifyPropertyChanged("AskedFor911"); } } }
        /// <summary>
        /// Whether or not the user checked the patient's responsiveness in the Vocal Interactions stage.
        /// </summary>
        public bool CheckedResponsiveness { get { return this.checkedResponsiveness; } set { if (this.checkedResponsiveness == value) this.checkedResponsiveness = value; else { this.checkedResponsiveness = value; NotifyPropertyChanged("CheckedResponsiveness"); } } }
        /// <summary>
        /// Score for the VocalInteraction stage.
        /// </summary>
        public double VocalInteractionScore { get { return this.vocalInteractionScore; } set { if (this.vocalInteractionScore == value) this.vocalInteractionScore = Double.IsNaN(value) ? 0 : value; else { this.vocalInteractionScore = Double.IsNaN(value) ? 0 : value; NotifyPropertyChanged("VocalInteractionScore"); } } }
        /// <summary>
        /// ID for the CheckPulse Image in the DB. Should only get set when retrieving from the DB. Null if image was not found.
        /// </summary>
        public int? CheckPulseImageID { get { return this.checkPulseImageID; } set { if (this.checkPulseImageID == value) this.checkPulseImageID = value; else { this.checkPulseImageID = value; NotifyPropertyChanged("CheckPulseImageID"); } } }
        /// <summary>
        /// KinectImage image for the CheckPulse Stage.
        /// </summary>
        public KinectImage CheckPulseImage { get { return this.checkPulseImage; } set { if (this.checkPulseImage == value) this.checkPulseImage = value; else { this.checkPulseImage = value; NotifyPropertyChanged("CheckPulseImage"); } } }
        /// <summary>
        /// CheckPulse Enumerator value defining how the pulse was checked.
        /// </summary>
        public CheckPulse CheckedPulse { get { return this.checkedPulse; } set { if (this.checkedPulse == value) this.checkedPulse = value; else { this.checkedPulse = value; NotifyPropertyChanged("CheckedPulse"); } } }
        /// <summary>
        /// Score for the CheckPulse stage.
        /// </summary>
        public double CheckPulseScore { get { return this.checkPulseScore; } set { if (this.checkPulseScore == value) this.checkPulseScore = double.IsNaN(value) ? 0 : value; else this.checkPulseScore = double.IsNaN(value) ? 0 : value; NotifyPropertyChanged("CheckPulseScore"); } }
        /// <summary>
        /// Amount of time the pulse was checked for.
        /// </summary>
        public TimeSpan CheckPulseDuration { get { return this.checkPulseDuration; } set { if (this.checkPulseDuration == value) this.checkPulseDuration = value; else { this.checkPulseDuration = value; NotifyPropertyChanged("CheckPulseDuration"); } } }
        /// <summary>
        /// List of CprResults.CompressionSet objects. Each Set contains details for a sub-set of compressions performed
        /// during the Compressions Stage. The Set is constrained by time, the duration of which is defined during initialization
        /// of the Compressions Stage; the amount of Sets collected are also defined during initialization.
        /// </summary>
        public ObservableRangeCollection<CprCompressionSetResult> CompressionSets { get { return this.compressionSets; } }
        /// <summary>
        /// Amount of CompressionSets.
        /// </summary>
        public int CompressionSetCount { get { return this.compressionSets.Count; } }
        /// <summary>
        /// Score of the Total Compression Depths for the Compression stage.
        /// </summary>
        public double AverageDepth { get { return this.avgDepth; } set { if (this.avgDepth == value) this.avgDepth = Double.IsNaN(value) ? 0 : value; else { this.avgDepth = Double.IsNaN(value) ? 0 : value; NotifyPropertyChanged("AverageDepth"); NotifyPropertyChanged("AverageDepthInches"); } } }
        public double AverageDepthInches { get { return this.avgDepth * 39.3700787; } }
        /// <summary>
        /// Score of the Total Compression Rate for the Compression stage.
        /// </summary>
        public double AverageRate { get { return this.avgRate; } set { if (this.avgRate == value) this.avgRate = Double.IsNaN(value) ? 0 : value; else { this.avgRate = Double.IsNaN(value) ? 0 : value; NotifyPropertyChanged("AverageRate"); } } }
        /// <summary>
        /// Score of the Total Body Mechanics for the Compression stage.
        /// </summary>
        public double AverageBodyMech { get { return this.avgBodyMech; } set { if (this.avgBodyMech == value) this.avgBodyMech = Double.IsNaN(value) ? 0 : value; else { this.avgBodyMech = Double.IsNaN(value) ? 0 : value; NotifyPropertyChanged("AverageBodyMech"); } } }
        /// <summary>
        /// List of overall feedback ID's.
        /// </summary>
        public List<int> OverallFeedback { get { return this.overallFeedback; } set { this.overallFeedback = value; } }

        public TaskFeedback[] OverallFeedbackValues { get { return TaskFeedback.GetFeedbackFromIDs( this.overallFeedback, Task.CPR ); } }

        #endregion

        public CprResult()
            : base()
        {
            this.TaskType = Task.CPR;
        }

        #region Public Functions

        /// <summary>
        /// Adds a CprCompressionSetResult to the list of CompressionSets.
        /// </summary>
        /// <param name="set">CprCompressionSetResult to add.</param>
        public void AddCompressionSet(CprCompressionSetResult set)
        {
            this.compressionSets.Add(set);
        }

        /// <summary>
        /// Removes a CompressionSet from the list of CompressionSets.
        /// </summary>
        /// <param name="index">Index of where to remove the CompressionSet.</param>
        public void RemoveCompressionSet(int index)
        {
            this.compressionSets.RemoveAt(index);
        }

        #region Database Interaction Functions

        /// <summary>
        /// Stores the CprResult in the database.
        /// </summary>
        /// <param name="result">Object to be stored.</param>
        /// <returns></returns>
        public static int StoreResult(CprResult result) 
        {
            int id = 0;

            if (result == null)
                return id;

            // Submit the overall result information to the overall results table - returning it's generated uid.
            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "INSERT INTO " + OVERALL_RESULTS_TABLE_NAME +
                " ( studentid, assignmentid, tasktype, masterylevel, " +
                "taskmode, score, starttime, endtime ) " +
                "VALUES ( :studentid, :assignmentid, CAST(:tasktype as \"TaskType\"), CAST(:masterylevel as \"MasteryLevel\"), " +
                "CAST(:taskmode as \"TaskMode\"), :score, :starttime, :endtime ) RETURNING uid;";
            command.Parameters.Add("studentid", NpgsqlDbType.Integer).Value = result.StudentID;
            command.Parameters.Add("assignmentid", NpgsqlDbType.Integer).Value = result.AssignmentID;
            command.Parameters.Add("tasktype", NpgsqlDbType.Text).Value = result.TaskType.ToString();
            command.Parameters.Add("masterylevel", NpgsqlDbType.Text).Value = result.MasteryLevel.ToString();
            command.Parameters.Add("taskmode", NpgsqlDbType.Text).Value = result.TaskMode.ToString();
            command.Parameters.Add("score", NpgsqlDbType.Double).Value = result.Score;
            command.Parameters.Add("starttime", NpgsqlDbType.Timestamp).Value = result.StartTime;
            command.Parameters.Add("endtime", NpgsqlDbType.Timestamp).Value = result.EndTime;
            try
            {
                // Execute the query, attempting to store the result object, and returning the UID of the created database entry for additional relational usage.
                id = (int)command.ExecuteScalar();
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print("CprResults StoreResult {1} ERROR: {0} \n{2}", exc.Message, OVERALL_RESULTS_TABLE_NAME, command.CommandText);
            }

            // Main CPR result info stored.
            if (id > 0)
            {
                try
                {
                    // Store the KinectImages, if they exist, to the images table.
                    result.CalibrationImageID = KinectImage.StoreImage(result.CalibrationImage, IMAGES_TABLE_NAME, false);
                    result.CheckPulseImageID = KinectImage.StoreImage(result.CheckPulseImage, IMAGES_TABLE_NAME, false);
                }
                catch (Exception exc)
                {
                    System.Diagnostics.Debug.Print("CprResult StoreResult {0} ERROR: {1}", IMAGES_TABLE_NAME, exc.Message);
                }
                finally
                {
                    if (result.CalibrationImageID == 0)
                        result.CalibrationImageID = null;

                    if (result.CheckPulseImageID == 0)
                        result.CheckPulseImageID = null;

                    System.Diagnostics.Debug.Print("Calibration Image ID and CheckPulseImageID {0} {1}", result.CalibrationImageID, result.CheckPulseImageID);
                }

                int cprResultId = 0;

                command = DatabaseManager.Connection.CreateCommand();
                command.CommandType = System.Data.CommandType.Text;
                command.CommandText = "INSERT INTO " + CPR_RESULTS_TABLE_NAME +
                    " (resultid, calibrationimageid, askedforaed, askedfor911, askedifok, checkpulseimageid, checkedpulse, checkpulseduration, averagedepth, averagerate, averagebodymech, vocalinteractionscore, feedbackids ) " +
                    "VALUES (:resultid, :calibrationimageid, :askedforaed, :askedfor911, :askedifok, :checkpulseimageid, CAST(:checkedpulse as \"CheckPulse\"), :checkpulseduration, :averagedepth, :averagerate, :averagebodymech, :vocalinteractionscore, :feedbackids ) " +
                    "RETURNING uid;";
                command.Parameters.Add("resultid", NpgsqlDbType.Integer).Value = id;
                command.Parameters.Add("calibrationimageid", NpgsqlDbType.Integer).Value = result.CalibrationImageID;
                command.Parameters.Add("askedforaed", NpgsqlDbType.Boolean).Value = result.AskedForAED;
                command.Parameters.Add("askedfor911", NpgsqlDbType.Boolean).Value = result.AskedFor911;
                command.Parameters.Add("askedifok", NpgsqlDbType.Boolean).Value = result.CheckedResponsiveness;
                command.Parameters.Add("checkpulseimageid", NpgsqlDbType.Integer).Value = result.CheckPulseImageID;
                command.Parameters.Add("checkedpulse", NpgsqlDbType.Text).Value = result.CheckedPulse.ToString();
                command.Parameters.Add("checkpulseduration", NpgsqlDbType.Interval).Value = result.CheckPulseDuration;
                command.Parameters.Add("averagedepth", NpgsqlDbType.Double).Value = result.AverageDepth;
                command.Parameters.Add("averagerate", NpgsqlDbType.Integer).Value = result.AverageRate;
                command.Parameters.Add("averagebodymech", NpgsqlDbType.Double).Value = result.AverageBodyMech;
                command.Parameters.Add("vocalinteractionscore", NpgsqlDbType.Double).Value = result.VocalInteractionScore;
                command.Parameters.Add("feedbackids", NpgsqlDbType.Integer | NpgsqlDbType.Array).Value = result.OverallFeedback.ToArray();

                try
                {
                    // Execute the query, attempting to store the result object, and returning the UID of the created database entry for additional relational usage.
                    cprResultId = (int)command.ExecuteScalar();
                }
                catch (Exception exc)
                {
                    System.Diagnostics.Debug.Print("CprResults StoreResult {1} ERROR: {0}", exc.Message, CPR_RESULTS_TABLE_NAME);
                }

                // Store the CPR Compressions data for the result.
                if (cprResultId > 0)
                {
                    for (int i = 0; i < result.CompressionSetCount; i++)
                    {
                        // Attempt to store the compression image.
                        result.CompressionSets[i].ImageID = KinectImage.StoreImage(result.CompressionSets[i].Image, IMAGES_TABLE_NAME, false);

                        System.Diagnostics.Debug.Print("KinectImage Compression ID for {0} ~ {1}", i, result.CompressionSets[i].ImageID);

                        // StoreImage returns 0 on error, so if it is 0 make it null.
                        if (result.CompressionSets[i].ImageID == 0) 
                            result.CompressionSets[i].ImageID = null;

                        command = DatabaseManager.Connection.CreateCommand();
                        command.CommandType = System.Data.CommandType.Text;
                        command.CommandText = "INSERT INTO " + CPR_COMPRESSIONS_RESULTS_TABLE_NAME +
                            " (resultid, setnumber, setdurationmiliseconds, depth, depthinches, repsperminute, lockedarmsscore, shouldersovermanikinscore, handplacementscore, score, imageid, validcompressioncount, feedbackids ) " +
                            "VALUES (:resultid, :setnumber, :setdurationmiliseconds, :depth, :depthinches, :repsperminute, :lockedarmsscore, :shouldersovermanikinscore, :handplacementscore, :score, :imageid, :validcompressioncount, :feedbackids) ";
                        command.Parameters.Add("resultid", NpgsqlDbType.Integer).Value = cprResultId;
                        command.Parameters.Add("setnumber", NpgsqlDbType.Integer).Value = result.CompressionSets[i].SetNumber;
                        command.Parameters.Add("setdurationmiliseconds", NpgsqlDbType.Integer).Value = result.CompressionSets[i].SetDurationMiliseconds;
                        command.Parameters.Add("depth", NpgsqlDbType.Double).Value = result.CompressionSets[i].Depth;
                        command.Parameters.Add("depthinches", NpgsqlDbType.Double).Value = result.CompressionSets[i].DepthInches; 
                        command.Parameters.Add("repsperminute", NpgsqlDbType.Double).Value = result.CompressionSets[i].RepsPerMinute;
                        command.Parameters.Add("lockedarmsscore", NpgsqlDbType.Double).Value = result.CompressionSets[i].LockedArmsScore;
                        command.Parameters.Add("shouldersovermanikinscore", NpgsqlDbType.Double).Value = result.CompressionSets[i].ShouldersOverManikinScore;
                        command.Parameters.Add("handplacementscore", NpgsqlDbType.Double).Value = result.CompressionSets[i].HandPlacementScore;
                        command.Parameters.Add("score", NpgsqlDbType.Double).Value = result.CompressionSets[i].Score;
                        command.Parameters.Add("imageid", NpgsqlDbType.Integer).Value = result.CompressionSets[i].ImageID;
                        command.Parameters.Add("validcompressioncount", NpgsqlDbType.Integer).Value = result.CompressionSets[i].ValidCompressionCount;
                        command.Parameters.Add("feedbackids", NpgsqlDbType.Integer | NpgsqlDbType.Array).Value = result.CompressionSets[i].FeedbackIDs.ToArray();

                        try
                        {
                            command.ExecuteScalar();
                        }
                        catch (Exception exc)
                        {
                            System.Diagnostics.Debug.Print("CprResults StoreResult {1} ERROR: {0}", exc.Message, CPR_COMPRESSIONS_RESULTS_TABLE_NAME);
                        }
                    }
                }
            }

            DatabaseManager.CloseConnection();

            return id;
        }

        /// <summary>
        /// Retrieves a result from the resultId
        /// If retrieveImages is set to true, the result's images will also be retrieved from the database.
        /// </summary>
        /// <param name="resultId"></param>
        /// <param name="retreiveImages"></param>
        public static CprResult RetrieveResult(int resultId, bool retrieveImages) 
        {
            if (resultId <= 0)
                return null;

            CprResult result = new CprResult() { UID = resultId };

            //Retrieve the basic result information
            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "SELECT studentid, assignmentid, tasktype, taskmode, masterylevel, score, starttime, endtime, submissiontime" +
                " FROM " + OVERALL_RESULTS_TABLE_NAME +
                " WHERE uid = :uid";
            command.Parameters.Add("uid", NpgsqlDbType.Integer).Value = resultId;

            bool resultFound = true;

            try
            {
                using (NpgsqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        result.StudentID = reader.GetInt32(reader.GetOrdinal("studentid"));
                        result.TaskType = (Task)Enum.Parse(typeof(Task), reader.GetString(reader.GetOrdinal("tasktype")));
                        result.TaskMode = (TaskMode)Enum.Parse(typeof(TaskMode), reader.GetString(reader.GetOrdinal("taskmode")));
                        result.MasteryLevel = (MasteryLevel)Enum.Parse(typeof(MasteryLevel), reader.GetString(reader.GetOrdinal("masterylevel")));
                        result.Score = reader.GetDouble(reader.GetOrdinal("score"));
                        result.StartTime = reader.GetTimeStamp(reader.GetOrdinal("starttime"));
                        result.EndTime = reader.GetTimeStamp(reader.GetOrdinal("endtime"));
                        result.SubmissionTime = reader.GetTimeStamp(reader.GetOrdinal("submissiontime"));
                    }
                }
            }
            catch (Exception exc)
            {
                resultFound = false;
                System.Diagnostics.Debug.Print("CprResult RetrieveResult {1} ERROR: {0}", exc.Message, OVERALL_RESULTS_TABLE_NAME);
            }

            // Retrieve the Cpr specific result information
            if (resultFound)
            {
                int cprUID = 0;

                command = DatabaseManager.Connection.CreateCommand();
                command.CommandType = System.Data.CommandType.Text;
                command.CommandText = "SELECT uid, calibrationimageid, askedforaed, askedfor911, askedifok, checkpulseimageid, checkedpulse, checkpulseduration, averagedepth, averagerate, averagebodymech, vocalinteractionscore, feedbackids" +
                    " FROM " + CPR_RESULTS_TABLE_NAME +
                    " WHERE resultid = :resultid";
                command.Parameters.Add("resultid", NpgsqlDbType.Integer).Value = resultId;

                try
                {
                    using (NpgsqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            cprUID = reader.GetInt32(reader.GetOrdinal("uid"));
                            result.UID = cprUID;
                            result.CalibrationImageID = reader.IsDBNull(reader.GetOrdinal("calibrationimageid")) ? null : (int?)reader.GetInt32(reader.GetOrdinal("calibrationimageid"));
                            result.AskedForAED = reader.GetBoolean(reader.GetOrdinal("askedforaed"));
                            result.AskedFor911 = reader.GetBoolean(reader.GetOrdinal("askedfor911"));
                            result.CheckedResponsiveness = reader.GetBoolean(reader.GetOrdinal("askedifok"));
                            result.CheckPulseImageID = reader.IsDBNull(reader.GetOrdinal("checkpulseimageid")) ? null : (int?)reader.GetInt32(reader.GetOrdinal("checkpulseimageid"));
                            result.CheckedPulse = (CheckPulse)Enum.Parse(typeof(CheckPulse), reader.GetString(reader.GetOrdinal("checkedpulse")));
                            result.CheckPulseDuration = reader.IsDBNull(reader.GetOrdinal("checkpulseduration")) ? TimeSpan.Zero : reader.GetInterval(reader.GetOrdinal("checkpulseduration")).Time;
                            result.AverageDepth = reader.GetDouble(reader.GetOrdinal("averagedepth"));
                            result.AverageRate = reader.GetInt32(reader.GetOrdinal("averagerate"));
                            result.AverageBodyMech = reader.GetDouble(reader.GetOrdinal("averagebodymech"));
                            result.VocalInteractionScore = reader.GetDouble(reader.GetOrdinal("vocalinteractionscore"));
                            result.OverallFeedback.AddRange(reader.GetValue(reader.GetOrdinal("feedbackids")) as int[]);
                        }
                    }
                }
                catch (Exception exc)
                {
                    resultFound = false;
                    System.Diagnostics.Debug.Print("CprResult RetrieveResult {1} ERROR: {0}", exc.Message, CPR_RESULTS_TABLE_NAME);
                }

                // Retrieve the Cpr Compressions result information
                if (resultFound)
                {
                    command = DatabaseManager.Connection.CreateCommand();
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = "SELECT resultid, setnumber, setdurationmiliseconds, depth, depthinches, repsperminute, lockedarmsscore, shouldersovermanikinscore, handplacementscore, score, imageid, validcompressioncount, feedbackids" +
                        " FROM " + CPR_COMPRESSIONS_RESULTS_TABLE_NAME +
                        " WHERE resultid = :cprUID" +
                        " ORDER BY setnumber ASC";
                    command.Parameters.Add("cprUID", NpgsqlDbType.Integer).Value = cprUID;

                    try
                    {
                        using (NpgsqlDataReader reader = command.ExecuteReader())
                        {
                            // For every row, add a CompressionSet
                            while (reader.Read())
                            {
                                CprCompressionSetResult set = new CprCompressionSetResult();

                                set.CprResultID = cprUID;
                                set.SetNumber = reader.GetInt32(reader.GetOrdinal("setnumber"));
                                set.SetDurationMiliseconds = reader.GetInt32(reader.GetOrdinal("setdurationmiliseconds"));
                                set.Depth = reader.GetDouble(reader.GetOrdinal("depth"));
                                set.DepthInches = reader.GetDouble(reader.GetOrdinal("depthinches"));
                                set.RepsPerMinute = reader.GetInt32(reader.GetOrdinal("repsperminute"));
                                set.LockedArmsScore = reader.GetDouble(reader.GetOrdinal("lockedarmsscore"));
                                set.ShouldersOverManikinScore = reader.GetDouble(reader.GetOrdinal("shouldersovermanikinscore"));
                                set.HandPlacementScore = reader.GetDouble(reader.GetOrdinal("handplacementscore"));
                                set.Score = reader.GetDouble(reader.GetOrdinal("score"));
                                set.ImageID = reader.IsDBNull(reader.GetOrdinal("imageid")) ? null : (int?)reader.GetInt32(reader.GetOrdinal("imageid"));
                                set.ValidCompressionCount = reader.IsDBNull(reader.GetOrdinal("validcompressioncount")) ? 0 : reader.GetInt32(reader.GetOrdinal("validcompressioncount"));
                                set.FeedbackIDs.AddRange(reader.GetValue(reader.GetOrdinal("feedbackids")) as int[]);

                                result.AddCompressionSet(set);
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        System.Diagnostics.Debug.Print("CprResults RetrieveResult {1} ERROR: {0}", exc.Message, CPR_COMPRESSIONS_RESULTS_TABLE_NAME);
                    }

                    #region fix old results..
                    if (result.CompressionSets != null && result.CompressionSets.Count > 0)
                    {
                        if (result.AverageRate == 0)
                        {
                            double temp = 0;

                            foreach (CprCompressionSetResult set in result.CompressionSets)
                            {
                                temp += set.RepsPerMinute.GetValueOrDefault(0);
                            }

                            temp /= result.CompressionSets.Count;

                            result.AverageRate = temp;

                            // Update the entry back in the database, so this wont have to occur again.
                            if (temp != 0)
                            {
                                command = DatabaseManager.Connection.CreateCommand();
                                command.CommandText = @"UPDATE " + CPR_RESULTS_TABLE_NAME + " SET averagerate = :averagerate WHERE uid = :uid";
                                command.Parameters.Add("averagerate", NpgsqlDbType.Double).Value = temp;
                                command.Parameters.Add("uid", NpgsqlDbType.Integer).Value = result.UID;

                                try
                                {
                                    command.ExecuteNonQuery();
                                }
                                catch
                                {
                                }
                            }
                        }

                        if (result.AverageDepth == 0 || result.AverageDepth > 8)
                        {
                            double temp = 0;

                            foreach (CprCompressionSetResult set in result.CompressionSets)
                            {
                                if (set.DepthInches.GetValueOrDefault(0) == 0)
                                    temp += set.Depth.GetValueOrDefault(0);
                                else
                                    temp += set.DepthInches.GetValueOrDefault(0);
                            }

                            temp /= result.CompressionSets.Count;

                            result.AverageDepth = temp;

                            // Update the entry back in the database, so this wont have to occur again.
                            if (temp != 0)
                            {
                                command = DatabaseManager.Connection.CreateCommand();
                                command.CommandText = @"UPDATE " + CPR_RESULTS_TABLE_NAME + " SET averagedepth = :averagedepth WHERE uid = :uid";
                                command.Parameters.Add("averagedepth", NpgsqlDbType.Double).Value = temp;
                                command.Parameters.Add("uid", NpgsqlDbType.Integer).Value = result.UID;
                            }
                        }

                        if (result.AverageBodyMech == 0)
                        {
                            double temp = 0;

                            foreach (CprCompressionSetResult set in result.CompressionSets)
                            {
                                temp += set.ShouldersOverManikinScore.GetValueOrDefault(0);
                                temp += set.LockedArmsScore.GetValueOrDefault(0);
                                temp += set.HandPlacementScore.GetValueOrDefault(0);
                            }

                            temp /= (result.CompressionSets.Count * 3);

                            result.AverageBodyMech = temp;

                            // Update the entry back in the database, so this wont have to occur again.
                            if (result.AverageBodyMech != 0)
                            {
                                command = DatabaseManager.Connection.CreateCommand();
                                command.CommandText = @"UPDATE " + CPR_RESULTS_TABLE_NAME + " SET averagebodymech = :averagebodymech WHERE uid = :uid";
                                command.Parameters.Add("averagebodymech", NpgsqlDbType.Double).Value = temp;
                                command.Parameters.Add("uid", NpgsqlDbType.Integer).Value = result.UID;

                                try
                                {
                                    command.ExecuteNonQuery();
                                }
                                catch
                                {
                                }
                            }
                        }
                    }
                    #endregion
                }

                // Retrieve the images for the result
                if (retrieveImages)
                    CprResult.RetrieveImages(ref result);
            }
            DatabaseManager.CloseConnection();

            return result;
        }

        /// <summary>
        /// Retrieve images from the database and store them in the passed CprResult object.
        /// </summary>
        /// <param name="Result">CprResult object to retrieve images for.</param>
        public static void RetrieveImages(ref CprResult Result) 
        {
            // Build list of image id's to get
            List<int> imageIDs = new List<int>();

            if (Result.CalibrationImage == null && Result.CalibrationImageID.HasValue)
                imageIDs.Add(Result.CalibrationImageID.Value);

            if (Result.CheckPulseImage == null && Result.CheckPulseImageID.HasValue)
                imageIDs.Add(Result.CheckPulseImageID.Value);

            foreach (CprCompressionSetResult compSet in Result.CompressionSets)
            {
                if (compSet.Image == null && compSet.ImageID.HasValue)
                    imageIDs.Add(compSet.ImageID.Value);
            }

            // Retrieve Images from Database.
            Dictionary<int, KinectImage> kinectImages = KinectImage.RetreiveImages(imageIDs.ToArray(), IMAGES_TABLE_NAME);

            // Update the results images based on what was retrieved from the database.
            if (Result.CalibrationImage == null && Result.CalibrationImageID.HasValue)
            {
                try
                {
                    Result.CalibrationImage = kinectImages[Result.CalibrationImageID.Value];
                }
                catch (Exception exc) { System.Diagnostics.Debug.Print("CprResult RetrieveImages ERROR: {0}", exc.Message); }
            }
            if (Result.CheckPulseImage == null && Result.CheckPulseImageID.HasValue)
            {
                try
                {
                    Result.CheckPulseImage = kinectImages[Result.CheckPulseImageID.Value];
                }
                catch (Exception exc) { System.Diagnostics.Debug.Print("CprResult RetrieveImages ERROR: {0}", exc.Message); }
            }
            foreach (CprCompressionSetResult compSet in Result.CompressionSets)
            {
                if (compSet.Image == null && compSet.ImageID.HasValue)
                {
                    try
                    {
                        compSet.Image = kinectImages[compSet.ImageID.Value];
                    }
                    catch (Exception exc) { System.Diagnostics.Debug.Print("CprResult RetrieveImages ERROR: {0}", exc.Message); }
                }
            }
        }

        #endregion

        #endregion

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
