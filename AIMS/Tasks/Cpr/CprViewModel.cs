﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace AIMS.Tasks.Cpr
{
    public class CprViewModel : DependencyObject
    {   
        /*
        public static readonly DependencyProperty KinectSensorManagerProperty =
            DependencyProperty.Register(
                "KinectSensorManager",
                typeof(KinectSensorManager),
                typeof(CprViewModel),
                new PropertyMetadata(null));
        */
        
        public static readonly DependencyProperty CprLessonProperty =
            DependencyProperty.Register(
                "CprLesson",
                typeof(Cpr),
                typeof(CprViewModel),
                new PropertyMetadata(null, CprLessonObjectChanged));

        //public KinectSensorManager KinectSensorManager { get { return (KinectSensorManager)GetValue(KinectSensorManagerProperty); } set { SetValue(KinectSensorManagerProperty, value); } }
        public Cpr Cpr { get { return (Cpr)GetValue(CprLessonProperty); } set { SetValue(CprLessonProperty, value); } }

        //Change Handlers
        private static void CprLessonObjectChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs args)
        {
            CprViewModel ivm = dependencyObject as CprViewModel;
        }
    }
}
