﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using System.Windows.Media.Imaging;
using System.Diagnostics;
using Microsoft.Kinect;
using System.Windows;
using System.ComponentModel;

namespace AIMS.Tasks.Cpr
{
    public class CprVocalInteractions : Stage
    {
        private Cpr lesson;

        private bool checkedResponsiveness;
        private bool askedFor911;
        private bool askedForAED;

        private bool updateCheckedResponsiveness { get { return this.checkedResponsiveness; } set { this.checkedResponsiveness = value; this.CheckVoiceStates(); } }
        private bool updateAskedFor911 { get { return this.askedFor911; } set { this.askedFor911 = value; this.CheckVoiceStates(); } }
        private bool updateAskedForAED { get { return this.askedForAED; } set { this.askedForAED = value; this.CheckVoiceStates(); } }

        public bool CheckedResponsiveness { get { return this.checkedResponsiveness; } }
        public bool AskedFor911 { get { return this.askedFor911; } }
        public bool AskedForAED { get { return this.askedForAED; } }

        public CprVocalInteractions( Cpr cpr )
        {
            this.lesson = cpr;

            Name = "Check Responsiveness, Asking for 911, and Asking for an AED";
            Instruction = "Ask the patient if they are okay. Shake them if no response. Tell AIMI to call 911. Tell AIMI to get a Defibrillator or AED.";
            TargetTime = TimeSpan.MaxValue;
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Tasks/Cpr/Graphics/CPR004.png";
            Zones = new List<Zone>(1);
            Stopwatch = new System.Diagnostics.Stopwatch();
            this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.CanTimeOut = false;
            this.TimeOutTime = TimeSpan.FromSeconds(10.0);
        }

        public override void SetupStage()
        {
            try
            {
                // Update CprPage for this Stage
                this.lesson.CprPage.StageInfoTextBlock.Text = this.Name;
                this.lesson.CprPage.InstructionTextBlock.Text = this.Instruction;
                this.lesson.CprPage.CurrentStageImage.Source = new BitmapImage(new Uri(this.ImagePath, UriKind.Relative));
                this.lesson.CprPage.zoneOverlay.ClearZones();

                // Set vocal checks to unheard.
                this.updateCheckedResponsiveness = false;
                this.updateAskedFor911 = false;
                this.updateAskedForAED = false;

                // Reset Timers / Clocks / Watches
                this.StartTime = DateTime.Now;
                this.EndTime = DateTime.MaxValue;
                this.Stopwatch.Reset();
                this.Stopwatch.Start();
                this.TimeoutStopwatch.Reset();
                this.TimeoutStopwatch.Start();

                // Subscribe to SpeechCommander event to hear user speech.
                KinectManager.KinectSpeechCommander.SpeechRecognized += new EventHandler<Speech.SpeechCommanderEventArgs>(KinectSpeechCommander_SpeechRecognized);

                // Reset StageScore
                this.StageScore.WipeClean();
                this.StageScore = null;
                this.StageScore = new StageScore();

                // Done setting up!
                this.IsSetup = true;
                this.Complete = false;
            }
            catch (Exception exc)
            {
                Debug.Print("CprVocalInteraction SetupStage() ERROR: {0}", exc.Message);
            }
        }

        /// <summary>
        /// Whenever a voice state changes ( heard or not ) check if all voice states have fired. If so, end stage.
        /// </summary>
        private void CheckVoiceStates()
        {
            if (this.Complete)
                return;

            if (this.updateCheckedResponsiveness && this.updateAskedFor911 && this.updateAskedForAED)
            {
                this.EndStage();
            }
        }
        
        /// <summary>
        /// Nothing to update in this stage except to setup or check if timed out. ( also doesn't time out )
        /// </summary>
        public override void UpdateStage()
        {
            if (!this.IsSetup)
                SetupStage();

            // Check if stage timed out.
            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
                this.TimeoutStage();
        }

        // Placeholder until this function signature is removed from Stage
        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            // No Watchers in this Stage...
        }

        // Updates all of the watchers within this stage.
        public void UpdateWatcherStates()
        {
            // No Watchers in this Stage...
        }

        // Listens for speech recognized by the SpeechCommander.
        void KinectSpeechCommander_SpeechRecognized(object sender, Speech.SpeechCommanderEventArgs e)
        {
            switch (e.Command)
            {
                case "CHECK_RESPONSIVENESS":
                    OnSaid_CheckResponsiveness(e);
                    break;
                case "ASK_FOR_911":
                    OnSaid_AskFor911(e);
                    break;
                case "ASK_FOR_DEFIB":
                    OnSaid_AskForDefib(e);
                    break;
                default:
                    break;
            }
        }

        // The user asked the manikin if it was alright.
        private void OnSaid_CheckResponsiveness(Speech.SpeechCommanderEventArgs speechArgs)
        {
            if (!this.updateCheckedResponsiveness)
            {
                lesson.PlayNextStageSound();

                this.lesson.DetectableActions.DetectAction("Asked", "Are You Okay?", "Vocal Interactions", true);
            }

            this.updateCheckedResponsiveness = true;
        }

        // The user asked AIMI to notify 911.
        private void OnSaid_AskFor911(Speech.SpeechCommanderEventArgs speechArgs)
        {
            if (!this.updateAskedFor911)
            {
                lesson.PlayNextStageSound();

                this.lesson.DetectableActions.DetectAction("Called For 911", "Call For 911", "Vocal Interactions", true);
            }

            this.updateAskedFor911 = true;
        }

        // The user asked AIMI to get a defibrillator or AED.
        private void OnSaid_AskForDefib(Speech.SpeechCommanderEventArgs speechArgs)
        {
            if (!this.updateAskedForAED)
            {
                lesson.PlayNextStageSound();

                this.lesson.DetectableActions.DetectAction("Called For An AED", "Call For An AED", "Vocal Interactions", true);
            }

            this.updateAskedForAED = true;
        }

        public override void EndStage( bool playSound = true )
        {
            this.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            // Get the current time to signify the time the stage ended.
            EndTime = DateTime.Now;

            // Unsubscribe to SpeechCommander event.
            KinectManager.KinectSpeechCommander.SpeechRecognized -= KinectSpeechCommander_SpeechRecognized;

            if( playSound )
                lesson.PlayNextStageSound();

            this.Complete = true;
            this.lesson.StageComplete(this);
        }

        public override void PauseStage()
        {
            this.lesson.Paused = true;
            this.lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.lesson.Paused = false;
            this.lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void ResetStage()
        {
            this.WipeClean();
            this.SetupStage();
        }

        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            //this.PauseStage();
            this.TimedOut = true;
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            this.RequestStageVideo();   // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
        }

        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.lesson.CprPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Cpr/Videos/CPR 003.wmv", UriKind.Relative));
            this.lesson.CprPage.VideoPlayer.startVideo( this, new RoutedEventArgs() );

            this.lesson.CprPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.lesson.CprPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.lesson.CprPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.lesson.CprPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.lesson.CprPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.lesson.CprPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        public override void WipeClean()
        {
            this.IsSetup = false;
            this.Complete = false;
            this.TimedOut = false;
        }
    }
}
