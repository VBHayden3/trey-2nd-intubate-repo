﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIMS.Tasks.Cpr
{
    /// <summary>
    /// The state of how the pulse of the manikin was checked.
    /// </summary>
    public enum CheckPulse
    {
        No,
        Incorrectly,
        Correctly
    };
}
