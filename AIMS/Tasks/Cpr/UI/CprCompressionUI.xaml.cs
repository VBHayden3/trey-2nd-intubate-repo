﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIMS.Tasks.Cpr
{
    /// <summary>
    /// Interaction logic for CprCompressionUI.xaml
    /// </summary>
    public partial class CprCompressionUI : UserControl, INotifyPropertyChanged
    {
        public static readonly DependencyProperty CompressionSetsProperty =
            DependencyProperty.Register("CompressionSets", typeof(ObservableRangeCollection<CprCompressions.CompressionSet>), typeof(CprCompressionUI), new PropertyMetadata(new ObservableRangeCollection<CprCompressions.CompressionSet>()));

        public ObservableRangeCollection<CprCompressions.CompressionSet> CompressionSets
        {
            get { return (ObservableRangeCollection<CprCompressions.CompressionSet>)GetValue(CompressionSetsProperty); }
            set { SetValue(CompressionSetsProperty, value); NotifyPropertyChanged("CompressionSets"); }
        }

        public static readonly DependencyProperty CompressionPartProperty =
            DependencyProperty.Register("CompressionPart", typeof(CprCompressions.CompressionPart), typeof(CprCompressionUI), new PropertyMetadata(new CprCompressions.CompressionPart()));

        public CprCompressions.CompressionPart CompressionPart
        {
            get { return (CprCompressions.CompressionPart)GetValue(CompressionPartProperty); }
            set { SetValue(CompressionPartProperty, value); NotifyPropertyChanged("CompressionPart"); }
        }

        public CprCompressionUI()
        {
            this.DataContext = this;

            InitializeComponent();

            /*
            // Add some default sets into place: (FOR TESTING)
            #region Default data for testing

            CprCompressions.CompressionSet set0 = new CprCompressions.CompressionSet(0, 5000);
            CprCompressions.CompressionSet set1 = new CprCompressions.CompressionSet(1, 5000);
            CprCompressions.CompressionSet set2 = new CprCompressions.CompressionSet(2, 5000);

            for( int i = 0; i < 3; i++ )
                set0.AddCompression(new CprCompressions.Compression(0.00125));
            for (int i = 0; i < 3; i++)
                set1.AddCompression(new CprCompressions.Compression(0.0025));
            for (int i = 0; i < 3; i++)
                set2.AddCompression(new CprCompressions.Compression(0.00375));

            this.CompressionSets.Add(set0);
            this.CompressionSets.Add(set1);
            this.CompressionSets.Add(set2);

            #endregion
            */
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
