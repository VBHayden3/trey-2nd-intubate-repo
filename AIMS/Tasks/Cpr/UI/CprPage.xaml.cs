﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using AIMS.Assets.Lessons2;
using AIMS.Speech;
using System.IO;
using AIMS.Tasks;
using System.ComponentModel;
using Microsoft.Speech.Synthesis;

namespace AIMS.Tasks.Cpr
{
    /// <summary>
    /// Interaction logic for CprPage.xaml
    /// </summary>
    public partial class CprPage : Page, INotifyPropertyChanged
    {
        public CprPage()
        {
            InitializeComponent();

            Loaded += CprPage_Loaded;
            Unloaded += CprPage_Unloaded;
        }

        private DispatcherTimer showResultDelayTimer = new DispatcherTimer();
        private readonly TimeSpan showResultDelay = TimeSpan.FromSeconds(3.0);

        private System.Media.SoundPlayer tiltHead;
        private System.Media.SoundPlayer calibrate;
        private System.Media.SoundPlayer pickUpLaryngoscopeAndInsert;
        private System.Media.SoundPlayer sweepTheLaryngoscope;
        private System.Media.SoundPlayer pickUpTube;
        private System.Media.SoundPlayer insertTube;
        private System.Media.SoundPlayer removeLaryngoscope;
        private System.Media.SoundPlayer useSyringe;
        private System.Media.SoundPlayer removeStylet;
        private System.Media.SoundPlayer bendDown;

        private AIMS.Tasks.Cpr.Cpr lesson = null;
        public AIMS.Tasks.Cpr.Cpr Lesson 
        { 
            get { return lesson; } 
            set 
            {
                if (this.lesson != value)
                {
                    lesson = value;
                    NotifyPropertyChanged("Lesson");
                }
                else
                {
                    lesson = value;
                }
            } 
        }

        private void CprPage_Loaded(object sender, RoutedEventArgs e)
        {
            LessonStatsheet statsheet = null;

            if (lesson != null)
            {
                lesson.WipeClean();
                lesson = null;
            }

            if (((App)App.Current).CurrentStudent != null)
            {
                statsheet = LessonStatsheet.GetOrCreateStatsheetForStudent(((App)App.Current).CurrentStudent.ID, LessonType.CPR);
            }

            if (statsheet != null)
            {
                ((App)App.Current).CurrentMasteryLevel = statsheet.MasteryLevel;

                this.Lesson = new AIMS.Tasks.Cpr.Cpr(this, ((App)App.Current).CurrentTaskMode, statsheet.MasteryLevel);
            }
            else
            {
                // Instantiate the lesson.
                this.Lesson = new AIMS.Tasks.Cpr.Cpr(this, ((App)App.Current).CurrentTaskMode, ((App)App.Current).CurrentMasteryLevel);
            }

            try
            {
                if (KinectManager.KinectSpeechCommander != null)
                {
                    KinectManager.KinectSpeechCommander.SetModuleGrammars(new Uri("pack://application:,,,/AIMS;Component/Tasks/Cpr/CprSpeechGrammar.xml"),
                        "activeListen", "defaultListen");
                    KinectManager.KinectSpeechCommander.SpeechRecognized += new EventHandler<SpeechCommanderEventArgs>(KinectSpeechCommander_SpeechRecognized);
                }

                #region LoadSpeechPlaybackFiles
                calibrate = new System.Media.SoundPlayer(Properties.Resources.AIMS_Intubation_Calibrate);
                calibrate.LoadAsync();

                bendDown = new System.Media.SoundPlayer(Properties.Resources.AIMS_Intubation_BendDown);
                bendDown.LoadAsync();

                tiltHead = new System.Media.SoundPlayer(Properties.Resources.AIMS_Intubation_TiltTheHead);
                tiltHead.LoadAsync();

                pickUpLaryngoscopeAndInsert = new System.Media.SoundPlayer(Properties.Resources.AIMS_Intubation_InsertTheLaryngoscopeAndInsert);
                pickUpLaryngoscopeAndInsert.LoadAsync();

                sweepTheLaryngoscope = new System.Media.SoundPlayer(Properties.Resources.AIMS_Intubation_SweepTheLaryngoscope);
                sweepTheLaryngoscope.LoadAsync();

                pickUpTube = new System.Media.SoundPlayer(Properties.Resources.AIMS_Intubation_PickUpEndotrachealTube);
                pickUpTube.LoadAsync();

                insertTube = new System.Media.SoundPlayer(Properties.Resources.AIMS_Intubation_InsertTheEndotrachealTube);
                insertTube.LoadAsync();

                removeLaryngoscope = new System.Media.SoundPlayer(Properties.Resources.AIMS_Intubation_RemoveTheLaryngoscope);
                removeLaryngoscope.LoadAsync();

                useSyringe = new System.Media.SoundPlayer(Properties.Resources.AIMS_Intubation_UseSyringe);
                useSyringe.LoadAsync();

                removeStylet = new System.Media.SoundPlayer(Properties.Resources.AIMS_Intubation_RemoveStylet);
                removeStylet.LoadAsync();
                #endregion
            }
            catch(Exception exc)
            {
                System.Diagnostics.Debug.Print("CprPage.xaml.cs ERROR: {0}", exc.Message);
            }

            if (lesson != null)
                lesson.StartLesson();
        }

        private void CprPage_Unloaded(object sender, RoutedEventArgs e)
        {
            if (lesson != null)
            {
                lesson.WipeClean();
                lesson = null;
            }

            try
            {
                KinectManager.KinectSpeechCommander.SpeechRecognized -= KinectSpeechCommander_SpeechRecognized;
                KinectManager.KinectSpeechCommander.ClearModuleGrammars();
            }
            catch
            {
            }
        }

        #region Kinect Speech processing
        void KinectSpeechCommander_SpeechRecognized(object sender, SpeechCommanderEventArgs e)
        {
            switch (e.Command)
            {
                case "HELP":
                    OnSaid_Help();
                    break;
                case "VIDEO":
                    OnSaid_Video();
                    break;
                case "TUTORIAL":
                    OnSaid_Tutorial();
                    break;
            }
        }

        /// <summary>
        /// "Help" was said, display the help menu and/or give auditory response. 
        /// </summary>
        private void OnSaid_Help()
        {
            if( lesson != null )
            {
                if (lesson.Stages != null && lesson.CurrentStageIndex < lesson.Stages.Count)
                {
                    switch (lesson.CurrentStageIndex)
                    {
                        case 0:
                        {
                            KinectManager.KinectSpeechCommander.SpeechSynthesizer.SpeakAsync("Using the image on the screen as a reference, raise one hand into the air, and place your other hand onto the manikin’s chest.");

                            break;
                        }
                        case 1:
                        {
                            CprVocalInteractions vocalInteractionsStage = (CprVocalInteractions)lesson.Stages[lesson.CurrentStageIndex];

                            if (!vocalInteractionsStage.CheckedResponsiveness)
                                KinectManager.KinectSpeechCommander.SpeechSynthesizer.SpeakAsync("Check the patient’s responsiveness by asking: \"Are you okay?\"");
                            else if (!vocalInteractionsStage.AskedFor911)
                                KinectManager.KinectSpeechCommander.SpeechSynthesizer.SpeakAsync("Ensure that emergency assistance is immediately alerted by telling Amy to Call 9 1 1.");
                            else if (!vocalInteractionsStage.AskedForAED)
                                KinectManager.KinectSpeechCommander.SpeechSynthesizer.SpeakAsync("Prepare for further resuscitation action by telling Amy to \"Get an AED\"");

                            break;
                        }
                        case 2:
                        {
                            CprCheckPulse checkPulseStage = (CprCheckPulse)lesson.Stages[lesson.CurrentStageIndex];

                            if (!checkPulseStage.HasCheckedPulse)
                            {
                                CprVocalInteractions vocalInteractionsStage = (CprVocalInteractions)lesson.Stages[1];

                                // First, if vocal interactions havn't occured, inform the user that the should complete those first.
                                if (!vocalInteractionsStage.CheckedResponsiveness)
                                    KinectManager.KinectSpeechCommander.SpeechSynthesizer.SpeakAsync("Check the patient’s responsiveness by asking: \"Are you okay?\"");
                                else if (!vocalInteractionsStage.AskedFor911)
                                    KinectManager.KinectSpeechCommander.SpeechSynthesizer.SpeakAsync("Ensure that emergency assistance is immediately alerted by telling Amy to Call 9 1 1.");
                                else if (!vocalInteractionsStage.AskedForAED)
                                    KinectManager.KinectSpeechCommander.SpeechSynthesizer.SpeakAsync("Prepare for further resuscitation action by telling Amy to \"Get an AED\"");
                                else
                                {
                                    // Otherwise, if all vocal interactions have occured, give information about how to check the patient's pulse.
                                    KinectManager.KinectSpeechCommander.SpeechSynthesizer.SpeakAsync("Using the hand closest to the patient’s head, check for the patient’s pulse for 5 to 10 seconds on the nearest side of the patient’s neck.");
                                }
                            }

                            break;
                        }
                        case 3:
                        {
                            PromptBuilder pb = new PromptBuilder();

                            pb.AppendText("Place the heel of one of your hands on the patient’s chest, between the nipples. Place the heel of your other hand on top of the first hand. Locking your arms at your elbows, position yourself directly over your hands.");
                            pb.AppendBreak(PromptBreak.Medium); // regular pause between instructions
                            pb.AppendText("Perform compressions at a constant rate of 100 to 120 compressions per minute. Keep your arms locked and your shoulders over the patient's chest. On an adult patient, the depth of compressions should be 2 inches.");

                            KinectManager.KinectSpeechCommander.SpeechSynthesizer.SpeakAsync(pb);

                            break;
                        }
                    }
                }
            }
        }

        private void OnSaid_Video()
        {
            if (lesson != null)
            {
                if (lesson.Stages != null && lesson.CurrentStageIndex < lesson.Stages.Count)
                {
                    lesson.Stages[lesson.CurrentStageIndex].RequestStageVideo();
                }
            }
        }

        private void OnSaid_Tutorial()
        {
            if (lesson != null)
            {
                lesson.RequestTutorialVideo();
            }
        }
        
        #endregion Kinect Speech processing

        public void ShowResults(CprResult result)
        {
            // make a ShowResults function in code-behind like lateral transfer
            this.ResultsComponent.IsEnabled = true;
            this.ResultsComponent.ShowResults(result, true);
            this.ResultsComponent.Visibility = System.Windows.Visibility.Visible;
        }

        public void GoHome()
        {
            System.Uri uri = new System.Uri("../MainPage3.xaml", System.UriKind.Relative);
            this.NavigationService.Navigate(uri);
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            // Reset the lesson
            this.lesson.ResetLesson();
        }

        private void SidePanelExpansionButton_Click(object sender, RoutedEventArgs e)
        {
            if (SidePanelContentGrid.Visibility == System.Windows.Visibility.Collapsed)
            {
                SidePanelContentGrid.Visibility = System.Windows.Visibility.Visible;
                (sender as Button).Content = "<";
                SidePanelContentGrid.Focus();
            }
            else
            {
                SidePanelContentGrid.Visibility = System.Windows.Visibility.Collapsed;
                (sender as Button).Content = ">";
                SidePanelContentGrid.Focus();
            }
        }

        private void HeaderButtonsListBox_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                HeaderButtonsListBox.SelectedIndex = 0;
            }
            catch
            {
            }
        }

        private void ProceduralTaskButtonsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            try
            {
                switch (ProceduralTaskButtonsListBox.SelectedIndex)
                {
                    case 0:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    case 1:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    case 2:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    case 3:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    case 4:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    default:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                }
            }
            catch
            {
            }
        }

        private void HeaderButtonsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (HeaderButtonsListBox.SelectedIndex == -1)
            {
                return;
            }

            try
            {
                switch (HeaderButtonsListBox.SelectedIndex)
                {
                    case 0:

                        break;
                    case 1:
                        //  HeaderButtonsListBox.SelectedIndex = -1;    // Deselect
                        ProceduralTaskButtonsListBox.SelectedIndex = -1;    // Deselect
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Visible;
                        break;
                    case 2:

                        break;
                    case 3:

                        break;
                    case 4:

                        break;
                    default:

                        break;
                }
            }
            catch
            {
            }
        }

        private void ProceduralTaskButtonsListBox_MouseLeave(object sender, MouseEventArgs e)
        {
            ProceduralTaskButtonsListBox.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void TopPanelExpansionButton_Click(object sender, RoutedEventArgs e)
        {
            if (TopPanelContentGrid.Visibility == System.Windows.Visibility.Collapsed)
            {
                TopPanelContentGrid.Visibility = System.Windows.Visibility.Visible;
                (sender as Button).Content = "^";
                TopPanelContentGrid.Focus();
            }
            else
            {
                TopPanelContentGrid.Visibility = System.Windows.Visibility.Collapsed;
                (sender as Button).Content = "v";
                TopPanelContentGrid.Focus();
            }
        }

        private void ResultsComponent_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue.Equals(true))
                this.BlurEffect.Radius = 25;
            else
                this.BlurEffect.Radius = 0;
        }

        private void ProceduralTaskButtonsListBox_Loaded(object sender, RoutedEventArgs e)
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
