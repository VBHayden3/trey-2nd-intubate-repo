﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Controls.DataVisualization.Charting;
using AIMS.Tasks;
using AIMS.Tasks.Cpr;
using System.Windows.Threading;

namespace AIMS
{
	public partial class TestResultsCPR : AIMS.Tasks.ITaskResultsControl, INotifyPropertyChanged
    {
        #region Public Properties

        /// <summary>
        /// CprResults Object which represents the result information of CPR.
        /// </summary>
        public CprResult CprResults
        { 
            get { return this.cprResults; } 
            set 
            { 
                this.cprResults = value;
                NotifyPropertyChanged("CprResults");

                // Notify of additional changes
                NotifyPropertyChanged("OverallFeedback");
                NotifyPropertyChanged("SelectedCompressionSet");
            }
        }
        /// <summary>
        /// TaskFeedback Array of Feedback for the overal CPR result.
        /// </summary>
        public TaskFeedback[] OverallFeedback
        {
            get
            {
                return (this.cprResults != null) ? TaskFeedback.GetFeedbackFromIDs(this.cprResults.OverallFeedback, Task.CPR) : null;
            }
        }
        /// <summary>
        /// TaskFeedback Array of Feedback for a single compression set.
        /// </summary>
        public TaskFeedback[] CompressionFeedback
        {
            get
            {
                // If a column hasn't been selected, return the first compressionset.
                if (this.series_compressions.SelectedItem == null && this.cprResults != null)
                {
                    return (this.cprResults.CompressionSets.Count == 0) ? null : (this.cprResults.CompressionSets[0] != null) ? TaskFeedback.GetFeedbackFromIDs(this.cprResults.CompressionSets[0].FeedbackIDs, Task.CPR) : null;
                }
                // Identify the selected column on the graph and return the information for that column.
                else if (this.series_compressions.SelectedItem != null && this.cprResults != null)
                {
                    KeyValuePair<int, double> point = (KeyValuePair<int, double>)this.series_compressions.SelectedItem;
                    if (point.Key <= cprResults.CompressionSetCount)
                    {
                        return (this.cprResults.CompressionSets[point.Key - 1] != null) ?
                            TaskFeedback.GetFeedbackFromIDs(this.cprResults.CompressionSets[point.Key - 1].FeedbackIDs, Task.CPR) :
                            null;
                    }
                }

                return null;
            }
        }

        private CprCompressionSetResult selectedCompressionSet = null;
        public CprCompressionSetResult SelectedCompressionSet 
        {
            get
            {
                // If the selectedCompressionSet exists, return it. Otherwise, return the first compression set in the result. Finally, fault back to returning null if nothing else exists.
                return selectedCompressionSet != null ? selectedCompressionSet : this.CprResults != null && this.CprResults.CompressionSets != null && this.CprResults.CompressionSets.Count > 0 ? selectedCompressionSet = this.CprResults.CompressionSets[0] : null;
            }
            set
            {
                if (this.selectedCompressionSet != value)
                {
                    selectedCompressionSet = value;
                    NotifyPropertyChanged("SelectedCompressionSet");
                }
                else
                {
                    selectedCompressionSet = value;
                }
            }
        }

        #endregion

        #region Private Fields

        private CprResult cprResults = null;
        private WriteableBitmap compressionsImage = new WriteableBitmap(640, 480, 96, 96, PixelFormats.Bgr32, null);
        private Border previousColumn = null;

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

		public TestResultsCPR()
		{
			this.InitializeComponent();

            this.DataContext = this;
		}

        #region Public Functions

        public void ShowResults(TaskResult Result, bool ShowButtons = false)
        {
            /*if (result == null)
                return;*/
            if (Result is CprResult)
            {
                /*compressionsImage = new WriteableBitmap(640, 480, 96, 96, PixelFormats.Bgr32, null);
                this.img_compressions.Source = compressionsImage;*/

                this.CprResults = Result as CprResult;

                this.Visibility = System.Windows.Visibility.Visible;
                this.IsEnabled = true;

                this.btn_submit.IsEnabled = true;
                this.btn_submit.Content = "Submit";
                this.btn_tryAgain.IsEnabled = true;
                this.btn_exit.IsEnabled = true;

                Page parentPage = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

                if (parentPage as CprPage != null)
                    (parentPage as CprPage).BlurEffect.Radius = 25;
            }
            else if (Result != null && Result.TaskType == Task.CPR)
            {
                // TODO: Handle incomplete CPR Result.
            }
            else
                return;

            if (ShowButtons)
                this.ShowButtons();
            else this.HideButtons();
        }

        /// <summary>
        /// Closes (Hides/Disables/Un-blurs) the Results Overlay.
        /// </summary>
        public void CloseResults(bool wipeInformation)
        {
            this.Visibility = System.Windows.Visibility.Collapsed;
            this.IsEnabled = false;

            Page parentPage = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

            if (parentPage as CprPage != null)
                (parentPage as CprPage).BlurEffect.Radius = 0;

            if (wipeInformation)
                this.cprResults = null;
        }

        public void HideButtons()
        {
            this.wrppnl_buttonsPanel.Visibility = System.Windows.Visibility.Collapsed;
            this.wrppnl_buttonsPanel.IsEnabled = false;
        }

        public void ShowButtons()
        {
            this.wrppnl_buttonsPanel.Visibility = System.Windows.Visibility.Visible;
            this.wrppnl_buttonsPanel.IsEnabled = true;
        }

        #endregion

        #region Private Functions

        /// <summary>
        /// Updates the UI with the CprResults data when the CprResults property is set.
        /// </summary>
        private void UpdateUI()
        {
            // Update the UI with values from the CprResults object if it exists.
            if (this.cprResults == null)
                return;

            // Update the overall tab.
            this.chkbx_askedFor911.IsChecked = this.cprResults.AskedFor911;
            this.chkbx_askedForAED.IsChecked = this.cprResults.AskedForAED;
            this.chkbx_checkedResponsiveness.IsChecked = this.cprResults.CheckedResponsiveness;
            this.chkbx_checkedPulse.IsChecked = (this.cprResults.CheckedPulse != CheckPulse.No);
            this.chkbx_bodyMechPass.IsChecked = (this.cprResults.AverageBodyMech >= 0.8); // At least 80 %
            this.chkbx_depthRatePass.IsChecked = (this.cprResults.AverageRate >= Cpr.GOOD_CPR_RATE);

            this.pbar_cprScore.Value = this.cprResults.Score;
            this.pbar_bodyMech.Value = this.cprResults.AverageBodyMech;
            this.pbar_compRate.Value = this.cprResults.AverageRate;
            this.pbar_compDepth.Value = this.cprResults.AverageDepth * 39.3701; // Convert from meters to inches.

            // Set the initial compression image on the compressions tab.
            try
            {
                this.compressionsImage.WritePixels(new Int32Rect(0, 0, 640, 480), this.cprResults.CompressionSets[0].Image.ColorData, 640 * 4, 0);
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print("TestResultsCPR UpdateUI Not writing bitmap ERROR: {0}", exc.Message);
            }

            // Setup graph on compressions tab.
            try
            {
                this.xAxis.Maximum = this.cprResults.CompressionSetCount + 0.5;

                // list of key value pairs ( int = Set#, double = score )
                List<KeyValuePair<int, double>> values = new List<KeyValuePair<int, double>>(this.cprResults.CompressionSetCount);

                for (int i = 0; i < this.cprResults.CompressionSetCount; i++)
                {
                    values.Add(new KeyValuePair<int, double>(i + 1, this.cprResults.CompressionSets[i].Score.GetValueOrDefault(0)));

                    if (this.cprResults.CompressionSets[i].Image == null)
                    {
                        System.Diagnostics.Debug.Print("Missing CprResult Compression Image {0}", i);
                    }
                }

                // Add the values to the chart
                try
                {
                    List<KeyValuePair<int, double>> olditems = ((DataPointSeries)this.series_compressions).ItemsSource as List<KeyValuePair<int, double>>;

                    if (olditems != null)
                    {
                        olditems.Clear();
                    }
                }
                catch
                {
                }

                ((DataPointSeries)this.series_compressions).ItemsSource = null;
                Application.Current.Dispatcher.Invoke(new Action(delegate { }), DispatcherPriority.Background);
                ((DataPointSeries)this.series_compressions).ItemsSource = values;
                Application.Current.Dispatcher.Invoke(new Action(delegate { }), DispatcherPriority.Background);
                
                try
                {
                    ((DataPointSeries)this.series_compressions).SelectedItem = values[0];
                }
                catch (Exception exc)
                {
                    System.Diagnostics.Debug.Print("TestResultsCPR UpdateUI ERROR: {0}", exc.Message);
                }

                this.chrt_compressions.UpdateLayout();
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print("TestResultsCPR UpdateUI Adding compressions to graph failed : {0}", exc.Message);
            }

            // Notify Bindable Properties
            this.NotifyPropertyChanged("OverallFeedback");
            this.NotifyPropertyChanged("CompressionFeedback");
        }

        /// <summary>
        /// Cycles up the visual tree from the "startObject" in search for a DependencyObject of type "type".
        /// </summary>
        /// <param name="startObject"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private DependencyObject GetDependencyObjectFromVisualTree(DependencyObject startObject, Type type)
        {
            DependencyObject parent = startObject;

            while (parent != null)
            {
                if (type.IsInstanceOfType(parent))
                    break;

                else
                    parent = VisualTreeHelper.GetParent(parent);
            }

            return parent;
        }

        #region Private Action Functions

        private void column_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            /*
            if (this.series_compressions.SelectedItem != null)
            {
                KeyValuePair<int, double> point = (KeyValuePair<int, double>)this.series_compressions.SelectedItem;

                // Set the border around the column and remove it from the previous column.
                if (this.previousColumn != null)
                {
                    this.previousColumn.BorderBrush = null;
                    this.previousColumn.BorderThickness = new Thickness();
                }

                ((Border)sender).BorderBrush = Brushes.CornflowerBlue;
                ((Border)sender).BorderThickness = new Thickness(3);

                this.previousColumn = (Border)sender;

                try
                {
                    this.compressionsImage.WritePixels(new Int32Rect(0, 0, 640, 480), this.cprResults.CompressionSets[point.Key - 1].Image.ColorData, 640 * 4, 0);
                }
                catch (Exception exc)
                {
                    System.Diagnostics.Debug.Print("TestResultsCPR rc_MouseLeftButtonUp ERROR: {0}", exc.Message);
                }

                // Update the compression feedback
                this.NotifyPropertyChanged("CompressionFeedback");
            }
             */
        }

        private void btn_exit_Click(object sender, RoutedEventArgs e)
        {
            DependencyObject dependencyObject = this;
            Type type = typeof(Page);

            while (dependencyObject != null)
            {
                if (type.IsInstanceOfType(dependencyObject))
                    break;
                else
                    dependencyObject = VisualTreeHelper.GetParent(dependencyObject);
            }

            if (dependencyObject != null)
            {
                Page pg = (Page)dependencyObject;
                Uri uri = new Uri("../../MainPage3.xaml", UriKind.Relative);
                pg.NavigationService.Navigate(uri);
            }
        }

        private void btn_tryAgain_Click(object sender, RoutedEventArgs e)
        {
            Page parentPage = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

            if (parentPage as CprPage != null)
            {
                CprPage cp = parentPage as CprPage;
                cp.Lesson.ResetLesson();// Restart();
            }

            CloseResults(true);
        }

        private void btn_submit_Click(object sender, RoutedEventArgs e)
        {
            if (this.cprResults == null)
                return;

            this.btn_submit.IsEnabled = false;
            this.btn_submit.Content = "Submitting...";

            // Disable the TryAgainButton and ExitButton to be reenabled upon submission (otherwise this was causing a null reference crash before)
            this.btn_exit.IsEnabled = false;
            this.btn_tryAgain.IsEnabled = false;

            if (((App)App.Current).CurrentAccount == null || ((App)App.Current).CurrentStudent == null || ((App)App.Current).CurrentUserID <= 0)
            {
                MessageBox.Show("You cannot submit results to the database unless you are logged in as a student.");
                return;
            }

            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                //SerializeResultsAndSubmit();
                CprResult.StoreResult(this.cprResults);
            };

            worker.RunWorkerCompleted += delegate(object s, RunWorkerCompletedEventArgs args)
            {
                this.btn_submit.Content = "Success";
                this.btn_tryAgain.IsEnabled = true;
                this.btn_exit.IsEnabled = true;
            };

            worker.RunWorkerAsync();

            e.Handled = true;
        }

        #endregion

        private void series_compressions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //this.SelectedCompressionSet = this.series_compressions.SelectedItem as CprCompressionSetResult;
            System.Diagnostics.Debug.WriteLine("Selection Changed");
        }

        #endregion

    }
}