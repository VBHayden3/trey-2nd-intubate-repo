﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Media;
using System.Windows;
using Microsoft.Kinect;

namespace AIMS.Tasks.DataLogger
{
    /**************************************************************************
     * 
     *  DataLogger is a utility lesson that saves all joint positions to a local file.
     *      By default, the file saves to c:/AIMS/<YYYY-MM-DD>/<HHmmss>.txt
     *  
     *  File format: The file is a comma separated text file.
     *      The first line in the file is the headers for each column
     *  
     *  Usage: Call Record() to begin or end recording.
     * ************************************************************************/
    public class DataLogger : Lesson, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsRecording
        {
            get { return isRecording; }
            private set
            {
                isRecording = value;
                NotifyPropertyChanged("IsRecording");
            }

        }

        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        private SoundPlayer nextStageSound;
        private bool isRecording = false;
        private bool changedState = false;
        private Stopwatch stopwatch = new Stopwatch();
        private string fileName = "";

        private string outputDirectory = @"c:\AIMS";
        FileStream outputFile = null;
        StreamWriter outStream = null;
        private int maxEntries = 18000; // At 15 fps, 18,000 entries allows for up to 20 minutes in one file.
        private int numWritten = 0;

        public DataLogger()
        {
            Name = "Data Logger";
            Description = "Save skeleton movements to a file";

            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            Paused = false;
            Running = false;
            CurrentStageIndex = 0;

            nextStageSound = new System.Media.SoundPlayer(Properties.Resources.Ding);

            App.Current.Exit += App_Exiting;
        }

        public override void StartLesson()
        {
            if (KinectManager.Kinect == null)
            {
            //    MessageBox.Show("No Kinect Discovered.");
                return;
            }
            
            KinectManager.AllDataReady += KinectManager_AllDataReady;

            
            StartTime = DateTime.Now;

            KinectManager.NumStickySkeletons = 1;
        }

        public override void UpdateLesson()
        {
            if (IsRecording)
            {
                if (numWritten >= maxEntries)
                {
                    StopLogging();
                    StartLogging();
                }

                outStream.WriteLine(LogSkeleton());
                numWritten++;
            }
        }

        public override void EndLesson()
        {
            throw new NotImplementedException();
        }

        public override void ResetLesson()
        {
            throw new NotImplementedException();
        }

        public override void PauseLesson()
        {
            return;
        }

        public override void ResumeLesson()
        {
            return;
        }

        public override void WipeClean()
        {
            throw new NotImplementedException();
        }

        public override void OnTutorialVideoRequest()
        {
            return;
        }

        /// <summary>
        /// Starts or stops data logging.
        /// </summary>
        public void Record()
        {
            if (IsRecording)
            {
                StopLogging();
            }
            else
            {
                StartLogging();
            }

            IsRecording = !IsRecording;
        }

        private void KinectManager_AllDataReady()
        {
            UpdateLesson();
        }

        private void StartLogging()
        {
            outputFile = CreateOutputFile();
            outStream = new StreamWriter(outputFile);
            outStream.WriteLine(CreateFileHeader());
            numWritten = 0;
        }

        private void StopLogging()
        {
            outStream.Close();
            outputFile = null;
            outStream = null;
        }

        private FileStream CreateOutputFile()
        {
            string filePath = outputDirectory + @"\" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day;

            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            
            if (fileName.Length > 0)
            {
                filePath = filePath + @"\" + fileName + " " + DateTime.Now.ToString("HHmmss") + ".csv";                
            }
            else
            {
                filePath = filePath + @"\" + DateTime.Now.ToString("HHmmss") + ".csv";                
            }

            return File.Open(filePath, FileMode.CreateNew, FileAccess.Write);
        }

        private void App_Exiting(object sender, EventArgs e)
        {
            IsRecording = false;

            // If the application closes while logging, save the file
            if (outStream != null)
                outStream.Close();
        }

        private string CreateFileHeader()
        {
            string str = "";

            // Head
            str = PrintHeader(JointType.Head);

            // Shoulder
            str = str + ", " + PrintHeader(JointType.SpineShoulder);
            str = str + ", " + PrintHeader(JointType.ShoulderRight);
            str = str + ", " + PrintHeader(JointType.ShoulderLeft);
            
            // Right Arm
            str = str + ", " + PrintHeader(JointType.ElbowRight);
            str = str + ", " + PrintHeader(JointType.WristRight);
            str = str + ", " + PrintHeader(JointType.HandRight);

            // Left Arm
            str = str + ", " + PrintHeader(JointType.ElbowLeft);
            str = str + ", " + PrintHeader(JointType.WristLeft);
            str = str + ", " + PrintHeader(JointType.HandLeft);

            // Spine and Hips
            str = str + ", " + PrintHeader(JointType.SpineMid);
            str = str + ", " + PrintHeader(JointType.SpineBase);
            str = str + ", " + PrintHeader(JointType.HipRight);
            str = str + ", " + PrintHeader(JointType.HipLeft);

            // Right Leg
            str = str + ", " + PrintHeader(JointType.KneeRight);
            str = str + ", " + PrintHeader(JointType.AnkleRight);
            str = str + ", " + PrintHeader(JointType.FootRight);

            // Left Leg
            str = str + ", " + PrintHeader(JointType.KneeLeft);
            str = str + ", " + PrintHeader(JointType.AnkleLeft);
            str = str + ", " + PrintHeader(JointType.FootLeft);           

            return str;
        }

        private string PrintHeader(JointType jointType)
        {
            return jointType + ".X, " + jointType + ".Y, " + jointType + ".Z, " + jointType + " Tracked";
        }

        private string LogSkeleton()
        {
            string str = "";

            if (KinectManager.ClosestBody != null)
            {
                Body s = KinectManager.ClosestBody;

                // Head
                str = PrintJoint(s.Joints[JointType.Head]);

                // Shoulder
                str = str + ", " + PrintJoint(s.Joints[JointType.SpineShoulder]);
                str = str + ", " + PrintJoint(s.Joints[JointType.ShoulderRight]);
                str = str + ", " + PrintJoint(s.Joints[JointType.ShoulderLeft]);

                // Right Arm
                str = str + ", " + PrintJoint(s.Joints[JointType.ElbowRight]);
                str = str + ", " + PrintJoint(s.Joints[JointType.WristRight]);
                str = str + ", " + PrintJoint(s.Joints[JointType.HandRight]);

                // Left Arm
                str = str + ", " + PrintJoint(s.Joints[JointType.ElbowLeft]);
                str = str + ", " + PrintJoint(s.Joints[JointType.WristLeft]);
                str = str + ", " + PrintJoint(s.Joints[JointType.HandLeft]);

                // Spine and Hips
                str = str + ", " + PrintJoint(s.Joints[JointType.SpineMid]);
                str = str + ", " + PrintJoint(s.Joints[JointType.SpineBase]);
                str = str + ", " + PrintJoint(s.Joints[JointType.HipRight]);
                str = str + ", " + PrintJoint(s.Joints[JointType.HipLeft]);

                // Right Leg
                str = str + ", " + PrintJoint(s.Joints[JointType.KneeRight]);
                str = str + ", " + PrintJoint(s.Joints[JointType.AnkleRight]);
                str = str + ", " + PrintJoint(s.Joints[JointType.FootRight]);

                // Left Leg
                str = str + ", " + PrintJoint(s.Joints[JointType.KneeLeft]);
                str = str + ", " + PrintJoint(s.Joints[JointType.AnkleLeft]);
                str = str + ", " + PrintJoint(s.Joints[JointType.FootLeft]);
            }
            else
            {
                // 20 calls to print joint, plus 19 more commas above
                for (int i = 0; i < 39; i++)
                {
                    str = str + ", ";
                }
            }

            return str;
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private string PrintJoint(Joint joint)
        {
            return joint.Position.X + ", " + joint.Position.Y + ", " + joint.Position.Z + ", " + joint.TrackingState;
        }
    }
}
