﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIMS.Tasks.DataLogger
{
    /// <summary>
    /// Interaction logic for DataLoggerPage.xaml
    /// </summary>
    public partial class DataLoggerControl : UserControl
    {
        public static readonly DependencyProperty IsRecordingProperty =
            DependencyProperty.Register("IsRecording", typeof(bool), typeof(DataLoggerControl), new PropertyMetadata(false, IsRecordingChangedCallback));

        public bool IsRecording
        {
            get { return (bool)GetValue(IsRecordingProperty); }
        }

        private static void IsRecordingChangedCallback(object sender, DependencyPropertyChangedEventArgs e)
        {
            DataLoggerControl obj = sender as DataLoggerControl;

            if (obj != null)
            {
                if (obj.IsRecording)
                {
                    obj.RecordingIcon.Visibility = Visibility.Visible;
                    obj.FileNameTextBox.IsEnabled = false;
                }
                else
                {
                    obj.RecordingIcon.Visibility = Visibility.Hidden;
                    obj.FileNameTextBox.IsEnabled = true;
                }
            }
        }

        DataLogger dataLogger = new DataLogger();

        public DataLoggerControl()
        {
            InitializeComponent();

            Binding binding = new Binding("IsRecording");
            binding.Source = dataLogger;
            binding.Mode = BindingMode.OneWay;
            this.SetBinding(DataLoggerControl.IsRecordingProperty, binding);

            binding = new Binding("FileName");
            binding.Source = dataLogger;
            binding.Mode = BindingMode.OneWayToSource;
            FileNameTextBox.SetBinding(TextBox.TextProperty, binding);

            this.Loaded += DataLogger_Loaded;
        }

        private void DataLogger_Loaded(object sender, EventArgs e)
        {
            dataLogger.StartLesson();
        }

        private void RecordButton_Click(object sender, RoutedEventArgs e)
        {
            dataLogger.Record();
        }
    }
}
