﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIMS.Tasks
{
    interface ITaskResultsControl
    {
        /// <summary>
        /// Show Return, Try Again, and Submit buttons.
        /// </summary>
        void ShowButtons();
        /// <summary>
        /// Hide Return, Try Again, and Submit buttons.
        /// </summary>
        void HideButtons();
        /// <summary>
        /// Shows the results on the Task Result Control given the Result object.
        /// </summary>
        /// <param name="result">Result object to derive results from.</param>
        void ShowResults(TaskResult result, bool showButtons);
    }
}
