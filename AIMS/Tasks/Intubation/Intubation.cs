﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Media3D;
using System.Media;
using AIMS.ODU_Dev.Utilities;
using AIMS.Watchers;

namespace AIMS.Tasks.Intubation
{
    public class Intubation : Lesson
    {
        private IntubationPage intubationPage;
        public IntubationPage IntubationPage { get { return intubationPage; } set { intubationPage = value; } }

        private SoundPlayer nextStageSound;
        public SoundPlayer NextStageSound { get { return nextStageSound; } set { nextStageSound = value; } }

        private SoundPlayer stageTimeoutSound;
        public SoundPlayer StageSound { get { return stageTimeoutSound; } set { stageTimeoutSound = value; } }

        private double o2Level;
        public double O2Level { get { return o2Level; } set { o2Level = value; } }
        private double o2bonus;
        public double O2Bonus { get { return o2bonus; } set { o2bonus = value; } }

        private Point3D endotrachealTubeLocation;
        public Point3D EndotrachealTubeLocation { get { return endotrachealTubeLocation; } set { endotrachealTubeLocation = value; UpdateEndotrachealTubePosition(); } }

        private Point3D laryngoscopeLocation;
        public Point3D LaryngoscopeLocation { get { return laryngoscopeLocation; } set { laryngoscopeLocation = value; UpdateLaryngoscopePosition(); } }

        private Point3D syringeLocation;
        public Point3D SyringeLocation { get { return syringeLocation; } set { syringeLocation = value; UpdateSyringePosition(); } }

        private Point3D styletLocation;
        public Point3D StyletLocation { get { return styletLocation; } set { styletLocation = value; UpdateStyletPosition(); } }

        private Point3D manikinFaceLocation;
        public Point3D ManikinFaceLocation { get { return manikinFaceLocation; } set { manikinFaceLocation = value; UpdateManikinChinLocation(); } }

        private readonly double deductionPercentPerSecond = 4.615;
        public double DeductionPercentPerSecond { get { return deductionPercentPerSecond; } }

        private PersistentColorTracker pct;
        public PersistentColorTracker PersistentColorTracker { get { return pct; } set { pct = value; } }

        private List<BaseWatcher> lessonWatchers;
        public List<BaseWatcher> LessonWatchers { get { return lessonWatchers; } set { lessonWatchers = value; } }

        private ulong? stuckSkeletonProfileID;
        public ulong? StuckSkeletonProfileID { get { return stuckSkeletonProfileID; } set { stuckSkeletonProfileID = value; } }

        private DetectableActionCollection detectableActions;
        public DetectableActionCollection DetectableActions { get { return detectableActions; } }

        private IntubationResult result;
        public IntubationResult Result { get { return result; } set { result = value; } }

        private KinectImage calibrationImage = null;
        public KinectImage CalibrationImage { get { return calibrationImage; } set { calibrationImage = value; } }

        private double tiltHeadScore = 0;
        private double grabLaryngoscopeScore = 0;
        private double insertLaryngoscopeScore = 0;
        private double viewVocalChordsScore = 0;
        private double grabEndotrachealTubeScore = 0;
        private double insertEndotrachealTubeScore = 0;
        private double removeLaryngoscopeScore = 0;
        private double useSyringeScore = 0;
        private double removeStyletScore = 0;
        private double baggingScore = 0;
        private double chestRiseScore = 0;

        public double TiltHeadScore { get { return tiltHeadScore; } set { tiltHeadScore = value; } }
        public double GrabLaryngoscopeScore { get { return grabLaryngoscopeScore; } set { grabLaryngoscopeScore = value; } }
        public double InsertLaryngoscopeScore { get { return insertLaryngoscopeScore; } set { insertLaryngoscopeScore = value; } }
        public double ViewVocalChordsScore { get { return viewVocalChordsScore; } set { viewVocalChordsScore = value; } }
        public double GrabEndotrachealTubeScore { get { return grabEndotrachealTubeScore; } set { grabEndotrachealTubeScore = value; } }
        public double InsertEndotrachealTubeScore { get { return insertEndotrachealTubeScore; } set { insertEndotrachealTubeScore = value; } }
        public double RemoveLaryngoscopeScore { get { return removeLaryngoscopeScore; } set { removeLaryngoscopeScore = value; } }
        public double UseSyringeScore { get { return useSyringeScore; } set { useSyringeScore = value; } }
        public double RemoveStyletScore { get { return removeStyletScore; } set { removeStyletScore = value; } }
        public double BaggingScore { get { return baggingScore; } set { baggingScore = value; } }
        public double ChestRiseScore { get { return chestRiseScore; } set { chestRiseScore = value; } }

        private List<int> tiltHeadFeedbackIDs = new List<int>();
        private List<int> grabLaryngoscopeFeedbackIDs = new List<int>();
        private List<int> insertLaryngoscopeFeedbackIDs = new List<int>();
        private List<int> viewVocalChordsFeedbackIDs = new List<int>();
        private List<int> grabEndotrachealTubeFeedbackIDs = new List<int>();
        private List<int> insertEndotrachealTubeFeedbackIDs = new List<int>();
        private List<int> removeLaryngoscopeFeedbackIDs = new List<int>();
        private List<int> useSyringeFeedbackIDs = new List<int>();
        private List<int> removeStyletFeedbackIDs = new List<int>();
        private List<int> baggingFeedbackIDs = new List<int>();
        private List<int> chestRiseFeedbackIDs = new List<int>();

        public List<int> TiltHeadFeedbackIDs { get { return tiltHeadFeedbackIDs; } set { tiltHeadFeedbackIDs = value; } }
        public List<int> GrabLaryngoscopeFeedbackIDs { get { return grabLaryngoscopeFeedbackIDs; } set { grabLaryngoscopeFeedbackIDs = value; } }
        public List<int> InsertLaryngoscopeFeedbackIDs { get { return insertLaryngoscopeFeedbackIDs; } set { insertLaryngoscopeFeedbackIDs = value; } }
        public List<int> ViewVocalChordsFeedbackIDs { get { return viewVocalChordsFeedbackIDs; } set { viewVocalChordsFeedbackIDs = value; } }
        public List<int> GrabEndotrachealTubeFeedbackIDs { get { return grabEndotrachealTubeFeedbackIDs; } set { grabEndotrachealTubeFeedbackIDs = value; } }
        public List<int> InsertEndotrachealTubeFeedbackIDs { get { return insertEndotrachealTubeFeedbackIDs; } set { insertEndotrachealTubeFeedbackIDs = value; } }
        public List<int> RemoveLaryngoscopeFeedbackIDs { get { return removeLaryngoscopeFeedbackIDs; } set { removeLaryngoscopeFeedbackIDs = value; } }
        public List<int> UseSyringeFeedbackIDs { get { return useSyringeFeedbackIDs; } set { useSyringeFeedbackIDs = value; } }
        public List<int> RemoveStyletFeedbackIDs { get { return removeStyletFeedbackIDs; } set { removeStyletFeedbackIDs = value; } }
        public List<int> BaggingFeedbackIDs { get { return baggingFeedbackIDs; } set { baggingFeedbackIDs = value; } }
        public List<int> ChestRiseFeedbackIDs { get { return chestRiseFeedbackIDs; } set { chestRiseFeedbackIDs = value; } }

        private KinectImage tiltHeadImage = null;
        private KinectImage grabLaryngoscopeImage = null;
        private KinectImage insertLaryngoscopeImage = null;
        private KinectImage viewVocalChordsImage = null;
        private KinectImage grabEndotrachealTubeImage = null;
        private KinectImage insertEndotrachealTubeImage = null;
        private KinectImage removeLaryngoscopeImage = null;
        private KinectImage useSyringeImage = null;
        private KinectImage removeStyletImage = null;
        private KinectImage baggingImage = null;
        private KinectImage chestRiseImage = null;

        public KinectImage TiltHeadImage { get { return tiltHeadImage; } set { tiltHeadImage = value; } }
        public KinectImage GrabLaryngoscopeImage { get { return grabLaryngoscopeImage; } set { grabLaryngoscopeImage = value; } }
        public KinectImage InsertLaryngoscopeImage { get { return insertLaryngoscopeImage; } set { insertLaryngoscopeImage = value; } }
        public KinectImage ViewVocalChordsImage { get { return viewVocalChordsImage; } set { viewVocalChordsImage = value; } }
        public KinectImage GrabEndotrachealTubeImage { get { return grabEndotrachealTubeImage; } set { grabEndotrachealTubeImage = value; } }
        public KinectImage InsertEndotrachealTubeImage { get { return insertEndotrachealTubeImage; } set { insertEndotrachealTubeImage = value; } }
        public KinectImage RemoveLaryngoscopeImage { get { return removeLaryngoscopeImage; } set { removeLaryngoscopeImage = value; } }
        public KinectImage UseSyringeImage { get { return useSyringeImage; } set { useSyringeImage = value; } }
        public KinectImage RemoveStyletImage { get { return removeStyletImage; } set { removeStyletImage = value; } }
        public KinectImage BaggingImage { get { return baggingImage; } set { baggingImage = value; } }
        public KinectImage ChestRiseImage { get { return chestRiseImage; } set { chestRiseImage = value; } }
        
        // TREY SCALING FACTORS
        public static int SCALE_X = 3;
        public static int SCALE_Y = 2;

        public Intubation( IntubationPage page, TaskMode taskmode, MasteryLevel masterylevel )
        {
            IntubationPage = page;

            TaskMode = taskmode;
            MasteryLevel = masterylevel;

            Name = "Intubation";
            Description = "Lateral Airway Intubation";

            Stages = new List<Stage>();
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            Paused = false;
            Running = false;
            CurrentStageIndex = 0;
            ImagePath = "/Tasks/Intubation/Graphics/Intubation.png";

            this.Result = new IntubationResult();

            TargetTime = TimeSpan.FromSeconds(30.0);

            lessonWatchers = new List<BaseWatcher>();

            LaryngoscopeLocation = new Point3D();
            EndotrachealTubeLocation = new Point3D();
            SyringeLocation = new Point3D();
            StyletLocation = new Point3D();

            nextStageSound = new System.Media.SoundPlayer(Properties.Resources.Ding);
            // Load the sound.
            nextStageSound.LoadAsync();

            stageTimeoutSound = new System.Media.SoundPlayer(Properties.Resources.Ding);
            // Load the sound.
            stageTimeoutSound.LoadAsync();

            // Add all of the stages to the lesson. Score weights should add up to 100%. Stage numbers are in order 
            //     of when they are added to the list, and order of execution in the lesson.
            Stages.Add(new IntubationCalibrationStage(this) { ScoreWeight = 0.00, StageNumber = 0 });
            Stages.Add(new IntubationStage1(this) { ScoreWeight = 0.20, StageNumber = 1 });
            Stages.Add(new IntubationStage2(this) { ScoreWeight = 0.05, StageNumber = 2 });
            Stages.Add(new IntubationStage3(this) { ScoreWeight = 0.20, StageNumber = 3 });
            Stages.Add(new IntubationStage4(this) { ScoreWeight = 0.10, StageNumber = 4 });
            Stages.Add(new IntubationStage5(this) { ScoreWeight = 0.05, StageNumber = 5 });
            Stages.Add(new IntubationStage6(this) { ScoreWeight = 0.10, StageNumber = 6 });
            Stages.Add(new IntubationStage7(this) { ScoreWeight = 0.05, StageNumber = 7 });
            Stages.Add(new IntubationStage8(this) { ScoreWeight = 0.10, StageNumber = 8 });
            Stages.Add(new IntubationStage9(this) { ScoreWeight = 0.05, StageNumber = 9 });
            Stages.Add(new IntubationStage10(this) { ScoreWeight = 0.05, StageNumber = 10 });
            Stages.Add(new IntubationStage11(this) { ScoreWeight = 0.05, StageNumber = 11 });
            Stages.Add(new IntubationDisplayResultsStage(this) { ScoreWeight = 0.00, StageNumber = 12 });

            if (this.intubationPage != null)
            {
                //if( this.IntubationPage.flow.Cache == null )
                    this.IntubationPage.flow.Cache = new ThumbnailManager();    // create the flow cache (enhances re-loading preformance).

                foreach (Stage stage in this.Stages)
                {
                    this.intubationPage.flow.Add(null, stage.ImagePath);
                }
            }

            this.Complete = false;
            Stopwatch = new System.Diagnostics.Stopwatch();

            CreateDetectableActions();
        }

        public override void StartLesson()
        {
            if (!KinectManager.HasKinect)   // Kinect sensor does not exist, abort the lesson.
            {
                this.AbortLesson();
            }
            else
            {
                KinectManager.AllDataReady += KinectManager_AllDataReady;
                KinectManager.SensorNotReady += KinectManager_SensorNotReady;               

                StartTime = DateTime.Now;

                Stopwatch.Start();

                KinectManager.NumStickySkeletons = 1;

                UpdateLesson();
            }
        }

        private void CreateDetectableActions()
        {
            detectableActions = new DetectableActionCollection("All Intubation Actions");

            // USER:
            DetectableActionGroup userActions = new DetectableActionGroup("User");

                DetectableActionCategory user_BecomeSticky = new DetectableActionCategory(userActions, "Become Sticky");
                    DetectableAction user_BecameSticky = new DetectableAction("Became Sticky");
                    DetectableAction user_NoSticky = new DetectableAction("No Sticky");
                    DetectableAction user_CantSticky = new DetectableAction("Can't Sticky");
                    user_BecomeSticky.AddProperAction(user_BecameSticky);
                    user_BecomeSticky.AddInaction(user_NoSticky);
                    user_BecomeSticky.AddInaction(user_CantSticky);

                DetectableActionCategory user_TiltHead = new DetectableActionCategory(userActions, "Tilt Head");
                    DetectableAction user_TwoHandedHeadTilt = new DetectableAction("Two Handed Head Tilt");
                    DetectableAction user_NoHeadTilt = new DetectableAction("No Head Tilt");
                    DetectableAction user_RightHandedHeadTilt = new DetectableAction("Right Handed Head Tilt");
                    DetectableAction user_LeftHandedHeadTilt = new DetectableAction("Left Handed Head Tilt");
                    DetectableAction user_ChinAndHeadTilt = new DetectableAction("Chin and Head Tilt");
                    user_TiltHead.AddProperAction(user_TwoHandedHeadTilt);
                    user_TiltHead.AddInaction(user_NoHeadTilt);
                    user_TiltHead.AddImproperAction(user_RightHandedHeadTilt);
                    user_TiltHead.AddImproperAction(user_LeftHandedHeadTilt);
                    user_TiltHead.AddImproperAction(user_ChinAndHeadTilt);

                DetectableActionCategory user_GrabLaryngoscope = new DetectableActionCategory(userActions, "Grab Laryngoscope");
                    DetectableAction user_LeftHandLaryngoscopeGrab = new DetectableAction("Left Handed Laryngoscope Grab");
                    DetectableAction user_NoGrabLaryngoscope = new DetectableAction("No Grab Laryngoscope");
                    DetectableAction user_RightHandLaryngoscopeGrab = new DetectableAction("Right Handed Laryngoscope Grab");
                    user_GrabLaryngoscope.AddProperAction(user_LeftHandLaryngoscopeGrab);
                    user_GrabLaryngoscope.AddInaction(user_NoGrabLaryngoscope);
                    user_GrabLaryngoscope.AddImproperAction(user_RightHandLaryngoscopeGrab);

                DetectableActionCategory user_BendDown = new DetectableActionCategory(userActions, "Bend Down");
                    DetectableAction user_BentDownLOS = new DetectableAction("Vocal Cords Viewed");
                    DetectableAction user_NoBendDown = new DetectableAction("No Bend Down");
                    DetectableAction user_NoLineOfSight = new DetectableAction("No Line Of Sight");
                    user_BendDown.AddProperAction(user_BentDownLOS);
                    user_BendDown.AddInaction(user_NoBendDown);
                    user_BendDown.AddImproperAction(user_NoLineOfSight);

                DetectableActionCategory user_GrabEndotrachealTube = new DetectableActionCategory(userActions, "Grab Endotracheal Tube");
                    DetectableAction user_ETTRightHandGrab = new DetectableAction("Right Handed Endotracheal Tube Grab");
                    DetectableAction user_ETTNotGrabbed = new DetectableAction("Endotracheal Tube Not Grabbed");
                    DetectableAction user_ETTLeftHandGrab = new DetectableAction("Left Handed Endotracheal Tube Grab");
                    user_GrabEndotrachealTube.AddProperAction(user_ETTRightHandGrab);
                    user_GrabEndotrachealTube.AddInaction(user_ETTNotGrabbed);
                    user_GrabEndotrachealTube.AddImproperAction(user_ETTLeftHandGrab);
                
                DetectableActionCategory user_RemoveLaryngoscope = new DetectableActionCategory(userActions, "Remove Laryngoscope");
                    DetectableAction user_LeftHandedLaryngoscopeRemove = new DetectableAction("Left Handed Laryngoscope Remove");
                    DetectableAction user_LaryngoscopeNotRemoved = new DetectableAction("Laryngoscope Not Removed");
                    DetectableAction user_RightHandedLaryngoscopeRemove = new DetectableAction("Right Handed Laryngoscope Remove");
                    DetectableAction user_CantRemoveLaryngoscope = new DetectableAction("Can't Remove Laryngoscope");
                    user_RemoveLaryngoscope.AddProperAction(user_LeftHandedLaryngoscopeRemove);
                    user_RemoveLaryngoscope.AddInaction(user_LaryngoscopeNotRemoved);
                    user_RemoveLaryngoscope.AddInaction(user_CantRemoveLaryngoscope);
                    user_RemoveLaryngoscope.AddImproperAction(user_RightHandedLaryngoscopeRemove);
                
                DetectableActionCategory user_SecureEndotrachealTube = new DetectableActionCategory(userActions, "Secure Endotracheal Tube");
                    DetectableAction user_ETTSecured = new DetectableAction("Endotracheal Tube Secured");
                    DetectableAction user_ETTNotSecured = new DetectableAction("Endotracheal Tube Not Secured");
                    DetectableAction user_ETTSecuredOnlyOnce = new DetectableAction("Endotracheal Tube Secured Only Once");
                    DetectableAction user_CantSecureETT = new DetectableAction("Can't Secure Endotracheal Tube");
                    user_SecureEndotrachealTube.AddProperAction(user_ETTSecured);
                    user_SecureEndotrachealTube.AddInaction(user_ETTNotSecured);
                    user_SecureEndotrachealTube.AddInaction(user_CantSecureETT);
                    user_SecureEndotrachealTube.AddImproperAction(user_ETTSecuredOnlyOnce);
                
                DetectableActionCategory user_RemoveStylet = new DetectableActionCategory(userActions, "Remove Stylet");
                    DetectableAction user_RemovedStylet = new DetectableAction("Removed Stylet");
                    DetectableAction user_StyletNotRemoved = new DetectableAction("Stylet Not Removed");
                    DetectableAction user_StyletRemovedOutOfOrder = new DetectableAction("Stylet Removed Out Of Order");
                    user_RemoveStylet.AddProperAction(user_RemovedStylet);
                    user_RemoveStylet.AddInaction(user_StyletNotRemoved);
                    user_RemoveStylet.AddImproperAction(user_StyletRemovedOutOfOrder);

                DetectableActionCategory user_UseSyringe = new DetectableActionCategory(userActions, "Use Syringe");
                    DetectableAction user_UsedSyringe = new DetectableAction("Syringe Used");
                    DetectableAction user_SyringeNotUsed = new DetectableAction("Syringe Not Used");
                    DetectableAction user_CantUseSyringe = new DetectableAction("Can't Use Syringe");
                    DetectableAction user_SyringeUsedOutOfOrder = new DetectableAction("Syringe Used Out Of Order");
                    user_UseSyringe.AddProperAction(user_UsedSyringe);
                    user_UseSyringe.AddInaction(user_SyringeNotUsed);
                    user_UseSyringe.AddInaction(user_CantUseSyringe);
                    user_UseSyringe.AddImproperAction(user_SyringeUsedOutOfOrder);

                DetectableActionCategory user_BagTube = new DetectableActionCategory(userActions, "Bag Tube");
                    DetectableAction user_CantBagTube = new DetectableAction("Can't Bag Tube");
                    DetectableAction user_TubeNotBagged = new DetectableAction("Tube Not Bagged");
                    DetectableAction user_TubeBagged = new DetectableAction("Tube Bagged");
                    user_BagTube.AddProperAction(user_TubeBagged);
                    user_BagTube.AddInaction(user_CantBagTube);
                    user_BagTube.AddInaction(user_TubeNotBagged);

            userActions.AddActionCategory(user_BecomeSticky);
            userActions.AddActionCategory(user_TiltHead);
            userActions.AddActionCategory(user_GrabLaryngoscope);
            userActions.AddActionCategory(user_BendDown);
            userActions.AddActionCategory(user_GrabEndotrachealTube);
            userActions.AddActionCategory(user_RemoveLaryngoscope);
            userActions.AddActionCategory(user_SecureEndotrachealTube);
            userActions.AddActionCategory(user_RemoveStylet);
            userActions.AddActionCategory(user_UseSyringe);
            userActions.AddActionCategory(user_BagTube);

            // TOOLS:
            DetectableActionGroup manikinChinActions = new DetectableActionGroup("Manikin Chin");
            
                DetectableActionCategory chin_InitialLocate = new DetectableActionCategory(manikinChinActions, "Locate Initially");
                    DetectableAction chin_Located = new DetectableAction("Initially Located");
                    DetectableAction chin_NotFound = new DetectableAction("Not Found");
                    DetectableAction chin_CantFind = new DetectableAction("Can't Find");
                    chin_InitialLocate.AddProperAction(chin_Located);
                    chin_InitialLocate.AddInaction(chin_NotFound);
                    chin_InitialLocate.AddInaction(chin_CantFind);
                
                DetectableActionCategory chin_TiltBack = new DetectableActionCategory(manikinChinActions, "Tilt Back");
                    DetectableAction chin_TiltedBack = new DetectableAction("Tilted Back");
                    DetectableAction chin_ExcessiveTilt = new DetectableAction("Excessive Tilt");
                    DetectableAction chin_NoTilt = new DetectableAction("No Tilt");
                    DetectableAction chin_CantTilt = new DetectableAction("Can't Tilt");
                    chin_TiltBack.AddProperAction(chin_TiltedBack);
                    chin_TiltBack.AddInaction(chin_NoTilt);
                    chin_TiltBack.AddInaction(chin_CantTilt);
                    chin_TiltBack.AddImproperAction(chin_ExcessiveTilt);

                    manikinChinActions.AddActionCategory(chin_InitialLocate);
                    manikinChinActions.AddActionCategory(chin_TiltBack);

            DetectableActionGroup laryngoscopeActions = new DetectableActionGroup("Laryngoscope");

                DetectableActionCategory laryngoscope_PickUp = new DetectableActionCategory(laryngoscopeActions, "Pick Up");
                    DetectableAction laryngoscope_PickedUp = new DetectableAction("Picked Up");
                    DetectableAction laryngoscope_NotPickedUp = new DetectableAction("Not Picked Up");
                    DetectableAction laryngoscope_RightSidePickup = new DetectableAction("Right Side Pickup");
                    laryngoscope_PickUp.AddProperAction(laryngoscope_PickedUp);
                    laryngoscope_PickUp.AddInaction(laryngoscope_NotPickedUp);
                    laryngoscope_PickUp.AddImproperAction(laryngoscope_RightSidePickup);
                DetectableActionCategory laryngoscope_Insert = new DetectableActionCategory(laryngoscopeActions, "Insert");
                    DetectableAction laryngoscope_InsertedRightSide = new DetectableAction("Inserted Right Side");
                    DetectableAction laryngoscope_NotInserted = new DetectableAction("Not Inserted");
                    DetectableAction laryngoscope_CantInsert = new DetectableAction("Can't Insert");
                    DetectableAction laryngoscope_LeftSideInsertion = new DetectableAction("Inserted Left Side");
                    laryngoscope_Insert.AddProperAction(laryngoscope_InsertedRightSide);
                    laryngoscope_Insert.AddInaction(laryngoscope_NotInserted);
                    laryngoscope_Insert.AddInaction(laryngoscope_CantInsert);
                    laryngoscope_Insert.AddImproperAction(laryngoscope_LeftSideInsertion);
                DetectableActionCategory laryngoscope_Sweep = new DetectableActionCategory(laryngoscopeActions, "Sweep");
                    DetectableAction laryngoscope_Swept = new DetectableAction("Swept");
                    DetectableAction laryngoscope_NotSwept = new DetectableAction("Not Swept");
                    DetectableAction laryngoscope_CantSweep = new DetectableAction("Can't Sweep");
                    laryngoscope_Sweep.AddProperAction(laryngoscope_Swept);
                    laryngoscope_Sweep.AddInaction(laryngoscope_NotSwept);
                    laryngoscope_Sweep.AddInaction(laryngoscope_CantSweep);
                DetectableActionCategory laryngoscope_Lift = new DetectableActionCategory(laryngoscopeActions, "Lift");
                    DetectableAction laryngoscope_Lifted = new DetectableAction("Lifted");
                    DetectableAction laryngoscope_CantLift = new DetectableAction("Can't Lift");
                    DetectableAction laryngoscope_NotLifted = new DetectableAction("Not Lifted");
                    DetectableAction laryngoscope_TeethFulcrum = new DetectableAction("Fulcruming On Teeth");
                    laryngoscope_Lift.AddProperAction(laryngoscope_Lifted);
                    laryngoscope_Lift.AddInaction(laryngoscope_CantLift);
                    laryngoscope_Lift.AddInaction(laryngoscope_NotLifted);
                    laryngoscope_Lift.AddImproperAction(laryngoscope_TeethFulcrum);
                DetectableActionCategory laryngoscope_Remove = new DetectableActionCategory(laryngoscopeActions, "Remove");
                    DetectableAction laryngoscope_RemovedLeft = new DetectableAction("Removed Left");
                    DetectableAction laryngoscope_NotRemoved = new DetectableAction("Not Removed");
                    DetectableAction laryngoscope_CantRemove = new DetectableAction("Can't Remove");
                    DetectableAction laryngoscope_RemovedRight = new DetectableAction("Removed Right");
                    DetectableAction laryngoscope_Removed = new DetectableAction("Removed");
                    laryngoscope_Remove.AddProperAction(laryngoscope_RemovedLeft);
                    laryngoscope_Remove.AddInaction(laryngoscope_NotRemoved);
                    laryngoscope_Remove.AddInaction(laryngoscope_CantRemove);
                    laryngoscope_Remove.AddImproperAction(laryngoscope_RemovedRight);
                    laryngoscope_Remove.AddImproperAction(laryngoscope_Removed);
                DetectableActionCategory laryngoscope_SetDown = new DetectableActionCategory(laryngoscopeActions, "Set Down");
                    DetectableAction laryngoscope_SetDownLeftSide = new DetectableAction("Set Down Left Side");
                    DetectableAction laryngoscope_CantSetDown = new DetectableAction("Can't Set Down");
                    DetectableAction laryngoscope_SetDownRight = new DetectableAction("Set Down Right Side");
                    DetectableAction laryngoscope_NotSetDown = new DetectableAction("Not Set Down");
                    laryngoscope_SetDown.AddProperAction(laryngoscope_SetDownLeftSide);
                    laryngoscope_SetDown.AddInaction(laryngoscope_CantSetDown);
                    laryngoscope_SetDown.AddInaction(laryngoscope_NotSetDown);
                    laryngoscope_SetDown.AddImproperAction(laryngoscope_SetDownRight);

            laryngoscopeActions.AddActionCategory(laryngoscope_PickUp);
            laryngoscopeActions.AddActionCategory(laryngoscope_Insert);
            laryngoscopeActions.AddActionCategory(laryngoscope_Sweep);
            laryngoscopeActions.AddActionCategory(laryngoscope_Lift);
            laryngoscopeActions.AddActionCategory(laryngoscope_Remove);
            laryngoscopeActions.AddActionCategory(laryngoscope_SetDown);
            
            DetectableActionGroup endotrachealTubeActions = new DetectableActionGroup("Endotracheal Tube");
                DetectableActionCategory endotrachealTube_PickUp = new DetectableActionCategory(endotrachealTubeActions, "Pick Up");
                    DetectableAction endotrachealTube_PickedUpRight = new DetectableAction("Picked Up Right");
                    DetectableAction endotrachealTube_NotPickedUp = new DetectableAction("Not Picked Up");
                    DetectableAction endotrachealTube_PickedUpLeft = new DetectableAction("Picked Up Left");
                    endotrachealTube_PickUp.AddProperAction(endotrachealTube_PickedUpRight);
                    endotrachealTube_PickUp.AddInaction(endotrachealTube_NotPickedUp);
                    endotrachealTube_PickUp.AddImproperAction(endotrachealTube_PickedUpLeft);
                DetectableActionCategory endotrachealTube_Insert = new DetectableActionCategory(endotrachealTubeActions, "Insert");
                    DetectableAction endotrachealTube_InsertedCenter = new DetectableAction("Inserted Center");
                    DetectableAction endotrachealTube_NotInserted = new DetectableAction("Not Inserted");
                    DetectableAction endotrachealTube_CantInsert = new DetectableAction("Cant Insert");
                    DetectableAction endotrachealTube_InsertedLeft = new DetectableAction("Inserted Left");
                    DetectableAction endotrachealTube_InsertedRight = new DetectableAction("Inserted Right");
                    endotrachealTube_Insert.AddProperAction(endotrachealTube_InsertedCenter);
                    endotrachealTube_Insert.AddInaction(endotrachealTube_NotInserted);
                    endotrachealTube_Insert.AddInaction(endotrachealTube_CantInsert);
                    endotrachealTube_Insert.AddImproperAction(endotrachealTube_InsertedLeft);
                    endotrachealTube_Insert.AddImproperAction(endotrachealTube_InsertedRight);
                DetectableActionCategory endotrachealTube_DontRemove = new DetectableActionCategory(endotrachealTubeActions, "Don't Remove");
                    DetectableAction endotrachealTube_NotRemoved = new DetectableAction("Not Removed");
                    DetectableAction endotrachealTube_CantRemove = new DetectableAction("Can't Remove");
                    DetectableAction endotrachealTube_Removed = new DetectableAction("Removed");
                    endotrachealTube_DontRemove.AddProperAction(endotrachealTube_NotRemoved);
                    endotrachealTube_DontRemove.AddInaction(endotrachealTube_CantRemove);
                    endotrachealTube_DontRemove.AddImproperAction(endotrachealTube_Removed);
                DetectableActionCategory endotrachealTube_DontSetDown = new DetectableActionCategory(endotrachealTubeActions, "Don't Set Down");
                    DetectableAction endotrachealTube_NotSetDown = new DetectableAction("Not Set Down");
                    DetectableAction endotrachealTube_CantSetDown = new DetectableAction("Can't Set Down");
                    DetectableAction endotrachealTube_SetDown = new DetectableAction("Set Down");
                    endotrachealTube_DontSetDown.AddProperAction(endotrachealTube_NotSetDown);
                    endotrachealTube_DontSetDown.AddImproperAction(endotrachealTube_CantSetDown);
                    endotrachealTube_DontSetDown.AddInaction(endotrachealTube_SetDown);
                    

            endotrachealTubeActions.AddActionCategory(endotrachealTube_PickUp);
            endotrachealTubeActions.AddActionCategory(endotrachealTube_Insert);
            endotrachealTubeActions.AddActionCategory(endotrachealTube_DontRemove);
            endotrachealTubeActions.AddActionCategory(endotrachealTube_DontSetDown);

            DetectableActionGroup styletActions = new DetectableActionGroup("Stylet");
                DetectableActionCategory stylet_Remove = new DetectableActionCategory(styletActions, "Remove");
                    DetectableAction stylet_Removed = new DetectableAction("Removed");
                    DetectableAction stylet_NotRemoved = new DetectableAction("Not Removed");
                    DetectableAction stylet_CantRemove = new DetectableAction("Cant Remove");
                    DetectableAction stylet_RemovedOutOfOrder = new DetectableAction("Removed Out Of Order");
                    stylet_Remove.AddProperAction(stylet_Removed);
                    stylet_Remove.AddInaction(stylet_NotRemoved);
                    stylet_Remove.AddInaction(stylet_CantRemove);
                    stylet_Remove.AddImproperAction(stylet_RemovedOutOfOrder);

            styletActions.AddActionCategory(stylet_Remove);

            DetectableActionGroup syringeActions = new DetectableActionGroup("Syringe");
                DetectableActionCategory syringe_PickUp = new DetectableActionCategory(syringeActions, "Pick Up");
                    DetectableAction syringe_PickedUp = new DetectableAction("Picked Up");
                    DetectableAction syringe_NotPickedUp = new DetectableAction("Not Picked Up");
                    DetectableAction syringe_CantPickUp = new DetectableAction("Can't Pick Up");
                    DetectableAction syringe_OutOfOrderPickup = new DetectableAction("Out Of Order Pickup");
                    syringe_PickUp.AddProperAction(syringe_PickedUp);
                    syringe_PickUp.AddInaction(syringe_NotPickedUp);
                    syringe_PickUp.AddInaction(syringe_CantPickUp);
                    syringe_PickUp.AddImproperAction(syringe_OutOfOrderPickup);
                DetectableActionCategory syringe_Use = new DetectableActionCategory(syringeActions, "Use");
                    DetectableAction syringe_Used = new DetectableAction("Used");
                    DetectableAction syringe_NotUsed = new DetectableAction("Not Used");
                    DetectableAction syringe_CantUse = new DetectableAction("Can't Use");
                    syringe_Use.AddProperAction(syringe_Used);
                    syringe_Use.AddInaction(syringe_NotUsed);
                    syringe_Use.AddInaction(syringe_CantUse);

                DetectableActionCategory syringe_SetDown = new DetectableActionCategory(syringeActions, "Set Down");
                    DetectableAction syringe_PutDown = new DetectableAction("Put Down");
                    DetectableAction syringe_NotSetDown = new DetectableAction("Not Set Down");
                    DetectableAction syringe_CantSetDown = new DetectableAction("Can't Set Down");
                    syringe_SetDown.AddProperAction(syringe_PutDown);
                    syringe_SetDown.AddInaction(syringe_NotSetDown);
                    syringe_SetDown.AddInaction(syringe_CantSetDown);

            syringeActions.AddActionCategory(syringe_PickUp);
            syringeActions.AddActionCategory(syringe_Use);
            syringeActions.AddActionCategory(syringe_SetDown);

            DetectableActionGroup chestActions = new DetectableActionGroup("Manikin Chest");
                DetectableActionCategory chest_RiseAndFall = new DetectableActionCategory(chestActions, "Rise and Fall");
                    DetectableAction chest_RoseAndFell = new DetectableAction("Rose and Fell");
                    DetectableAction chest_NoRise = new DetectableAction("No Rise");
                    DetectableAction chest_CantRise = new DetectableAction("Cant Rise");
                    chest_RiseAndFall.AddProperAction(chest_RoseAndFell);
                    chest_RiseAndFall.AddInaction(chest_NoRise);
                    chest_RiseAndFall.AddInaction(chest_CantRise);

            chestActions.AddActionCategory(chest_RiseAndFall);

            detectableActions.AddGroup(userActions);
            detectableActions.AddGroup(manikinChinActions);
            detectableActions.AddGroup(laryngoscopeActions);
            detectableActions.AddGroup(endotrachealTubeActions);
            detectableActions.AddGroup(styletActions);
            detectableActions.AddGroup(syringeActions);
            detectableActions.AddGroup(chestActions);

            if (this.IntubationPage != null)
            {
                this.IntubationPage.LessonDetectableActionsControl.SetActionCollection( detectableActions );
            }
        }

        private void KinectManager_AllDataReady()
        {
            UpdateLesson();
        }

        private void KinectManager_SensorNotReady()
        {
            if (!this.Complete)
            {
                this.AbortLesson();
            }
        }

        public override void UpdateLesson()
        {

            //System.Diagnostics.Debug.WriteLine("Intubation -> UpdateLesson()... UpdateSkeleton()...");
            this.IntubationPage.skeletonOverlay.UpdateSkeleton();

            //System.Diagnostics.Debug.WriteLine("Intubation -> UpdateLesson()... UpdateManikinHeadCrosshair()...");
            this.IntubationPage.manikinHeadCrosshairOverlay.UpdateManikinHeadCrosshair();

            //System.Diagnostics.Debug.WriteLine("Intubation -> UpdateLesson() UpdateZones()...");
            this.IntubationPage.zoneOverlay.UpdateZones();

            if (!this.Complete)
            {
                // Update the stage specific watchers.
                for (int i = this.CurrentStageIndex; i < this.Stages.Count; i++)    // update the current stage's watchers, and then all the following ones.
                {
                    Stage stage = this.Stages[i];

                    int numStagesAwayFromCurrent = (int)System.Math.Abs(i - this.CurrentStageIndex);

                    if (stage != null)
                    {
                        // For each stage, go through and update the state of all watchers relevant to which lesson is current.
                        // It is up to the stage itself to identify which watcher is relevant at various positionings of the current stage.
                        stage.UpdateWatcherStates(numStagesAwayFromCurrent == 0, numStagesAwayFromCurrent == 1, numStagesAwayFromCurrent >= 2);
                    }
                }

                for (int i = 0; i < this.CurrentStageIndex; i++)    // update the ones beforehand, too.
                {
                    Stage stage = this.Stages[i];

                    int numStagesAwayFromCurrent = (int)System.Math.Abs(i - this.CurrentStageIndex);

                    if (stage != null)
                    {
                        // For each stage, go through and update the state of all watchers relevant to which lesson is current.
                        // It is up to the stage itself to identify which watcher is relevant at various positionings of the current stage.
                        stage.UpdateWatcherStates(numStagesAwayFromCurrent == 0, numStagesAwayFromCurrent == 1, numStagesAwayFromCurrent >= 2);
                    }
                }

                // Go through and update the state of all lesson-wide watchers.
                foreach (BaseWatcher watcher in lessonWatchers)
                {
                    watcher.UpdateWatcher();
                }
                
                UpdateO2Meter();

                this.Stages[this.CurrentStageIndex].UpdateStage();
            }
        }


        private void UpdateO2Meter()
        {
            if (this.Running && CurrentStageIndex > 0)
            {
                O2Level = CalculateCO2Level();
                this.IntubationPage.PatientHealthBar.Value = O2Level;
            }
        }

        public void PlayNextStageSound()
        {
            nextStageSound.Play();
        }

        public void PlayStageTimeoutSound()
        {
            stageTimeoutSound.Play();
        }

        private double CalculateCO2Level()
        {
            return 1 - System.Math.Min(System.Math.Max(0, this.Stopwatch.Elapsed.TotalSeconds / this.TargetTime.TotalSeconds + this.O2Bonus), 1);
        }

        /// <summary>
        /// Calculate the total score by scaling each individual stage's score depending on it's worth.
        /// </summary>
        /// <returns></returns>
        private double CalculateResultTotalScore()
        {
            double score = 0;

            if (this.Stages != null)
            {
                TimeSpan totalTime = TimeSpan.Zero;

                for (int i = 1; i < (this.Stages.Count - 1); i++)
                {
                    totalTime += this.Stages[i].StageScore.TimeInStage;
                }

                score += Stages[0].StageScore.Score * 0;

                if (totalTime > this.TargetTime)
                {
                    for (int i = 1; i < Stages.Count - 1; i++)
                    {
                        if (Stages[i].StageScore.TimeInStage > Stages[i].TargetTime)
                        {
                            double secondsOverTarget = Stages[i].StageScore.TimeInStage.TotalSeconds - Stages[i].TargetTime.TotalSeconds;
                            double pointsOff = System.Math.Max(0, (100 - (secondsOverTarget * deductionPercentPerSecond)));

                            Stages[i].StageScore.Score *= System.Math.Min(pointsOff / 100, 1);
                        }
                    }
                }

                // *** BPC *** Tally up the scores across all of the stages.
                foreach (Stage stage in this.Stages)
                {
                    score += stage.StageScore.Score * stage.ScoreWeight;
                }
            }

            return score;
        }

        /// <summary>
        /// Refreshes the Manikin's Chin Crosshair on the page's ManikinHeadCrosshairOverlay using the currently set ManikinFaceLocation.
        /// </summary>
        private void UpdateManikinChinLocation()
        {
            if (this.IntubationPage != null)
            {
                if (this.IntubationPage.manikinHeadCrosshairOverlay != null)
                {
                    this.IntubationPage.manikinHeadCrosshairOverlay.UpdateManikinHeadCrosshair(this.ManikinFaceLocation.X, this.ManikinFaceLocation.Y, this.ManikinFaceLocation.Z);
                }
            }
        }

        /// <summary>
        /// Refreshes the Laryngoscope's Crosshair on the page's IntubationToolsOverlay using the currently set LaryngoscopeLocation.
        /// </summary>
        private void UpdateLaryngoscopePosition()
        {
            if (this.IntubationPage != null)
            {
                if (this.IntubationPage.IntubationToolsOverlay != null)
                {
                    this.IntubationPage.IntubationToolsOverlay.UpdateLaryngoscopeCrosshair(this.LaryngoscopeLocation.X, this.LaryngoscopeLocation.Y, this.LaryngoscopeLocation.Z);
                }
            }
        }

        /// <summary>
        /// Refreshes the Endotracheal Tube's Crosshair on the page's IntubationToolsOverlay using the currently set EndotrachealTubeLocation.
        /// </summary>
        private void UpdateEndotrachealTubePosition()
        {
            if (this.IntubationPage != null)
            {
                if (this.IntubationPage.IntubationToolsOverlay != null)
                {
                    this.IntubationPage.IntubationToolsOverlay.UpdateEndotrachealTubeCrosshair(this.EndotrachealTubeLocation.X, this.EndotrachealTubeLocation.Y, this.EndotrachealTubeLocation.Z);
                }
            }
        }

        /// <summary>
        /// Refreshes the Syringe's Crosshair on the page's IntubationToolsOverlay using the currently set SyringeLocation.
        /// </summary>
        private void UpdateSyringePosition()
        {
            if (this.IntubationPage != null)
            {
                if (this.IntubationPage.IntubationToolsOverlay != null)
                {
                    this.IntubationPage.IntubationToolsOverlay.UpdateSyringeCrosshair(this.SyringeLocation.X, this.SyringeLocation.Y, this.SyringeLocation.Z);
                }
            }
        }

        /// <summary>
        /// Refreshes the Stylet's Crosshair on the page's IntubationToolsOverlay using the currently set StyletLocation.
        /// </summary>
        private void UpdateStyletPosition()
        {
            if (this.IntubationPage != null)
            {
                if (this.IntubationPage.IntubationToolsOverlay != null)
                {
                    this.IntubationPage.IntubationToolsOverlay.UpdateStyletCrosshair(this.StyletLocation.X, this.StyletLocation.Y, this.StyletLocation.Z);
                }
            }
        }

        private void ShowResults()
        {
            if (this.IntubationPage != null)
            {
                this.IntubationPage.ShowResults(this.Result);
            }
        }

        public override void EndLesson()
        {
            this.Complete = true;

            this.Stopwatch.Stop();
            this.Running = false;

            this.EndTime = DateTime.Now;

            this.Result.StartTime = this.StartTime;
            this.Result.EndTime = this.EndTime;

            this.Result.TaskMode = this.TaskMode;
            this.Result.MasteryLevel = this.MasteryLevel;

            if( ((App)App.Current).CurrentStudent != null )
                this.Result.StudentID = ((App)App.Current).CurrentStudent.ID;

            // Set the Scores
            this.Result.TiltHeadScore = this.tiltHeadScore;
            this.Result.GrabLaryngoscopeScore = this.grabLaryngoscopeScore;
            this.Result.InsertLaryngoscopeScore = this.insertLaryngoscopeScore;
            this.Result.ViewVocalChordsScore = this.viewVocalChordsScore;
            this.Result.GrabEndotrachealTubeScore = this.grabEndotrachealTubeScore;
            this.Result.InsertEndotrachealTubeScore = this.insertEndotrachealTubeScore;
            this.Result.RemoveLaryngoscopeScore = this.removeLaryngoscopeScore;
            this.Result.UseSyringeScore = this.useSyringeScore;
            this.Result.RemoveStyletScore = this.removeStyletScore;
            this.Result.BaggingScore = this.baggingScore;
            this.Result.ChestRiseScore = this.chestRiseScore;

            // Set the Images
            this.Result.CalibrationImage = this.CalibrationImage;

            this.Result.TiltHeadImage = this.tiltHeadImage;
            this.Result.GrabLaryngoscopeImage = this.grabLaryngoscopeImage;
            this.Result.InsertLaryngoscopeImage = this.insertLaryngoscopeImage;
            this.Result.ViewVocalChordsImage = this.viewVocalChordsImage;
            this.Result.GrabEndotrachealTubeImage = this.grabEndotrachealTubeImage;
            this.Result.InsertEndotrachealTubeImage = this.insertEndotrachealTubeImage;
            this.Result.RemoveLaryngoscopeImage = this.removeLaryngoscopeImage;
            this.Result.UseSyringeImage = this.useSyringeImage;
            this.Result.RemoveStyletImage = this.removeStyletImage;
            this.Result.BaggingImage = this.baggingImage;
            this.Result.ChestRiseImage = this.chestRiseImage;

            // Set the FeedbackID collection
            this.Result.TiltHeadFeedbackIDs = this.tiltHeadFeedbackIDs;
            this.Result.GrabLaryngoscopeFeedbackIDs = this.grabLaryngoscopeFeedbackIDs;
            this.Result.InsertLaryngoscopeFeedbackIDs = this.insertLaryngoscopeFeedbackIDs;
            this.Result.ViewVocalChordsFeedbackIDs = this.viewVocalChordsFeedbackIDs;
            this.Result.GrabEndotrachealTubeFeedbackIDs = this.grabEndotrachealTubeFeedbackIDs;
            this.Result.InsertEndotrachealTubeFeedbackIDs = this.insertEndotrachealTubeFeedbackIDs;
            this.Result.RemoveLaryngoscopeFeedbackIDs = this.removeLaryngoscopeFeedbackIDs;
            this.Result.UseSyringeFeedbackIDs = this.useSyringeFeedbackIDs;
            this.Result.RemoveStyletFeedbackIDs = this.removeStyletFeedbackIDs;
            this.Result.BaggingFeedbackIDs = this.baggingFeedbackIDs;
            this.Result.ChestRiseFeedbackIDs = this.chestRiseFeedbackIDs;

            double unscaledByTimeScore = CalculateTotalScore();

            TimeSpan intubateTime = TimeSpan.Zero;

            for (int i = 1; i < this.Stages.Count - 1; i++)    // each stages minus Calibration and Display Results
            {
                intubateTime.Add( this.Stages[i].Stopwatch.Elapsed );   // accumulate the time spent in each stage.
            }

            if (intubateTime > this.TargetTime)   // the time of the task took longer than the targeted time defined for this task.
            {
                TimeSpan timeOverTarget = intubateTime - this.TargetTime;

                tiltHeadScore = ScaleScoreBackByTime(tiltHeadScore, (this.Stages[1].Stopwatch.Elapsed - this.Stages[1].TargetTime));
                grabLaryngoscopeScore = ScaleScoreBackByTime(grabLaryngoscopeScore, (this.Stages[2].Stopwatch.Elapsed - this.Stages[2].TargetTime));
                insertLaryngoscopeScore = ScaleScoreBackByTime(insertLaryngoscopeScore, (this.Stages[3].Stopwatch.Elapsed - this.Stages[3].TargetTime));
                viewVocalChordsScore = ScaleScoreBackByTime(viewVocalChordsScore, (this.Stages[4].Stopwatch.Elapsed - this.Stages[4].TargetTime));
                grabEndotrachealTubeScore = ScaleScoreBackByTime(grabEndotrachealTubeScore, (this.Stages[5].Stopwatch.Elapsed - this.Stages[5].TargetTime));
                insertEndotrachealTubeScore = ScaleScoreBackByTime(insertEndotrachealTubeScore, (this.Stages[6].Stopwatch.Elapsed - this.Stages[6].TargetTime));
                removeLaryngoscopeScore = ScaleScoreBackByTime(removeLaryngoscopeScore, (this.Stages[7].Stopwatch.Elapsed - this.Stages[7].TargetTime));
                useSyringeScore = ScaleScoreBackByTime(useSyringeScore, (this.Stages[8].Stopwatch.Elapsed - this.Stages[8].TargetTime));
                removeStyletScore = ScaleScoreBackByTime(removeStyletScore, (this.Stages[9].Stopwatch.Elapsed - this.Stages[9].TargetTime));
                //  baggingScore = ScaleScoreBackByTime( baggingScore, ( this.Stages[10].Stopwatch.Elapsed - this.Stages[10].TargetTime ) );    // Bagging Stage is un-effected by the target time.
                //  chestRiseScore = ScaleScoreBackByTime( chestRiseScore, ( this.Stages[11].Stopwatch.Elapsed - this.Stages[11].TargetTime ) );    // Chest Rise Stage is un-effected by the target time.

                // Recalculate the scores based on the newer scaled values.
                this.Result.Score = CalculateTotalScore();
            }
            else
            {
                this.Result.Score = unscaledByTimeScore;
            }

            System.Diagnostics.Debug.WriteLine(String.Format("Intubation Score: {0:0.0%}", this.Result.Score));

            if (this.Result.Score >= 1)
            {
                // Proper performance.
                this.Result.OverallFeedbackIDs.Add(18);  // You have properly performed Intubation.
                System.Diagnostics.Debug.WriteLine("You have properly performed Intubation.");
            }
            else
            {
                // The performance wasn't 100%.

                if (this.Result.ChestRiseScore == 1.0)
                {
                    // the intubation was effective.. now to determine why the score went ary.

                    // scaled back by exceeding the target time.
                    if (unscaledByTimeScore > this.Result.Score)
                    {
                        this.Result.OverallFeedbackIDs.Add(25); // You did not complete the Intubation in a timely fashion.
                        System.Diagnostics.Debug.WriteLine("You did not complete the Intubation in a timely fashion.");
                    }

                    if (this.Result.GrabLaryngoscopeScore < 1 || this.Result.GrabEndotrachealTubeScore < 1 || this.Result.UseSyringeScore < 1 || this.Result.RemoveStyletScore < 1 || this.Result.RemoveLaryngoscopeScore < 1 || this.Result.TiltHeadScore < 1 || this.Result.ViewVocalChordsScore < 1)
                    {
                        this.Result.OverallFeedbackIDs.Add(26); // You did not use proper technique while Intubating.
                        System.Diagnostics.Debug.WriteLine("You did not use proper technique while Intubating.");
                    }

                    if (this.Result.InsertLaryngoscopeScore < 1 || this.Result.InsertEndotrachealTubeScore < 1)
                    {
                        this.Result.OverallFeedbackIDs.Add(27); // You did not use proper tool positioning while Intubating.
                        System.Diagnostics.Debug.WriteLine("You did not use proper tool positioning while Intubating.");
                    }
                }
                else
                {
                    this.Result.OverallFeedbackIDs.Add(19); // You have improperly performed Intubation.
                    System.Diagnostics.Debug.WriteLine("You have improperly performed Intubation.");
                }
            }
            
            System.Diagnostics.Debug.WriteLine(String.Format("[{0}] Score: {1:0.0%}, Image: {2}, FeedbackIDs: {3}.", "TiltHead", this.TiltHeadScore, this.TiltHeadImage == null ? "null" : "{KinectImage}", CreateSingleFeedbackIDsString(this.TiltHeadFeedbackIDs)));
            System.Diagnostics.Debug.WriteLine(String.Format("[{0}] Score: {1:0.0%}, Image: {2}, FeedbackIDs: {3}.", "GrabLaryngoscope", this.GrabLaryngoscopeScore, this.GrabLaryngoscopeImage == null ? "null" : "{KinectImage}", CreateSingleFeedbackIDsString(this.GrabLaryngoscopeFeedbackIDs)));
            System.Diagnostics.Debug.WriteLine(String.Format("[{0}] Score: {1:0.0%}, Image: {2}, FeedbackIDs: {3}.", "InsertLaryngoscope", this.InsertLaryngoscopeScore, this.InsertLaryngoscopeImage == null ? "null" : "{KinectImage}", CreateSingleFeedbackIDsString(this.InsertLaryngoscopeFeedbackIDs)));
            System.Diagnostics.Debug.WriteLine(String.Format("[{0}] Score: {1:0.0%}, Image: {2}, FeedbackIDs: {3}.", "ViewVocalChords", this.ViewVocalChordsScore, this.ViewVocalChordsImage == null ? "null" : "{KinectImage}", CreateSingleFeedbackIDsString(this.ViewVocalChordsFeedbackIDs)));
            System.Diagnostics.Debug.WriteLine(String.Format("[{0}] Score: {1:0.0%}, Image: {2}, FeedbackIDs: {3}.", "GrabEndotrachealTube", this.GrabEndotrachealTubeScore, this.GrabEndotrachealTubeImage == null ? "null" : "{KinectImage}", CreateSingleFeedbackIDsString(this.GrabEndotrachealTubeFeedbackIDs)));
            System.Diagnostics.Debug.WriteLine(String.Format("[{0}] Score: {1:0.0%}, Image: {2}, FeedbackIDs: {3}.", "InsertEndotrachealTube", this.InsertEndotrachealTubeScore, this.InsertEndotrachealTubeImage == null ? "null" : "{KinectImage}", CreateSingleFeedbackIDsString(this.InsertEndotrachealTubeFeedbackIDs)));
            System.Diagnostics.Debug.WriteLine(String.Format("[{0}] Score: {1:0.0%}, Image: {2}, FeedbackIDs: {3}.", "RemoveLaryngoscope", this.RemoveLaryngoscopeScore, this.RemoveLaryngoscopeImage == null ? "null" : "{KinectImage}", CreateSingleFeedbackIDsString(this.RemoveLaryngoscopeFeedbackIDs)));
            System.Diagnostics.Debug.WriteLine(String.Format("[{0}] Score: {1:0.0%}, Image: {2}, FeedbackIDs: {3}.", "UseSyringe", this.UseSyringeScore, this.UseSyringeImage == null ? "null" : "{KinectImage}", CreateSingleFeedbackIDsString(this.UseSyringeFeedbackIDs)));
            System.Diagnostics.Debug.WriteLine(String.Format("[{0}] Score: {1:0.0%}, Image: {2}, FeedbackIDs: {3}.", "RemoveStylet", this.RemoveStyletScore, this.RemoveStyletImage == null ? "null" : "{KinectImage}", CreateSingleFeedbackIDsString(this.RemoveStyletFeedbackIDs)));
            System.Diagnostics.Debug.WriteLine(String.Format("[{0}] Score: {1:0.0%}, Image: {2}, FeedbackIDs: {3}.", "Bagging", this.BaggingScore, this.BaggingImage == null ? "null" : "{KinectImage}", CreateSingleFeedbackIDsString(this.BaggingFeedbackIDs)));
            System.Diagnostics.Debug.WriteLine(String.Format("[{0}] Score: {1:0.0%}, Image: {2}, FeedbackIDs: {3}.", "ChestRise", this.ChestRiseScore, this.ChestRiseImage == null ? "null" : "{KinectImage}", CreateSingleFeedbackIDsString(this.ChestRiseFeedbackIDs)));

            System.Diagnostics.Debug.WriteLine(String.Format("Intubate Time: {0}", intubateTime));
            
            this.ShowResults();
        }

        private string CreateSingleFeedbackIDsString(List<int> feedbackids)
        {
            if (feedbackids == null)
                return "null";

            if( feedbackids.Count <= 0 )
                return "none";

            string feedbackidsstring = "";
            for( int i = 0; i < feedbackids.Count; i++ )
            {
                feedbackidsstring += (i > 0 ? feedbackids[i].ToString() + ", " : feedbackids[i].ToString());
            }

            return feedbackidsstring;

        }

        private double ScaleScoreBackByTime( double unscaledScore, TimeSpan timeOverTarget )
        {
            double scaledScore = unscaledScore;

            double pointsOff = System.Math.Max(0, (100 - (timeOverTarget.TotalSeconds * deductionPercentPerSecond)));

            scaledScore *= System.Math.Min(pointsOff / 100, 1);  // scale the score of the stage.

            return scaledScore;
        }

        private void CalculateOverallFeedback()
        {
            if (this.Result.Score == 100)
            {
               
            }
        }

        private double CalculateTotalScore()
        {
            double totalscore = 0;

            double tiltHeadWeight = 0.15;
            double grabLaryngoscopeWeight = 0.05;
            double insertLaryngoscopeWeight = 0.20;
            double viewVocalChordsWeight = 0.10;
            double grabEndotrachealTubeWeight = 0.05;
            double insertEndotrachealTubeWeight = 0.10;
            double removeLaryngoscopeWeight = 0.05;
            double useSyringeWeight = 0.10;
            double removeStyletWeight = 0.05;
            double baggingWeight = 0.05;
            double chestRiseWeight = 0.10;

            bool useWeightsInsteadOfAverage = true; // Flag to switch how grading occurs. Are certain scores weighted more than the others? True if so. False if they're all equal.

            if( useWeightsInsteadOfAverage )
                totalscore = ((tiltHeadScore * tiltHeadWeight) + (grabLaryngoscopeScore * grabLaryngoscopeWeight) + (insertLaryngoscopeScore * insertLaryngoscopeWeight) + (viewVocalChordsScore * viewVocalChordsWeight) + (grabEndotrachealTubeScore * grabEndotrachealTubeWeight) + (insertEndotrachealTubeScore * insertEndotrachealTubeWeight ) + (removeLaryngoscopeScore * removeLaryngoscopeWeight) + (useSyringeScore * useSyringeWeight) + (removeStyletScore * removeStyletWeight) + (baggingScore * baggingWeight) + (chestRiseScore * chestRiseWeight));
            else
                totalscore = (tiltHeadScore + grabLaryngoscopeScore + insertLaryngoscopeScore + viewVocalChordsScore + grabEndotrachealTubeScore + insertEndotrachealTubeScore + removeLaryngoscopeScore + useSyringeScore + removeStyletScore + baggingScore + chestRiseScore)/11;

            return totalscore;
        }

        public override void AbortLesson()
        {
            // Stop calls to update the lesson.
            KinectManager.AllDataReady -= KinectManager_AllDataReady;

            // Stop the Overlays that use the Kinect sensor.
            this.IntubationPage.skeletonOverlay.ShowBoneOverlays = false;
            this.IntubationPage.skeletonOverlay.ShowJointOverlays = false;
            this.IntubationPage.skeletonOverlay.ShowJointTextOverlays = false;

            // Pause and cleanout the lesson.
            this.PauseLesson();
            this.WipeClean();

            // Go back to the Main Page.  This will trigger IntubationPage Unloaded
            if (this.IntubationPage != null)
                this.IntubationPage.GoHome();
        }

        public override void ResetLesson()
        {
            this.WipeClean();
            this.StartLesson();
        }

        public override void PauseLesson()
        {
            //throw new NotImplementedException();
            this.Paused = true;
            this.Stopwatch.Stop();

            if (this.Stages != null && this.Stages.Count > this.CurrentStageIndex)
            {
                this.Stages[this.CurrentStageIndex].PauseStage();
            }
        }

        public override void OnTutorialVideoRequest()
        {
            this.PauseLesson();

            // Show the stage help video
            this.IntubationPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Intubation/Videos/Full Intubation.wmv", UriKind.Relative));
            this.IntubationPage.VideoPlayer.startVideo(this, new RoutedEventArgs());

            this.IntubationPage.VideoPlayer.MediaElement.MediaEnded += this.TutorialVideo_MediaEnded;
            this.IntubationPage.VideoPlayer.MediaElement.MediaFailed += this.TutorialVideo_MediaFailed;
        }

        private void TutorialVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeLesson();

            this.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.TutorialVideo_MediaEnded;
            this.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.TutorialVideo_MediaFailed;
        }

        private void TutorialVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeLesson();

            this.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.TutorialVideo_MediaEnded;
            this.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.TutorialVideo_MediaFailed;
        }

        public override void ResumeLesson()
        {
            //throw new NotImplementedException();
            this.Paused = false;
            this.Stopwatch.Start();

            if (this.Stages != null && this.Stages.Count > this.CurrentStageIndex)
            {
                this.Stages[this.CurrentStageIndex].ResumeStage();
            }
        }

        public override void WipeClean()
        {
            tiltHeadScore = 0;
            grabLaryngoscopeScore = 0;
            insertLaryngoscopeScore = 0;
            viewVocalChordsScore = 0;
            grabEndotrachealTubeScore = 0;
            insertEndotrachealTubeScore = 0;
            removeLaryngoscopeScore = 0;
            useSyringeScore = 0;
            removeStyletScore = 0;
            baggingScore = 0;
            chestRiseScore = 0;

            tiltHeadFeedbackIDs = new List<int>();
            grabLaryngoscopeFeedbackIDs = new List<int>();
            insertLaryngoscopeFeedbackIDs = new List<int>();
            viewVocalChordsFeedbackIDs = new List<int>();
            grabEndotrachealTubeFeedbackIDs = new List<int>();
            insertEndotrachealTubeFeedbackIDs = new List<int>();
            removeLaryngoscopeFeedbackIDs = new List<int>();
            useSyringeFeedbackIDs = new List<int>();
            removeStyletFeedbackIDs = new List<int>();
            baggingFeedbackIDs = new List<int>();
            chestRiseFeedbackIDs = new List<int>();

            tiltHeadImage = null;
            grabLaryngoscopeImage = null;
            insertLaryngoscopeImage = null;
            viewVocalChordsImage = null;
            grabEndotrachealTubeImage = null;
            insertEndotrachealTubeImage = null;
            removeLaryngoscopeImage = null;
            useSyringeImage = null;
            removeStyletImage = null;
            baggingImage = null;
            chestRiseImage = null;

            if (this.DetectableActions != null)
            {
                this.DetectableActions.ResetActions();  // reset actions to undetected & clear their detection logs.
            }

            this.StuckSkeletonProfileID = 0;

            AIMS.Assets.Lessons2.ColorTracker.Images.Clear();   // makes a call to clear out the old color tracker images for a fresh view.

            if (this.Stages != null)
            {
                foreach (Stage stage in this.Stages)
                {
                    stage.WipeClean();
                }
            }

            this.O2Level = 1;
            this.O2Bonus = 0;
            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.Paused = false;
            this.Running = false;
            this.CurrentStageIndex = 0;
            this.Complete = false;

            if (this.Result != null)
            {
                this.Result.WipeClean();

                this.Result = null;
                this.Result = new IntubationResult();
            }

            this.LaryngoscopeLocation = new Point3D();
            this.EndotrachealTubeLocation = new Point3D();
            this.SyringeLocation = new Point3D();
            this.StyletLocation = new Point3D();

            this.Stopwatch.Reset();

            KinectManager.AllDataReady -= KinectManager_AllDataReady;
            KinectManager.SensorNotReady -= KinectManager_SensorNotReady;

            if (this.IntubationPage != null)
            {
                this.IntubationPage.zoneOverlay.ClearZones();
                this.IntubationPage.InstructionTextBlock.Text = "";
                this.IntubationPage.StageInfoTextBlock.Text = "";
                this.IntubationPage.PatientHealthBar.Value = 1;
            }
        }
    }
}
