﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Kinect;
using System.Windows;
using System.Windows.Media.Media3D;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using System.Diagnostics;

using AIMS.Assets.Lessons2;
using AIMS.Tasks;

namespace AIMS.Tasks.Intubation
{
    public class IntubationCalibrationStage : Stage
    {
        private Intubation lesson;
        public Intubation Lesson { get { return lesson; } set { lesson = value; } }

        private bool hasFoundChinLocation = false;
        private Point3D manikinFaceLocation = new Point3D();
        private int yellowCounter = 0;
        private bool hasLockedOnToTarget = false;

        private ulong? watcher_CurrentStuckSkeletonProfileID;
        private Point3D watcher_CurrentManikinChinLocation;
        private int watcher_yellowCounter = 0;

        public bool HasFoundChinLocation { get { return hasFoundChinLocation; } set { hasFoundChinLocation = value; } }
        public Point3D ManikinFaceLocation { get { return manikinFaceLocation; } set { manikinFaceLocation = value; } }
        public int YellowCounter { get { return yellowCounter; } set { yellowCounter = value; } }
        public bool HasLockedOnToTarget { get { return hasLockedOnToTarget; } set { hasLockedOnToTarget = value; } }

        private Watcher stuckSkeletonWatcher;
        private Watcher yellowBelowLeftHandWatcher;
        private Watcher yellowBelowRightHandWatcher;

        private ColorRange _manikinChinColor;

        public IntubationCalibrationStage( Intubation lesson )
        {
            Lesson = lesson;

            StageNumber = 0;
            Name = "Calibration";
            Instruction = "Place one hand on the manikin's face, and the other in the air.";
            TargetTime = TimeSpan.MaxValue; // No target time for calibration...
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Tasks/Intubation/Graphics/Intubation000.png";
            Active = false;
            Complete = false;
            ScoreWeight = 0;    // Calibration stage is not weighted
            Zones = new List<Zone>();
            Stopwatch = new System.Diagnostics.Stopwatch();
            TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.TimeOutTime = TimeSpan.FromSeconds(30.0);
            this.CanTimeOut = false;
        }

        public override void SetupStage()
        {
            this.Lesson.IntubationPage.StageInfoTextBlock.Text = this.Name;
            this.Lesson.IntubationPage.InstructionTextBlock.Text = this.Instruction;

            this.Lesson.IntubationPage.flow.Index = this.Lesson.CurrentStageIndex;  // change cover flow image.

            try
            {
                // Set colors to look for.
                this._manikinChinColor = ColorTracker.GetObjectColorRange("manikin_chin").ColorRange;
            }
            catch (Exception exc) { Debug.Print("IntubationCalibrationStage SetupStage() ERROR: {0}", exc.Message); }

            // Set the image for the current stage.
            this.Lesson.IntubationPage.CurrentStageImage.Source = new BitmapImage(new Uri("/Tasks/Intubation/Graphics/Intubation000.png", UriKind.Relative));

            this.Zones.Clear(); // Clear the stage zone list.

            this.Lesson.IntubationPage.zoneOverlay.ClearZones();

            // Add the zones into the zoneOverlay
            foreach (Zone zone in this.Zones)
            {
                this.Lesson.IntubationPage.zoneOverlay.AddZone(zone);
            }

            this.CurrentStageWatchers = new List<BaseWatcher>();
            this.PeripheralStageWatchers = new List<BaseWatcher>();
            this.OutlyingStageWatchers = new List<BaseWatcher>();

            CreateStageWatchers();

            this.StartTime = DateTime.Now;

            this.IsSetup = true;

            this.Stopwatch.Start();

            this.TimeoutStopwatch.Start();
        }

        private void CreateStageWatchers()
        {
            // Clear the list of watchers.
            this.CurrentStageWatchers.Clear();
            this.PeripheralStageWatchers.Clear();
            this.OutlyingStageWatchers.Clear();

            // Add in new watchers.
            stuckSkeletonWatcher = new Watcher();

            #region WatcherLogic

            // Checks for the primary skeleton to become sticky.
            stuckSkeletonWatcher.CheckForActionDelegate = () =>
            {
                if (this.Complete)
                    return;

                if (!hasLockedOnToTarget)   // Lock onto a skeleton target.
                {
                    if (KinectManager.Bodies != null && KinectManager.Bodies.Length > 0) // Ensure that there are skeletons to select from.
                    {
                        if (!hasLockedOnToTarget)   // if we havn't yet locked onto any specific skeleton.
                        {
                            if (KinectManager.SingleStickySkeletonProfile != null)
                            {
                                //hasLockedOnToTarget = true;
                                stuckSkeletonWatcher.IsTriggered = true;
                                this.watcher_CurrentStuckSkeletonProfileID = KinectManager.SingleStickySkeletonProfile.CurrentTrackingID;
                            }
                            else
                            {
                                stuckSkeletonWatcher.IsTriggered = false;
                                this.watcher_CurrentStuckSkeletonProfileID = null;
                            }
                        }
                    }

                    return; // return, the next ClosestSkeletonProfile returned by the KinectManager will use the stickySkeletonID, if it was found. If not, we'll still be in this process until a skeleton is found.
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(stuckSkeletonWatcher);

            yellowBelowLeftHandWatcher = new Watcher();

            #region WatcherLogic

            yellowBelowLeftHandWatcher.CheckForActionDelegate = () => {
                if (this.Complete)
                    return;

                if (hasLockedOnToTarget)
                {
                    SkeletonProfile skele = null;

                    if (watcher_CurrentStuckSkeletonProfileID.HasValue)
                    {
                        skele = KinectManager.GetSkeletonProfile(watcher_CurrentStuckSkeletonProfileID, true);
                    }

                    if (skele != null)
                    {
                        // CONVERT THE SKELETAL POSITION INTO A 2D POINT FOR THE COLOR FRAME.
                        #region ConvertSkeletalPointToColorFrame
                        ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(skele.JointProfiles.First(o => o.JointType == JointType.HandLeft).CurrentPosition);

                        Point point = new Point();
                        point.X = (double)colorSpacePoint.X;
                        point.Y = (double)colorSpacePoint.Y;

                        #endregion

                        // Check around the skeleton's left hand for the chin.

                        double ratioX = 3.0;
                        double ratioY = 3.0;

                        int offsetX = (int)(point.X - (32 * ratioX));
                        int offsetY = (int)(point.Y - (10 * ratioY));
                        int widthX = (int)(96 * ratioX);
                        int widthY = (int)(64 * ratioY);

                        Point y = AIMS.Assets.Lessons2.ColorTracker.find(this._manikinChinColor, new Int32Rect(offsetX, offsetY, widthX, widthY), KinectManager.ColorData);

                        if (!Double.IsNaN(y.X) && !Double.IsNaN(y.Y) && y.X != 0 && y.Y != 0)
                        {   
                            int depth = KinectManager.GetDepthValueForColorPoint(y);

                            System.Diagnostics.Debug.WriteLine("IntubationCalibrationStage -> Yellow point: " + (int)y.X + "," + (int)y.Y + " at " + depth + "mm" + " with skel.Z = " + skele.CurrentPosition.Z);

                            if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth)
                            {
                                //RESTRICTION: Depth of the found color must be IN FRONT of the depth of the user. (Logical).

                                // Compare the difference in depths to determine which is closest.
                                double deltaDepth = 0;
                                bool foundPointIsCloserThanUser = false;

                                deltaDepth = (skele.CurrentPosition.Z * 1000) - depth;

                                // If the difference is positive (far - near > 0 ... eg: 2000 - 1000 > 0), then the color is closer than the user.
                                if (deltaDepth > 0)
                                {
                                    foundPointIsCloserThanUser = true;
                                }

                                // Use the point if it's good.
                                if (foundPointIsCloserThanUser)
                                {
                                    this.watcher_CurrentManikinChinLocation = KinectManager.Get3DPointForColorPoint(y, depth);

                                    yellowBelowLeftHandWatcher.IsTriggered = true;
                                }
                                else
                                {
                                    yellowBelowLeftHandWatcher.IsTriggered = false;
                                }
                            }
                            else
                            {
                                yellowBelowLeftHandWatcher.IsTriggered = false;
                            }
                        }
                        else
                        {
                            yellowBelowLeftHandWatcher.IsTriggered = false;
                        }
                    }
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(yellowBelowLeftHandWatcher);   // Add the watcher to the list of watchers

            yellowBelowRightHandWatcher = new Watcher();

            #region WatcherLogic
            yellowBelowRightHandWatcher.CheckForActionDelegate = () =>
            {
                if (this.Complete)
                    return;

                if (hasLockedOnToTarget)
                {
                    SkeletonProfile skele = null;

                    if (watcher_CurrentStuckSkeletonProfileID.HasValue)
                    {
                        skele = KinectManager.GetSkeletonProfile(watcher_CurrentStuckSkeletonProfileID, true);
                    }

                    if (skele != null)
                    {
                        // CONVERT THE SKELETAL POSITION INTO A 2D POINT FOR THE COLOR FRAME.
                        #region ConvertSkeletalPointToColorFrame
                        ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(skele.JointProfiles.First(o => o.JointType == JointType.HandRight).CurrentPosition);

                        Point point = new Point();
                        point.X = (double)colorSpacePoint.X;
                        point.Y = (double)colorSpacePoint.Y;

                        #endregion

                        // Check around the skeleton's right hand for the tool.

                        double ratioX = 3.0;
                        double ratioY = 3.0;

                        int offsetX = (int)(point.X - (64 * ratioX));
                        int offsetY = (int)(point.Y - (0 * ratioY));
                        int widthX = (int)(96 * ratioX);
                        int widthY = (int)(48 * ratioY);

                        Point y = AIMS.Assets.Lessons2.ColorTracker.find(this._manikinChinColor, new Int32Rect(offsetX, offsetY, widthX, widthY), KinectManager.ColorData);

                        if (!Double.IsNaN(y.X) && !Double.IsNaN(y.Y))
                        {
                            int depth = KinectManager.GetDepthValueForColorPoint(y);

                            //System.Diagnostics.Debug.WriteLine("IntubationCalibrationStage -> depth range for " + depth + " is: " + KinectManager.ReliableMinDepth + " to " + KinectManager.ReliableMaxDepth);

                            if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                            {
                                //RESTRICTION: Depth of the found color must be IN FRONT of the depth of the user. (Logical).

                                // Compare the difference in depths to determine which is closest.
                                double deltaDepth = 0;
                                bool foundPointIsCloserThanUser = false;

                                deltaDepth = (skele.CurrentPosition.Z * 1000) - depth;

                                // If the difference is positive (far - near > 0 ... eg: 2000 - 1000 > 0), then the color is closer than the user.
                                if (deltaDepth > 0)
                                {
                                    foundPointIsCloserThanUser = true;
                                }

                                // Use the point of it's good.
                                if (foundPointIsCloserThanUser)
                                {
                                    this.watcher_CurrentManikinChinLocation = KinectManager.Get3DPointForColorPoint(y, depth);                                    

                                    yellowBelowRightHandWatcher.IsTriggered = true;
                                }
                                else
                                {
                                    yellowBelowRightHandWatcher.IsTriggered = false;
                                }
                            }
                            else
                            {
                                yellowBelowRightHandWatcher.IsTriggered = false;
                            }
                        }
                        else
                        {
                            yellowBelowRightHandWatcher.IsTriggered = false;
                        }
                    }
                }
            };
            #endregion

            this.CurrentStageWatchers.Add(yellowBelowRightHandWatcher);   // Add the watcher to the list of watchers
        }

        public override void UpdateStage()
        {
            if (!IsSetup)
            {
                SetupStage();
            }

            if (KinectManager.DepthWidth == 0)
                return;

            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
            {
                this.OnStageTimeout();
            }

            if (this.CurrentStageWatchers[0].IsTriggered)
            {
                // Stuck Skeleton is found.
                if (!this.HasLockedOnToTarget)
                {
                    this.HasLockedOnToTarget = true;
                    this.Lesson.StuckSkeletonProfileID = this.watcher_CurrentStuckSkeletonProfileID;

                    this.Lesson.DetectableActions.DetectAction("Became Sticky", "Become Sticky", "User", true);   // Detect the Action

                    System.Diagnostics.Debug.WriteLine(String.Format("Locked onto target. SkeletonProfileID: {0}.", this.Lesson.StuckSkeletonProfileID));
                }
            }
            else
            {
                if (this.HasLockedOnToTarget)   // Skeleton was stuck, but is no longer found.
                {
                    this.HasLockedOnToTarget = false;   // Lost the target.
                    this.Lesson.StuckSkeletonProfileID = null;
                    System.Diagnostics.Debug.WriteLine("Lost stuck target.");
                    this.Lesson.DetectableActions.UndetectAction("Became Sticky", "Become Sticky", "User", false);   // Detect the Action
                    this.Lesson.DetectableActions.DetectAction("No Sticky", "Become Sticky", "User", true);   // Detect the Action
                }
            }

            if (KinectManager.SingleStickySkeletonProfile == null)
                return;

            if (this.CurrentStageWatchers[1].IsTriggered || this.CurrentStageWatchers[2].IsTriggered)
            {
                if (!this.HasFoundChinLocation)
                {
                    this.HasFoundChinLocation = true;

                    System.Diagnostics.Debug.WriteLine("IntubationCalibrationStage -> Manikin Chin Location Found.");

                    this.Lesson.DetectableActions.UndetectAction("Not Found", "Locate Initially", "Manikin Chin", false);   // Detect the Action
                    this.Lesson.DetectableActions.DetectAction("Initially Located", "Locate Initially", "Manikin Chin", true);   // Detect the Action
                }
            }
            else
            {
                if (this.HasFoundChinLocation)
                {
                    this.HasFoundChinLocation = false;  // Lost the chin location below the hands.
                    System.Diagnostics.Debug.WriteLine("IntubationCalibrationStage -> Manikin Chin Location Lost.");

                    this.Lesson.DetectableActions.UndetectAction("Initially Located", "Locate Initially", "Manikin Chin", false);   // Detect the Action
                    this.Lesson.DetectableActions.DetectAction("Not Found", "Locate Initially", "Manikin Chin", true);   // Detect the Action
                }
            }

            if (this.HasLockedOnToTarget && this.HasFoundChinLocation)
            {
                this.Lesson.ManikinFaceLocation = watcher_CurrentManikinChinLocation;
                this.Lesson.StuckSkeletonProfileID = watcher_CurrentStuckSkeletonProfileID;
                System.Diagnostics.Debug.WriteLine(String.Format("IntubationCalibrationStage -> ManikinChinLocation set to ({0},{1},{2}mm).", this.Lesson.ManikinFaceLocation.X, this.Lesson.ManikinFaceLocation.Y, this.Lesson.ManikinFaceLocation.Z));
                this.Complete = true;
                EndStage();
            }
        }

        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }

        public override void EndStage( bool playSound = true )
        {
            this.Stopwatch.Stop();

            this.TimeoutStopwatch.Stop();

            // Get the current time to signify the time the stage ended.
            EndTime = DateTime.Now;

            //StageScore stagescore = new StageScore();
            // Create a screenshot for the current stage.

            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                Intubation lesson = (Intubation)args.Argument;

                lesson.CalibrationImage = KinectImage.TakeKinectSnapshot(true, true);
            };

            worker.RunWorkerAsync(this.Lesson);

            this.Lesson.CurrentStageIndex++;

            if( playSound ) Lesson.PlayNextStageSound();

            this.Lesson.Running = true;
        }

        public override void PauseStage()
        {
            this.Lesson.Paused = true;
            this.Lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.Lesson.Paused = false;
            this.Lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void ResetStage()
        {

        }

        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            //this.PauseStage();
            this.TimedOut = true;
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            this.RequestStageVideo();   // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
        }

        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.Lesson.IntubationPage.VideoPlayer.setVideoSource(new Uri("Tasks/Intubation/Videos/Stage 000.wmv", UriKind.Relative));
            this.Lesson.IntubationPage.VideoPlayer.startVideo(this, new RoutedEventArgs());

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        public override void WipeClean()
        {
            HasFoundChinLocation = false;
            YellowCounter = 0;
            ManikinFaceLocation = new Point3D();
            HasLockedOnToTarget = false;

            watcher_CurrentStuckSkeletonProfileID = null;
            watcher_CurrentManikinChinLocation = new Point3D();
            watcher_yellowCounter = 0;

            if (this.CurrentStageWatchers != null)
            {
                this.CurrentStageWatchers.Clear();
            }
            else
            {
                this.CurrentStageWatchers = new List<BaseWatcher>();
            }
            if (this.PeripheralStageWatchers != null)
            {
                this.PeripheralStageWatchers.Clear();
            }
            else
            {
                this.PeripheralStageWatchers = new List<BaseWatcher>();
            }
            if (this.OutlyingStageWatchers != null)
            {
                this.OutlyingStageWatchers.Clear();
            }
            else
            {
                this.OutlyingStageWatchers = new List<BaseWatcher>();
            }

            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;

            this.IsSetup = false;

            if (this.StageScore != null)
            {
                this.StageScore.WipeClean();
                this.StageScore = null;
                this.StageScore = new StageScore();
            }
            else
            {
                this.StageScore = new StageScore();
            }

            this.Active = false;
            this.Complete = false;

            if (this.Zones != null)
            {
                this.Zones.Clear();
            }
            else
            {
                this.Zones = new List<Zone>();
            }

            if (this.Stopwatch != null)
            {
                this.Stopwatch.Reset();
            }
            else
            {
                this.Stopwatch = new System.Diagnostics.Stopwatch();
            }

            if (this.TimeoutStopwatch != null)
            {
                this.TimeoutStopwatch.Reset();
            }
            else
            {
                this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            }

            this.TimedOut = false;
        }
    }
}
