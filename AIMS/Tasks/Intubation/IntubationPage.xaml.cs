﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using AIMS.Assets.Lessons2;
using AIMS.Speech;
using System.IO;
using AIMS.Tasks;
using AIMS.Tasks.Intubation;

namespace AIMS
{
    /// <summary>
    /// Interaction logic for IntubationPage.xaml
    /// </summary>
    public partial class IntubationPage : Page
    {
        public IntubationPage()
        {
            InitializeComponent();

            Loaded += IntubationPage_Loaded;
            Unloaded += IntubationPage_Unloaded;
        }

        private DispatcherTimer showResultDelayTimer = new DispatcherTimer();
        private readonly TimeSpan showResultDelay = TimeSpan.FromSeconds(3.0);

        private System.Media.SoundPlayer insertLaryngoscope;
        private System.Media.SoundPlayer tiltHead;
        private System.Media.SoundPlayer calibrate;
        private System.Media.SoundPlayer pickUpLaryngoscopeAndInsert;
        private System.Media.SoundPlayer sweepTheLaryngoscope;
        private System.Media.SoundPlayer pickUpTube;
        private System.Media.SoundPlayer insertTube;
        private System.Media.SoundPlayer removeLaryngoscope;
        private System.Media.SoundPlayer useSyringe;
        private System.Media.SoundPlayer removeStylet;
        private System.Media.SoundPlayer bendDown;

        private AIMS.Tasks.Intubation.Intubation lesson = null;
        public AIMS.Tasks.Intubation.Intubation Lesson { get { return lesson; } set { lesson = value; } }


        private void IntubationPage_Loaded(object sender, RoutedEventArgs e)
        {
            LessonStatsheet statsheet = null;

            if (lesson != null)
            {
                lesson.WipeClean();
                lesson = null;
            }

            if (((App)App.Current).CurrentStudent != null)
            {
                statsheet = LessonStatsheet.GetOrCreateStatsheetForStudent(((App)App.Current).CurrentStudent.ID, LessonType.Intubation);
            }

            if (statsheet != null)
            {
                this.CurrentMasteryLevelTB.Text = statsheet.MasteryLevel.ToString();

                ((App)App.Current).CurrentMasteryLevel = statsheet.MasteryLevel;

                lesson = new AIMS.Tasks.Intubation.Intubation(this, ((App)App.Current).CurrentTaskMode, statsheet.MasteryLevel);
            }
            else
            {
                // Instantiate the lesson.
                lesson = new AIMS.Tasks.Intubation.Intubation(this, ((App)App.Current).CurrentTaskMode, ((App)App.Current).CurrentMasteryLevel);
            }

            try
            {
                KinectManager.KinectSpeechCommander.SetModuleGrammars(new Uri("pack://application:,,,/AIMS;component/Tasks/Intubation/IntubationSpeechGrammar.xml"),
                                                                        "activeListen", "defaultListen");
                KinectManager.KinectSpeechCommander.SpeechRecognized += new EventHandler<SpeechCommanderEventArgs>(KinectSpeechCommander_SpeechRecognized);

                if (KinectManager.KinectSpeechCommander != null)
                {
                    #region LoadSpeechPlaybackFiles
                    calibrate = new System.Media.SoundPlayer(Properties.Resources.AIMS_Intubation_Calibrate);
                    calibrate.LoadAsync();

                    bendDown = new System.Media.SoundPlayer(Properties.Resources.AIMS_Intubation_BendDown);
                    bendDown.LoadAsync();

                    tiltHead = new System.Media.SoundPlayer(Properties.Resources.AIMS_Intubation_TiltTheHead);
                    tiltHead.LoadAsync();

                    pickUpLaryngoscopeAndInsert = new System.Media.SoundPlayer(Properties.Resources.AIMS_Intubation_InsertTheLaryngoscopeAndInsert);
                    pickUpLaryngoscopeAndInsert.LoadAsync();

                    sweepTheLaryngoscope = new System.Media.SoundPlayer(Properties.Resources.AIMS_Intubation_SweepTheLaryngoscope);
                    sweepTheLaryngoscope.LoadAsync();

                    pickUpTube = new System.Media.SoundPlayer(Properties.Resources.AIMS_Intubation_PickUpEndotrachealTube);
                    pickUpTube.LoadAsync();

                    insertTube = new System.Media.SoundPlayer(Properties.Resources.AIMS_Intubation_InsertTheEndotrachealTube);
                    insertTube.LoadAsync();

                    removeLaryngoscope = new System.Media.SoundPlayer(Properties.Resources.AIMS_Intubation_RemoveTheLaryngoscope);
                    removeLaryngoscope.LoadAsync();

                    useSyringe = new System.Media.SoundPlayer(Properties.Resources.AIMS_Intubation_UseSyringe);
                    useSyringe.LoadAsync();

                    removeStylet = new System.Media.SoundPlayer(Properties.Resources.AIMS_Intubation_RemoveStylet);
                    removeStylet.LoadAsync();
                    #endregion
                }
            }
            catch
            {

            }

            this.CurrentTaskModeTB.Text = ((App)App.Current).CurrentTaskMode.ToString();
            this.CurrentTaskTB.Text = ((App)App.Current).CurrentTask.ToString();

            //this.SkeletonChooser.SkeletonChooserMode = SkeletonChooserMode.MostActive1Player;

            if (lesson != null)
            {
                lesson.StartLesson();
            }
        }

        private void IntubationPage_Unloaded(object sender, RoutedEventArgs e)
        {
            if (lesson != null)
            {
                lesson.WipeClean();
                lesson = null;
            }

            try
            {
                KinectManager.KinectSpeechCommander.SpeechRecognized -= this.KinectSpeechCommander_SpeechRecognized;
            }
            catch
            {
            }
        }

        #region Kinect Speech processing
        void KinectSpeechCommander_SpeechRecognized(object sender, SpeechCommanderEventArgs e)
        {
            switch (e.Command)
            {
                case "HELP":
                    OnSaid_Help();
                    break;
                case "VIDEO":
                    OnSaid_Video();
                    break;
                case "TUTORIAL":
                    OnSaid_Tutorial();
                    break;
                case "END_VIDEO":
                    OnSaid_EndVideo();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// "Help" was said, display the help menu and/or give auditory response. 
        /// </summary>
        private void OnSaid_Help()
        {
            if( lesson != null )
            {
                if (lesson.Stages != null && lesson.CurrentStageIndex < lesson.Stages.Count)
                {
                    switch (lesson.CurrentStageIndex)
                    {
                        case 0:

                            break;
                        case 1:
                            tiltHead.Play();
                            break;
                        case 2:
                            pickUpLaryngoscopeAndInsert.Play();
                            break;
                        case 3:
                            sweepTheLaryngoscope.Play();
                            break;
                        case 4:
                            bendDown.Play();
                            break;
                        case 5:
                            pickUpTube.Play();
                            break;
                        case 6:
                            insertTube.Play();
                            break;
                        case 7:
                            removeLaryngoscope.Play();
                            break;
                        case 8:
                            removeStylet.Play();
                            break;
                        case 9:
                            useSyringe.Play();
                            break;
                    }
                }
            }
        }

        private void OnSaid_Video()
        {
            if (lesson != null)
            {
                if (lesson.Stages != null && lesson.CurrentStageIndex < lesson.Stages.Count)
                {
                    lesson.Stages[lesson.CurrentStageIndex].RequestStageVideo();
                }
            }
        }

        private void OnSaid_EndVideo()
        {
            if (lesson != null)
            {
                this.VideoPlayer.CloseVideo();
            }
        }

        private void OnSaid_Tutorial()
        {
            if (lesson != null)
            {
                lesson.RequestTutorialVideo();
            }
        }
        
        #endregion Kinect Speech processing

        public void ShowResults(IntubationResult result)
        {
            bool delayResultShowing = true;

            if (delayResultShowing)
            {
                if (this.showResultDelayTimer.IsEnabled)    // timer already running
                    return;

                this.showResultDelayTimer.Interval = showResultDelay;   // time between 1st tick

                this.showResultDelayTimer.Tick += delegate  // event to fire once it ticks.
                {
                    showResultDelayTimer.Stop();    // prevent watch from firing again.

                    this.ResultsComponent.ShowResults(result);  // show the result.
                };

                this.showResultDelayTimer.Start();  // start the timer.
            }
            else
            {
                this.ResultsComponent.ShowResults(result);  // just show the result without a delay.
            }
        }

        public void GoHome()
        {
            System.Uri uri = new System.Uri("../MainPage3.xaml", System.UriKind.Relative);
            this.NavigationService.Navigate(uri);
        }

        private void ResultsComponent_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void PatientHealthBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }

        public void UpdateBoneSizeText()
        {
           // BoneSizeTextBlock.Text = String.Format("RHand-RWrist: {0}\rLHand-LWrist: {1}\rRWrist-RElbow: {2}\rLWrist-LElbow: {3}\rRElbow-RShoulder: {4}\rLElbow-LShoulder: {5}\rRShoulder-CShoulder: {6}\rLShoulder-CShoulder: {7}\rCShoulder-Head: {8}\rCShoulder-Spine: {9}\rSpine-CHip: {10}\rCHip-RHip: {11}\rCHip-LHip: {12}\rRHip-RKnee: {13}\rLHip-LKnee: {14}\rRKnee-RAnkle: {15}\rLKnee-LAnkle: {16}\rRAnkle-RFoot: {17}\rLAnkle-LFoot: {18}", lesson.BoneProfile.RHandRWristBoneLength, lesson.BoneProfile.LHandLWristBoneLength, lesson.BoneProfile.RWristRElbowBoneLength, lesson.BoneProfile.LWristLElbowBoneLength, lesson.BoneProfile.RElbowRShoulderBoneLength, lesson.BoneProfile.LElbowLShoulderBoneLength, lesson.BoneProfile.RShoulderCShoulderBoneLength, lesson.BoneProfile.LShoulderCShoulderBoneLength, lesson.BoneProfile.CShoulderHeadBoneLength, lesson.BoneProfile.CShoulderSpineBoneLength, lesson.BoneProfile.SpineCHipBoneLength, lesson.BoneProfile.CHipRHipBoneLength, lesson.BoneProfile.CHipLHipBoneLength, lesson.BoneProfile.RHipRKneeBoneLength, lesson.BoneProfile.LHipLKneeBoneLength, lesson.BoneProfile.RKneeRAnkleBoneLength, lesson.BoneProfile.LKneeLAnkleBoneLength, lesson.BoneProfile.RAnkleRFootBoneLength, lesson.BoneProfile.LAnkleLFootBoneLength);
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            Lesson.ResetLesson();// Restart();
        }

        private void GetJpgButton_Click(object sender, RoutedEventArgs e)
        {
            byte[] imageBytes = JpgScreenshot.GetJpgImage( this.OverlayGrid, 0.75, 100 );

            // Convert Byte-Array in Base64String --> Image as String
            string bildBase64 = Convert.ToBase64String(imageBytes);

            // save uncompressed bitmap to disk
            string fileName = String.Format( "{0}{1}-{2}-{3}_{4}-{5}-{6}.jpg", "C:\\TestJPG_", DateTime.Now.Month, DateTime.Now.Day , DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second );

            try
            {
                System.IO.FileStream fs = System.IO.File.Open(fileName, System.IO.FileMode.OpenOrCreate);
                fs.Write(imageBytes, 0, imageBytes.Length);
                fs.Close();
            }
            catch
            {
            }
        }

        private void SidePanelExpansionButton_Click(object sender, RoutedEventArgs e)
        {
            if (SidePanelContentGrid.Visibility == System.Windows.Visibility.Collapsed)
            {
                SidePanelContentGrid.Visibility = System.Windows.Visibility.Visible;
                (sender as Button).Content = "<";
                SidePanelContentGrid.Focus();
            }
            else
            {
                SidePanelContentGrid.Visibility = System.Windows.Visibility.Collapsed;
                (sender as Button).Content = ">";
                SidePanelContentGrid.Focus();
            }
        }

        private void HeaderButtonsListBox_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                HeaderButtonsListBox.SelectedIndex = 0;
            }
            catch
            {
            }
        }

        private void ProceduralTaskButtonsListBox_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                
            }
            catch
            {
            }
        }

        private void ProceduralTaskButtonsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            try
            {
                switch (ProceduralTaskButtonsListBox.SelectedIndex)
                {
                    case 0:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    case 1:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    case 2:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    case 3:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    case 4:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    default:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                }
            }
            catch
            {
            }
        }

        private void HeaderButtonsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (HeaderButtonsListBox.SelectedIndex == -1)
            {
                return;
            }

            try
            {
                switch (HeaderButtonsListBox.SelectedIndex)
                {
                    case 0:
                        
                        break;
                    case 1:
                      //  HeaderButtonsListBox.SelectedIndex = -1;    // Deselect
                        ProceduralTaskButtonsListBox.SelectedIndex = -1;    // Deselect
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Visible;
                        break;
                    case 2:
                        
                        break;
                    case 3:
                        
                        break;
                    case 4:
                        
                        break;
                    default:
                        
                        break;
                }
            }
            catch
            {
            }
        }

        private void ProceduralTaskButtonsListBox_MouseLeave(object sender, MouseEventArgs e)
        {
            ProceduralTaskButtonsListBox.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void TopPanelExpansionButton_Click(object sender, RoutedEventArgs e)
        {
            if (TopPanelContentGrid.Visibility == System.Windows.Visibility.Collapsed)
            {
                TopPanelContentGrid.Visibility = System.Windows.Visibility.Visible;
                (sender as Button).Content = "^";
                TopPanelContentGrid.Focus();
            }
            else
            {
                TopPanelContentGrid.Visibility = System.Windows.Visibility.Collapsed;
                (sender as Button).Content = "v";
                TopPanelContentGrid.Focus();
            }
        }

        private void ResultsComponent_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue.Equals(true))
                this.BlurEffect.Radius = 25;
            else
                this.BlurEffect.Radius = 0;
        }
    }
    /*
    public static class JpgScreenshot
    {
        public static byte[] GetJpgImage(this UIElement source, double scale, int quality)
        {
            double actualHeight = source.RenderSize.Height;
            double actualWidth = source.RenderSize.Width;

            double renderHeight = actualHeight * scale;
            double renderWidth = actualWidth * scale;

            RenderTargetBitmap renderTarget = new RenderTargetBitmap((int)renderWidth, (int)renderHeight, 96, 96, PixelFormats.Pbgra32);
            VisualBrush sourceBrush = new VisualBrush(source);

            DrawingVisual drawingVisual = new DrawingVisual();
            DrawingContext drawingContext = drawingVisual.RenderOpen();

            using (drawingContext)
            {
                drawingContext.PushTransform(new ScaleTransform(scale, scale));
                drawingContext.DrawRectangle(sourceBrush, null, new Rect(new Point(0, 0), new Point(actualWidth, actualHeight)));
            }
            renderTarget.Render(drawingVisual);

            JpegBitmapEncoder jpgEncoder = new JpegBitmapEncoder();
            jpgEncoder.QualityLevel = quality;
            jpgEncoder.Frames.Add(BitmapFrame.Create(renderTarget));

            Byte[] _imageArray;

            using (MemoryStream outputStream = new MemoryStream())
            {
                jpgEncoder.Save(outputStream);
                _imageArray = outputStream.ToArray();
            }

            return _imageArray;
        }
    }*/
}
