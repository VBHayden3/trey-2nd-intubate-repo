﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using System.Windows;


namespace AIMS.Tasks.Intubation
{
    public class IntubationResult : TaskResult
    {
        private const string OVERALL_RESULTS_TABLE_NAME = "taskresults";
        private const string INTUBATION_RESULTS_TABLE_NAME = "taskresults_intubation";
        private const string IMAGES_TABLE_NAME = "kinect_images";

        #region Private Fields
        // uid is given by postgresql upon insertion using a self-incrementing "serial" int. It will be 0, unless otherwise specified - indicating it's uid in the postgresql database.
        private int? calibrationImageID = null;
        private KinectImage calibrationImage = null;

        private double tiltHeadScore = 0;
        private int? tiltHeadImageID = null;
        private KinectImage tiltHeadImage = null;
        private List<int> tiltHeadFeedbackIDs = new List<int>();

        private double grabLaryngoscopeScore = 0;
        private int? grabLaryngoscopeImageID = null;
        private KinectImage grabLaryngoscopeImage = null;
        private List<int> grabLaryngoscopeFeedbackIDs = new List<int>();

        private double insertLaryngoscopeScore = 0;
        private int? insertLaryngoscopeImageID = null;
        private KinectImage insertLaryngoscopeImage = null;
        private List<int> insertLaryngoscopeFeedbackIDs = new List<int>();

        private double viewVocalChordsScore = 0;
        private int? viewVocalChordsImageID = null;
        private KinectImage viewVocalChordsImage = null;
        private List<int> viewVocalChordsFeedbackIDs = new List<int>();

        private double grabEndotrachealTubeScore = 0;
        private int? grabEndotrachealTubeImageID = null;
        private KinectImage grabEndotrachealTubeImage = null;
        private List<int> grabEndotrachealTubeFeedbackIDs = new List<int>();

        private double insertEndotrachealTubeScore = 0;
        private int? insertEndotrachealTubeImageID = null;
        private KinectImage insertEndotrachealTubeImage = null;
        private List<int> insertEndotrachealTubeFeedbackIDs = new List<int>();

        private double removeLaryngoscopeScore = 0;
        private int? removeLaryngoscopeImageID = null;
        private KinectImage removeLaryngoscopeImage = null;
        private List<int> removeLaryngoscopeFeedbackIDs = new List<int>();

        private double useSyringeScore = 0;
        private int? useSyringeImageID = null;
        private KinectImage useSyringeImage = null;
        private List<int> useSyringeFeedbackIDs = new List<int>();

        private double removeStyletScore = 0;
        private int? removeStyletImageID = null;
        private KinectImage removeStyletImage = null;
        private List<int> removeStyletFeedbackIDs = new List<int>();

        private double baggingScore = 0;
        private int? baggingImageID = null;
        private KinectImage baggingImage = null;
        private List<int> baggingFeedbackIDs = new List<int>();

        private double chestRiseScore = 0;
        private int? chestRiseImageID = null;
        private KinectImage chestRiseImage = null;
        private List<int> chestRiseFeedbackIDs = new List<int>();

        // Holds feedback uid's pertaining to the feedback strings relevant to this run - which are being stored in the database's lookup table.
        private List<int> overallFeedbackIDs = new List<int>();
        #endregion

        #region Public Properties
        public int? CalibrationImageID { get { return calibrationImageID; } set { calibrationImageID = value; } }
        public KinectImage CalibrationImage { get { return calibrationImage; } set { calibrationImage = value; } }

        public double TiltHeadScore { get { return tiltHeadScore; } set { tiltHeadScore = value; } }
        public int? TiltHeadImageID { get { return tiltHeadImageID; } set { tiltHeadImageID = value; } }
        public KinectImage TiltHeadImage { get { return tiltHeadImage; } set { tiltHeadImage = value; } }
        public List<int> TiltHeadFeedbackIDs { get { return tiltHeadFeedbackIDs; } set { tiltHeadFeedbackIDs = value; } }

        public double GrabLaryngoscopeScore { get { return grabLaryngoscopeScore; } set { grabLaryngoscopeScore = value; } }
        public int? GrabLaryngoscopeImageID { get { return grabLaryngoscopeImageID; } set { grabLaryngoscopeImageID = value; } }
        public KinectImage GrabLaryngoscopeImage { get { return grabLaryngoscopeImage; } set { grabLaryngoscopeImage = value; } }
        public List<int> GrabLaryngoscopeFeedbackIDs { get { return grabLaryngoscopeFeedbackIDs; } set { grabLaryngoscopeFeedbackIDs = value; } }

        public double InsertLaryngoscopeScore { get { return insertLaryngoscopeScore; } set { insertLaryngoscopeScore = value; } }
        public int? InsertLaryngoscopeImageID { get { return insertLaryngoscopeImageID; } set { insertLaryngoscopeImageID = value; } }
        public KinectImage InsertLaryngoscopeImage { get { return insertLaryngoscopeImage; } set { insertLaryngoscopeImage = value; } }
        public List<int> InsertLaryngoscopeFeedbackIDs { get { return insertLaryngoscopeFeedbackIDs; } set { insertLaryngoscopeFeedbackIDs = value; } }

        public double ViewVocalChordsScore { get { return viewVocalChordsScore; } set { viewVocalChordsScore = value; } }
        public int? ViewVocalChordsImageID { get { return viewVocalChordsImageID; } set { viewVocalChordsImageID = value; } }
        public KinectImage ViewVocalChordsImage { get { return viewVocalChordsImage; } set { viewVocalChordsImage = value; } }
        public List<int> ViewVocalChordsFeedbackIDs { get { return viewVocalChordsFeedbackIDs; } set { viewVocalChordsFeedbackIDs = value; } }

        public double GrabEndotrachealTubeScore { get { return grabEndotrachealTubeScore; } set { grabEndotrachealTubeScore = value; } }
        public int? GrabEndotrachealTubeImageID { get { return grabEndotrachealTubeImageID; } set { grabEndotrachealTubeImageID = value; } }
        public KinectImage GrabEndotrachealTubeImage { get { return grabEndotrachealTubeImage; } set { grabEndotrachealTubeImage = value; } }
        public List<int> GrabEndotrachealTubeFeedbackIDs { get { return grabEndotrachealTubeFeedbackIDs; } set { grabEndotrachealTubeFeedbackIDs = value; } }

        public double InsertEndotrachealTubeScore { get { return insertEndotrachealTubeScore; } set { insertEndotrachealTubeScore = value; } }
        public int? InsertEndotrachealTubeImageID { get { return insertEndotrachealTubeImageID; } set { insertEndotrachealTubeImageID = value; } }
        public KinectImage InsertEndotrachealTubeImage { get { return insertEndotrachealTubeImage; } set { insertEndotrachealTubeImage = value; } }
        public List<int> InsertEndotrachealTubeFeedbackIDs { get { return insertEndotrachealTubeFeedbackIDs; } set { insertEndotrachealTubeFeedbackIDs = value; } }

        public double RemoveLaryngoscopeScore { get { return removeLaryngoscopeScore; } set { removeLaryngoscopeScore = value; } }
        public int? RemoveLaryngoscopeImageID { get { return removeLaryngoscopeImageID; } set { removeLaryngoscopeImageID = value; } }
        public KinectImage RemoveLaryngoscopeImage { get { return removeLaryngoscopeImage; } set { removeLaryngoscopeImage = value; } }
        public List<int> RemoveLaryngoscopeFeedbackIDs { get { return removeLaryngoscopeFeedbackIDs; } set { removeLaryngoscopeFeedbackIDs = value; } }

        public double UseSyringeScore { get { return useSyringeScore; } set { useSyringeScore = value; } }
        public int? UseSyringeImageID { get { return useSyringeImageID; } set { useSyringeImageID = value; } }
        public KinectImage UseSyringeImage { get { return useSyringeImage; } set { useSyringeImage = value; } }
        public List<int> UseSyringeFeedbackIDs { get { return useSyringeFeedbackIDs; } set { useSyringeFeedbackIDs = value; } }

        public double RemoveStyletScore { get { return removeStyletScore; } set { removeStyletScore = value; } }
        public int? RemoveStyletImageID { get { return removeStyletImageID; } set { removeStyletImageID = value; } }
        public KinectImage RemoveStyletImage { get { return removeStyletImage; } set { removeStyletImage = value; } }
        public List<int> RemoveStyletFeedbackIDs { get { return removeStyletFeedbackIDs; } set { removeStyletFeedbackIDs = value; } }

        public double BaggingScore { get { return baggingScore; } set { baggingScore = value; } }
        public int? BaggingImageID { get { return baggingImageID; } set { baggingImageID = value; } }
        public KinectImage BaggingImage { get { return baggingImage; } set { baggingImage = value; } }
        public List<int> BaggingFeedbackIDs { get { return baggingFeedbackIDs; } set { baggingFeedbackIDs = value; } }

        public double ChestRiseScore { get { return chestRiseScore; } set { chestRiseScore = value; } }
        public int? ChestRiseImageID { get { return chestRiseImageID; } set { chestRiseImageID = value; } }
        public KinectImage ChestRiseImage { get { return chestRiseImage; } set { chestRiseImage = value; } }
        public List<int> ChestRiseFeedbackIDs { get { return chestRiseFeedbackIDs; } set { chestRiseFeedbackIDs = value; } }

        public List<int> OverallFeedbackIDs { get { return overallFeedbackIDs; } set { overallFeedbackIDs = value; } }
        #endregion

        #region Constructors
        public IntubationResult() : base()
        {
            this.TaskType = Task.Intubation;
        }
        #endregion

        #region AIMS Usable Methods
        /// <summary>
        /// Resets all values of the result object back to their defaults.
        /// </summary>
        public void WipeClean()
        {
            uid = 0;

            studentID = 0;

            score = 0;
            startTime = DateTime.MinValue;
            endTime = DateTime.MaxValue;
            taskMode = TaskMode.Practice;
            masteryLevel = MasteryLevel.Novice;
            submissionTime = DateTime.MinValue;
            assignmentID = null;

            calibrationImageID = null;
            calibrationImage = null;

            tiltHeadScore = 0;
            tiltHeadImageID = null;
            tiltHeadImage = null;
            if (tiltHeadFeedbackIDs != null)
            {
                tiltHeadFeedbackIDs.Clear();
                tiltHeadFeedbackIDs = null;
            }
            tiltHeadFeedbackIDs = new List<int>();

            grabLaryngoscopeScore = 0;
            grabLaryngoscopeImageID = null;
            grabLaryngoscopeImage = null;
            if (grabLaryngoscopeFeedbackIDs != null)
            {
                grabLaryngoscopeFeedbackIDs.Clear();
                grabLaryngoscopeFeedbackIDs = null;
            }
            grabLaryngoscopeFeedbackIDs = new List<int>();

            insertLaryngoscopeScore = 0;
            insertLaryngoscopeImageID = null;
            insertLaryngoscopeImage = null;
            if (insertLaryngoscopeFeedbackIDs != null)
            {
                insertLaryngoscopeFeedbackIDs.Clear();
                insertLaryngoscopeFeedbackIDs = null;
            }
            insertLaryngoscopeFeedbackIDs = new List<int>();

            viewVocalChordsScore = 0;
            viewVocalChordsImageID = null;
            viewVocalChordsImage = null;
            if (viewVocalChordsFeedbackIDs != null)
            {
                viewVocalChordsFeedbackIDs.Clear();
                viewVocalChordsFeedbackIDs = null;
            }
            viewVocalChordsFeedbackIDs = new List<int>();

            grabEndotrachealTubeScore = 0;
            grabEndotrachealTubeImageID = null;
            grabEndotrachealTubeImage = null;
            if (grabEndotrachealTubeFeedbackIDs != null)
            {
                grabEndotrachealTubeFeedbackIDs.Clear();
                grabEndotrachealTubeFeedbackIDs = null;
            }
            grabEndotrachealTubeFeedbackIDs = new List<int>();

            insertEndotrachealTubeScore = 0;
            insertEndotrachealTubeImageID = null;
            insertEndotrachealTubeImage = null;
            if (insertEndotrachealTubeFeedbackIDs != null)
            {
                insertEndotrachealTubeFeedbackIDs.Clear();
                insertEndotrachealTubeFeedbackIDs = null;
            }
            insertEndotrachealTubeFeedbackIDs = new List<int>();

            removeLaryngoscopeScore = 0;
            removeLaryngoscopeImageID = null;
            removeLaryngoscopeImage = null;
            if (removeLaryngoscopeFeedbackIDs != null)
            {
                removeLaryngoscopeFeedbackIDs.Clear();
                removeLaryngoscopeFeedbackIDs = null;
            }
            removeLaryngoscopeFeedbackIDs = new List<int>();

            useSyringeScore = 0;
            useSyringeImageID = null;
            useSyringeImage = null;
            if (useSyringeFeedbackIDs != null)
            {
                useSyringeFeedbackIDs.Clear();
                useSyringeFeedbackIDs = null;
            }
            useSyringeFeedbackIDs = new List<int>();

            removeStyletScore = 0;
            removeStyletImageID = null;
            removeStyletImage = null;
            if (removeStyletFeedbackIDs != null)
            {
                removeStyletFeedbackIDs.Clear();
                removeStyletFeedbackIDs = null;
            }
            removeStyletFeedbackIDs = new List<int>();

            baggingScore = 0;
            baggingImageID = null;
            baggingImage = null;
            if (baggingFeedbackIDs != null)
            {
                baggingFeedbackIDs.Clear();
                baggingFeedbackIDs = null;
            }
            baggingFeedbackIDs = new List<int>();

            chestRiseScore = 0;
            chestRiseImageID = null;
            chestRiseImage = null;
            if (chestRiseFeedbackIDs != null)
            {
                chestRiseFeedbackIDs.Clear();
                chestRiseFeedbackIDs = null;
            }
            chestRiseFeedbackIDs = new List<int>();

            if (overallFeedbackIDs != null)
            {
                overallFeedbackIDs.Clear();
                overallFeedbackIDs = null;
            }
            overallFeedbackIDs = new List<int>();
        }
        #endregion

        #region Database Interaction Methods
        /// <summary>
        /// Stores the result into the database.
        /// </summary>
        /// <param name="result"></param>
        public static int StoreResult(IntubationResult result)
        {
            int id = 0;

            if (result == null)
                return id;

            #region Store the KinectImages - if they exist - to the images table.
            result.CalibrationImageID = KinectImage.StoreImage(result.CalibrationImage, IMAGES_TABLE_NAME);

            result.TiltHeadImageID = KinectImage.StoreImage(result.TiltHeadImage, IMAGES_TABLE_NAME);

            result.GrabLaryngoscopeImageID = KinectImage.StoreImage(result.GrabLaryngoscopeImage, IMAGES_TABLE_NAME);

            result.InsertLaryngoscopeImageID = KinectImage.StoreImage(result.InsertLaryngoscopeImage, IMAGES_TABLE_NAME);

            result.ViewVocalChordsImageID = KinectImage.StoreImage(result.ViewVocalChordsImage, IMAGES_TABLE_NAME);

            result.GrabEndotrachealTubeImageID = KinectImage.StoreImage(result.GrabEndotrachealTubeImage, IMAGES_TABLE_NAME);

            result.InsertEndotrachealTubeImageID = KinectImage.StoreImage(result.InsertEndotrachealTubeImage, IMAGES_TABLE_NAME);

            result.RemoveLaryngoscopeImageID = KinectImage.StoreImage(result.RemoveLaryngoscopeImage, IMAGES_TABLE_NAME);

            result.UseSyringeImageID = KinectImage.StoreImage(result.UseSyringeImage, IMAGES_TABLE_NAME);

            result.RemoveStyletImageID = KinectImage.StoreImage(result.RemoveStyletImage, IMAGES_TABLE_NAME);

            result.BaggingImageID = KinectImage.StoreImage(result.BaggingImage, IMAGES_TABLE_NAME);

            result.ChestRiseImageID = KinectImage.StoreImage(result.ChestRiseImage, IMAGES_TABLE_NAME);
            #endregion

            // Submit the overal result information to the overal results table - returning its generated uid.
            #region Store the overall result to results table.
            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "INSERT INTO " + OVERALL_RESULTS_TABLE_NAME + " ( studentid, assignmentid, tasktype, taskmode, masterylevel, score, starttime, endtime ) VALUES ( :studentid, :assignmentid, CAST( :tasktype as \"TaskType\"), CAST(:taskmode as \"TaskMode\"), CAST(:masterylevel as \"MasteryLevel\"), :score, :starttime, :endtime ) RETURNING uid;";

            command.Parameters.Add("studentid", NpgsqlTypes.NpgsqlDbType.Integer).Value = result.StudentID;
            command.Parameters.Add("assignmentid", NpgsqlTypes.NpgsqlDbType.Integer).Value = result.AssignmentID;
            command.Parameters.Add("tasktype", NpgsqlTypes.NpgsqlDbType.Text).Value = result.TaskType.ToString();
            command.Parameters.Add("taskmode", NpgsqlTypes.NpgsqlDbType.Text).Value = result.TaskMode.ToString();
            command.Parameters.Add("masterylevel", NpgsqlTypes.NpgsqlDbType.Text).Value = result.MasteryLevel.ToString();
            command.Parameters.Add("score", NpgsqlTypes.NpgsqlDbType.Double).Value = result.Score;
            command.Parameters.Add("starttime", NpgsqlTypes.NpgsqlDbType.Timestamp).Value = result.StartTime;
            command.Parameters.Add("endtime", NpgsqlTypes.NpgsqlDbType.Timestamp).Value = result.EndTime;

            try
            {
                id = (int)command.ExecuteScalar();  // Execute the query, attempting to store the result object, and returning the UID of the created database entry for additional relational usage.
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
            }

            DatabaseManager.CloseConnection();
            #endregion

            // Once we submit an overal result object to the overal results table, we submit a task-specific result to the task-specific results table - linked to the overal result submission, and returning its generated uid.
            #region Store the lateral transfer specific result to lateral transfer results table.
            int intubationresultid = 0;

            if (id > 0)
            {
                DatabaseManager.OpenConnection();

                NpgsqlCommand command2 = DatabaseManager.Connection.CreateCommand();
                command2.CommandType = System.Data.CommandType.Text;
                command2.CommandText = "INSERT INTO " + INTUBATION_RESULTS_TABLE_NAME + " ( resultid, calibrationimageid, tiltheadscore, tiltheadimageid, tiltheadfeedbackids, grablaryngoscopescore, grablaryngoscopeimageid, grablaryngoscopefeedbackids, insertlaryngoscopescore, insertlaryngoscopeimageid, insertlaryngoscopefeedbackids, viewvocalchordsscore, viewvocalchordsimageid, viewvocalchordsfeedbackids, grabendotrachealtubescore, grabendotrachealtubeimageid, grabendotrachealtubefeedbackids, insertendotrachealtubescore, insertendotrachealtubeimageid, insertendotrachealtubefeedbackids, removelaryngoscopescore, removelaryngoscopeimageid, removelaryngoscopefeedbackids, usesyringescore, usesyringeimageid, usesyringefeedbackids, removestyletscore, removestyletimageid, removestyletfeedbackids, baggingscore, baggingimageid, baggingfeedbackids, chestrisescore, chestriseimageid, chestrisefeedbackids, overallfeedbackids ) VALUES ( :resultid, :calibrationimageid, :tiltheadscore, :tiltheadimageid, :tiltheadfeedbackids, :grablaryngoscopescore, :grablaryngoscopeimageid, :grablaryngoscopefeedbackids, :insertlaryngoscopescore, :insertlaryngoscopeimageid, :insertlaryngoscopefeedbackids, :viewvocalchordsscore, :viewvocalchordsimageid, :viewvocalchordsfeedbackids, :grabendotrachealtubescore, :grabendotrachealtubeimageid, :grabendotrachealtubefeedbackids, :insertendotrachealtubescore, :insertendotrachealtubeimageid, :insertendotrachealtubefeedbackids, :removelaryngoscopescore, :removelaryngoscopeimageid, :removelaryngoscopefeedbackids, :usesyringescore, :usesyringeimageid, :usesyringefeedbackids, :removestyletscore, :removestyletimageid, :removestyletfeedbackids, :baggingscore, :baggingimageid, :baggingfeedbackids, :chestrisescore, :chestriseimageid, :chestrisefeedbackids, :overallfeedbackids ) RETURNING uid;";

                command2.Parameters.Add("resultid", NpgsqlTypes.NpgsqlDbType.Integer).Value = id;
                command2.Parameters.Add("calibrationimageid", NpgsqlTypes.NpgsqlDbType.Integer).Value = result.CalibrationImageID;
                command2.Parameters.Add("tiltheadscore", NpgsqlTypes.NpgsqlDbType.Double).Value = result.TiltHeadScore;
                command2.Parameters.Add("tiltheadimageid", NpgsqlTypes.NpgsqlDbType.Integer).Value = result.TiltHeadImageID;
                command2.Parameters.Add("tiltheadfeedbackids", NpgsqlTypes.NpgsqlDbType.Integer | NpgsqlTypes.NpgsqlDbType.Array).Value = result.TiltHeadFeedbackIDs.ToArray();
                command2.Parameters.Add("grablaryngoscopescore", NpgsqlTypes.NpgsqlDbType.Double).Value = result.GrabLaryngoscopeScore;
                command2.Parameters.Add("grablaryngoscopeimageid", NpgsqlTypes.NpgsqlDbType.Integer).Value = result.GrabLaryngoscopeImageID;
                command2.Parameters.Add("grablaryngoscopefeedbackids", NpgsqlTypes.NpgsqlDbType.Integer | NpgsqlTypes.NpgsqlDbType.Array).Value = result.GrabLaryngoscopeFeedbackIDs.ToArray();
                command2.Parameters.Add("insertlaryngoscopescore", NpgsqlTypes.NpgsqlDbType.Double).Value = result.InsertLaryngoscopeScore;
                command2.Parameters.Add("insertlaryngoscopeimageid", NpgsqlTypes.NpgsqlDbType.Integer).Value = result.InsertLaryngoscopeImageID;
                command2.Parameters.Add("insertlaryngoscopefeedbackids", NpgsqlTypes.NpgsqlDbType.Integer | NpgsqlTypes.NpgsqlDbType.Array).Value = result.InsertLaryngoscopeFeedbackIDs.ToArray();
                command2.Parameters.Add("viewvocalchordsscore", NpgsqlTypes.NpgsqlDbType.Double).Value = result.ViewVocalChordsScore;
                command2.Parameters.Add("viewvocalchordsimageid", NpgsqlTypes.NpgsqlDbType.Integer).Value = result.ViewVocalChordsImageID;
                command2.Parameters.Add("viewvocalchordsfeedbackids", NpgsqlTypes.NpgsqlDbType.Integer | NpgsqlTypes.NpgsqlDbType.Array).Value = result.ViewVocalChordsFeedbackIDs.ToArray();
                command2.Parameters.Add("grabendotrachealtubescore", NpgsqlTypes.NpgsqlDbType.Double).Value = result.GrabEndotrachealTubeScore;
                command2.Parameters.Add("grabendotrachealtubeimageid", NpgsqlTypes.NpgsqlDbType.Integer).Value = result.GrabEndotrachealTubeImageID;
                command2.Parameters.Add("grabendotrachealtubefeedbackids", NpgsqlTypes.NpgsqlDbType.Integer | NpgsqlTypes.NpgsqlDbType.Array).Value = result.GrabEndotrachealTubeFeedbackIDs.ToArray();
                command2.Parameters.Add("insertendotrachealtubescore", NpgsqlTypes.NpgsqlDbType.Double).Value = result.InsertEndotrachealTubeScore;
                command2.Parameters.Add("insertendotrachealtubeimageid", NpgsqlTypes.NpgsqlDbType.Integer).Value = result.InsertEndotrachealTubeImageID;
                command2.Parameters.Add("insertendotrachealtubefeedbackids", NpgsqlTypes.NpgsqlDbType.Integer | NpgsqlTypes.NpgsqlDbType.Array).Value = result.InsertEndotrachealTubeFeedbackIDs.ToArray();
                command2.Parameters.Add("removelaryngoscopescore", NpgsqlTypes.NpgsqlDbType.Double).Value = result.RemoveLaryngoscopeScore;
                command2.Parameters.Add("removelaryngoscopeimageid", NpgsqlTypes.NpgsqlDbType.Integer).Value = result.RemoveLaryngoscopeImageID;
                command2.Parameters.Add("removelaryngoscopefeedbackids", NpgsqlTypes.NpgsqlDbType.Integer | NpgsqlTypes.NpgsqlDbType.Array).Value = result.RemoveLaryngoscopeFeedbackIDs.ToArray();
                command2.Parameters.Add("usesyringescore", NpgsqlTypes.NpgsqlDbType.Double).Value = result.UseSyringeScore;
                command2.Parameters.Add("usesyringeimageid", NpgsqlTypes.NpgsqlDbType.Integer).Value = result.UseSyringeImageID;
                command2.Parameters.Add("usesyringefeedbackids", NpgsqlTypes.NpgsqlDbType.Integer | NpgsqlTypes.NpgsqlDbType.Array).Value = result.UseSyringeFeedbackIDs.ToArray();
                command2.Parameters.Add("removestyletscore", NpgsqlTypes.NpgsqlDbType.Double).Value = result.RemoveStyletScore;
                command2.Parameters.Add("removestyletimageid", NpgsqlTypes.NpgsqlDbType.Integer).Value = result.RemoveStyletImageID;
                command2.Parameters.Add("removestyletfeedbackids", NpgsqlTypes.NpgsqlDbType.Integer | NpgsqlTypes.NpgsqlDbType.Array).Value = result.RemoveStyletFeedbackIDs.ToArray();
                command2.Parameters.Add("baggingscore", NpgsqlTypes.NpgsqlDbType.Double).Value = result.BaggingScore;
                command2.Parameters.Add("baggingimageid", NpgsqlTypes.NpgsqlDbType.Integer).Value = result.BaggingImageID;
                command2.Parameters.Add("baggingfeedbackids", NpgsqlTypes.NpgsqlDbType.Integer | NpgsqlTypes.NpgsqlDbType.Array).Value = result.BaggingFeedbackIDs.ToArray();
                command2.Parameters.Add("chestrisescore", NpgsqlTypes.NpgsqlDbType.Double).Value = result.ChestRiseScore;
                command2.Parameters.Add("chestriseimageid", NpgsqlTypes.NpgsqlDbType.Integer).Value = result.ChestRiseImageID;
                command2.Parameters.Add("chestrisefeedbackids", NpgsqlTypes.NpgsqlDbType.Integer | NpgsqlTypes.NpgsqlDbType.Array).Value = result.ChestRiseFeedbackIDs.ToArray();

                command2.Parameters.Add("overallfeedbackids", NpgsqlTypes.NpgsqlDbType.Integer | NpgsqlTypes.NpgsqlDbType.Array).Value = result.OverallFeedbackIDs.ToArray();

                try
                {
                    intubationresultid = (int)command2.ExecuteScalar();  // Execute the query, attempting to store the result object, and returning the UID of the created database entry for additional relational usage.
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.ToString());
                }

                DatabaseManager.CloseConnection();

            }
            else
            {
                // No overall result id was returned - running the next query would be a waste since we already know it won't point to any overall result.
                MessageBox.Show("Error submitting result to the database.");
            }
            #endregion

            // Return the uid of the submitted overal result for relational purposes.
            return id;
        }

        /// <summary>
        /// Retrieves a result from a resultid.
        /// 
        /// If retrieveImages is set to true, the result's images will also be pulled and stored in the result object.
        /// </summary>
        /// <param name="resultid"></param>
        /// <param name="retrieveImages"></param>
        /// <returns></returns>
        public static IntubationResult RetrieveResult(int resultid, bool retrieveImages)
        {
            if (resultid <= 0)
                return null;

            IntubationResult result = new IntubationResult();

            #region Retrieve the basic result information.
            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "SELECT * FROM " + OVERALL_RESULTS_TABLE_NAME + " WHERE uid=:id";

            command.Parameters.Add("id", NpgsqlTypes.NpgsqlDbType.Integer).Value = resultid;

            NpgsqlDataReader reader = command.ExecuteReader();

            int studentid = 0;
            int? assignmentid = 0;
            Task tasktype = Task.None;
            TaskMode taskmode = TaskMode.Practice;
            MasteryLevel masterylevel = MasteryLevel.Novice;
            double score = 0;
            DateTime starttime = DateTime.MinValue;
            DateTime endtime = DateTime.MinValue;
            DateTime submissiontime = DateTime.MinValue;

            while (reader.Read())
            {
                try
                {
                    studentid = reader.GetInt32(reader.GetOrdinal("studentid"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    assignmentid = reader.IsDBNull(reader.GetOrdinal("assignmentid")) ? null : (int?)reader.GetInt32(reader.GetOrdinal("assignmentid"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    tasktype = (Task)Enum.Parse(typeof(Task), reader.GetString(reader.GetOrdinal("tasktype")));
                }
                catch (Exception e)
                {
                }
                try
                {
                    taskmode = (TaskMode)Enum.Parse(typeof(TaskMode), reader.GetString(reader.GetOrdinal("taskmode")));
                }
                catch (Exception e)
                {
                }
                try
                {
                    masterylevel = (MasteryLevel)Enum.Parse(typeof(MasteryLevel), reader.GetString(reader.GetOrdinal("masterylevel")));
                }
                catch (Exception e)
                {
                }
                try
                {
                    score = reader.GetDouble(reader.GetOrdinal("score"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    starttime = reader.GetTimeStamp(reader.GetOrdinal("starttime"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    endtime = reader.GetTimeStamp(reader.GetOrdinal("endtime"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    submissiontime = reader.GetTimeStamp(reader.GetOrdinal("submissiontime"));
                }
                catch (Exception e)
                {
                }
            }

            reader.Dispose();

            DatabaseManager.CloseConnection();

            result.UID = resultid;
            result.StudentID = studentid;
            result.AssignmentID = assignmentid;
            result.TaskType = tasktype;
            result.TaskMode = taskmode;
            result.MasteryLevel = masterylevel;
            result.Score = score;
            result.StartTime = starttime;
            result.EndTime = endtime;
            result.SubmissionTime = submissiontime;
            #endregion

            #region Retrieve the intubation specific content.
            DatabaseManager.OpenConnection();

            NpgsqlCommand command2 = DatabaseManager.Connection.CreateCommand();
            command2.CommandType = System.Data.CommandType.Text;
            command2.CommandText = "SELECT calibrationimageid, tiltheadscore, tiltheadimageid, tiltheadfeedbackids, grablaryngoscopescore, grablaryngoscopeimageid, grablaryngoscopefeedbackids, insertlaryngoscopescore, insertlaryngoscopeimageid, insertlaryngoscopefeedbackids, viewvocalchordsscore, viewvocalchordsimageid, viewvocalchordsfeedbackids, grabendotrachealtubescore, grabendotrachealtubeimageid, grabendotrachealtubefeedbackids, insertendotrachealtubescore, insertendotrachealtubeimageid, insertendotrachealtubefeedbackids, removelaryngoscopescore, removelaryngoscopeimageid, removelaryngoscopefeedbackids, usesyringescore, usesyringeimageid, usesyringefeedbackids, removestyletscore, removestyletimageid, removestyletfeedbackids, baggingscore, baggingimageid, baggingfeedbackids, chestrisescore, chestriseimageid, chestrisefeedbackids, overallfeedbackids FROM " + INTUBATION_RESULTS_TABLE_NAME + " WHERE resultid=:id";

            command2.Parameters.Add("id", NpgsqlTypes.NpgsqlDbType.Integer).Value = resultid;

            NpgsqlDataReader reader2 = command2.ExecuteReader();

            int? calibrationimageid = 0;

            double tiltheadscore = 0;
            int? tiltheadimageid = null;
            int[] tiltheadfeedbackids = null;

            double grablaryngoscopescore = 0;
            int? grablaryngoscopeimageid = null;
            int[] grablaryngoscopefeedbackids = null;

            double insertlaryngoscopescore = 0;
            int? insertlaryngoscopeimageid = null;
            int[] insertlaryngoscopefeedbackids = null;

            double viewvocalchordsscore = 0;
            int? viewvocalchordsimageid = null;
            int[] viewvocalchordsfeedbackids = null;

            double grabendotrachealtubescore = 0;
            int? grabendotrachealtubeimageid = null;
            int[] grabendotrachealtubefeedbackids = null;

            double insertendotrachealtubescore = 0;
            int? insertendotrachealtubeimageid = null;
            int[] insertendotrachealtubefeedbackids = null;

            double removelaryngoscopescore = 0;
            int? removelaryngoscopeimageid = null;
            int[] removelaryngoscopefeedbackids = null;

            double usesyringescore = 0;
            int? usesyringeimageid = null;
            int[] usesyringefeedbackids = null;

            double removestyletscore = 0;
            int? removestyletimageid = null;
            int[] removestyletfeedbackids = null;

            double baggingscore = 0;
            int? baggingimageid = null;
            int[] baggingfeedbackids = null;

            double chestrisescore = 0;
            int? chestriseimageid = null;
            int[] chestrisefeedbackids = null;

            int[] overallfeedbackids = null;

            while (reader2.Read())
            {
                try
                {
                    var ordinal = reader2.GetOrdinal("calibrationimageid");

                    calibrationimageid = reader2.IsDBNull(ordinal) ? null : (int?)reader2.GetInt32(ordinal);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    tiltheadscore = reader2.GetDouble(reader2.GetOrdinal("tiltheadscore"));
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    var ordinal = reader2.GetOrdinal("tiltheadimageid");
                    tiltheadimageid = reader2.IsDBNull(ordinal) ? null : (int?)reader2.GetInt32(ordinal);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    tiltheadfeedbackids = reader2.GetValue(reader2.GetOrdinal("tiltheadfeedbackids")) as int[];
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    grablaryngoscopescore = reader2.GetDouble(reader2.GetOrdinal("grablaryngoscopescore"));
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    var ordinal = reader2.GetOrdinal("grablaryngoscopeimageid");
                    grablaryngoscopeimageid = reader2.IsDBNull(ordinal) ? null : (int?)reader2.GetInt32(ordinal);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    grablaryngoscopefeedbackids = reader2.GetValue(reader2.GetOrdinal("grablaryngoscopefeedbackids")) as int[];
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    insertlaryngoscopescore = reader2.GetDouble(reader2.GetOrdinal("insertlaryngoscopescore"));
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    var ordinal = reader2.GetOrdinal("insertlaryngoscopeimageid");
                    insertlaryngoscopeimageid = reader2.IsDBNull(ordinal) ? null : (int?)reader2.GetInt32(ordinal);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    insertlaryngoscopefeedbackids = reader2.GetValue(reader2.GetOrdinal("insertlaryngoscopefeedbackids")) as int[];
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    viewvocalchordsscore = reader2.GetDouble(reader2.GetOrdinal("viewvocalchordsscore"));
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    var ordinal = reader2.GetOrdinal("viewvocalchordsimageid");
                    viewvocalchordsimageid = reader2.IsDBNull(ordinal) ? null : (int?)reader2.GetInt32(ordinal);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    viewvocalchordsfeedbackids = reader2.GetValue(reader2.GetOrdinal("viewvocalchordsfeedbackids")) as int[];
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    grabendotrachealtubescore = reader2.GetDouble(reader2.GetOrdinal("grabendotrachealtubescore"));
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    var ordinal = reader2.GetOrdinal("grabendotrachealtubeimageid");
                    grabendotrachealtubeimageid = reader2.IsDBNull(ordinal) ? null : (int?)reader2.GetInt32(ordinal);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    grabendotrachealtubefeedbackids = reader2.GetValue(reader2.GetOrdinal("grabendotrachealtubefeedbackids")) as int[];
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    insertendotrachealtubescore = reader2.GetDouble(reader2.GetOrdinal("insertendotrachealtubescore"));
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    var ordinal = reader2.GetOrdinal("insertendotrachealtubeimageid");
                    insertendotrachealtubeimageid = reader2.IsDBNull(ordinal) ? null : (int?)reader2.GetInt32(ordinal);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    insertendotrachealtubefeedbackids = reader2.GetValue(reader2.GetOrdinal("insertendotrachealtubefeedbackids")) as int[];
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    removelaryngoscopescore = reader2.GetDouble(reader2.GetOrdinal("removelaryngoscopescore"));
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    var ordinal = reader2.GetOrdinal("removelaryngoscopeimageid");
                    removelaryngoscopeimageid = reader2.IsDBNull(ordinal) ? null : (int?)reader2.GetInt32(ordinal);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    removelaryngoscopefeedbackids = reader2.GetValue(reader2.GetOrdinal("removelaryngoscopefeedbackids")) as int[];
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    usesyringescore = reader2.GetDouble(reader2.GetOrdinal("usesyringescore"));
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    var ordinal = reader2.GetOrdinal("usesyringeimageid");
                    usesyringeimageid = reader2.IsDBNull(ordinal) ? null : (int?)reader2.GetInt32(ordinal);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    usesyringefeedbackids = reader2.GetValue(reader2.GetOrdinal("usesyringefeedbackids")) as int[];
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    removestyletscore = reader2.GetDouble(reader2.GetOrdinal("removestyletscore"));
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    var ordinal = reader2.GetOrdinal("removestyletimageid");
                    removestyletimageid = reader2.IsDBNull(ordinal) ? null : (int?)reader2.GetInt32(ordinal);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    removestyletfeedbackids = reader2.GetValue(reader2.GetOrdinal("removestyletfeedbackids")) as int[];
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    baggingscore = reader2.GetDouble(reader2.GetOrdinal("baggingscore"));
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    var ordinal = reader2.GetOrdinal("baggingimageid");
                    baggingimageid = reader2.IsDBNull(ordinal) ? null : (int?)reader2.GetInt32(ordinal);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    baggingfeedbackids = reader2.GetValue(reader2.GetOrdinal("baggingfeedbackids")) as int[];
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    chestrisescore = reader2.GetDouble(reader2.GetOrdinal("chestrisescore"));
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    var ordinal = reader2.GetOrdinal("chestriseimageid");
                    chestriseimageid = reader2.IsDBNull(ordinal) ? null : (int?)reader2.GetInt32(ordinal);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    chestrisefeedbackids = reader2.GetValue(reader2.GetOrdinal("chestrisefeedbackids")) as int[];
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
                try
                {
                    overallfeedbackids = reader2.GetValue(reader2.GetOrdinal("overallfeedbackids")) as int[];
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message.ToString());
                }
            }

            reader2.Dispose();

            DatabaseManager.CloseConnection();

            result.CalibrationImageID = calibrationimageid;

            result.TiltHeadScore = tiltheadscore;
            result.TiltHeadImageID = tiltheadimageid;
            result.TiltHeadFeedbackIDs = ( tiltheadfeedbackids == null ? null : tiltheadfeedbackids.ToList<int>() );

            result.GrabLaryngoscopeScore = grablaryngoscopescore;
            result.GrabLaryngoscopeImageID = grablaryngoscopeimageid;
            result.GrabLaryngoscopeFeedbackIDs = ( grablaryngoscopefeedbackids == null ? null : grablaryngoscopefeedbackids.ToList<int>() );

            result.InsertLaryngoscopeScore = insertlaryngoscopescore;
            result.InsertLaryngoscopeImageID = insertlaryngoscopeimageid;
            result.InsertLaryngoscopeFeedbackIDs = ( insertlaryngoscopefeedbackids == null ? null : insertlaryngoscopefeedbackids.ToList<int>() );

            result.ViewVocalChordsScore = viewvocalchordsscore;
            result.ViewVocalChordsImageID = viewvocalchordsimageid;
            result.ViewVocalChordsFeedbackIDs = ( viewvocalchordsfeedbackids == null ? null : viewvocalchordsfeedbackids.ToList<int>() );

            result.GrabEndotrachealTubeScore = grabendotrachealtubescore;
            result.GrabEndotrachealTubeImageID = grabendotrachealtubeimageid;
            result.GrabEndotrachealTubeFeedbackIDs = ( grabendotrachealtubefeedbackids == null ? null : grabendotrachealtubefeedbackids.ToList<int>() );

            result.InsertEndotrachealTubeScore = insertendotrachealtubescore;
            result.InsertEndotrachealTubeImageID = insertendotrachealtubeimageid;
            result.InsertEndotrachealTubeFeedbackIDs = ( insertendotrachealtubefeedbackids == null ? null : insertendotrachealtubefeedbackids.ToList<int>() );

            result.RemoveLaryngoscopeScore = removelaryngoscopescore;
            result.RemoveLaryngoscopeImageID = removelaryngoscopeimageid;
            result.RemoveLaryngoscopeFeedbackIDs = ( removelaryngoscopefeedbackids == null ? null : removelaryngoscopefeedbackids.ToList<int>() );

            result.UseSyringeScore = usesyringescore;
            result.UseSyringeImageID = usesyringeimageid;
            result.UseSyringeFeedbackIDs = ( usesyringefeedbackids == null ? null : usesyringefeedbackids.ToList<int>() );

            result.RemoveStyletScore = removestyletscore;
            result.RemoveStyletImageID = removestyletimageid;
            result.RemoveStyletFeedbackIDs = ( removestyletfeedbackids == null ? null : removestyletfeedbackids.ToList<int>() );

            result.BaggingScore = baggingscore;
            result.BaggingImageID = baggingimageid;
            result.BaggingFeedbackIDs = ( baggingfeedbackids == null ? null : baggingfeedbackids.ToList<int>() );

            result.ChestRiseScore = chestrisescore;
            result.ChestRiseImageID = chestriseimageid;
            result.ChestRiseFeedbackIDs = ( chestrisefeedbackids == null ? null : chestrisefeedbackids.ToList<int>() );

            result.OverallFeedbackIDs = ( overallfeedbackids == null ?  null : overallfeedbackids.ToList<int>() );

            #region Retrieve the Images if retrieveImages paramater is true.
            if (retrieveImages)
            {
                if (result.CalibrationImageID.HasValue && result.CalibrationImageID > 0)
                {
                    result.CalibrationImage = KinectImage.RetreiveImage((int)result.CalibrationImageID, IMAGES_TABLE_NAME);
                }

                if (result.TiltHeadImageID.HasValue && result.TiltHeadImageID > 0)
                {
                    result.TiltHeadImage = KinectImage.RetreiveImage((int)result.TiltHeadImageID, IMAGES_TABLE_NAME);
                }

                if (result.GrabLaryngoscopeImageID.HasValue && result.GrabLaryngoscopeImageID > 0)
                {
                    result.GrabLaryngoscopeImage = KinectImage.RetreiveImage((int)result.GrabLaryngoscopeImageID, IMAGES_TABLE_NAME);
                }

                if (result.InsertLaryngoscopeImageID.HasValue && result.InsertLaryngoscopeImageID > 0)
                {
                    result.InsertLaryngoscopeImage = KinectImage.RetreiveImage((int)result.InsertLaryngoscopeImageID, IMAGES_TABLE_NAME);
                }

                if (result.ViewVocalChordsImageID.HasValue && result.ViewVocalChordsImageID > 0)
                {
                    result.ViewVocalChordsImage = KinectImage.RetreiveImage((int)result.ViewVocalChordsImageID, IMAGES_TABLE_NAME);
                }

                if (result.GrabEndotrachealTubeImageID.HasValue && result.GrabEndotrachealTubeImageID > 0)
                {
                    result.GrabEndotrachealTubeImage = KinectImage.RetreiveImage((int)result.GrabEndotrachealTubeImageID, IMAGES_TABLE_NAME);
                }

                if (result.InsertEndotrachealTubeImageID.HasValue && result.InsertEndotrachealTubeImageID > 0)
                {
                    result.InsertEndotrachealTubeImage = KinectImage.RetreiveImage((int)result.InsertEndotrachealTubeImageID, IMAGES_TABLE_NAME);
                }

                if (result.RemoveLaryngoscopeImageID.HasValue && result.RemoveLaryngoscopeImageID > 0)
                {
                    result.RemoveLaryngoscopeImage = KinectImage.RetreiveImage((int)result.RemoveLaryngoscopeImageID, IMAGES_TABLE_NAME);
                }

                if (result.UseSyringeImageID.HasValue && result.UseSyringeImageID > 0)
                {
                    result.UseSyringeImage = KinectImage.RetreiveImage((int)result.UseSyringeImageID, IMAGES_TABLE_NAME);
                }

                if (result.RemoveStyletImageID.HasValue && result.RemoveStyletImageID > 0)
                {
                    result.RemoveStyletImage = KinectImage.RetreiveImage((int)result.RemoveStyletImageID, IMAGES_TABLE_NAME);
                }

                if (result.BaggingImageID.HasValue && result.BaggingImageID > 0)
                {
                    result.BaggingImage = KinectImage.RetreiveImage((int)result.BaggingImageID, IMAGES_TABLE_NAME);
                }

                if (result.ChestRiseImageID.HasValue && result.ChestRiseImageID > 0)
                {
                    result.ChestRiseImage = KinectImage.RetreiveImage((int)result.ChestRiseImageID, IMAGES_TABLE_NAME);
                }
            }
            #endregion

            #endregion

            return result;
        }

        /// <summary>
        /// Retrieve images from the database and store them in the passed IntubationResult object.
        /// </summary>
        /// <param name="Result">IntubationResult object to retrieve images for.</param>
        /// <returns>True if successful, False if not.</returns>
        public static bool RetrieveImages(ref IntubationResult Result)
        {
            // Fail if UID is not set, or is an invalid value.
            if (Result.UID <= 0)
                return false;

            bool endState = true;

            // Build list of image id's to get
            List<int> imageIDs = new List<int>();

            // Figure out which Images to get.
            if (Result.CalibrationImage == null && Result.CalibrationImageID.HasValue)
                imageIDs.Add(Result.CalibrationImageID.Value);
            if (Result.TiltHeadImage == null & Result.TiltHeadImageID.HasValue)
                imageIDs.Add(Result.TiltHeadImageID.Value);
            if (Result.GrabLaryngoscopeImage == null && Result.GrabLaryngoscopeImageID.HasValue)
                imageIDs.Add(Result.GrabLaryngoscopeImageID.Value);
            if (Result.InsertLaryngoscopeImage == null && Result.InsertLaryngoscopeImageID.HasValue)
                imageIDs.Add(Result.InsertLaryngoscopeImageID.Value);
            if (Result.ViewVocalChordsImage == null && Result.ViewVocalChordsImageID.HasValue)
                imageIDs.Add(Result.ViewVocalChordsImageID.Value);
            if (Result.GrabEndotrachealTubeImage == null && Result.GrabEndotrachealTubeImageID.HasValue)
                imageIDs.Add(Result.GrabEndotrachealTubeImageID.Value);
            if (Result.InsertEndotrachealTubeImage == null && Result.InsertEndotrachealTubeImageID.HasValue)
                imageIDs.Add(Result.InsertEndotrachealTubeImageID.Value);
            if (Result.RemoveLaryngoscopeImage == null && Result.RemoveLaryngoscopeImageID.HasValue)
                imageIDs.Add(Result.RemoveLaryngoscopeImageID.Value);
            if (Result.UseSyringeImage == null && Result.UseSyringeImageID.HasValue)
                imageIDs.Add(Result.UseSyringeImageID.Value);
            if (Result.RemoveStyletImage == null && Result.RemoveStyletImageID.HasValue)
                imageIDs.Add(Result.RemoveStyletImageID.Value);
            if (Result.BaggingImage == null && Result.BaggingImageID.HasValue)
                imageIDs.Add(Result.BaggingImageID.Value);
            if (Result.ChestRiseImage == null && Result.ChestRiseImageID.HasValue)
                imageIDs.Add(Result.ChestRiseImageID.Value);

            // Pull images from DataBase.
            Dictionary<int, KinectImage> kinectImages = KinectImage.RetreiveImages(imageIDs.ToArray(), IMAGES_TABLE_NAME);

            if (kinectImages != null && kinectImages.Count > 0)
            {
                // Refill the images.
                try
                {
                    if (Result.CalibrationImage == null && Result.CalibrationImageID.HasValue && kinectImages.ContainsKey(Result.CalibrationImageID.Value))
                        Result.CalibrationImage = kinectImages[Result.CalibrationImageID.Value];
                }
                catch (Exception exc) { System.Diagnostics.Debug.Print("IntubationResult RetrieveImages ERROR: {0}", exc.Message); endState = false; }
                try
                {
                    if (Result.TiltHeadImage == null && Result.TiltHeadImageID.HasValue && kinectImages.ContainsKey(Result.TiltHeadImageID.Value))
                        Result.TiltHeadImage = kinectImages[Result.TiltHeadImageID.Value];
                }
                catch (Exception exc) { System.Diagnostics.Debug.Print("IntubationResult RetrieveImages ERROR: {0}", exc.Message); endState = false; }
                try
                {
                    if (Result.GrabLaryngoscopeImage == null && Result.GrabLaryngoscopeImageID.HasValue && kinectImages.ContainsKey(Result.GrabLaryngoscopeImageID.Value))
                        Result.GrabLaryngoscopeImage = kinectImages[Result.GrabLaryngoscopeImageID.Value];
                }
                catch (Exception exc) { System.Diagnostics.Debug.Print("IntubationResult RetrieveImages ERROR: {0}", exc.Message); endState = false; }
                try
                {
                    if (Result.InsertLaryngoscopeImage == null && Result.InsertLaryngoscopeImageID.HasValue && kinectImages.ContainsKey(Result.InsertLaryngoscopeImageID.Value))
                        Result.InsertLaryngoscopeImage = kinectImages[Result.InsertLaryngoscopeImageID.Value];
                }
                catch (Exception exc) { System.Diagnostics.Debug.Print("IntubationResult RetrieveImages ERROR: {0}", exc.Message); endState = false; }
                try
                {
                    if (Result.ViewVocalChordsImage == null && Result.ViewVocalChordsImageID.HasValue && kinectImages.ContainsKey(Result.ViewVocalChordsImageID.Value))
                        Result.ViewVocalChordsImage = kinectImages[Result.ViewVocalChordsImageID.Value];
                }
                catch (Exception exc) { System.Diagnostics.Debug.Print("IntubationResult RetrieveImages ERROR: {0}", exc.Message); endState = false; }
                try
                {
                    if (Result.GrabEndotrachealTubeImage == null && Result.GrabEndotrachealTubeImageID.HasValue && kinectImages.ContainsKey(Result.GrabEndotrachealTubeImageID.Value))
                        Result.GrabEndotrachealTubeImage = kinectImages[Result.GrabEndotrachealTubeImageID.Value];
                }
                catch (Exception exc) { System.Diagnostics.Debug.Print("IntubationResult RetrieveImages ERROR: {0}", exc.Message); endState = false; }
                try
                {
                    if (Result.InsertEndotrachealTubeImage == null && Result.InsertEndotrachealTubeImageID.HasValue && kinectImages.ContainsKey(Result.InsertEndotrachealTubeImageID.Value))
                        Result.InsertEndotrachealTubeImage = kinectImages[Result.InsertEndotrachealTubeImageID.Value];
                }
                catch (Exception exc) { System.Diagnostics.Debug.Print("IntubationResult RetrieveImages ERROR: {0}", exc.Message); endState = false; }
                try
                {
                    if (Result.RemoveLaryngoscopeImage == null && Result.RemoveLaryngoscopeImageID.HasValue && kinectImages.ContainsKey(Result.RemoveLaryngoscopeImageID.Value))
                        Result.RemoveLaryngoscopeImage = kinectImages[Result.RemoveLaryngoscopeImageID.Value];
                }
                catch (Exception exc) { System.Diagnostics.Debug.Print("IntubationResult RetrieveImages ERROR: {0}", exc.Message); endState = false; }
                try
                {
                    if (Result.UseSyringeImage == null && Result.UseSyringeImageID.HasValue && kinectImages.ContainsKey(Result.UseSyringeImageID.Value))
                        Result.UseSyringeImage = kinectImages[Result.UseSyringeImageID.Value];
                }
                catch (Exception exc) { System.Diagnostics.Debug.Print("IntubationResult RetrieveImages ERROR: {0}", exc.Message); endState = false; }
                try
                {
                    if (Result.RemoveStyletImage == null && Result.RemoveStyletImageID.HasValue && kinectImages.ContainsKey(Result.RemoveStyletImageID.Value))
                        Result.RemoveStyletImage = kinectImages[Result.RemoveStyletImageID.Value];
                }
                catch (Exception exc) { System.Diagnostics.Debug.Print("IntubationResult RetrieveImages ERROR: {0}", exc.Message); endState = false; }
                try
                {
                    if (Result.BaggingImage == null && Result.BaggingImageID.HasValue && kinectImages.ContainsKey(Result.BaggingImageID.Value))
                        Result.BaggingImage = kinectImages[Result.BaggingImageID.Value];
                }
                catch (Exception exc) { System.Diagnostics.Debug.Print("IntubationResult RetrieveImages ERROR: {0}", exc.Message); endState = false; }
                try
                {
                    if (Result.ChestRiseImage == null && Result.ChestRiseImageID.HasValue && kinectImages.ContainsKey(Result.ChestRiseImageID.Value))
                        Result.ChestRiseImage = kinectImages[Result.ChestRiseImageID.Value];
                }
                catch (Exception exc) { System.Diagnostics.Debug.Print("IntubationResult RetrieveImages ERROR: {0}", exc.Message); endState = false; }
            }

            return endState;
        }

        #endregion


    } // End class IntubationResult


} // End namespace AIMS.Tasks.Intubation
