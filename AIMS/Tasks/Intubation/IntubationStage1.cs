﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using System.Windows.Media.Imaging;
using System.Windows;
using System.ComponentModel;
using System.Diagnostics;

using Microsoft.Kinect;

using AIMS.Assets.Lessons2;

namespace AIMS.Tasks.Intubation
{
    public class IntubationStage1 : Stage
    {
        private Intubation lesson;
        public Intubation Lesson { get { return lesson; } set { lesson = value; } }

        private bool wasHeadTiltedBack = false;
        private bool rightHandOrWristByManikinsChin = false;
        private bool leftHandOrWristByManikinsChin = false;
        private Point3D newHeadLocation = new Point3D();

        public bool WasHeadTiltedBack { get { return wasHeadTiltedBack; } set { wasHeadTiltedBack = value; } }
        public bool RightHandOrWristByManikinsChin { get { return rightHandOrWristByManikinsChin; } set { rightHandOrWristByManikinsChin = value; } }
        public bool LeftHandOrWristByManikinsChin { get { return leftHandOrWristByManikinsChin; } set { leftHandOrWristByManikinsChin = value; } }
        public Point3D NewHeadLocation { get { return newHeadLocation; } set { newHeadLocation = value; } }

        private Point3D watcher_FoundManikinHeadPos = new Point3D();
        private int watcher_YellowCounter = 0;
        private int watcher_YellowDepthCounter = 0;
        private bool watcher_FoundChin = false;

        private bool manikinHeadMoved = false;

        private bool laryngoscopeMoving = false;
        public Point3D oldLaryngoscopePosition = new Point3D();

        private ColorRange _manikinChinColor;
        private ColorRange _laryngoscopeColor;

        public IntubationStage1(Intubation intubation)
        {
            Lesson = intubation;

            StageNumber = 1;
            Name = "Tilt Head";
            Instruction = "Use both hands to tilt the manikin's head back slightly.";
            TargetTime = TimeSpan.FromSeconds(3.5);
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Tasks/Intubation/Graphics/Intubation001.png";
            Active = false;
            Complete = false;
            ScoreWeight = 0.20;
            Zones = new List<Zone>();
            IsSetup = false;
            Stopwatch = new System.Diagnostics.Stopwatch();
            this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.CanTimeOut = false;
            this.TimeOutTime = TimeSpan.FromSeconds(10.0);
        }

        public override void SetupStage()
        {
            this.Lesson.IntubationPage.StageInfoTextBlock.Text = this.Name;
            this.Lesson.IntubationPage.InstructionTextBlock.Text = this.Instruction;

            this.Lesson.IntubationPage.flow.Index = this.Lesson.CurrentStageIndex;  // change cover flow image.

            // Set the image for the current stage.
            this.Lesson.IntubationPage.CurrentStageImage.Source = new BitmapImage(new Uri("/Tasks/Intubation/Graphics/Intubation_Stage001_640.jpg", UriKind.Relative));

            // Get the initial color ranges to use.
            try
            {
                this._manikinChinColor = ColorTracker.GetObjectColorRange("manikin_chin").ColorRange;
                this._laryngoscopeColor = ColorTracker.GetObjectColorRange("laryngoscope").ColorRange;
            }
            catch (Exception exc) { Debug.Print("IntubationStage1 SetupStage() ERROR: {0}", exc.Message); }

            this.Zones.Clear(); // Clear the stage zone list.

            // Add the right and left zones (where the hands should be).

            // These pixels are in depth space... but are now color space
            int scale_x = Intubation.SCALE_X;
            int scale_y = Intubation.SCALE_Y;

            // The ManikinFaceLocation must be changed from Point3D to ColorPoint
            CameraSpacePoint cameraSpacePoint = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };
            ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(cameraSpacePoint);

            System.Diagnostics.Debug.WriteLine(String.Format("IntubationStage1 -> using ManikinFaceLocation Pixels ({0},{1},{2}m).", colorSpacePoint.X, colorSpacePoint.Y, cameraSpacePoint.Z));

            // stageZones[0]
            #region Right Wrist Zone
            RecZone rightzonewrist = new RecZone();
            rightzonewrist.X = colorSpacePoint.X + (28.6 - 5.5 * 3) * scale_x;
            rightzonewrist.Y = colorSpacePoint.Y - (21 - 9.8 * 3) * scale_y;
            rightzonewrist.Z = 0;
            rightzonewrist.Name = "R Wrist";
            rightzonewrist.Width = 5.5 * 6 * scale_x;
            rightzonewrist.Height = 9.8 * 6 * scale_y;
            rightzonewrist.Depth = 0;
            rightzonewrist.AcceptedJointType = JointType.WristRight;
            Zones.Add(rightzonewrist);
            #endregion

            // stageZones[1]
            #region Left Wrist Zone
            RecZone leftzonewrist = new RecZone();
            leftzonewrist.X = colorSpacePoint.X - (26.4 * scale_x) - (6.5 * 3 * scale_x);
            leftzonewrist.Y = colorSpacePoint.Y - (15 * scale_y) - (9.9 * 3 * scale_y);
            leftzonewrist.Z = 0;
            leftzonewrist.Name = "L Wrist";
            //leftzonewrist.ZoneType = ZoneType.Position;
            leftzonewrist.Width = 6.5 * 6 * scale_x;
            leftzonewrist.Height = 9.9 * 6 * scale_y;
            leftzonewrist.Depth = 0;
            leftzonewrist.AcceptedJointType = JointType.WristLeft;
            Zones.Add(leftzonewrist);
            #endregion

            // stageZones[2]
            #region Right Hand Zone
            RecZone rightzonehand = new RecZone();
            rightzonehand.X = colorSpacePoint.X + (28.6 * scale_x) - (5.5 * 3 * scale_x);
            rightzonehand.Y = colorSpacePoint.Y - (21 * scale_y) - (9.8 * 3 * scale_y);
            rightzonehand.Z = 0;
            rightzonehand.Name = "R Hand";
            //rightzonehand.ZoneType = ZoneType.Position;
            rightzonehand.Width = 5.5 * 6 * scale_x;
            rightzonehand.Height = 9.8 * 6 * scale_y;
            rightzonehand.Depth = 0;
            rightzonehand.AcceptedJointType = JointType.HandRight;
            Zones.Add(rightzonehand);
            #endregion

            // stageZones[3]
            #region Left Hand Zone
            RecZone leftzonehand = new RecZone();
            leftzonehand.X = colorSpacePoint.X - (26.4 * scale_x) - (6.5 * 3 * scale_x);
            leftzonehand.Y = colorSpacePoint.Y - (15 * scale_y) - (9.9 * 3 * scale_y);
            leftzonehand.Z = 0;
            leftzonehand.Name = "L Hand";
            //leftzonehand.ZoneType = ZoneType.Position;
            leftzonehand.Width = 6.5 * 6 * scale_x;
            leftzonehand.Height = 9.9 * 6 * scale_y;
            leftzonehand.Depth = 0;
            leftzonehand.AcceptedJointType = JointType.HandLeft;
            Zones.Add(leftzonehand);
            #endregion

            // @note is object name correct? looks like reusing one from above outsiderightzonehand
            #region Right Hand Zone
            RecZone outsiderightzonehand = new RecZone();
            //rightzonehand.X = colorSpacePoint.X + (57.2 * scale_x) - (5.5 * 3 * scale_x);
            //rightzonehand.Y = colorSpacePoint.Y - (21 * scale_y) - (9.8 * 3 * scale_y);
            //rightzonehand.Z = 0;
            //rightzonehand.Name = "R Hand";
            ////rightzonehand.ZoneType = ZoneType.Position;
            //rightzonehand.Width = 5.5 * 6 * scale_x;
            //rightzonehand.Height = 9.8 * 6 * scale_y;
            //rightzonehand.Depth = 0;
            //rightzonehand.AcceptedJointType = JointType.HandRight;
            Zones.Add(rightzonehand);
            #endregion

            // stageZones[3]
            // @note is object name correct? looks like reusing one from above outsideleftzonehand
            #region Left Hand Zone
            RecZone outsideleftzonehand = new RecZone();
            //leftzonehand.X = colorSpacePoint.X - (52.4 * scale_x) - (6.5 * 3 * scale_x);
            //leftzonehand.Y = colorSpacePoint.Y - (15 * scale_y) - (9.9 * 3 * scale_y);
            //leftzonehand.Z = 0;
            //leftzonehand.Name = "L Hand";
            ////leftzonehand.ZoneType = ZoneType.Position;
            //leftzonehand.Width = 6.5 * 6 * scale_x;
            //leftzonehand.Height = 9.9 * 6 * scale_y;
            //leftzonehand.Depth = 0;
            //leftzonehand.AcceptedJointType = JointType.HandLeft;
            Zones.Add(leftzonehand);
            #endregion

            this.Lesson.IntubationPage.zoneOverlay.ClearZones();
            // Add the zones into the zoneOverlay
            for (int i = 0; i < Zones.Count - 2; i++)
            {
                this.Lesson.IntubationPage.zoneOverlay.AddZone(Zones[i]);
            }

            this.CurrentStageWatchers = new List<BaseWatcher>();
            this.PeripheralStageWatchers = new List<BaseWatcher>();
            this.OutlyingStageWatchers = new List<BaseWatcher>();

            CreateStageWatchers();

            this.StartTime = DateTime.Now;

            this.IsSetup = true;

            this.Stopwatch.Start();

            this.TimeoutStopwatch.Start();
        }

        private void CreateStageWatchers()
        {
            // Clear the list of watchers.
            this.CurrentStageWatchers.Clear();
            this.PeripheralStageWatchers.Clear();
            this.OutlyingStageWatchers.Clear();

            // Add in new watchers.
            Watcher manikinChinDepthChangeWatcher = new Watcher();

            #region WatcherLogic

            // Checks for the manikin's head to change position.
            manikinChinDepthChangeWatcher.CheckForActionDelegate = () =>
            {
                // @note kept for CurrentStageWatchers index
            };

            #endregion
            // 0
            this.CurrentStageWatchers.Add(manikinChinDepthChangeWatcher);

            Watcher rightHandOrWristByChinWatcher = new Watcher();

            #region WatcherLogic

            // Checks for the right hand or wrist to come into close proximity of the manikin's chin location.
            rightHandOrWristByChinWatcher.CheckForActionDelegate = () =>
            {
                CameraSpacePoint manikinChinInSkeletalSpace;
                CameraSpacePoint rHandLoc;
                CameraSpacePoint rWristLoc;
                double rHandDistance;
                double rWristDistance;

                // Use our function eventually
                manikinChinInSkeletalSpace = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };

                // TREY: REPLACED WITH FUNCTION
                //manikinChinInSkeletalSpace = Watcher.CameraSpaceFromPoint3D(this.Lesson.ManikinFaceLocation);

                SkeletonProfile skele = null;

                if (this.Lesson.StuckSkeletonProfileID.HasValue)
                {
                    // Retreive
                    skele = KinectManager.SingleStickySkeletonProfile;
                }
                if (skele != null)
                {
                    // Get the right hand and right wrist joint positions out of the profile.
                    rHandLoc = skele.JointProfiles.First(o => o.JointType == JointType.HandRight).CurrentPosition;
                    rWristLoc = skele.JointProfiles.First(o => o.JointType == JointType.WristRight).CurrentPosition;

                    // Distance returned by KinectManager.Distance3D( SkeletalPoint a, SkeletalPoint b) is in SkeletalSpace, and therefore is represented in Meters.
                    rHandDistance = System.Math.Abs(KinectManager.Distance3D(manikinChinInSkeletalSpace, rHandLoc));     // Check the distance between the manikinChin in SkeletalSpace and the right hand. Absolute value is used to negate which point is used as the base.
                    rWristDistance = System.Math.Abs(KinectManager.Distance3D(manikinChinInSkeletalSpace, rWristLoc));   // Check the distance between the manikinChin in the SkeletalSpace and the right wrist. Absolute value is used to negate which point is used as the base.

                    //System.Diagnostics.Debug.WriteLine(String.Format("R-Hand to chin:  {0:#}mm", rHandDistance * 1000));
                    //System.Diagnostics.Debug.WriteLine(String.Format("R-Wrist to chin: {0:#}mm @{1}", rWristDistance * 1000, KinectManager.AzimuthAngle(manikinChinInSkeletalSpace, rWristLoc)));

                    double desiredMaxDistance = 0.300;  // 300 milimeters

                    // Check if the right hand or wrist is found near the chin of the manikin.
                    if (rHandDistance <= desiredMaxDistance || rWristDistance <= desiredMaxDistance)
                    {
                        // If it is within the disiredMaxDistance, flip our trigger switch on.
                        rightHandOrWristByChinWatcher.IsTriggered = true;
                    }
                    else
                    {
                        // If it's outside the desiredMaxDistance, then flip our trigger switch off.
                        rightHandOrWristByChinWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    // If no skeleton was found, then the hand/wrist can't be near the face..
                    rightHandOrWristByChinWatcher.IsTriggered = false;
                }
            };

            #endregion
            // 1
            this.CurrentStageWatchers.Add(rightHandOrWristByChinWatcher);

            Watcher leftHandOrWristByChinWatcher = new Watcher();

            #region WatcherLogic

            // Checks for the left hand or wrist to come into close proximity of the manikin's chin location.
            leftHandOrWristByChinWatcher.CheckForActionDelegate = () =>
            {
                CameraSpacePoint manikinChinInSkeletalSpace;
                CameraSpacePoint lHandLoc;
                CameraSpacePoint lWristLoc;
                double lHandDistance;
                double lWristDistance;


                manikinChinInSkeletalSpace = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };

                // TREY: REPLACED WITH FUNCTION
                //manikinChinInSkeletalSpace = Watcher.CameraSpaceFromPoint3D(this.Lesson.ManikinFaceLocation);

                SkeletonProfile skele = null;

                if (this.Lesson.StuckSkeletonProfileID.HasValue)
                {
                    // Retreive
                    skele = KinectManager.SingleStickySkeletonProfile;
                }
                if (skele != null)
                {
                    // Get the left hand and left wrist joint positions out of the profile.
                    lHandLoc = skele.JointProfiles.First(o => o.JointType == JointType.HandLeft).CurrentPosition;
                    lWristLoc = skele.JointProfiles.First(o => o.JointType == JointType.WristLeft).CurrentPosition;

                    // Distance returned by KinectManager.Distance3D( SkeletalPoint a, SkeletalPoint b) is in SkeletalSpace, and therefore is represented in Meters.
                    lHandDistance = System.Math.Abs(KinectManager.Distance3D(manikinChinInSkeletalSpace, lHandLoc));     // Check the distance between the manikinChin in SkeletalSpace and the left hand. Absolute value is used to negate which point is used as the base.
                    lWristDistance = System.Math.Abs(KinectManager.Distance3D(manikinChinInSkeletalSpace, lWristLoc));   // Check the distance between the manikinChin in the SkeletalSpace and the left wrist. Absolute value is used to negate which point is used as the base.

                    //System.Diagnostics.Debug.WriteLine(String.Format("L-Hand to chin:  {0:#}mm", lHandDistance * 1000));
                    //System.Diagnostics.Debug.WriteLine(String.Format("L-Wrist to chin: {0:#}mm @{1}", lWristDistance * 1000, KinectManager.AzimuthAngle(manikinChinInSkeletalSpace, lWristLoc)));

                    double desiredMaxDistance = 0.300;  // 300 milimeters

                    // Check if the left hand or wrist is found near the chin of the manikin.
                    if (lHandDistance <= desiredMaxDistance || lWristDistance <= desiredMaxDistance)
                    {
                        // If it is within the disiredMaxDistance, flip our trigger switch on.
                        leftHandOrWristByChinWatcher.IsTriggered = true;
                    }
                    else
                    {
                        // If it's outside the desiredMaxDistance, then flip our trigger switch off.
                        leftHandOrWristByChinWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    // If no skeleton was found, then the hand/wrist can't be near the face..
                    leftHandOrWristByChinWatcher.IsTriggered = false;
                }
            };

            #endregion
            // 2
            this.CurrentStageWatchers.Add(leftHandOrWristByChinWatcher);

            // Add in new watchers.
            Watcher manikinChinPositionChangeWatcher = new Watcher();

            #region WatcherLogic

            // Checks for the manikin's head to change position.
            manikinChinPositionChangeWatcher.CheckForActionDelegate = () =>
            {
                CameraSpacePoint originalLocation;
                CameraSpacePoint currentLocation;
                double displacement = 0;

                // originalLocation = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };

                // TREY: REPLACED WITH FUNCTION
                originalLocation = Watcher.CameraSpaceFromPoint3D(this.Lesson.ManikinFaceLocation);

                if (!manikinHeadMoved)
                {
                    System.Diagnostics.Debug.WriteLine("IntubationStage1-> check for manikin head moved around camera space point " + originalLocation.X + "," + originalLocation.Y + "," + originalLocation.Z + "m");

                    ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(originalLocation);
                    System.Diagnostics.Debug.WriteLine("IntubationStage1-> check for manikin head moved around color space point " + colorSpacePoint.X + "," + colorSpacePoint.Y);

                    // Check around for the chin.
                    Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, -10, -10, 20, 20);

                    // TREY: DID NOT REPLACE
                    System.Windows.Point p = AIMS.Assets.Lessons2.ColorTracker.find(this._manikinChinColor,
                                                                                        rect,
                                                                                        KinectManager.ColorData);

                    if (!Double.IsNaN(p.X) && !Double.IsNaN(p.Y))
                    {
                        int depth = KinectManager.GetDepthValueForColorPoint(p);
                        System.Diagnostics.Debug.WriteLine("IntubationStage1-> compare to point " + p.X + "," + p.Y + "," + depth + "mm");


                        if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth)
                        {
                            watcher_FoundManikinHeadPos.Z += ((double)depth);
                            watcher_YellowDepthCounter++;
                        }
                        else
                        {
                            watcher_FoundManikinHeadPos.Z = 0;
                            watcher_YellowDepthCounter = 0;
                        }

                        Point3D tapePoint = KinectManager.Get3DPointForColorPoint(p, depth);
                        watcher_FoundManikinHeadPos.X += tapePoint.X;
                        watcher_FoundManikinHeadPos.Y += tapePoint.Y;

                        watcher_YellowCounter++;
                    }
                    else
                    {
                        // Color wasn't found this time, reset X, Y, Z, and both counters.
                        watcher_YellowCounter = 0;
                        watcher_FoundManikinHeadPos.X = 0;
                        watcher_FoundManikinHeadPos.Y = 0;

                        watcher_YellowDepthCounter = 0;
                        watcher_FoundManikinHeadPos.Z = 0;

                    }

                    if (watcher_YellowCounter > 1 && watcher_YellowDepthCounter > 1)
                    {
                        Point3D temp = new Point3D();
                        // Get the average of the found x's.
                        temp.X = watcher_FoundManikinHeadPos.X / watcher_YellowCounter;
                        // Get the average of the found y's.
                        temp.Y = watcher_FoundManikinHeadPos.Y / watcher_YellowCounter;
                        // Get the average of the found z's.
                        temp.Z = watcher_FoundManikinHeadPos.Z / watcher_YellowDepthCounter;

                        watcher_FoundManikinHeadPos = temp;

                        //System.Diagnostics.Debug.WriteLine(String.Format("ManikinRefoundAt: ({0},{1},{2}).", watcher_FoundManikinHeadPos.X, watcher_FoundManikinHeadPos.Y, watcher_FoundManikinHeadPos.Z));

                        // Set the manikinFaceLocation to the newly found point.
                        watcher_FoundChin = true;
                        //MessageBox.Show(String.Format("Depth of chin is {0}mm from the Kinect sensor.", newHeadLocation.Z));




                        //currentLocation = new CameraSpacePoint() { X = (float)watcher_FoundManikinHeadPos.X, Y = (float)watcher_FoundManikinHeadPos.Y, Z = (float) watcher_FoundManikinHeadPos.Z / 1000 };

                        // TREY: REPLACED WITH  FUNCTION
                        currentLocation = Watcher.CameraSpaceFromPoint3D(watcher_FoundManikinHeadPos);

                        /*
                        // Check for a change in Depth.
                        double deltaDepth = System.Math.Abs(watcher_FoundManikinHeadPos.Z - this.Lesson.ManikinFaceLocation.Z);

                        System.Diagnostics.Debug.WriteLine(String.Format("Change in depth is: {0}mm", deltaDepth));

                        if (deltaDepth > 14 && deltaDepth < 300)*/

                        // trey
                        // displacement is in METERS
                        displacement = KinectManager.Distance3D(originalLocation, currentLocation);

                        System.Diagnostics.Debug.WriteLine(String.Format("IntubationStage1 -> Displacement is: {0:#}mm", displacement * 1000));

                        // If we're over 21mm and under a full meter
                        if (displacement > 0.021 && displacement < 1.000)
                        {
                            manikinChinPositionChangeWatcher.IsTriggered = true;
                        }
                        else
                        {
                            manikinChinPositionChangeWatcher.IsTriggered = false;
                            watcher_FoundManikinHeadPos = new Point3D();
                            watcher_YellowCounter = 0;
                            watcher_YellowDepthCounter = 0;
                            watcher_FoundChin = false;
                        }
                    }
                }
            };

            #endregion
            // 3
            this.CurrentStageWatchers.Add(manikinChinPositionChangeWatcher);

            Watcher laryngoscopeDetectionWatcher = new Watcher();

            #region WatcherLogic

            laryngoscopeDetectionWatcher.CheckForActionDelegate = () =>
            {
                CameraSpacePoint cameraSpacePoint = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };
                ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(cameraSpacePoint);

                // Scale Factors
                int scale_x = Intubation.SCALE_X;
                int scale_y = Intubation.SCALE_Y;

                // @todo consider scale of area; consider mappedcolordata as the depth adjusted color values

                // TREY: CHANGED FOR FUNCTION
                //Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._laryngoscopeColor,
                //                                                        new Int32Rect(  (int)System.Math.Max(0, colorSpacePoint.X - 50 * scale_x),
                //                                                                        (int)System.Math.Max(0, colorSpacePoint.Y - 80 * scale_y),
                //                                                                        40 * scale_x,
                //                                                                        80 * scale_y), 
                //                                                        KinectManager.ColorData);

                // TREY: STATING  PARAMS  EXPLICITLY
                int x = -50;
                int y = -80;
                int width = 40;
                int height = 80;
                Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, x, y, width, height);

                Point found = AIMS.Assets.Lessons2.ColorTracker.find(this._laryngoscopeColor,
                                                                        rect,
                                                                        KinectManager.ColorData);

                if (!double.IsNaN(found.X) && !double.IsNaN(found.Y))
                {
                    int depth = KinectManager.GetDepthValueForColorPoint(found);
                    if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth)
                    {
                        // convert color point to camera space point
                        Point3D loc = KinectManager.Get3DPointForColorPoint(found, depth);

                        this.oldLaryngoscopePosition = this.Lesson.LaryngoscopeLocation;

                        this.Lesson.LaryngoscopeLocation = loc;

                        laryngoscopeDetectionWatcher.IsTriggered = true;

                        System.Diagnostics.Debug.WriteLine("IntubationStage1-> found laryngoscope: " + loc.X + ", " + loc.Y + ", " + loc.Z + "mm");
                    }
                    else
                    {
                        laryngoscopeDetectionWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    laryngoscopeDetectionWatcher.IsTriggered = false;
                }
            };

            #endregion
            // 4
            this.CurrentStageWatchers.Add(laryngoscopeDetectionWatcher);

            Watcher laryngoscopeMotionWatcher = new Watcher();

            #region WatcherLogic

            laryngoscopeMotionWatcher.CheckForActionDelegate = () =>
            {
                if (laryngoscopeDetectionWatcher.IsTriggered)
                {
                    if (oldLaryngoscopePosition.X == 0)
                        return; // ensures that we have at least two points to compare displacement for. (starting position is 0,0,0 - which will fail out with this check in place).

                    CameraSpacePoint oldLoc = new CameraSpacePoint() { X = (float)oldLaryngoscopePosition.X, Y = (float)oldLaryngoscopePosition.Y, Z = (float)oldLaryngoscopePosition.Z / 1000 };
                    CameraSpacePoint newLoc = new CameraSpacePoint() { X = (float)this.Lesson.LaryngoscopeLocation.X, Y = (float)this.Lesson.LaryngoscopeLocation.Y, Z = (float)this.Lesson.LaryngoscopeLocation.Z / 1000 };

                    double toolDisplacement = KinectManager.Distance2D(oldLoc, newLoc);

                    if (toolDisplacement > 0.05)
                    {
                        laryngoscopeMotionWatcher.IsTriggered = true;

                        System.Diagnostics.Debug.WriteLine(String.Format("Tool Displacement is: {0}px. Movement Detected.", toolDisplacement));
                    }
                    else
                    {
                        laryngoscopeMotionWatcher.IsTriggered = false;

                        System.Diagnostics.Debug.WriteLine(String.Format("Tool Displacement is: {0}px.", toolDisplacement));
                    }
                }
                else
                {
                    laryngoscopeMotionWatcher.IsTriggered = false;
                }
            };

            #endregion
            // 5
            this.CurrentStageWatchers.Add(laryngoscopeMotionWatcher);
        }

        public override void UpdateStage()
        {
            if (!this.IsSetup)
                SetupStage();

            if (KinectManager.ClosestSkeletonProfile == null)
                return;

            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
                this.TimeoutStage();

            #region Update each zone's joint state.
            foreach (Zone zone in this.Zones)
            {
                zone.UpdateJointWithinZone();
            }
            #endregion

            if (this.CurrentStageWatchers[3].IsTriggered)
            {
                if (this.watcher_FoundManikinHeadPos.X != 0 && this.watcher_FoundManikinHeadPos.Y != 0 && this.watcher_FoundManikinHeadPos.Z != 0)
                {
                    // trey
                    // what is the point of newheadlocation?
                    this.NewHeadLocation = this.watcher_FoundManikinHeadPos;
                    this.Lesson.ManikinFaceLocation = this.NewHeadLocation;

                    // Chin was found with a change in depth within 8-300mm.
                    if (!this.WasHeadTiltedBack)
                    {
                        this.WasHeadTiltedBack = true;
                        System.Diagnostics.Debug.WriteLine("IntubationStage1 -> CW3 Manikin Head Tilted Back.");
                        this.Lesson.DetectableActions.DetectAction("Tilted Back", "Tilt Back", "Manikin Chin", true);   // Detect the Action
                    }
                }
                else
                {
                    this.watcher_YellowCounter = 0;
                    this.watcher_YellowDepthCounter = 0;
                }
            }

            if (this.CurrentStageWatchers[1].IsTriggered)
            {
                // The right hand is within close proximity to the manikin's chin location.
                if (!this.RightHandOrWristByManikinsChin)
                {
                    this.RightHandOrWristByManikinsChin = true;
                    System.Diagnostics.Debug.WriteLine("IntubationStage1 -> CW1 Right Hand/Wrist in Proximity to Manikin's Chin.");
                }
            }
            else
            {
                if (this.RightHandOrWristByManikinsChin)
                {
                    this.RightHandOrWristByManikinsChin = false;
                    System.Diagnostics.Debug.WriteLine("Right Hand/Wrist out of Proximity to Manikin's Chin.");
                }
            }

            if (this.CurrentStageWatchers[2].IsTriggered)
            {
                // The left hand is within close proximity to the manikin's chin location.
                if (!this.LeftHandOrWristByManikinsChin)
                {
                    this.LeftHandOrWristByManikinsChin = true;
                    System.Diagnostics.Debug.WriteLine("IntubationStage1 -> CW2 Left Hand/Wrist in Proximity to Manikin's Chin.");
                }
            }
            else
            {
                if (this.LeftHandOrWristByManikinsChin)
                {
                    this.LeftHandOrWristByManikinsChin = false;
                    System.Diagnostics.Debug.WriteLine("Left Hand/Wrist out of Proximity to Manikin's Chin.");
                }
            }

            if (WasHeadTiltedBack && (RightHandOrWristByManikinsChin || LeftHandOrWristByManikinsChin))
            {
                if (RightHandOrWristByManikinsChin && !LeftHandOrWristByManikinsChin)
                {
                    this.Lesson.DetectableActions.DetectAction("Right Handed Head Tilt", "Tilt Head", "User", true);   // Detect the Action
                }
                else if (!RightHandOrWristByManikinsChin && LeftHandOrWristByManikinsChin)
                {
                    this.Lesson.DetectableActions.DetectAction("Left Handed Head Tilt", "Tilt Head", "User", true);   // Detect the Action
                }
                else if (RightHandOrWristByManikinsChin && LeftHandOrWristByManikinsChin)
                {
                    this.Lesson.DetectableActions.DetectAction("Two Handed Head Tilt", "Tilt Head", "User", true);   // Detect the Action
                }

                this.Complete = true;
                EndStage();
            }
            else if (this.CurrentStageWatchers[5].IsTriggered)
            {
                this.Lesson.DetectableActions.DetectAction("No Head Tilt", "Tilt Head", "User", true);   // Detect the Action

                this.Complete = true;
                EndStage();
            }
        }

        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }

        public override void EndStage(bool playSound = true)
        {
            this.Stopwatch.Stop();

            this.TimeoutStopwatch.Stop();

            // Get the current time to signify the time the stage ended.
            EndTime = DateTime.Now;

            // Create a screenshot for the current stage.
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                Intubation lesson = (Intubation)args.Argument;

                lesson.TiltHeadImage = KinectImage.TakeKinectSnapshot(true, true);
            };

            worker.RunWorkerAsync(this.Lesson);

            CalculateTiltHeadScore();

            this.Lesson.CurrentStageIndex++;

            if (playSound) Lesson.PlayNextStageSound();
        }

        public void CalculateTiltHeadScore()
        {
            double score = 0;

            if (WasHeadTiltedBack)
            {
                score = 1;
                this.Lesson.TiltHeadFeedbackIDs.Add(22);    // You have tilted the patient's head into sniffing position.
            }
            else
            {
                this.Lesson.TiltHeadFeedbackIDs.Add(23);    // You did not tilt the patient's head back into sniffing position.
            }

            this.Lesson.TiltHeadScore = System.Math.Max(0, System.Math.Min(score, 1)); // clamp the score between 0 and 1, and then set it.
        }

        public override void PauseStage()
        {
            this.Lesson.Paused = true;
            this.Lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.Lesson.Paused = false;
            this.Lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void ResetStage()
        {

        }

        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            //this.PauseStage();
            this.TimedOut = true;
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            this.RequestStageVideo();   // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
        }

        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.Lesson.IntubationPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Intubation/Videos/Stage 001.wmv", UriKind.Relative));
            this.Lesson.IntubationPage.VideoPlayer.startVideo(this, new RoutedEventArgs());

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        public override void WipeClean()
        {
            wasHeadTiltedBack = false;
            rightHandOrWristByManikinsChin = false;
            leftHandOrWristByManikinsChin = false;
            newHeadLocation = new Point3D();

            watcher_FoundManikinHeadPos = new Point3D();
            watcher_YellowCounter = 0;
            watcher_YellowDepthCounter = 0;
            watcher_FoundChin = false;

            manikinHeadMoved = false;

            laryngoscopeMoving = false;
            oldLaryngoscopePosition = new Point3D();

            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;

            this.IsSetup = false;

            if (this.StageScore != null)
            {
                this.StageScore.WipeClean();
                this.StageScore = null;
                this.StageScore = new StageScore();
            }
            else
            {
                this.StageScore = new StageScore();
            }

            this.Active = false;
            this.Complete = false;

            if (this.Zones != null)
            {
                this.Zones.Clear();
            }
            else
            {
                this.Zones = new List<Zone>();
            }

            if (this.Stopwatch != null)
            {
                this.Stopwatch.Reset();
            }
            else
            {
                this.Stopwatch = new System.Diagnostics.Stopwatch();
            }

            if (this.TimeoutStopwatch != null)
                this.TimeoutStopwatch.Reset();
            else this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();

            this.TimedOut = false;
        }
    }
}
