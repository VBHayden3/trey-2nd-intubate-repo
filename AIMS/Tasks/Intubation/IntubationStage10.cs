﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;
using System.Windows.Media.Media3D;
using Microsoft.Kinect;
using System.ComponentModel;


namespace AIMS.Tasks.Intubation
{

    class IntubationStage10 : Stage
    {
        private Intubation lesson;
        public Intubation Lesson { get { return this.lesson; } set { this.lesson = value; } }

        // Current position of color marker on the manikin chin in Skeleton Space
        private CameraSpacePoint manikinChinInSkeletalSpace;

        // Found a Skeleton Hand in Proximity of Manikin Head
        private bool foundHands = false;

        // Hand Detection Timer
        private System.Diagnostics.Stopwatch handTimeStopwatch = new System.Diagnostics.Stopwatch();

        // Required minimum Time (ms) a hand needs to be observed in range
        private const long handTime = 4000;


        public IntubationStage10(Intubation lesson)
        {
            this.lesson = lesson;
            this.StageNumber = 10;
            this.Name = "Attach Valve Bag";
            this.Instruction = "Attach the Valve Bag to the end of the Endotracheal Tube.";
            this.StageScore = new StageScore();
            this.ImagePath = "/Tasks/Intubation/Graphics/Intubation010.png";
            this.Active = false;
            this.Complete = false;
            this.ScoreWeight = 0.05;
            this.Zones = new List<Zone>();
            this.IsSetup = false;
            this.TargetTime = TimeSpan.FromSeconds(10.0);
            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.Stopwatch = new System.Diagnostics.Stopwatch();
            this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.TimeOutTime = TimeSpan.FromSeconds(20.0);
            this.CanTimeOut = false;
        }

        public override void SetupStage()
        {
            this.Lesson.IntubationPage.StageInfoTextBlock.Text = this.Name;
            this.Lesson.IntubationPage.InstructionTextBlock.Text = this.Instruction;

            this.Lesson.IntubationPage.flow.Index = this.Lesson.CurrentStageIndex;  // change cover flow image.

            // Set the image for the current stage.
            this.Lesson.IntubationPage.CurrentStageImage.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri("/Tasks/Intubation/Graphics/Intubation010.png", UriKind.Relative));

            this.Zones.Clear(); // Clear the stage zone list.

            this.Lesson.IntubationPage.zoneOverlay.ClearZones();

            // Add the zones into the zoneOverlay
            foreach (Zone zone in this.Zones)
            {
                this.Lesson.IntubationPage.zoneOverlay.AddZone(zone);
            }

            this.manikinChinInSkeletalSpace = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };

            this.CurrentStageWatchers = new List<BaseWatcher>();
            this.PeripheralStageWatchers = new List<BaseWatcher>();
            this.OutlyingStageWatchers = new List<BaseWatcher>();

            this.CreateStageWatchers();

            this.StartTime = DateTime.Now;

            this.IsSetup = true;

            this.Stopwatch.Start();

            this.TimeoutStopwatch.Start();
        }

        private void CreateStageWatchers()
        {
            // Clear the list of watchers.
            this.CurrentStageWatchers.Clear();
            this.PeripheralStageWatchers.Clear();
            this.OutlyingStageWatchers.Clear();

            // Add new Watcher - Checks for User Hands to be in close proximity of the Manikin Head
            Watcher handsByManikinHead = new Watcher();

            #region User HANDS are near the Manikin Head Watcher Logic
            handsByManikinHead.CheckForActionDelegate = () =>
            {
                if (this.IsHandInRangeToChin("Left", true) || this.IsHandInRangeToChin("Right", true))
                {
                    if (this.handTimeStopwatch.IsRunning)
                    {
                        if (this.handTimeStopwatch.ElapsedMilliseconds >= IntubationStage10.handTime)
                        {
                            System.Diagnostics.Debug.WriteLine("IntubationStage10::Found a Hand in Range at Elapsed Time = " + this.handTimeStopwatch.ElapsedMilliseconds);

                            this.foundHands = true;
                            handsByManikinHead.IsTriggered = true;
                            this.handTimeStopwatch.Stop();
                        }
                    }
                    else
                    {
                        this.handTimeStopwatch.Start();
                    }
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("IntubationStage10::Hands not Found");

                    if (this.handTimeStopwatch.IsRunning)
                        this.handTimeStopwatch.Reset();

                    handsByManikinHead.IsTriggered = false;
                }
            };
            #endregion

            this.CurrentStageWatchers.Add(handsByManikinHead);

        } // End CreateStageWatchers()


        #region Hand Distance Check
        private bool IsHandInRangeToChin(string handType, bool useDepth)
        {
            CameraSpacePoint handLoc;
            double handDistance;

            SkeletonProfile skele = null;

            if (this.Lesson.StuckSkeletonProfileID.HasValue)
                skele = KinectManager.SingleStickySkeletonProfile;

            if (skele != null)
            {
                // Get the correct hand joint position out of the profile.
                if (handType == "Right")
                    handLoc = skele.JointProfiles.First(o => o.JointType == JointType.HandRight).CurrentPosition;
                else
                    handLoc = skele.JointProfiles.First(o => o.JointType == JointType.HandLeft).CurrentPosition;

                if (useDepth)
                {
                    handDistance = System.Math.Abs(KinectManager.Distance3D(this.manikinChinInSkeletalSpace, handLoc));

                    double desiredMaxDistance = 0.515;  // meters

                    bool isHandOutOfRange = false;

                    if (handDistance >= desiredMaxDistance)
                    {
                        // Distance between the user's hand and the manikin's chin is outside the desiredMaxDistance.
                        isHandOutOfRange = true;
                    }

                    if (isHandOutOfRange)
                    {
                        //System.Diagnostics.Debug.WriteLine("Value Proximity Breached -> Attempting 2D comparison...");
                        return IsHandInRangeToChin(handType, false);
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    handDistance = System.Math.Abs(KinectManager.Distance2D(this.manikinChinInSkeletalSpace, handLoc));

                    //System.Diagnostics.Debug.WriteLine(String.Format("2D: ManikinChin -> Right Hand Distance: {0:#}pixels", rightHandDistance * 1000));

                    if (handDistance < 0.4)
                        return true;
                }
            }

            return false;
        }
        #endregion


        public override void UpdateStage()
        {
            if (!this.IsSetup)
                this.SetupStage();

            // Check the Stage Timeout
            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
                this.TimeoutStage();

            #region User HANDS are near the Manikin Head
            if (this.foundHands)
            {
                System.Diagnostics.Debug.WriteLine("IntubationStage10::Found Hands");

                this.Lesson.DetectableActions.DetectAction("Tube Bagged", "Bag Tube", "User", true);
                this.Complete = true;
                this.EndStage();
            }
            #endregion

        } //End UpdateStage()


        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }



        public override void EndStage( bool playSound = true )
        {
            this.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            // Get the current time to signify the time the stage ended.
            this.EndTime = DateTime.Now;

            // Create a screenshot for the current stage.
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                Intubation lesson = (Intubation)args.Argument;

                KinectImage image = KinectImage.TakeKinectSnapshot(true, true);

                lesson.BaggingImage = image;
            };

            worker.RunWorkerAsync(this.Lesson);

            AssessBagging();

            this.Lesson.CurrentStageIndex++;

            if( playSound ) Lesson.PlayNextStageSound();
        }

        private void AssessBagging()
        {
            double score = 1;

            if (this.foundHands)
            {
                this.Lesson.BaggingFeedbackIDs.Add(57);  // The Valve Bag was attached.
            }
            else
            {
                score -= 1;
                this.Lesson.BaggingFeedbackIDs.Add(58);  // The Valve Bag was not attached.
            }

            this.Lesson.BaggingScore = System.Math.Max(0, System.Math.Min(score, 1)); // Clamp the score between 0 and 1.
        }

        public override void PauseStage()
        {
            this.Lesson.Paused = true;
            this.Lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.Lesson.Paused = false;
            this.Lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void ResetStage()
        {

        }

        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            this.TimedOut = true;
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            this.RequestStageVideo();       // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
        }


        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.Lesson.IntubationPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Intubation/Videos/Stage 010.wmv", UriKind.Relative));
            this.Lesson.IntubationPage.VideoPlayer.startVideo(this, new System.Windows.RoutedEventArgs());

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }


        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }


        public override void WipeClean()
        {
            // Stage Reinitialization

            // Look for Skeleton Hands in Proximity of Manikin Head
            this.foundHands = false;

            if (this.handTimeStopwatch != null)
                this.handTimeStopwatch.Reset();
            else
                this.handTimeStopwatch = new System.Diagnostics.Stopwatch();

            manikinChinInSkeletalSpace = new CameraSpacePoint();

            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.IsSetup = false;

            if (this.StageScore != null)
            {
                this.StageScore.WipeClean();
                this.StageScore = null;
                this.StageScore = new StageScore();
            }
            else
            {
                this.StageScore = new StageScore();
            }

            this.Active = false;
            this.Complete = false;

            if (this.Zones != null)
            {
                this.Zones.Clear();
            }
            else
            {
                this.Zones = new List<Zone>();
            }

            if (this.Stopwatch != null)
            {
                this.Stopwatch.Reset();
            }
            else
            {
                this.Stopwatch = new System.Diagnostics.Stopwatch();
            }

            if (this.TimeoutStopwatch != null)
            {
                this.TimeoutStopwatch.Reset();
            }
            else
            {
                this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            }

            this.TimedOut = false;
        }


    } //End class IntubationStage10


} //End namespace AIMS.Tasks.Intubation
