﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;
using System.Windows.Media.Media3D;
using System.ComponentModel;
using System.Diagnostics;

using Microsoft.Kinect;

using AIMS.Assets.Lessons2;

namespace AIMS.Tasks.Intubation
{

    /// <summary>
    /// A struct for storing data associated with a stage detection (e.g. Chest or Stomach Rise)
    /// </summary>
    public struct Detection
    {
        public double distance;
        public long time;

        public Detection(long t, double dist)
        {
            time = t;
            distance = dist;
        }
    }


    class IntubationStage11 : Stage
    {
        private Intubation lesson;
        public Intubation Lesson { get { return this.lesson; } set { this.lesson = value; } }

        // Chest & Stomach Counts
        private int yellowLeftCounter = 0;
        private int yellowRightCounter = 0;
        private int redCounter = 0;

        // Baseline color markers on the manikin chest and stomach found
        private bool foundChestBaselineLeftColor = false;
        private bool foundChestBaselineRightColor = false;
        private bool foundStomachBaselineColor = false;

        // Baseline color markers on the manikin chest and stomach in X,Y coordinates
        private Point chestBaselineLeftPt = new Point();
        private Point chestBaselineRightPt = new Point();
        private Point stomachBaselinePt = new Point();

        // Computed from baseline color tracker find() Rectangles for the manikin chest and stomach
        private Int32Rect chestLeftRect;
        private Int32Rect chestRightRect;
        private Int32Rect stomachRect;

        // Current color markers on the manikin chest and stomach found
        private bool foundChestCurrentLeftColor = false;
        private bool foundChestCurrentRightColor = false;
        private bool foundStomachCurrentColor = false;

        // Current color markers on the manikin chest and stomach in X,Y coordinates
        private Point chestCurrentLeftPt;
        private Point chestCurrentRightPt;
        private Point stomachCurrentPt;

        // Look for Skeleton Hands in Proximity of Manikin Head
        private bool foundHands = false;

        // Current position of color marker on the manikin chin in Skeleton Space
        private CameraSpacePoint manikinChinInSkeletalSpace;

        // Save Chest & Stomach Rise Detections
        private List<Detection> chestRiseLeft = new List<Detection>();
        private List<Detection> chestRiseRight = new List<Detection>();
        private List<Detection> stomachRise = new List<Detection>();
        
        // Distance required for chest to rise (Y Axis Pixels)
        private const double chestReqPtDistance = 3;
        // Distance required for stomach to rise
        private const double stomachReqPtDistance = 3;

        // Required minimum Delta Time (ms) between rise observations
        private const long deltaTime = 4000;

        // Baseline color marker sample count for manikin chest and stomach
        private const int baselineCount = 3;
        //private const int baselineCount = 15;  // TREY: DEBUG USING 5 SAMPLES INSTEAD

        // TREY VALUES:
        //
        // We're looking for 3 points that are somewhat related.  Use a common offset:
        private const int offsetX = 0;
        private const int offsetY = 0;

        // Colors to look for
        private ColorRange _manikinChestColor;
        private ColorRange _manikinStomachColor;

        public IntubationStage11(Intubation lesson)
        {
            this.lesson = lesson;
            this.StageNumber = 11;
            this.Name = "Compress Valve Bag";
            this.Instruction = "Squeeze the bag at a constant rate of 1 compression every 6 seconds for a duration of 30 seconds. Observe the chest rising.";
            this.StageScore = new StageScore();
            this.ImagePath = "/Tasks/Intubation/Graphics/Intubation011.png";
            this.Active = false;
            this.Complete = false;
            this.ScoreWeight = 0.05;
            this.Zones = new List<Zone>();
            this.IsSetup = false;
            this.TargetTime = TimeSpan.FromSeconds(10.0);
            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.Stopwatch = new System.Diagnostics.Stopwatch();
            this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.TimeOutTime = TimeSpan.FromSeconds(30.0);
            this.CanTimeOut = true;
        }

        public override void SetupStage()
        {
            this.Lesson.IntubationPage.StageInfoTextBlock.Text = this.Name;
            this.Lesson.IntubationPage.InstructionTextBlock.Text = this.Instruction;

            this.Lesson.IntubationPage.flow.Index = this.Lesson.CurrentStageIndex;  // change cover flow image.

            // Set the image for the current stage.
            this.Lesson.IntubationPage.CurrentStageImage.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri("/Tasks/Intubation/Graphics/Intubation011.png", UriKind.Relative));

            try
            {
                this._manikinChestColor = ColorTracker.GetObjectColorRange("manikin_chest").ColorRange;
                this._manikinStomachColor = ColorTracker.GetObjectColorRange("manikin_stomach").ColorRange;
            }
            catch (Exception exc) { Debug.Print("IntubationStage11 SetupStage() ERROR: {0}", exc.Message); }

            this.Zones.Clear(); // Clear the stage zone list.

            this.Lesson.IntubationPage.zoneOverlay.ClearZones();

            // Add the zones into the zoneOverlay
            foreach (Zone zone in this.Zones)
            {
                this.Lesson.IntubationPage.zoneOverlay.AddZone(zone);
            }

            this.manikinChinInSkeletalSpace = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };

            this.CurrentStageWatchers = new List<BaseWatcher>();
            this.PeripheralStageWatchers = new List<BaseWatcher>();
            this.OutlyingStageWatchers = new List<BaseWatcher>();

            this.CreateStageWatchers();

            this.StartTime = DateTime.Now;

            this.IsSetup = true;

            this.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
        }


        private void CreateStageWatchers()
        {
            // Clear the list of watchers.
            this.CurrentStageWatchers.Clear();
            this.PeripheralStageWatchers.Clear();
            this.OutlyingStageWatchers.Clear();

            #region BASELINE
            // Add new Watcher - Find BASELINE Color Marker Position on LEFT Manikin Chest
            Watcher foundChestBaselineLeftColorWatcher = new Watcher();

            #region Find BASELINE Color Marker Position on LEFT Manikin Chest Watcher Logic
            foundChestBaselineLeftColorWatcher.CheckForActionDelegate = () =>
            {
                if (!this.foundChestBaselineLeftColor)
                {
                    // TREY: USING FUNCTIONS
                    ColorSpacePoint colorSpacePoint = Watcher.ColorSpaceFromPoint3D(this.Lesson.ManikinFaceLocation);
                    //Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, 30, 30, 120, 120);
                    // TREY DEBUG
                    Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, 5 + offsetX, 5 + offsetY, 20, 40);

                    Point point = AIMS.Assets.Lessons2.ColorTracker.find(   this._manikinChestColor,
                                                                            rect, 
                                                                            KinectManager.ColorData);

//                    CameraSpacePoint cameraSpacePoint = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };
//                    ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(cameraSpacePoint);
////                    Point point = AIMS.Assets.Lessons2.ColorTracker.find(this._manikinChestColor, new Int32Rect((int)colorSpacePoint.X, (int)colorSpacePoint.Y + 30, 40, 40), KinectManager.ColorData);
//                    Point point = AIMS.Assets.Lessons2.ColorTracker.find(   this._manikinChestColor, 
//                                                                            new Int32Rect(  (int)colorSpacePoint.X + 30, 
//                                                                                            (int)colorSpacePoint.Y + 30, 
//                                                                                            120, 
//                                                                                            120), KinectManager.ColorData);

                    if (!Double.IsNaN(point.X) && !Double.IsNaN(point.Y))
                    {
                        // TREY: MOVED ADDITIONS TO IF STATEMENT BELOW
                        this.yellowLeftCounter++;

                        // TREY: MOVED ADDITIONS TO HERE
                        this.chestBaselineLeftPt.X += point.X;
                        this.chestBaselineLeftPt.Y += point.Y;

                        // DEBUG
                        this.Lesson.IntubationPage.FOUND_LEFT_CHEST.Content = "L.CH.:" + this.yellowLeftCounter.ToString();

                        if (this.yellowLeftCounter >= IntubationStage11.baselineCount)
                        {
                            // Get the average of the found x's and y's
                            this.chestBaselineLeftPt.X /= this.yellowLeftCounter;
                            this.chestBaselineLeftPt.Y /= this.yellowLeftCounter;

                            // WORKAROUND
                            this.chestLeftRect = new Int32Rect( (int)System.Math.Round(this.chestBaselineLeftPt.X) - 15 * 3, 
                                                                (int)System.Math.Round(this.chestBaselineLeftPt.Y) - 15 * 2, 
                                                                30 * 3, 
                                                                30 * 2);

                            this.foundChestBaselineLeftColor = true;
                            foundChestBaselineLeftColorWatcher.IsTriggered = true;

                            System.Diagnostics.Debug.WriteLine("IntubationStage11::Baseline Left Yellow Point: X = " + this.chestBaselineLeftPt.X + ", Y = " + this.chestBaselineLeftPt.Y);

                        }
                    }
                    else
                    {
                        // Color not found this frame, reset point X, Y, and counter
                        foundChestBaselineLeftColorWatcher.IsTriggered = false;
                        this.yellowLeftCounter = 0;
                        this.chestBaselineLeftPt.X = 0;
                        this.chestBaselineLeftPt.Y = 0;
                    }
                }
            };
            #endregion

            this.CurrentStageWatchers.Add(foundChestBaselineLeftColorWatcher);


            // Add new Watcher - Find BASELINE Color Marker Position on RIGHT Manikin Chest
            Watcher foundChestBaselineRightColorWatcher = new Watcher();

            #region Find BASELINE Color Marker Position on RIGHT Manikin Chest Watcher Logic
            foundChestBaselineRightColorWatcher.CheckForActionDelegate = () =>
            {
                if (!this.foundChestBaselineRightColor)
                {
                    // TREY ADDING FUNCTIONS
                    ColorSpacePoint colorSpacePoint = Watcher.ColorSpaceFromPoint3D(this.Lesson.ManikinFaceLocation);
                    //Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, -150, 20, 120, 120);
                    // TREY DEBUG
                    Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, -20 + offsetX, 5 + offsetY, 20, 40);

                    Point point = AIMS.Assets.Lessons2.ColorTracker.find(   this._manikinChestColor,
                                                                            rect,
                                                                            KinectManager.ColorData);


//                    CameraSpacePoint cameraSpacePoint = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };
//                    ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(cameraSpacePoint);
////                    Point point = AIMS.Assets.Lessons2.ColorTracker.find(this._manikinChestColor, new System.Windows.Int32Rect((int)colorSpacePoint.X - 40, (int)colorSpacePoint.Y + 30, 40, 40), KinectManager.ColorData);
//                    Point point = AIMS.Assets.Lessons2.ColorTracker.find(   this._manikinChestColor, 
//                                                                            new System.Windows.Int32Rect(   (int)colorSpacePoint.X - 150, 
//                                                                                                            (int)colorSpacePoint.Y + 20, 
//                                                                                                            120, 
//                                                                                                            120), 
//                                                                            KinectManager.ColorData);

                    if (!Double.IsNaN(point.X) && !Double.IsNaN(point.Y))
                    {
                        // TREY: MOVED ADDITIONS
                        this.yellowRightCounter++;

                        // TREY: MOVED ADDITIONS HERE
                        this.chestBaselineRightPt.X += point.X;
                        this.chestBaselineRightPt.Y += point.Y;

                        // DEBUG
                        this.Lesson.IntubationPage.FOUND_RIGHT_CHEST.Content = "R.CH.:" + this.yellowRightCounter.ToString();

                        if (this.yellowRightCounter >= IntubationStage11.baselineCount)
                        {
                            // Get the average of the found x's and y's
                            this.chestBaselineRightPt.X /= this.yellowRightCounter;
                            this.chestBaselineRightPt.Y /= this.yellowRightCounter;

                            this.chestRightRect = new Int32Rect(    (int)System.Math.Round(this.chestBaselineRightPt.X) - 15 * 3, 
                                                                    (int)System.Math.Round(this.chestBaselineRightPt.Y) - 15 * 2, 
                                                                    30 * 3, 
                                                                    30 * 2);

                            this.foundChestBaselineRightColor = true;
                            foundChestBaselineRightColorWatcher.IsTriggered = true;

                            System.Diagnostics.Debug.WriteLine("IntubationStage11::Baseline Right Yellow Point: X = " + this.chestBaselineRightPt.X + ", Y = " + this.chestBaselineRightPt.Y);
                        
                        }
                    }
                    else
                    {
                        // Color not found this frame, reset point X, Y, and counter
                        foundChestBaselineRightColorWatcher.IsTriggered = false;
                        this.yellowRightCounter = 0;
                        this.chestBaselineRightPt.X = 0;
                        this.chestBaselineRightPt.Y = 0;
                    }
                }
            };
            #endregion

            this.CurrentStageWatchers.Add(foundChestBaselineRightColorWatcher);
            //

            // Add new Watcher - Find BASELINE Color Marker Position on Manikin Stomach
            Watcher foundStomachBaselineColorWatcher = new Watcher();

            #region Find BASELINE Color Marker Position on Manikin Stomach Watcher Logic
            foundStomachBaselineColorWatcher.CheckForActionDelegate = () =>
            {
                if (!this.foundStomachBaselineColor)
                {
                    // TREY: USING FUNCTIONS
                    ColorSpacePoint colorSpacePoint = Watcher.ColorSpaceFromPoint3D(this.Lesson.ManikinFaceLocation);
                    //Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, -40, 120, 80, 120);
                    // TREY DEBUG
                    Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, -20 + offsetX, 50 + offsetY, 40, 30);

                    Point point = AIMS.Assets.Lessons2.ColorTracker.find(   this._manikinStomachColor,
                                                                            rect,
                                                                            KinectManager.ColorData);

//                    CameraSpacePoint cameraSpacePoint = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };
//                    ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(cameraSpacePoint);
////                    Point point = AIMS.Assets.Lessons2.ColorTracker.find(this._manikinStomachColor, new System.Windows.Int32Rect((int)colorSpacePoint.X - 20, (int)colorSpacePoint.Y + 60, 40, 60), KinectManager.ColorData);
//                    Point point = AIMS.Assets.Lessons2.ColorTracker.find(   this._manikinStomachColor, 
//                                                                            new System.Windows.Int32Rect(   (int)colorSpacePoint.X - 40, 
//                                                                                                            (int)colorSpacePoint.Y + 120, 
//                                                                                                            80, 
//                                                                                                            120),
//                                                                            KinectManager.ColorData);

                    if (!Double.IsNaN(point.X) && !Double.IsNaN(point.Y))
                    {
                        // TREY: MOVED ADDITIONS
                        this.redCounter++;

                        // TREY: MOVED ADDITIONS HERE
                        this.stomachBaselinePt.X += point.X;
                        this.stomachBaselinePt.Y += point.Y;

                        // DEBUG
                        this.Lesson.IntubationPage.FOUND_STOMACH.Content = "STOM.:"+ this.redCounter.ToString();

                        if (this.redCounter >= IntubationStage11.baselineCount)
                        {
                            // Get the average of the found x's and y's
                            this.stomachBaselinePt.X /= this.redCounter;
                            this.stomachBaselinePt.Y /= this.redCounter;

                            this.stomachRect = new Int32Rect(   (int)System.Math.Round(this.stomachBaselinePt.X) - 15 * 3,  
                                                                (int)System.Math.Round(this.stomachBaselinePt.Y) - 15 * 2, 
                                                                30 * 3, 
                                                                30 * 2);

                            this.foundStomachBaselineColor = true;
                            foundStomachBaselineColorWatcher.IsTriggered = true;

                            System.Diagnostics.Debug.WriteLine("IntubationStage11::Baseline Stomach Red Point: X = " + this.stomachBaselinePt.X + ", Y = " + this.stomachBaselinePt.Y);

                        }
                    }
                    else
                    {
                        // Color not found this frame, reset point X, Y, and counter
                        foundStomachBaselineColorWatcher.IsTriggered = false;
                        this.redCounter = 0;
                        this.stomachBaselinePt.X = 0;
                        this.stomachBaselinePt.Y = 0;
                    }
                }
            };
            #endregion

            this.CurrentStageWatchers.Add(foundStomachBaselineColorWatcher);
            #endregion

            #region CURRENT
            // Add new Watcher - Find CURRENT Color Marker Position on LEFT Manikin Chest
            Watcher foundChestCurrentLeftColorWatcher = new Watcher();

            #region Find CURRENT Color Marker Position on LEFT Manikin Chest Watcher Logic
            foundChestCurrentLeftColorWatcher.CheckForActionDelegate = () =>
            {
                if (!this.foundChestCurrentLeftColor && this.foundChestBaselineLeftColor)
                {
                    Point point = AIMS.Assets.Lessons2.ColorTracker.find(   this._manikinChestColor, this.chestLeftRect, KinectManager.ColorData);

                    if (!Double.IsNaN(point.X) && !Double.IsNaN(point.Y))
                    {
                        this.chestCurrentLeftPt = point;
                        this.foundChestCurrentLeftColor = true;
                        foundChestCurrentLeftColorWatcher.IsTriggered = true;

                        //System.Diagnostics.Debug.WriteLine("IntubationStage11::Current Left Yellow Point: X = " + this.chestCurrentLeftPt.X + ", Y = " + this.chestCurrentLeftPt.Y);
                    }
                    else
                    {
                        foundChestCurrentLeftColorWatcher.IsTriggered = false;
                    }
                }
            };
            #endregion

            this.CurrentStageWatchers.Add(foundChestCurrentLeftColorWatcher);


            // Add new Watcher - Find CURRENT Color Marker Position on RIGHT Manikin Chest
            Watcher foundChestCurrentRightColorWatcher = new Watcher();

            #region Find CURRENT Color Marker Position on RIGHT Manikin Chest Watcher Logic
            foundChestCurrentRightColorWatcher.CheckForActionDelegate = () =>
            {
                if (!this.foundChestCurrentRightColor && this.foundChestBaselineRightColor)
                {
                    Point point = AIMS.Assets.Lessons2.ColorTracker.find(this._manikinChestColor, this.chestRightRect, KinectManager.ColorData);

                    if (!Double.IsNaN(point.X) && !Double.IsNaN(point.Y))
                    {
                        this.chestCurrentRightPt = point;
                        this.foundChestCurrentRightColor = true;
                        foundChestCurrentRightColorWatcher.IsTriggered = true;

                        //System.Diagnostics.Debug.WriteLine("IntubationStage11::Current Right Yellow Point: X = " + this.chestCurrentRightPt.X + ", Y = " + this.chestCurrentRightPt.Y);
                    }
                    else
                    {
                        foundChestCurrentRightColorWatcher.IsTriggered = false;
                    }
                }
            };
            #endregion

            this.CurrentStageWatchers.Add(foundChestCurrentRightColorWatcher);


            // Add new Watcher - Find CURRENT Color Marker Position on Manikin Stomach
            Watcher foundStomachCurrentColorWatcher = new Watcher();

            #region Find CURRENT Color Marker Position on Manikin Stomach Watcher Logic
            foundStomachCurrentColorWatcher.CheckForActionDelegate = () =>
            {
                if (!this.foundStomachCurrentColor && this.foundStomachBaselineColor)
                {
                    Point point = AIMS.Assets.Lessons2.ColorTracker.find(this._manikinStomachColor, this.stomachRect, KinectManager.ColorData);

                    if (!Double.IsNaN(point.X) && !Double.IsNaN(point.Y))
                    {
                        this.stomachCurrentPt = point;
                        this.foundStomachCurrentColor = true;
                        foundStomachCurrentColorWatcher.IsTriggered = true;

                        //System.Diagnostics.Debug.WriteLine("IntubationStage11::Current Stomach Red Point: X = " + this.stomachCurrentPt.X + ", Y = " + this.stomachCurrentPt.Y);
                    }
                    else
                    {
                        foundStomachCurrentColorWatcher.IsTriggered = false;
                    }
                }
            };
            #endregion

            this.CurrentStageWatchers.Add(foundStomachCurrentColorWatcher);
            #endregion

            // Add new Watcher - Checks for User Hands to be in close proximity of the Manikin Head
            Watcher handsByManikinHead = new Watcher();

            #region User HANDS are near the Manikin Head Watcher Logic
            handsByManikinHead.CheckForActionDelegate = () =>
            {
                if (this.IsHandInRangeToChin("Left", true) || this.IsHandInRangeToChin("Right", true))
                {
                    this.foundHands = true;

                    if (!this.TimeoutStopwatch.IsRunning && !this.TimedOut)
                        this.TimeoutStopwatch.Start();

                }
                else
                {
                    this.foundHands = false;
                }
            };
            #endregion

            this.CurrentStageWatchers.Add(handsByManikinHead);

            #region DISTANCE
            // Add new Watcher - Find DISTANCE between Left Chest Color Marker Positions by Comparing CURRENT to BASELINE
            Watcher chestRiseLeftWatcher = new Watcher();

            #region Find DISTANCE between Left Chest Color Marker Positions by Comparing CURRENT to BASELINE Watcher Logic
            chestRiseLeftWatcher.CheckForActionDelegate = () =>
            {
                if (this.foundChestBaselineLeftColor && this.foundHands)
                {
                    if (this.foundChestCurrentLeftColor)
                    {
                        double yDist = this.chestBaselineLeftPt.Y - this.chestCurrentLeftPt.Y;

                        // Check if the required distance has been observed
                        if (yDist >= IntubationStage11.chestReqPtDistance)
                        {
                            if (this.chestRiseLeft.Count == 0)
                            {
                                this.chestRiseLeft.Add(new Detection(this.Stopwatch.ElapsedMilliseconds, yDist));
                                chestRiseLeftWatcher.IsTriggered = true;

                                //System.Diagnostics.Debug.WriteLine("IntubationStage11::Left Chest Dist = " + yDist + ", Time = " + this.Stopwatch.ElapsedMilliseconds);
                            }
                            else if (this.chestRiseLeft.Count > 0)
                            {
                                if ((this.Stopwatch.ElapsedMilliseconds - this.chestRiseLeft[this.chestRiseLeft.Count - 1].time) >= IntubationStage11.deltaTime)
                                {
                                    this.chestRiseLeft.Add(new Detection(this.Stopwatch.ElapsedMilliseconds, yDist));
                                    chestRiseLeftWatcher.IsTriggered = true;

                                    //System.Diagnostics.Debug.WriteLine("IntubationStage11::Left Chest Dist = " + yDist + ", Time = " + this.Stopwatch.ElapsedMilliseconds);
                                }
                            }
                        }
                        else
                        {
                            chestRiseLeftWatcher.IsTriggered = false;
                        }

                        // Reset
                        this.foundChestCurrentLeftColor = false;
                    }
                }
            };
            #endregion

            this.CurrentStageWatchers.Add(chestRiseLeftWatcher);


            // Add new Watcher - Find DISTANCE between Right Chest Color Marker Positions by Comparing CURRENT to BASELINE
            Watcher chestRiseRightWatcher = new Watcher();

            #region Find DISTANCE between Right Chest Color Marker Positions by Comparing CURRENT to BASELINE Watcher Logic
            chestRiseRightWatcher.CheckForActionDelegate = () =>
            {
                if (this.foundChestBaselineRightColor && this.foundHands)
                {
                    if (this.foundChestCurrentRightColor)
                    {
                        double yDist = this.chestBaselineRightPt.Y - this.chestCurrentRightPt.Y;

                        // Check if the required distance has been observed
                        if (yDist >= IntubationStage11.chestReqPtDistance)
                        {
                            if (this.chestRiseRight.Count == 0)
                            {
                                this.chestRiseRight.Add(new Detection(this.Stopwatch.ElapsedMilliseconds, yDist));
                                chestRiseRightWatcher.IsTriggered = true;

                                //System.Diagnostics.Debug.WriteLine("IntubationStage11::Right Chest Dist = " + yDist + ", Time = " + this.Stopwatch.ElapsedMilliseconds);
                            }
                            else if (this.chestRiseRight.Count > 0)
                            {
                                if ((this.Stopwatch.ElapsedMilliseconds - this.chestRiseRight[this.chestRiseRight.Count - 1].time) >= IntubationStage11.deltaTime)
                                {
                                    this.chestRiseRight.Add(new Detection(this.Stopwatch.ElapsedMilliseconds, yDist));
                                    chestRiseRightWatcher.IsTriggered = true;

                                    //System.Diagnostics.Debug.WriteLine("IntubationStage11::Right Chest Dist = " + yDist + ", Time = " + this.Stopwatch.ElapsedMilliseconds);
                                }
                            }
                        }
                        else
                        {
                            chestRiseRightWatcher.IsTriggered = false;
                        }

                        // Reset
                        this.foundChestCurrentRightColor = false;
                    }
                }
            };
            #endregion

            this.CurrentStageWatchers.Add(chestRiseRightWatcher);


            // Add new Watcher - Find DISTANCE between Stomach Color Marker Positions by Comparing CURRENT to BASELINE
            Watcher stomachRiseWatcher = new Watcher();

            #region Find DISTANCE between Stomach Color Marker Positions by Comparing CURRENT to BASELINE Watcher Logic
            stomachRiseWatcher.CheckForActionDelegate = () =>
            {
                if (this.foundStomachBaselineColor && this.foundHands)
                {
                    if (this.foundStomachCurrentColor)
                    {
                        double yDist = this.stomachBaselinePt.Y - this.stomachCurrentPt.Y;

                        // Check if the required distance has been observed
                        if (yDist >= IntubationStage11.stomachReqPtDistance)
                        {
                            if (this.stomachRise.Count == 0)
                            {
                                this.stomachRise.Add(new Detection(this.Stopwatch.ElapsedMilliseconds, yDist));
                                stomachRiseWatcher.IsTriggered = true;

                                //System.Diagnostics.Debug.WriteLine("IntubationStage11::Stomach Dist = " + yDist + ", Time = " + this.Stopwatch.ElapsedMilliseconds);
                            }
                            else if (this.stomachRise.Count > 0)
                            {
                                if ((this.Stopwatch.ElapsedMilliseconds - this.stomachRise[this.stomachRise.Count - 1].time) >= IntubationStage11.deltaTime)
                                {
                                    this.stomachRise.Add(new Detection(this.Stopwatch.ElapsedMilliseconds, yDist));
                                    stomachRiseWatcher.IsTriggered = true;

                                    //System.Diagnostics.Debug.WriteLine("IntubationStage11::Stomach Dist = " + yDist + ", Time = " + this.Stopwatch.ElapsedMilliseconds);
                                }
                            }
                        }
                        else
                        {
                            stomachRiseWatcher.IsTriggered = false;
                        }

                        // Reset
                        this.foundStomachCurrentColor = false;
                    }
                }
            };
            #endregion

            this.CurrentStageWatchers.Add(stomachRiseWatcher);

            #endregion

            #region Test Data
            /*
            // Add new TEST DATA Watcher - Find CURRENT Color Marker Positions and Associated Time
            Watcher colorsWatcher = new Watcher();

            colorsWatcher.CheckForActionDelegate = () =>
            {
                if (this.foundStomachBaselineColor && this.foundChestBaselineLeftColor && this.foundChestBaselineRightColor)
                {
                    Point leftpoint = AIMS.Assets.Lessons2.ColorTracker.find(ColorTracker.GetColorRange("yellow", Lighting.none), new System.Windows.Int32Rect((int)this.Lesson.ManikinFaceLocation.X, (int)this.Lesson.ManikinFaceLocation.Y + 30, 40, 40), KinectManager.MappedColorData);
                    Point rightpoint = AIMS.Assets.Lessons2.ColorTracker.find(ColorTracker.GetColorRange("yellow", Lighting.none), new System.Windows.Int32Rect((int)this.Lesson.ManikinFaceLocation.X - 40, (int)this.Lesson.ManikinFaceLocation.Y + 30, 40, 40), KinectManager.MappedColorData);
                    Point stomachpoint = AIMS.Assets.Lessons2.ColorTracker.find(ColorTracker.GetColorRange("red", Lighting.none), new System.Windows.Int32Rect((int)this.Lesson.ManikinFaceLocation.X - 20, (int)this.Lesson.ManikinFaceLocation.Y + 60, 40, 40), KinectManager.MappedColorData);

                    System.Diagnostics.Debug.WriteLine(this.Stopwatch.ElapsedMilliseconds + ", " + leftpoint.X + ", " + leftpoint.Y + ", " + KinectManager.PixelData[(640 * (int)leftpoint.Y + (int)leftpoint.X)].Depth + ", " +
                                                       rightpoint.X + ", " + rightpoint.Y + ", " + KinectManager.PixelData[(640 * (int)rightpoint.Y + (int)rightpoint.X)].Depth + ", " +
                                                       stomachpoint.X + ", " + stomachpoint.Y + ", " + KinectManager.PixelData[(640 * (int)stomachpoint.Y + (int)stomachpoint.X)].Depth);

                    colorsWatcher.IsTriggered = true;
                }
                else
                {
                    colorsWatcher.IsTriggered = false;
                }
            };

            this.CurrentStageWatchers.Add(colorsWatcher);
            */
            #endregion

        } // End CreateStageWatchers()

        #region Hand Distance Check
        private bool IsHandInRangeToChin(string handType, bool useDepth)
        {
            CameraSpacePoint handLoc;
            double handDistance;

            SkeletonProfile skele = null;

            if (this.Lesson.StuckSkeletonProfileID.HasValue)
                skele = KinectManager.SingleStickySkeletonProfile;

            if (skele != null)
            {
                // Get the correct hand joint position out of the profile.
                if (handType == "Right")
                    handLoc = skele.JointProfiles.First(o => o.JointType == JointType.HandRight).CurrentPosition;
                else
                    handLoc = skele.JointProfiles.First(o => o.JointType == JointType.HandLeft).CurrentPosition;

                if (useDepth)
                {
                    handDistance = System.Math.Abs(KinectManager.Distance3D(this.manikinChinInSkeletalSpace, handLoc));

                    double desiredMaxDistance = 0.515;  // meters

                    bool isHandOutOfRange = false;

                    if (handDistance >= desiredMaxDistance)
                    {
                        // Distance between the user's hand and the manikin's chin is outside the desiredMaxDistance.
                        isHandOutOfRange = true;
                    }

                    if (isHandOutOfRange)
                    {
                        //System.Diagnostics.Debug.WriteLine("Value Proximity Breached -> Attempting 2D comparison...");
                        return IsHandInRangeToChin(handType, false);
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    handDistance = System.Math.Abs(KinectManager.Distance2D(this.manikinChinInSkeletalSpace, handLoc));

                    //System.Diagnostics.Debug.WriteLine(String.Format("2D: ManikinChin -> Right Hand Distance: {0:#}pixels", rightHandDistance * 1000));

                    if (handDistance < 0.4)
                        return true;
                }
            }

            return false;
        }
        #endregion

        public override void UpdateStage()
        {
            if (!this.IsSetup)
                this.SetupStage();

            // Check the Stage Timeout
            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
            {
                if (this.TimedOut)
                {
                    this.Lesson.DetectableActions.DetectAction("No Rise", "Rise and Fall", "Manikin Chest", true);
                    this.EndStage();
                }
                else
                    this.TimeoutStage();
            }

            #region Check Manikin Rise Counters for Stage Completion
            if (this.chestRiseLeft.Count >= 3 || this.chestRiseRight.Count >= 3)
            {
                foreach (Detection rise in this.chestRiseLeft)
                {
                    System.Diagnostics.Debug.WriteLine("IntubationStage11::UpdateStage Stage Complete Chest Rise LEFT: Dist = " + rise.distance + ", " + "Time = " + rise.time);
                }

                foreach (Detection rise in this.chestRiseRight)
                {
                    System.Diagnostics.Debug.WriteLine("IntubationStage11::UpdateStage Stage Complete Chest Rise RIGHT: Dist = " + rise.distance + ", " + "Time = " + rise.time);
                }

                this.Lesson.DetectableActions.DetectAction("Rose and Fell", "Rise and Fall", "Manikin Chest", true);
                this.Complete = true;
                this.EndStage();
            }
            else if (this.stomachRise.Count >= 3)
            {
                foreach (Detection rise in this.stomachRise)
                {
                    System.Diagnostics.Debug.WriteLine("IntubationStage11::UpdateStage Stage Complete Stomach Rise: Dist = " + rise.distance + ", " + "Time = " + rise.time);
                }

                this.Lesson.DetectableActions.DetectAction("Cant Rise", "Rise and Fall", "Manikin Chest", true);
                this.Complete = true;
                this.EndStage();
            }
            #endregion

            // TREY  DEBUG
            IntubationPage page = this.Lesson.IntubationPage;
            page.FOUND_LEFT_CHEST.IsChecked = this.foundChestBaselineLeftColor;
            page.FOUND_RIGHT_CHEST.IsChecked = this.foundChestBaselineRightColor;
            page.FOUND_STOMACH.IsChecked = this.foundStomachBaselineColor;

        } //End UpdateStage()


        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }

        public override void EndStage( bool playSound = true )
        {
            this.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            // Get the current time to signify the time the stage ended.
            this.EndTime = DateTime.Now;

            // Create a screenshot for the current stage.
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                Intubation lesson = (Intubation)args.Argument;

                KinectImage image = KinectImage.TakeKinectSnapshot(true, true);

                lesson.ChestRiseImage = image;
            };

            worker.RunWorkerAsync(this.Lesson);

            AssessChestRise();

            this.Lesson.CurrentStageIndex++;

            if( playSound ) Lesson.PlayNextStageSound();
        }

        private void AssessChestRise()
        {
            double score = 1;

            if (this.chestRiseLeft.Count >= 3 || this.chestRiseRight.Count >= 3)
            {
                this.Lesson.ChestRiseFeedbackIDs.Add(59);    // Chest rises were observed, indicating a properly inserted Endotracheal Tube.
            }
            else if (this.stomachRise.Count >= 3)
            {
                score -= 1;
                this.Lesson.ChestRiseFeedbackIDs.Add(60);    // Stomach rise was observed, indicating an improperly inserted Endotracheal Tube.
            }
            else
            {
                score -= 1;
                this.Lesson.ChestRiseFeedbackIDs.Add(61);    // The stage timed out and the lungs were not properly ventilated.
            }

            this.Lesson.ChestRiseScore = System.Math.Max(0, System.Math.Min(score, 1)); // Clamp the score between 0 and 1.
        }

        public override void PauseStage()
        {
            this.Lesson.Paused = true;
            this.Lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.Lesson.Paused = false;
            this.Lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void ResetStage()
        {

        }

        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            this.TimedOut = true;
            /*
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            
            this.RequestStageVideo();       // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
            this.TimeoutStopwatch.Start();
             */
        }


        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.Lesson.IntubationPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Intubation/Videos/Stage 011.wmv", UriKind.Relative));
            this.Lesson.IntubationPage.VideoPlayer.startVideo(this, new System.Windows.RoutedEventArgs());

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }


        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }


        public override void WipeClean()
        {
            // Stage Reinitialization

            // Chest & Stomach Counts
            yellowLeftCounter = 0;
            yellowRightCounter = 0;
            redCounter = 0;

            // Baseline color markers on the manikin chest and stomach found
            foundChestBaselineLeftColor = false;
            foundChestBaselineRightColor = false;
            foundStomachBaselineColor = false;

            // Baseline color markers on the manikin chest and stomach in X,Y coordinates
            chestBaselineLeftPt = new Point();
            chestBaselineRightPt = new Point();
            stomachBaselinePt = new Point();

            // Computed from baseline color tracker find() Rectangles for the manikin chest and stomach
            chestLeftRect = new Int32Rect();
            chestRightRect = new Int32Rect();
            stomachRect = new Int32Rect();

            // Current color markers on the manikin chest and stomach found
            foundChestCurrentLeftColor = false;
            foundChestCurrentRightColor = false;
            foundStomachCurrentColor = false;

            // Current color markers on the manikin chest and stomach in X,Y coordinates
            chestCurrentLeftPt = new Point();
            chestCurrentRightPt = new Point();
            stomachCurrentPt = new Point();

            // Look for Skeleton Hands in Proximity of Manikin Head
            foundHands = false;

            // Current position of color marker on the manikin chin in Skeleton Space
            manikinChinInSkeletalSpace = new CameraSpacePoint();

            // Save Chest & Stomach Rise Detections
            if (chestRiseLeft == null)
                chestRiseLeft = new List<Detection>();
            else chestRiseLeft.Clear();
            if (chestRiseRight == null)
                chestRiseRight = new List<Detection>();
            else chestRiseRight.Clear();
            if (stomachRise == null)
                stomachRise = new List<Detection>();
            else stomachRise.Clear();

            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.IsSetup = false;

            if (this.StageScore != null)
            {
                this.StageScore.WipeClean();
                this.StageScore = null;
                this.StageScore = new StageScore();
            }
            else
            {
                this.StageScore = new StageScore();
            }

            this.Active = false;
            this.Complete = false;

            if (this.Zones != null)
            {
                this.Zones.Clear();
            }
            else
            {
                this.Zones = new List<Zone>();
            }

            if (this.Stopwatch != null)
            {
                this.Stopwatch.Reset();
            }
            else
            {
                this.Stopwatch = new System.Diagnostics.Stopwatch();
            }

            if (this.TimeoutStopwatch != null)
            {
                this.TimeoutStopwatch.Reset();
            }
            else
            {
                this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            }

            this.TimedOut = false;
        }


    } //End class IntubationStage11


} //End namespace AIMS.Tasks.Intubation
