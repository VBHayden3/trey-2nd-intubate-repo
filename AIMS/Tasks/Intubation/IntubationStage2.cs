﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using System.Windows.Media.Imaging;
using System.Windows;
using System.ComponentModel;
using System.Diagnostics;

using Microsoft.Kinect;

using AIMS.Assets.Lessons2;

namespace AIMS.Tasks.Intubation
{
    public class IntubationStage2 : Stage
    {
        private Intubation lesson;
        public Intubation Lesson { get { return lesson; } set { lesson = value; } }

        private bool toolIsAboveMouth = false;
        private RecZone aboveMouthZone = null;
        private Point3D watcher_FoundLaryngoscopePos = new Point3D();
        private int watcher_YellowCounter = 0;
        private int watcher_YellowDepthCounter = 0;

        public bool ToolIsAboveMouth { get { return toolIsAboveMouth; } set { toolIsAboveMouth = value; } }
        public RecZone AboveMouthZone { get { return aboveMouthZone; } set { aboveMouthZone = value; } }


        private bool rightHandByTool = false;
        private bool leftHandByTool = false;
        private bool toolLeftSide = false;
        private bool toolRightSide = false;

        private bool toolInsertedRight = false;
        private bool toolInsertedLeft = false;

        private ColorRange _laryngoscopeColor;

        public IntubationStage2(Intubation intubation)
        {
            Lesson = intubation;

            StageNumber = 2;
            Name = "Grab Laryngoscope";
            Instruction = "Pick up the laryngoscope with your left hand and bring it over the patient's mouth.";
            TargetTime = TimeSpan.FromSeconds(3.5);
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Tasks/Intubation/Graphics/Intubation002.png";
            Active = false;
            Complete = false;
            ScoreWeight = 0.05;
            Zones = new List<Zone>();
            IsSetup = false;
            Stopwatch = new System.Diagnostics.Stopwatch();
            this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.CanTimeOut = false;
            this.TimeOutTime = TimeSpan.FromSeconds(10.0);
        }

        public override void SetupStage()
        {
            System.Diagnostics.Debug.WriteLine("IntubationStage2 -> SetupStage()");

            this.Lesson.IntubationPage.StageInfoTextBlock.Text = this.Name;
            this.Lesson.IntubationPage.InstructionTextBlock.Text = this.Instruction;

            this.Lesson.IntubationPage.flow.Index = this.Lesson.CurrentStageIndex;  // change cover flow image.

            // Set the image for the current stage.
            this.Lesson.IntubationPage.CurrentStageImage.Source = new BitmapImage(new Uri("/Tasks/Intubation/Graphics/Intubation002.png", UriKind.Relative));
            //this.Lesson.IntubationPage.CurrentStageImage.Source = new BitmapImage(new Uri("/Tasks/Intubation/Graphics/Intubation_Stage002_640.jpg", UriKind.Relative));

            try
            {
                this._laryngoscopeColor = ColorTracker.GetObjectColorRange("laryngoscope").ColorRange;
            }
            catch (Exception exc) { Debug.Print("IntubationStage2 SetupStage() ERROR: {0}", exc.Message); }

            this.Zones.Clear(); // Clear the stage zone list.

            this.Lesson.IntubationPage.zoneOverlay.ClearZones();

            // Add the zones into the zoneOverlay
            foreach (Zone zone in this.Zones)
            {
                this.Lesson.IntubationPage.zoneOverlay.AddZone(zone);
            }

            this.CurrentStageWatchers = new List<BaseWatcher>();
            this.PeripheralStageWatchers = new List<BaseWatcher>();
            this.OutlyingStageWatchers = new List<BaseWatcher>();

            CreateStageWatchers();

            this.StartTime = DateTime.Now;

            this.IsSetup = true;

            this.Stopwatch.Start();

            this.TimeoutStopwatch.Start();
        }

        private void CreateStageWatchers()
        {
            // Clear the list of watchers.
            this.CurrentStageWatchers.Clear();
            this.PeripheralStageWatchers.Clear();
            this.OutlyingStageWatchers.Clear();

            Watcher laryngoscopeDetectionWatcher = new Watcher();

            #region WatcherLogic

            laryngoscopeDetectionWatcher.CheckForActionDelegate = () =>
            {

                System.Diagnostics.Debug.WriteLine("IntubationStage2-> using manikin point " + this.Lesson.ManikinFaceLocation.X + "," + this.Lesson.ManikinFaceLocation.Y + "," + this.Lesson.ManikinFaceLocation.Z + "mm");

                // TREY: REPLACING WITH FUNCTIONS
                ColorSpacePoint colorSpacePoint = Watcher.ColorSpaceFromPoint3D(this.Lesson.ManikinFaceLocation);
                Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, -50, -100, 40, 100);
                Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._laryngoscopeColor,  
                                                                        rect, 
                                                                        KinectManager.ColorData);

                //CameraSpacePoint point = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };
                //ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(point);
                //Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._laryngoscopeColor,
                //                                                        new Int32Rect(  (int)System.Math.Max(0, colorSpacePoint.X - 50), 
                //                                                                        (int)System.Math.Max(0, colorSpacePoint.Y - 100),
                //                                                                        40, 
                //                                                                        100), 
                //                                                        KinectManager.ColorData);

                if (!double.IsNaN(found.X) && !double.IsNaN(found.Y))
                {
                    int depth = KinectManager.GetDepthValueForColorPoint(found);
                    if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                    {
                        Point3D loc = KinectManager.Get3DPointForColorPoint(found, depth);

                        this.Lesson.LaryngoscopeLocation = loc;

                        laryngoscopeDetectionWatcher.IsTriggered = true;

                        System.Diagnostics.Debug.WriteLine("IntubationStage2-> laryngoscopeDetectionWatcher.IsTriggered = true");
                    }
                    else
                    {
                        laryngoscopeDetectionWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    laryngoscopeDetectionWatcher.IsTriggered = false;
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(laryngoscopeDetectionWatcher);

            // Add in new watchers.
            Watcher laryngoscopePositionChangeWatcher = new Watcher();

            #region WatcherLogic

            // Checks for the laryngoscope to change position.
            laryngoscopePositionChangeWatcher.CheckForActionDelegate = () =>
            {
                // This watcher will check the position of the current laryngoscope and update our known position of the layrngoscope to the latest/greatest.
                // This watcher will only trigger when motion is detected, so if it's not triggered - then the watcher's determined location for the laryngoscope will be the last known position stored previously. 
                // +-> This is intended to allow the watcher to gather data from a few frames, and average them together in order to come up with the current position confidently.
                
                if (this.Lesson.LaryngoscopeLocation.X != 0 && this.Lesson.LaryngoscopeLocation.Y != 0)
                {
                    // TREY: REPLACING WITH FUNCTIONS
                    ColorSpacePoint colorSpacePoint = Watcher.ColorSpaceFromPoint3D(this.Lesson.LaryngoscopeLocation);
                    int pixelScanRange = 40;
                    int x = -1 * pixelScanRange / 2;
                    int y = -1 * pixelScanRange / 2;
                    int width = pixelScanRange;
                    int height = pixelScanRange;
                    Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, x, y, width, height);
                    Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._laryngoscopeColor,
                                                                            rect,
                                                                            KinectManager.ColorData);

                    //Point3D currentLoc = this.Lesson.LaryngoscopeLocation;
                    //CameraSpacePoint point = new CameraSpacePoint() { X = (float)currentLoc.X, Y = (float)currentLoc.Y, Z = (float)currentLoc.Z / 1000 };
                    //ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(point);

                    //int pixelScanRange = 40;
                    //Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._laryngoscopeColor, 
                    //                                                        new Int32Rect(  (int)System.Math.Max(0, colorSpacePoint.X - pixelScanRange / 2), 
                    //                                                                        (int)System.Math.Max(0, colorSpacePoint.Y - pixelScanRange / 2), 
                    //                                                                        (int)pixelScanRange, 
                    //                                                                        (int)pixelScanRange), 
                    //                                                        KinectManager.ColorData);

                    if( !double.IsNaN(found.X) && !double.IsNaN(found.Y) )
                    {
                        // The Point was found.
                        int depth = KinectManager.GetDepthValueForColorPoint(found);
                        if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                        {
                            watcher_FoundLaryngoscopePos.Z += ((double)depth);
                            watcher_YellowDepthCounter++;
                        }
                        else
                        {
                            watcher_FoundLaryngoscopePos.Z = 0;
                            watcher_YellowDepthCounter = 0;
                        }

                        watcher_FoundLaryngoscopePos.X += found.X;
                        watcher_FoundLaryngoscopePos.Y += found.Y;
                        watcher_YellowCounter++;
                    }
                    else
                    {
                        // Color wasn't found this time, reset X, Y, Z, and both counters.
                        watcher_YellowCounter = 0;
                        watcher_FoundLaryngoscopePos.X = 0;
                        watcher_FoundLaryngoscopePos.Y = 0;

                        watcher_YellowDepthCounter = 0;
                        watcher_FoundLaryngoscopePos.Z = 0;

                    }

                    if (watcher_YellowCounter > 0 && watcher_YellowDepthCounter > 0)
                    {
                        Point3D temp = new Point3D();
                        // Get the average of the found x's.
                        temp.X = watcher_FoundLaryngoscopePos.X / watcher_YellowCounter;
                        // Get the average of the found y's.
                        temp.Y = watcher_FoundLaryngoscopePos.Y / watcher_YellowCounter;
                        // Get the average of the found z's.
                        temp.Z = watcher_FoundLaryngoscopePos.Z / watcher_YellowDepthCounter;

                        watcher_FoundLaryngoscopePos = temp;

                        //System.Diagnostics.Debug.WriteLine(String.Format("ManikinRefoundAt: ({0},{1},{2}).", watcher_FoundLaryngoscopePos.X, watcher_FoundLaryngoscopePos.Y, watcher_FoundLaryngoscopePos.Z));

                        //MessageBox.Show(String.Format("Depth of chin is {0}mm from the Kinect sensor.", newHeadLocation.Z));

                        // Check for a change in Depth.
                        double deltaDepth = System.Math.Abs(watcher_FoundLaryngoscopePos.Z - this.Lesson.LaryngoscopeLocation.Z);

                        System.Diagnostics.Debug.WriteLine(String.Format("IntubationStage2 -> Change in laryngoscope depth is: {0}mm", deltaDepth));

                        if ( deltaDepth > 14 && deltaDepth < 300 )
                        {
                            laryngoscopePositionChangeWatcher.IsTriggered = true;
                        }
                        else
                        {
                            laryngoscopePositionChangeWatcher.IsTriggered = false;
                            watcher_FoundLaryngoscopePos = new Point3D();
                            watcher_YellowCounter = 0;
                            watcher_YellowDepthCounter = 0;
                        }
                    }
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(laryngoscopePositionChangeWatcher);

            Watcher leftHandOrWristByLaryngoscopeWatcher = new Watcher();

            #region WatcherLogic

            // Checks for the left hand or wrist to come into close proximity of the laryngoscope's location.
            leftHandOrWristByLaryngoscopeWatcher.CheckForActionDelegate = () =>
            {
                CameraSpacePoint laryngoscopeInSkeletalSpace;
                CameraSpacePoint lHandLoc;
                CameraSpacePoint lWristLoc;
                double lHandDistance;
                double lWristDistance;

                // TREY: REPLACING  WITH FUNCTION
                // laryngoscopeInSkeletalSpace = new CameraSpacePoint() { X = (float)this.Lesson.LaryngoscopeLocation.X, Y = (float)this.Lesson.LaryngoscopeLocation.Y, Z = (float)this.Lesson.LaryngoscopeLocation.Z / 1000 };
                laryngoscopeInSkeletalSpace = Watcher.CameraSpaceFromPoint3D(this.Lesson.LaryngoscopeLocation);


                SkeletonProfile skele = null;

                if (this.Lesson.StuckSkeletonProfileID.HasValue)
                {
                    // Retreive
                    skele = KinectManager.SingleStickySkeletonProfile;
                }
                if (skele != null)
                {
                    // Get the left hand and left wrist joint positions out of the profile.
                    lHandLoc = skele.JointProfiles.First(o => o.JointType == JointType.HandLeft).CurrentPosition;
                    lWristLoc = skele.JointProfiles.First(o => o.JointType == JointType.WristLeft).CurrentPosition;

                    // Distance returned by KinectManager.Distance3D( SkeletalPoint a, SkeletalPoint b) is in SkeletalSpace, and therefore is represented in Meters.
                    lHandDistance = System.Math.Abs(KinectManager.Distance3D(laryngoscopeInSkeletalSpace, lHandLoc));     // Check the distance between the laryngoscope in SkeletalSpace and the left hand. Absolute value is used to negate which point is used as the base.
                    lWristDistance = System.Math.Abs(KinectManager.Distance3D(laryngoscopeInSkeletalSpace, lWristLoc));   // Check the distance between the laryngoscope in the SkeletalSpace and the left wrist. Absolute value is used to negate which point is used as the base.

                    //System.Diagnostics.Debug.WriteLine(String.Format("L-Hand to laryngoscope:  {0:#}mm", lHandDistance * 1000));
                    //System.Diagnostics.Debug.WriteLine(String.Format("L-Wrist to laryngoscope: {0:#}mm @{1}", lWristDistance * 1000, KinectManager.AzimuthAngle(laryngoscopeInSkeletalSpace, lWristLoc)));

                    double desiredMaxDistance = 0.210;  // 210 mm

                    // Check if the left hand or wrist is found near the laryngoscope of the manikin.
                    if (lHandDistance <= desiredMaxDistance || lWristDistance <= desiredMaxDistance)
                    {
                        // If it is within the disiredMaxDistance, flip our trigger switch on.
                        leftHandOrWristByLaryngoscopeWatcher.IsTriggered = true;
                    }
                    else
                    {
                        // If it's outside the desiredMaxDistance, then flip our trigger switch off.
                        leftHandOrWristByLaryngoscopeWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    // If no skeleton was found, then the hand/wrist can't be near the laryngoscope.
                    leftHandOrWristByLaryngoscopeWatcher.IsTriggered = false;
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(leftHandOrWristByLaryngoscopeWatcher);

            Watcher laryngoscopeDetectedOverRightMouthWatcher = new Watcher();

            #region WatcherLogic

            laryngoscopeDetectedOverRightMouthWatcher.CheckForActionDelegate = () =>
            {
                // TREY: REPLACING WITH  FUNCTION
                ColorSpacePoint colorSpacePoint = Watcher.ColorSpaceFromPoint3D(this.Lesson.ManikinFaceLocation);
                Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, 0, -80, 30, 60);
                Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._laryngoscopeColor,
                                                                        rect,
                                                                        KinectManager.ColorData);

                //CameraSpacePoint point = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };
                //ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(point);
                //Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._laryngoscopeColor, 
                //                                                        new Int32Rect(  (int)System.Math.Max(0, colorSpacePoint.X), 
                //                                                                        (int)System.Math.Max(0, colorSpacePoint.Y - 80), 
                //                                                                        30, 
                //                                                                        60), 
                //                                                        KinectManager.ColorData);

                if (!double.IsNaN(found.X) && !double.IsNaN(found.Y))
                {
                    int depth = KinectManager.GetDepthValueForColorPoint(found);
                    if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                    {
                        Point3D loc = KinectManager.Get3DPointForColorPoint(found, depth);

                        this.Lesson.LaryngoscopeLocation = loc;

                        laryngoscopeDetectedOverRightMouthWatcher.IsTriggered = true;
                    }
                    else
                    {
                        laryngoscopeDetectedOverRightMouthWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    laryngoscopeDetectedOverRightMouthWatcher.IsTriggered = false;
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(laryngoscopeDetectedOverRightMouthWatcher);

            Watcher laryngoscopeDetectedOverLeftMouthWatcher = new Watcher();

            #region WatcherLogic

            laryngoscopeDetectedOverLeftMouthWatcher.CheckForActionDelegate = () =>
            {
                //CameraSpacePoint point = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };
                // ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(point);
                // TREY: REPLACING  WITH  FUNCTIONS
                ColorSpacePoint colorSpacePoint = Watcher.ColorSpaceFromPoint3D(this.Lesson.ManikinFaceLocation);
               
                //System.Diagnostics.Debug.WriteLine("IntubationStage2-> get manikin camera space point " + point.X + "," + point.Y + "," + point.Z + "m");                
                System.Diagnostics.Debug.WriteLine("IntubationStage2-> check for laryngoscopeColor around color space point " + colorSpacePoint.X + "," + colorSpacePoint.Y);

                // TREY; REPLACING  WITH  FUNCTION
                //Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._laryngoscopeColor, 
                //                                                        new Int32Rect(  (int)System.Math.Max(0, colorSpacePoint.X - 30), 
                //                                                                        (int)System.Math.Max(0, colorSpacePoint.Y - 80), 
                //                                                                        30, 
                //                                                                        60), 
                //                                                        KinectManager.ColorData);
                
                // TREY: STATING  EXPLICITLY
                Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, -30, -80, 30, 60);
                Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._laryngoscopeColor,
                                                                        rect,
                                                                        KinectManager.ColorData);



                if (!double.IsNaN(found.X) && !double.IsNaN(found.Y))
                {
                    int depth = KinectManager.GetDepthValueForColorPoint(found);
                    if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                    {
                        Point3D loc = KinectManager.Get3DPointForColorPoint(found, depth);

                        this.Lesson.LaryngoscopeLocation = loc;

                        laryngoscopeDetectedOverLeftMouthWatcher.IsTriggered = true;
                    }
                    else
                    {
                        laryngoscopeDetectedOverLeftMouthWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    laryngoscopeDetectedOverLeftMouthWatcher.IsTriggered = false;
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(laryngoscopeDetectedOverLeftMouthWatcher);
        }

        public override void UpdateStage()
        {
            if (!this.IsSetup)
                SetupStage();

            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
                this.TimeoutStage();

            if (this.CurrentStageWatchers != null)
            {
                if (this.CurrentStageWatchers[0].IsTriggered)
                {
                    // Laryngoscope is detected to the left of the manikin's chin.
                    if (!this.toolLeftSide)
                    {
                        toolLeftSide = true;
                        this.Lesson.DetectableActions.DetectAction("Picked Up", "Pick Up", "Laryngoscope", true);

                        if (RightHandCloserToLaryngoscopeThanLeft( true ))
                        {
                            this.Lesson.DetectableActions.DetectAction("Right Handed Laryngoscope Grab", "Grab Laryngoscope", "User", true);
                        }
                        else
                        {
                            this.Lesson.DetectableActions.DetectAction("Left Handed Laryngoscope Grab", "Grab Laryngoscope", "User", true);
                        }
                    }
                }
                /*
                else
                {
                    if (this.toolLeftSide)
                    {
                        toolLeftSide = false;
                        this.Lesson.DetectableActions.UndetectAction("Picked Up", "Pick Up", "Laryngoscope", false);
                    }
                }*/

                if (this.CurrentStageWatchers[1].IsTriggered)
                {
                    // Laryngoscope is in motion.
                }

                if (this.CurrentStageWatchers[2].IsTriggered)
                {
                    // User's left hand/wrist is by the Laryngoscope.
                    if (!this.leftHandByTool)
                    {
                        this.leftHandByTool = true;
                        this.Lesson.DetectableActions.DetectAction("Left Handed Laryngoscope Grab", "Grab Laryngoscope", "User", true);
                    }
                }
                else
                {
                    if (this.leftHandByTool)
                    {
                        this.leftHandByTool = false;
                        //this.Lesson.DetectableActions.UndetectAction("Left Handed Laryngoscope Grab", "Grab Laryngoscope", "User", false);
                    }
                }

                if (this.CurrentStageWatchers[3].IsTriggered)
                {
                    // Larnygoscope is located above the mouth.
                    if (!this.toolInsertedRight)
                    {
                        this.toolInsertedRight = true;
                        this.Lesson.DetectableActions.DetectAction("Inserted Right Side", "Insert", "Laryngoscope", true);
                    }
                }
                else
                {
                    if (this.toolInsertedRight)
                    {
                        this.toolInsertedRight = false;
                        this.Lesson.DetectableActions.UndetectAction("Inserted Right Side", "Insert", "Laryngoscope", false);
                    }
                }

                if (this.CurrentStageWatchers[4].IsTriggered)
                {
                    // Larnygoscope is located above the mouth.
                    if (!this.toolInsertedLeft)
                    {
                        this.toolInsertedLeft = true;
                        this.Lesson.DetectableActions.DetectAction("Inserted Left Side", "Insert", "Laryngoscope", true);
                    }
                }
                else
                {
                    if (this.toolInsertedLeft)
                    {
                        this.toolInsertedLeft = false;
                        this.Lesson.DetectableActions.UndetectAction("Inserted Left Side", "Insert", "Laryngoscope", false);
                    }
                }
            }

            if (toolInsertedLeft || toolInsertedRight)
            {
                this.ToolIsAboveMouth = true;
            }
            else
            {
                this.ToolIsAboveMouth = false;
            }

            if (ToolIsAboveMouth)
            {
                this.Complete = true;
                this.EndStage();
            }
        }


        private bool RightHandCloserToLaryngoscopeThanLeft(bool useDepth)
        {
            CameraSpacePoint laryngoscopeInSkeletalSpace;
            CameraSpacePoint rightHandLoc;
            CameraSpacePoint leftHandLoc;
            double rightHandDistance;
            double leftHandDistance;

            laryngoscopeInSkeletalSpace = new CameraSpacePoint() { X = (float)this.Lesson.LaryngoscopeLocation.X, Y = (float)this.Lesson.LaryngoscopeLocation.Y, Z = (float)this.Lesson.LaryngoscopeLocation.Z / 1000 };

            SkeletonProfile skele = null;

            if (this.Lesson.StuckSkeletonProfileID.HasValue)
            {
                // Retreive
                skele = KinectManager.SingleStickySkeletonProfile;
            }
            if (skele != null)
            {
                // Get the right hand and right wrist joint positions out of the profile.
                rightHandLoc = skele.JointProfiles.First(o => o.JointType == JointType.HandRight).CurrentPosition;
                leftHandLoc = skele.JointProfiles.First(o => o.JointType == JointType.HandLeft).CurrentPosition;

                if (useDepth)
                {
                    // Distance returned by KinectManager.Distance3D( SkeletalPoint a, SkeletalPoint b) is in SkeletalSpace, and therefore is represented in Meters.
                    rightHandDistance = System.Math.Abs(KinectManager.Distance3D(laryngoscopeInSkeletalSpace, rightHandLoc));     // Check the distance between the manikinChin in SkeletalSpace and the right hand. Absolute value is used to negate which point is used as the base.
                    leftHandDistance = System.Math.Abs(KinectManager.Distance3D(laryngoscopeInSkeletalSpace, leftHandLoc));     // Check the distance between the manikinChin in SkeletalSpace and the right hand. Absolute value is used to negate which point is used as the base.

                    System.Diagnostics.Debug.WriteLine(String.Format("IntubationStage2 -> 3D: Laryngoscope -> Right Hand Distance: {0:#}mm", rightHandDistance * 1000));
                    System.Diagnostics.Debug.WriteLine(String.Format("IntubationStage2 -> 3D: Laryngoscope -> Left Hand Distance: {0:#}mm", leftHandDistance * 1000));

                    double desiredMaxDistance = 0.515;  // meters

                    bool isRightHandOutOfRange = false;
                    bool isLeftHandOutOfRange = false;

                    if (rightHandDistance >= desiredMaxDistance)
                    {
                        // Distance between the user's head and the manikin's chin is within the desiredMaxDistance.
                        isRightHandOutOfRange = true;
                    }

                    if (leftHandDistance >= desiredMaxDistance)
                    {
                        // Distance between the user's head and the manikin's chin is within the desiredMaxDistance.
                        isLeftHandOutOfRange = true;
                    }

                    if (isLeftHandOutOfRange && isRightHandOutOfRange)
                    {
                        System.Diagnostics.Debug.WriteLine("Value Proximity Breached -> Attempting 2D comparison...");
                        return RightHandCloserToLaryngoscopeThanLeft(false);
                    }
                }
                else
                {
                    // Distance returned by KinectManager.Distance3D( SkeletalPoint a, SkeletalPoint b) is in SkeletalSpace, and therefore is represented in Meters.
                    rightHandDistance = System.Math.Abs(KinectManager.Distance2D(laryngoscopeInSkeletalSpace, rightHandLoc));     // Check the distance between the manikinChin in SkeletalSpace and the right hand. Absolute value is used to negate which point is used as the base.
                    leftHandDistance = System.Math.Abs(KinectManager.Distance2D(laryngoscopeInSkeletalSpace, leftHandLoc));     // Check the distance between the manikinChin in SkeletalSpace and the right hand. Absolute value is used to negate which point is used as the base.

                    System.Diagnostics.Debug.WriteLine(String.Format("2D: Laryngoscope -> Right Hand Distance: {0:#}pixels", rightHandDistance * 1000));
                    System.Diagnostics.Debug.WriteLine(String.Format("2D: Laryngoscope -> Left Hand Distance: {0:#}pixels", leftHandDistance * 1000));
                }

                if (rightHandDistance < leftHandDistance)   // if the Right Hand is closer (less than) the Left Hand to the Syringe
                    return true;
            }

            return false;
        }

        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }

        /*

        public override void EndStage( bool playSound = true )
        {
            this.Stopwatch.Stop();

            this.TimeoutStopwatch.Stop();

            // Get the current time to signify the time the stage ended.
            EndTime = DateTime.Now;

            StageScore stagescore = new StageScore();

            // Create a screenshot for the current stage.

            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                StageScore ss = (StageScore)args.Argument;

                //do something
                ss.ScreenShot = KinectManager.CreateScreenshot();
                // Set the hasScreenshot variable if the screenshot was created successfully.
                if (ss.ScreenShot == null)
                    ss.HasScreenshot = false;
                else ss.HasScreenshot = true;

            };

            worker.RunWorkerAsync(stagescore);

            stagescore.StageNumber = this.StageNumber;

            stagescore.StageName = this.Name;

            CalculateStage2Score(stagescore);

            // store how much time was spent in this stage.
            stagescore.TimeInStage = this.Stopwatch.Elapsed;

            stagescore.Target = this.Instruction;

            this.StageScore = stagescore;

            //this.Lesson.Result.StageScores.Add(stagescore);    // Add the stagescore to the collection of StageScore objects for this Lesson's Result.

            this.Lesson.CurrentStageIndex++;

            if( playSound ) Lesson.PlayNextStageSound();
        }*/

        public override void EndStage( bool playSound = true )
        {
            this.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            // Get the current time to signify the time the stage ended.
            this.EndTime = DateTime.Now;

            // Create a screenshot for the current stage.
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                Intubation lesson = (Intubation)args.Argument;

                KinectImage image = KinectImage.TakeKinectSnapshot(true, true);

                lesson.GrabLaryngoscopeImage = image;
            };

            worker.RunWorkerAsync(this.Lesson);

            AssessLaryngoscopeGrab();

            this.Lesson.CurrentStageIndex++;

            if( playSound ) Lesson.PlayNextStageSound();
        }

        private void AssessLaryngoscopeGrab()
        {
            double score = 1;

            // Laryngoscope found in the correct position.
            if (leftHandByTool)
            {
                // Laryngoscope grabbed with the correct hand.
                this.Lesson.GrabLaryngoscopeFeedbackIDs.Add(28); // You have grabbed the Laryngoscope with the correct hand.
            }
            else if (rightHandByTool)
            {
                score -= 0.5;

                // Laryngoscope grabbed with the incorrect hand.
                this.Lesson.GrabLaryngoscopeFeedbackIDs.Add(29); // You have grabbed the Laryngoscope with the incorrect hand.
            }
            else
            {
                // Unable to ascertain which hand is grabbing the laryngoscope.
                this.Lesson.GrabLaryngoscopeFeedbackIDs.Add(33);
            }

            // Laryngoscope was grabbed.
            if (toolLeftSide)
            {
                // Tool picked up on the left side.
                this.Lesson.GrabLaryngoscopeFeedbackIDs.Add(30); // The Laryngoscope was picked up on the correct side.
            }
            else if (toolRightSide)
            {
                score -= 0.5;
                // Tool picked up on the right side.
                this.Lesson.GrabLaryngoscopeFeedbackIDs.Add(31); // The Laryngoscope was picked up on the incorrect side of the patient's head.
            }
            else
            {
                // Unable to ascertain which side the laryngoscope was grabbed on.
                this.Lesson.GrabLaryngoscopeFeedbackIDs.Add(32);
            }

            this.Lesson.GrabLaryngoscopeScore = System.Math.Max(0, System.Math.Min(score, 1)); // Clamp the score between 0 and 1.
        }

        public override void PauseStage()
        {
            this.Lesson.Paused = true;
            this.Lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.Lesson.Paused = false;
            this.Lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void ResetStage()
        {

        }

        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            //this.PauseStage();
            this.TimedOut = true;
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            this.RequestStageVideo();   // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
        }

        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.Lesson.IntubationPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Intubation/Videos/Stage 002.wmv", UriKind.Relative));
            this.Lesson.IntubationPage.VideoPlayer.startVideo(this, new RoutedEventArgs());

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        public override void WipeClean()
        {
            toolIsAboveMouth = false;
            aboveMouthZone = null;
            watcher_FoundLaryngoscopePos = new Point3D();
            watcher_YellowCounter = 0;
            watcher_YellowDepthCounter = 0;

            rightHandByTool = false;
            leftHandByTool = false;
            toolLeftSide = false;
            toolRightSide = false;

            toolInsertedRight = false;
            toolInsertedLeft = false;

            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.IsSetup = false;

            if (this.StageScore != null)
            {
                this.StageScore.WipeClean();
                this.StageScore = null;
                this.StageScore = new StageScore();
            }
            else
            {
                this.StageScore = new StageScore();
            }

            this.Active = false;
            this.Complete = false;

            if (this.Zones != null)
            {
                this.Zones.Clear();
            }
            else
            {
                this.Zones = new List<Zone>();
            }

            if (this.Stopwatch != null)
            {
                this.Stopwatch.Reset();
            }
            else
            {
                this.Stopwatch = new System.Diagnostics.Stopwatch();
            }

            if (this.TimeoutStopwatch != null)
                this.TimeoutStopwatch.Reset();
            else this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.TimedOut = false;
        }
    }
}
