﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using System.Windows.Media.Imaging;
using System.Windows;
using System.ComponentModel;
using System.Diagnostics;

using Microsoft.Kinect;

using AIMS.Assets.Lessons2;

namespace AIMS.Tasks.Intubation
{
    public class IntubationStage3 : Stage
    {
        private Intubation lesson;
        public Intubation Lesson { get { return lesson; } set { lesson = value; } }

        private bool laryngoscopeInserted = false;
        private RecZone properInsertionZone = null;
        private RecZone upperTeethForceZone = null;
        private RecZone lowerTeethForceZone = null;
        private RecZone midLeftForceZone = null;
        private RecZone midRightForceZone = null;
        private Point3D currentLaryngoscopePosition = new Point3D();
        private bool laryngoscopeInsertedProperly = false;
        private bool laryngoscopeDamagedUpperTeeth = false;
        private bool laryngoscopeDamagedLowerTeeth = false;
        private bool laryngoscopeInsertedRight = false;
        private bool laryngoscopeInsertedLeft = false;
        private int yellowCounter = 0;
        private bool hasFoundLaryngoscopeLocation = false;
        private int yellowDepthCounter = 0;

        //private PersistentColorTracker PersistentColorTracker;

        public bool LaryngoscopeInserted { get { return laryngoscopeInserted; } set { laryngoscopeInserted = value; } }
        public RecZone ProperInsertionZone { get { return properInsertionZone; } set { properInsertionZone = value; } }
        public RecZone UpperTeethForceZone { get { return upperTeethForceZone; } set { upperTeethForceZone = value; } }
        public RecZone LowerTeethForceZone { get { return lowerTeethForceZone; } set { lowerTeethForceZone = value; } }
        public RecZone MidLeftForceZone { get { return midLeftForceZone; } set { midLeftForceZone = value; } }
        public RecZone MidRightForceZone { get { return midRightForceZone; } set { midRightForceZone = value; } }
        public Point3D CurrentLaryngoscopePosition { get { return currentLaryngoscopePosition; } set { currentLaryngoscopePosition = value; } }
        public bool LaryngoscopeInsertedProperly { get { return laryngoscopeInsertedProperly; } set { laryngoscopeInsertedProperly = value; } }
        public bool LaryngoscopeDamagedUpperTeeth { get { return laryngoscopeDamagedUpperTeeth; } set { laryngoscopeDamagedUpperTeeth = value; } }
        public bool LaryngoscopeDamagedLowerTeeth { get { return laryngoscopeDamagedLowerTeeth; } set { laryngoscopeDamagedLowerTeeth = value; } }
        public bool LaryngoscopeInsertedRight { get { return laryngoscopeInsertedRight; } set { laryngoscopeInsertedRight = value; } }
        public bool LaryngoscopeInsertedLeft { get { return laryngoscopeInsertedLeft; } set { laryngoscopeInsertedLeft = value; } }
        public int YellowCounter { get { return yellowCounter; } set { yellowCounter = value; } }
        public bool HasFoundLaryngoscopeLocation { get { return hasFoundLaryngoscopeLocation; } set { hasFoundLaryngoscopeLocation = value; } }
        public int YellowDepthCounter { get { return yellowDepthCounter; } set { yellowDepthCounter = value; } }

        private Watcher laryngoscopeDetectionWatcher;

        private ColorRange _laryngoscopeColor;

        public IntubationStage3(Intubation intubation)
        {
            Lesson = intubation;

            StageNumber = 3;
            Name = "Insert Laryngoscope";
            Instruction = "Insert the laryngoscope into the right side of the mouth. Make sure not to hit the teeth.";
            TargetTime = TimeSpan.FromSeconds(3.5);
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Tasks/Intubation/Graphics/Intubation003.png";
            Active = false;
            Complete = false;
            ScoreWeight = 0.20;         // ***TWF*** - Dropped score to accomodate adding Intubation Stages 10 & 11
            Zones = new List<Zone>();
            IsSetup = false;
            Stopwatch = new System.Diagnostics.Stopwatch();
            this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.CanTimeOut = false;
            this.TimeOutTime = TimeSpan.FromSeconds(10.0);

            this.CurrentStageWatchers = new List<BaseWatcher>();
            this.PeripheralStageWatchers = new List<BaseWatcher>();
            this.OutlyingStageWatchers = new List<BaseWatcher>();

            CreateStageWatchers();
        }

        public override void SetupStage()
        {
            System.Diagnostics.Debug.WriteLine("IntubationStage3 -> SetupStage()");

            this.Lesson.IntubationPage.StageInfoTextBlock.Text = this.Name;
            this.Lesson.IntubationPage.InstructionTextBlock.Text = this.Instruction;

            this.Lesson.IntubationPage.flow.Index = this.Lesson.CurrentStageIndex;  // change cover flow image.

            // Set the image for the current stage.
            this.Lesson.IntubationPage.CurrentStageImage.Source = new BitmapImage(new Uri("/Tasks/Intubation/Graphics/Intubation003.png", UriKind.Relative));
            //this.Lesson.IntubationPage.CurrentStageImage.Source = new BitmapImage(new Uri("/Tasks/Intubation/Graphics/Intubation_Stage003_640.jpg", UriKind.Relative));

            try
            {
                this._laryngoscopeColor = ColorTracker.GetObjectColorRange("laryngoscope").ColorRange;
            }
            catch (Exception exc) { Debug.Print("IntubationStage3 SetupStage() ERROR: {0}", exc.Message); }

            this.Zones.Clear(); // Clear the stage zone list.

            this.Lesson.IntubationPage.zoneOverlay.ClearZones();


            /*  _________
             * |    -    |   // Mid-Left zone = Improper Posture zone
             * |_________|   // Mid-Right zone = Improper Posture zone
             * |  |   |  |   // Upper zone = Excessive Upper Force Zone
             * |- | + | -|   // Center Zone = Proper Insertion Zone
             * |__|___|__|   // Lower zone = Excessive Lower Force Zone
             * |    -    |
             * |_________|
             */

            // Set the current laryngoscope point to the last point it was found at in stage2
            this.CurrentLaryngoscopePosition = this.Lesson.LaryngoscopeLocation;

            #region Zone Setup

            // These pixels are in depth space... but are now color space
            int scale_x = Intubation.SCALE_X;
            int scale_y = Intubation.SCALE_Y;

            // The ManikinFaceLocation must be changed from Point3D to ColorPoint
            CameraSpacePoint manikinFace = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };
            ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(manikinFace);

            #region Mid Left Bad Zone
            MidLeftForceZone = new RecZone();
            MidLeftForceZone.X = colorSpacePoint.X - (50 * scale_x);
            MidLeftForceZone.Y = colorSpacePoint.Y - (60 * scale_y);
            MidLeftForceZone.Z = 0;
            MidLeftForceZone.Name = "Mid Left Bad Zone";
            //MidLeftForceZone.ZoneType = ZoneType.Force;
            MidLeftForceZone.Width = 30 * scale_x;
            MidLeftForceZone.Height = 40 * scale_y;
            MidLeftForceZone.Depth = 0;
            MidLeftForceZone.DoesTrackJoint = false;  // This zone is not used for joint position, so this flag is set.
            MidLeftForceZone.IsImproperZone = true;
            #endregion

            #region Mid Right Bad Zone
            MidRightForceZone = new RecZone();
            MidRightForceZone.X = colorSpacePoint.X;
            MidRightForceZone.Y = colorSpacePoint.Y - (60 * scale_y);
            MidRightForceZone.Z = 0;
            MidRightForceZone.Name = "Mid Right Bad Zone";
            //MidRightForceZone.ZoneType = ZoneType.Force;
            MidRightForceZone.Width = 10 * scale_x;
            MidRightForceZone.Height = 40 * scale_y;
            MidRightForceZone.Depth = 0;
            MidRightForceZone.DoesTrackJoint = false;  // This zone is not used for joint position, so this flag is set.
            MidRightForceZone.IsImproperZone = true;
            #endregion

            #region Upper Teeth Force Zone
            UpperTeethForceZone = new RecZone();
            UpperTeethForceZone.X = colorSpacePoint.X - (30 * scale_x);
            UpperTeethForceZone.Y = colorSpacePoint.Y - (60 * scale_y);
            UpperTeethForceZone.Z = 0;
            UpperTeethForceZone.Name = "Upper Teeth Force Zone";
            //UpperTeethForceZone.ZoneType = ZoneType.Force;
            UpperTeethForceZone.Width = 40 * scale_x;
            UpperTeethForceZone.Height = 20 * scale_y;
            UpperTeethForceZone.Depth = 0;
            UpperTeethForceZone.DoesTrackJoint = false;  // This zone is not used for joint position, so this flag is set.
            UpperTeethForceZone.IsImproperZone = true;
            #endregion

            #region Proper Insertion Zone
            ProperInsertionZone = new RecZone();
            ProperInsertionZone.X = colorSpacePoint.X - (20 * scale_x);
            ProperInsertionZone.Y = colorSpacePoint.Y - (40 * scale_y);
            ProperInsertionZone.Z = 0;
            ProperInsertionZone.Name = "Proper Insertion Zone";
            //ProperInsertionZone.ZoneType = ZoneType.Position;
            ProperInsertionZone.Width = 20 * scale_x;
            ProperInsertionZone.Height = 20 * scale_y;
            ProperInsertionZone.Depth = 0;
            ProperInsertionZone.DoesTrackJoint = false;  // This zone is not used for joint position, so this flag is set.
            ProperInsertionZone.IsImproperZone = false;
            #endregion

            #region Lower Teeth Force Zone
            LowerTeethForceZone = new RecZone();
            LowerTeethForceZone.X = colorSpacePoint.X - (30 * scale_x);
            LowerTeethForceZone.Y = colorSpacePoint.Y - (20 * scale_y);
            LowerTeethForceZone.Z = 0;
            LowerTeethForceZone.Name = "Lower Teeth Force Zone";
            //LowerTeethForceZone.ZoneType = ZoneType.Force;
            LowerTeethForceZone.Width = 40 * scale_x;
            LowerTeethForceZone.Height = 10 * scale_y;
            LowerTeethForceZone.Depth = 0;
            LowerTeethForceZone.DoesTrackJoint = false;  // This zone is not used for joint position, so this flag is set.
            LowerTeethForceZone.IsImproperZone = true;
            #endregion

            Zones.Add(ProperInsertionZone);
            Zones.Add(UpperTeethForceZone);
            Zones.Add(LowerTeethForceZone);
            Zones.Add(MidLeftForceZone);
            Zones.Add(MidRightForceZone);

            #endregion

            // Add the zones into the zoneOverlay
            foreach (Zone zone in this.Zones)
            {
                this.Lesson.IntubationPage.zoneOverlay.AddZone(zone);
            }

            yellowCounter = 0;
            yellowDepthCounter = 0;

            StartTime = DateTime.Now;

            this.IsSetup = true;

            Stopwatch.Start();

            TimeoutStopwatch.Start();
        }

        public void CreateStageWatchers()
        {
            this.CurrentStageWatchers.Clear();
            this.PeripheralStageWatchers.Clear();
            this.OutlyingStageWatchers.Clear();

            laryngoscopeDetectionWatcher = new Watcher();

            #region WatcherLogic

            laryngoscopeDetectionWatcher.CheckForActionDelegate = () =>
            {
                if (this.Lesson.CurrentStageIndex < 2 || this.Lesson.CurrentStageIndex > 8)
                    return;

                // TREY: USING FUNCTIONS
                ColorSpacePoint colorSpacePoint = Watcher.ColorSpaceFromPoint3D(this.Lesson.ManikinFaceLocation);
                Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, -50, -80, 80, 60);
                Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._laryngoscopeColor,
                                                                        rect,
                                                                        KinectManager.ColorData);

                //CameraSpacePoint cameraSpacePoint = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };
                //ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(cameraSpacePoint);
                //Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._laryngoscopeColor, 
                //                                                        new Int32Rect(  (int)System.Math.Max(0, colorSpacePoint.X - 50), 
                //                                                                        (int)System.Math.Max(0, colorSpacePoint.Y - 80), 
                //                                                                        80, 
                //                                                                        60), 
                //                                                        KinectManager.ColorData);

                if (!double.IsNaN(found.X) && !double.IsNaN(found.Y))
                {
                    int depth = KinectManager.GetDepthValueForColorPoint(found);
                    if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                    {
                        Point3D loc = KinectManager.Get3DPointForColorPoint(found, depth);

                        this.Lesson.LaryngoscopeLocation = loc;

                        laryngoscopeDetectionWatcher.IsTriggered = true;
                    }
                    else
                    {
                        laryngoscopeDetectionWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    laryngoscopeDetectionWatcher.IsTriggered = false;
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(laryngoscopeDetectionWatcher);
            this.PeripheralStageWatchers.Add(laryngoscopeDetectionWatcher);
            this.OutlyingStageWatchers.Add(laryngoscopeDetectionWatcher);
        }

        public override void UpdateStage()
        {
            if (!this.IsSetup)
                SetupStage();

            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
                this.TimeoutStage();

            if (KinectManager.ClosestSkeletonProfile == null)
                return;

            if (this.CurrentStageWatchers[0].IsTriggered)
            {
                // Convert the 3DPosition CameraSpace to 2DPosition ColorSpace
                CameraSpacePoint laryngoscope = new CameraSpacePoint() { X = (float)this.Lesson.LaryngoscopeLocation.X, Y = (float)this.Lesson.LaryngoscopeLocation.Y, Z = (float)this.Lesson.LaryngoscopeLocation.Z / 1000 };
                ColorSpacePoint colorPoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(laryngoscope);
                Point3D colorPoint3D = new Point3D(colorPoint.X, colorPoint.Y, laryngoscope.Z * 1000);

                // Update the zones with the found laryngoscope point
                ProperInsertionZone.UpdatePointWithinZone(colorPoint3D);
                UpperTeethForceZone.UpdatePointWithinZone(colorPoint3D);
                LowerTeethForceZone.UpdatePointWithinZone(colorPoint3D);
                MidLeftForceZone.UpdatePointWithinZone(colorPoint3D);
                MidRightForceZone.UpdatePointWithinZone(colorPoint3D);
            }
            else
            {
                // Laryngoscope wasn't found. Update the zones accordingly by turning them off.
                Point3D p = new Point3D();

                ProperInsertionZone.UpdatePointWithinZone(p);
                UpperTeethForceZone.UpdatePointWithinZone(p);
                LowerTeethForceZone.UpdatePointWithinZone(p);
                MidLeftForceZone.UpdatePointWithinZone(p);
                MidRightForceZone.UpdatePointWithinZone(p);
            }

            // Refresh the zone overlay.
            this.Lesson.IntubationPage.zoneOverlay.UpdateZones();

            // Check if the tool goes into improper zones.
            if (ProperInsertionZone.IsWithinZone && ProperInsertionZone.TotalTimeWithinZone >= TimeSpan.FromMilliseconds(700.0))
            {
                if (!this.LaryngoscopeInserted)
                {
                    LaryngoscopeInserted = true;
                    this.Lesson.DetectableActions.DetectAction("IntubationStage3-> Inserted Right Side", "Insert", "Laryngoscope", true);
                }
                if (!this.LaryngoscopeInsertedProperly)
                {
                    LaryngoscopeInsertedProperly = true;

                    System.Diagnostics.Debug.WriteLine("IntubationStage3-> The Laryngoscope has been Inserted Properly.");
                    this.Lesson.DetectableActions.UndetectAction("Inserted Right Side", "Insert", "Laryngoscope", false);
                }
            }
            else if (UpperTeethForceZone.IsWithinZone && UpperTeethForceZone.TotalTimeWithinZone >= TimeSpan.FromMilliseconds(700.0))
            {
                LaryngoscopeInserted = true;

                LaryngoscopeDamagedUpperTeeth = true;

                System.Diagnostics.Debug.WriteLine("Laryngoscope Inserted Inproperly. Potential Damage to the Upper Teeth.");
                this.Lesson.DetectableActions.DetectAction("Fulcruming On Teeth", "Lift", "Laryngoscope", true);
            }
            else if (LowerTeethForceZone.IsWithinZone && LowerTeethForceZone.TotalTimeWithinZone >= TimeSpan.FromMilliseconds(700.0))
            {
                LaryngoscopeInserted = true;
                LaryngoscopeDamagedLowerTeeth = true;

                System.Diagnostics.Debug.WriteLine("Laryngoscope Inserted Inproperly. Potential Damage to the Lower Teeth.");
            }
            else if (MidLeftForceZone.IsWithinZone && MidLeftForceZone.TotalTimeWithinZone >= TimeSpan.FromMilliseconds(700.0))
            {
                LaryngoscopeInserted = true;
                LaryngoscopeInsertedLeft = true;

                System.Diagnostics.Debug.WriteLine("Laryngoscope Inserted Inproperly. Left.");

                this.Lesson.DetectableActions.DetectAction("Inserted Left Side", "Insert", "Laryngoscope", true);
            }
            else if (MidRightForceZone.IsWithinZone && MidRightForceZone.TotalTimeWithinZone >= TimeSpan.FromMilliseconds(700.0))
            {
                LaryngoscopeInserted = true;
                LaryngoscopeInsertedRight = true;

                System.Diagnostics.Debug.WriteLine("Laryngoscope Inserted Inproperly. Right.");
            }

            // Check for stage end (laryngoscope has been inserted.
            if (LaryngoscopeInserted)
            {
                this.Complete = true;
                this.EndStage();
            }
        }

        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }


        public override void EndStage( bool playSound = true )
        {
            this.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            // Get the current time to signify the time the stage ended.
            this.EndTime = DateTime.Now;

            // Create a screenshot for the current stage.
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                Intubation lesson = (Intubation)args.Argument;

                KinectImage image = KinectImage.TakeKinectSnapshot(true, true);

                lesson.InsertLaryngoscopeImage = image;
            };

            worker.RunWorkerAsync(this.Lesson);

            AssessLaryngoscopeInsertion();

            this.Lesson.CurrentStageIndex++;

            if( playSound ) Lesson.PlayNextStageSound();
        }

        private void AssessLaryngoscopeInsertion()
        {
            double score = 1;

            if (this.LaryngoscopeInsertedProperly)
            {
                this.Lesson.InsertLaryngoscopeFeedbackIDs.Add(34);   // You have properly inserted the Laryngoscope.
            }
            else
            {
                this.Lesson.InsertLaryngoscopeFeedbackIDs.Add(35);   // You have improperlly inserted the Laryngoscope.

                if (this.LaryngoscopeDamagedUpperTeeth)
                {
                    score -= 0.33;
                    this.Lesson.InsertLaryngoscopeFeedbackIDs.Add(36);   // The Layngoscope potentially damaged the upper teeth.
                }
                else
                {
                    score -= 0.33;
                    this.Lesson.InsertLaryngoscopeFeedbackIDs.Add(37);   // The Laryngoscope potentially damaged the lower teeth.
                }

                if (this.LaryngoscopeInsertedRight)
                {
                    score -= 0.33;
                    this.Lesson.InsertLaryngoscopeFeedbackIDs.Add(38);   // The Laryngscope was inserted incorrectly to the right of the patient's mouth.
                }

                if (this.LaryngoscopeInsertedLeft)
                {
                    score -= 0.33;
                    this.Lesson.InsertLaryngoscopeFeedbackIDs.Add(39);   // The Laryngoscope was inserted incorrectly to the left of the patient's mouth.
                }
            }

            this.Lesson.InsertLaryngoscopeScore = System.Math.Max(0, System.Math.Min(score, 1)); // Clamp the score between 0 and 1.
        }

        public override void PauseStage()
        {
            this.Lesson.Paused = true;
            this.Lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.Lesson.Paused = false;
            this.Lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void ResetStage()
        {

        }

        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            //this.PauseStage();
            this.TimedOut = true;
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            this.RequestStageVideo();   // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
        }

        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.Lesson.IntubationPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Intubation/Videos/Stage 003.wmv", UriKind.Relative));
            this.Lesson.IntubationPage.VideoPlayer.startVideo( this, new RoutedEventArgs() );

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        public override void WipeClean()
        {
            laryngoscopeInserted = false;
            properInsertionZone = null;
            upperTeethForceZone = null;
            lowerTeethForceZone = null;
            midLeftForceZone = null;
            midRightForceZone = null;
            currentLaryngoscopePosition = new Point3D();
            laryngoscopeInsertedProperly = false;
            laryngoscopeDamagedUpperTeeth = false;
            laryngoscopeDamagedLowerTeeth = false;
            laryngoscopeInsertedRight = false;
            laryngoscopeInsertedLeft = false;
            yellowCounter = 0;
            hasFoundLaryngoscopeLocation = false;
            yellowDepthCounter = 0;
            laryngoscopeDetectionWatcher = null;

            CreateStageWatchers();

            /*if (this.Lesson.PersistentColorTracker != null)
                this.Lesson.PersistentColorTracker.WipeClean();*/

            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.IsSetup = false;

            if (this.StageScore != null)
            {
                this.StageScore.WipeClean();
                this.StageScore = null;
                this.StageScore = new StageScore();
            }
            else
            {
                this.StageScore = new StageScore();
            }

            this.Active = false;
            this.Complete = false;

            if (this.Zones != null)
            {
                this.Zones.Clear();
            }
            else
            {
                this.Zones = new List<Zone>();
            }

            if (this.Stopwatch != null)
            {
                this.Stopwatch.Reset();
            }
            else
            {
                this.Stopwatch = new System.Diagnostics.Stopwatch();
            }

            if (this.TimeoutStopwatch != null)
                this.TimeoutStopwatch.Reset();
            else this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();

            this.TimedOut = false;
        }
    }
}
