﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using System.Windows.Media.Imaging;
using Microsoft.Kinect;
using System.Windows;
using System.ComponentModel;

namespace AIMS.Tasks.Intubation
{
    public class IntubationStage4 : Stage
    {
        private Intubation lesson;
        public Intubation Lesson { get { return lesson; } set { lesson = value; } }

        private RecZone userHeadBendingDownZone = null;
        private bool userBentDown = false;

        public RecZone UserHeadBendingDownZone { get { return userHeadBendingDownZone; } set { userHeadBendingDownZone = value; } }
        public bool UserBentDown { get { return userBentDown; } set { userBentDown = value; } }

        public IntubationStage4(Intubation intubation)
        {
            Lesson = intubation;

            StageNumber = 4;
            Name = "View Vocal Cords";
            Instruction = "Bend down to view the vocal cords.";
            TargetTime = TimeSpan.FromSeconds(3.5);
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Tasks/Intubation/Graphics/Intubation004.png";
            Active = false;
            Complete = false;
            ScoreWeight = 0.10;
            Zones = new List<Zone>();
            IsSetup = false;
            Stopwatch = new System.Diagnostics.Stopwatch();
            this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.CanTimeOut = false;
            this.TimeOutTime = TimeSpan.FromSeconds(10.0);
        }

        public override void SetupStage()
        {
            this.Lesson.IntubationPage.StageInfoTextBlock.Text = this.Name;
            this.Lesson.IntubationPage.InstructionTextBlock.Text = this.Instruction;

            this.Lesson.IntubationPage.flow.Index = this.Lesson.CurrentStageIndex;  // change cover flow image.

            // Set the image for the current stage.

            this.Lesson.IntubationPage.CurrentStageImage.Source = new BitmapImage(new Uri("/Tasks/Intubation/Graphics/Intubation004.png", UriKind.Relative));

            this.Zones.Clear(); // Clear the stage zone list.

            this.Lesson.IntubationPage.zoneOverlay.ClearZones();

            // The ManikinFaceLocation must be changed from Point3D to ColorPoint
            CameraSpacePoint cameraSpacePoint = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };
            ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(cameraSpacePoint);

            // These pixels are in depth space... but are now color space
            int scale_x = Intubation.SCALE_X;
            int scale_y = Intubation.SCALE_Y;

            UserHeadBendingDownZone = new RecZone();
            UserHeadBendingDownZone.X = colorSpacePoint.X - (50 * scale_x);
            UserHeadBendingDownZone.Y = colorSpacePoint.Y - (140 * scale_y);
            //UserHeadBendingDownZone.Z = manikinFaceLocation.Z;
            UserHeadBendingDownZone.Name = "User Head Bending Down Zone";
            //UserHeadBendingDownZone.ZoneType = ZoneType.Position;
            UserHeadBendingDownZone.Width = 100 * scale_x;
            UserHeadBendingDownZone.Height = 75 * scale_y;
            UserHeadBendingDownZone.Depth = 0;
            UserHeadBendingDownZone.DoesTrackJoint = true;
            UserHeadBendingDownZone.AcceptedJointType = Microsoft.Kinect.JointType.Head;

            this.Zones.Add(UserHeadBendingDownZone);

            // Add the zones into the zoneOverlay
            foreach (Zone zone in this.Zones)
            {
                this.Lesson.IntubationPage.zoneOverlay.AddZone(zone);
            }

            this.CurrentStageWatchers = new List<BaseWatcher>();
            this.PeripheralStageWatchers = new List<BaseWatcher>();
            this.OutlyingStageWatchers = new List<BaseWatcher>();

            CreateStageWatchers();

            this.StartTime = DateTime.Now;

            this.IsSetup = true;

            this.Stopwatch.Start();

            this.TimeoutStopwatch.Start();
        }

        private void CreateStageWatchers()
        {
            // Clear the list of watchers.
            this.CurrentStageWatchers.Clear();
            this.PeripheralStageWatchers.Clear();
            this.OutlyingStageWatchers.Clear();

            // Add in new watchers.
            Watcher userBendsDownWatcher = new Watcher();

            #region WatcherLogic

            // Checks for the user to bend down.
            userBendsDownWatcher.CheckForActionDelegate = () =>
            {
                bool userBentDown = false;

                CameraSpacePoint manikinChinInSkeletalSpace;
                CameraSpacePoint headLoc;
                double headDistance;

                manikinChinInSkeletalSpace = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };

                SkeletonProfile skele = null;

                if (this.Lesson.StuckSkeletonProfileID.HasValue)
                {
                    // Retreive
                    skele = KinectManager.SingleStickySkeletonProfile;
                }
                if (skele != null)
                {
                    // Get the right hand and right wrist joint positions out of the profile.
                    headLoc = skele.JointProfiles.First(o => o.JointType == JointType.Head).CurrentPosition;

                    // Distance returned by KinectManager.Distance3D( SkeletalPoint a, SkeletalPoint b) is in SkeletalSpace, and therefore is represented in Meters.
                    headDistance = System.Math.Abs(KinectManager.Distance3D(manikinChinInSkeletalSpace, headLoc));     // Check the distance between the manikinChin in SkeletalSpace and the right hand. Absolute value is used to negate which point is used as the base.

                    System.Diagnostics.Debug.WriteLine(String.Format("IntubationStage4 -> Head to chin: {0:#}mm", headDistance * 1000));

                    double desiredMaxDistance = 0.7;  // meters, previously 0.515

                    if (headDistance <= desiredMaxDistance)
                    {
                        // Distance between the user's head and the manikin's chin is within the desiredMaxDistance.
                        userBentDown = true;
                    }
                }

                if (userBentDown)
                {
                    userBendsDownWatcher.IsTriggered = true;
                }
                else
                {
                    userBendsDownWatcher.IsTriggered = false;
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(userBendsDownWatcher);
        }

        public override void UpdateStage()
        {
            if (!this.IsSetup)
                SetupStage();

            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
                this.TimeoutStage();

            if (KinectManager.ClosestSkeletonProfile == null)
                return;

            if (this.CurrentStageWatchers != null)
            {
                if (this.CurrentStageWatchers[0].IsTriggered)
                {
                    if (!this.UserBentDown)
                    {
                        // User is bending down with their head near the manikin's chin.
                        this.UserBentDown = true;
                        System.Diagnostics.Debug.WriteLine("The user is bending down.");

                        this.Lesson.DetectableActions.DetectAction("Vocal Cords Viewed", "Bend Down", "User", true);
                    }
                }
            }

            if (UserBentDown)
            {
                this.Complete = true;
                EndStage();
            }
        }

        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }


        public override void EndStage( bool playSound = true )
        {
            this.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            // Get the current time to signify the time the stage ended.
            this.EndTime = DateTime.Now;

            // Create a screenshot for the current stage.
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                Intubation lesson = (Intubation)args.Argument;

                KinectImage image = KinectImage.TakeKinectSnapshot(true, true);

                lesson.ViewVocalChordsImage = image;
            };

            worker.RunWorkerAsync(this.Lesson);

            AssessVocalChordViewing();

            this.Lesson.CurrentStageIndex++;

            if( playSound ) Lesson.PlayNextStageSound();
        }

        private void AssessVocalChordViewing()
        {
            double score = 1;

            if (UserBentDown)
            {
                this.Lesson.ViewVocalChordsFeedbackIDs.Add(40);  // The vocal chords were viewed correctly.
            }
            else
            {
                score -= 1;
                this.Lesson.ViewVocalChordsFeedbackIDs.Add(41);  // The vocal chords were not viewed.
            }

            this.Lesson.ViewVocalChordsScore = System.Math.Max(0, System.Math.Min(score, 1)); // Clamp the score between 0 and 1.
        }

        public override void PauseStage()
        {
            this.Lesson.Paused = true;
            this.Lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.Lesson.Paused = false;
            this.Lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void ResetStage()
        {

        }

        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            //this.PauseStage();
            this.TimedOut = true;
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            this.RequestStageVideo();   // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
        }

        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.Lesson.IntubationPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Intubation/Videos/Stage 004.wmv", UriKind.Relative));
            this.Lesson.IntubationPage.VideoPlayer.startVideo(this, new RoutedEventArgs());

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        public override void WipeClean()
        {
            this.UserHeadBendingDownZone = null;
            this.UserBentDown = false;

            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.IsSetup = false;

            if (this.StageScore != null)
            {
                this.StageScore.WipeClean();
                this.StageScore = null;
                this.StageScore = new StageScore();
            }
            else
            {
                this.StageScore = new StageScore();
            }

            this.Active = false;
            this.Complete = false;

            if (this.Zones != null)
            {
                this.Zones.Clear();
            }
            else
            {
                this.Zones = new List<Zone>();
            }

            if (this.Stopwatch != null)
            {
                this.Stopwatch.Reset();
            }
            else
            {
                this.Stopwatch = new System.Diagnostics.Stopwatch();
            }

            if (this.TimeoutStopwatch != null)
                this.TimeoutStopwatch.Reset();
            else this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();

            this.TimedOut = false;
        }
    }
}
