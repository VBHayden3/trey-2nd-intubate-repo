﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using System.Windows.Media.Imaging;
using System.Windows;
using System.ComponentModel;
using System.Diagnostics;

using Microsoft.Kinect;

using AIMS.Assets.Lessons2;

namespace AIMS.Tasks.Intubation
{
    public class IntubationStage5 : Stage
    {
        private Intubation lesson;
        public Intubation Lesson { get { return lesson; } set { lesson = value; } }

        private bool toolIsAboveMouth = false;
        private bool toolisGrabbed = false;

        private int watcher_PinkCounter = 0;
        private int watcher_PinkDepthCounter = 0;
        private Point3D watcher_FoundEndotrachealTubePos = new Point3D();

        public bool ToolIsAboveMouth { get { return toolIsAboveMouth; } set { toolIsAboveMouth = value; } }
        public bool ToolIsGrabbed { get { return toolisGrabbed; } set { toolisGrabbed = value; } }

        private bool toolGrabbedRightHand = false;
        private bool toolGrabbedLeftHand = false;

        private ColorRange _intubationTubeColor;

        public IntubationStage5(Intubation intubation)
        {
            Lesson = intubation;

            StageNumber = 5;
            Name = "Grab Endotracheal Tube";
            Instruction = "Pick up the endotracheal tube with your right hand.";
            TargetTime = TimeSpan.FromSeconds(3.5);
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Tasks/Intubation/Graphics/Intubation005.png";
            Active = false;
            Complete = false;
            ScoreWeight = 0.05;
            Zones = new List<Zone>();
            IsSetup = false;
            Stopwatch = new System.Diagnostics.Stopwatch();
            this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.CanTimeOut = false;
            this.TimeOutTime = TimeSpan.FromSeconds(10.0);
            this.CurrentStageWatchers = new List<BaseWatcher>();
            this.PeripheralStageWatchers = new List<BaseWatcher>();
            this.OutlyingStageWatchers = new List<BaseWatcher>();

            CreateStageWatchers();
        }

        public override void SetupStage()
        {
            this.Lesson.IntubationPage.StageInfoTextBlock.Text = this.Name;
            this.Lesson.IntubationPage.InstructionTextBlock.Text = this.Instruction;

            this.Lesson.IntubationPage.flow.Index = this.Lesson.CurrentStageIndex;  // change cover flow image.

            // Set the image for the current stage.
            this.Lesson.IntubationPage.CurrentStageImage.Source = new BitmapImage(new Uri("/Tasks/Intubation/Graphics/Intubation005.png", UriKind.Relative));
            //this.Lesson.IntubationPage.CurrentStageImage.Source = new BitmapImage(new Uri("/Tasks/Intubation/Graphics/Intubation_Stage005_640.jpg", UriKind.Relative));

            try
            {
                this._intubationTubeColor = ColorTracker.GetObjectColorRange("intubation_tube").ColorRange;
            }
            catch (Exception exc) { Debug.Print("IntubationStage5 SetupStage() ERROR: {0}", exc.Message); }

            this.Zones.Clear(); // Clear the stage zone list.

            this.Lesson.IntubationPage.zoneOverlay.ClearZones();

            // Add the zones into the zoneOverlay
            foreach (Zone zone in this.Zones)
            {
                this.Lesson.IntubationPage.zoneOverlay.AddZone(zone);
            }

            this.StartTime = DateTime.Now;

            this.IsSetup = true;

            this.Stopwatch.Start();

            this.TimeoutStopwatch.Start();
        }

        private void CreateStageWatchers()
        {
            // Clear the list of watchers.
            this.CurrentStageWatchers.Clear();
            this.PeripheralStageWatchers.Clear();
            this.OutlyingStageWatchers.Clear();

            Watcher endotrachealTubeDetectionWatcher = new Watcher();

            #region WatcherLogic

            endotrachealTubeDetectionWatcher.CheckForActionDelegate = () =>
            {
                // TREY: USING FUNCTIONS
                ColorSpacePoint colorSpacePoint = Watcher.ColorSpaceFromPoint3D(this.Lesson.ManikinFaceLocation);
                Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, -25, -100, 100, 100);
                Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._intubationTubeColor,
                                                                        rect,
                                                                        KinectManager.ColorData);

                //CameraSpacePoint cameraSpacePoint = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };
                //ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(cameraSpacePoint);
                //Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._intubationTubeColor, 
                //                                                        new Int32Rect(  (int)System.Math.Max(0, colorSpacePoint.X - 25), 
                //                                                                        (int)System.Math.Max(0, colorSpacePoint.Y - 100), 
                //                                                                        100, 
                //                                                                        100), 
                //                                                        KinectManager.ColorData);

                if (!double.IsNaN(found.X) && !double.IsNaN(found.Y))
                {
                    int depth = KinectManager.GetDepthValueForColorPoint(found);
                    if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                    {
                        Point3D loc = KinectManager.Get3DPointForColorPoint(found, depth);

                        this.Lesson.EndotrachealTubeLocation = loc;

                        endotrachealTubeDetectionWatcher.IsTriggered = true;
                    }
                    else
                    {
                        endotrachealTubeDetectionWatcher.IsTriggered = false;

                        //System.Diagnostics.Debug.WriteLine("No depth found for Endotracheal Tube.");
                    }
                }
                else
                {
                    endotrachealTubeDetectionWatcher.IsTriggered = false;
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(endotrachealTubeDetectionWatcher);
            this.PeripheralStageWatchers.Add(endotrachealTubeDetectionWatcher);

            // Add in new watchers.
            Watcher endotrachealTubePositionChangeWatcher = new Watcher();

            #region WatcherLogic

            // Checks for the endotrachealTube to change position.
            endotrachealTubePositionChangeWatcher.CheckForActionDelegate = () =>
            {
                // This watcher will check the position of the current endotrachealTube and update our known position of the layrngoscope to the latest/greatest.
                // This watcher will only trigger when motion is detected, so if it's not triggered - then the watcher's determined location for the endotrachealTube will be the last known position stored previously. 
                // +-> This is intended to allow the watcher to gather data from a few frames, and average them together in order to come up with the current position confidently.

                if (this.Lesson.EndotrachealTubeLocation.X != 0 && this.Lesson.EndotrachealTubeLocation.Y != 0)
                {

                    //  TREY: USING FUNCTIONS
                    ColorSpacePoint colorSpacePoint = Watcher.ColorSpaceFromPoint3D(this.Lesson.EndotrachealTubeLocation);
                    // Since they use pixel  range for this...
                    int pixelScanRange = 50;
                    int x = -1 * pixelScanRange / 2;
                    int y = -1 * pixelScanRange / 2;
                    Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, x, y, pixelScanRange, pixelScanRange);

                    Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._intubationTubeColor,
                                                                            rect,
                                                                            KinectManager.ColorData);

                    //CameraSpacePoint cameraSpacePoint = new CameraSpacePoint() { X = (float)this.Lesson.EndotrachealTubeLocation.X, Y = (float)this.Lesson.EndotrachealTubeLocation.Y, Z = (float)this.Lesson.EndotrachealTubeLocation.Z / 1000 };
                    //ColorSpacePoint currentLoc = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(cameraSpacePoint);

                    //int pixelScanRange = 50;
                    //Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._intubationTubeColor, 
                    //                                                        new Int32Rect(  (int)System.Math.Max(0, currentLoc.X - pixelScanRange / 2), 
                    //                                                                        (int)System.Math.Max(0, currentLoc.Y - pixelScanRange / 2), 
                    //                                                                        (int)pixelScanRange, 
                    //                                                                        (int)pixelScanRange), 
                    //                                                        KinectManager.ColorData);

                    if (!double.IsNaN(found.X) && !double.IsNaN(found.Y))
                    {
                        // The Point was found
                        int depth = KinectManager.GetDepthValueForColorPoint(found);
                        if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                        {
                            watcher_FoundEndotrachealTubePos.Z += ((double)depth);
                            watcher_PinkDepthCounter++;
                        }
                        else
                        {
                            watcher_FoundEndotrachealTubePos.Z = 0;
                            watcher_PinkDepthCounter = 0;
                        }

                        watcher_FoundEndotrachealTubePos.X += found.X;
                        watcher_FoundEndotrachealTubePos.Y += found.Y;
                        watcher_PinkCounter++;
                    }
                    else
                    {
                        // Color wasn't found this time, reset X, Y, Z, and both counters.
                        watcher_PinkCounter = 0;
                        watcher_FoundEndotrachealTubePos.X = 0;
                        watcher_FoundEndotrachealTubePos.Y = 0;

                        watcher_PinkDepthCounter = 0;
                        watcher_FoundEndotrachealTubePos.Z = 0;

                    }

                    if (watcher_PinkCounter > 0 && watcher_PinkDepthCounter > 0)
                    {
                        Point3D temp = new Point3D();
                        // Get the average of the found x's.
                        temp.X = watcher_FoundEndotrachealTubePos.X / watcher_PinkCounter;
                        // Get the average of the found y's.
                        temp.Y = watcher_FoundEndotrachealTubePos.Y / watcher_PinkCounter;
                        // Get the average of the found z's.
                        temp.Z = watcher_FoundEndotrachealTubePos.Z / watcher_PinkDepthCounter;

                        watcher_FoundEndotrachealTubePos = temp;

                        //System.Diagnostics.Debug.WriteLine(String.Format("ManikinRefoundAt: ({0},{1},{2}).", watcher_FoundEndotrachealTubePos.X, watcher_FoundEndotrachealTubePos.Y, watcher_FoundEndotrachealTubePos.Z));

                        //MessageBox.Show(String.Format("Depth of chin is {0}mm from the Kinect sensor.", newHeadLocation.Z));

                        // Check for a change in Depth.
                        double deltaDepth = System.Math.Abs(watcher_FoundEndotrachealTubePos.Z - this.Lesson.EndotrachealTubeLocation.Z);

                        //System.Diagnostics.Debug.WriteLine(String.Format("Change in depth is: {0}mm", deltaDepth));

                        if (deltaDepth > 14 && deltaDepth < 300)
                        {
                            endotrachealTubePositionChangeWatcher.IsTriggered = true;
                        }
                        else
                        {
                            endotrachealTubePositionChangeWatcher.IsTriggered = false;
                            watcher_FoundEndotrachealTubePos = new Point3D();
                            watcher_PinkCounter = 0;
                            watcher_PinkDepthCounter = 0;
                        }
                    }
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(endotrachealTubePositionChangeWatcher);
            this.PeripheralStageWatchers.Add(endotrachealTubePositionChangeWatcher);

            Watcher rightHandOrWristByEndotrachealTubeWatcher = new Watcher();

            #region WatcherLogic

            // Checks for the right hand or wrist to come into close proximity of the endotrachealTube's location.
            rightHandOrWristByEndotrachealTubeWatcher.CheckForActionDelegate = () =>
            {
                CameraSpacePoint endotrachealTubeInSkeletalSpace;
                CameraSpacePoint rHandLoc;
                CameraSpacePoint rWristLoc;
                double rHandDistance;
                double rWristDistance;

                // Get the tube location  in  skeleton space
                //endotrachealTubeInSkeletalSpace = new CameraSpacePoint() { X = (float)this.Lesson.EndotrachealTubeLocation.X, Y = (float)this.Lesson.EndotrachealTubeLocation.Y, Z = (float)this.Lesson.EndotrachealTubeLocation.Z / 1000 };

                // Convert 
                endotrachealTubeInSkeletalSpace = Watcher.CameraSpaceFromPoint3D(this.Lesson.EndotrachealTubeLocation);

                SkeletonProfile skele = null;

                if (this.Lesson.StuckSkeletonProfileID.HasValue)
                {
                    // Retreive
                    skele = KinectManager.SingleStickySkeletonProfile;
                }
                if (skele != null)
                {
                    // Get the right hand and right wrist joint positions out of the profile.
                    rHandLoc = skele.JointProfiles.First(o => o.JointType == JointType.HandRight).CurrentPosition;
                    rWristLoc = skele.JointProfiles.First(o => o.JointType == JointType.WristRight).CurrentPosition;

                    // Distance returned by KinectManager.Distance3D( SkeletalPoint a, SkeletalPoint b) is in SkeletalSpace, and therefore is represented in Meters.
                    rHandDistance = System.Math.Abs(KinectManager.Distance3D(endotrachealTubeInSkeletalSpace, rHandLoc));     // Check the distance between the endotrachealTube in SkeletalSpace and the right hand. Absolute value is used to negate which point is used as the base.
                    rWristDistance = System.Math.Abs(KinectManager.Distance3D(endotrachealTubeInSkeletalSpace, rWristLoc));   // Check the distance between the endotrachealTube in the SkeletalSpace and the right wrist. Absolute value is used to negate which point is used as the base.

                    //System.Diagnostics.Debug.WriteLine(String.Format("R-Hand to endotrachealTube:  {0:#}mm", rHandDistance * 1000));
                    //System.Diagnostics.Debug.WriteLine(String.Format("R-Wrist to endotrachealTube: {0:#}mm @{1}", rWristDistance * 1000, KinectManager.AzimuthAngle(endotrachealTubeInSkeletalSpace, rWristLoc)));

                    double desiredMaxDistance = 0.400;  // 250 mm

                    // Check if the right hand or wrist is found near the endotrachealTube of the manikin.
                    if (rHandDistance <= desiredMaxDistance || rWristDistance <= desiredMaxDistance)
                    {
                        // If it is within the disiredMaxDistance, flip our trigger switch on.
                        rightHandOrWristByEndotrachealTubeWatcher.IsTriggered = true;
                    }
                    else
                    {
                        // If it's outside the desiredMaxDistance, then flip our trigger switch off.
                        rightHandOrWristByEndotrachealTubeWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    // If no skeleton was found, then the hand/wrist can't be near the endotrachealTube.
                    rightHandOrWristByEndotrachealTubeWatcher.IsTriggered = false;
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(rightHandOrWristByEndotrachealTubeWatcher);
            this.PeripheralStageWatchers.Add(rightHandOrWristByEndotrachealTubeWatcher);

            //Watcher endotrachealTubeDetectedOverMouthWatcher = new Watcher();
        }

        public override void UpdateStage()
        {
            if (!this.IsSetup)
                SetupStage();

            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
                this.TimeoutStage();

            if (this.CurrentStageWatchers != null)
            {
                if (this.CurrentStageWatchers[0].IsTriggered)
                {
                    // EndotrachealTube is detected to the right of the manikin's chin.
                    if (!this.ToolIsGrabbed)
                    {
                        this.ToolIsGrabbed = true;
                        System.Diagnostics.Debug.WriteLine("The Endotracheal Tube has been Grabbed.");
                        this.Lesson.DetectableActions.DetectAction("Picked Up Right", "Pick Up", "Endotracheal Tube", true);

                        if (RightHandCloserToEndotrachealTubeThanLeft( true ))
                        {
                            this.toolGrabbedRightHand = true;
                            this.Lesson.DetectableActions.DetectAction("Right Handed Endotracheal Tube Grab", "Grab Endotracheal Tube", "User", true);
                        }
                        else
                        {
                            this.toolGrabbedLeftHand = true;
                            this.Lesson.DetectableActions.DetectAction("Left Handed Endotracheal Tube Grab", "Grab Endotracheal Tube", "User", true);
                        }
                    }
                }

                if (this.CurrentStageWatchers[1].IsTriggered)
                {
                    // EndotrachealTube is in motion.
                }

                if (this.CurrentStageWatchers[2].IsTriggered)
                {
                    // User's right hand/wrist is by the Endotracheal Tube.
                    // EndotrachealTube is detected to the right of the manikin's chin.
                    if (!this.toolGrabbedRightHand)
                    {
                        this.toolGrabbedRightHand = true;
                        this.Lesson.DetectableActions.DetectAction("Right Handed Endotracheal Tube Grab", "Grab Endotracheal Tube", "User", true);
                    }
                }
            }

            if( this.ToolIsGrabbed )
            {
                this.Complete = true;
                EndStage();
            }
        }



        private bool RightHandCloserToEndotrachealTubeThanLeft(bool useDepth)
        {
            CameraSpacePoint endotrachealTubeInSkeletalSpace;
            CameraSpacePoint rightHandLoc;
            CameraSpacePoint leftHandLoc;
            double rightHandDistance;
            double leftHandDistance;

            endotrachealTubeInSkeletalSpace = new CameraSpacePoint() { X = (float)this.Lesson.EndotrachealTubeLocation.X, Y = (float)this.Lesson.EndotrachealTubeLocation.Y, Z = (float)this.Lesson.EndotrachealTubeLocation.Z / 1000 };

            SkeletonProfile skele = null;

            if (this.Lesson.StuckSkeletonProfileID.HasValue)
            {
                // Retreive
                skele = KinectManager.SingleStickySkeletonProfile;
            }
            if (skele != null)
            {
                // Get the right hand and right wrist joint positions out of the profile.
                rightHandLoc = skele.JointProfiles.First(o => o.JointType == JointType.HandRight).CurrentPosition;
                leftHandLoc = skele.JointProfiles.First(o => o.JointType == JointType.HandLeft).CurrentPosition;

                if (useDepth)
                {
                    // Distance returned by KinectManager.Distance3D( SkeletalPoint a, SkeletalPoint b) is in SkeletalSpace, and therefore is represented in Meters.
                    rightHandDistance = System.Math.Abs(KinectManager.Distance3D(endotrachealTubeInSkeletalSpace, rightHandLoc));     // Check the distance between the manikinChin in SkeletalSpace and the right hand. Absolute value is used to negate which point is used as the base.
                    leftHandDistance = System.Math.Abs(KinectManager.Distance3D(endotrachealTubeInSkeletalSpace, leftHandLoc));     // Check the distance between the manikinChin in SkeletalSpace and the right hand. Absolute value is used to negate which point is used as the base.

                    System.Diagnostics.Debug.WriteLine(String.Format("3D: EndotrachealTube -> Right Hand Distance: {0:#}mm", rightHandDistance * 1000));
                    System.Diagnostics.Debug.WriteLine(String.Format("3D: EndotrachealTube -> Left Hand Distance: {0:#}mm", leftHandDistance * 1000));

                    double desiredMaxDistance = 0.515;  // meters

                    bool isRightHandOutOfRange = false;
                    bool isLeftHandOutOfRange = false;

                    if (rightHandDistance >= desiredMaxDistance)
                    {
                        // Distance between the user's head and the manikin's chin is within the desiredMaxDistance.
                        isRightHandOutOfRange = true;
                    }

                    if (leftHandDistance >= desiredMaxDistance)
                    {
                        // Distance between the user's head and the manikin's chin is within the desiredMaxDistance.
                        isLeftHandOutOfRange = true;
                    }

                    if (isLeftHandOutOfRange && isRightHandOutOfRange)
                    {
                        System.Diagnostics.Debug.WriteLine("Value Proximity Breached -> Attempting 2D comparison...");
                        return RightHandCloserToEndotrachealTubeThanLeft(false);
                    }
                }
                else
                {
                    // Distance returned by KinectManager.Distance3D( SkeletalPoint a, SkeletalPoint b) is in SkeletalSpace, and therefore is represented in Meters.
                    rightHandDistance = System.Math.Abs(KinectManager.Distance2D(endotrachealTubeInSkeletalSpace, rightHandLoc));     // Check the distance between the manikinChin in SkeletalSpace and the right hand. Absolute value is used to negate which point is used as the base.
                    leftHandDistance = System.Math.Abs(KinectManager.Distance2D(endotrachealTubeInSkeletalSpace, leftHandLoc));     // Check the distance between the manikinChin in SkeletalSpace and the right hand. Absolute value is used to negate which point is used as the base.

                    System.Diagnostics.Debug.WriteLine(String.Format("2D: EndotrachealTube -> Right Hand Distance: {0:#}pixels", rightHandDistance * 1000));
                    System.Diagnostics.Debug.WriteLine(String.Format("2D: EndotrachealTube -> Left Hand Distance: {0:#}pixels", leftHandDistance * 1000));
                }

                if (rightHandDistance < leftHandDistance)   // if the Right Hand is closer (less than) the Left Hand to the Syringe
                    return true;
            }

            return false;
        }

        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }

        public override void EndStage( bool playSound = true )
        {
            this.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            // Get the current time to signify the time the stage ended.
            this.EndTime = DateTime.Now;

            // Create a screenshot for the current stage.
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                Intubation lesson = (Intubation)args.Argument;

                KinectImage image = KinectImage.TakeKinectSnapshot(true, true);

                lesson.GrabEndotrachealTubeImage = image;
            };

            worker.RunWorkerAsync(this.Lesson);

            AssessEndotrachealTubeGrab();

            this.Lesson.CurrentStageIndex++;

            if( playSound ) Lesson.PlayNextStageSound();
        }

        private void AssessEndotrachealTubeGrab()
        {
            double score = 1;

            if (this.ToolIsGrabbed)
            {
                if (this.toolGrabbedRightHand)
                {
                    this.Lesson.GrabEndotrachealTubeFeedbackIDs.Add(42); // The Endotracheal Tube was grabbed with the proper hand.
                }
                else if (this.toolGrabbedLeftHand)
                {
                    score -= 0.5;
                    this.Lesson.GrabEndotrachealTubeFeedbackIDs.Add(43); // The Endotracheal Tube was grabbed with the incorrect hand.
                }
            }

            this.Lesson.GrabEndotrachealTubeScore = System.Math.Max(0, System.Math.Min(score, 1)); // Clamp the score between 0 and 1.
        }

        public override void PauseStage()
        {
            this.Lesson.Paused = true;
            this.Lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.Lesson.Paused = false;
            this.Lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void ResetStage()
        {

        }

        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            //this.PauseStage();
            this.TimedOut = true;
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            this.RequestStageVideo();   // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
        }

        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.Lesson.IntubationPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Intubation/Videos/Stage 005.wmv", UriKind.Relative));
            this.Lesson.IntubationPage.VideoPlayer.startVideo(this, new RoutedEventArgs());

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        public override void WipeClean()
        {
            toolIsAboveMouth = false;
            toolisGrabbed = false;

            watcher_PinkCounter = 0;
            watcher_PinkDepthCounter = 0;
            watcher_FoundEndotrachealTubePos = new Point3D();

            toolGrabbedRightHand = false;
            toolGrabbedLeftHand = false;

            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.IsSetup = false;

            if (this.StageScore != null)
            {
                this.StageScore.WipeClean();
                this.StageScore = null;
                this.StageScore = new StageScore();
            }
            else
            {
                this.StageScore = new StageScore();
            }

            this.Active = false;
            this.Complete = false;

            if (this.Zones != null)
            {
                this.Zones.Clear();
            }
            else
            {
                this.Zones = new List<Zone>();
            }

            if (this.Stopwatch != null)
            {
                this.Stopwatch.Reset();
            }
            else
            {
                this.Stopwatch = new System.Diagnostics.Stopwatch();
            }

            if (this.TimeoutStopwatch != null)
                this.TimeoutStopwatch.Reset();
            else this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();

            this.TimedOut = false;
        }
    }
}