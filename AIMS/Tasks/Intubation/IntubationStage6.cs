﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using System.Windows.Media.Imaging;
using System.Windows;
using System.ComponentModel;
using System.Diagnostics;

using Microsoft.Kinect;

using AIMS.Assets.Lessons2;

namespace AIMS.Tasks.Intubation
{
    public class IntubationStage6 : Stage
    {
        private Intubation lesson;
        public Intubation Lesson { get { return lesson; } set { lesson = value; } }

        // Endotracheal Tube:
        private RecZone aboveMouthZone = null;
        private RecZone leftBadMouthZone = null;
        private RecZone rightBadMouthZone = null;
        private bool hasFoundTubeColor = false;
        private bool toolIsAboveMouth = false;
        private bool hasFoundLaryngoscopeColor = false;
        private bool toolIsInserted = false;
        private bool toolIsInsertedProperly = false;
        private bool toolIsInsertedInproperly_Left = false;
        private bool toolIsInsertedInproperly_Right = false;

        // Laryngoscope:
        private RecZone upperTeethForceZone = null;
        private RecZone lowerTeethForceZone = null;
        private bool laryngoscopeDamagedUpperTeeth = false;
        private bool laryngoscopeDamagedLowerTeeth = false;

        private ColorRange _laryngoscopeColor;
        private ColorRange _intubationTubeColor;

        #region Public Getters/Setters
        // Endotracheal Tube
        public RecZone AboveMouthZone { get { return aboveMouthZone; } set { aboveMouthZone = value; } }
        public RecZone LeftBadMouthZone { get { return leftBadMouthZone; } set { leftBadMouthZone = value; } }
        public RecZone RightBadMouthZone { get { return rightBadMouthZone; } set { rightBadMouthZone = value; } }
        public bool HasFoundTubeColor { get { return hasFoundTubeColor; } set { hasFoundTubeColor = value; } }
        public bool ToolIsAboveMouth { get { return toolIsAboveMouth; } set { toolIsAboveMouth = value; } }
        public bool HasFoundLaryngoscopeColor { get { return hasFoundLaryngoscopeColor; } set { hasFoundLaryngoscopeColor = value; } }
        public bool ToolIsInserted { get { return toolIsInserted; } set { toolIsInserted = value; } }
        public bool ToolIsInsertedProperly { get { return toolIsInsertedProperly; } set { toolIsInsertedProperly = value; } }
        public bool ToolIsInsertedInproperly_Left { get { return toolIsInsertedInproperly_Left; } set { toolIsInsertedInproperly_Left = value; } }
        public bool ToolIsInsertedInproperly_Right { get { return toolIsInsertedInproperly_Right; } set { toolIsInsertedInproperly_Right = value; } }

        // Laryngoscope
        public RecZone UpperTeethForceZone { get { return upperTeethForceZone; } set { upperTeethForceZone = value; } }
        public RecZone LowerTeethForceZone { get { return lowerTeethForceZone; } set { lowerTeethForceZone = value; } }
        public bool LaryngoscopeDamagedUpperTeeth { get { return laryngoscopeDamagedUpperTeeth; } set { laryngoscopeDamagedUpperTeeth = value; } }
        public bool LaryngoscopeDamagedLowerTeeth { get { return laryngoscopeDamagedLowerTeeth; } set { laryngoscopeDamagedLowerTeeth = value; } }
        #endregion

        public IntubationStage6(Intubation intubation)
        {
            Lesson = intubation;

            StageNumber = 6;
            Name = "Insert Endotracheal Tube";
            Instruction = "Insert the endotracheal tube so that the balloon cuff is just beyond the vocal cords.";
            TargetTime = TimeSpan.FromSeconds(3.5);
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Tasks/Intubation/Graphics/Intubation006.png";
            Active = false;
            Complete = false;
            ScoreWeight = 0.10;
            Zones = new List<Zone>();
            IsSetup = false;
            Stopwatch = new System.Diagnostics.Stopwatch();
            this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.CanTimeOut = false;
            this.TimeOutTime = TimeSpan.FromSeconds(10.0);
        }

        public override void SetupStage()
        {
            this.Lesson.IntubationPage.StageInfoTextBlock.Text = this.Name;
            this.Lesson.IntubationPage.InstructionTextBlock.Text = this.Instruction;

            this.Lesson.IntubationPage.flow.Index = this.Lesson.CurrentStageIndex;  // change cover flow image.

            // Set the image for the current stage.
            this.Lesson.IntubationPage.CurrentStageImage.Source = new BitmapImage(new Uri("/Tasks/Intubation/Graphics/Intubation006.png", UriKind.Relative));

            try
            {
                this._laryngoscopeColor = ColorTracker.GetObjectColorRange("laryngoscope").ColorRange;
                this._intubationTubeColor = ColorTracker.GetObjectColorRange("intubation_tube").ColorRange;
            }
            catch (Exception exc) { Debug.Print("IntubationStage6 SetupStage() ERROR: {0}", exc.Message); }

            this.Zones.Clear(); // Clear the stage zone list.

            this.Lesson.IntubationPage.zoneOverlay.ClearZones();

            // The ManikinFaceLocation must be changed from Point3D to ColorPoint
            //CameraSpacePoint cameraSpacePoint = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };

            // TREY: replaced this with the function
            CameraSpacePoint cameraSpacePoint = Watcher.CameraSpaceFromPoint3D(this.Lesson.ManikinFaceLocation);
            ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(cameraSpacePoint);

            // These pixels are in depth space... but are now color space
            int scale_x = Intubation.SCALE_X;
            int scale_y = Intubation.SCALE_Y;

            AboveMouthZone = new RecZone();
            AboveMouthZone.X = colorSpacePoint.X - (10 * scale_x);
            AboveMouthZone.Y = colorSpacePoint.Y - (40 * scale_y);
            AboveMouthZone.Z = 0;
            AboveMouthZone.Name = "Above Mouth Zone";
            //AboveMouthZone.ZoneType = ZoneType.Position;
            AboveMouthZone.Width = 20 * scale_x;
            AboveMouthZone.Height = 30 * scale_y;
            AboveMouthZone.Depth = 0;
            AboveMouthZone.DoesTrackJoint = false;  // This zone is not used for joint position, so this flag is set.

            this.Zones.Add(AboveMouthZone);
                
            LeftBadMouthZone = new RecZone();
            LeftBadMouthZone.X = colorSpacePoint.X - (30 * scale_x);
            LeftBadMouthZone.Y = colorSpacePoint.Y - (40 * scale_y);
            LeftBadMouthZone.Z = 0;
            LeftBadMouthZone.Name = "Left Bad Mouth Zone";
            //LeftBadMouthZone.ZoneType = ZoneType.Position;
            LeftBadMouthZone.Width = 20 * scale_x;
            LeftBadMouthZone.Height = 30 * scale_y;
            LeftBadMouthZone.Depth = 0;
            LeftBadMouthZone.DoesTrackJoint = false; // This zone is not used for joint position, so this flag is set.

            this.Zones.Add(LeftBadMouthZone);

            RightBadMouthZone = new RecZone();
            RightBadMouthZone.X = colorSpacePoint.X + (10 * scale_x);
            RightBadMouthZone.Y = colorSpacePoint.Y - (40 * scale_y);
            RightBadMouthZone.Z = 0;
            RightBadMouthZone.Name = "Right Bad Mouth Zone";
            //RightBadMouthZone.ZoneType = ZoneType.Position;
            RightBadMouthZone.Width = 20 * scale_x;
            RightBadMouthZone.Height = 30 * scale_y;
            RightBadMouthZone.Depth = 0;
            RightBadMouthZone.DoesTrackJoint = false;  // This zone is not used for joint position, so this flag is set.

            this.Zones.Add(RightBadMouthZone);

            #region Laryngoscope Zones
            #region Upper Teeth Force Zone
            UpperTeethForceZone = new RecZone();
            UpperTeethForceZone.X = colorSpacePoint.X - (30 * scale_x);
            UpperTeethForceZone.Y = colorSpacePoint.Y - (65 * scale_y);
            UpperTeethForceZone.Z = 0;
            UpperTeethForceZone.Name = "Upper Teeth Force Zone";
            //UpperTeethForceZone.ZoneType = ZoneType.Force;
            UpperTeethForceZone.Width = 40 * scale_x;
            UpperTeethForceZone.Height = 20 * scale_y;
            UpperTeethForceZone.Depth = 0;
            UpperTeethForceZone.DoesTrackJoint = false;  // This zone is not used for joint position, so this flag is set.
            UpperTeethForceZone.IsImproperZone = true;

            this.Zones.Add(UpperTeethForceZone);
            #endregion

            #region Lower Teeth Force Zone
            LowerTeethForceZone = new RecZone();
            LowerTeethForceZone.X = colorSpacePoint.X - (30 * scale_x);
            LowerTeethForceZone.Y = colorSpacePoint.Y - (20 * scale_y);
            LowerTeethForceZone.Z = 0;
            LowerTeethForceZone.Name = "Lower Teeth Force Zone";
            //LowerTeethForceZone.ZoneType = ZoneType.Force;
            LowerTeethForceZone.Width = 40 * scale_x;
            LowerTeethForceZone.Height = 10 * scale_y;
            LowerTeethForceZone.Depth = 0;
            LowerTeethForceZone.DoesTrackJoint = false;  // This zone is not used for joint position, so this flag is set.
            LowerTeethForceZone.IsImproperZone = true;

            this.Zones.Add(LowerTeethForceZone);
            #endregion
            #endregion

            // Add the zones into the zoneOverlay
            foreach (Zone zone in this.Zones)
            {
                this.Lesson.IntubationPage.zoneOverlay.AddZone(zone);
            }

            this.CurrentStageWatchers = new List<BaseWatcher>();
            this.OutlyingStageWatchers = new List<BaseWatcher>();
            this.PeripheralStageWatchers = new List<BaseWatcher>();

            CreateStageWatchers();

            StartTime = DateTime.Now;

            this.IsSetup = true;

            Stopwatch.Start();

            this.TimeoutStopwatch.Start();
        }

        public void CreateStageWatchers()
        {
            Watcher endotrachealTubeDetectedOverMouthWatcher = new Watcher();

            #region WatcherLogic

            endotrachealTubeDetectedOverMouthWatcher.CheckForActionDelegate = () =>
            {
                // TREY: USING FUNCTIONS
                ColorSpacePoint colorSpacePoint = Watcher.ColorSpaceFromPoint3D(this.Lesson.ManikinFaceLocation);
                Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, -30, -100, 60, 80);

                Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._intubationTubeColor,
                                                                        rect,
                                                                        KinectManager.ColorData);

                //CameraSpacePoint cameraSpacePoint = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };
                //ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(cameraSpacePoint);
                //Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._intubationTubeColor, 
                //                                                        new Int32Rect(  (int)System.Math.Max(0, colorSpacePoint.X - 30), 
                //                                                                        (int)System.Math.Max(0, colorSpacePoint.Y - 100), 
                //                                                                        60, 
                //                                                                        80), 
                //                                                        KinectManager.ColorData);

                if (!double.IsNaN(found.X) && !double.IsNaN(found.Y))
                {
                    int depth = KinectManager.GetDepthValueForColorPoint(found);
                    if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                    {
                        Point3D loc = KinectManager.Get3DPointForColorPoint(found, depth);

                        this.Lesson.EndotrachealTubeLocation = loc;

                        System.Diagnostics.Debug.WriteLine("Depth found");
                        endotrachealTubeDetectedOverMouthWatcher.IsTriggered = true;
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("Depth not known");
                        endotrachealTubeDetectedOverMouthWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Color not found");
                    endotrachealTubeDetectedOverMouthWatcher.IsTriggered = false;
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(endotrachealTubeDetectedOverMouthWatcher);

            Watcher laryngoscopeDetectedOverMouthWatcher = new Watcher();

            #region WatcherLogic

            laryngoscopeDetectedOverMouthWatcher.CheckForActionDelegate = () =>
            {
                //  TREY: USING FUNCTIONS
                ColorSpacePoint colorSpacePoint = Watcher.ColorSpaceFromPoint3D(this.Lesson.ManikinFaceLocation);
                Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, -30, -80, 60, 60);

                Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._laryngoscopeColor,
                                                                        rect,
                                                                        KinectManager.ColorData);

                //CameraSpacePoint point = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };
                //ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(point);
                //Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._laryngoscopeColor,
                //                                                        new Int32Rect(  (int)System.Math.Max(0, colorSpacePoint.X - 30), 
                //                                                                        (int)System.Math.Max(0, colorSpacePoint.Y - 80), 
                //                                                                        60,
                //                                                                        60), 
                //                                                        KinectManager.ColorData);

                if (!double.IsNaN(found.X) && !double.IsNaN(found.Y))
                {
                    int depth = KinectManager.GetDepthValueForColorPoint(found);
                    if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                    {
                        Point3D loc = KinectManager.Get3DPointForColorPoint(found, depth);

                        this.Lesson.LaryngoscopeLocation = loc;

                        laryngoscopeDetectedOverMouthWatcher.IsTriggered = true;
                    }
                    else
                    {
                        laryngoscopeDetectedOverMouthWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    laryngoscopeDetectedOverMouthWatcher.IsTriggered = false;
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(laryngoscopeDetectedOverMouthWatcher);

            Watcher TubeInsertedProperlyWatcher = new Watcher();

            #region WatcherLogic

            TubeInsertedProperlyWatcher.CheckForActionDelegate = () =>
            {
                if (this.HasFoundTubeColor)
                {
                    CameraSpacePoint tubeInSkeletalSpace = new CameraSpacePoint() { X = (float)this.Lesson.EndotrachealTubeLocation.X, Y = (float)this.Lesson.EndotrachealTubeLocation.Y, Z = (float)this.Lesson.EndotrachealTubeLocation.Z / 1000 };                    
                }
                else
                {
                    TubeInsertedProperlyWatcher.IsTriggered = false;
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(laryngoscopeDetectedOverMouthWatcher);
        }

        public override void UpdateStage()
        {
            if (!this.IsSetup)
                SetupStage();

            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
                this.TimeoutStage();

            {
                // Convert the 3DPosition CameraSpace to 2DPosition ColorSpace
                CameraSpacePoint endotrachealTube = new CameraSpacePoint() { X = (float)this.Lesson.EndotrachealTubeLocation.X, Y = (float)this.Lesson.EndotrachealTubeLocation.Y, Z = (float)this.Lesson.EndotrachealTubeLocation.Z / 1000 };
                ColorSpacePoint colorPoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(endotrachealTube);
                Point3D colorPoint3D = new Point3D(colorPoint.X, colorPoint.Y, endotrachealTube.Z * 1000);

                this.AboveMouthZone.UpdatePointWithinZone(colorPoint3D);
                this.LeftBadMouthZone.UpdatePointWithinZone(colorPoint3D);
                this.RightBadMouthZone.UpdatePointWithinZone(colorPoint3D);
            }
            {
                // Convert the 3DPosition CameraSpace to 2DPosition ColorSpace
                CameraSpacePoint laryngoscope = new CameraSpacePoint() { X = (float)this.Lesson.LaryngoscopeLocation.X, Y = (float)this.Lesson.LaryngoscopeLocation.Y, Z = (float)this.Lesson.LaryngoscopeLocation.Z / 1000 };
                ColorSpacePoint colorPoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(laryngoscope);
                Point3D colorPoint3D = new Point3D(colorPoint.X, colorPoint.Y, laryngoscope.Z * 1000);

                this.UpperTeethForceZone.UpdatePointWithinZone(colorPoint3D);
                this.LowerTeethForceZone.UpdatePointWithinZone(colorPoint3D);
            }

            if (this.CurrentStageWatchers != null)
            {
                if (this.CurrentStageWatchers[0].IsTriggered)
                {
                    // The endotracheal tube is above the mouth.
                    if (!this.HasFoundTubeColor)
                        this.HasFoundTubeColor = true;  // set flag

                }
                else
                {
                    if (this.HasFoundTubeColor)
                        this.HasFoundTubeColor = false; // unset flag

                }

                if (this.CurrentStageWatchers[1].IsTriggered)
                {
                    // The laryngoscope is above the mouth.
                    if (!this.HasFoundLaryngoscopeColor)
                        this.HasFoundLaryngoscopeColor = true;  // set flag if it wasn't set.
                }
                else
                {
                    if (this.HasFoundLaryngoscopeColor)
                        this.HasFoundLaryngoscopeColor = false; // unset flag if it was set.
                }
            }

            if (UpperTeethForceZone.IsWithinZone && UpperTeethForceZone.TotalTimeWithinZone >= TimeSpan.FromMilliseconds(700.0))
            {
                LaryngoscopeDamagedUpperTeeth = true;

                System.Diagnostics.Debug.WriteLine("Laryngoscope Positioning - Potential Damage to the Upper Teeth.");
                this.Lesson.DetectableActions.DetectAction("Fulcruming On Teeth", "Lift", "Laryngoscope", true);
            }
            else if (LowerTeethForceZone.IsWithinZone && LowerTeethForceZone.TotalTimeWithinZone >= TimeSpan.FromMilliseconds(700.0))
            {
                LaryngoscopeDamagedLowerTeeth = true;

                System.Diagnostics.Debug.WriteLine("Laryngoscope Positioning - Potential Damage to the Lower Teeth.");
            }

            if (HasFoundTubeColor && HasFoundLaryngoscopeColor)
            {
                ToolIsAboveMouth = true;
            }

            if (ToolIsAboveMouth)
            {

                if (this.AboveMouthZone.IsWithinZone && this.AboveMouthZone.TimeWithinZone > TimeSpan.FromSeconds(0.25))
                {
                    ToolIsInserted = true;
                    ToolIsInsertedProperly = true;

                    this.Lesson.DetectableActions.DetectAction("Inserted Center", "Insert", "Endotracheal Tube", true);
                }
                if (this.LeftBadMouthZone.IsWithinZone && this.LeftBadMouthZone.TimeWithinZone > TimeSpan.FromSeconds(0.25))
                {
                    ToolIsInserted = true;
                    ToolIsInsertedProperly = false;
                    ToolIsInsertedInproperly_Left = true;

                    this.Lesson.DetectableActions.DetectAction("Inserted Left", "Insert", "Endotracheal Tube", true);
                }
                if (this.RightBadMouthZone.IsWithinZone && this.RightBadMouthZone.TimeWithinZone > TimeSpan.FromSeconds(0.25))
                {
                    ToolIsInserted = true;
                    ToolIsInsertedProperly = false;
                    ToolIsInsertedInproperly_Right = true;

                    this.Lesson.DetectableActions.DetectAction("Inserted Right", "Insert", "Endotracheal Tube", true);
                }
            }

            if (ToolIsInserted)
            {
                this.Complete = true;
                this.EndStage();
            }
        }

        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }

        public override void EndStage( bool playSound = true )
        {
            this.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            // Get the current time to signify the time the stage ended.
            this.EndTime = DateTime.Now;

            // Create a screenshot for the current stage.
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                Intubation lesson = (Intubation)args.Argument;

                KinectImage image = KinectImage.TakeKinectSnapshot(true, true);

                lesson.InsertEndotrachealTubeImage = image;
            };

            worker.RunWorkerAsync(this.Lesson);

            AssessEndotrachealTubeInsertion();

            this.Lesson.CurrentStageIndex++;

            if( playSound ) Lesson.PlayNextStageSound();
        }

        private void AssessEndotrachealTubeInsertion()
        {
            double score = 1;

            if (this.LaryngoscopeDamagedUpperTeeth)
            {
                score -= 0.5;
                this.Lesson.InsertEndotrachealTubeFeedbackIDs.Add(77);   // The Laryngoscope potentially damaged the upper teeth while inserting the Endotracheal Tube.
            }
            else if (this.LaryngoscopeDamagedLowerTeeth)
            {
                score -= 0.5;
                this.Lesson.InsertEndotrachealTubeFeedbackIDs.Add(78);   // The Laryngoscope potentially damaged the lower teeth while inserting the Endotracheal Tube.
            }

            if (this.ToolIsInsertedProperly)
            {
                this.Lesson.InsertEndotrachealTubeFeedbackIDs.Add(44);   // The Endotracheal Tube was correctly inserted into the patient's mouth.
            }
            else
            {
                if (this.ToolIsInsertedInproperly_Right)
                {
                    score -= 0.5;
                    this.Lesson.InsertEndotrachealTubeFeedbackIDs.Add(45);   // The Endotracheal Tube was improperly inserted into the right side of the patient's mouth.
                }
                else if (this.ToolIsInsertedInproperly_Left)
                {
                    score -= 0.5;
                    this.Lesson.InsertEndotrachealTubeFeedbackIDs.Add(46);   // The Endotracheal Tube was improperly inserted into the left side of the patient's mouth.
                }
                else
                {
                    if (!this.ToolIsInserted)
                    {
                        score -= 1;
                        this.Lesson.InsertEndotrachealTubeFeedbackIDs.Add(47);   // The Endotracheal Tube wasn't inserted into the patient's mouth.
                    }
                }
            }

            this.Lesson.InsertEndotrachealTubeScore = System.Math.Max(0, System.Math.Min(score, 1)); // Clamp the score between 0 and 1.
        }

        public override void PauseStage()
        {
            this.Lesson.Paused = true;
            this.Lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.Lesson.Paused = false;
            this.Lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void ResetStage()
        {

        }

        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            //this.PauseStage();
            this.TimedOut = true;
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            this.RequestStageVideo();   // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
        }

        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.Lesson.IntubationPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Intubation/Videos/Stage 006.wmv", UriKind.Relative));
            this.Lesson.IntubationPage.VideoPlayer.startVideo(this, new RoutedEventArgs());

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        public override void WipeClean()
        {
            this.AboveMouthZone = null;
            this.LeftBadMouthZone = null;
            this.RightBadMouthZone = null;
            this.HasFoundTubeColor = false;
            this.ToolIsAboveMouth = false;
            this.HasFoundLaryngoscopeColor = false;
            this.ToolIsInsertedProperly = false;
            this.ToolIsInserted = false;
            this.ToolIsInsertedInproperly_Left = false;
            this.ToolIsInsertedInproperly_Right = false;            

            // Laryngoscope:
            upperTeethForceZone = null;
            lowerTeethForceZone = null;
            laryngoscopeDamagedUpperTeeth = false;
            laryngoscopeDamagedLowerTeeth = false;


            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.IsSetup = false;

            if (this.StageScore != null)
            {
                this.StageScore.WipeClean();
                this.StageScore = null;
                this.StageScore = new StageScore();
            }
            else
            {
                this.StageScore = new StageScore();
            }

            this.Active = false;
            this.Complete = false;

            if (this.Zones != null)
            {
                this.Zones.Clear();
            }
            else
            {
                this.Zones = new List<Zone>();
            }

            if (this.Stopwatch != null)
            {
                this.Stopwatch.Reset();
            }
            else
            {
                this.Stopwatch = new System.Diagnostics.Stopwatch();
            }

            if (this.TimeoutStopwatch != null)
                this.TimeoutStopwatch.Reset();
            else this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();

            this.TimedOut = false;
        }
    }
}