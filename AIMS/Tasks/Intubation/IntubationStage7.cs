﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using System.Windows.Media.Imaging;
using System.Windows;
using System.ComponentModel;
using System.Diagnostics;

using Microsoft.Kinect;

using AIMS.Assets.Lessons2;

namespace AIMS.Tasks.Intubation
{
    public class IntubationStage7 : Stage
    {
        private Intubation lesson;
        public Intubation Lesson { get { return lesson; } set { lesson = value; } }

        private RecZone setDownLaryngoscopeZone = null;
        private bool laryngoscopeSetDown = false;

        public RecZone SetDownLaryngoscopeZone { get { return setDownLaryngoscopeZone; } set { setDownLaryngoscopeZone = value; } }
        public bool LaryngoscopeSetDown { get { return laryngoscopeSetDown; } set { laryngoscopeSetDown = value; } }

        private bool rightHandLaryngoscopeRemoval = false;
        private bool leftHandLaryngoscopeRemoval = false;

        private bool isLaryngoscopeStillAboveMouth = false;

        private DateTime lastFoundAboveMouthTime = DateTime.MaxValue;

        private ColorRange _laryngoscopeColor;
        private ColorRange _intubationTubeColor;

        public IntubationStage7(Intubation intubation)
        {
            Lesson = intubation;

            StageNumber = 7;
            Name = "Remove Laryngoscope";
            Instruction = "Remove the laryngoscope from the manikin's mouth and place it onto the table.";
            TargetTime = TimeSpan.FromSeconds(3.5);
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Tasks/Intubation/Graphics/Intubation007.png";
            Active = false;
            Complete = false;
            ScoreWeight = 0.05;
            Zones = new List<Zone>();
            IsSetup = false;
            Stopwatch = new System.Diagnostics.Stopwatch();
            this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.CanTimeOut = false;
            this.TimeOutTime = TimeSpan.FromSeconds(10.0);
        }

        public override void SetupStage()
        {
            this.Lesson.IntubationPage.StageInfoTextBlock.Text = this.Name;
            this.Lesson.IntubationPage.InstructionTextBlock.Text = this.Instruction;

            this.Lesson.IntubationPage.flow.Index = this.Lesson.CurrentStageIndex;  // change cover flow image.

            // Set the image for the current stage.
            this.Lesson.IntubationPage.CurrentStageImage.Source = new BitmapImage(new Uri("/Tasks/Intubation/Graphics/Intubation007.png", UriKind.Relative));

            try
            {
                this._laryngoscopeColor = ColorTracker.GetObjectColorRange("laryngoscope").ColorRange;
                this._intubationTubeColor = ColorTracker.GetObjectColorRange("intubation_tube").ColorRange;
            }
            catch (Exception exc) { Debug.Print("IntubationStage7 SetupStage() ERROR: {0}", exc.Message); }

            this.Zones.Clear(); // Clear the stage zone list.

            this.Lesson.IntubationPage.zoneOverlay.ClearZones();

            // These pixels are in depth space... but are now color space
            int scale_x = Intubation.SCALE_X;
            int scale_y = Intubation.SCALE_Y;

            //zone left of the face
            SetDownLaryngoscopeZone = new RecZone();
            SetDownLaryngoscopeZone.X = this.Lesson.ManikinFaceLocation.X - (100 * scale_x);
            SetDownLaryngoscopeZone.Y = this.Lesson.ManikinFaceLocation.Y - (10 * scale_y);
            SetDownLaryngoscopeZone.Name = "Laryngoscope Zone";
            //SetDownLaryngoscopeZone.ZoneType = ZoneType.Position;
            SetDownLaryngoscopeZone.Width = 70 * scale_x;
            SetDownLaryngoscopeZone.Height = 75 * scale_y;
            SetDownLaryngoscopeZone.Depth = 0;
            this.SetDownLaryngoscopeZone.DoesTrackJoint = false;
            SetDownLaryngoscopeZone.AcceptedJointType = JointType.HandLeft;

            this.Zones.Add(SetDownLaryngoscopeZone);

            // Add the zones into the zoneOverlay
            foreach (Zone zone in this.Zones)
            {
                this.Lesson.IntubationPage.zoneOverlay.AddZone(zone);
            }

            this.CurrentStageWatchers = new List<BaseWatcher>();
            this.OutlyingStageWatchers = new List<BaseWatcher>();
            this.PeripheralStageWatchers = new List<BaseWatcher>();

            CreateStageWatchers();

            StartTime = DateTime.Now;

            this.IsSetup = true;

            Stopwatch.Start();

            this.TimeoutStopwatch.Start();
        }

        public void CreateStageWatchers()
        {
            // Checks for the laryngoscope remaining over the mouth of the manikin. 
            // ( We want this to move away during this stage. )
            Watcher laryngoscopeDetectedOverMouthWatcher = new Watcher();

            #region WatcherLogic

            laryngoscopeDetectedOverMouthWatcher.CheckForActionDelegate = () =>
            {
                // TREY: USING FUNCTIONS
                ColorSpacePoint colorSpacePoint = Watcher.ColorSpaceFromPoint3D(this.Lesson.ManikinFaceLocation);
                Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, -30, -80, 60, 60);

                Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._laryngoscopeColor,
                                                                        rect, 
                                                                        KinectManager.ColorData);

                //CameraSpacePoint point = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };
                //ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(point);
                //Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._laryngoscopeColor, 
                //                                                        new Int32Rect(  (int)System.Math.Max(0, colorSpacePoint.X - 30), 
                //                                                                        (int)System.Math.Max(0, colorSpacePoint.Y - 80), 
                //                                                                        60, 
                //                                                                        60), KinectManager.ColorData);

                if (!double.IsNaN(found.X) && !double.IsNaN(found.Y))
                {
                    lastFoundAboveMouthTime = DateTime.Now; // Timestamp when we last spotted yellow above the mouth.

                    int depth = KinectManager.GetDepthValueForColorPoint(found);
                    if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                    {
                        Point3D loc = KinectManager.Get3DPointForColorPoint(found, depth);

                        this.Lesson.LaryngoscopeLocation = loc;

                        laryngoscopeDetectedOverMouthWatcher.IsTriggered = true;
                    }
                    else
                    {
                        laryngoscopeDetectedOverMouthWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    laryngoscopeDetectedOverMouthWatcher.IsTriggered = false;
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(laryngoscopeDetectedOverMouthWatcher);

            Watcher endotrachealTubeDetectedOverMouthWatcher = new Watcher();

            #region WatcherLogic

            // Checks if the Endotracheal Tube remains inserted into the mouth of the manikin.
            // ( We want this to flag true, though we also know that the hand may cover the tip of the endotracheal tube when the user goes to stabalize it. )
            endotrachealTubeDetectedOverMouthWatcher.CheckForActionDelegate = () =>
            {
                // TREY: USING FUNCTIONS
                ColorSpacePoint colorSpacePoint = Watcher.ColorSpaceFromPoint3D(this.Lesson.ManikinFaceLocation);
                Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, -30, -65, 60, 60);

                Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._intubationTubeColor,
                                                                        rect,
                                                                        KinectManager.ColorData);

                //CameraSpacePoint point = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };
                //ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(point);
                //Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._intubationTubeColor, 
                //                                                        new Int32Rect(  (int)System.Math.Max(0, colorSpacePoint.X - 30), 
                //                                                                        (int)System.Math.Max(0, colorSpacePoint.Y - 65), 
                //                                                                        60, 
                //                                                                        60), 
                //                                                        KinectManager.ColorData);

                if (!double.IsNaN(found.X) && !double.IsNaN(found.Y))
                {
                    int depth = KinectManager.GetDepthValueForColorPoint(found);
                    if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                    {
                        Point3D loc = KinectManager.Get3DPointForColorPoint(found, depth);

                        this.Lesson.EndotrachealTubeLocation = loc;

                        endotrachealTubeDetectedOverMouthWatcher.IsTriggered = true;
                    }
                    else
                    {
                        endotrachealTubeDetectedOverMouthWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    endotrachealTubeDetectedOverMouthWatcher.IsTriggered = false;
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(endotrachealTubeDetectedOverMouthWatcher);

            // Checks for the laryngoscope to be detected to the left of the manikin's chin.
            Watcher laryngoscopeDetectionWatcher = new Watcher();

            #region WatcherLogic

            laryngoscopeDetectionWatcher.CheckForActionDelegate = () =>
            {
                // TREY: USING FUNCTIONS
                ColorSpacePoint colorSpacePoint = Watcher.ColorSpaceFromPoint3D(this.Lesson.ManikinFaceLocation);
                Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, -80, -50, 60, 70);

                Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._laryngoscopeColor,
                                                                        rect,
                                                                        KinectManager.ColorData);

                //CameraSpacePoint point = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };
                //ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(point);
                //Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._laryngoscopeColor,
                //                                                        new Int32Rect(  (int)System.Math.Max(0, colorSpacePoint.X - 80), 
                //                                                                        (int)System.Math.Max(0, colorSpacePoint.Y - 50), 
                //                                                                        60, 
                //                                                                        70),
                //                                                        KinectManager.ColorData);

                if (!double.IsNaN(found.X) && !double.IsNaN(found.Y))
                {
                    int depth = KinectManager.GetDepthValueForColorPoint(found);
                    if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                    {
                        Point3D loc = KinectManager.Get3DPointForColorPoint(found, depth);

                        this.Lesson.LaryngoscopeLocation = loc;

                        laryngoscopeDetectionWatcher.IsTriggered = true;
                    }
                    else
                    {
                        laryngoscopeDetectionWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    laryngoscopeDetectionWatcher.IsTriggered = false;
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(laryngoscopeDetectionWatcher);
        }

        public override void UpdateStage()
        {
            if (!this.IsSetup)
                SetupStage();

            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
                this.TimeoutStage();

            // Convert the 3DPosition CameraSpace to 2DPosition ColorSpace
            CameraSpacePoint laryngoscope = new CameraSpacePoint() { X = (float)this.Lesson.LaryngoscopeLocation.X, Y = (float)this.Lesson.LaryngoscopeLocation.Y, Z = (float)this.Lesson.LaryngoscopeLocation.Z / 1000 };
            ColorSpacePoint colorPoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(laryngoscope);
            Point3D colorPoint3D = new Point3D(colorPoint.X, colorPoint.Y, laryngoscope.Z * 1000);

            this.SetDownLaryngoscopeZone.UpdatePointWithinZone(colorPoint3D);

            // The laryngoscope was found out around the left of the manikin's chin position, and also the laryngoscope is no longer found above the manikin's chin.
            if (this.SetDownLaryngoscopeZone.IsWithinZone && this.SetDownLaryngoscopeZone.TimeWithinZone > TimeSpan.FromSeconds(0.5) && !this.CurrentStageWatchers[0].IsTriggered)
            {
                if (!this.LaryngoscopeSetDown)
                {
                    this.LaryngoscopeSetDown = true;    // set flag

                    this.Lesson.DetectableActions.DetectAction("Removed Left", "Remove", "Laryngoscope", true);
                    this.Lesson.DetectableActions.DetectAction("Set Down Left Side", "Set Down", "Laryngoscope", true);

                    if (RightHandCloserToLaryngoscopeThanLeft(true))
                    {
                        this.Lesson.DetectableActions.DetectAction("Right Handed Laryngoscope Remove", "Remove Laryngoscope", "User", true);

                        this.rightHandLaryngoscopeRemoval = true;   // right
                        this.leftHandLaryngoscopeRemoval = false;
                    }
                    else
                    {
                        this.Lesson.DetectableActions.DetectAction("Left Handed Laryngoscope Remove", "Remove Laryngoscope", "User", true);

                        this.rightHandLaryngoscopeRemoval = false;
                        this.leftHandLaryngoscopeRemoval = true;    // left
                    }
                }
            }
            else
            {
                if (this.LaryngoscopeSetDown)
                {
                    this.LaryngoscopeSetDown = false;   // unset flag
                }
            }

            // Check if the Laryngoscope has been missing. 
            // The purpose of this stage is to remove the laryngoscope, if it was set down behind the mannequin - we may have missed it. 
            // If it cannot be found, then let's assume it's been removed and move on.
            if (DateTime.Now - this.lastFoundAboveMouthTime > TimeSpan.FromSeconds(1.5))
            {
                if (!this.LaryngoscopeSetDown)
                {
                    this.LaryngoscopeSetDown = true;

                    this.Lesson.DetectableActions.DetectAction("Removed", "Remove", "Laryngoscope", true);
                }
            }

            if (this.LaryngoscopeSetDown)
            {
                this.Complete = true;
                this.EndStage();
            }
        }


        private bool RightHandCloserToLaryngoscopeThanLeft(bool useDepth)
        {
            CameraSpacePoint laryngoscopeInSkeletonSpace;
            CameraSpacePoint rightHandLoc;
            CameraSpacePoint leftHandLoc;
            double rightHandDistance;
            double leftHandDistance;

            laryngoscopeInSkeletonSpace = new CameraSpacePoint() { X = (float)this.Lesson.LaryngoscopeLocation.X, Y = (float)this.Lesson.LaryngoscopeLocation.Y, Z = (float)this.Lesson.LaryngoscopeLocation.Z / 1000 };

            SkeletonProfile skele = null;

            if (this.Lesson.StuckSkeletonProfileID.HasValue)
            {
                // Retreive
                skele = KinectManager.SingleStickySkeletonProfile;
            }
            if (skele != null)
            {
                // Get the right hand and right wrist joint positions out of the profile.
                rightHandLoc = skele.JointProfiles.First(o => o.JointType == JointType.HandRight).CurrentPosition;
                leftHandLoc = skele.JointProfiles.First(o => o.JointType == JointType.HandLeft).CurrentPosition;

                if (useDepth)
                {
                    // Distance returned by KinectManager.Distance3D( SkeletalPoint a, SkeletalPoint b) is in SkeletalSpace, and therefore is represented in Meters.
                    rightHandDistance = System.Math.Abs(KinectManager.Distance3D(laryngoscopeInSkeletonSpace, rightHandLoc));     // Check the distance between the manikinChin in SkeletalSpace and the right hand. Absolute value is used to negate which point is used as the base.
                    leftHandDistance = System.Math.Abs(KinectManager.Distance3D(laryngoscopeInSkeletonSpace, leftHandLoc));     // Check the distance between the manikinChin in SkeletalSpace and the right hand. Absolute value is used to negate which point is used as the base.

                    System.Diagnostics.Debug.WriteLine(String.Format("3D: Laryngoscope -> Right Hand Distance: {0:#}mm", rightHandDistance * 1000));
                    System.Diagnostics.Debug.WriteLine(String.Format("3D: Laryngoscope -> Left Hand Distance: {0:#}mm", leftHandDistance * 1000));

                    double desiredMaxDistance = 0.515;  // meters

                    bool isRightHandOutOfRange = false;
                    bool isLeftHandOutOfRange = false;

                    if (rightHandDistance >= desiredMaxDistance)
                    {
                        // Distance between the user's head and the manikin's chin is within the desiredMaxDistance.
                        isRightHandOutOfRange = true;
                    }

                    if (leftHandDistance >= desiredMaxDistance)
                    {
                        // Distance between the user's head and the manikin's chin is within the desiredMaxDistance.
                        isLeftHandOutOfRange = true;
                    }

                    if (isLeftHandOutOfRange && isRightHandOutOfRange)
                    {
                        System.Diagnostics.Debug.WriteLine("Value Proximity Breached -> Attempting 2D comparison...");
                        return RightHandCloserToLaryngoscopeThanLeft(false);
                    }
                }
                else
                {
                    // Distance returned by KinectManager.Distance3D( SkeletalPoint a, SkeletalPoint b) is in SkeletalSpace, and therefore is represented in Meters.
                    rightHandDistance = System.Math.Abs(KinectManager.Distance2D(laryngoscopeInSkeletonSpace, rightHandLoc));     // Check the distance between the manikinChin in SkeletalSpace and the right hand. Absolute value is used to negate which point is used as the base.
                    leftHandDistance = System.Math.Abs(KinectManager.Distance2D(laryngoscopeInSkeletonSpace, leftHandLoc));     // Check the distance between the manikinChin in SkeletalSpace and the right hand. Absolute value is used to negate which point is used as the base.

                    System.Diagnostics.Debug.WriteLine(String.Format("2D: Laryngoscope -> Right Hand Distance: {0:#}pixels", rightHandDistance * 1000));
                    System.Diagnostics.Debug.WriteLine(String.Format("2D: Laryngoscope -> Left Hand Distance: {0:#}pixels", leftHandDistance * 1000));
                }

                if (rightHandDistance < leftHandDistance)   // if the Right Hand is closer (less than) the Left Hand to the Syringe
                    return true;
            }

            return false;
        }

        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }

        public override void EndStage( bool playSound = true )
        {
            this.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            // Get the current time to signify the time the stage ended.
            this.EndTime = DateTime.Now;

            // Create a screenshot for the current stage.
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                Intubation lesson = (Intubation)args.Argument;

                KinectImage image = KinectImage.TakeKinectSnapshot(true, true);

                lesson.RemoveLaryngoscopeImage = image;
            };

            worker.RunWorkerAsync(this.Lesson);

            AssessLaryngoscopeRemoval();

            this.Lesson.CurrentStageIndex++;

            if( playSound ) Lesson.PlayNextStageSound();
        }

        private void AssessLaryngoscopeRemoval()
        {
            double score = 1;

            if (this.LaryngoscopeSetDown)
            {
                if (this.leftHandLaryngoscopeRemoval)
                {
                    this.Lesson.RemoveLaryngoscopeFeedbackIDs.Add(48);   // The Laryngoscope was removed properly with the left hand.
                }
                else if (this.rightHandLaryngoscopeRemoval)
                {
                    score -= 0.5;
                    this.Lesson.RemoveLaryngoscopeFeedbackIDs.Add(49);   // The Laryngocope was removed improperly with the right hand.
                }
                else
                {
                    // Default to simply "removed" if the hands wern't determined.
                    this.Lesson.RemoveLaryngoscopeFeedbackIDs.Add(50);   // The Laryngoscope was removed.
                }
            }
            else
            {
                score -= 1;
                this.Lesson.RemoveLaryngoscopeFeedbackIDs.Add(51);   // The Laryngscope was not removed.
            }

            this.Lesson.RemoveLaryngoscopeScore = System.Math.Max(0, System.Math.Min(score, 1)); // Clamp the score between 0 and 1.
        }

        public override void PauseStage()
        {
            this.Lesson.Paused = true;
            this.Lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.Lesson.Paused = false;
            this.Lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void ResetStage()
        {

        }

        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            //this.PauseStage();
            this.TimedOut = true;
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            this.RequestStageVideo();   // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
        }

        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.Lesson.IntubationPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Intubation/Videos/Stage 007.wmv", UriKind.Relative));
            this.Lesson.IntubationPage.VideoPlayer.startVideo(this, new RoutedEventArgs());

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        public override void WipeClean()
        {
            setDownLaryngoscopeZone = null;
            laryngoscopeSetDown = false;

            rightHandLaryngoscopeRemoval = false;
            leftHandLaryngoscopeRemoval = false;

            isLaryngoscopeStillAboveMouth = false;

            lastFoundAboveMouthTime = DateTime.MaxValue;

            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.IsSetup = false;

            if (this.StageScore != null)
            {
                this.StageScore.WipeClean();
                this.StageScore = null;
                this.StageScore = new StageScore();
            }
            else
            {
                this.StageScore = new StageScore();
            }

            this.Active = false;
            this.Complete = false;


            if (this.Zones != null)
            {
                this.Zones.Clear();
            }
            else
            {
                this.Zones = new List<Zone>();
            }

            if (this.Stopwatch != null)
            {
                this.Stopwatch.Reset();
            }
            else
            {
                this.Stopwatch = new System.Diagnostics.Stopwatch();
            }

            if (this.TimeoutStopwatch != null)
                this.TimeoutStopwatch.Reset();
            else this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();

            this.TimedOut = false;
        }
    }
}