﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using System.Windows.Media.Imaging;
using System.Windows;
using System.ComponentModel;
using System.Diagnostics;

using Microsoft.Kinect;

using AIMS.Assets.Lessons2;

namespace AIMS.Tasks.Intubation
{
    public class IntubationStage8 : Stage
    {
        private Intubation lesson;
        public Intubation Lesson { get { return lesson; } set { lesson = value; } }

        private RecZone removeStyletZone = null;
        public RecZone RemoveStyletZone { get { return removeStyletZone; } set { removeStyletZone = value; } }

        private bool styletRemoved = false;
        private bool styletFoundOverManikinChin = false;
        private bool styletFoundOverEndotrachealTube = false;
        private bool styletRemovalLiftGuestureDetected = false;

        public bool StyletRemoved { get { return styletRemoved; } set { styletRemoved = value; } }
        public bool StyletFoundOverManikinChin { get { return styletFoundOverManikinChin; } set { styletFoundOverManikinChin = value; } }
        public bool StyletFoundOverEndotrachealTube { get { return styletFoundOverEndotrachealTube; } set { styletFoundOverEndotrachealTube = value; } }
        public bool StyletRemovalLiftGuestureDetected { get { return styletRemovalLiftGuestureDetected; } set { styletRemovalLiftGuestureDetected = value; } }

        private ColorRange _intubationStyletColor;

        public IntubationStage8(Intubation intubation)
        {
            Lesson = intubation;

            StageNumber = 8;
            Name = "Remove Stylet";
            Instruction = "Remove the stylet from the Endotracheal Tube.";
            TargetTime = TimeSpan.FromSeconds(3.5);
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Tasks/Intubation/Graphics/Intubation008.png";
            Active = false;
            Complete = false;
            ScoreWeight = 0.10;
            Zones = new List<Zone>();
            IsSetup = false;
            Stopwatch = new System.Diagnostics.Stopwatch();
            this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.CanTimeOut = false;
            this.TimeOutTime = TimeSpan.FromSeconds(10.0);
        }

        public override void SetupStage()
        {
            this.Lesson.IntubationPage.StageInfoTextBlock.Text = this.Name;
            this.Lesson.IntubationPage.InstructionTextBlock.Text = this.Instruction;

            this.Lesson.IntubationPage.flow.Index = this.Lesson.CurrentStageIndex;  // change cover flow image.

            // Set the image for the current stage.
            this.Lesson.IntubationPage.CurrentStageImage.Source = new BitmapImage(new Uri("/Tasks/Intubation/Graphics/Intubation008.png", UriKind.Relative));

            try
            {
                this._intubationStyletColor = ColorTracker.GetObjectColorRange("intubation_stylet").ColorRange;
            }
            catch (Exception exc) { Debug.Print("IntubationStage8 SetupStage() ERROR: {0}", exc.Message); }

            this.Zones.Clear(); // Clear the stage zone list.

            this.Lesson.IntubationPage.zoneOverlay.ClearZones();

            // These pixels are in depth space... but are now color space
            int scale_x = Intubation.SCALE_X;
            int scale_y = Intubation.SCALE_Y;

            //zone left of the face
            RemoveStyletZone = new RecZone();
            RemoveStyletZone.X = this.Lesson.ManikinFaceLocation.X - (20 * scale_x);
            RemoveStyletZone.Y = this.Lesson.ManikinFaceLocation.Y - (125 * scale_y);
            RemoveStyletZone.Name = "Remove Stylet Zone";
            //RemoveStyletZone.ZoneType = ZoneType.Position;
            RemoveStyletZone.Width = 90 * scale_x;
            RemoveStyletZone.Height = 90 * scale_y;
            RemoveStyletZone.Depth = 0;
            RemoveStyletZone.AcceptedJointType = JointType.HandRight;

            this.Zones.Add(RemoveStyletZone);

            // Add the zones into the zoneOverlay
            foreach (Zone zone in this.Zones)
            {
                this.Lesson.IntubationPage.zoneOverlay.AddZone(zone);
            }

            //redCounter = 0;

            this.CurrentStageWatchers = new List<BaseWatcher>();
            this.OutlyingStageWatchers = new List<BaseWatcher>();
            this.PeripheralStageWatchers = new List<BaseWatcher>();

            CreateStageWatchers();

            StartTime = DateTime.Now;

            this.IsSetup = true;

            Stopwatch.Start();

            this.TimeoutStopwatch.Start();
        }

        public void CreateStageWatchers()
        {
            // Checks for the stylet to be pulled out over the manikin's chin.
            Watcher styletDetectedOverMouthWatcher = new Watcher();

            #region WatcherLogic

            styletDetectedOverMouthWatcher.CheckForActionDelegate = () =>
            {
                if (this.Lesson.CurrentStageIndex < 8 || this.Lesson.CurrentStageIndex > 9)
                    return;

                // TREY: USING FUNCTIONS
                ColorSpacePoint colorSpacePoint = Watcher.ColorSpaceFromPoint3D(this.Lesson.ManikinFaceLocation);
                Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, -30, -80, 60, 60);
                // TREY: AGAIN, THIS HAS A ZERO VALUE FOR NUMNEIGHBORS?
                Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._intubationStyletColor,
                                                                        rect,
                                                                        KinectManager.ColorData, 0);


                //CameraSpacePoint point = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };
                //ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(point);
                //Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._intubationStyletColor, 
                //                                                        new Int32Rect(  (int)System.Math.Max(0, colorSpacePoint.X - 30), 
                //                                                                        (int)System.Math.Max(0, colorSpacePoint.Y - 80), 
                //                                                                        60, 
                //                                                                        60), 
                //                                                        KinectManager.ColorData, 0);

                if (!double.IsNaN(found.X) && !double.IsNaN(found.Y))
                {
                    int depth = KinectManager.GetDepthValueForColorPoint(found);
                    if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                    {
                        Point3D loc = KinectManager.Get3DPointForColorPoint(found, depth);

                        this.Lesson.StyletLocation = loc;

                        styletDetectedOverMouthWatcher.IsTriggered = true;
                    }
                    else
                    {
                        styletDetectedOverMouthWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    styletDetectedOverMouthWatcher.IsTriggered = false;
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(styletDetectedOverMouthWatcher);

            // Checks for the stylet to be pulled out over the endotracheal tube.
            Watcher styletDetectedOverEndotrachealTubeWatcher = new Watcher();

            #region WatcherLogic

            styletDetectedOverEndotrachealTubeWatcher.CheckForActionDelegate = () =>
            {
                if (this.Lesson.CurrentStageIndex < 8 || this.Lesson.CurrentStageIndex > 9)
                    return;

                // TREY: USING FUNCTIONS
                ColorSpacePoint colorSpacePoint = Watcher.ColorSpaceFromPoint3D(this.Lesson.EndotrachealTubeLocation);
                Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, -15, -55, 30, 50);
                
                // TREY: STAGE 8 WATCHER HAS A numNeighbors=0 Parameter?
                Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._intubationStyletColor,
                                                                        rect,
                                                                        KinectManager.ColorData, 0);

                //CameraSpacePoint point = new CameraSpacePoint() { X = (float)this.Lesson.EndotrachealTubeLocation.X, Y = (float)this.Lesson.EndotrachealTubeLocation.Y, Z = (float)this.Lesson.EndotrachealTubeLocation.Z / 1000 };
                //ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(point);


                //Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._intubationStyletColor, 
                //                                                        new Int32Rect(  (int)System.Math.Max(0, colorSpacePoint.X - 15), 
                //                                                                        (int)System.Math.Max(0, colorSpacePoint.Y - 55), 
                //                                                                        30, 
                //                                                                        50), 
                //                                                        KinectManager.ColorData, 0);

                if (!double.IsNaN(found.X) && !double.IsNaN(found.Y))
                {
                    int depth = KinectManager.GetDepthValueForColorPoint(found);
                    if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                    {
                        Point3D loc = KinectManager.Get3DPointForColorPoint(found, depth);

                        this.Lesson.StyletLocation = loc;

                        styletDetectedOverEndotrachealTubeWatcher.IsTriggered = true;
                    }
                    else
                    {
                        styletDetectedOverEndotrachealTubeWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    styletDetectedOverEndotrachealTubeWatcher.IsTriggered = false;
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(styletDetectedOverEndotrachealTubeWatcher);

            Watcher endotrachealTubeDetectedOverMouthWatcher = new Watcher();

            #region WatcherLogic

            // Checks if the Endotracheal Tube remains inserted into the mouth of the manikin.
            // ( We want this to flag true, though we also know that the hand may cover the tip of the endotracheal tube when the user goes to stabalize it. )
            endotrachealTubeDetectedOverMouthWatcher.CheckForActionDelegate = () =>
            {
                
            };

            #endregion

            this.CurrentStageWatchers.Add(endotrachealTubeDetectedOverMouthWatcher);

            // Detects lift motions being made by the user, indicating that the stylet is being lifted out of the endotracheal tube.
            Watcher liftGuestureDetectedOverMouthWatcher = new Watcher();

            #region WatcherLogic

            liftGuestureDetectedOverMouthWatcher.CheckForActionDelegate = () =>
            {
                CameraSpacePoint endotrachealTubeInSkeletalSpace;  // This is used to determine whether or not the hands are near the endotracheal tube. If they're too far away, then they're not the ones doing the lifting.
                CameraSpacePoint lHandLoc;
                CameraSpacePoint lWristLoc;
                CameraSpacePoint lElbowLoc;
                CameraSpacePoint rHandLoc;
                CameraSpacePoint rWristLoc;
                CameraSpacePoint rElbowLoc;
                double lHandDistanceToTube;
                double lWristDistanceToTube;
                double rHandDistanceToTube;
                double rWristDistanceToTube;

                endotrachealTubeInSkeletalSpace = new CameraSpacePoint() { X = (float)this.Lesson.EndotrachealTubeLocation.X, Y = (float)this.Lesson.EndotrachealTubeLocation.Y, Z = (float)this.Lesson.EndotrachealTubeLocation.Z / 1000 };

                // Retreive Tracked Skeleton
                SkeletonProfile skele = KinectManager.SingleStickySkeletonProfile;

                if (skele != null)
                {
                    // Get the left hand and left wrist joint positions out of the profile.
                    lHandLoc = skele.JointProfiles.First(o => o.JointType == JointType.HandLeft).CurrentPosition;
                    lWristLoc = skele.JointProfiles.First(o => o.JointType == JointType.WristLeft).CurrentPosition;

                    rHandLoc = skele.JointProfiles.First(o => o.JointType == JointType.HandRight).CurrentPosition;
                    rWristLoc = skele.JointProfiles.First(o => o.JointType == JointType.WristRight).CurrentPosition;

                    // Distance returned by KinectManager.Distance3D( SkeletalPoint a, SkeletalPoint b) is in SkeletalSpace, and therefore is represented in Meters.
                    lHandDistanceToTube = System.Math.Abs(KinectManager.Distance3D(endotrachealTubeInSkeletalSpace, lHandLoc));     // Check the distance between the manikinChin in SkeletalSpace and the left hand. Absolute value is used to negate which point is used as the base.
                    lWristDistanceToTube = System.Math.Abs(KinectManager.Distance3D(endotrachealTubeInSkeletalSpace, lWristLoc));   // Check the distance between the manikinChin in the SkeletalSpace and the left wrist. Absolute value is used to negate which point is used as the base.

                    rHandDistanceToTube = System.Math.Abs(KinectManager.Distance3D(endotrachealTubeInSkeletalSpace, rHandLoc));     // Check the distance between the manikinChin in SkeletalSpace and the left hand. Absolute value is used to negate which point is used as the base.
                    rWristDistanceToTube = System.Math.Abs(KinectManager.Distance3D(endotrachealTubeInSkeletalSpace, rWristLoc));   // Check the distance between the manikinChin in the SkeletalSpace and the left wrist. Absolute value is used to negate which point is used as the base.

                    double desiredMaxDistance = 0.500;  // 500 milimeters
                    bool detectedLift = false;

                    // Check if the left hand or wrist is found near the endotracheal tube.
                    if (lHandDistanceToTube <= desiredMaxDistance || lWristDistanceToTube <= desiredMaxDistance)
                    {
                        // Check if their positions are above the elbow of the user.
                        lElbowLoc = skele.JointProfiles.First(o => o.JointType == JointType.ElbowLeft).CurrentPosition;

                        if (lWristLoc.Y > lElbowLoc.Y || lHandLoc.Y > lElbowLoc.Y)
                        {
                            // Either the left wrist or hand is above the left elbow.
                            detectedLift = true;
                        }
                    }

                    // Check if the left hand or wrist is found near the endotracheal tube.
                    if (rHandDistanceToTube <= desiredMaxDistance || rWristDistanceToTube <= desiredMaxDistance)
                    {
                        // Check if their positions are above the elbow of the user.
                        rElbowLoc = skele.JointProfiles.First(o => o.JointType == JointType.ElbowRight).CurrentPosition;

                        if (rWristLoc.Y > rElbowLoc.Y || rHandLoc.Y > rElbowLoc.Y)
                        {
                            // Either the left wrist or hand is above the left elbow.
                            detectedLift = true;
                        }
                    }

                    if( detectedLift )
                    {
                        // If it is within the disiredMaxDistance, flip our trigger switch on.
                        liftGuestureDetectedOverMouthWatcher.IsTriggered = true;
                    }
                    else
                    {
                        // If it's outside the desiredMaxDistance, then flip our trigger switch off.
                        liftGuestureDetectedOverMouthWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    // If no skeleton was found, then the hand/wrist can't be near the face..
                    liftGuestureDetectedOverMouthWatcher.IsTriggered = false;
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(liftGuestureDetectedOverMouthWatcher);
        }

        public override void UpdateStage()
        {
            if (!this.IsSetup)
                SetupStage();

            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
                this.TimeoutStage();

            if (this.CurrentStageWatchers[0].IsTriggered)
            {
                // The stylet's color was found over the mouth of the manikin.
                if (!StyletFoundOverManikinChin)
                {
                    this.StyletFoundOverManikinChin = true;   // set flag

                    this.Lesson.DetectableActions.DetectAction("Removed", "Remove", "Stylet", true);
                }
            }
            else
            {
                if (StyletFoundOverManikinChin)
                    this.StyletFoundOverManikinChin = false;  // unset flag
            }

            if (this.CurrentStageWatchers[1].IsTriggered)
            {
                // The stylet's color was found above the endotracheal tube.
                if (!StyletFoundOverEndotrachealTube)
                {
                    this.StyletFoundOverEndotrachealTube = true;    // set flag

                    this.Lesson.DetectableActions.DetectAction("Removed", "Remove", "Stylet", true);
                }
            }

            if (this.CurrentStageWatchers[2].IsTriggered)
            {
                //The endotracheal tube remains over the mouth of the manikin.
            }

            if (this.CurrentStageWatchers[3].IsTriggered)
            {
                // A lift guesture was detected around where the stylet should be lifted out of the endotracheal tube.
                if (!StyletRemovalLiftGuestureDetected)
                {
                    this.StyletRemovalLiftGuestureDetected = true;  // set flag

                    this.Lesson.DetectableActions.DetectAction("Removed Stylet", "Remove Stylet", "User", true);
                }
            }
            else
            {
                /*if (StyletRemovalLiftGuestureDetected)
                    this.StyletRemovalLiftGuestureDetected = false; // unset flag*/             // NOTE: This is being commented out until BaseWatcher.HasTriggered is implemeneted.
            }

            if (this.StyletFoundOverEndotrachealTube /*|| this.StyletFoundOverManikinChin*/ // Either the stylet was found over the chin/tube
                || (this.StyletRemovalLiftGuestureDetected && this.Stopwatch.Elapsed >= TimeSpan.FromSeconds(0.75) ) ) // Or a lift guesture was detected AND enough time elapsed.
                this.StyletRemoved = true;

            if (StyletRemoved)
            {
                this.Complete = true;
                this.EndStage();
            }
        }

        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }

        public override void EndStage( bool playSound = true )
        {
            this.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            // Get the current time to signify the time the stage ended.
            this.EndTime = DateTime.Now;

            // Create a screenshot for the current stage.
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                Intubation lesson = (Intubation)args.Argument;

                KinectImage image = KinectImage.TakeKinectSnapshot(true, true);

                lesson.RemoveStyletImage = image;
            };

            worker.RunWorkerAsync(this.Lesson);

            AssessStyletRemoval();

            this.Lesson.CurrentStageIndex++;

            if( playSound ) Lesson.PlayNextStageSound();
        }

        private void AssessStyletRemoval()
        {
            double score = 1;


            if (!StyletFoundOverEndotrachealTube && !StyletFoundOverManikinChin && StyletRemovalLiftGuestureDetected)
            {
                //score -= 0.05;    // Score is not deducted. The stylet is a very small instrument at range given the resolution of the Kinect's camera. If we can't detect the color, but see the lifting motion, give them credit.

                this.Lesson.RemoveStyletFeedbackIDs.Add(52); // A lifting motion was detected, but the stylet was not located.
            }
            else if (StyletRemoved)
            {
                this.Lesson.RemoveStyletFeedbackIDs.Add(53); // The Stylet was removed.
            }
            else
            {
                score -= 1;
                this.Lesson.RemoveStyletFeedbackIDs.Add(54); // The Stylet was not removed.
            }

            this.Lesson.RemoveStyletScore = System.Math.Max(0, System.Math.Min(score, 1)); // Clamp the score between 0 and 1.
        }

        public override void PauseStage()
        {
            this.Lesson.Paused = true;
            this.Lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.Lesson.Paused = false;
            this.Lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void ResetStage()
        {

        }

        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            //this.PauseStage();
            this.TimedOut = true;
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            this.RequestStageVideo();   // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
        }

        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.Lesson.IntubationPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Intubation/Videos/Stage 008.wmv", UriKind.Relative));
            this.Lesson.IntubationPage.VideoPlayer.startVideo(this, new RoutedEventArgs());

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        public override void WipeClean()
        {
            this.RemoveStyletZone = null;
            this.StyletRemovalLiftGuestureDetected = false;
            this.StyletFoundOverManikinChin = false;
            this.StyletRemoved = false;
            this.StyletFoundOverEndotrachealTube = false;

            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.IsSetup = false;

            if (this.StageScore != null)
            {
                this.StageScore.WipeClean();
                this.StageScore = null;
                this.StageScore = new StageScore();
            }
            else
            {
                this.StageScore = new StageScore();
            }


            this.Active = false;
            this.Complete = false;


            if (this.Zones != null)
            {
                this.Zones.Clear();
            }
            else
            {
                this.Zones = new List<Zone>();
            }

            if (this.Stopwatch != null)
            {
                this.Stopwatch.Reset();
            }
            else
            {
                this.Stopwatch = new System.Diagnostics.Stopwatch();
            }

            if (this.TimeoutStopwatch != null)
                this.TimeoutStopwatch.Reset();
            else this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();

            this.TimedOut = false;
        }
    }
}