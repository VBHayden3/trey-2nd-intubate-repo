﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using System.Windows.Media.Imaging;
using System.Windows;
using System.ComponentModel;
using System.Diagnostics;

using Microsoft.Kinect;

using AIMS.Assets.Lessons2;

namespace AIMS.Tasks.Intubation
{
    public class IntubationStage9 : Stage
    {
        private Intubation lesson;
        public Intubation Lesson { get { return lesson; } set { lesson = value; } }

        private RecZone grabSyringeZone = null;
        private RecZone syringeOverMouthZone = null;
        private bool hasFoundSyringeColor = false;
        private Point3D foundSyringePoint = new Point3D();
        private bool syringeInPlace = false;
        private bool syringePickedUp = false;
        private bool syringeZoneCreated = false;
        private int greenCounter = 0;
        private bool syringeSetDown = false;
        private RecZone syringeSetDownZone = null;
        private bool syringeOverMouth = false;
        private int syringeDepthCounter = 0;
        private ColorRange _intubationSyringeColor;

        public RecZone GrabSyringeZone { get { return grabSyringeZone; } set { grabSyringeZone = value; } }
        public RecZone SyringeOverMouthZone { get { return syringeOverMouthZone; } set { syringeOverMouthZone = value; } }
        public bool HasFoundSyringeColor { get { return hasFoundSyringeColor; } set { hasFoundSyringeColor = value; } }
        public Point3D FoundSyringePoint { get { return foundSyringePoint; } set { foundSyringePoint = value; } }
        public bool SyringeInPlace { get { return syringeInPlace; } set { syringeInPlace = value; } }
        public bool SyringePickedUp { get { return syringePickedUp; } set { syringePickedUp = value; } }
        public bool SyringeZoneCreated { get { return syringeZoneCreated; } set { syringeZoneCreated = value; } }
        public int GreenCounter { get { return greenCounter; } set { greenCounter = value; } }
        public bool SyringeSetDown { get { return syringeSetDown; } set { syringeSetDown = value; } }
        public RecZone SyringeSetDownZone { get { return syringeSetDownZone; } set { syringeSetDownZone = value; } }
        public bool SyringeOverMouth { get { return syringeOverMouth; } set { syringeOverMouth = value; } }
        public int SyringeDepthCounter { get { return syringeDepthCounter; } set { syringeDepthCounter = value; } }

        public IntubationStage9(Intubation intubation)
        {
            Lesson = intubation;

            StageNumber = 9;
            Name = "Compress Syringe";
            Instruction = "Use the syringe to inflate the balloon cuff, expanding the wind pipe.";
            TargetTime = TimeSpan.FromSeconds(3.5);
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Tasks/Intubation/Graphics/Intubation009.png";
            Active = false;
            Complete = false;
            ScoreWeight = 0.05;
            Zones = new List<Zone>();
            IsSetup = false;
            Stopwatch = new System.Diagnostics.Stopwatch();
            this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.CanTimeOut = false;
            this.TimeOutTime = TimeSpan.FromSeconds(10.0);
        }

        public override void SetupStage()
        {
            this.Lesson.IntubationPage.StageInfoTextBlock.Text = this.Name;
            this.Lesson.IntubationPage.InstructionTextBlock.Text = this.Instruction;

            this.Lesson.IntubationPage.flow.Index = this.Lesson.CurrentStageIndex;  // change cover flow image.

            // Set the image for the current stage.
            this.Lesson.IntubationPage.CurrentStageImage.Source = new BitmapImage(new Uri("/Tasks/Intubation/Graphics/Intubation009.png", UriKind.Relative));

            try
            {
                this._intubationSyringeColor = ColorTracker.GetObjectColorRange("intubation_syringe").ColorRange;
            }
            catch (Exception exc) { Debug.Print("IntubationStage9 SetupStage() ERROR: {0}", exc.Message); }

            this.Zones.Clear(); // Clear the stage zone list.

            this.Lesson.IntubationPage.zoneOverlay.ClearZones();

            // These pixels are in depth space... but are now color space
            int scale_x = Intubation.SCALE_X;
            int scale_y = Intubation.SCALE_Y;

            //zone right of the face
            GrabSyringeZone = new RecZone();
            GrabSyringeZone.X = this.Lesson.ManikinFaceLocation.X + (20 * scale_x);
            GrabSyringeZone.Y = this.Lesson.ManikinFaceLocation.Y - (50 * scale_y);
            GrabSyringeZone.Name = "Grab Syringe Zone";
            //GrabSyringeZone.ZoneType = ZoneType.Position;
            GrabSyringeZone.Width = 80 * scale_x;
            GrabSyringeZone.Height = 85 * scale_y;
            GrabSyringeZone.Depth = 0;
            GrabSyringeZone.AcceptedJointType = JointType.HandRight;
            GrabSyringeZone.DoesTrackJoint = false;

            this.Zones.Add(GrabSyringeZone);

            // Add the zones into the zoneOverlay
            foreach (Zone zone in this.Zones)
            {
                this.Lesson.IntubationPage.zoneOverlay.AddZone(zone);
            }

            greenCounter = 0;

            this.CurrentStageWatchers = new List<BaseWatcher>();
            this.OutlyingStageWatchers = new List<BaseWatcher>();
            this.PeripheralStageWatchers = new List<BaseWatcher>();

            CreateStageWatchers();

            StartTime = DateTime.Now;

            this.IsSetup = true;

            Stopwatch.Start();

            this.TimeoutStopwatch.Start();
        }

        public void CreateStageWatchers()
        {
            // Checks for the syringe to be pulled out over the manikin's chin.
            Watcher syringeDetectedOverMouthWatcher = new Watcher();

            #region WatcherLogic

            syringeDetectedOverMouthWatcher.CheckForActionDelegate = () =>
            {
                if (this.Lesson.CurrentStageIndex < 8 || this.Lesson.CurrentStageIndex > 9)
                    return;
                // TREY: USING FUNCTIONS
                ColorSpacePoint colorSpacePoint = Watcher.ColorSpaceFromPoint3D(this.Lesson.ManikinFaceLocation);
                Int32Rect rect = Watcher.RectFromReference(colorSpacePoint, 20, -50, 80, 55);
                Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._intubationSyringeColor,
                                                                        rect,
                                                                        KinectManager.ColorData);


                //CameraSpacePoint point = new CameraSpacePoint() { X = (float)this.Lesson.ManikinFaceLocation.X, Y = (float)this.Lesson.ManikinFaceLocation.Y, Z = (float)this.Lesson.ManikinFaceLocation.Z / 1000 };
                //ColorSpacePoint colorSpacePoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(point);
                //Point found = AIMS.Assets.Lessons2.ColorTracker.find(   this._intubationSyringeColor, 
                //                                                        new Int32Rect(  (int)System.Math.Max(0, colorSpacePoint.X + 20), 
                //                                                                        (int)System.Math.Max(0, colorSpacePoint.Y - 50), 
                //                                                                        80, 
                //                                                                        55), 
                //                                                        KinectManager.ColorData);

                if (!double.IsNaN(found.X) && !double.IsNaN(found.Y))
                {
                    int depth = KinectManager.GetDepthValueForColorPoint(found);
                    if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                    {
                        Point3D loc = KinectManager.Get3DPointForColorPoint(found, depth);

                        this.Lesson.SyringeLocation = loc;

                        syringeDetectedOverMouthWatcher.IsTriggered = true;
                    }
                    else
                    {
                        syringeDetectedOverMouthWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    syringeDetectedOverMouthWatcher.IsTriggered = false;
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(syringeDetectedOverMouthWatcher);
            this.PeripheralStageWatchers.Add(syringeDetectedOverMouthWatcher);
        }

        public override void UpdateStage()
        {
            if (!this.IsSetup)
                SetupStage();

            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
                this.TimeoutStage();

            if (this.CurrentStageWatchers != null)
            {
                if (this.CurrentStageWatchers[0].IsTriggered)
                {
                    // The syringe was found to the right of the manikin's chin.
                    if (!this.SyringePickedUp)
                        this.SyringePickedUp = true;    // set flag.

                    this.Lesson.DetectableActions.DetectAction("Picked Up", "Pick Up", "Syringe", true);
                    this.Lesson.DetectableActions.DetectAction("Used", "Use", "Syringe", true);
                    this.Lesson.DetectableActions.DetectAction("Put Down", "Set Down", "Syringe", true);

                    if (RightHandCloserToSyringeThanLeft())
                    {
                        this.Lesson.DetectableActions.DetectAction("Syringe Used", "Use Syringe", "User", true);
                    }
                    else
                    {
                        this.Lesson.DetectableActions.DetectAction("Syringe Used", "Use Syringe", "User", true);
                    }
                }
                else
                {
                    if (this.SyringePickedUp)
                        this.SyringePickedUp = false;   // unset flag.
                }
            }
            
            if (this.SyringePickedUp)
            {
                this.Complete = true;
                this.EndStage();
            }
        }


        private bool RightHandCloserToSyringeThanLeft()
        {

            CameraSpacePoint syringeInSkeletonSpace;
            CameraSpacePoint rightHandLoc;
            CameraSpacePoint leftHandLoc;
            double rightHandDistance;
            double leftHandDistance;

            syringeInSkeletonSpace = new CameraSpacePoint() { X = (float)this.Lesson.SyringeLocation.X, Y = (float)this.Lesson.SyringeLocation.Y, Z = (float)this.Lesson.SyringeLocation.Z / 1000 };

            SkeletonProfile skele = null;

            if (this.Lesson.StuckSkeletonProfileID.HasValue)
            {
                // Retreive
                skele = KinectManager.SingleStickySkeletonProfile;
            }
            if (skele != null)
            {
                // Get the right hand and right wrist joint positions out of the profile.
                rightHandLoc = skele.JointProfiles.First(o => o.JointType == JointType.HandRight).CurrentPosition;
                leftHandLoc = skele.JointProfiles.First(o => o.JointType == JointType.HandLeft).CurrentPosition;

                // Distance returned by KinectManager.Distance3D( SkeletalPoint a, SkeletalPoint b) is in SkeletalSpace, and therefore is represented in Meters.
                // Check the distance between the manikinChin in SkeletalSpace and the right hand. 
                // Absolute value is used to negate which point is used as the base.
                rightHandDistance = System.Math.Abs(KinectManager.Distance3D(syringeInSkeletonSpace, rightHandLoc));
                // Check the distance between the manikinChin in SkeletalSpace and the left hand. 
                // Absolute value is used to negate which point is used as the base.
                leftHandDistance = System.Math.Abs(KinectManager.Distance3D(syringeInSkeletonSpace, leftHandLoc));     
               
                System.Diagnostics.Debug.WriteLine(String.Format("Syringe -> Right Hand Distance: {0:#}mm", rightHandDistance * 1000));
                System.Diagnostics.Debug.WriteLine(String.Format("Syringe -> Left Hand Distance: {0:#}mm", leftHandDistance * 1000));

                /*
                double desiredMaxDistance = 0.515;  // meters

                bool isRightHandOutOfRange = false;
                bool isLeftHandOutOfRange = false;

                if (rightHandDistance <= desiredMaxDistance)
                {
                    // Distance between the user's head and the manikin's chin is within the desiredMaxDistance.
                    isRightHandOutOfRange = true;
                }

                if (leftHandDistance <= desiredMaxDistance)
                {
                    // Distance between the user's head and the manikin's chin is within the desiredMaxDistance.
                    isRightHandOutOfRange = true;
                }*/

                if (rightHandDistance < leftHandDistance)   // if the Right Hand is closer (less than) the Left Hand to the Syringe
                    return true;
            }

            return false;
        }

        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }

        public override void EndStage( bool playSound = true )
        {
            this.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            // Get the current time to signify the time the stage ended.
            this.EndTime = DateTime.Now;

            // Create a screenshot for the current stage.
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                Intubation lesson = (Intubation)args.Argument;

                KinectImage image = KinectImage.TakeKinectSnapshot(true, true);

                lesson.UseSyringeImage = image;
            };

            worker.RunWorkerAsync(this.Lesson);

            AssessSyringeUsage();

            this.Lesson.CurrentStageIndex++;

            if( playSound ) Lesson.PlayNextStageSound();
        }

        private void AssessSyringeUsage()
        {
            double score = 1;

            if (SyringeInPlace)
            {
                this.Lesson.UseSyringeFeedbackIDs.Add(55);   // The Syringe was used to inflate the Baloon Cuff.
            }
            else if (SyringePickedUp)   // Seeing as we cannot detect thumb motion, we must infer that "pickup" and "use" are synonymous.
            {
                this.Lesson.UseSyringeFeedbackIDs.Add(55);   // The Syringe was used to inflate the Baloon CUff.
            }
            else
            {
                score -= 1;
                this.Lesson.UseSyringeFeedbackIDs.Add(56);   // The Syringe was not used.
            }

            this.Lesson.UseSyringeScore = System.Math.Max(0, System.Math.Min(score, 1)); // Clamp the score between 0 and 1.
        }
        
        public override void PauseStage()
        {
            this.Lesson.Paused = true;
            this.Lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.Lesson.Paused = false;
            this.Lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void ResetStage()
        {

        }

        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            //this.PauseStage();
            this.TimedOut = true;
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            this.RequestStageVideo();   // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
        }

        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.Lesson.IntubationPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Intubation/Videos/Stage 009.wmv", UriKind.Relative));
            this.Lesson.IntubationPage.VideoPlayer.startVideo(this, new RoutedEventArgs());

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.IntubationPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        public override void WipeClean()
        {
            grabSyringeZone = null;
            syringeOverMouthZone = null;
            hasFoundSyringeColor = false;
            foundSyringePoint = new Point3D();
            syringeInPlace = false;
            syringePickedUp = false;
            syringeZoneCreated = false;
            greenCounter = 0;
            syringeSetDown = false;
            syringeSetDownZone = null;
            syringeOverMouth = false;
            syringeDepthCounter = 0;

            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.IsSetup = false;

            if (this.StageScore != null)
            {
                this.StageScore.WipeClean();
                this.StageScore = null;
                this.StageScore = new StageScore();
            }
            else
            {
                this.StageScore = new StageScore();
            }


            this.Active = false;
            this.Complete = false;

            if (this.Zones != null)
            {
                this.Zones.Clear();
            }
            else
            {
                this.Zones = new List<Zone>();
            }

            if (this.Stopwatch != null)
            {
                this.Stopwatch.Reset();
            }
            else
            {
                this.Stopwatch = new System.Diagnostics.Stopwatch();
            }

            if (this.TimeoutStopwatch != null)
                this.TimeoutStopwatch.Reset();
            else this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();

            this.TimedOut = false;
        }
    }
}
