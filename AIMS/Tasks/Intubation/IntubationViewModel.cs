﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace AIMS.Tasks.Intubation
{
    public class IntubationViewModel : DependencyObject
    {   
        /*
        public static readonly DependencyProperty KinectSensorManagerProperty =
            DependencyProperty.Register(
                "KinectSensorManager",
                typeof(KinectSensorManager),
                typeof(IntubationViewModel),
                new PropertyMetadata(null));
        */
        
        public static readonly DependencyProperty IntubationLessonProperty =
            DependencyProperty.Register(
                "IntubationLesson",
                typeof(Intubation),
                typeof(IntubationViewModel),
                new PropertyMetadata(null, IntubationLessonObjectChanged));

        //public KinectSensorManager KinectSensorManager { get { return (KinectSensorManager)GetValue(KinectSensorManagerProperty); } set { SetValue(KinectSensorManagerProperty, value); } }
        public Intubation Intubation { get { return (Intubation)GetValue(IntubationLessonProperty); } set { SetValue(IntubationLessonProperty, value); } }

        //Change Handlers
        private static void IntubationLessonObjectChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs args)
        {
            IntubationViewModel ivm = dependencyObject as IntubationViewModel;
            
            // What happens when the view model changes

            /*
            if ((ivm == null) || (ivm.KinectSensorManager == null) || !(args.NewValue is Intubation))
            {
                return;
            }

            
            var isEnabled = (bool)args.NewValue;

            if (!isEnabled || (null == ivm.KinectSensorManager) || (ColorImageFormat.Undefined == ivm.KinectSensorManager.ColorFormat))
            {
                ivm.SelectedColorMode = ColorMode.Off;
            }
            else
            {
                ivm.SelectedColorMode = (ColorMode)ivm.KinectSensorManager.ColorFormat;
            }
            */
        }
    }
}
