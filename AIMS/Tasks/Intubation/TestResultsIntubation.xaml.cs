﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using AIMS.Tasks.Intubation;
using AIMS.Tasks;

namespace AIMS
{
	public partial class TestResultsIntubation : INotifyPropertyChanged, ITaskResultsControl
	{
        private IntubationResult result = null;
        public IntubationResult Result 
        { 
            get 
            { 
                return result; 
            } 
            set 
            { 
                result = value;
                
                NotifyPropertyChanged("Result");

                NotifyPropertyChanged("TaskMode");
                NotifyPropertyChanged("MasteryLevel");

                NotifyPropertyChanged("LessonStartTime");
                NotifyPropertyChanged("LessonEndTime");

                NotifyPropertyChanged("Score");

                NotifyPropertyChanged("OverallFeedback");

                NotifyPropertyChanged("TiltHeadFeedback");
                NotifyPropertyChanged("GrabLaryngoscopeFeedback");
                NotifyPropertyChanged("InsertLaryngoscopeFeedback");
                NotifyPropertyChanged("ViewVocalChordsFeedback");
                NotifyPropertyChanged("GrabEndotrachealTubeFeedback");
                NotifyPropertyChanged("InsertEndotrachealTubeFeedback");
                NotifyPropertyChanged("RemoveLaryngoscopeFeedback");
                NotifyPropertyChanged("UseSyringeFeedback");
                NotifyPropertyChanged("RemoveStyletFeedback");
                NotifyPropertyChanged("BaggingFeedback");
                NotifyPropertyChanged("ChestRiseFeedback");

                NotifyPropertyChanged("TiltHeadScore");
                NotifyPropertyChanged("GrabLaryngoscopeScore");
                NotifyPropertyChanged("InsertLaryngoscopeScore");
                NotifyPropertyChanged("ViewVocalChordsScore");
                NotifyPropertyChanged("GrabEndotrachealTubeScore");
                NotifyPropertyChanged("InsertEndotrachealTubeScore");
                NotifyPropertyChanged("RemoveLaryngoscopeScore");
                NotifyPropertyChanged("UseSyringeScore");
                NotifyPropertyChanged("RemoveStyletScore");
                NotifyPropertyChanged("BaggingScore");
                NotifyPropertyChanged("ChestRiseScore");


                NotifyPropertyChanged("KinectImages");

            } 
        }

        #region Getters and Setters for Binding
        public KinectImage[] KinectImages
        {
            get
            {
                if (this.Result == null)
                    return null;

                return new KinectImage[] {
                    this.Result.CalibrationImage,
                    this.Result.TiltHeadImage,
                    this.Result.GrabLaryngoscopeImage,
                    this.Result.InsertLaryngoscopeImage,
                    this.Result.ViewVocalChordsImage,
                    this.Result.GrabEndotrachealTubeImage,
                    this.Result.InsertEndotrachealTubeImage,
                    this.Result.RemoveLaryngoscopeImage,
                    this.Result.UseSyringeImage,
                    this.Result.RemoveStyletImage,
                    this.Result.BaggingImage,
                    this.Result.ChestRiseImage
                };
            }
        }



        public TaskMode TaskMode
        {
            get
            {
                if (this.Result == null)
                    return AIMS.TaskMode.Practice;

                return this.Result.TaskMode;
            }
        }

        public MasteryLevel MasteryLevel
        {
            get
            {
                if (this.Result == null)
                    return MasteryLevel.Novice;

                return this.Result.MasteryLevel;
            }
        }

        public DateTime StartTime
        {
            get
            {
                if (this.Result == null)
                    return DateTime.MinValue;

                return this.Result.StartTime;
            }
        }

        public DateTime EndTime
        {
            get
            {
                if (this.Result == null)
                    return DateTime.MaxValue;

                return this.Result.EndTime;
            }
        }

        public double Score
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.Score;
            }
        }

        public TaskFeedback[] OverallFeedback
        {
            get
            {
                if( this.Result == null )
                    return null;

                return GetTaskFeedbacksFromIDs(this.Result.OverallFeedbackIDs);
            }
        }

        public TaskFeedback[] TiltHeadFeedback
        {
            get
            {
                if (this.Result == null)
                    return null;

                return GetTaskFeedbacksFromIDs(this.Result.TiltHeadFeedbackIDs);
            }
        }

        public TaskFeedback[] GrabLaryngoscopeFeedback
        {
            get
            {
                if (this.Result == null)
                    return null;

                return GetTaskFeedbacksFromIDs(this.Result.GrabLaryngoscopeFeedbackIDs);
            }
        }

        public TaskFeedback[] InsertLaryngoscopeFeedback
        {
            get
            {
                if (this.Result == null)
                    return null;

                return GetTaskFeedbacksFromIDs(this.Result.InsertLaryngoscopeFeedbackIDs);
            }
        }

        public TaskFeedback[] ViewVocalChordsFeedback
        {
            get
            {
                if (this.Result == null)
                    return null;

                return GetTaskFeedbacksFromIDs(this.Result.ViewVocalChordsFeedbackIDs);
            }
        }

        public TaskFeedback[] GrabEndotrachealTubeFeedback
        {
            get
            {
                if (this.Result == null)
                    return null;

                return GetTaskFeedbacksFromIDs(this.Result.GrabEndotrachealTubeFeedbackIDs);
            }
        }

        public TaskFeedback[] InsertEndotrachealTubeFeedback
        {
            get
            {
                if (this.Result == null)
                    return null;

                return GetTaskFeedbacksFromIDs(this.Result.InsertEndotrachealTubeFeedbackIDs);
            }
        }

        public TaskFeedback[] RemoveLaryngoscopeFeedback
        {
            get
            {
                if (this.Result == null)
                    return null;

                return GetTaskFeedbacksFromIDs(this.Result.RemoveLaryngoscopeFeedbackIDs);
            }
        }

        public TaskFeedback[] UseSyringeFeedback
        {
            get
            {
                if (this.Result == null)
                    return null;

                return GetTaskFeedbacksFromIDs(this.Result.UseSyringeFeedbackIDs);
            }
        }

        public TaskFeedback[] RemoveStyletFeedback
        {
            get
            {
                if (this.Result == null)
                    return null;

                return GetTaskFeedbacksFromIDs(this.Result.RemoveStyletFeedbackIDs);
            }
        }

        public TaskFeedback[] BaggingFeedback
        {
            get
            {
                if (this.Result == null)
                    return null;

                return GetTaskFeedbacksFromIDs(this.Result.BaggingFeedbackIDs);
            }
        }

        public TaskFeedback[] ChestRiseFeedback
        {
            get
            {
                if (this.Result == null)
                    return null;

                return GetTaskFeedbacksFromIDs(this.Result.ChestRiseFeedbackIDs);
            }
        }

        public double TiltHeadScore
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.TiltHeadScore;
            }
        }

        public double GrabLaryngoscopeScore
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.GrabLaryngoscopeScore;
            }
        }

        public double InsertLaryngoscopeScore
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.InsertLaryngoscopeScore;
            }
        }

        public double ViewVocalChordsScore
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.ViewVocalChordsScore;
            }
        }

        public double GrabEndotrachealTubeScore
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.GrabEndotrachealTubeScore;
            }
        }

        public double InsertEndotrachealTubeScore
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.InsertEndotrachealTubeScore;
            }
        }

        public double RemoveLaryngoscopeScore
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.RemoveLaryngoscopeScore;
            }
        }

        public double UseSyringeScore
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.UseSyringeScore;
            }
        }

        public double RemoveStyletScore
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.RemoveStyletScore;
            }
        }

        public double BaggingScore
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.BaggingScore;
            }
        }

        public double ChestRiseScore
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.ChestRiseScore;
            }
        }
        #endregion

        /// <summary>
        /// Helper function to convert and return the TaskFeedback objects rather than int ids.
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        private TaskFeedback[] GetTaskFeedbacksFromIDs(List<int> ids)
        {
            if( ids == null )
                return null;

            TaskFeedback[] feedbacks = new TaskFeedback[ids.Count];

            for (int i = 0; i < ids.Count; i++)
            {
                feedbacks[i] = TaskFeedback.GetFeedbackFromID(ids[i], Task.Intubation);
            }

            return feedbacks;
        }
	
		public TestResultsIntubation()
		{
			this.InitializeComponent();
			// Insert code required on object creation below this point.

            Loaded += TestResultsIntubation_Loaded;
            ImagesScrollViewer.ScrollChanged += (o, e) => this.UpdateImagesPagingButtonState();
		}

        void TestResultsIntubation_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = this;
            this.ImagesListView.DataContext = this;

        }

        public void ShowResults(IntubationResult result)
        {
            /*if (result == null)
                return;*/

            this.Result = result;

            this.Visibility = System.Windows.Visibility.Visible;
            this.IsEnabled = true;

            SubmitButton.IsEnabled = true;
            SubmitButton.Content = "Submit";    // reset submition text
            TryAgainButton.IsEnabled = true;
            ExitButton.IsEnabled = true;
 
            Page parentPage = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

            if (parentPage as IntubationPage != null)
            {
                IntubationPage tp = parentPage as IntubationPage;

                tp.BlurEffect.Radius = 25;
            }

            this.OverallResultTab.IsSelected = true;        // The first tab of the TabControl should be reset to 0 (Overall Tab).
            this.ImagesListView.SelectedIndex = 0;    // The first image in the Images tab should be reset to 0.
            this.ImagesListView.ScrollIntoView(this.ImagesListView.SelectedItem);   // Scroll back up to the top.
        }
        
        #region Scroll Up/Down Hover Buttons

        public static readonly DependencyProperty ImagesPageUpEnabledProperty = DependencyProperty.Register(
            "ImagesPageUpEnabled", typeof(bool), typeof(TestResultsIntubation), new PropertyMetadata(false));

        public static readonly DependencyProperty ImagesPageDownEnabledProperty = DependencyProperty.Register(
            "ImagesPageDownEnabled", typeof(bool), typeof(TestResultsIntubation), new PropertyMetadata(false));

        private const double ScrollErrorMargin = 0.001;

        private const int PixelScrollByAmount = 20;

        /// <summary>
        /// CLR Property Wrappers for PageUpEnabledProperty
        /// </summary>
        public bool ImagesPageUpEnabled
        {
            get
            {
                return (bool)GetValue(ImagesPageUpEnabledProperty);
            }

            set
            {
                this.SetValue(ImagesPageUpEnabledProperty, value);
            }
        }

        /// <summary>
        /// CLR Property Wrappers for PageDownEnabledProperty
        /// </summary>
        public bool ImagesPageDownEnabled
        {
            get
            {
                return (bool)GetValue(ImagesPageDownEnabledProperty);
            }

            set
            {
                this.SetValue(ImagesPageDownEnabledProperty, value);
            }
        }

        /// <summary>
        /// Change button state depending on scroll viewer position
        /// </summary>
        private void UpdateImagesPagingButtonState()
        {
            this.ImagesPageUpEnabled = ImagesScrollViewer.VerticalOffset > ScrollErrorMargin;

            this.ImagesPageDownEnabled = ImagesScrollViewer.VerticalOffset < ImagesScrollViewer.ScrollableHeight - ScrollErrorMargin;
        }

        private void ImagesPageUpButtonClick(object sender, RoutedEventArgs e)
        {
            ImagesScrollViewer.ScrollToVerticalOffset(ImagesScrollViewer.VerticalOffset - PixelScrollByAmount);
        }

        private void ImagesPageDownButtonClick(object sender, RoutedEventArgs e)
        {
            ImagesScrollViewer.ScrollToVerticalOffset(ImagesScrollViewer.VerticalOffset + PixelScrollByAmount);
        }
        #endregion


        /// <summary>
        /// Unfortunate hardcoded override to cycle to the next/previous image in the wrap panel on key down.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImagesListView_KeyUp(object sender, KeyEventArgs e)
        {
            if (object.ReferenceEquals(sender, this.ImagesListView))
            {
                if (this.ImagesListView.Items.Count > 0)
                {
                    switch (e.Key)
                    {
                        case Key.Right:
                        {
                            goto case Key.Down;
                        }
                        case Key.Left:
                        {
                            goto case Key.Up;
                        }
                        case Key.Down:
                        {
                            this.SelectNextImage();
                            break;
                        }
                        case Key.Up:
                        {
                            this.SelectPreviousImage();
                            break;
                        }

                        default:
                        {
                            return; // return if the key pressed wasn't listened to by this switch
                        }
                    }

                    e.Handled = true;   // handle the event if it made it this far
                }
            }
        }

        private void SelectNextImage()
        {
            if (this.ImagesListView.Items != null && this.ImagesListView.Items.Count > 0)   // no items
            {
                if (this.ImagesListView.SelectedItem == null) // no selected item yet
                {
                    this.ImagesListView.SelectedItem = this.ImagesListView.Items[0];    // select the first item
                }
                else // we have a selected item
                {
                    if (this.ImagesListView.Items.IndexOf(this.ImagesListView.SelectedItem) >= this.ImagesListView.Items.Count - 1)   // we're at the end of the list
                    {
                        this.ImagesListView.SelectedItem = this.ImagesListView.Items[0];    // select the first item
                    }
                    else
                    {
                        this.ImagesListView.SelectedItem = this.ImagesListView.Items[this.ImagesListView.Items.IndexOf(this.ImagesListView.SelectedItem) + 1];  // select the next item
                    }
                }

                if (this.ImagesListView.SelectedItem != null)
                {
                    this.ImagesListView.ScrollIntoView(this.ImagesListView.SelectedItem);   // scroll the new selected item into view.
                }
            }
        }

        private void SelectPreviousImage()
        {
            if (this.ImagesListView.Items != null && this.ImagesListView.Items.Count > 0)   // no items
            {
                if (this.ImagesListView.SelectedItem == null) // no selected item yet
                {
                    this.ImagesListView.SelectedItem = this.ImagesListView.Items[this.ImagesListView.Items.Count - 1]; // select the last item
                }
                else
                {
                    if (this.ImagesListView.Items.IndexOf(this.ImagesListView.SelectedItem) == 0)   // we're at the beginning of the list
                    {
                        this.ImagesListView.SelectedItem = this.ImagesListView.Items[this.ImagesListView.Items.Count - 1];    // select the last item
                    }
                    else
                    {
                        this.ImagesListView.SelectedItem = this.ImagesListView.Items[this.ImagesListView.Items.IndexOf(this.ImagesListView.SelectedItem) - 1];  // select the previous item
                    }
                }

                if (this.ImagesListView.SelectedItem != null)
                {
                    this.ImagesListView.ScrollIntoView(this.ImagesListView.SelectedItem);   // scroll the new selected item into view.
                }
            }
        }

        /// <summary>
        /// Closes (Hides/Disables/Un-blurs) the Results Overlay.
        /// </summary>
        public void CloseResults(bool wipeInformation)
        {
            this.Visibility = System.Windows.Visibility.Collapsed;
            this.IsEnabled = false;

            Page parentPage = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

            if (parentPage as IntubationPage != null)
            {
                IntubationPage tp = parentPage as IntubationPage;

                tp.BlurEffect.Radius = 0;
            }

            if (wipeInformation)
                WipeInformationClean();
        }

        /// <summary>
        /// Cleans out the information being stored in the overlay.
        /// </summary>
        public void WipeInformationClean()
        {
            if (this.Result != null)
            {
                this.Result = null;
            }
        }

        /// <summary>
        /// Event triggered when the Exit button on the component is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitClicked(object sender, RoutedEventArgs e)
        {
            DependencyObject dependencyObject = this;
            Type type = typeof(Page);

            while (dependencyObject != null)
            {
                if (type.IsInstanceOfType(dependencyObject))
                    break;
                else
                    dependencyObject = VisualTreeHelper.GetParent(dependencyObject);
            }

            if (dependencyObject != null)
            {
                Page pg = (Page)dependencyObject;
                Uri uri = new Uri("../../MainPage3.xaml", UriKind.Relative);
                pg.NavigationService.Navigate(uri);
            }
        }

        /// <summary>
        /// Cycles up the visual tree from the "startObject" in search for a DependencyObject of type "type".
        /// </summary>
        /// <param name="startObject"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private DependencyObject GetDependencyObjectFromVisualTree(DependencyObject startObject, Type type)
        {
            DependencyObject parent = startObject;

            while (parent != null)
            {
                if (type.IsInstanceOfType(parent))
                    break;

                else
                    parent = VisualTreeHelper.GetParent(parent);
            }

            return parent;
        }

        /// <summary>
        /// Event which fires when the TryAgainButton on the component is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TryAgainClicked(object sender, RoutedEventArgs e)
        {
            Page parentPage = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

            if (parentPage as IntubationPage != null)
            {
                IntubationPage tp = parentPage as IntubationPage;
                tp.Lesson.ResetLesson();// Restart();
            }

            CloseResults(true);
        }

        /// <summary>
        /// Event triggered when the submit button on the component is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubmitClicked(object sender, RoutedEventArgs e)
        {
            if (this.Result == null)
                return;

            SubmitButton.IsEnabled = false;
            SubmitButton.Content = "Submitting...";

            // Disable the TryAgainButton and ExitButton to be reenabled upon submission (otherwise this was causing a null reference crash before)
            this.ExitButton.IsEnabled = false;
            this.TryAgainButton.IsEnabled = false;

            if (((App)App.Current).CurrentAccount == null || ((App)App.Current).CurrentStudent == null || ((App)App.Current).CurrentUserID <= 0)
            {
                MessageBox.Show("You cannot submit results to the database unless you are logged in as a student.");
                return;
            }

            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                //SerializeResultsAndSubmit();
                IntubationResult.StoreResult(this.Result);
            };

            worker.RunWorkerCompleted += delegate(object s, RunWorkerCompletedEventArgs args)
            {
                SubmitButton.Content = "Success";
                //Re-enable TryAgain and Exit buttons
                TryAgainButton.IsEnabled = true;
                ExitButton.IsEnabled = true;
            };

            worker.RunWorkerAsync();

            e.Handled = true;
        }
		
		public event PropertyChangedEventHandler PropertyChanged;
		
        private void NotifyPropertyChanged( string propertyName )
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void ImagesListView_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;  // Handle the PreviewKeyDown event to disable keyboard navigation of images since we implemented our own for the multi-coulumn wrap panel.
        }


        public void ShowButtons()
        {
            this.SubmitButton.Visibility = System.Windows.Visibility.Visible;
            this.SubmitButton.IsEnabled = true;

            this.TryAgainButton.Visibility = System.Windows.Visibility.Visible;
            this.TryAgainButton.IsEnabled = true;

            this.ExitButton.Visibility = System.Windows.Visibility.Visible;
            this.ExitButton.IsEnabled = true;
        }

        public void HideButtons()
        {
            this.SubmitButton.Visibility = System.Windows.Visibility.Collapsed;
            this.SubmitButton.IsEnabled = false;

            this.TryAgainButton.Visibility = System.Windows.Visibility.Collapsed;
            this.TryAgainButton.IsEnabled = false;

            this.ExitButton.Visibility = System.Windows.Visibility.Collapsed;
            this.ExitButton.IsEnabled = false;
        }

        public void ShowResults(TaskResult result, bool showButtons)
        {
            if (result is IntubationResult)
            {
                IntubationResult intubationresult = result as IntubationResult;

                this.ShowResults(intubationresult);
            }
            else if( result.TaskType == Task.Intubation )
            {
                // Non-complete result..

                // TODO: Handle non-complete results.
                return;
            }
            else
            {
                // Not the right task type.
                return;
            }
            
            if (showButtons)
                this.ShowButtons();
            else this.HideButtons();
        }

        private void overview_Click(object sender, RoutedEventArgs e)
        {
            OverallResultTab.IsSelected = true;
            ImagesTab.IsSelected = false;
            StagesTab.IsSelected = false;
        }

        private void imagestab_Click(object sender, RoutedEventArgs e)
        {
            ImagesTab.IsSelected = true;
            StagesTab.IsSelected = false;
            OverallResultTab.IsSelected = false;
        }

        private void stages_Click(object sender, RoutedEventArgs e)
        {
            StagesTab.IsSelected = true;
            ImagesTab.IsSelected = false;
            OverallResultTab.IsSelected = false;
        }

        private void ImageTileButton_Click(object sender, RoutedEventArgs e)
        {
            ImagesListView.SelectedItem = sender;
        }
    }
}