﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;

namespace AIMS
{
    public class LaryngoscopeWatcher //: IDisposable
    {
        private Point3D startingPosition = new Point3D();
        public Point3D StartingPosition { get { return startingPosition; } set { startingPosition = value; } }

        private Point3D currentPosition = new Point3D();
        public Point3D CurrentPosition { get { return currentPosition; } set { currentPosition = value; } }

        private bool isMoving = false;
        private bool isSetDown = false;
        private bool isInRightHand = false;
        private bool isFound = false;

        /*
        public LaryngoscopeWatcher() 
        {
            LaryngoscopeSetDown += LaryngoscopeWatcher_LaryngoscopeSetDown;
        }
        
        public void CheckLaryngoscopeState()
        {
        }

        public void LaryngoscopeWatcher_LaryngoscopeSetDown()
        {
            
        }

        public delegate void LaryngoscopeSetDownEventHandler();

        public static event LaryngoscopeSetDownEventHandler LaryngoscopeSetDown
        {
        }

        public void Dispose()
        {
            LaryngoscopeSetDown -= LaryngoscopeWatcher_LaryngoscopeSetDown;
        }*/
    }
}
