﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Kinect;

namespace AIMS
{
    /// <summary>
    /// A custom class which builds upon the Kinect Joint class, adding additional features.
    /// </summary>
    public class JointProfile : IDisposable
    {
        private JointType jointType;
        private SkeletonProfile linkedSkeleton;
        private CameraSpacePoint previousPosition;
        private CameraSpacePoint currentPosition;
        private TrackingState previousTrackingState;
        private TrackingState currentTrackingState;
        private MovementAnalysis movementAnalysis;

        public JointType JointType { get { return jointType; } set { jointType = value; } }
        public SkeletonProfile LinkedSkeleton { get { return linkedSkeleton; } set { linkedSkeleton = value; } }
        public CameraSpacePoint PreviousPosition { get { return previousPosition; } private set { previousPosition = value; } }
        public CameraSpacePoint CurrentPosition
        {
            get { return currentPosition; } 
            set 
            {
                this.PreviousPosition = currentPosition;
                currentPosition = value; 
            } }

        public TrackingState PreviousTrackingState { get { return previousTrackingState; } private set { previousTrackingState = value; } }
        public TrackingState CurrentTrackingState { get { return currentTrackingState; }
            set 
            {
                this.PreviousTrackingState = currentTrackingState;
                currentTrackingState = value;
            } }
        public MovementAnalysis MovementAnalysis { get { return movementAnalysis; } set { movementAnalysis = value; } }

        public JointProfile(JointType type, SkeletonProfile linkedskeleton)
        {
            this.JointType = type;
            this.LinkedSkeleton = linkedskeleton;
        }

        /// <summary>
        /// Destructor.
        /// </summary>
        ~JointProfile()
        {
            this.Dispose(false);
        }

        public void UpdateJointData(Body skele)
        {
            if (skele != null)
            {
                Joint j = skele.Joints[this.JointType];

                if (j != null)
                {
                    this.CurrentPosition = j.Position;
                    this.CurrentTrackingState = j.TrackingState;

                    this.MovementAnalysis = MovementAnalysis.ComparePoints(this.PreviousPosition, this.CurrentPosition);

                    //System.Diagnostics.Debug.WriteLine("Joint: " + this.MovementAnalysis.ToString());
                }
            }
        }
        
        #region Disposing
        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    #region dispose cleanup

                    if (this.LinkedSkeleton != null)
                    {
                        this.LinkedSkeleton = null;
                    }

                    #endregion dispose cleanup

                    this.disposed = true;
                }
            }
        }
        #endregion Disposing
    }
}
