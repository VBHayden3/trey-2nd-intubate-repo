﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Media3D;
using System.Media;
using System.IO;
using AIMS.ODU_Dev.Utilities;
using AIMS.Watchers;
using Microsoft.Kinect;
using AIMS.Assets.Lessons2;
using System.ComponentModel;

namespace AIMS.Tasks.LateralTransfer
{
    public class LateralTransfer : Lesson
    {
        private LateralTransferPage lateralTransferPage;
        public LateralTransferPage LateralTransferPage { get { return lateralTransferPage; } set { lateralTransferPage = value; } }

        private SoundPlayer nextStageSound;
        public SoundPlayer NextStageSound { get { return nextStageSound; } set { nextStageSound = value; } }

        private SoundPlayer stageTimeoutSound;
        public SoundPlayer StageSound { get { return stageTimeoutSound; } set { stageTimeoutSound = value; } }

        private readonly double deductionPercentPerSecond = 4.615;
        public double DeductionPercentPerSecond { get { return deductionPercentPerSecond; } }

        private PersistentColorTracker pct;
        public PersistentColorTracker PersistentColorTracker { get { return pct; } set { pct = value; } }

        private List<BaseWatcher> lessonWatchers;
        public List<BaseWatcher> LessonWatchers { get { return lessonWatchers; } set { lessonWatchers = value; } }

        private Point3D manikinTorsoLocation;
        public Point3D ManikinTorsoLocation { get { return manikinTorsoLocation; } set { manikinTorsoLocation = value; } }

        private Point3D manikinLegLocation;
        public Point3D ManikinLegLocation { get { return manikinLegLocation; } set { manikinLegLocation = value; } }

        private ulong? mainUserSkeletonProfileID;
        /// <summary>
        /// SkeletonID# of the profile representing the Secondary User in this Lesson. This value is set at Calibration.
        /// </summary>
        public ulong? MainUserSkeletonProfileID { get { return mainUserSkeletonProfileID; } set { mainUserSkeletonProfileID = value; } }

        private ulong? secondaryUserSkeletonProfileID;
        /// <summary>
        /// SkeletonID# of the profile representing the Secondary User in this Lesson. This value is set at Calibration.
        /// </summary>
        public ulong? SecondaryUserSkeletonProfileID { get { return secondaryUserSkeletonProfileID; } set { secondaryUserSkeletonProfileID = value; } }

        /// <summary>
        /// Calls KinectManager.GetSkeletonProfile, searching only for sticky skeletons, and looking for the current MainUserSkeletonProfileID.
        /// Returns the found SkeletonProfile for that ID, if found. Else returns null.
        /// </summary>
        public SkeletonProfile MainUserSkeletonProfile { get { return KinectManager.GetSkeletonProfile((ulong)mainUserSkeletonProfileID, true); } }
        /// <summary>
        /// Calls KinectManager.GetSkeletonProfile, searching only for sticky skeletons, and looking for the current SecondaryUserSkeletonProfileID.
        /// Returns the found SkeletonProfile for that ID, if found. Else returns null.
        /// </summary>
        public SkeletonProfile SecondaryUserSkeletonProfile { get { return KinectManager.GetSkeletonProfile((ulong)secondaryUserSkeletonProfileID, true); } }

        private DetectableActionCollection detectableActions;
        public DetectableActionCollection DetectableActions { get { return detectableActions; } set { detectableActions = value; } }

        /// <summary>
        /// Object records points of two users to create paths, then (for a given time range within that data) performs comparison analysis to determine the amount of sync between the two user's actions.
        /// </summary>
        public SkeletonPathComparison skeletonPathComparison;
        public SkeletonPathComparison SkeletonPathComparison { get { return skeletonPathComparison; } set { skeletonPathComparison = value; } }

        private LateralTransferResult result;
        public LateralTransferResult Result { get { return result; } set { result = value; } }

        private DateTime syncPullStartTime = DateTime.MinValue;
        private DateTime syncPullEndTime = DateTime.MaxValue;
        private CameraSpacePoint primaryUserStartPoint;
        private CameraSpacePoint secondaryUserStartPoint;
        private CameraSpacePoint manikinTorsoEndPoint;
        private CameraSpacePoint manikinLegEndPoint;
        private CameraSpacePoint primaryUserEndPoint;
        private CameraSpacePoint secondaryUserEndPoint;
        private CameraSpacePoint manikinTorsoStartPoint;
        private CameraSpacePoint manikinLegStartPoint;

        public DateTime SyncPullStartTime { get { return syncPullStartTime; } set { syncPullStartTime = value; } }
        public DateTime SyncPullEndTime { get { return syncPullEndTime; } set { syncPullEndTime = value; } }
        public CameraSpacePoint PrimaryUserStartPoint { get { return primaryUserStartPoint; } set { primaryUserStartPoint = value; } }
        public CameraSpacePoint SecondaryUserStartPoint { get { return secondaryUserStartPoint; } set { secondaryUserStartPoint = value; } }
        public CameraSpacePoint ManikinTorsoStartPoint { get { return manikinTorsoStartPoint; } set { manikinTorsoEndPoint = value; } }
        public CameraSpacePoint ManikinLegStartPoint { get { return manikinLegStartPoint; } set { manikinLegStartPoint = value; } }
        public CameraSpacePoint PrimaryUserEndPoint { get { return primaryUserEndPoint; } set { primaryUserEndPoint = value; } }
        public CameraSpacePoint SecondaryUserEndPoint { get { return secondaryUserEndPoint; } set { secondaryUserEndPoint = value; } }
        public CameraSpacePoint ManikinTorsoEndPoint { get { return manikinTorsoEndPoint; } set { manikinTorsoEndPoint = value; } }
        public CameraSpacePoint ManikinLegEndPoint { get { return manikinLegEndPoint; } set { manikinLegEndPoint = value; } }

        private bool pullStarted = false;
        private bool pullEnded = false;
        /// <summary>
        /// Master flag indicating pull start.
        /// </summary>
        public bool PullStarted { get { return pullStarted; } set { pullStarted = value; } }
        /// <summary>
        /// Master flag indicating pull end.
        /// </summary>
        public bool PullEnded { get { return pullEnded; } set { pullEnded = value; } }

        private Watcher manikinLegPositionWatcher;
        private Watcher manikinTorsoPositionWatcher;
        private Watcher manikinPullStartedWatcher;

        private Point3D prevLegLoc;
        private Point3D prevTorsoLoc;

        public Point3D PrevLegLoc { get { return prevLegLoc; } set { prevLegLoc = value; } }
        public Point3D PrevTorsoLoc { get { return prevTorsoLoc; } set { prevTorsoLoc = value; } }

        private DateTime vocalCountTime = DateTime.MinValue;
        private bool didVocalCount = false;

        public DateTime VocalCountTime { get { return vocalCountTime; } set { vocalCountTime = value; } }
        public bool DidVocalCount { get { return didVocalCount; } set { didVocalCount = value; } }

        public LateralTransfer( LateralTransferPage page, TaskMode taskmode, MasteryLevel masterylevel )
        {
            LateralTransferPage = page;

            TaskMode = taskmode;
            MasteryLevel = masterylevel;

            Name = "Lateral Transfer";
            Description = "Lateral Transfer of a Patient";

            Stages = new List<Stage>();
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            Paused = false;
            Running = false;
            CurrentStageIndex = 0;
            ImagePath = "/Assets/Graphics/Lateral Transfer/LateralTransfer.png";

            //Result = new Result();

            Result = new LateralTransferResult();

            TargetTime = TimeSpan.FromSeconds(30.0);

            lessonWatchers = new List<BaseWatcher>();

            nextStageSound = new System.Media.SoundPlayer(Properties.Resources.Ding);
            // Load the sound.
            nextStageSound.LoadAsync();

            stageTimeoutSound = new System.Media.SoundPlayer(Properties.Resources.Ding);
            // Load the sound.
            stageTimeoutSound.LoadAsync();

            // Add all of the stages to the lesson. Score weights should add up to 100%. Stage numbers are in order
            // of when they are added to the list, and order of execution in the lesson.
            
            Stages.Add(new LateralTransferCalibrationStage(this) { ScoreWeight = 0.00, StageNumber = 0 });
            Stages.Add(new LateralTransferUsersPositionsStage(this) { ScoreWeight = 0.20, StageNumber = 1 });
            Stages.Add(new LateralTransferVocalInteractionStage(this) { ScoreWeight = 0.20, StageNumber = 2 });
            Stages.Add(new LateralTransferTransferStage(this) { ScoreWeight = 0.60, StageNumber = 3 });
            Stages.Add(new LateralTransferDisplayResultsStage(this) { ScoreWeight = 0.00, StageNumber = 4 });

            if (this.lateralTransferPage != null)
            {
                //if( this.LateralTransferPage.flow.Cache == null )
                this.LateralTransferPage.flow.Cache = new ThumbnailManager();    // create the flow cache (enhances re-loading preformance).

                foreach (Stage stage in this.Stages)
                {
                    this.lateralTransferPage.flow.Add(null, stage.ImagePath);
                }
            }

            this.Complete = false;
            Stopwatch = new System.Diagnostics.Stopwatch();

            KinectManager.NumStickySkeletons = 2;   // Lateral Transfer has two users.

            CreateActions();

            SetupLessonWatchers();

            this.SkeletonPathComparison = new SkeletonPathComparison();
        }

        public void SetupLessonWatchers()
        {
            // Watcher keeps the torso position up to date.
            this.manikinTorsoPositionWatcher = new Watcher();

            #region Get Current Location for Manikin Torso.
            this.manikinTorsoPositionWatcher.CheckForActionDelegate = () =>
            {
                if (this.CurrentStageIndex <= 0)    // We must be passed calibration.
                    return;

                Point found = ColorTracker.find(ColorTracker.GetColorRange("yellow-torso", Lighting.none), new Int32Rect((int)this.ManikinTorsoLocation.X - 30, (int)this.ManikinTorsoLocation.Y - 45, 60, 60), KinectManager.MappedColorData);

                if (found == null || Double.IsNaN(found.X) || Double.IsNaN(found.Y))
                    return;    // Point not found in color tracker.

                //int pixelIndex = 640 * (int)found.Y + (int)found.X;

                int depth = KinectManager.GetDepthValue((int)found.X, (int)found.Y);
                if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                {
                    Point3D loc = KinectManager.Get3DPointForColorPoint(found, depth);

                    if ((this.PrimaryUserStartPoint.Z * 1000) > depth)   // ensure that the depth of the color is infront of the user since their should really be 1.5 tables inbetween the user and the found point.
                    {
                        this.prevTorsoLoc = this.ManikinTorsoLocation;
                        this.ManikinTorsoLocation = loc;
                        // Passed depth validation check.
                        // this.foundTorso = true;
                        //System.Diagnostics.Debug.WriteLine(String.Format("TorsoWatcher - Found Torso: X, Y, Z ({0}, {1}, {2})", this.ManikinTorsoLocation.X, this.ManikinTorsoLocation.Y, this.ManikinTorsoLocation.Z));
                        manikinTorsoPositionWatcher.IsTriggered = true;
                    }
                    else
                    {
                        // Invalid based on depth validation.
                        System.Diagnostics.Debug.WriteLine("Torso: Invalid Depth");
                        manikinTorsoPositionWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Torso: No Depth");
                    manikinTorsoPositionWatcher.IsTriggered = false;
                }
            };
            #endregion

            this.LessonWatchers.Add(this.manikinTorsoPositionWatcher);

            // Watcher keeps the leg position up to date.
            this.manikinLegPositionWatcher = new Watcher();

            #region Get Current Location for Manikin Leg.
            this.manikinLegPositionWatcher.CheckForActionDelegate = () =>
            {
                if (this.CurrentStageIndex <= 0)    // We must be passed calibration.
                    return;

                Point found = ColorTracker.find(ColorTracker.GetColorRange("red-leg", Lighting.none), new Int32Rect((int)this.ManikinLegLocation.X - 30, (int)this.ManikinLegLocation.Y - 45, 60, 60), KinectManager.MappedColorData);

                if (found == null || Double.IsNaN(found.X) || Double.IsNaN(found.Y))
                    return;    // Point not found in color tracker.

                //int pixelIndex = 640 * (int)found.Y + (int)found.X;
                int depth = KinectManager.GetDepthValue((int)found.X, (int)found.Y);
                if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                {
                    Point3D loc = KinectManager.Get3DPointForColorPoint(found, depth);

                    if ((this.PrimaryUserStartPoint.Z * 1000) > depth)   // ensure that the depth of the color is infront of the user since their should really be 1.5 tables inbetween the user and the found point.
                    {
                        this.prevLegLoc = this.ManikinLegLocation;
                        this.ManikinLegLocation = loc;
                        // Passed depth validation check.
                        // this.foundLeg = true;
                        //System.Diagnostics.Debug.WriteLine(String.Format("LegWatcher - Found Leg: X, Y, Z ({0}, {1}, {2})", this.ManikinLegLocation.X, this.ManikinLegLocation.Y, this.ManikinLegLocation.Z));
                        manikinLegPositionWatcher.IsTriggered = true;
                    }
                    else
                    {
                        // Invalid based on depth validation.
                        System.Diagnostics.Debug.WriteLine("Leg: Invalid Depth");
                        manikinLegPositionWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Leg: No Depth");
                    manikinLegPositionWatcher.IsTriggered = false;
                }
            };
            #endregion

            this.LessonWatchers.Add(this.manikinLegPositionWatcher);

            // Watcher checks for the manikin to begin being pulled.
            this.manikinPullStartedWatcher = new Watcher();
            this.manikinPullStartedWatcher.IsTriggered = false;

            #region Detect the start of the pull.
            this.manikinPullStartedWatcher.CheckForActionDelegate = () =>
            {
                if (this.PullStarted)    // Already started.
                    return;

                if (this.CurrentStageIndex <= 0)    // We must be passed calibration.
                    return;

                // This watcher checks for the manikin to begin movement, and is intended to identify the DateTime of pull start.
                // Pull start DateTime will be used as the lower clamp for the timerange when performing the comparison analysis of the skeleton paths between two users, 
                // as well as the color paths between the toro and leg of the manikin.
                // To accomplish identifying the start of the pull, we check for the manikin to breach a specified Z-displacement threshold, indicating pull movement.
                CameraSpacePoint skelTorso = KinectManager.CoordinateMapper.MapDepthPointToCameraSpace(new DepthSpacePoint() { X = (int)this.ManikinTorsoLocation.X, Y = (int)this.ManikinTorsoLocation.Y }, (ushort)this.ManikinTorsoLocation.Z);

                double torsoDisplacementTotal = 0;
                double torsoDisplacement = 0;

                if (skelTorso != null && skelTorso.Z != 0)
                {
                    torsoDisplacementTotal = this.ManikinTorsoStartPoint.Z - skelTorso.Z;
                }

                torsoDisplacement = this.prevTorsoLoc.Z - this.ManikinTorsoLocation.Z;

                CameraSpacePoint skelLeg = KinectManager.CoordinateMapper.MapDepthPointToCameraSpace(new DepthSpacePoint() { X = (int)this.ManikinLegLocation.X, Y = (int)this.ManikinLegLocation.Y }, (ushort)this.ManikinLegLocation.Z);

                double legDisplacementTotal = 0;
                double legDisplacement = 0;

                if (skelLeg != null && skelLeg.Z != 0)
                {
                    legDisplacementTotal = this.ManikinLegStartPoint.Z - skelLeg.Z;
                }

                legDisplacement = this.prevLegLoc.Z - this.ManikinLegLocation.Z;

                //System.Diagnostics.Debug.WriteLine(String.Format("TOTAL: TorsoDisplacement: {0}, LegDisplacement: {1};", torsoDisplacementTotal, legDisplacementTotal));
                System.Diagnostics.Debug.WriteLine(String.Format("DELTA: TorsoDisplacement: {0}, LegDisplacement: {1};", torsoDisplacement, legDisplacement));

                if (torsoDisplacement < -16 && legDisplacement < -16)
                {
                    this.manikinPullStartedWatcher.IsTriggered = true;
                }
            };
            #endregion

            this.LessonWatchers.Add(this.manikinPullStartedWatcher);
        }

        /// <summary>
        /// Sets up the collection of DetectableActions.
        /// </summary>
        public void CreateActions()
        {
            detectableActions = new DetectableActionCollection("All Lateral Transfer Actions");

                // PRIMARY USER:
                DetectableActionGroup primaryUserActions = new DetectableActionGroup("Primary User");

                    DetectableActionCategory primaryUser_BecomeSticky = new DetectableActionCategory(primaryUserActions, "Become Sticky");
                        DetectableAction primaryUser_BecameSticky = new DetectableAction("Became Sticky");
                        primaryUser_BecomeSticky.AddProperAction(primaryUser_BecameSticky);

                    DetectableActionCategory primaryUser_InPosition = new DetectableActionCategory(primaryUserActions, "In Position");
                        DetectableAction primaryUser_HandRightInPosition = new DetectableAction("Right Hand In Position");
                        DetectableAction primaryUser_HandLeftInPosition = new DetectableAction("Left Hand In Position");
                        DetectableAction primaryUser_ArmsInPosition = new DetectableAction("Arms In Position");
                        DetectableAction primaryUser_BodyInPosition = new DetectableAction("Body In Position");
                        primaryUser_InPosition.AddProperAction(primaryUser_HandRightInPosition);
                        primaryUser_InPosition.AddProperAction(primaryUser_HandLeftInPosition);
                        primaryUser_InPosition.AddProperAction(primaryUser_ArmsInPosition);
                        primaryUser_InPosition.AddProperAction(primaryUser_BodyInPosition);

                    primaryUserActions.AddActionCategory(primaryUser_BecomeSticky);
                    primaryUserActions.AddActionCategory(primaryUser_InPosition);

                detectableActions.AddGroup(primaryUserActions);

                // SECONDARY USER:
                DetectableActionGroup secondaryUserActions = new DetectableActionGroup("Secondary User");

                    DetectableActionCategory secondaryUser_BecomeSticky = new DetectableActionCategory(secondaryUserActions, "Become Sticky");
                        DetectableAction secondaryUser_BecameSticky = new DetectableAction("Became Sticky");
                        secondaryUser_BecomeSticky.AddProperAction(secondaryUser_BecameSticky);

                    DetectableActionCategory secondaryUser_InPosition = new DetectableActionCategory(secondaryUserActions, "In Position");
                        DetectableAction secondary_HandRightInPosition = new DetectableAction("Right Hand In Position");
                        DetectableAction secondary_HandLeftInPosition = new DetectableAction("Left Hand In Position");
                        DetectableAction secondary_ArmsInPosition = new DetectableAction("Arms In Position");
                        DetectableAction secondary_BodyInPosition = new DetectableAction("Body In Position");
                        secondaryUser_InPosition.AddProperAction(secondary_HandRightInPosition);
                        secondaryUser_InPosition.AddProperAction(secondary_HandLeftInPosition);
                        secondaryUser_InPosition.AddProperAction(secondary_ArmsInPosition);
                        secondaryUser_InPosition.AddProperAction(secondary_BodyInPosition);

                    secondaryUserActions.AddActionCategory(secondaryUser_BecomeSticky);
                    secondaryUserActions.AddActionCategory(secondaryUser_InPosition);

                detectableActions.AddGroup(secondaryUserActions);

                // Manikin Torso:
                DetectableActionGroup manikinTorsoActions = new DetectableActionGroup("Manikin Torso");

                    DetectableActionCategory manikinTorso_LocateInitially = new DetectableActionCategory(manikinTorsoActions, "Locate Initially");
                        DetectableAction manikinTorso_LocatedInitially = new DetectableAction("Initially Located");
                        DetectableAction manikinTorso_NotInitiallyFound = new DetectableAction("Not Initially Found");
                        manikinTorso_LocateInitially.AddProperAction(manikinTorso_LocatedInitially);
                        manikinTorso_LocateInitially.AddImproperAction(manikinTorso_NotInitiallyFound);

                        manikinTorsoActions.AddActionCategory(manikinTorso_LocateInitially);

                    DetectableActionCategory manikinTorso_StartPulling = new DetectableActionCategory(manikinTorsoActions, "Start Pulling");
                        DetectableAction manikinTorso_PullStarted = new DetectableAction("Pull Started");
                        DetectableAction manikinTorso_NotPulled = new DetectableAction("Not Initially Found");
                        manikinTorso_StartPulling.AddProperAction(manikinTorso_PullStarted);
                        manikinTorso_StartPulling.AddImproperAction(manikinTorso_NotPulled);

                        manikinTorsoActions.AddActionCategory(manikinTorso_StartPulling);

                    DetectableActionCategory manikinTorso_EndPulling = new DetectableActionCategory(manikinTorsoActions, "End Pulling");
                        DetectableAction manikinTorso_PullEnded = new DetectableAction("Pull Ended");
                        DetectableAction manikinTorso_CantEndPull = new DetectableAction("Can't End Pull");
                        manikinTorso_EndPulling.AddProperAction(manikinTorso_PullEnded);
                        manikinTorso_EndPulling.AddImproperAction(manikinTorso_CantEndPull);

                        manikinTorsoActions.AddActionCategory(manikinTorso_EndPulling);

                detectableActions.AddGroup(manikinTorsoActions);

                // Manikin Leg:
                DetectableActionGroup manikinLegActions = new DetectableActionGroup("Manikin Leg");

                    DetectableActionCategory manikinLeg_LocateInitially = new DetectableActionCategory(manikinLegActions, "Locate Initially");
                        DetectableAction manikinLeg_LocatedInitially = new DetectableAction("Initially Located");
                        DetectableAction manikinLeg_NotInitiallyFound = new DetectableAction("Not Initially Found");
                        manikinLeg_LocateInitially.AddProperAction(manikinLeg_LocatedInitially);
                        manikinLeg_LocateInitially.AddImproperAction(manikinLeg_NotInitiallyFound);

                    manikinLegActions.AddActionCategory(manikinLeg_LocateInitially);

                    DetectableActionCategory manikinLeg_StartPulling = new DetectableActionCategory(manikinLegActions, "Start Pulling");
                        DetectableAction manikinLeg_PullStarted = new DetectableAction("Pull Started");
                        DetectableAction manikinLeg_NotPulled = new DetectableAction("Not Initially Found");
                        manikinLeg_StartPulling.AddProperAction(manikinLeg_PullStarted);
                        manikinLeg_StartPulling.AddImproperAction(manikinLeg_NotPulled);

                        manikinLegActions.AddActionCategory(manikinLeg_StartPulling);

                    DetectableActionCategory manikinLeg_EndPulling = new DetectableActionCategory(manikinLegActions, "End Pulling");
                        DetectableAction manikinLeg_PullEnded = new DetectableAction("Pull Ended");
                        DetectableAction manikinLeg_CantEndPull = new DetectableAction("Can't End Pull");
                        manikinLeg_EndPulling.AddProperAction(manikinLeg_PullEnded);
                        manikinLeg_EndPulling.AddImproperAction(manikinLeg_CantEndPull);

                        manikinLegActions.AddActionCategory(manikinLeg_EndPulling);

                detectableActions.AddGroup(manikinLegActions);

                // VOCAL INTERACTION:
                DetectableActionGroup vocalInteractionGroup = new DetectableActionGroup("Vocal Interaction");

                    DetectableActionCategory countPull_Category = new DetectableActionCategory(vocalInteractionGroup, "Count and Pull");
                        DetectableAction countPull_Action = new DetectableAction("One Two Three Pull");
                        countPull_Category.AddProperAction(countPull_Action);

                    vocalInteractionGroup.AddActionCategory(countPull_Category);

                detectableActions.AddGroup(vocalInteractionGroup);
            
            if (this.LateralTransferPage != null)
            {
                this.LateralTransferPage.LessonDetectableActionsControl.SetActionCollection(detectableActions);
            }
        }

        public override void StartLesson()
        {
            if (!KinectManager.HasKinect)   // Kinect sensor does not exist, abort the lesson.
            {
                this.AbortLesson();
            }
            else
            {
                KinectManager.AllDataReady += KinectManager_AllDataReady;
                KinectManager.SensorNotReady += KinectManager_SensorNotReady;

                Started = true;

                StartTime = DateTime.Now;

                Stopwatch.Start();

                UpdateLesson();
            }
        }

        private void KinectManager_AllDataReady()
        {
            UpdateLesson();
        }

        private void KinectManager_SensorNotReady()
        {
            if (!this.Complete)
            {
                this.AbortLesson();
            }
        }

        public override void UpdateLesson()
        {
            this.LateralTransferPage.skeletonOverlay.UpdateSkeleton();
            this.LateralTransferPage.zoneOverlay.UpdateZones();

            if (!this.Complete)
            {
                // Update the stage specific watchers.
                for (int i = this.CurrentStageIndex; i < this.Stages.Count; i++)
                {
                    Stage stage = this.Stages[i];

                    int numStagesAwayFromCurrent = (int)System.Math.Abs(i - this.CurrentStageIndex);

                    if (stage != null)
                    {
                        // For each stage, go through and update the state of all watchers relevant to which lesson is current.
                        // It is up to the stage itself to identify which watcher is relevant at various positionings of the current stage.
                        stage.UpdateWatcherStates(numStagesAwayFromCurrent == 0, numStagesAwayFromCurrent == 1, numStagesAwayFromCurrent >= 2);
                    }
                }

                // Go through and update the state of all lesson-wide watchers.
                foreach (BaseWatcher watcher in lessonWatchers)
                {
                    watcher.UpdateWatcher();
                }

                LessonWatcherUpdate();

                switch (this.CurrentStageIndex)
                {
                    case 0:
                        this.Stages[0].UpdateStage();  // do calibration
                        break;
                    case 1:
                        this.Stages[1].UpdateStage();   // check user positions
                        this.Stages[2].UpdateStage();   // check vocal input
                        this.Stages[3].UpdateStage();   // check pull end
                        break;
                    case 2:
                        this.Stages[1].UpdateStage();   // check user positions
                        this.Stages[2].UpdateStage();   // check vocal input
                        this.Stages[3].UpdateStage();   // check pull end
                        break;
                    case 3:
                        this.Stages[2].UpdateStage();   // check vocal input
                        this.Stages[3].UpdateStage();   // check pull end
                        break;
                    case 4:
                        this.Stages[4].UpdateStage();
                        break;
                }
//                this.Stages[this.CurrentStageIndex].UpdateStage();
            }
        }

        private void LessonWatcherUpdate()
        {
            if (!this.PullStarted)
            {
                if (this.manikinPullStartedWatcher.IsTriggered)
                {
                    this.PullStarted = true;
                    this.SyncPullStartTime = DateTime.Now;

                    #region Create a screenshot for the pull start.
                    BackgroundWorker worker = new BackgroundWorker();

                    worker.DoWork += delegate(object s, DoWorkEventArgs args)
                    {
                        LateralTransferResult result = (LateralTransferResult)args.Argument;

                        KinectImage image = KinectImage.TakeKinectSnapshot(true, true);

                        result.PullStartImage = image;
                    };

                    worker.RunWorkerAsync(this.Result);
                    #endregion

                    this.DetectableActions.DetectAction("Pull Started", "Start Pulling", "Manikin Torso", true);
                    this.DetectableActions.DetectAction("Pull Started", "Start Pulling", "Manikin Leg", true);

                    System.Diagnostics.Debug.WriteLine(String.Format("{0}:: Pull Started.", this.SyncPullStartTime));

                    #region Handle pull if starting out of order.
                    /* If the pull has begun, and the previous stages haven't yet ended, we need to end those stages, indicating that they weren't completed correctly.
                     * For this to happen, we can go through and call EndStage() on both the UserPositionVerification stage and the VocalInput stage, which has logic built into
                     * their EndStage() function to gracefully handle an incomplete, forced EndStage call.
                     */
                    if (!this.Stages[1].Complete)
                    {
                        this.Stages[1].EndStage();  // End the user position verification stage.
                    }
                    if (!this.Stages[2].Complete)
                    {
                        this.Stages[2].EndStage();
                    }

                    this.CurrentStageIndex = 3;
                    #endregion
                }
            }
        }

        public void PlayNextStageSound()
        {
            nextStageSound.Play();
        }

        public void PlayStageTimeoutSound()
        {
            stageTimeoutSound.Play();
        }

        /// <summary>
        /// Calculate the total score by combining the values found within the stages.
        /// </summary>
        /// <returns></returns>
        private double CalculateResultTotalScore()
        {
            double score = 0;

            if (this.Stages != null)
            {
                double totalsync = this.Result.TotalSync;

                double handplacementscore = this.Result.HandPlacementScore;

                double verbalcountscore = 0;

                if (this.Result.VerbalCount)
                {
                    verbalcountscore = 1;

                    if (this.Result.VerbalCountTime > this.Result.PullStartTime)
                    {
                        verbalcountscore -= 0.5;
                    }
                }

                this.Result.VerbalCountScore = verbalcountscore;

                score = (totalsync * 0.6) + (handplacementscore * 0.2) + (verbalcountscore * 0.2);
            }

            return score;
        }

        private void ShowResults()
        {
            //this.Result = CreateResultFromStageScores();
            if (this.LateralTransferPage != null)
            {
                this.LateralTransferPage.ShowResults(this.Result, this.SkeletonPathComparison);
            }
        }

        public override void EndLesson()
        {
            this.Complete = true;

            this.EndTime = DateTime.Now;
            this.Running = false;

            this.Stopwatch.Stop();

            this.Result.VerbalCount = this.DidVocalCount;
            this.Result.VerbalCountTime = this.VocalCountTime;

            if (((App)App.Current).CurrentStudent != null)
                this.Result.StudentID = ((App)App.Current).CurrentStudent.ID;

            this.Result.PullStartTime = this.SyncPullStartTime;
            this.Result.PullEndTime = this.SyncPullEndTime;
            this.Result.EndTime = this.EndTime;
            this.Result.StartTime = this.StartTime;

            this.Result.Score = this.CalculateResultTotalScore();   // calculate the total score using our known values

            this.CreateFeedbacks();

            this.ShowResults();
        }

        /// <summary>
        /// Fill in the Overall, Hand Placement, and Pull Feedback ID collection.
        /// </summary>
        private void CreateFeedbacks()
        {
            if (this.Result == null)
                return;

            #region Overall
            if (this.Result.Score >= 0.85)
            {
                this.Result.OverallFeedbackIDs.Add(11); // You have properly performed Lateral Transfer.
            }
            else
            {
                this.Result.OverallFeedbackIDs.Add(12); // You have improperly performed Lateral Transfer.
            }

            if (this.Result.HandPlacementScore == 1.0)
            {
                this.Result.OverallFeedbackIDs.Add(3);          // Both users were properly in position prior to the transfer.
            }
            else
            {
                this.Result.OverallFeedbackIDs.Add(13);         // Not in proper position prior to the pull.
            }

            if (this.Result.VerbalCount)
            {
                if (this.VocalCountTime < this.SyncPullStartTime)
                    this.Result.OverallFeedbackIDs.Add(6);  // You verbally coordinated the transfer.
                else
                    this.Result.OverallFeedbackIDs.Add(8);  // You did not verbally coordinate the transfer prior to the pull.

            }
            else
            {
                this.Result.OverallFeedbackIDs.Add(7);  // You did not verbally coordinate the transfer.
            }

            if (this.Result.TotalSync > 0.8)
            {
                this.Result.OverallFeedbackIDs.Add(9);  // You and your partner's movements were synchronous during the transfer.
            }
            else
            {
                this.Result.OverallFeedbackIDs.Add(10); // You and your partner's movements were not synchronous during the transfer.
            }

            #endregion
        }

        public override void AbortLesson()
        {
            // Stop calls to update the lesson.
            KinectManager.AllDataReady -= KinectManager_AllDataReady;

            // Stop the Overlays that use the Kinect sensor.
            this.LateralTransferPage.skeletonOverlay.ShowBoneOverlays = false;
            this.LateralTransferPage.skeletonOverlay.ShowJointOverlays = false;
            this.LateralTransferPage.skeletonOverlay.ShowJointTextOverlays = false;

            // Pause and cleanout the lesson.
            this.PauseLesson();
            this.WipeClean();

            // Go back to the Main Page.  This will trigger LateralTransferPage Unloaded
            if (this.LateralTransferPage != null)
                this.LateralTransferPage.GoHome();
        }

        public override void ResetLesson()
        {
            this.WipeClean();
            this.StartLesson();
        }

        public override void PauseLesson()
        {
            //throw new NotImplementedException();
            this.Paused = true;
            this.Stopwatch.Stop();

            if (this.Stages != null && this.Stages.Count > this.CurrentStageIndex)
            {
                this.Stages[this.CurrentStageIndex].PauseStage();
            }
        }

        public override void OnTutorialVideoRequest()
        {
            this.PauseLesson();

            // Show the stage help video
            this.LateralTransferPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Lateral Transfer/Videos/Test Transfer.wmv", UriKind.Relative));
            this.LateralTransferPage.VideoPlayer.startVideo(this, new RoutedEventArgs());

            this.LateralTransferPage.VideoPlayer.MediaElement.MediaEnded += this.TutorialVideo_MediaEnded;
            this.LateralTransferPage.VideoPlayer.MediaElement.MediaFailed += this.TutorialVideo_MediaFailed;
        }

        private void TutorialVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeLesson();

            this.LateralTransferPage.VideoPlayer.MediaElement.MediaEnded -= this.TutorialVideo_MediaEnded;
            this.LateralTransferPage.VideoPlayer.MediaElement.MediaFailed -= this.TutorialVideo_MediaFailed;
        }

        private void TutorialVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeLesson();

            this.LateralTransferPage.VideoPlayer.MediaElement.MediaEnded -= this.TutorialVideo_MediaEnded;
            this.LateralTransferPage.VideoPlayer.MediaElement.MediaFailed -= this.TutorialVideo_MediaFailed;
        }

        public override void ResumeLesson()
        {
            //throw new NotImplementedException();
            this.Paused = false;
            this.Stopwatch.Start();

            if (this.Stages != null && this.Stages.Count > this.CurrentStageIndex)
            {
                this.Stages[this.CurrentStageIndex].ResumeStage();
            }
        }

        public override void WipeClean()
        {
            this.MainUserSkeletonProfileID = 0;
            this.SecondaryUserSkeletonProfileID = 0;

            syncPullStartTime = DateTime.MinValue;
            syncPullEndTime = DateTime.MaxValue;
            primaryUserStartPoint = new CameraSpacePoint();
            secondaryUserStartPoint = new CameraSpacePoint();
            manikinTorsoEndPoint = new CameraSpacePoint();
            manikinLegEndPoint = new CameraSpacePoint();
            primaryUserEndPoint = new CameraSpacePoint();
            secondaryUserEndPoint = new CameraSpacePoint();
            manikinTorsoStartPoint = new CameraSpacePoint();
            manikinLegStartPoint = new CameraSpacePoint();

            skeletonPathComparison.WipeClean();
            skeletonPathComparison = null;
            skeletonPathComparison = new SkeletonPathComparison();

            pullStarted = false;
            pullEnded = false;

            manikinLegPositionWatcher.IsTriggered = false;
            manikinLegPositionWatcher.HasTriggered = false;
            manikinTorsoPositionWatcher.IsTriggered = false;
            manikinTorsoPositionWatcher.HasTriggered = false;
            manikinPullStartedWatcher.IsTriggered = false;
            manikinPullStartedWatcher.HasTriggered = false;

            prevLegLoc = new Point3D();
            prevTorsoLoc = new Point3D();

            vocalCountTime = DateTime.MinValue;
            didVocalCount = false;

            if (this.DetectableActions != null)
            {
                this.DetectableActions.ClearLogs();
                this.DetectableActions.ResetActions();
            }

            if (this.PersistentColorTracker != null)
            {
                this.PersistentColorTracker.StopTracking();
                this.PersistentColorTracker.WipeClean();
            }

            if (this.Stages != null)
            {
                foreach (Stage stage in this.Stages)
                {
                    stage.WipeClean();
                }
            }

            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.Paused = false;
            this.Running = false;
            this.CurrentStageIndex = 0;
            this.Complete = false;

            if (Result != null)
            {
                Result.WipeClean();

                this.Result = null;
                this.Result = new LateralTransferResult();
            }

            this.Stopwatch.Reset();

            KinectManager.AllDataReady -= KinectManager_AllDataReady;
            KinectManager.SensorNotReady -= KinectManager_SensorNotReady;

            if (this.LateralTransferPage != null)
            {
                this.LateralTransferPage.zoneOverlay.ClearZones();
                this.LateralTransferPage.InstructionTextBlock.Text = "";
                this.LateralTransferPage.StageInfoTextBlock.Text = "";
                this.LateralTransferPage.PatientHealthBar.Value = 1;
            }
        }
    }
}
