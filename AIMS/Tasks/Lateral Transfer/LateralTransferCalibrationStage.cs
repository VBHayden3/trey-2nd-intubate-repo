﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Kinect;
using System.Windows;
using System.Windows.Media.Media3D;
using AIMS.Assets.Lessons2;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using AIMS.Tasks;

namespace AIMS.Tasks.LateralTransfer
{
    /// <summary>
    /// Calibration stage for the Lateral Transfer lesson.
    /// 
    /// Stage Goals:
    /// 1. Locate and identify the position of the upper and lower parts of the manikin.
    /// 2. Locate and identify the ID of the primary and secondary user.
    /// </summary>
    public class LateralTransferCalibrationStage : Stage
    {
        private LateralTransfer lesson;
        public LateralTransfer Lesson { get { return lesson; } set { lesson = value; } }

        private Watcher manikinTorsoWatcher;
        private Watcher manikinLegWatcher;

        private Watcher firstUserSkeletonWatcher;
        private Watcher secondUserSkeletonWatcher;

        private Point3D watcher_manikinTorsoLocation = new Point3D();
        private Point3D watcher_manikinLegLocation = new Point3D();

        private ulong? watcher_firstUserSkeletonID = null;
        private ulong? watcher_secondUserSkeletonID = null;

        private ulong? primaryUserID = null;
        private ulong? secondaryUserID = null;

        private bool bothUsersLocated = false;
        private bool bothUserRolesIdentified = false;

        private bool bothTorsoAndLegLocated = false;

        private bool hasFoundTorso = false;
        private bool hasFoundLeg = false;
        private Point3D torsoLocation = new Point3D();
        private Point3D legLocation = new Point3D();

        private bool foundTorso = false;
        private bool foundLeg = false;

        private int torsoScanIndex = 0;
        private int legScanIndex = 0;

        public LateralTransferCalibrationStage( LateralTransfer lesson )
        {
            Lesson = lesson;

            StageNumber = 0;
            Name = "Calibration";
            Instruction = "Place one hand on the manikin's face, and the other in the air.";
            TargetTime = TimeSpan.MaxValue; // No target time for calibration...
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Tasks/Lateral Transfer/Graphics/LateralTransfer000.png";
            Active = false;
            Complete = false;
            ScoreWeight = 0;    // Calibration stage is not weighted
            Zones = new List<Zone>();
            Stopwatch = new System.Diagnostics.Stopwatch();
            TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.TimeOutTime = TimeSpan.FromSeconds(30.0);
            this.CanTimeOut = false;
        }

        public override void SetupStage()
        {
            this.Lesson.LateralTransferPage.StageInfoTextBlock.Text = this.Name;
            this.Lesson.LateralTransferPage.InstructionTextBlock.Text = this.Instruction;

            // Set the image for the current stage.
            //this.Lesson.LateralTransferPage.CurrentStageImage.Source = new BitmapImage(new Uri(this.ImagePath, UriKind.Relative));

            //System.Diagnostics.Debug.WriteLine(String.Format("Set up stage {0}!", this.StageNumber));

            this.Lesson.LateralTransferPage.flow.Index = this.Lesson.CurrentStageIndex;  // change cover flow image.

            this.Zones.Clear(); // Clear the stage zone list.

            this.Lesson.LateralTransferPage.zoneOverlay.ClearZones();

            // Add the zones into the zoneOverlay
            foreach (Zone zone in this.Zones)
            {
                this.Lesson.LateralTransferPage.zoneOverlay.AddZone(zone);
            }

            this.CurrentStageWatchers = new List<BaseWatcher>();
            this.PeripheralStageWatchers = new List<BaseWatcher>();
            this.OutlyingStageWatchers = new List<BaseWatcher>();

            CreateStageWatchers();

            this.StartTime = DateTime.Now;

            this.IsSetup = true;

            this.Stopwatch.Start();

            this.TimeoutStopwatch.Start();
        }

        private void CreateStageWatchers()
        {
            // Clear the list of watchers.
            this.CurrentStageWatchers.Clear();
            this.PeripheralStageWatchers.Clear();
            this.OutlyingStageWatchers.Clear();

            // Add in new watchers.
            firstUserSkeletonWatcher = new Watcher();

            #region WatcherLogic

            // Checks for the primary skeleton to become sticky.
            firstUserSkeletonWatcher.CheckForActionDelegate = () =>
            {
                if (this.Complete)
                    return;

                if (!watcher_firstUserSkeletonID.HasValue || watcher_firstUserSkeletonID == 0)   // Lock onto a skeleton target.
                {
                    if (KinectManager.Bodies != null && KinectManager.Bodies.Length > 0) // Ensure that there are skeletons to select from.
                    {
                        if (KinectManager.SkeletonProfiles != null && KinectManager.SkeletonProfiles.Count >= 1 && KinectManager.SkeletonProfiles[0] != null && KinectManager.SkeletonProfiles[0].CurrentTrackingID.HasValue)
                        {
                            this.watcher_firstUserSkeletonID = KinectManager.SkeletonProfiles[0].CurrentTrackingID;

                            firstUserSkeletonWatcher.IsTriggered = true;
                        }
                        else
                        {
                            this.watcher_firstUserSkeletonID = null;

                            firstUserSkeletonWatcher.IsTriggered = false;
                        }
                    }
                    return;
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(firstUserSkeletonWatcher);

            secondUserSkeletonWatcher = new Watcher();

            #region WatcherLogic

            // Checks for the primary skeleton to become sticky.
            secondUserSkeletonWatcher.CheckForActionDelegate = () =>
            {
                if (this.Complete)
                    return;

                if (!watcher_secondUserSkeletonID.HasValue || watcher_secondUserSkeletonID == 0)   // Lock onto a skeleton target.
                {
                    if (KinectManager.Bodies != null && KinectManager.Bodies.Length > 0) // Ensure that there are skeletons to select from.
                    {
                        if (KinectManager.SkeletonProfiles != null && KinectManager.SkeletonProfiles.Count >= 2 && KinectManager.SkeletonProfiles[1] != null && KinectManager.SkeletonProfiles[1].CurrentTrackingID.HasValue)
                        {
                            this.watcher_secondUserSkeletonID = KinectManager.SkeletonProfiles[1].CurrentTrackingID;
                            //hasLockedOnToTarget = true;
                            secondUserSkeletonWatcher.IsTriggered = true;

                        }
                        else
                        {
                            this.watcher_secondUserSkeletonID = null;
                            secondUserSkeletonWatcher.IsTriggered = false;
                        }
                    }

                    return;
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(secondUserSkeletonWatcher);

            manikinTorsoWatcher = new Watcher();

            #region WatcherLogic

            // Searches for the Torso of the Manikin.
            manikinTorsoWatcher.CheckForActionDelegate = () =>
            {
                System.Diagnostics.Debug.WriteLine("manikinTorsoWatcher");

                if (this.Complete)
                    return;

                if (this.foundTorso)
                    return;

                SkeletonProfile firstuser = KinectManager.GetSkeletonProfile(watcher_firstUserSkeletonID, true);

                if (firstuser == null) 
                    return;

                // Scans the color image frame from bottom to top searching for the color of the torso.
                Point found = MuliFrameColorScan(ColorTracker.GetColorRange("yellow-torso", Lighting.none), 80, 480, 480, 12, KinectManager.MappedColorData, torsoScanIndex, true, true);//ColorTracker.find(ColorTracker.RedTape, new Int32Rect(0, 640 - (32 * (i + 1)), 64, 32), KinectManager.MappedColorData);

                //System.Diagnostics.Debug.WriteLine(String.Format("TorsoWatcher: X, Y ({0}, {1})", found.X, found.Y));

                if (!Double.IsNaN(found.X) || !Double.IsNaN(found.Y))
                {
                    //int pixelIndex = 640 * (int)found.Y + (int)found.X;

                    int depth = KinectManager.GetDepthValue((int)found.X, (int)found.Y);
                    if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                    {
                        Point3D loc = KinectManager.Get3DPointForColorPoint(found, depth);

                        //System.Diagnostics.Debug.WriteLine(String.Format("TorsoWatcher: User.Z, Color Depth ({0}, {1})", firstuser.CurrentPosition.Z*1000, depth));

                        if ( (firstuser.CurrentPosition.Z * 1000) > depth)   // ensure that the depth of the color is infront of the user since their should really be 1.5 tables inbetween the user and the found point.
                        {
                            this.watcher_manikinTorsoLocation = loc;
                            this.foundTorso = true;
                            // Passed depth validation check.
                            //System.Diagnostics.Debug.WriteLine(String.Format("TorsoWatcher - Found Torso: X, Y, Z ({0}, {1}, {2})", this.watcher_manikinTorsoLocation.X, this.watcher_manikinTorsoLocation.Y, this.watcher_manikinTorsoLocation.Z));
                            manikinTorsoWatcher.IsTriggered = true;
                        }
                        else
                        {
                            // Invalid based on depth validation.
                            //System.Diagnostics.Debug.WriteLine("Torso: Invalid Depth");
                            manikinTorsoWatcher.IsTriggered = false;
                        }
                    }
                    else
                    {
                        //System.Diagnostics.Debug.WriteLine("Torso: No Depth");
                        manikinTorsoWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    //System.Diagnostics.Debug.WriteLine("Torso: Not Found");
                    manikinTorsoWatcher.IsTriggered = false;
                }

                torsoScanIndex++;

                if (torsoScanIndex > 10)
                {
                    torsoScanIndex = 0; // reset the scan index.
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(manikinTorsoWatcher);

            manikinLegWatcher = new Watcher();

            #region WatcherLogic

            // Searches for the Leg of the Manikin.
            manikinLegWatcher.CheckForActionDelegate = () =>
            {
                if (this.Complete)
                    return;

                if (this.foundLeg)
                    return;

                SkeletonProfile firstuser = KinectManager.GetSkeletonProfile(watcher_firstUserSkeletonID, true);

                if (firstuser == null)
                    return;

                // scan the bottom half of the image searching for red, starting at the bottom and moving upwards by 32 pixels.
                // for every red value found, validate that it has a depth, and also is infront of the user.
                
                // Scans the color image frame from bottom to top searching for the color of the leg.
                Point found = MuliFrameColorScan(ColorTracker.GetColorRange("red-leg", Lighting.none), 80, 480, 480, 24, KinectManager.MappedColorData, legScanIndex, true, true);

                //System.Diagnostics.Debug.WriteLine(String.Format("LegWatcher: X, Y ({0}, {1})", found.X, found.Y));

                if (!Double.IsNaN(found.X) || !Double.IsNaN(found.Y))
                {
                    //int pixelIndex = 640 * (int)found.Y + (int)found.X;

                    int depth = KinectManager.GetDepthValue((int)found.X, (int)found.Y);
                    if (depth >= KinectManager.ReliableMinDepth && depth <= KinectManager.ReliableMaxDepth) 
                    {
                        Point3D loc = KinectManager.Get3DPointForColorPoint(found, depth);

                        //System.Diagnostics.Debug.WriteLine(String.Format("LegWatcher: User.Z, Color Depth ({0}, {1})", firstuser.CurrentPosition.Z * 1000, depth));
                        // @todo determine if depth is m or mm
                        if ( (firstuser.CurrentPosition.Z * 1000) > depth)   // ensure that the depth of the color is infront of the user since there should really be 1.5 tables inbetween the user and the found point.
                        {
                            this.watcher_manikinLegLocation = loc;
                            this.foundLeg = true;
                            // Passed depth validation check.
                            //System.Diagnostics.Debug.WriteLine(String.Format("LegWatcher - Found Leg: X, Y, Z ({0}, {1}, {2})", this.watcher_manikinLegLocation.X, this.watcher_manikinLegLocation.Y, this.watcher_manikinLegLocation.Z));
                            manikinLegWatcher.IsTriggered = true;
                        }
                        else
                        {
                            // Invalid based on depth validation.
                            //System.Diagnostics.Debug.WriteLine("Leg: Invalid Depth");
                            manikinLegWatcher.IsTriggered = false;
                        }
                    }
                    else
                    {
                        //System.Diagnostics.Debug.WriteLine("Leg: No Depth");
                        manikinLegWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    //System.Diagnostics.Debug.WriteLine("Leg: Not Found");
                    manikinLegWatcher.IsTriggered = false;
                }

                legScanIndex++;

                if (legScanIndex > 10)
                {
                    legScanIndex = 0; // reset the scan index.
                }
            };

            #endregion

            this.CurrentStageWatchers.Add(manikinLegWatcher);
        }

        /// <summary>
        /// This function is used to scan through the colorData in an Int32Rect using the x, y, width, height values searching for the color range.
        /// 
        /// The purpose of this method is to spread out the iterations of color tracking scans across multiple frames.
        /// 
        /// This method is good for finding stationary objects at an area, and is currently being used while scanning the colorData from the bottom up in seach for the
        /// legs and torso tape.
        /// 
        /// To use this function, call the same method, incrementing the index per frame.
        /// 
        /// The function will multiply the index by the scan height to determine how far away relative to the starting point it should scan.
        /// 
        /// Vertical is whether you want to go from Top to Bottom versus Left to Right.
        /// 
        /// Reverse is whether you want to go from Bottom to Top versus Top to Bottom.
        /// </summary>
        /// <param name="color"></param>
        /// <param name="startX"></param>
        /// <param name="startY"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="colorData"></param>
        /// <param name="scanIndex"></param>
        /// <param name="reverse"></param>
        /// <param name="vertical"></param>
        private Point MuliFrameColorScan( AIMS.Assets.Lessons2.ColorRange color, int startX, int startY, int width, int height, byte[] colorData, int index, bool reverse, bool vertical )
        {
            Point found = ColorTracker.find(color, new Int32Rect(vertical ? startX : startX + (reverse ? -1 : 1) * (width * (index + 1)), (vertical ? startY + (reverse ? -1 : 1) * (height * (index + 1)) : startY), width, height), colorData);

            return found;
        }

        public override void UpdateStage()
        {
            System.Diagnostics.Debug.WriteLine("UpdateStage");

            if (!IsSetup && this.Lesson.CurrentStageIndex == this.StageNumber)
            {
                SetupStage();
            }

            if (KinectManager.DepthWidth == 0)
                return;

            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
            {
                this.OnStageTimeout();
            }

            if (this.firstUserSkeletonWatcher.IsTriggered && this.secondUserSkeletonWatcher.IsTriggered)
            {
                if (!this.bothUsersLocated)
                {
                    this.bothUsersLocated = true;

                    this.Lesson.DetectableActions.DetectAction("Became Sticky", "Become Sticky", "Primary User", true);
                    this.Lesson.DetectableActions.DetectAction("Became Sticky", "Become Sticky", "Secondary User", true);
                }
            }
            else
            {
                if (this.bothUsersLocated)
                {
                    this.bothUsersLocated = false;

                    this.Lesson.DetectableActions.UndetectAction("Became Sticky", "Become Sticky", "Primary User", true);
                    this.Lesson.DetectableActions.UndetectAction("Became Sticky", "Become Sticky", "Secondary User", true);
                }
            }

            if (this.manikinLegWatcher.IsTriggered)
            {
                if (!this.hasFoundLeg)
                {
                    this.hasFoundLeg = true;

                    this.legLocation = watcher_manikinLegLocation;
                    this.Lesson.ManikinLegLocation = this.legLocation;
                    
                    this.Lesson.DetectableActions.DetectAction("Initially Located", "Locate Initially", "Manikin Leg", true);

                    //System.Diagnostics.Debug.WriteLine(String.Format("UpdateStage - Found Leg: X, Y, Z ({0}, {1}, {2})", this.Lesson.ManikinLegLocation.X, this.Lesson.ManikinLegLocation.Y, this.Lesson.ManikinLegLocation.Z));
                }
            }

            if (this.manikinTorsoWatcher.IsTriggered)
            {
                if (!this.hasFoundTorso)
                {
                    this.hasFoundTorso = true;

                    this.torsoLocation = watcher_manikinTorsoLocation;
                    this.Lesson.ManikinTorsoLocation = this.torsoLocation;

                    this.Lesson.DetectableActions.DetectAction("Initially Located", "Locate Initially", "Manikin Torso", true);

                    //System.Diagnostics.Debug.WriteLine(String.Format("UpdateStage - Found Torso: X, Y, Z ({0}, {1}, {2})", this.Lesson.ManikinTorsoLocation.X, this.Lesson.ManikinTorsoLocation.Y, this.Lesson.ManikinTorsoLocation.Z));
                }
            }
            /*
            if (this.hasFoundTorso && this.hasFoundLeg)
            {
                if (watcher_firstUserSkeletonID != null && watcher_secondUserSkeletonID != null)
                {
                    // Determine which of the two skeletons are at the torso, and which is at the leg.
                    double firstUserToTorso = this.skeletonToTorso2DDistance((int)this.watcher_firstUserSkeletonID);
                    double secondUserToTorso = this.skeletonToTorso2DDistance((int)this.watcher_secondUserSkeletonID);
                }
            }*/

            if (this.hasFoundTorso && this.hasFoundLeg) // Torso and Legs have both been found.
            {
                if (watcher_firstUserSkeletonID != null && watcher_secondUserSkeletonID != null)    // Both skeletons have been located - but there roles not identified.
                {
                    // Determine which of the two skeletons are at the torso, and which is at the leg.
                    double firstUserToTorso = this.skeletonToTorso2DDistance((ulong)this.watcher_firstUserSkeletonID);
                    double secondUserToTorso = this.skeletonToTorso2DDistance((ulong)this.watcher_secondUserSkeletonID);

                    //System.Diagnostics.Debug.WriteLine(String.Format("User1 to torso: {0}", firstUserToTorso));
                    //System.Diagnostics.Debug.WriteLine(String.Format("User2 to torso: {0}", secondUserToTorso));

                    if (firstUserToTorso < secondUserToTorso)
                    {
                        // 1st user is closer to the torso. Set the 1st user as the Primary, and the 2nd user as the Secondary.
                        this.primaryUserID = watcher_firstUserSkeletonID;
                        this.secondaryUserID = watcher_secondUserSkeletonID;

                        this.bothUserRolesIdentified = true;
                    }
                    else
                    {
                        // 2nd user is closer to the torso. Set the 2nd user as the Primary, and the 1st user as the Secondary.
                        this.primaryUserID = watcher_secondUserSkeletonID;
                        this.secondaryUserID = watcher_firstUserSkeletonID;

                        this.bothUserRolesIdentified = true;
                    }
                }
            }

            if (this.bothUserRolesIdentified)
            {
                this.Lesson.MainUserSkeletonProfileID = this.primaryUserID;
                this.Lesson.SecondaryUserSkeletonProfileID = this.secondaryUserID;

                this.Lesson.PrimaryUserStartPoint = this.Lesson.MainUserSkeletonProfile.CurrentPosition;
                this.Lesson.SecondaryUserStartPoint = this.Lesson.SecondaryUserSkeletonProfile.CurrentPosition;

                this.Lesson.ManikinTorsoStartPoint = KinectManager.CoordinateMapper.MapDepthPointToCameraSpace(new DepthSpacePoint() { X = (int)this.Lesson.ManikinTorsoLocation.X, Y = (int)this.Lesson.ManikinTorsoLocation.Y}, (ushort)this.Lesson.ManikinTorsoLocation.Z);
                this.Lesson.ManikinLegStartPoint = KinectManager.CoordinateMapper.MapDepthPointToCameraSpace(new DepthSpacePoint() { X = (int)this.Lesson.ManikinLegLocation.X, Y = (int)this.Lesson.ManikinLegLocation.Y }, (ushort)this.Lesson.ManikinLegLocation.Z);

                #region Add the two relevant skeletons to the SkeletonPathComparison

                // Set which joints to track for comparison.
                bool head = false;
                bool shoulderCenter = false;
                bool shoulderRight = true;  // +
                bool shoulderLeft = true;   // +
                bool elbowRight = true;     // +
                bool elbowLeft = true;      // +
                bool wristRight = true;     // +
                bool wristLeft = true;      // +
                bool handRight = true;      // +
                bool handLeft = true;       // +
                bool spine = false;
                bool hipCenter = false;
                bool hipRight = false;
                bool hipLeft = false;
                bool kneeRight = false;
                bool kneeLeft = false;
                bool ankleRight = false;
                bool ankleLeft = false;
                bool footRight = false;
                bool footLeft = false;

                // Set the skeleton1 and skeleton2 in the path comparison object, and designate the options for which joints to track.
                this.Lesson.SkeletonPathComparison.SetSkeleton1((ulong)this.Lesson.MainUserSkeletonProfileID, head, shoulderCenter, shoulderRight, shoulderLeft, elbowRight, elbowLeft, wristRight, wristLeft, handRight, handLeft, spine, hipCenter, hipRight, hipLeft, kneeRight, kneeLeft, ankleRight, ankleLeft, footRight, footLeft);
                this.Lesson.SkeletonPathComparison.SetSkeleton2((ulong)this.Lesson.SecondaryUserSkeletonProfileID, head, shoulderCenter, shoulderRight, shoulderLeft, elbowRight, elbowLeft, wristRight, wristLeft, handRight, handLeft, spine, hipCenter, hipRight, hipLeft, kneeRight, kneeLeft, ankleRight, ankleLeft, footRight, footLeft);

                // Begin recording the skeleton path data.
                this.Lesson.SkeletonPathComparison.StartRecording();
                #endregion

                this.Complete = true;
                EndStage();
            }        
        }

        private double skeletonToTorso2DDistance(ulong skeletonID)
        {
            SkeletonProfile p = KinectManager.GetSkeletonProfile(skeletonID, true);

            if (p != null)
            {
                double distance = KinectManager.Distance2D(KinectManager.CoordinateMapper.MapDepthPointToCameraSpace(new DepthSpacePoint() { X = (int)torsoLocation.X, Y = (int)torsoLocation.Y }, (ushort)torsoLocation.Z), p.CurrentPosition);

                //System.Diagnostics.Debug.WriteLine(String.Format("Skeleton {1} to Torso = {0}", distance, skeletonID));

                return distance;
            }

            return 0;
        }

        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }

        public override void EndStage( bool playSound = true )
        {
            Stopwatch.Stop();

            this.TimeoutStopwatch.Stop();

            this.EndTime = DateTime.Now;

            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                LateralTransferResult result = (LateralTransferResult)args.Argument;

                KinectImage image = KinectImage.TakeKinectSnapshot(true, true);

                result.CalibrationImage = image;
            };

            worker.RunWorkerAsync(this.Lesson.Result);

            this.Lesson.CurrentStageIndex++;

            if( playSound ) Lesson.PlayNextStageSound();

            this.Lesson.Running = true;
        }

        public override void PauseStage()
        {
            this.Lesson.Paused = true;
            this.Lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.Lesson.Paused = false;
            this.Lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void ResetStage()
        {

        }

        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            //this.PauseStage();
            this.TimedOut = true;
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            this.RequestStageVideo();   // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
        }

        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.Lesson.LateralTransferPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/LateralTransfer/Videos/Stage 000.wmv", UriKind.Relative));
            this.Lesson.LateralTransferPage.VideoPlayer.startVideo(this, new RoutedEventArgs());

            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        public override void WipeClean()
        {
            if (manikinTorsoWatcher != null)
            {
                manikinTorsoWatcher.IsTriggered = false;
                manikinTorsoWatcher.HasTriggered = false;
            }

            if (manikinLegWatcher != null)
            {
                manikinLegWatcher.IsTriggered = false;
                manikinLegWatcher.HasTriggered = false;
            }

            if (firstUserSkeletonWatcher != null)
            {
                firstUserSkeletonWatcher.IsTriggered = false;
                firstUserSkeletonWatcher.HasTriggered = false;
            }

            if (secondUserSkeletonWatcher != null)
            {
                secondUserSkeletonWatcher.IsTriggered = false;
                secondUserSkeletonWatcher.HasTriggered = false;
            }

            watcher_manikinTorsoLocation = new Point3D();
            watcher_manikinLegLocation = new Point3D();

            watcher_firstUserSkeletonID = null;
            watcher_secondUserSkeletonID = null;

            primaryUserID = null;
            secondaryUserID = null;

            bothUsersLocated = false;
            bothUserRolesIdentified = false;

            bothTorsoAndLegLocated = false;

            hasFoundTorso = false;
            hasFoundLeg = false;

            torsoLocation = new Point3D();
            legLocation = new Point3D();

            foundTorso = false;
            foundLeg = false;

            torsoScanIndex = 0;
            legScanIndex = 0;

            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;

            this.IsSetup = false;

            if (this.StageScore != null)
            {
                this.StageScore.WipeClean();
                this.StageScore = null;
                this.StageScore = new StageScore();
            }
            else
            {
                this.StageScore = new StageScore();
            }

            this.Active = false;
            this.Complete = false;

            if (this.Zones != null)
            {
                this.Zones.Clear();
            }
            else
            {
                this.Zones = new List<Zone>();
            }

            if (this.Stopwatch != null)
            {
                this.Stopwatch.Reset();
            }
            else
            {
                this.Stopwatch = new System.Diagnostics.Stopwatch();
            }

            if (this.TimeoutStopwatch != null)
            {
                this.TimeoutStopwatch.Reset();
            }
            else
            {
                this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            }

            this.TimedOut = false;
        }
    }
}
