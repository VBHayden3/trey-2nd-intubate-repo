﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Kinect;
using System.ComponentModel;

namespace AIMS.Tasks.LateralTransfer
{
    public class LateralTransferDisplayResultsStage : Stage
    {
        private LateralTransfer lesson;
        public LateralTransfer Lesson { get { return lesson; } set { lesson = value; } }

        private CameraSpacePoint initialSkeletonDepth = new CameraSpacePoint();
        public CameraSpacePoint InitialSkeletonDepth { get { return initialSkeletonDepth; } set { initialSkeletonDepth = value; } }

        public LateralTransferDisplayResultsStage(LateralTransfer lesson)
        {
            Lesson = lesson;

            StageNumber = 4;
            Name = "View Results";
            Instruction = "Lateral Transfer complete. Step back to view your results.";
            TargetTime = TimeSpan.MaxValue;
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Assets/Graphics/checklist-icon-psd.jpeg";
            Active = false;
            Complete = false;
            ScoreWeight = 0;
            Zones = new List<Zone>();
            IsSetup = false;
            Stopwatch = new System.Diagnostics.Stopwatch();
            this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.CanTimeOut = false;
            this.TimeOutTime = TimeSpan.FromSeconds(10.0);
        }

        public override void SetupStage()
        {
            this.Lesson.LateralTransferPage.StageInfoTextBlock.Text = this.Name;
            this.Lesson.LateralTransferPage.InstructionTextBlock.Text = this.Instruction;

            // Set the image for the current stage.
            //this.Lesson.LateralTransferPage.CurrentStageImage.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri(this.ImagePath, UriKind.Relative));

            System.Diagnostics.Debug.WriteLine(String.Format("Set up stage {0}!", this.StageNumber));

            this.Lesson.LateralTransferPage.flow.Index = this.Lesson.CurrentStageIndex;  // change cover flow image.

            this.Zones.Clear(); // Clear the stage zone list.

            this.Lesson.LateralTransferPage.zoneOverlay.ClearZones();

            if (KinectManager.ClosestSkeletonProfile != null)
            {
                // Check for step back (get initial skeleton depth).
                this.InitialSkeletonDepth = KinectManager.ClosestSkeletonProfile.CurrentPosition;
            }

            // Add the zones into the zoneOverlay
            foreach (Zone zone in this.Zones)
            {
                this.Lesson.LateralTransferPage.zoneOverlay.AddZone(zone);
            }

            StartTime = DateTime.Now;

            this.IsSetup = true;

            Stopwatch.Start();
            this.TimeoutStopwatch.Start();
        }

        public override void UpdateStage()
        {
            if (!this.IsSetup && this.Lesson.CurrentStageIndex == this.StageNumber)
                SetupStage();

            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
                this.TimeoutStage();

            /*
             * REMOVED: This block of code could alter the overall lesson's total time.
             *          To delay the showing of results we'll instead just add a timer to the result component's show result block.
             *          (Which actually helps us gain some loading time for the results component UI to populate data).
            // Wait up to 3 seconds or for the user to step back to display the results.
            if (this.Stopwatch.Elapsed > TimeSpan.FromSeconds(3.0) || (KinectManager.ClosestSkeletonProfile != null && KinectManager.ClosestSkeletonProfile.CurrentPosition.Z >= initialSkeletonDepth.Z + 40))
            {
                this.Complete = true;
                this.EndStage();
            }
            */

            this.EndStage();    // End the stage.
        }

        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }

        public override void EndStage( bool playSound = true )
        {
            Complete = true;

            this.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            
            // Get the current time to signify the time the stage ended.
            EndTime = DateTime.Now;

            this.Lesson.EndLesson();
        }

        public override void PauseStage()
        {
            this.Lesson.Paused = true;
            this.Lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.Lesson.Paused = false;
            this.Lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void ResetStage()
        {

        }

        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            //this.PauseStage();
            this.TimedOut = true;
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            this.RequestStageVideo();   // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
        }

        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.Lesson.LateralTransferPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Intubation/Videos/IntubationStage10_Vid.mov", UriKind.Relative));
            this.Lesson.LateralTransferPage.VideoPlayer.startVideo(this, new System.Windows.RoutedEventArgs());

            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        public override void WipeClean()
        {
            initialSkeletonDepth = new CameraSpacePoint();

            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.IsSetup = false;

            if (this.StageScore != null)
            {
                this.StageScore.WipeClean();
                this.StageScore = null;
                this.StageScore = new StageScore();
            }
            else
            {
                this.StageScore = new StageScore();
            }

            this.Active = false;
            this.Complete = false;

            if (this.Zones != null)
            {
                this.Zones.Clear();
            }
            else
            {
                this.Zones = new List<Zone>();
            }

            if (this.Stopwatch != null)
            {
                this.Stopwatch.Reset();
            }
            else
            {
                this.Stopwatch = new System.Diagnostics.Stopwatch();
            }

            if (this.TimeoutStopwatch != null)
                this.TimeoutStopwatch.Reset();
            else this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();

            this.TimedOut = false;
        }
    }
}
