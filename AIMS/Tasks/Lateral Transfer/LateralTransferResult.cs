﻿using Microsoft.Kinect;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace AIMS.Tasks.LateralTransfer
{
    public class LateralTransferResult : TaskResult
    {
        private const string OVERALL_RESULTS_TABLE_NAME = "taskresults";
        private const string LATERAL_TRANSFER_RESULTS_TABLE_NAME = "taskresults_lateraltransfer";
        private const string IMAGES_TABLE_NAME = "kinect_images";

        #region Private Fields
        private DateTime pullStartTime = DateTime.MinValue;
        private DateTime pullEndTime = DateTime.MaxValue;
        private double totalSync = 0;
        private double rHandSync = 0;
        private double lHandSync = 0;
        private double rWristSync = 0;
        private double lWristSync = 0;
        private double rElbowSync = 0;
        private double lElbowSync = 0;
        private double rShoulderSync = 0;
        private double lShoulderSync = 0;
        private double manikinSync = 0;

        private double handPlacementScore = 0;

        private bool verbalCount = false;
        private DateTime verbalCountTime = DateTime.MinValue;
        private double verbalCountScore = 0;

        private int? calibrationImageID = 0; // uid is given by postgresql upon insertion using a self-incrementing "serial" int. It will be 0, unless otherwise specified - indicating it's uid in the postgresql database.
        private KinectImage calibrationImage = null;

        private int? handPlacementImageID = 0;
        private KinectImage handPlacementImage = null;

        private int? pullStartImageID = 0;
        private KinectImage pullStartImage = null;

        private int? pullEndImageID = 0;
        private KinectImage pullEndImage = null;

        private List<int> overallFeedbackIDs = new List<int>();    // list which holds feedback uid's pertaining to the feedback strings relevant to this run - which are being stored in the database's lateral transfer lookup table.
        private List<int> handPlacementFeedbackIDs = new List<int>();
        private List<int> pullFeedbackIDs = new List<int>();
        #endregion

        #region Public Properties
        public DateTime PullStartTime { get { return pullStartTime; } set { pullStartTime = value; } }
        public DateTime PullEndTime { get { return pullEndTime; } set { pullEndTime = value; } }
        public double TotalSync { get { return totalSync; } set { totalSync = value; } }
        public double RHandSync { get { return rHandSync; } set { rHandSync = value; } }
        public double LHandSync { get { return lHandSync; } set { lHandSync = value; } }
        public double RWristSync { get { return rWristSync; } set { rWristSync = value; } }
        public double LWristSync { get { return lWristSync; } set { lWristSync = value; } }
        public double RElbowSync { get { return rElbowSync; } set { rElbowSync = value; } }
        public double LElbowSync { get { return lElbowSync; } set { lElbowSync = value; } }
        public double RShoulderSync { get { return rShoulderSync; } set { rShoulderSync = value; } }
        public double LShoulderSync { get { return lShoulderSync; } set { lShoulderSync = value; } }
        public double ManikinSync { get { return manikinSync; } set { manikinSync = value; } }

        public double HandPlacementScore { get { return handPlacementScore; } set { handPlacementScore = value; } }

        public bool VerbalCount { get { return verbalCount; } set { verbalCount = value; } }
        public DateTime VerbalCountTime { get { return verbalCountTime; } set { verbalCountTime = value; } }
        public double VerbalCountScore { get { return verbalCountScore; } set { verbalCountScore = value; } }

        public int? CalibrationImageID { get { return calibrationImageID; } set { calibrationImageID = value; } }
        public KinectImage CalibrationImage { get { return calibrationImage; } set { calibrationImage = value; } }

        public int? HandPlacementImageID { get { return handPlacementImageID; } set { handPlacementImageID = value; } }
        public KinectImage HandPlacementImage { get { return handPlacementImage; } set { handPlacementImage = value; } }

        public int? PullStartImageID { get { return pullStartImageID; } set { pullStartImageID = value; } }
        public KinectImage PullStartImage { get { return pullStartImage; } set { pullStartImage = value; } }

        public int? PullEndImageID { get { return pullEndImageID; } set { pullEndImageID = value; } }
        public KinectImage PullEndImage { get { return pullEndImage; } set { pullEndImage = value; } }

        public List<int> OverallFeedbackIDs { get { return overallFeedbackIDs; } set { overallFeedbackIDs = value; } }
        public List<int> HandPlacementFeedbackIDs { get { return handPlacementFeedbackIDs; } set { handPlacementFeedbackIDs = value; } }
        public List<int> PullFeedbackIDs { get { return pullFeedbackIDs; } set { pullFeedbackIDs = value; } }
        #endregion

        #region Constructors
        public LateralTransferResult() : base()
        {
            this.TaskType = Task.LateralTransfer;
        }
        #endregion

        #region AIMS Usable Methods
        /// <summary>
        /// Resets all values of the result object back to their defaults.
        /// </summary>
        public void WipeClean()
        {
            uid = 0;    // uid is given by postgresql upon insertion using a self-incrimenting "serial" int. It will be 0, unless otherwise specified - indicating it's uid in the postgresql database.

            studentID = 0;  // uid of the student who performed the lesson.

            score = 0;
            startTime = DateTime.MinValue;
            endTime = DateTime.MaxValue;
            taskMode = TaskMode.Practice;
            masteryLevel = MasteryLevel.Novice;
            submissionTime = DateTime.MinValue;
            assignmentID = null;

            pullStartTime = DateTime.MinValue;
            pullEndTime = DateTime.MaxValue;
            totalSync = 0;
            rHandSync = 0;
            lHandSync = 0;
            rWristSync = 0;
            lWristSync = 0;
            rElbowSync = 0;
            lElbowSync = 0;
            rShoulderSync = 0;
            lShoulderSync = 0;
            manikinSync = 0;

            handPlacementScore = 0;

            verbalCount = false;
            verbalCountTime = DateTime.MinValue;
            verbalCountScore = 0;

            calibrationImageID = null; // uid is given by postgresql upon insertion using a self-incrimenting "serial" int. It will be 0, unless otherwise specified - indicating it's uid in the postgresql database.
            calibrationImage = null;

            handPlacementImageID = null;
            handPlacementImage = null;

            pullStartImageID = null;
            pullStartImage = null;

            pullEndImageID = null;
            pullEndImage = null;

            overallFeedbackIDs = new List<int>();    // list which holds feedback uid's pertaining to the feedback strings relevant to this run - which are being stored in the database's lateral transfer lookup table.
            handPlacementFeedbackIDs = new List<int>();
            pullFeedbackIDs = new List<int>();
        }
        #endregion

        #region Database Interaction Methods
        /// <summary>
        /// Stores the result into the database.
        /// </summary>
        /// <param name="result"></param>
        public static int StoreResult(LateralTransferResult result)
        {
            int id = 0;

            if (result == null)
                return id;

            #region Store the KinectImages - if they exist - to the images table.
            result.CalibrationImageID = KinectImage.StoreImage(result.CalibrationImage, IMAGES_TABLE_NAME);

            result.HandPlacementImageID = KinectImage.StoreImage(result.HandPlacementImage, IMAGES_TABLE_NAME);

            result.PullStartImageID = KinectImage.StoreImage(result.PullStartImage, IMAGES_TABLE_NAME);

            result.PullEndImageID = KinectImage.StoreImage(result.PullEndImage, IMAGES_TABLE_NAME);
            #endregion

            // Submit the overal result information to the overal results table - returning its generated uid.
            #region Store the overall result to results table.
            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "INSERT INTO " + OVERALL_RESULTS_TABLE_NAME + " ( studentid, assignmentid, tasktype, taskmode, masterylevel, score, starttime, endtime ) VALUES ( :studentid, :assignmentid, CAST( :tasktype as \"TaskType\"), CAST(:taskmode as \"TaskMode\"), CAST(:masterylevel as \"MasteryLevel\"), :score, :starttime, :endtime ) RETURNING uid;";

            command.Parameters.Add("studentid", NpgsqlTypes.NpgsqlDbType.Integer).Value = result.StudentID;
            command.Parameters.Add("assignmentid", NpgsqlTypes.NpgsqlDbType.Integer).Value = result.AssignmentID;
            command.Parameters.Add("tasktype", NpgsqlTypes.NpgsqlDbType.Text).Value = result.TaskType.ToString();
            command.Parameters.Add("taskmode", NpgsqlTypes.NpgsqlDbType.Text).Value = result.TaskMode.ToString();
            command.Parameters.Add("masterylevel", NpgsqlTypes.NpgsqlDbType.Text).Value = result.MasteryLevel.ToString();
            command.Parameters.Add("score", NpgsqlTypes.NpgsqlDbType.Double).Value = result.Score;
            command.Parameters.Add("starttime", NpgsqlTypes.NpgsqlDbType.Timestamp).Value = result.StartTime;
            command.Parameters.Add("endtime", NpgsqlTypes.NpgsqlDbType.Timestamp).Value = result.EndTime;
            
            try
            {
                id = (int)command.ExecuteScalar();  // Execute the query, attempting to store the result object, and returning the UID of the created database entry for additional relational usage.
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
            }

            DatabaseManager.CloseConnection();
            #endregion

            // Once we submit an overal result object to the overal results table, we submit a task-specific result to the task-specific results table - linked to the overal result submission, and returning its generated uid.
            #region Store the lateral transfer specific result to lateral transfer results table.
            int lateraltransferresultid = 0;

            if (id > 0)
            {
                DatabaseManager.OpenConnection();

                NpgsqlCommand command2 = DatabaseManager.Connection.CreateCommand();
                command2.CommandType = System.Data.CommandType.Text;
                command2.CommandText = "INSERT INTO " + LATERAL_TRANSFER_RESULTS_TABLE_NAME + " ( resultid, totalsync, rhandsync, lhandsync, rwristsync, lwristsync, relbowsync, lelbowsync, rshouldersync, lshouldersync, manikinsync, handplacementscore, verbalcount, verbalcounttime, verbalcountscore, pullstarttime, pullendtime, calibrationimageid, handplacementimageid, pullstartimageid, pullendimageid, overallfeedbackids, pullfeedbackids, handplacementfeedbackids) VALUES (:resultid, :totalsync, :rhandsync, :lhandsync, :rwristsync, :lwristsync, :relbowsync, :lelbowsync, :rshouldersync, :lshouldersync, :manikinsync, :handplacementscore, :verbalcount, :verbalcounttime, :verbalcountscore, :pullstarttime, :pullendtime, :calibrationimageid, :handplacementimageid, :pullstartimageid, :pullendimageid, :overallfeedbackids, :pullfeedbackids, :handplacementfeedbackids) RETURNING uid;";

                command2.Parameters.Add("resultid", NpgsqlTypes.NpgsqlDbType.Integer).Value = id;
                command2.Parameters.Add("totalsync", NpgsqlTypes.NpgsqlDbType.Double).Value = result.TotalSync;
                command2.Parameters.Add("rhandsync", NpgsqlTypes.NpgsqlDbType.Double).Value = result.RHandSync;
                command2.Parameters.Add("lhandsync", NpgsqlTypes.NpgsqlDbType.Double).Value = result.LHandSync;
                command2.Parameters.Add("rwristsync", NpgsqlTypes.NpgsqlDbType.Double).Value = result.RWristSync;
                command2.Parameters.Add("lwristsync", NpgsqlTypes.NpgsqlDbType.Double).Value = result.LWristSync;
                command2.Parameters.Add("relbowsync", NpgsqlTypes.NpgsqlDbType.Double).Value = result.RElbowSync;
                command2.Parameters.Add("lelbowsync", NpgsqlTypes.NpgsqlDbType.Double).Value = result.LElbowSync;
                command2.Parameters.Add("rshouldersync", NpgsqlTypes.NpgsqlDbType.Double).Value = result.RShoulderSync;
                command2.Parameters.Add("lshouldersync", NpgsqlTypes.NpgsqlDbType.Double).Value = result.LShoulderSync;
                command2.Parameters.Add("manikinsync", NpgsqlTypes.NpgsqlDbType.Double).Value = result.ManikinSync;
                command2.Parameters.Add("handplacementscore", NpgsqlTypes.NpgsqlDbType.Double).Value = result.HandPlacementScore;
                command2.Parameters.Add("verbalcount", NpgsqlTypes.NpgsqlDbType.Boolean).Value = result.VerbalCount;
                command2.Parameters.Add("verbalcounttime", NpgsqlTypes.NpgsqlDbType.Timestamp).Value = result.VerbalCountTime;
                command2.Parameters.Add("verbalcountscore", NpgsqlTypes.NpgsqlDbType.Double).Value = result.VerbalCountScore;
                command2.Parameters.Add("pullstarttime", NpgsqlTypes.NpgsqlDbType.Timestamp).Value = result.PullStartTime;
                command2.Parameters.Add("pullendtime", NpgsqlTypes.NpgsqlDbType.Timestamp).Value = result.PullEndTime;
                command2.Parameters.Add("calibrationimageid", NpgsqlTypes.NpgsqlDbType.Integer).Value = result.CalibrationImageID;
                command2.Parameters.Add("handplacementimageid", NpgsqlTypes.NpgsqlDbType.Integer).Value = result.HandPlacementImageID;
                command2.Parameters.Add("pullstartimageid", NpgsqlTypes.NpgsqlDbType.Integer).Value = result.PullStartImageID;
                command2.Parameters.Add("pullendimageid", NpgsqlTypes.NpgsqlDbType.Integer).Value = result.PullEndImageID;

                command2.Parameters.Add("overallfeedbackids", NpgsqlTypes.NpgsqlDbType.Integer | NpgsqlTypes.NpgsqlDbType.Array).Value = result.OverallFeedbackIDs.ToArray();
                command2.Parameters.Add("pullfeedbackids", NpgsqlTypes.NpgsqlDbType.Integer | NpgsqlTypes.NpgsqlDbType.Array).Value = result.PullFeedbackIDs.ToArray();
                command2.Parameters.Add("handplacementfeedbackids", NpgsqlTypes.NpgsqlDbType.Integer | NpgsqlTypes.NpgsqlDbType.Array).Value = result.HandPlacementFeedbackIDs.ToArray();

                try
                {
                    lateraltransferresultid = (int)command2.ExecuteScalar();  // Execute the query, attempting to store the result object, and returning the UID of the created database entry for additional relational usage.
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.ToString());
                }

                DatabaseManager.CloseConnection();

            }
            else
            {
                // No overall result id was returned - running the next query would be a waste since we already know it won't point to any overall result.
                MessageBox.Show("Error submitting result to the database.");
            }
            #endregion

            // Return the uid of the submitted overal result for relational purposes.
            return id;
        }

        /// <summary>
        /// Retrieves a result from a resultid.
        /// 
        /// If retrieveImages is set to true, the result's images will also be pulled and stored in the result object.
        /// </summary>
        /// <param name="resultid"></param>
        /// <param name="retrieveImages"></param>
        /// <returns></returns>
        public static LateralTransferResult RetrieveResult(int resultid, bool retrieveImages)
        {
            if (resultid <= 0)
                return null;

            LateralTransferResult result = new LateralTransferResult();

            #region Retrieve the basic result information.
            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "SELECT studentid, assignmentid, tasktype, taskmode, masterylevel, score, starttime, endtime, submissiontime FROM " + OVERALL_RESULTS_TABLE_NAME + " WHERE uid = :uid";

            command.Parameters.Add("uid", NpgsqlTypes.NpgsqlDbType.Integer).Value = resultid;

            NpgsqlDataReader reader = command.ExecuteReader();

            int studentid = 0;
            int? assignmentid = 0;
            Task tasktype = Task.None;
            TaskMode taskmode = TaskMode.Practice;
            MasteryLevel masterylevel = MasteryLevel.Novice;
            double score = 0;
            DateTime starttime = DateTime.MinValue;
            DateTime endtime = DateTime.MinValue;
            DateTime submissiontime = DateTime.MinValue;

            while (reader.Read())
            {
                try
                {
                    studentid = reader.GetInt32(reader.GetOrdinal("studentid"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    assignmentid = reader.IsDBNull(reader.GetOrdinal("assignmentid")) ? null : (int?)reader.GetInt32(reader.GetOrdinal("assignmentid"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    tasktype = (Task)Enum.Parse(typeof(Task), reader.GetString(reader.GetOrdinal("tasktype")));
                }
                catch (Exception e)
                {
                }
                try
                {
                    taskmode = (TaskMode)Enum.Parse(typeof(TaskMode), reader.GetString(reader.GetOrdinal("taskmode")));
                }
                catch (Exception e)
                {
                }
                try
                {
                    masterylevel = (MasteryLevel)Enum.Parse(typeof(MasteryLevel), reader.GetString(reader.GetOrdinal("masterylevel")));
                }
                catch (Exception e)
                {
                }
                try
                {
                    score = reader.GetDouble(reader.GetOrdinal("score"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    starttime = reader.GetTimeStamp(reader.GetOrdinal("starttime"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    endtime = reader.GetTimeStamp(reader.GetOrdinal("endtime"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    submissiontime = reader.GetTimeStamp(reader.GetOrdinal("submissiontime"));
                }
                catch (Exception e)
                {
                }
            }

            reader.Dispose();

            DatabaseManager.CloseConnection();

            result.UID = resultid;
            result.StudentID = studentid;
            result.AssignmentID = assignmentid;
            result.TaskType = tasktype;
            result.TaskMode = taskmode;
            result.MasteryLevel = masterylevel;
            result.Score = score;
            result.StartTime = starttime;
            result.EndTime = endtime;
            result.SubmissionTime = submissiontime;
            #endregion

            #region Retrieve the lateral transfer specific content.
            DatabaseManager.OpenConnection();

            NpgsqlCommand command2 = DatabaseManager.Connection.CreateCommand();
            command2.CommandType = System.Data.CommandType.Text;
            command2.CommandText = "SELECT totalsync, rhandsync, lhandsync, rwristsync, lwristsync, relbowsync, lelbowsync, rshouldersync, lshouldersync, manikinsync, handplacementscore, verbalcount, verbalcounttime, verbalcountscore, pullstarttime, pullendtime, calibrationimageid, handplacementimageid, pullstartimageid, pullendimageid, overallfeedbackids, pullfeedbackids, handplacementfeedbackids FROM " + LATERAL_TRANSFER_RESULTS_TABLE_NAME + " WHERE resultid=:resultid";
            
            command2.Parameters.Add("resultid", NpgsqlTypes.NpgsqlDbType.Integer).Value = resultid;

            NpgsqlDataReader reader2 = command2.ExecuteReader();

            double totalsync = 0;
            double rhandsync = 0;
            double lhandsync = 0;
            double rwristsync = 0;
            double lwristsync = 0;
            double relbowsync = 0;
            double lelbowsync = 0;
            double rshouldersync = 0;
            double lshouldersync = 0;
            double manikinsync = 0;
            double handplacementscore = 0;
            bool verbalcount = false;
            double verbalcountscore = 0;
            DateTime verbalcounttime = DateTime.MinValue;
            DateTime pullstarttime = DateTime.MinValue;
            DateTime pullendtime = DateTime.MaxValue;
            int calibrationimageid = 0;
            int handplacementimageid = 0;
            int pullstartimageid = 0;
            int pullendimageid = 0;
            int[] overallfeedbackids = null;
            int [] pullfeedbackids = null;
            int[] handplacementfeedbackids = null;

            while (reader2.Read())
            {
                try
                {
                    totalsync = reader2.GetDouble(reader2.GetOrdinal("totalsync"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    rhandsync = reader2.GetDouble(reader2.GetOrdinal("rhandsync"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    lhandsync = reader2.GetDouble(reader2.GetOrdinal("lhandsync"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    rwristsync = reader2.GetDouble(reader2.GetOrdinal("rwristsync"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    lwristsync = reader2.GetDouble(reader2.GetOrdinal("lwristsync"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    relbowsync = reader2.GetDouble(reader2.GetOrdinal("relbowsync"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    lelbowsync = reader2.GetDouble(reader2.GetOrdinal("lelbowsync"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    rshouldersync = reader2.GetDouble(reader2.GetOrdinal("rshouldersync"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    lshouldersync = reader2.GetDouble(reader2.GetOrdinal("lshouldersync"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    manikinsync = reader2.GetDouble(reader2.GetOrdinal("manikinsync"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    handplacementscore = reader2.GetDouble(reader2.GetOrdinal("handplacementscore"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    verbalcount = reader2.GetBoolean(reader2.GetOrdinal("verbalcount"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    verbalcounttime = reader2.GetTimeStamp(reader2.GetOrdinal("verbalcounttime"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    verbalcountscore = reader2.GetDouble(reader2.GetOrdinal("verbalcountscore"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    pullstarttime = reader2.GetTimeStamp(reader2.GetOrdinal("pullstarttime"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    pullendtime = reader2.GetTimeStamp(reader2.GetOrdinal("pullendtime"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    calibrationimageid = reader2.GetInt32(reader2.GetOrdinal("calibrationimageid"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    handplacementimageid = reader2.GetInt32(reader2.GetOrdinal("handplacementimageid"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    pullstartimageid = reader2.GetInt32(reader2.GetOrdinal("pullstartimageid"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    pullendimageid = reader2.GetInt32(reader2.GetOrdinal("pullendimageid"));
                }
                catch (Exception e)
                {
                }
                try
                {
                    overallfeedbackids = reader2.GetValue(reader2.GetOrdinal("overallfeedbackids")) as int[];
                }
                catch (Exception e)
                {
                }
                try
                {
                    pullfeedbackids = reader2.GetValue(reader2.GetOrdinal("pullfeedbackids")) as int[];
                }
                catch (Exception e)
                {
                }
                try
                {
                    handplacementfeedbackids = reader2.GetValue(reader2.GetOrdinal("handplacementfeedbackids")) as int[];
                }
                catch (Exception e)
                {
                }
            }

            reader2.Dispose();

            DatabaseManager.CloseConnection();

            result.TotalSync = totalsync;
            result.RHandSync = rhandsync;
            result.LHandSync = lhandsync;
            result.RWristSync = rwristsync;
            result.LWristSync = lwristsync;
            result.RElbowSync = relbowsync;
            result.LElbowSync = lelbowsync;
            result.RShoulderSync = rshouldersync;
            result.LShoulderSync = lshouldersync;
            result.ManikinSync = manikinsync;
            result.HandPlacementScore = handplacementscore;
            result.VerbalCount = verbalcount;
            result.VerbalCountTime = verbalcounttime;
            result.VerbalCountScore = verbalcountscore;
            result.PullStartTime = pullstarttime;
            result.PullEndTime = pullendtime;
            result.CalibrationImageID = calibrationimageid;
            result.HandPlacementImageID = handplacementimageid;
            result.PullStartImageID = pullstartimageid;
            result.PullEndImageID = pullendimageid;

            if (overallfeedbackids != null)
            {
                List<int> asList = new List<int>();

                for( int i = 0; i < overallfeedbackids.Length; i++ )
                {
                    asList.Add(overallfeedbackids[i]);
                }

                result.OverallFeedbackIDs = asList;
            }

            if (pullfeedbackids != null)
            {
                List<int> asList = new List<int>();

                for (int i = 0; i < pullfeedbackids.Length; i++)
                {
                    asList.Add(pullfeedbackids[i]);
                }

                result.PullFeedbackIDs = asList;
            }

            if (handplacementfeedbackids != null)
            {
                List<int> asList = new List<int>();

                for (int i = 0; i < handplacementfeedbackids.Length; i++)
                {
                    asList.Add(handplacementfeedbackids[i]);
                }

                result.HandPlacementFeedbackIDs = asList;
            }

            #region Retrieve the Images if retrieveImages paramater is true.
            if (retrieveImages)
            {
                if (result.CalibrationImageID.HasValue && result.CalibrationImageID > 0)
                {
                    result.CalibrationImage = KinectImage.RetreiveImage((int)result.CalibrationImageID, IMAGES_TABLE_NAME);
                }

                if (result.HandPlacementImageID.HasValue && result.HandPlacementImageID > 0)
                {
                    result.HandPlacementImage = KinectImage.RetreiveImage((int)result.HandPlacementImageID, IMAGES_TABLE_NAME);
                }

                if ( result.PullStartImageID.HasValue && result.PullStartImageID > 0)
                {
                    result.PullStartImage = KinectImage.RetreiveImage((int)result.PullStartImageID, IMAGES_TABLE_NAME);
                }

                if ( result.PullEndImageID.HasValue && result.PullEndImageID > 0)
                {
                    result.PullEndImage = KinectImage.RetreiveImage((int)result.PullEndImageID, IMAGES_TABLE_NAME);
                }
            }
            #endregion

            #endregion

            return result;
        }

        public static bool RetrieveImages(ref LateralTransferResult Result)
        {

            // Fail if UID is not set, or is an invalid value.
            if (Result.UID <= 0)
                return false;

            bool endState = true;

            // Build list of image id's to get
            List<int> imageIDs = new List<int>();

            if (Result.CalibrationImage == null && Result.CalibrationImageID.HasValue)
                imageIDs.Add(Result.CalibrationImageID.Value);
            if (Result.HandPlacementImage == null && Result.HandPlacementImageID.HasValue)
                imageIDs.Add(Result.HandPlacementImageID.Value);
            if (Result.PullStartImage == null && Result.PullStartImageID.HasValue)
                imageIDs.Add(Result.PullStartImageID.Value);
            if (Result.PullEndImage == null && Result.PullEndImageID.HasValue)
                imageIDs.Add(Result.PullEndImageID.Value);

            Dictionary<int, KinectImage> kinectImages = KinectImage.RetreiveImages(imageIDs.ToArray(), IMAGES_TABLE_NAME);

            try
            {
                if (Result.CalibrationImage == null && Result.CalibrationImageID.HasValue)
                    Result.CalibrationImage = kinectImages[Result.CalibrationImageID.Value];
            }
            catch (Exception exc) { System.Diagnostics.Debug.Print("LateralTransferResult RetrieveImages ERROR: {0}", exc.Message); endState = false; }
            try
            {
                if (Result.HandPlacementImage == null && Result.HandPlacementImageID.HasValue)
                    Result.HandPlacementImage = kinectImages[Result.HandPlacementImageID.Value];
            }
            catch (Exception exc) { System.Diagnostics.Debug.Print("LateralTransferResult RetrieveImages ERROR: {0}", exc.Message); endState = false; }
            try
            {
                if (Result.PullStartImage == null && Result.PullStartImageID.HasValue)
                    Result.PullStartImage = kinectImages[Result.PullStartImageID.Value];
            }
            catch (Exception exc) { System.Diagnostics.Debug.Print("LateralTransferResult RetrieveImages ERROR: {0}", exc.Message); endState = false; }
            try
            {
                if (Result.PullEndImage == null && Result.PullEndImageID.HasValue)
                    Result.PullEndImage = kinectImages[Result.PullEndImageID.Value];
            }
            catch (Exception exc) { System.Diagnostics.Debug.Print("LateralTransferResult RetrieveImages ERROR: {0}", exc.Message); endState = false; }

            return endState;
        }

        #endregion
    }
}
