﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;
using Microsoft.Kinect;
using AIMS.Assets.Lessons2;
using System.Windows.Media.Media3D;
using System.ComponentModel;


namespace AIMS.Tasks.LateralTransfer
{
    /// <summary>
    /// Prepare for Lateral Transfer Stage. Check that both Users are in the correct position before performing transfer.
    /// </summary>
    class LateralTransferTransferStage : Stage
    {
        private LateralTransfer lesson;
        public LateralTransfer Lesson { get { return lesson; } set { lesson = value; } }

        private bool usersPulledInSync = false;
        private bool manikinMovedInSync = false;

        private Watcher manikinPullEndedWatcher;
        
        private int haltcount = 0;
        private DateTime haltStartTime = DateTime.MaxValue;

        public LateralTransferTransferStage(LateralTransfer lesson)
        {
            this.lesson = lesson;
            this.StageNumber = 1;
            this.Name = "Transfer The Patient";
            this.Instruction = "Perform the transfer by pulling on the sheet in sync with your partner. " +
                               "Ensure to pull evenly with both arms, keeping your back straight and vertically aligned with your head.";
            this.StageScore = new StageScore();
            this.ImagePath = "/Tasks/Lateral Transfer/Graphics/LateralTransfer003.png";
            this.Active = false;
            this.Complete = false;
            this.ScoreWeight = 0.20;
            this.Zones = new List<Zone>();
            this.IsSetup = false;
            this.TargetTime = TimeSpan.FromSeconds(20.0);
            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.Stopwatch = new System.Diagnostics.Stopwatch();
            this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.TimeOutTime = TimeSpan.FromSeconds(30.0);
            this.CanTimeOut = false;

            this.CurrentStageWatchers = new List<BaseWatcher>();
            this.PeripheralStageWatchers = new List<BaseWatcher>();
            this.OutlyingStageWatchers = new List<BaseWatcher>();

            this.CreateStageWatchers();
        }

        public override void SetupStage()
        {
            this.Lesson.LateralTransferPage.StageInfoTextBlock.Text = this.Name;
            this.Lesson.LateralTransferPage.InstructionTextBlock.Text = this.Instruction;

            // Set the image for the current stage.
            //this.Lesson.LateralTransferPage.CurrentStageImage.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri(this.ImagePath, UriKind.Relative));
            System.Diagnostics.Debug.WriteLine(String.Format("Set up stage {0}!", this.StageNumber));

            this.Lesson.LateralTransferPage.flow.Index = this.Lesson.CurrentStageIndex;  // change cover flow image.

            //this.manikinTorsoInSkeletalSpace = KinectManager.CoordinateMapper.MapDepthPointToCameraSpace(new DepthSpacePoint() { X = (int)this.Lesson.ManikinTorsoLocation.X, Y = (int)this.Lesson.ManikinTorsoLocation.Y, Depth = (int)this.Lesson.ManikinTorsoLocation.Z });
            //this.manikinLegInSkeletalSpace = KinectManager.CoordinateMapper.MapDepthPointToCameraSpace(new DepthSpacePoint() { X = (int)this.Lesson.ManikinLegLocation.X, Y = (int)this.Lesson.ManikinLegLocation.Y, Depth = (int)this.Lesson.ManikinLegLocation.Z });

            this.SetupZones();

            this.StartTime = DateTime.Now;

            this.IsSetup = true;

            this.Stopwatch.Start();

            this.TimeoutStopwatch.Start();
        }


        /// <summary>
        /// Sets default values for zones.
        /// </summary>
        private void SetupZones()
        {
            // Clear Zone lists
            this.Zones.Clear();
            this.Lesson.LateralTransferPage.zoneOverlay.ClearZones();
            
            // Add the Zones into lesson page zoneOverlay
            foreach (Zone zone in this.Zones)
                this.Lesson.LateralTransferPage.zoneOverlay.AddZone(zone);
        }


        private void CreateStageWatchers()
        {
            // Clear the list of watchers.
            this.CurrentStageWatchers.Clear();
            this.PeripheralStageWatchers.Clear();
            this.OutlyingStageWatchers.Clear();

            // Watcher checks for the manikin to end being pulled.
            this.manikinPullEndedWatcher = new Watcher();

            #region Check for manikin to stop being pulled.
            this.manikinPullEndedWatcher.CheckForActionDelegate = () =>
            {
                if (this.Lesson.PullEnded)    // Already ended.
                    return;

                if (!this.Lesson.PullStarted)
                    return; // Pull hasn't started yet.

                // This watcher checks for the manikin to end movement, and is intended to identify the DateTime of pull end.
                // Pull end DateTime will be used as the upper clamp for the timerange when performing the comparison analysis of the skeleton paths between two users, 
                // as well as the color paths between the toro and leg of the manikin.
                // To accomplish identifying the end of the pull, we check for the manikin to z-displacement to return back to 0, indicating a rest of motion.
                CameraSpacePoint skelTorso = KinectManager.CoordinateMapper.MapDepthPointToCameraSpace(new DepthSpacePoint() { X = (int)this.Lesson.ManikinTorsoLocation.X, Y = (int)this.Lesson.ManikinTorsoLocation.Y }, (ushort)this.Lesson.ManikinTorsoLocation.Z);
                //coordinateMapper.MapDepthFrameToCameraSpace(depthFrameData, cameraPoints);
                //KinectManager.CoordinateMapper.MapDepthPointsToCameraSpace();

                double torsoDisplacementTotal = 0;
                double torsoDisplacement = 0;

                if (skelTorso != null && skelTorso.Z != 0)
                {
                    torsoDisplacementTotal = this.Lesson.ManikinTorsoStartPoint.Z - skelTorso.Z;
                }

                torsoDisplacement = this.Lesson.PrevTorsoLoc.Z - this.Lesson.ManikinTorsoLocation.Z;

                CameraSpacePoint skelLeg = KinectManager.CoordinateMapper.MapDepthPointToCameraSpace(new DepthSpacePoint() { X = (int)this.Lesson.ManikinLegLocation.X, Y = (int)this.Lesson.ManikinLegLocation.Y }, (ushort)this.Lesson.ManikinLegLocation.Z);

                double legDisplacementTotal = 0;
                double legDisplacement = 0;

                if (skelLeg != null && skelLeg.Z != 0)
                {
                    legDisplacementTotal = this.Lesson.ManikinLegStartPoint.Z - skelLeg.Z;
                }

                legDisplacement = this.Lesson.PrevLegLoc.Z - this.Lesson.ManikinLegLocation.Z;

                //System.Diagnostics.Debug.WriteLine(String.Format("TOTAL: TorsoDisplacement: {0}, LegDisplacement: {1};", torsoDisplacementTotal, legDisplacementTotal));
                System.Diagnostics.Debug.WriteLine(String.Format("DELTA: TorsoDisplacement: {0}, LegDisplacement: {1};", torsoDisplacement, legDisplacement));

                if (torsoDisplacement >= 0 && legDisplacement >= 0)
                {
                    this.manikinPullEndedWatcher.IsTriggered = true;
                }
                else
                {
                    this.manikinPullEndedWatcher.IsTriggered = false;
                }
            };
            #endregion

            this.CurrentStageWatchers.Add(this.manikinPullEndedWatcher);

        
        } // End CreateStageWatchers()

        public override void UpdateStage()
        {
            if (!this.IsSetup && this.Lesson.CurrentStageIndex == this.StageNumber)
                this.SetupStage();

            // Check the Stage Timeout
            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
                this.TimeoutStage();

            bool pullended = false;

            if (this.manikinPullEndedWatcher.IsTriggered)
            {
                if (!this.Lesson.PullEnded)
                {
                    if (this.haltcount == 0)
                    {
                        this.haltStartTime = DateTime.Now;
                    }

                    haltcount++;

                    System.Diagnostics.Debug.WriteLine(String.Format("HaltCount: {0} - {1}", haltcount, haltStartTime));

                    if (this.haltcount > 3)    // 3 = 1/5s of sequential non-movement.
                    {
                        this.Lesson.PullEnded = true;
                        this.Lesson.SyncPullEndTime = this.haltStartTime;

                        this.Lesson.DetectableActions.DetectAction("Pull Ended", "End Pulling", "Manikin Torso", true);
                        this.Lesson.DetectableActions.DetectAction("Pull Ended", "End Pulling", "Manikin Leg", true);

                        System.Diagnostics.Debug.WriteLine(String.Format("{0}:: Pull Ended.", this.Lesson.SyncPullEndTime));
                        pullended = true;
                    }
                }
            }
            else
            {
                this.haltcount = 0; // reset the count since the manikin is still moving.
            }

            if (pullended)
            {
                this.Lesson.SkeletonPathComparison.StopRecording(); // stop the recording.
                this.Lesson.SkeletonPathComparison.PerformAnalysis(this.Lesson.SyncPullStartTime, this.Lesson.SyncPullEndTime, 25, true);   // perform the analysis.

                // Report analysis to debug output:
                System.Diagnostics.Debug.WriteLine( String.Format("Hand Sync:: R: {0}, L: {1}, Avg: {2};", this.Lesson.SkeletonPathComparison.HandRightGoodSync, this.Lesson.SkeletonPathComparison.HandLeftGoodSync, (this.Lesson.SkeletonPathComparison.HandRightGoodSync + this.Lesson.SkeletonPathComparison.HandLeftGoodSync) * 0.5));
                System.Diagnostics.Debug.WriteLine(String.Format("Wrist Sync:: R: {0}, L: {1}, Avg: {2};", this.Lesson.SkeletonPathComparison.WristRightGoodSync, this.Lesson.SkeletonPathComparison.WristLeftGoodSync, (this.Lesson.SkeletonPathComparison.WristRightGoodSync + this.Lesson.SkeletonPathComparison.WristLeftGoodSync) * 0.5));
                System.Diagnostics.Debug.WriteLine(String.Format("Elbow Sync:: R: {0}, L: {1}, Avg: {2};", this.Lesson.SkeletonPathComparison.ElbowRightGoodSync, this.Lesson.SkeletonPathComparison.ElbowLeftGoodSync, (this.Lesson.SkeletonPathComparison.ElbowRightGoodSync + this.Lesson.SkeletonPathComparison.ElbowLeftGoodSync) * 0.5));
                System.Diagnostics.Debug.WriteLine(String.Format("Shoulder Sync:: R: {0}, L: {1}, Avg: {2};", this.Lesson.SkeletonPathComparison.ShoulderRightGoodSync, this.Lesson.SkeletonPathComparison.ShoulderLeftGoodSync, (this.Lesson.SkeletonPathComparison.ShoulderRightGoodSync + this.Lesson.SkeletonPathComparison.ShoulderLeftGoodSync) * 0.5));

                double totalSync = (this.Lesson.SkeletonPathComparison.HandRightGoodSync + this.Lesson.SkeletonPathComparison.HandLeftGoodSync + this.Lesson.SkeletonPathComparison.WristRightGoodSync + this.Lesson.SkeletonPathComparison.WristLeftGoodSync + this.Lesson.SkeletonPathComparison.ElbowRightGoodSync + this.Lesson.SkeletonPathComparison.ElbowLeftGoodSync + this.Lesson.SkeletonPathComparison.ShoulderRightGoodSync + this.Lesson.SkeletonPathComparison.ShoulderLeftGoodSync) / 8;

                System.Diagnostics.Debug.WriteLine(String.Format("Total Sync: {0};", totalSync));

                this.Complete = true;
                this.EndStage();
            }

        } //End UpdateStage()


        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }


        public override void EndStage( bool playSound = true )
        {
            this.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            // Get the current time to signify the time the stage ended.
            this.EndTime = DateTime.Now;

            // Create a screenshot for the current stage.
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                LateralTransferResult result = (LateralTransferResult)args.Argument;

                KinectImage image = KinectImage.TakeKinectSnapshot(true, true);

                result.PullEndImage = image;
            };

            worker.RunWorkerAsync(this.Lesson.Result);

            AssessPathComparison();

            this.Lesson.CurrentStageIndex++;

            if( playSound ) Lesson.PlayNextStageSound();
        }


        public void AssessPathComparison()
        {
            if (this.Lesson.SkeletonPathComparison == null)
                return;

            double rhandsync = this.Lesson.SkeletonPathComparison.HandRightGoodSync;
            double lhandsync = this.Lesson.SkeletonPathComparison.HandLeftGoodSync;
            double rwristsync = this.Lesson.SkeletonPathComparison.WristRightGoodSync;
            double lwristsync = this.Lesson.SkeletonPathComparison.WristLeftGoodSync;
            double relbowsync = this.Lesson.SkeletonPathComparison.ElbowRightGoodSync;
            double lelbowsync = this.Lesson.SkeletonPathComparison.ElbowLeftGoodSync;
            double rshouldersync = this.Lesson.SkeletonPathComparison.ShoulderRightGoodSync;
            double lshouldersync = this.Lesson.SkeletonPathComparison.ShoulderLeftGoodSync;

            double totalsync = (rhandsync + lhandsync + rwristsync + lwristsync + relbowsync + lelbowsync + rshouldersync + lshouldersync ) / 8;

            this.Lesson.Result.RHandSync = rhandsync;
            this.Lesson.Result.LHandSync = lhandsync;
            this.Lesson.Result.RWristSync = rwristsync;
            this.Lesson.Result.LWristSync = lwristsync;
            this.Lesson.Result.RElbowSync = relbowsync;
            this.Lesson.Result.LElbowSync = lelbowsync;
            this.Lesson.Result.RShoulderSync = rshouldersync;
            this.Lesson.Result.LShoulderSync = lshouldersync;

            this.Lesson.Result.TotalSync = totalsync;
        }

        public override void PauseStage()
        {
            this.Lesson.Paused = true;
            this.Lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.Lesson.Paused = false;
            this.Lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void ResetStage()
        {

        }

        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            this.TimedOut = true;
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            this.RequestStageVideo();       // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
        }


        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.Lesson.LateralTransferPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Lateral Transfer/Videos/Stage 001.wmv", UriKind.Relative));

            this.Lesson.LateralTransferPage.VideoPlayer.startVideo(this, new System.Windows.RoutedEventArgs());

            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }


        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }


        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        public override void WipeClean()
        {
            usersPulledInSync = false;
            manikinMovedInSync = false;

            manikinPullEndedWatcher.IsTriggered = false;
            manikinPullEndedWatcher.HasTriggered = false;
        
            haltcount = 0;
            haltStartTime = DateTime.MaxValue;

            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.IsSetup = false;

            if (this.StageScore != null)
            {
                this.StageScore.WipeClean();
                this.StageScore = null;
                this.StageScore = new StageScore();
            }
            else
            {
                this.StageScore = new StageScore();
            }

            this.Active = false;
            this.Complete = false;

            if (this.Zones != null)
            {
                this.Zones.Clear();
            }
            else
            {
                this.Zones = new List<Zone>();
            }

            if (this.Stopwatch != null)
            {
                this.Stopwatch.Reset();
            }
            else
            {
                this.Stopwatch = new System.Diagnostics.Stopwatch();
            }

            if (this.TimeoutStopwatch != null)
            {
                this.TimeoutStopwatch.Reset();
            }
            else
            {
                this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            }

            this.TimedOut = false;
        }

    } // End LateralTransferTransferStage Class

} // End namespace AIMS.Tasks.LateralTransfer
