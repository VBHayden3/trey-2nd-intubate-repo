﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;
using Microsoft.Kinect;
using System.ComponentModel;


namespace AIMS.Tasks.LateralTransfer
{

    /// <summary>
    /// A struct for storing data associated with a User's Skeleton Joint Locations
    /// </summary>
    public struct JointLocations
    {
        public CameraSpacePoint RightHandLoc;
        public CameraSpacePoint LeftHandLoc;
        public CameraSpacePoint RightShoulderLoc;
        public CameraSpacePoint LeftShoulderLoc;
        public CameraSpacePoint HeadLoc;
        public CameraSpacePoint SpineLoc;
    }

    /// <summary>
    /// Prepare for Lateral Transfer Stage. Check that both Users are in the correct position before performing transfer.
    /// </summary>
    class LateralTransferUsersPositionsStage : Stage
    {
        private LateralTransfer lesson;
        public LateralTransfer Lesson { get { return lesson; } set { lesson = value; } }

        // Current position of Primary and Secondary User Joint Locations in Skeleton Space
        private JointLocations primaryUserJointLocations;
        private JointLocations secondaryUserJointLocations;

        // Watchers
        // User Joint Locations
        private Watcher primaryUserLocationWatcher;
        private Watcher secondaryUserLocationWatcher;
        // User Hands in Position
        private Watcher primaryUserRightHandInPositionWatcher;
        private Watcher primaryUserLeftHandInPositionWatcher;
        private Watcher secondaryUserRightHandInPositionWatcher;
        private Watcher secondaryUserLeftHandInPositionWatcher;
        // Primary & Secondary User Hands in Horizontal Alignment
        private Watcher userHandsInAlignmentWatcher;
        // User Arms in Position
        private Watcher primaryUserArmsInPositionWatcher;
        private Watcher secondaryUserArmsInPositionWatcher;
        // User Skeletons (Head & Spine) in Vertical Alignment
        private Watcher primaryUserSkeletonInAlignmentWatcher;
        private Watcher secondaryUserSkeletonInAlignmentWatcher;
        
        // Zone to look for Primary and Secondary User Hands in the correct place above the Manikin
        private RecZone zoneHandsInAlignmentAboveManikin;
        // Required minimum Time (ms) to be in zone
        private const double zoneTime = 2000;

        // Max Delta in X-Axis for User Hand to Shoulder (Meters in Skeleton Space)
        private const float HandToShoulderXAxisDelta = 0.2f;
        // Max Delta in X-Axis for User Head to Spine (Meters in Skeleton Space)
        private const float HeadToSpineXAxisDelta = 0.2f;

        // User In Alignment/Position Timer for EndStage Detection
        private System.Diagnostics.Stopwatch userPositionStopwatch = new System.Diagnostics.Stopwatch();
        // Required minimum Time (ms) a user needs to be observed in alignment/position for EndStage Detection
        private const long userPositionTime = 250;

        private bool primaryUserRightHandInPosition = false;
        private bool primaryUserLeftHandInPosition = false;
        private bool primaryUserArmsInPosition = false;
        private bool primaryUserBodyInPosition = false;
        private bool secondaryUserRightHandInPosition = false;
        private bool secondaryUserLeftHandInPosition = false;
        private bool secondaryUserArmsInPosition = false;
        private bool secondaryUserBodyInPosition = false;

        public LateralTransferUsersPositionsStage(LateralTransfer lesson)
        {
            this.lesson = lesson;
            this.StageNumber = 1;
            this.Name = "Prepare for Lateral Transfer";
            this.Instruction = "Both you and your transfer assistant lean forward and grasp the sheet with both hands. " +
                               "Ensure that you have a firm grip and that your hands are about shoulder width apart. " +
                               "Verify that the manikin assistant is holding the side of the manikin head with both hands.";
            this.StageScore = new StageScore();
            this.ImagePath = "/Tasks/Lateral Transfer/Graphics/LateralTransfer001.png";
            this.Active = false;
            this.Complete = false;
            this.ScoreWeight = 0.20;
            this.Zones = new List<Zone>();
            this.IsSetup = false;
            this.TargetTime = TimeSpan.FromSeconds(20.0);
            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.Stopwatch = new System.Diagnostics.Stopwatch();
            this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.TimeOutTime = TimeSpan.FromSeconds(30.0);
            this.CanTimeOut = false;

            this.CurrentStageWatchers = new List<BaseWatcher>();
            this.PeripheralStageWatchers = new List<BaseWatcher>();
            this.OutlyingStageWatchers = new List<BaseWatcher>();

            this.CreateStageWatchers();
        }


        public override void SetupStage()
        {
            this.Lesson.LateralTransferPage.StageInfoTextBlock.Text = this.Name;
            this.Lesson.LateralTransferPage.InstructionTextBlock.Text = this.Instruction;

            // Set the image for the current stage.
            //this.Lesson.LateralTransferPage.CurrentStageImage.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri(this.ImagePath, UriKind.Relative));

            System.Diagnostics.Debug.WriteLine(String.Format("Set up stage {0}!", this.StageNumber));

            this.Lesson.LateralTransferPage.flow.Index = this.Lesson.CurrentStageIndex;  // change cover flow image.

            this.SetupZones();

            this.StartTime = DateTime.Now;

            this.IsSetup = true;

            this.Stopwatch.Start();

            this.TimeoutStopwatch.Start();
        }


        /// <summary>
        /// Sets default values for zones.
        /// </summary>
        private void SetupZones()
        {
            // Clear Zone lists
            this.Zones.Clear();
            this.Lesson.LateralTransferPage.zoneOverlay.ClearZones();

            // Setup zones to look for manikin face and side locations around left hand.
            this.zoneHandsInAlignmentAboveManikin = new RecZone()
            {
                // Horizontal Zone above the Manikin
                // Used to check that the Primary and Secondary Users Hands are in position and horizontal alignment.
                Width = 540,    // pixels
                Height = 75,    // pixels
                Depth = 400,    // mm
                CenterX = 320,
                CenterY = this.Lesson.ManikinTorsoLocation.Y - 75,
                CenterZ = this.Lesson.ManikinTorsoLocation.Z,
                IsImproperZone = false,
                DoesTrackJoint = false,
                Active = true,
                Name = "zoneHandsInAlignmentAboveManikin"
            };
            this.Zones.Add(this.zoneHandsInAlignmentAboveManikin);

            // Add the Zones into lesson page zoneOverlay
            foreach (Zone zone in this.Zones)
                this.Lesson.LateralTransferPage.zoneOverlay.AddZone(zone);
        }


        private void CreateStageWatchers()
        {
            // Clear the list of watchers.
            this.CurrentStageWatchers.Clear();
            this.PeripheralStageWatchers.Clear();
            this.OutlyingStageWatchers.Clear();

            // Watcher - Get Current Location for Primary User Joint Locations
            this.primaryUserLocationWatcher = new Watcher();

            #region Get Current Location for Primary User Joint Locations
            this.primaryUserLocationWatcher.CheckForActionDelegate = () =>
            {
                // Get the joint positions out of the primary user profile.
                if (this.Lesson.MainUserSkeletonProfile != null)
                {
                    this.primaryUserJointLocations.RightHandLoc = this.Lesson.MainUserSkeletonProfile.JointProfiles.First(o => o.JointType == JointType.HandRight).CurrentPosition;
                    this.primaryUserJointLocations.LeftHandLoc = this.Lesson.MainUserSkeletonProfile.JointProfiles.First(o => o.JointType == JointType.HandLeft).CurrentPosition;
                    this.primaryUserJointLocations.RightShoulderLoc = this.Lesson.MainUserSkeletonProfile.JointProfiles.First(o => o.JointType == JointType.ShoulderRight).CurrentPosition;
                    this.primaryUserJointLocations.LeftShoulderLoc = this.Lesson.MainUserSkeletonProfile.JointProfiles.First(o => o.JointType == JointType.ShoulderLeft).CurrentPosition;
                    this.primaryUserJointLocations.HeadLoc = this.Lesson.MainUserSkeletonProfile.JointProfiles.First(o => o.JointType == JointType.Head).CurrentPosition;
                    this.primaryUserJointLocations.SpineLoc = this.Lesson.MainUserSkeletonProfile.JointProfiles.First(o => o.JointType == JointType.SpineMid).CurrentPosition;

                    this.primaryUserLocationWatcher.IsTriggered = true;

                    //System.Diagnostics.Debug.WriteLine(String.Format("PrimaryUser Head & Spine: ({0}, {1}, {2}, {3})", this.primaryUserJointLocations.HeadLoc.X, this.primaryUserJointLocations.HeadLoc.Y, this.primaryUserJointLocations.SpineLoc.X, this.primaryUserJointLocations.SpineLoc.Y));
                    //System.Diagnostics.Debug.WriteLine("LateralTransferUserPositionsStage::primaryUserLocationWatcher IsTriggered = " + this.primaryUserLocationWatcher.IsTriggered);
                }
                else
                    this.primaryUserLocationWatcher.IsTriggered = false;
            };
            #endregion

            this.CurrentStageWatchers.Add(this.primaryUserLocationWatcher);


            // Watcher - Get Current Location for Secondary User Joint Locations
            this.secondaryUserLocationWatcher = new Watcher();

            #region Get Current Location for Secondary User Joint Locations
            this.secondaryUserLocationWatcher.CheckForActionDelegate = () =>
            {
                // Get the hand joint positions out of the secondary user profile.
                if (this.Lesson.SecondaryUserSkeletonProfile != null)
                {
                    this.secondaryUserJointLocations.RightHandLoc = this.Lesson.SecondaryUserSkeletonProfile.JointProfiles.First(o => o.JointType == JointType.HandRight).CurrentPosition;
                    this.secondaryUserJointLocations.LeftHandLoc = this.Lesson.SecondaryUserSkeletonProfile.JointProfiles.First(o => o.JointType == JointType.HandLeft).CurrentPosition;
                    this.secondaryUserJointLocations.RightShoulderLoc = this.Lesson.SecondaryUserSkeletonProfile.JointProfiles.First(o => o.JointType == JointType.ShoulderRight).CurrentPosition;
                    this.secondaryUserJointLocations.LeftShoulderLoc = this.Lesson.SecondaryUserSkeletonProfile.JointProfiles.First(o => o.JointType == JointType.ShoulderLeft).CurrentPosition;
                    this.secondaryUserJointLocations.HeadLoc = this.Lesson.SecondaryUserSkeletonProfile.JointProfiles.First(o => o.JointType == JointType.Head).CurrentPosition;
                    this.secondaryUserJointLocations.SpineLoc = this.Lesson.SecondaryUserSkeletonProfile.JointProfiles.First(o => o.JointType == JointType.SpineMid).CurrentPosition;

                    this.secondaryUserLocationWatcher.IsTriggered = true;

                    //System.Diagnostics.Debug.WriteLine(String.Format("SecondaryUser Head & Spine: ({0}, {1}, {2}, {3})", this.secondaryUserJointLocations.HeadLoc.X, this.secondaryUserJointLocations.HeadLoc.Y, this.secondaryUserJointLocations.SpineLoc.X, this.secondaryUserJointLocations.SpineLoc.Y));
                    //System.Diagnostics.Debug.WriteLine("LateralTransferUserPositionsStage::secondaryUserLocationWatcher IsTriggered = " + this.secondaryUserLocationWatcher.IsTriggered);
                }
                else
                    this.secondaryUserLocationWatcher.IsTriggered = false;
            };
            #endregion

            this.CurrentStageWatchers.Add(this.secondaryUserLocationWatcher);


            // Watcher - Check Position of a Hand using Manikin Torso Zone
            this.primaryUserRightHandInPositionWatcher = new Watcher();

            #region Check Position of a Hand using Manikin Torso Zone
            this.primaryUserRightHandInPositionWatcher.CheckForActionDelegate = () =>
            {
                if (this.primaryUserLocationWatcher.IsTriggered)
                {
                    DepthSpacePoint handPos = KinectManager.CoordinateMapper.MapCameraPointToDepthSpace(this.primaryUserJointLocations.RightHandLoc);

                    //System.Diagnostics.Debug.WriteLine("LateralTransferUserPositionsStage - PrimaryUser RightHand Position: X = " + handPos.X + ", Y = " + handPos.Y);

                    // Check Zone for a hand in 2D position.
                    if (this.zoneHandsInAlignmentAboveManikin.IsPoint2DInside(new Point(handPos.X, handPos.Y)))
                    {
                        this.primaryUserRightHandInPositionWatcher.IsTriggered = true;

                        //System.Diagnostics.Debug.WriteLine("LateralTransferUserPositionsStage - PrimaryUser RightHand In Zone Position: X = " + handPos.X + ", Y = " + handPos.Y);
                    }
                    else
                        this.primaryUserRightHandInPositionWatcher.IsTriggered = false;
                }
                else
                    this.primaryUserRightHandInPositionWatcher.IsTriggered = false;
            };
            #endregion

            this.CurrentStageWatchers.Add(this.primaryUserRightHandInPositionWatcher);


            // Watcher - Check Position of a Hand using Manikin Torso Zone
            this.primaryUserLeftHandInPositionWatcher = new Watcher();

            #region Check Position of a Hand using Manikin Torso Zone
            this.primaryUserLeftHandInPositionWatcher.CheckForActionDelegate = () =>
            {
                if (this.primaryUserLocationWatcher.IsTriggered)
                {
                    DepthSpacePoint handPos = KinectManager.CoordinateMapper.MapCameraPointToDepthSpace(this.primaryUserJointLocations.LeftHandLoc);

                    //System.Diagnostics.Debug.WriteLine("LateralTransferUserPositionsStage - PrimaryUser LeftHand Position: X = " + handPos.X + ", Y = " + handPos.Y);

                    // Check Zone for a hand in 2D position.
                    if (this.zoneHandsInAlignmentAboveManikin.IsPoint2DInside(new Point(handPos.X, handPos.Y)))
                    {
                        this.primaryUserLeftHandInPositionWatcher.IsTriggered = true;

                        //System.Diagnostics.Debug.WriteLine("LateralTransferUserPositionsStage - PrimaryUser LeftHand In Zone Position: X = " + handPos.X + ", Y = " + handPos.Y);
                    }
                    else
                        this.primaryUserLeftHandInPositionWatcher.IsTriggered = false;
                }
                else
                    this.primaryUserLeftHandInPositionWatcher.IsTriggered = false;
            };
            #endregion

            this.CurrentStageWatchers.Add(this.primaryUserLeftHandInPositionWatcher);


            // Watcher - Check Position of a Hand using Manikin Torso Zone
            this.secondaryUserRightHandInPositionWatcher = new Watcher();

            #region Check Position of a Hand using Manikin Torso Zone
            this.secondaryUserRightHandInPositionWatcher.CheckForActionDelegate = () =>
            {
                if (this.secondaryUserLocationWatcher.IsTriggered)
                {
                    DepthSpacePoint handPos = KinectManager.CoordinateMapper.MapCameraPointToDepthSpace(this.secondaryUserJointLocations.RightHandLoc);

                    //System.Diagnostics.Debug.WriteLine("LateralTransferUserPositionsStage - SecondaryUser RightHand Position: X = " + handPos.X + ", Y = " + handPos.Y);

                    // Check Zone for a hand in 2D position.
                    if (this.zoneHandsInAlignmentAboveManikin.IsPoint2DInside(new Point(handPos.X, handPos.Y)))
                    {
                        this.secondaryUserRightHandInPositionWatcher.IsTriggered = true;

                        //System.Diagnostics.Debug.WriteLine("LateralTransferUserPositionsStage - SecondaryUser RightHand In Zone Position: X = " + handPos.X + ", Y = " + handPos.Y);
                    }
                    else
                        this.secondaryUserRightHandInPositionWatcher.IsTriggered = false;
                }
                else
                    this.secondaryUserRightHandInPositionWatcher.IsTriggered = false;
            };
            #endregion

            this.CurrentStageWatchers.Add(this.secondaryUserRightHandInPositionWatcher);


            // Watcher - Check Position of a Hand using Manikin Torso Zone
            this.secondaryUserLeftHandInPositionWatcher = new Watcher();

            #region Check Position of a Hand using Manikin Torso Zone
            this.secondaryUserLeftHandInPositionWatcher.CheckForActionDelegate = () =>
            {
                if (this.secondaryUserLocationWatcher.IsTriggered)
                {
                    DepthSpacePoint handPos = KinectManager.CoordinateMapper.MapCameraPointToDepthSpace(this.secondaryUserJointLocations.LeftHandLoc);

                    //System.Diagnostics.Debug.WriteLine("LateralTransferUserPositionsStage - SecondaryUser LeftHand Position: X = " + handPos.X + ", Y = " + handPos.Y);

                    // Check Zone for a hand in 2D position.
                    if (this.zoneHandsInAlignmentAboveManikin.IsPoint2DInside(new Point(handPos.X, handPos.Y)))
                    {
                        this.secondaryUserLeftHandInPositionWatcher.IsTriggered = true;

                        //System.Diagnostics.Debug.WriteLine("LateralTransferUserPositionsStage - SecondaryUser LeftHand In Zone Position: X = " + handPos.X + ", Y = " + handPos.Y);
                    }
                    else
                        this.secondaryUserLeftHandInPositionWatcher.IsTriggered = false;
                }
                else
                    this.secondaryUserLeftHandInPositionWatcher.IsTriggered = false;
            };
            #endregion

            this.CurrentStageWatchers.Add(this.secondaryUserLeftHandInPositionWatcher);


            // Watcher - Check for all Hands to be in Horizontal Alignment using the Manikin Torso Zone
            this.userHandsInAlignmentWatcher = new Watcher();

            #region Check for all Hands to be in Horizontal Alignment using the Manikin Torso Zone
            this.userHandsInAlignmentWatcher.CheckForActionDelegate = () =>
            {
                if (this.primaryUserRightHandInPositionWatcher.IsTriggered && this.primaryUserLeftHandInPositionWatcher.IsTriggered &&
                    this.secondaryUserRightHandInPositionWatcher.IsTriggered && this.secondaryUserLeftHandInPositionWatcher.IsTriggered)
                {
                    this.zoneHandsInAlignmentAboveManikin.IsWithinZone = true;

                    //System.Diagnostics.Debug.WriteLine("LateralTransferUserPositionsStage - zoneHandsInAlignmentAboveManikin Time = " + this.zoneHandsInAlignmentAboveManikin.TimeWithinZone.Milliseconds);

                    // Make sure all hands are in alignment for a period of time before triggering watcher.
                    if (this.zoneHandsInAlignmentAboveManikin.TimeWithinZone >= TimeSpan.FromMilliseconds(LateralTransferUsersPositionsStage.zoneTime))
                    {
                        this.userHandsInAlignmentWatcher.IsTriggered = true;

                        //System.Diagnostics.Debug.WriteLine("LateralTransferUserPositionsStage - zoneHandsInAlignmentAboveManikin TimeWithinZone = " + this.zoneHandsInAlignmentAboveManikin.TimeWithinZone.TotalMilliseconds + ", at Stage Time = " + this.Stopwatch.ElapsedMilliseconds);
                    }
                    else
                        this.userHandsInAlignmentWatcher.IsTriggered = false;
                }
                else
                {
                    this.zoneHandsInAlignmentAboveManikin.IsWithinZone = false;
                    this.userHandsInAlignmentWatcher.IsTriggered = false;
                }
            };
            #endregion

            this.CurrentStageWatchers.Add(this.userHandsInAlignmentWatcher);


            // Watcher - Check for Arms in Position
            this.primaryUserArmsInPositionWatcher = new Watcher();

            #region Check for Arms in Position
            this.primaryUserArmsInPositionWatcher.CheckForActionDelegate = () =>
            {
                if (this.userHandsInAlignmentWatcher.IsTriggered)
                {
                    float deltaRight = System.Math.Abs(this.primaryUserJointLocations.RightHandLoc.X - this.primaryUserJointLocations.RightShoulderLoc.X);
                    float deltaLeft = System.Math.Abs(this.primaryUserJointLocations.LeftHandLoc.X - this.primaryUserJointLocations.LeftShoulderLoc.X);

                    if (deltaRight <= LateralTransferUsersPositionsStage.HandToShoulderXAxisDelta && deltaLeft <= LateralTransferUsersPositionsStage.HandToShoulderXAxisDelta)
                    {
                        this.primaryUserArmsInPositionWatcher.IsTriggered = true;

                        //System.Diagnostics.Debug.WriteLine("LateralTransferUserPositionsStage - PrimaryUser Arms In Position, DeltaRight = " + deltaRight + ", DeltaLeft = " + deltaLeft);
                    }
                    else
                        this.primaryUserArmsInPositionWatcher.IsTriggered = false;
                }
                else
                    this.primaryUserArmsInPositionWatcher.IsTriggered = false;
            };
            #endregion

            this.CurrentStageWatchers.Add(this.primaryUserArmsInPositionWatcher);


            // Watcher - Check for Arms in Position
            this.secondaryUserArmsInPositionWatcher = new Watcher();

            #region Check for Arms in Position
            this.secondaryUserArmsInPositionWatcher.CheckForActionDelegate = () =>
            {
                if (this.userHandsInAlignmentWatcher.IsTriggered)
                {
                    float deltaRight = System.Math.Abs(this.secondaryUserJointLocations.RightHandLoc.X - this.secondaryUserJointLocations.RightShoulderLoc.X);
                    float deltaLeft = System.Math.Abs(this.secondaryUserJointLocations.LeftHandLoc.X - this.secondaryUserJointLocations.LeftShoulderLoc.X);

                    if (deltaRight <= LateralTransferUsersPositionsStage.HandToShoulderXAxisDelta && deltaLeft <= LateralTransferUsersPositionsStage.HandToShoulderXAxisDelta)
                    {
                        this.secondaryUserArmsInPositionWatcher.IsTriggered = true;

                        //System.Diagnostics.Debug.WriteLine("LateralTransferUserPositionsStage - SecondaryUser Arms In Position, DeltaRight = " + deltaRight + ", DeltaLeft = " + deltaLeft);
                    }
                    else
                        this.secondaryUserArmsInPositionWatcher.IsTriggered = false;
                }
                else
                    this.secondaryUserArmsInPositionWatcher.IsTriggered = false;
            };
            #endregion

            this.CurrentStageWatchers.Add(this.secondaryUserArmsInPositionWatcher);


            // Watcher - Check for Skeleton in Vertical Alignment
            this.primaryUserSkeletonInAlignmentWatcher = new Watcher();

            #region Check for Skeleton in Vertical Alignment
            this.primaryUserSkeletonInAlignmentWatcher.CheckForActionDelegate = () =>
            {
                if (this.primaryUserArmsInPositionWatcher.IsTriggered)
                {
                    bool yAxisDeltaSkeleton = false;
                    if ((this.primaryUserJointLocations.HeadLoc.Y - this.primaryUserJointLocations.SpineLoc.Y) > 0)
                        yAxisDeltaSkeleton = true;

                    float xAxisDeltaSkeleton = System.Math.Abs(this.primaryUserJointLocations.HeadLoc.X - this.primaryUserJointLocations.SpineLoc.X);

                    if (xAxisDeltaSkeleton <= LateralTransferUsersPositionsStage.HeadToSpineXAxisDelta && yAxisDeltaSkeleton)
                    {
                        this.primaryUserSkeletonInAlignmentWatcher.IsTriggered = true;

                        //System.Diagnostics.Debug.WriteLine("LateralTransferUserPositionsStage - PrimaryUser Skeleton In Alignment, xAxisDeltaSkeleton = " + xAxisDeltaSkeleton + ", yAxisDeltaSkeleton = " + yAxisDeltaSkeleton);
                    }
                    else
                        this.primaryUserSkeletonInAlignmentWatcher.IsTriggered = false;
                }
                else
                    this.primaryUserSkeletonInAlignmentWatcher.IsTriggered = false;
            };
            #endregion

            this.CurrentStageWatchers.Add(this.primaryUserSkeletonInAlignmentWatcher);


            // Watcher - Check for Skeleton in Vertical Alignment
            this.secondaryUserSkeletonInAlignmentWatcher = new Watcher();

            #region Check for Skeleton in Vertical Alignment
            this.secondaryUserSkeletonInAlignmentWatcher.CheckForActionDelegate = () =>
            {
                if (this.secondaryUserArmsInPositionWatcher.IsTriggered)
                {
                    bool yAxisDeltaSkeleton = false;
                    if ( (this.secondaryUserJointLocations.HeadLoc.Y - this.secondaryUserJointLocations.SpineLoc.Y) > 0)
                        yAxisDeltaSkeleton = true;

                    float xAxisDeltaSkeleton = System.Math.Abs(this.secondaryUserJointLocations.HeadLoc.X - this.secondaryUserJointLocations.SpineLoc.X);

                    if (xAxisDeltaSkeleton <= LateralTransferUsersPositionsStage.HeadToSpineXAxisDelta && yAxisDeltaSkeleton)
                    {
                        this.secondaryUserSkeletonInAlignmentWatcher.IsTriggered = true;

                        //System.Diagnostics.Debug.WriteLine("LateralTransferUserPositionsStage - SecondaryUser Skeleton In Alignment, xAxisDeltaSkeleton = " + xAxisDeltaSkeleton + ", yAxisDeltaSkeleton = " + yAxisDeltaSkeleton);
                    }
                    else
                        this.secondaryUserSkeletonInAlignmentWatcher.IsTriggered = false;
                }
                else
                    this.secondaryUserSkeletonInAlignmentWatcher.IsTriggered = false;
            };
            #endregion

            this.CurrentStageWatchers.Add(this.secondaryUserSkeletonInAlignmentWatcher);
        }

        public override void UpdateStage()
        {
            if (this.Complete)
                return;

            if (!this.IsSetup && this.Lesson.CurrentStageIndex == this.StageNumber)
                this.SetupStage();

            // Check the Stage Timeout
            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
                this.TimeoutStage();

            if( this.Lesson.ManikinLegStartPoint.X == 0 && this.Lesson.ManikinLegStartPoint.Y == 0)
                this.Lesson.ManikinLegStartPoint = KinectManager.CoordinateMapper.MapDepthPointToCameraSpace(new DepthSpacePoint() { X = (int)this.Lesson.ManikinLegLocation.X, Y = (int)this.Lesson.ManikinLegLocation.Y }, (ushort)this.Lesson.ManikinLegLocation.Z);
            if( this.Lesson.ManikinTorsoStartPoint.X == 0 && this.Lesson.ManikinTorsoStartPoint.Y == 0)
                this.Lesson.ManikinTorsoStartPoint = KinectManager.CoordinateMapper.MapDepthPointToCameraSpace(new DepthSpacePoint() { X = (int)this.Lesson.ManikinTorsoLocation.X, Y = (int)this.Lesson.ManikinTorsoLocation.Y }, (ushort)this.Lesson.ManikinTorsoLocation.Z);

            #region Stage Detections
            // Detectable Actions
            // Primary User
            if (this.primaryUserRightHandInPositionWatcher.IsTriggered)
            {
                if (!this.primaryUserRightHandInPosition)
                {
                    this.primaryUserRightHandInPosition = true;
                    this.Lesson.DetectableActions.DetectAction("Right Hand In Position", "In Position", "Primary User", true);
                }
            }
            else
            {
                if (this.primaryUserRightHandInPosition)
                {
                    this.primaryUserRightHandInPosition = false;
                    this.Lesson.DetectableActions.UndetectAction("Right Hand In Position", "In Position", "Primary User", false);
                }
            }

            if (this.primaryUserLeftHandInPositionWatcher.IsTriggered)
            {
                if (!this.primaryUserLeftHandInPosition)
                {
                    this.primaryUserLeftHandInPosition = true;
                    this.Lesson.DetectableActions.DetectAction("Left Hand In Position", "In Position", "Primary User", true);
                }
            }
            else
            {
                if (this.primaryUserLeftHandInPosition)
                {
                    this.primaryUserLeftHandInPosition = false;
                    this.Lesson.DetectableActions.UndetectAction("Left Hand In Position", "In Position", "Primary User", false);
                }
            }

            if (this.primaryUserArmsInPositionWatcher.IsTriggered)
            {
                if (!this.primaryUserArmsInPosition)
                {
                    this.primaryUserArmsInPosition = true;
                    this.Lesson.DetectableActions.DetectAction("Arms In Position", "In Position", "Primary User", true);
                }
            }
            else
            {
                if (this.primaryUserArmsInPosition)
                {
                    this.primaryUserArmsInPosition = false;
                    this.Lesson.DetectableActions.UndetectAction("Arms In Position", "In Position", "Primary User", false);
                }
            }

            if (this.primaryUserSkeletonInAlignmentWatcher.IsTriggered)
            {
                if (!this.primaryUserBodyInPosition)
                {
                    this.primaryUserBodyInPosition = true;
                    this.Lesson.DetectableActions.DetectAction("Body In Position", "In Position", "Primary User", true);
                }
            }
            else
            {
                if (this.primaryUserBodyInPosition)
                {
                    this.primaryUserBodyInPosition = false;
                    this.Lesson.DetectableActions.UndetectAction("Body In Position", "In Position", "Primary User", false);
                }
            }

            // Secondary User

            if (this.secondaryUserRightHandInPositionWatcher.IsTriggered)
            {
                if (!this.secondaryUserRightHandInPosition)
                {
                    this.secondaryUserRightHandInPosition = true;
                    this.Lesson.DetectableActions.DetectAction("Right Hand In Position", "In Position", "Secondary User", true);
                }
            }
            else
            {
                if (this.secondaryUserRightHandInPosition)
                {
                    this.secondaryUserRightHandInPosition = false;
                    this.Lesson.DetectableActions.UndetectAction("Right Hand In Position", "In Position", "Secondary User", false);
                }
            }

            if (this.secondaryUserLeftHandInPositionWatcher.IsTriggered)
            {
                if (!this.secondaryUserLeftHandInPosition)
                {
                    this.secondaryUserLeftHandInPosition = true;
                    this.Lesson.DetectableActions.DetectAction("Left Hand In Position", "In Position", "Secondary User", true);
                }
            }
            else
            {
                if (this.secondaryUserLeftHandInPosition)
                {
                    this.secondaryUserLeftHandInPosition = false;
                    this.Lesson.DetectableActions.UndetectAction("Left Hand In Position", "In Position", "Secondary User", false);
                }
            }

            if (this.secondaryUserArmsInPositionWatcher.IsTriggered)
            {
                if (!this.secondaryUserArmsInPosition)
                {
                    this.secondaryUserArmsInPosition = true;
                    this.Lesson.DetectableActions.DetectAction("Arms In Position", "In Position", "Secondary User", true);
                }
            }
            else
            {
                if (this.secondaryUserArmsInPosition)
                {
                    this.secondaryUserArmsInPosition = false;
                    this.Lesson.DetectableActions.UndetectAction("Arms In Position", "In Position", "Secondary User", false);
                }
            }

            if (this.secondaryUserSkeletonInAlignmentWatcher.IsTriggered)
            {
                if (!this.secondaryUserBodyInPosition)
                {
                    this.secondaryUserBodyInPosition = true;
                    this.Lesson.DetectableActions.DetectAction("Body In Position", "In Position", "Secondary User", true);
                }
            }
            else
            {
                if (this.secondaryUserBodyInPosition)
                {
                    this.secondaryUserBodyInPosition = false;
                    this.Lesson.DetectableActions.UndetectAction("Body In Position", "In Position", "Secondary User", false);
                }
            }

            #endregion

            // End Stage Detection
            if (this.primaryUserSkeletonInAlignmentWatcher.IsTriggered && this.secondaryUserSkeletonInAlignmentWatcher.IsTriggered)
            {
                if (this.userPositionStopwatch.IsRunning)
                {
                    if (this.userPositionStopwatch.ElapsedMilliseconds >= LateralTransferUsersPositionsStage.userPositionTime)
                    {
                        System.Diagnostics.Debug.WriteLine("LateralTransferUserPositionsStage - EndStage");

                        this.Complete = true;
                        this.EndStage();
                    }
                }
                else
                {
                    this.userPositionStopwatch.Start();
                }
            }
            else
            {
                if (this.userPositionStopwatch.IsRunning)
                    this.userPositionStopwatch.Reset();
            }

        }

        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }

        public override void EndStage( bool playSound = true )
        {
            Complete = true;

            this.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            // Get the current time to signify the time the stage ended.
            this.EndTime = DateTime.Now;

            // Create a screenshot for the current stage.
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                LateralTransferResult result = (LateralTransferResult)args.Argument;

                KinectImage image = KinectImage.TakeKinectSnapshot(true, true);

                result.HandPlacementImage = image;
            };

            worker.RunWorkerAsync(this.Lesson.Result);

            CalculateHandPlacementScore();

            this.Lesson.CurrentStageIndex++;

            if( playSound ) Lesson.PlayNextStageSound();
        }


        public void CalculateHandPlacementScore()
        {
            double score = 1.0;

            // Assign a value to the HandPlacementScore based upon what was found by the Kinect.
            #region Primary User: Arms
            if (!primaryUserArmsInPosition)
            {
                score -= 0.15;

                this.Lesson.Result.HandPlacementFeedbackIDs.Add(14);    // Your arms were out of position prior to the transfer.
            }
            #endregion

            #region Primary user: Body
            if (!primaryUserBodyInPosition)
            {
                score -= 0.15;

                this.Lesson.Result.HandPlacementFeedbackIDs.Add(16);    // Your body was out of position prior to the transfer.
            }
            #endregion

            #region Primary User: Hands
            if (!primaryUserLeftHandInPosition)
            {
                score -= 0.075;
            }

            if (!primaryUserRightHandInPosition)
            {
                score -= 0.075;
            }

            if (!primaryUserRightHandInPosition || !primaryUserLeftHandInPosition)
            {
                this.Lesson.Result.HandPlacementFeedbackIDs.Add(4); // Your hands were out of position prior to the transfer.
            }
            #endregion

            #region Secondary User: Arms
            if (!secondaryUserArmsInPosition)
            {
                score -= 0.15;
                this.Lesson.Result.HandPlacementFeedbackIDs.Add(15);    // Your partner's arms were out of position prior to the transfer.
            }
            #endregion

            #region Secondary User: Body
            if (!secondaryUserBodyInPosition)
            {
                score -= 0.15;
                this.Lesson.Result.HandPlacementFeedbackIDs.Add(17);    // You partner's body was out of position prior to the transfer.
            }
            #endregion

            #region Secondary User: Hands
            if (!secondaryUserLeftHandInPosition)
            {
                score -= 0.075;
            }

            if (!secondaryUserRightHandInPosition)
            {
                score -= 0.075;
            }

            if (!secondaryUserRightHandInPosition || !secondaryUserLeftHandInPosition)
            {
                this.Lesson.Result.HandPlacementFeedbackIDs.Add(5); // Your partner's hands out of position prior to the transfer.
            }
            #endregion

            if (score == 1.0)
            {
                this.Lesson.Result.HandPlacementFeedbackIDs.Add(1); // You were in proper position prior to the transfer.
                this.Lesson.Result.HandPlacementFeedbackIDs.Add(2); // Your partner was in proper position prior to the transfer.
            }

            this.Lesson.Result.HandPlacementScore = System.Math.Max(0, System.Math.Min(score, 1)); // clamp the score between 0 and 1, and then set it.
        }


        public override void PauseStage()
        {
            this.Lesson.Paused = true;
            this.Lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }


        public override void ResumeStage()
        {
            this.Lesson.Paused = false;
            this.Lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }


        public override void ResetStage()
        {

        }


        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            this.TimedOut = true;
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            this.RequestStageVideo();       // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
        }


        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.Lesson.LateralTransferPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Lateral Transfer/Videos/Stage 001.wmv", UriKind.Relative));

            this.Lesson.LateralTransferPage.VideoPlayer.startVideo(this, new System.Windows.RoutedEventArgs());

            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }


        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }


        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        public override void WipeClean()
        {
            // Current position of Primary and Secondary User Joint Locations in Skeleton Space
            primaryUserJointLocations = new JointLocations();
            secondaryUserJointLocations = new JointLocations();

            // User Joint Locations
            primaryUserLocationWatcher.IsTriggered = false;
            primaryUserLocationWatcher.HasTriggered = false;
            secondaryUserLocationWatcher.IsTriggered = false;
            secondaryUserLocationWatcher.HasTriggered = false;
            // User Hands in Position
            primaryUserRightHandInPositionWatcher.IsTriggered = false;
            primaryUserRightHandInPositionWatcher.HasTriggered = false;
            primaryUserLeftHandInPositionWatcher.IsTriggered = false;
            primaryUserLeftHandInPositionWatcher.HasTriggered = false;
            secondaryUserRightHandInPositionWatcher.IsTriggered = false;
            secondaryUserRightHandInPositionWatcher.HasTriggered = false;
            secondaryUserLeftHandInPositionWatcher.IsTriggered = false;
            secondaryUserLeftHandInPositionWatcher.HasTriggered = false;
            // Primary & Secondary User Hands in Horizontal Alignment
            userHandsInAlignmentWatcher.IsTriggered = false;
            userHandsInAlignmentWatcher.HasTriggered = false;
            // User Arms in Position
            primaryUserArmsInPositionWatcher.IsTriggered = false;
            primaryUserArmsInPositionWatcher.HasTriggered = false;
            secondaryUserArmsInPositionWatcher.IsTriggered = false;
            secondaryUserArmsInPositionWatcher.HasTriggered = false;
            // User Skeletons (Head & Spine) in Vertical Alignment
            primaryUserSkeletonInAlignmentWatcher.IsTriggered = false;
            primaryUserSkeletonInAlignmentWatcher.HasTriggered = false;
            secondaryUserSkeletonInAlignmentWatcher.IsTriggered = false;
            secondaryUserSkeletonInAlignmentWatcher.HasTriggered = false;
        
            // Zone to look for Primary and Secondary User Hands in the correct place above the Manikin
            zoneHandsInAlignmentAboveManikin = null;

            primaryUserRightHandInPosition = false;
            primaryUserLeftHandInPosition = false;
            primaryUserArmsInPosition = false;
            primaryUserBodyInPosition = false;
            secondaryUserRightHandInPosition = false;
            secondaryUserLeftHandInPosition = false;
            secondaryUserArmsInPosition = false;
            secondaryUserBodyInPosition = false;

            // Stage Reinitialization
            this.userPositionStopwatch.Reset();

            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.IsSetup = false;

            if (this.StageScore != null)
            {
                this.StageScore.WipeClean();
                this.StageScore = null;
                this.StageScore = new StageScore();
            }
            else
            {
                this.StageScore = new StageScore();
            }

            this.Active = false;
            this.Complete = false;

            if (this.Zones != null)
            {
                this.Zones.Clear();
            }
            else
            {
                this.Zones = new List<Zone>();
            }

            if (this.Stopwatch != null)
            {
                this.Stopwatch.Reset();
            }
            else
            {
                this.Stopwatch = new System.Diagnostics.Stopwatch();
            }

            if (this.TimeoutStopwatch != null)
            {
                this.TimeoutStopwatch.Reset();
            }
            else
            {
                this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            }

            this.TimedOut = false;
        }

    } // End LateralTransferUsersPositions Class

} // End namespace AIMS.Tasks.LateralTransfer
