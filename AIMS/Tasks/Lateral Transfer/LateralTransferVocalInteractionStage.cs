﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIMS.Tasks.LateralTransfer
{
    class LateralTransferVocalInteractionStage : Stage
    {
        private LateralTransfer lesson;
        public LateralTransfer Lesson { get { return lesson; } set { lesson = value; } }

        private bool countAndPullDetected = false;
        public bool CountAndPullDetected { get { return this.countAndPullDetected; } set { this.countAndPullDetected = value; this.CheckVoiceStates(); } }

        private DateTime timeLastHeardPullCount = DateTime.MinValue;
        public DateTime TimeLastHeardPullCount { get { return timeLastHeardPullCount; } set { timeLastHeardPullCount = value; } }

        public LateralTransferVocalInteractionStage(LateralTransfer lesson)
        {
            this.lesson = lesson;
            this.StageNumber = 2;
            this.Name = "1, 2, 3, Pull";
            this.Instruction = "Prepare to perform the transfer by counting to three and then saying \"Pull\". Tell AIMI \"One, Two, Three, Pull\".";
            this.StageScore = new StageScore();
            this.ImagePath = "/Tasks/Lateral Transfer/Graphics/LateralTransfer002.png";
            this.Active = false;
            this.Complete = false;
            this.ScoreWeight = 0.20;
            this.Zones = new List<Zone>();
            this.IsSetup = false;
            this.TargetTime = TimeSpan.FromSeconds(20.0);
            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.Stopwatch = new System.Diagnostics.Stopwatch();
            this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.TimeOutTime = TimeSpan.FromSeconds(30.0);
            this.CanTimeOut = false;

            this.CurrentStageWatchers = new List<BaseWatcher>();
            this.PeripheralStageWatchers = new List<BaseWatcher>();
            this.OutlyingStageWatchers = new List<BaseWatcher>();

            this.CreateStageWatchers();

            // Subscribe to SpeechCommander event to hear user speech.
            // @todo update
            KinectManager.KinectSpeechCommander.SpeechRecognized += new EventHandler<Speech.SpeechCommanderEventArgs>(KinectSpeechCommander_SpeechRecognized);

        }

        public override void SetupStage()
        {
            this.Lesson.LateralTransferPage.StageInfoTextBlock.Text = this.Name;
            this.Lesson.LateralTransferPage.InstructionTextBlock.Text = this.Instruction;

            // Set the image for the current stage.
            //this.Lesson.LateralTransferPage.CurrentStageImage.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri(this.ImagePath, UriKind.Relative));

            System.Diagnostics.Debug.WriteLine(String.Format("Set up stage {0}!", this.StageNumber));

            this.Lesson.LateralTransferPage.flow.Index = this.Lesson.CurrentStageIndex;  // change cover flow image.

            this.Zones.Clear(); // Clear the stage zone list.

            this.Lesson.LateralTransferPage.zoneOverlay.ClearZones();

            // Add the zones into the zoneOverlay
            foreach (Zone zone in this.Zones)
            {
                this.Lesson.LateralTransferPage.zoneOverlay.AddZone(zone);
            }

            this.StartTime = DateTime.Now;

            this.IsSetup = true;

            this.Stopwatch.Start();

            this.TimeoutStopwatch.Start();
        }
        
        /// <summary>
        /// Whenever a voice state changes ( heard or not ) check if all voice states have fired. If so, end stage.
        /// </summary>
        private void CheckVoiceStates()
        {
            if (this.CountAndPullDetected)
            {
                // Detectable Action
                this.Lesson.DetectableActions.DetectAction("One Two Three Pull", "Count and Pull", "Vocal Interaction", true);
                this.EndStage();
            }
        }

        private void CreateStageWatchers()
        {
            // Clear the list of watchers.
            this.CurrentStageWatchers.Clear();
            this.PeripheralStageWatchers.Clear();
            this.OutlyingStageWatchers.Clear();
        }

        /// <summary>
        /// Checks if the stage has timed out or complete.
        /// </summary>
        public override void UpdateStage()
        {
            if (!this.IsSetup && this.Lesson.CurrentStageIndex == this.StageNumber)
                this.SetupStage();

            // Check the Stage Timeout
            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
                this.TimeoutStage();
        }

        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }
        
        // Listens for speech recognized by the SpeechCommander.
        void KinectSpeechCommander_SpeechRecognized(object sender, Speech.SpeechCommanderEventArgs e)
        {
            switch (e.Command)
            {
                case "COUNT_AND_PULL":
                    OnSaid_CountAndPull(e);
                    break;
                default:
                    break;
            }
        }

        // The user spoke to AIMI: "One Two Three Pull".
        private void OnSaid_CountAndPull(Speech.SpeechCommanderEventArgs speechArgs)
        {
            if (this.Lesson.Started && !this.Lesson.Complete && this.Lesson.CurrentStageIndex > 0)  // Check to make certain that we're listening after calibration, before the end of the lesson.
            {
                this.CountAndPullDetected = true;

                this.Lesson.DidVocalCount = true;
                this.Lesson.VocalCountTime = DateTime.Now;
            }
        }

        public override void EndStage( bool playSound = true )
        {
            this.Complete = true;

            this.Stopwatch.Stop();

            this.TimeoutStopwatch.Stop();

            // Get the current time to signify the time the stage ended.
            this.EndTime = DateTime.Now;

            if (!this.Lesson.PullStarted && this.Lesson.CurrentStageIndex == this.StageNumber) // We only want to incremint the stage if it's currently at this stage's index. - Otherwise we don't want to interfere.
            {
                this.Lesson.CurrentStageIndex++;

                if( playSound ) Lesson.PlayNextStageSound();
            }
        }

        public void CalculateStage2Score(StageScore stagescore)
        {
            double score = 1.0;

            //
            // Stage Score Calculation Logic
            //

            stagescore.Score = System.Math.Max(0, System.Math.Min(score, 1)); // clamp the score between 0 and 1, and then set it.
        }

        public override void PauseStage()
        {
            this.Lesson.Paused = true;
            this.Lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.Lesson.Paused = false;
            this.Lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void ResetStage()
        {

        }

        /// <summary>
        /// Event that occurs whenever a stage times out. (If it's allowed to).
        /// </summary>
        public override void OnStageTimeout()
        {
            this.TimedOut = true;
            this.TimeoutStopwatch.Reset();  // resets only the timeoutstopwatch, so that it can re-timeout.
            this.RequestStageVideo();       // stops all timers, plays video, then starts all timers again. (timeoutstopwatch was reset, everything else picks back up again from where it was).
        }

        public override void OnStageVideoRequest()
        {
            this.PauseStage();

            // Show the stage help video
            this.Lesson.LateralTransferPage.VideoPlayer.setVideoSource(new Uri("../../Tasks/Lateral Transfer/Videos/Stage 002.wmv", UriKind.Relative));

            this.Lesson.LateralTransferPage.VideoPlayer.startVideo(this, new System.Windows.RoutedEventArgs());

            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaEnded += this.HelpVideo_MediaEnded;
            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaFailed += this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaEnded(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        private void HelpVideo_MediaFailed(object sender, EventArgs e)
        {
            this.ResumeStage();

            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaEnded -= this.HelpVideo_MediaEnded;
            this.Lesson.LateralTransferPage.VideoPlayer.MediaElement.MediaFailed -= this.HelpVideo_MediaFailed;
        }

        public override void WipeClean()
        {
            // Stage Reinitialization
            this.CountAndPullDetected = false;

            this.StartTime = DateTime.Now;
            this.EndTime = DateTime.MaxValue;
            this.IsSetup = false;

            if (this.StageScore != null)
            {
                this.StageScore.WipeClean();
                this.StageScore = null;
                this.StageScore = new StageScore();
            }
            else
            {
                this.StageScore = new StageScore();
            }

            this.Active = false;
            this.Complete = false;

            if (this.Zones != null)
            {
                this.Zones.Clear();
            }
            else
            {
                this.Zones = new List<Zone>();
            }

            if (this.Stopwatch != null)
            {
                this.Stopwatch.Reset();
            }
            else
            {
                this.Stopwatch = new System.Diagnostics.Stopwatch();
            }

            if (this.TimeoutStopwatch != null)
            {
                this.TimeoutStopwatch.Reset();
            }
            else
            {
                this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            }

            this.TimedOut = false;
        }

    } // End LateralTransferVocalInteractionStage Class

} // End namespace AIMS.Tasks.LateralTransfer
