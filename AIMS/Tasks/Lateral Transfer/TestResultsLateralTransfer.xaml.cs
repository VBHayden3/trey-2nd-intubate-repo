﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using AIMS.Tasks.LateralTransfer;
using AIMS.Tasks;

namespace AIMS
{
	public partial class TestResultsLateralTransfer : INotifyPropertyChanged, ITaskResultsControl
	{
        private LateralTransferResult result = null;
        public LateralTransferResult Result 
        { 
            get 
            { 
                return result; 
            } 
            set 
            { 
                result = value;
                
                NotifyPropertyChanged("Result");

                NotifyPropertyChanged("TaskMode");
                NotifyPropertyChanged("MasteryLevel");


                NotifyPropertyChanged("LessonStartTime");
                NotifyPropertyChanged("LessonEndTime");

                NotifyPropertyChanged("Score");

                NotifyPropertyChanged("HandSync");
                NotifyPropertyChanged("RHandSync");
                NotifyPropertyChanged("LHandSync");

                NotifyPropertyChanged("WristSync");
                NotifyPropertyChanged("RWristSync");
                NotifyPropertyChanged("LWristSync");

                NotifyPropertyChanged("ElbowSync");
                NotifyPropertyChanged("RElbowSync");
                NotifyPropertyChanged("LElbowSync");

                NotifyPropertyChanged("ShoulderSync");
                NotifyPropertyChanged("RShoulderSync");
                NotifyPropertyChanged("LShoulderSync");

                NotifyPropertyChanged("ManikinSnyc");
                NotifyPropertyChanged("TotalSync");

                NotifyPropertyChanged("PullStartTime");
                NotifyPropertyChanged("PullEndTime");

                NotifyPropertyChanged("HandPlacementScore");

                NotifyPropertyChanged("DidVerbalCount");
                NotifyPropertyChanged("VerbalCountTime");

                NotifyPropertyChanged("VerbalCountScore");

                NotifyPropertyChanged("OverallFeedback");
                NotifyPropertyChanged("HandPlacementFeedback");
                NotifyPropertyChanged("PullFeedback");

            } 
        }

        public TaskMode TaskMode
        {
            get
            {
                if (this.Result == null)
                    return AIMS.TaskMode.Practice;

                return this.Result.TaskMode;
            }
        }

        public MasteryLevel MasteryLevel
        {
            get
            {
                if (this.Result == null)
                    return MasteryLevel.Novice;

                return this.Result.MasteryLevel;
            }
        }

        public DateTime StartTime
        {
            get
            {
                if (this.Result == null)
                    return DateTime.MinValue;

                return this.Result.StartTime;
            }
        }

        public DateTime EndTime
        {
            get
            {
                if (this.Result == null)
                    return DateTime.MaxValue;

                return this.Result.EndTime;
            }
        }

        public DateTime PullStartTime
        {
            get
            {
                if (this.Result == null)
                    return DateTime.MinValue;

                return this.Result.PullStartTime;
            }
        }

        public DateTime PullEndTime
        {
            get
            {
                if (this.Result == null)
                    return DateTime.MaxValue;

                return this.Result.PullEndTime;
            }
        }

        public double HandSync
        {
            get
            {
                if( this.Result == null )
                    return 0;

                return (this.Result.RHandSync + this.Result.LHandSync) * 0.5;
            }
        }

        public double RHandSync
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.RHandSync;
            }
        }

        public double LHandSync
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.LHandSync;
            }
        }

        public double WristSync
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return (this.Result.RWristSync + this.Result.LWristSync) * 0.5;
            }
        }

        public double RWristSync
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.RWristSync;
            }
        }

        public double LWristSync
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.LWristSync;
            }
        }

        public double ElbowSync
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return (this.Result.RElbowSync + this.Result.LElbowSync) * 0.5;
            }
        }

        public double RElbowSync
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.RElbowSync;
            }
        }

        public double LElbowSync
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.LElbowSync;
            }
        }

        public double ShoulderSync
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return (this.Result.RShoulderSync + this.Result.LShoulderSync) * 0.5;
            }
        }

        public double RShoulderSync
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.RShoulderSync;
            }
        }

        public double LShoulderSync
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.LShoulderSync;
            }
        }

        public double TotalSync
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.TotalSync;
            }
        }

        public double ManikinSync
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.ManikinSync;
            }
        }

        public double Score
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.Score;
            }
        }

        public double HandPlacementScore
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.HandPlacementScore;
            }
        }

        public bool DidVerbalCount
        {
            get
            {
                if (this.Result == null)
                    return false;

                return this.Result.VerbalCount;
            }
        }

        public DateTime VerbalCountTime
        {
            get
            {
                if (this.Result == null)
                    return DateTime.MinValue;

                return this.Result.VerbalCountTime;
            }
        }

        public double VerbalCountScore
        {
            get
            {
                if (this.Result == null)
                    return 0;

                return this.Result.VerbalCountScore;
            }
        }

        public TaskFeedback[] OverallFeedback
        {
            get
            {
                if( this.Result == null )
                    return null;

                return GetTaskFeedbacksFromIDs(this.Result.OverallFeedbackIDs);
            }
        }

        /// <summary>
        /// Helper function to convert and return the TaskFeedback objects rather than int ids.
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        private TaskFeedback[] GetTaskFeedbacksFromIDs(List<int> ids)
        {
            if( ids == null )
                return null;

            TaskFeedback[] feedbacks = new TaskFeedback[ids.Count];

            for (int i = 0; i < ids.Count; i++)
            {
                feedbacks[i] = TaskFeedback.GetFeedbackFromID(ids[i], Task.LateralTransfer);
            }

            return feedbacks;
        }
	
		public TestResultsLateralTransfer()
		{
			this.InitializeComponent();
			// Insert code required on object creation below this point.

            this.DataContext = this;

            Loaded += TestResultsLateralTransfer_Loaded;
		}

        void TestResultsLateralTransfer_Loaded(object sender, RoutedEventArgs e)
        {
            //this.DataContext = this;
        }

        public void ShowResults(LateralTransferResult result, SkeletonPathComparison spc)
        {
            /*
            if (result == null)
                return;*/

            this.Result = result;

            if (spc != null)
            {
                this.SkeletonPathComparisonViewer.SkeletonPathComparison = spc;
                this.SkeletonPathComparisonViewer.DrawPaths(this.Result.PullStartTime, this.Result.PullEndTime, 25, true);  // draw the paths.
                this.SkeletonPathComparisonViewer.Visibility = System.Windows.Visibility.Visible;
                this.SkeletonPathComparisonViewer.IsEnabled = true;
            }
            else
            {
                this.SkeletonPathComparisonViewer.Visibility = System.Windows.Visibility.Collapsed;
                this.SkeletonPathComparisonViewer.IsEnabled = false;
            }

            this.Visibility = System.Windows.Visibility.Visible;
            this.IsEnabled = true;

            SubmitButton.IsEnabled = true;
            SubmitButton.Content = "Submit";    // reset submition text
            TryAgainButton.IsEnabled = true;
            ExitButton.IsEnabled = true;

            Page parentPage = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

            if (parentPage as LateralTransferPage != null)
            {
                LateralTransferPage tp = parentPage as LateralTransferPage;

                tp.BlurEffect.Radius = 25;
            }
        }

        /// <summary>
        /// Closes (Hides/Disables/Un-blurs) the Results Overlay.
        /// </summary>
        public void CloseResults(bool wipeInformation)
        {
            this.Visibility = System.Windows.Visibility.Collapsed;
            this.IsEnabled = false;

            Page parentPage = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

            if (parentPage as LateralTransferPage != null)
            {
                LateralTransferPage tp = parentPage as LateralTransferPage;

                tp.BlurEffect.Radius = 0;
            }

            if (wipeInformation)
                WipeInformationClean();
        }

        /// <summary>
        /// Cleans out the information being stored in the overlay.
        /// </summary>
        public void WipeInformationClean()
        {
            if (this.Result != null)
            {
                this.Result = null;
            }
        }

        /// <summary>
        /// Event triggered when the Exit button on the component is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitClicked(object sender, RoutedEventArgs e)
        {
            DependencyObject dependencyObject = this;
            Type type = typeof(Page);

            while (dependencyObject != null)
            {
                if (type.IsInstanceOfType(dependencyObject))
                    break;
                else
                    dependencyObject = VisualTreeHelper.GetParent(dependencyObject);
            }

            if (dependencyObject != null)
            {
                Page pg = (Page)dependencyObject;
                Uri uri = new Uri("../../MainPage3.xaml", UriKind.Relative);
                pg.NavigationService.Navigate(uri);
            }
        }

        /// <summary>
        /// Cycles up the visual tree from the "startObject" in search for a DependencyObject of type "type".
        /// </summary>
        /// <param name="startObject"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private DependencyObject GetDependencyObjectFromVisualTree(DependencyObject startObject, Type type)
        {
            DependencyObject parent = startObject;

            while (parent != null)
            {
                if (type.IsInstanceOfType(parent))
                    break;

                else
                    parent = VisualTreeHelper.GetParent(parent);
            }

            return parent;
        }

        /// <summary>
        /// Event which fires when the TryAgainButton on the component is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TryAgainClicked(object sender, RoutedEventArgs e)
        {
            Page parentPage = GetDependencyObjectFromVisualTree(this, typeof(Page)) as Page;

            if (parentPage as LateralTransferPage != null)
            {
                LateralTransferPage tp = parentPage as LateralTransferPage;
                tp.Lesson.ResetLesson();// Restart();
            }

            CloseResults(true);
        }

        /// <summary>
        /// Event triggered when the submit button on the component is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubmitClicked(object sender, RoutedEventArgs e)
        {
            if (this.Result == null)
                return;

            SubmitButton.IsEnabled = false;
            SubmitButton.Content = "Submitting...";

            // Disable the TryAgainButton and ExitButton to be reenabled upon submission (otherwise this was causing a null reference crash before)
            this.ExitButton.IsEnabled = false;
            this.TryAgainButton.IsEnabled = false;

            if (((App)App.Current).CurrentAccount == null || ((App)App.Current).CurrentStudent == null || ((App)App.Current).CurrentUserID <= 0)
            {
                MessageBox.Show("You cannot submit results to the database unless you are logged in as a student.");
                return;
            }

            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                //SerializeResultsAndSubmit();
                LateralTransferResult.StoreResult(this.Result);
            };

            worker.RunWorkerCompleted += delegate(object s, RunWorkerCompletedEventArgs args)
            {
                SubmitButton.Content = "Success";
                //Re-enable TryAgain and Exit buttons
                TryAgainButton.IsEnabled = true;
                ExitButton.IsEnabled = true;
            };

            worker.RunWorkerAsync();

            e.Handled = true;
        }
		
		public event PropertyChangedEventHandler PropertyChanged;
		
        private void NotifyPropertyChanged( string propertyName )
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void ShowButtons()
        {
            this.SubmitButton.Visibility = System.Windows.Visibility.Visible;
            this.SubmitButton.IsEnabled = true;

            this.TryAgainButton.Visibility = System.Windows.Visibility.Visible;
            this.TryAgainButton.IsEnabled = true;

            this.ExitButton.Visibility = System.Windows.Visibility.Visible;
            this.ExitButton.IsEnabled = true;
        }

        public void HideButtons()
        {
            this.SubmitButton.Visibility = System.Windows.Visibility.Collapsed;
            this.SubmitButton.IsEnabled = false;

            this.TryAgainButton.Visibility = System.Windows.Visibility.Collapsed;
            this.TryAgainButton.IsEnabled = false;

            this.ExitButton.Visibility = System.Windows.Visibility.Collapsed;
            this.ExitButton.IsEnabled = false;
        }

        public void ShowResults(TaskResult result, bool showButtons)
        {
            if (result is LateralTransferResult)
            {
                LateralTransferResult lateraltransferresult = result as LateralTransferResult;

                this.ShowResults(lateraltransferresult, null);  // show the lateral transfer result, but set the skeleton path comparison to null - indicating not to draw the 3D viewport, but to instead use the images.
            }
            else if( result != null && result.TaskType == Task.LateralTransfer )
            {
                // TODO: Handle non-complete TaskResult objects.
                return;
            }
            else
            {
                return;
            }

            if (showButtons)
                this.ShowButtons();
            else this.HideButtons();
        }
    }
}