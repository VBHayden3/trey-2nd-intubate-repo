﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Diagnostics;

namespace AIMS
{
    /// <summary>
    /// Abstract class which all tasks derive.
    /// </summary>
    public abstract class Lesson : DependencyObject
    {
        /*
        protected string name;
        protected string description;
        protected List<Stage> stages;
        protected TaskMode taskMode;
        protected MasteryLevel masteryLevel;
        protected DateTime startTime;
        protected DateTime endTime;
        protected bool paused;
        protected bool running;
        protected int currentStageIndex;
        protected string imagePath;
        protected TimeSpan targetTime;
        protected bool started;
        protected Result result;
        protected Stopwatch stopwatch;
        protected bool complete;*/

        public static readonly DependencyProperty NameProperty =
            DependencyProperty.Register(
                "Name",
                typeof(string),
                typeof(Lesson),
                new PropertyMetadata(null));

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register(
                "Description",
                typeof(string),
                typeof(Lesson),
                new PropertyMetadata(null));

        public static readonly DependencyProperty StagesProperty =
            DependencyProperty.Register(
                "Stages",
                typeof(List<Stage>),
                typeof(Lesson),
                new PropertyMetadata(null));

        public static readonly DependencyProperty TaskModeProperty =
            DependencyProperty.Register(
                "TaskMode",
                typeof(TaskMode),
                typeof(Lesson),
                new PropertyMetadata(TaskMode.Practice));

        public static readonly DependencyProperty MasteryLevelProperty =
            DependencyProperty.Register(
                "MasteryLevel",
                typeof(MasteryLevel),
                typeof(Lesson),
                new PropertyMetadata(MasteryLevel.Novice));

        public static readonly DependencyProperty StartTimeProperty =
            DependencyProperty.Register(
                "StartTime",
                typeof(DateTime),
                typeof(Lesson),
                new PropertyMetadata(DateTime.MinValue));

        public static readonly DependencyProperty EndTimeProperty =
            DependencyProperty.Register(
                "EndTime",
                typeof(DateTime),
                typeof(Lesson),
                new PropertyMetadata(DateTime.MaxValue));

        public static readonly DependencyProperty PausedProperty =
            DependencyProperty.Register(
                "Paused",
                typeof(bool),
                typeof(Lesson),
                new PropertyMetadata(false));

        public static readonly DependencyProperty RunningProperty =
            DependencyProperty.Register(
                "Running",
                typeof(bool),
                typeof(Lesson),
                new PropertyMetadata(false));

        public static readonly DependencyProperty CurrentStageIndexProperty =
            DependencyProperty.Register(
                "CurrentStageIndex",
                typeof(int),
                typeof(Lesson),
                new PropertyMetadata(0));

        public static readonly DependencyProperty ImagePathProperty =
            DependencyProperty.Register(
                "ImagePath",
                typeof(string),
                typeof(Lesson),
                new PropertyMetadata(null));

        public static readonly DependencyProperty TargetTimeProperty =
            DependencyProperty.Register(
                "TargetTime",
                typeof(TimeSpan),
                typeof(Lesson),
                new PropertyMetadata(TimeSpan.Zero));

        public static readonly DependencyProperty StartedProperty =
            DependencyProperty.Register(
                "Started",
                typeof(bool),
                typeof(Lesson),
                new PropertyMetadata(false));

        public static readonly DependencyProperty ResultProperty =
            DependencyProperty.Register(
                "Result",
                typeof(Result),
                typeof(Lesson),
                new PropertyMetadata(null));

        public static readonly DependencyProperty StopwatchProperty =
            DependencyProperty.Register(
                "Stopwatch",
                typeof(Stopwatch),
                typeof(Lesson),
                new PropertyMetadata(null));

        public static readonly DependencyProperty CompleteProperty =
            DependencyProperty.Register(
                "Complete",
                typeof(bool),
                typeof(Lesson),
                new PropertyMetadata(false));

        public static readonly DependencyProperty DeductionPercentPerSecondProperty =
            DependencyProperty.Register(
                "DeductionPercentPerSecond",
                typeof(double),
                typeof(Lesson),
                new PropertyMetadata(4.615));
        /// <summary>
        /// The minimalistic name of the Lesson/Task.
        /// </summary>
        public string Name { get { return (string)GetValue(NameProperty); } set { SetValue(NameProperty, value); } }
        /// <summary>
        /// The slightly expanded name of the Lesson/Task.
        /// </summary>
        public string Description { get { return (string)GetValue(DescriptionProperty); } set { SetValue(DescriptionProperty, value ); } }
        /// <summary>
        /// The collection of stage objects (in order). 
        /// This list starts with the Callibration stage, 
        /// followed by the logic stages, 
        /// and ends with the display results stage.
        /// </summary>
        public List<Stage> Stages { get { return (List<Stage>)GetValue(StagesProperty); } set { SetValue(StagesProperty, value); } }
        /// <summary>
        /// The TaskMode (Practice or Test) which the lesson is being completed as.
        /// </summary>
        public TaskMode TaskMode { get { return (TaskMode)GetValue(TaskModeProperty); } set { SetValue( TaskModeProperty, value ); } }
        /// <summary>
        /// The MasteryLevel (Novice, Intermediate, or Master) which the lesson is being completed as.
        /// </summary>
        public MasteryLevel MasteryLevel { get { return (MasteryLevel)GetValue(MasteryLevelProperty); } set { SetValue(MasteryLevelProperty, value); } }
        /// <summary>
        /// The DateTime which the lesson is started at.
        /// </summary>
        public DateTime StartTime { get { return (DateTime)GetValue(StartTimeProperty); } set { SetValue(StartTimeProperty, value ); } }
        /// <summary>
        /// The DateTime which the lesson is ended at.
        /// </summary>
        public DateTime EndTime { get { return (DateTime)GetValue(EndTimeProperty); } set { SetValue(EndTimeProperty, value); } }
        /// <summary>
        /// Whether or not the entire lesson is Paused. Paused lessons may be unpaused by resume.
        /// </summary>
        public bool Paused { get { return (bool)GetValue(PausedProperty); } set { SetValue(PausedProperty, value); } }
        /// <summary>
        /// Whether or not the lesson is running.
        /// </summary>
        public bool Running { get { return (bool)GetValue(RunningProperty); } set { SetValue(RunningProperty, value); } }
        /// <summary>
        /// The index of the stage which the Lesson is currently running.
        /// </summary>
        public int CurrentStageIndex { get { return (int)GetValue(CurrentStageIndexProperty); } set { SetValue(CurrentStageIndexProperty, value); } }
        /// <summary>
        /// The relative string path to the Lesson's reprentative image.
        /// </summary>
        public string ImagePath { get { return (string)GetValue(ImagePathProperty); } set { SetValue(ImagePathProperty, value); } }
        /// <summary>
        /// The target total time of the lesson.
        /// </summary>
        public TimeSpan TargetTime { get { return (TimeSpan)GetValue(TargetTimeProperty); } set { SetValue(TargetTimeProperty, value); } }
        /// <summary>
        /// The Result object for the lesson.
        /// </summary>
        public Result Result { get { return (Result)GetValue(ResultProperty); } set { SetValue(ResultProperty, value); } }
        /// <summary>
        /// Whether or not the Lesson has been started.
        /// </summary>
        public bool Started { get { return (bool)GetValue(StartedProperty); } set { SetValue(StartedProperty, value); } }
        /// <summary>
        /// Stopwatch which tracks the entire time of the lesson. This is paused/resumed when PauseLesson/ResumeLesson is called.
        /// </summary>
        public Stopwatch Stopwatch { get { return (Stopwatch)GetValue(StopwatchProperty); } set { SetValue(StopwatchProperty, value); } }
        /// <summary>
        /// Whether or not the Lesson has ended.
        /// </summary>
        public bool Complete { get { return (bool)GetValue(CompleteProperty); } set { SetValue(CompleteProperty, value); } }

        public double DeductionPercentPerSecond { get { return (double)GetValue(DeductionPercentPerSecondProperty); } set { SetValue(DeductionPercentPerSecondProperty, value); } }

        /// <summary>
        /// Starts the Lesson.
        /// </summary>
        public abstract void StartLesson();
        /// <summary>
        /// Iterates through once the Update logic for the Lesson. (Called by the KinectManager.AllDataReady event).
        /// </summary>
        public abstract void UpdateLesson();
        /// <summary>
        /// Ends the Lesson.
        /// </summary>
        public abstract void EndLesson();
        /// <summary>
        /// Pauses the Lesson, called when Media clips are played.
        /// </summary>
        public abstract void PauseLesson();
        /// <summary>
        /// Resumes the Lesson from a Pause, called when Media clips end playing.
        /// </summary>
        public abstract void ResumeLesson();
        /// <summary>
        /// Resets the values of variables within the Lesson back to their initial state.
        /// </summary>
        public abstract void WipeClean();

        public void RequestTutorialVideo()
        {
            this.OnTutorialVideoRequest();
        }

        /// <summary>
        /// This method is called by RequestTutorialVideo(). Each stage is responsible for providing its own stage help request logic.
        /// This may include refusing to help completely (if it makes sense to do so), or (most likely) various stage help techniques.
        /// </summary>
        public abstract void OnTutorialVideoRequest();

        /// <summary>
        /// Calls the WipeClean function and then the StartLesson function to effectively restart the lesson.
        /// </summary>
        public virtual void ResetLesson()
        {
            WipeClean();
            StartLesson();
        }

        /// <summary>
        /// Call AbortLesson() when a Kinect sensor cannot be detected during the execution of a Lesson.
        /// This will stop and clearout a lesson.
        /// </summary>
        public virtual void AbortLesson(){}

        /// <summary>
        /// Calculate the total score by scaling each individual stage's score depending on it's worth. Stages which should not be included in the TotalScore should have a Stage.ScoreWeight of 0.
        /// </summary>
        /// <returns>Total Score for Lesson</returns>
        public virtual double CalculateResultTotalScore()
        {
            double score = 0;
            double secondsOverTarget;
            double pointsOff;

            if (this.Stages != null)
            {
                foreach (Stage stage in this.Stages)
                {
                    // Penalize stages which took too long.
                    if (stage.StageScore.TimeInStage > stage.TargetTime)
                    {
                        secondsOverTarget = stage.StageScore.TimeInStage.TotalSeconds - stage.TargetTime.TotalSeconds;
                        pointsOff = System.Math.Max(0, (100 - (secondsOverTarget * (double)GetValue(DeductionPercentPerSecondProperty))));

                        stage.StageScore.Score *= System.Math.Min(pointsOff / 100, 1);
                    }
                    // Compute the total score for the lesson.
                    score += stage.StageScore.Score * stage.ScoreWeight;
                }
            }

            return score;
        }

        /// <summary>
        /// Creates the Result object for the Lesson after the lesson has completed.
        /// </summary>
        /// <returns>Lesson Result</returns>
        public virtual Result CreateResultFromStageScores()
        {
            Result result = new Result();
            double score = 0;
            TimeSpan span = TimeSpan.Zero;

            result.StageScores = new List<StageScore>();

            foreach (Stage stage in Stages)
            {
                result.StageScores.Add(stage.StageScore);
                score += result.StageScores.Last().Score;
                span.Add(result.StageScores.Last().TimeInStage);
            }

            score /= result.StageScores.Count;

            result.TotalScore = CalculateResultTotalScore();
            result.TotalTime = span;
            result.Date = DateTime.Now;
            result.Passed = (result.TotalScore >= 0.70);

            if (((App)App.Current).CurrentStudent != null)
                result.StudentID = ((App)App.Current).CurrentStudent.ID;

            result.TaskMode = this.TaskMode;
            result.LessonType = LessonType.CPR;
            result.MasteryLevel = this.MasteryLevel;

            return result;
        }
    }
}
