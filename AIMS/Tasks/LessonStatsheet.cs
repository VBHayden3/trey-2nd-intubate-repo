﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using System.Windows;

namespace AIMS.Tasks
{
    public class LessonStatsheet
    {
        public int ID { get; set; }
        public int StudentID { get; set; }
        public int TimesPracticed { get; set; }
        public int TimesTested { get; set; }
        public DateTime TimeLastPracticed { get; set; }
        public DateTime TimeLastTested { get; set; }
        public MasteryLevel MasteryLevel { get; set; }
        public int Points { get; set; }
        public double AverageNoviceTestScore { get; set; }
        public double AverageNovicePracticeScore { get; set; }
        public double AverageIntermediateTestScore { get; set; }
        public double AverageIntermediatePracticeScore { get; set; }
        public double AverageMasterTestScore { get; set; }
        public double AverageMasterPracticeScore { get; set; }
        public int TimesPracticedNovice { get; set; }
        public int TimesPracticedIntermediate { get; set; }
        public int TimesPracticedMaster { get; set; }
        public int TimesTestedNovice { get; set; }
        public int TimesTestedIntermediate { get; set; }
        public int TimesTestedMaster { get; set; }
        public int? LastPracticeResultID { get; set; }
        public int? LastNovicePracticeResultID { get; set; }
        public int? LastIntermediatePracticeResultID { get; set; }
        public int? LastMasterPracticeResultID { get; set; }
        public int? LastTestResultID { get; set; }
        public int? LastNoviceTestResultID { get; set; }
        public int? LastIntermediateTestResultID { get; set; }
        public int? LastMasterTestResultID { get; set; }
        public LessonType LessonType { get; set; }
        public int NoviceTestsInSuccession { get; set; }
        public int NovicePracticesInSuccession { get; set; }
        public int IntermediateTestsInSuccession { get; set; }
        public int IntermediatePracticesInSuccession { get; set; }
        public int MasterTestsInSuccession { get; set; }
        public int MasterPracticesInSuccession { get; set; }

        public static LessonStatsheet GetLessonStatsheetFromStatsheetID(int id)
        {
            if (id <= 0)
            {
                return null;
            }

            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "SELECT uid, studentid, timespracticed, timestested, timelastpracticed, timelasttested, masterylevel, points, averagenovicetestscore, averagenovicepracticescore, averageintermediatetestscore, averageintermediatepracticescore, averagemastertestscore, averagemasterpracticescore, timestestednovice, timespracticednovice, timestestedintermediate, timespracticedintermediate, timestestedmaster, timespracticedmaster, lastpracticeresultid, lastnovicepracticeresultid, lastintermediatepracticeresultid, lastmasterpracticeresultid, lasttestresultid, lastnovicetestresultid, lastintermediatetestresultid, lastmastertestresultid, lessontype, novicetestsinsuccession, novicepracticesinsuccession, intermediatetestsinsuccession, intermediatepracticesinsuccession, mastertestsinsuccession, masterpracticesinsuccession FROM lesson_statsheets WHERE uid = :id";
            command.Parameters.Add("id", NpgsqlTypes.NpgsqlDbType.Integer).Value = id;

            NpgsqlDataReader reader = command.ExecuteReader();

            LessonStatsheet statsheet = new LessonStatsheet();

            bool read = false;

            while (reader.Read())
            {
                statsheet.ID = reader.GetInt32(reader.GetOrdinal("uid"));
                statsheet.StudentID = reader.GetInt32(reader.GetOrdinal("studentid"));
                statsheet.TimesPracticed = reader.GetInt32(reader.GetOrdinal("timespracticed"));
                statsheet.TimesTested = reader.GetInt32(reader.GetOrdinal("timestested"));
                statsheet.TimeLastPracticed = reader.GetTimeStamp(reader.GetOrdinal("timelastpracticed"));
                statsheet.TimeLastTested = reader.GetTimeStamp(reader.GetOrdinal("timelasttested"));
                statsheet.MasteryLevel = (MasteryLevel)reader.GetInt32(reader.GetOrdinal("masterlevel"));
                statsheet.Points = reader.GetInt32(reader.GetOrdinal("points"));
                statsheet.AverageNoviceTestScore = reader.GetDouble(reader.GetOrdinal("averagenovicetestscore"));
                statsheet.AverageNovicePracticeScore = reader.GetDouble(reader.GetOrdinal("averagenovicepracticescore"));
                statsheet.AverageIntermediateTestScore = reader.GetDouble(reader.GetOrdinal("averageintermediatetestscore"));
                statsheet.AverageIntermediatePracticeScore = reader.GetDouble(reader.GetOrdinal("averageintermediatepracticescore"));
                statsheet.AverageMasterTestScore = reader.GetDouble(reader.GetOrdinal("averagemastertestscore"));
                statsheet.AverageMasterPracticeScore = reader.GetDouble(reader.GetOrdinal("averagemasterpracticescore"));
                statsheet.TimesTestedNovice = reader.GetInt32(reader.GetOrdinal("timestestednovice"));
                statsheet.TimesPracticedNovice = reader.GetInt32(reader.GetOrdinal("timespracticednovice"));
                statsheet.TimesTestedIntermediate = reader.GetInt32(reader.GetOrdinal("timestestedintermediate"));
                statsheet.TimesPracticedIntermediate = reader.GetInt32(reader.GetOrdinal("timespracticedintermediate"));
                statsheet.TimesTestedMaster = reader.GetInt32(reader.GetOrdinal("timestestedmaster"));
                statsheet.TimesPracticedMaster = reader.GetInt32(reader.GetOrdinal("timespracticedmaster"));
                statsheet.LastPracticeResultID = reader.IsDBNull(reader.GetOrdinal("lastpracticeresultid")) ? null : (int?)reader.GetInt32(reader.GetOrdinal("lastpracticeresultid"));
                statsheet.LastNovicePracticeResultID = reader.IsDBNull(reader.GetOrdinal("lastpnoviceracticeresultid")) ? null : (int?)reader.GetInt32(reader.GetOrdinal("lastnovicepracticeresultid"));
                statsheet.LastIntermediatePracticeResultID = reader.IsDBNull(reader.GetOrdinal("lastintermediatepracticeresultid")) ? null : (int?)reader.GetInt32(reader.GetOrdinal("lastintermediatepracticeresultid"));
                statsheet.LastMasterPracticeResultID = reader.IsDBNull(reader.GetOrdinal("lastmasterpracticeresultid")) ? null : (int?)reader.GetInt32(reader.GetOrdinal("lastmasterpracticeresultid"));
                statsheet.LastTestResultID = reader.IsDBNull(reader.GetOrdinal("lasttestresultid")) ? null : (int?)reader.GetInt32(reader.GetOrdinal("lasttestresultid"));
                statsheet.LastNoviceTestResultID = reader.IsDBNull(reader.GetOrdinal("lastnovicetestresultid")) ? null : (int?)reader.GetInt32(reader.GetOrdinal("lastnovicetestresultid"));
                statsheet.LastIntermediateTestResultID = reader.IsDBNull(reader.GetOrdinal("lastintermediatetestresultid")) ? null : (int?)reader.GetInt32(reader.GetOrdinal("lastintermediatetestresultid"));
                statsheet.LastMasterTestResultID = reader.IsDBNull(reader.GetOrdinal("lastmastertestresultid")) ? null : (int?)reader.GetInt32(reader.GetOrdinal("lastmastertestresultid"));
                statsheet.LessonType = (LessonType)reader.GetInt32(reader.GetOrdinal("lessontype"));
                statsheet.NoviceTestsInSuccession = reader.GetInt32(reader.GetOrdinal("novicetestsinsuccession"));
                statsheet.NovicePracticesInSuccession = reader.GetInt32(reader.GetOrdinal("novicepracticesinsuccession"));
                statsheet.IntermediateTestsInSuccession = reader.GetInt32(reader.GetOrdinal("intermediatetestsinsuccession"));
                statsheet.IntermediatePracticesInSuccession = reader.GetInt32(reader.GetOrdinal("intermediatepracticesinsuccession"));
                statsheet.MasterTestsInSuccession = reader.GetInt32(reader.GetOrdinal("mastertestsinsuccession"));
                statsheet.MasterPracticesInSuccession = reader.GetInt32(reader.GetOrdinal("masterpracticesinsuccession"));

                read = true;
            }

            DatabaseManager.CloseConnection();

            return (read ? statsheet : null);
        }
        
        /// <summary>
        /// Retreives the LessonStatsheet object from the database linked to the passed studentID of the passed lessontype.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="lessontype"></param>
        /// <returns></returns>
        public static LessonStatsheet GetLessonStatsheetFromStudentIDOfLessonType(int id, LessonType lessontype)
        {
            if (id <= 0)
            {
                return null;
            }

            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "SELECT uid, studentid, timespracticed, timestested, timelastpracticed, timelasttested, masterylevel, points, averagenovicetestscore, averagenovicepracticescore, averageintermediatetestscore, averageintermediatepracticescore, averagemastertestscore, averagemasterpracticescore, timestestednovice, timespracticednovice, timestestedintermediate, timespracticedintermediate, timestestedmaster, timespracticedmaster, lastpracticeresultid, lastnovicepracticeresultid, lastintermediatepracticeresultid, lastmasterpracticeresultid, lasttestresultid, lastnovicetestresultid, lastintermediatetestresultid, lastmastertestresultid, lessontype, novicetestsinsuccession, novicepracticesinsuccession, intermediatetestsinsuccession, intermediatepracticesinsuccession, mastertestsinsuccession, masterpracticesinsuccession FROM lesson_statsheets WHERE studentid = :id";
            command.Parameters.Add("id", NpgsqlTypes.NpgsqlDbType.Integer).Value = id;

            NpgsqlDataReader reader = command.ExecuteReader();

            LessonStatsheet statsheet = new LessonStatsheet();

            bool read = false;

            while (reader.Read())
            {
                statsheet.ID = reader.GetInt32(reader.GetOrdinal("uid"));
                statsheet.StudentID = reader.GetInt32(reader.GetOrdinal("studentid"));
                statsheet.TimesPracticed = reader.GetInt32(reader.GetOrdinal("timespracticed"));
                statsheet.TimesTested = reader.GetInt32(reader.GetOrdinal("timestested"));
                statsheet.TimeLastPracticed = reader.GetTimeStamp(reader.GetOrdinal("timelastpracticed"));
                statsheet.TimeLastTested = reader.GetTimeStamp(reader.GetOrdinal("timelasttested"));
                statsheet.MasteryLevel = (MasteryLevel)reader.GetInt32(reader.GetOrdinal("masterylevel"));
                statsheet.Points = reader.GetInt32(reader.GetOrdinal("points"));
                statsheet.AverageNoviceTestScore = reader.GetDouble(reader.GetOrdinal("averagenovicetestscore"));
                statsheet.AverageNovicePracticeScore = reader.GetDouble(reader.GetOrdinal("averagenovicepracticescore"));
                statsheet.AverageIntermediateTestScore = reader.GetDouble(reader.GetOrdinal("averageintermediatetestscore"));
                statsheet.AverageIntermediatePracticeScore = reader.GetDouble(reader.GetOrdinal("averageintermediatepracticescore"));
                statsheet.AverageMasterTestScore = reader.GetDouble(reader.GetOrdinal("averagemastertestscore"));
                statsheet.AverageMasterPracticeScore = reader.GetDouble(reader.GetOrdinal("averagemasterpracticescore"));
                statsheet.TimesTestedNovice = reader.GetInt32(reader.GetOrdinal("timestestednovice"));
                statsheet.TimesPracticedNovice = reader.GetInt32(reader.GetOrdinal("timespracticednovice"));
                statsheet.TimesTestedIntermediate = reader.GetInt32(reader.GetOrdinal("timestestedintermediate"));
                statsheet.TimesPracticedIntermediate = reader.GetInt32(reader.GetOrdinal("timespracticedintermediate"));
                statsheet.TimesTestedMaster = reader.GetInt32(reader.GetOrdinal("timestestedmaster"));
                statsheet.TimesPracticedMaster = reader.GetInt32(reader.GetOrdinal("timespracticedmaster"));
                statsheet.LastPracticeResultID = reader.IsDBNull( reader.GetOrdinal("lastpracticeresultid") ) ? null : (int?)reader.GetInt32(reader.GetOrdinal("lastpracticeresultid"));
                statsheet.LastNovicePracticeResultID = reader.IsDBNull(reader.GetOrdinal("lastnovicepracticeresultid")) ? null : (int?)reader.GetInt32(reader.GetOrdinal("lastnovicepracticeresultid"));
                statsheet.LastIntermediatePracticeResultID = reader.IsDBNull(reader.GetOrdinal("lastintermediatepracticeresultid")) ? null : (int?)reader.GetInt32(reader.GetOrdinal("lastintermediatepracticeresultid"));
                statsheet.LastMasterPracticeResultID = reader.IsDBNull(reader.GetOrdinal("lastmasterpracticeresultid")) ? null : (int?)reader.GetInt32(reader.GetOrdinal("lastmasterpracticeresultid"));
                statsheet.LastTestResultID = reader.IsDBNull(reader.GetOrdinal("lasttestresultid")) ? null : (int?)reader.GetInt32(reader.GetOrdinal("lasttestresultid"));
                statsheet.LastNoviceTestResultID = reader.IsDBNull(reader.GetOrdinal("lastnovicetestresultid")) ? null : (int?)reader.GetInt32(reader.GetOrdinal("lastnovicetestresultid"));
                statsheet.LastIntermediateTestResultID = reader.IsDBNull(reader.GetOrdinal("lastintermediatetestresultid")) ? null : (int?)reader.GetInt32(reader.GetOrdinal("lastintermediatetestresultid"));
                statsheet.LastMasterTestResultID = reader.IsDBNull(reader.GetOrdinal("lastmastertestresultid")) ? null : (int?)reader.GetInt32(reader.GetOrdinal("lastmastertestresultid"));
                statsheet.LessonType = (LessonType)reader.GetInt32(reader.GetOrdinal("lessontype"));
                statsheet.NoviceTestsInSuccession = reader.GetInt32(reader.GetOrdinal("novicetestsinsuccession"));
                statsheet.NovicePracticesInSuccession = reader.GetInt32(reader.GetOrdinal("novicepracticesinsuccession"));
                statsheet.IntermediateTestsInSuccession = reader.GetInt32(reader.GetOrdinal("intermediatetestsinsuccession"));
                statsheet.IntermediatePracticesInSuccession = reader.GetInt32(reader.GetOrdinal("intermediatepracticesinsuccession"));
                statsheet.MasterTestsInSuccession = reader.GetInt32(reader.GetOrdinal("mastertestsinsuccession"));
                statsheet.MasterPracticesInSuccession = reader.GetInt32(reader.GetOrdinal("masterpracticesinsuccession"));

                read = true;
            }

            DatabaseManager.CloseConnection();

            return (read ? statsheet : null);
        }

        /// <summary>
        /// Creates a LessonStatsheet for a student of a particular lessontype.
        /// </summary>
        /// <param name="studentid"></param>
        /// <returns></returns>
        public static LessonStatsheet CreateStatsheetForStudentOfLessonType(int studentid, LessonType lessontype)
        {
            if (studentid <= 0)
            {
                return null;
            }

            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;

            LessonStatsheet statsheet = new LessonStatsheet();

            statsheet.ID = -1;
            statsheet.StudentID = studentid;
            statsheet.TimesPracticed = 0;
            statsheet.TimesTested = 0;
            statsheet.TimeLastPracticed = DateTime.MinValue;
            statsheet.TimeLastTested = DateTime.MinValue;
            statsheet.MasteryLevel = MasteryLevel.Novice;
            statsheet.Points = 0;
            statsheet.AverageNoviceTestScore = 0;
            statsheet.AverageNovicePracticeScore = 0;
            statsheet.AverageIntermediateTestScore = 0;
            statsheet.AverageIntermediatePracticeScore = 0;
            statsheet.AverageMasterTestScore = 0;
            statsheet.AverageMasterPracticeScore = 0;
            statsheet.TimesTestedNovice = 0;
            statsheet.TimesPracticedNovice = 0;
            statsheet.TimesTestedIntermediate = 0;
            statsheet.TimesPracticedIntermediate = 0;
            statsheet.TimesTestedMaster = 0;
            statsheet.TimesPracticedMaster = 0;
            statsheet.LessonType = lessontype;

            command.CommandText = "INSERT INTO lesson_statsheets ( studentid, timespracticed, timestested, timelastpracticed, timelasttested, masterylevel, points, averagenovicetestscore, averagenovicepracticescore, averageintermediatetestscore, averageintermediatepracticescore, averagemastertestscore, averagemasterpracticescore, timestestednovice, timespracticednovice, timestestedintermediate, timespracticedintermediate, timestestedmaster, timespracticedmaster, lessontype, novicetestsinsuccession, novicepracticesinsuccession, intermediatetestsinsuccession, intermediatepracticesinsuccession, mastertestsinsuccession, masterpracticesinsuccession ) VALUES ( :studentid, :timespracticed, :timestested, :timelastpracticed, :timelasttested, :masterylevel, :points, :averagenovicetestscore, :averagenovicepracticescore, :averageintermediatetestscore, :averageintermediatepracticescore, :averagemastertestscore, :averagemasterpracticescore, :timestestednovice, :timespracticednovice, :timestestedintermediate, :timespracticedintermediate, :timestestedmaster, :timespracticedmaster, :lessontype, :novicetestsinsuccession, :novicepracticesinsuccession, :intermediatetestsinsuccession, :intermediatepracticesinsuccession, :mastertestsinsuccession, :masterpracticesinsuccession ) RETURNING uid;";
            command.Parameters.Add("studentid", NpgsqlTypes.NpgsqlDbType.Integer).Value = statsheet.StudentID;
            command.Parameters.Add("timespracticed", NpgsqlTypes.NpgsqlDbType.Integer).Value = statsheet.TimesPracticed;
            command.Parameters.Add("timestested", NpgsqlTypes.NpgsqlDbType.Integer).Value = statsheet.TimesTested;
            command.Parameters.Add("timelastpracticed", NpgsqlTypes.NpgsqlDbType.Timestamp).Value = statsheet.TimeLastPracticed;
            command.Parameters.Add("timelasttested", NpgsqlTypes.NpgsqlDbType.Timestamp).Value = statsheet.TimeLastTested;
            command.Parameters.Add("masterylevel", NpgsqlTypes.NpgsqlDbType.Integer).Value = (int)statsheet.MasteryLevel;
            command.Parameters.Add("points", NpgsqlTypes.NpgsqlDbType.Integer).Value = statsheet.Points;
            command.Parameters.Add("averagenovicetestscore", NpgsqlTypes.NpgsqlDbType.Double).Value = statsheet.AverageNoviceTestScore;
            command.Parameters.Add("averagenovicepracticescore", NpgsqlTypes.NpgsqlDbType.Double).Value = statsheet.AverageNovicePracticeScore;
            command.Parameters.Add("averageintermediatetestscore", NpgsqlTypes.NpgsqlDbType.Double).Value = statsheet.AverageIntermediateTestScore;
            command.Parameters.Add("averageintermediatepracticescore", NpgsqlTypes.NpgsqlDbType.Double).Value = statsheet.AverageIntermediatePracticeScore;
            command.Parameters.Add("averagemastertestscore", NpgsqlTypes.NpgsqlDbType.Double).Value = statsheet.AverageMasterTestScore;
            command.Parameters.Add("averagemasterpracticescore", NpgsqlTypes.NpgsqlDbType.Double).Value = statsheet.AverageMasterPracticeScore;
            command.Parameters.Add("timestestednovice", NpgsqlTypes.NpgsqlDbType.Integer).Value = statsheet.TimesTestedNovice;
            command.Parameters.Add("timespracticednovice", NpgsqlTypes.NpgsqlDbType.Integer).Value = statsheet.TimesPracticedNovice;
            command.Parameters.Add("timestestedintermediate", NpgsqlTypes.NpgsqlDbType.Integer).Value = statsheet.TimesTestedIntermediate;
            command.Parameters.Add("timespracticedintermediate", NpgsqlTypes.NpgsqlDbType.Integer).Value = statsheet.TimesPracticedIntermediate;
            command.Parameters.Add("timestestedmaster", NpgsqlTypes.NpgsqlDbType.Integer).Value = statsheet.TimesTestedMaster;
            command.Parameters.Add("timespracticedmaster", NpgsqlTypes.NpgsqlDbType.Integer).Value = statsheet.TimesPracticedMaster;
            command.Parameters.Add("lessontype", NpgsqlTypes.NpgsqlDbType.Integer).Value = (int)statsheet.LessonType;
            command.Parameters.Add("novicetestsinsuccession", NpgsqlTypes.NpgsqlDbType.Integer).Value = statsheet.NoviceTestsInSuccession;
            command.Parameters.Add("novicepracticesinsuccession", NpgsqlTypes.NpgsqlDbType.Integer).Value = statsheet.NovicePracticesInSuccession;
            command.Parameters.Add("intermediatetestsinsuccession", NpgsqlTypes.NpgsqlDbType.Integer).Value = statsheet.IntermediateTestsInSuccession;
            command.Parameters.Add("intermediatepracticesinsuccession", NpgsqlTypes.NpgsqlDbType.Integer).Value = statsheet.IntermediatePracticesInSuccession;
            command.Parameters.Add("mastertestsinsuccession", NpgsqlTypes.NpgsqlDbType.Integer).Value = statsheet.MasterTestsInSuccession;
            command.Parameters.Add("masterpracticesinsuccession", NpgsqlTypes.NpgsqlDbType.Integer).Value = statsheet.MasterPracticesInSuccession;

            int returnedID = 0;

            try
            {
                returnedID = (int)command.ExecuteScalar();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            DatabaseManager.CloseConnection();

            if (returnedID <= 0)
            {
                return null; // statsheet wasn't created successfully. return null.
            }
            else
            {
                statsheet.ID = returnedID;  // statsheet was created successfully. set the id of the statsheet object to the returned uid value.
                return statsheet;   // return the newly created object.
            }
        }

        /// <summary>
        /// Gets an existing statsheet for a particular studentid of a specific lessontype. 
        /// If one doesn't exist, attempt to create one instead.
        /// </summary>
        /// <param name="studentid"></param>
        /// <param name="lessontype"></param>
        /// <returns></returns>
        public static LessonStatsheet GetOrCreateStatsheetForStudent(int studentid, LessonType lessontype)
        {
            if (studentid <= 0)
            {
                return null;
            }

            LessonStatsheet statsheet = null;

            statsheet = GetLessonStatsheetFromStudentIDOfLessonType(studentid, lessontype); // attempt to locate an already created statsheet for the student.

            if (statsheet == null)  // if it returns null, then there wasn't one..
            {
                statsheet = CreateStatsheetForStudentOfLessonType(studentid, lessontype);   // attempt to create a new statsheet for the student.
            }

            return statsheet;   // return the statsheet, where it is null, old, or newly created.
        }

        /// <summary>
        /// This will log the information stored within the result parameter object, but will pass a "0" parameter as the resultID - and won't save the last run's resultID within the Database.
        /// If you want to link the last run's result object within the database onto the statsheet, pass the resultID (database uid) of the stored result object to link.
        /// </summary>
        /// <param name="result"></param>
        public void LogRun(Result result)
        {
            LogRun(result, 0);
        }

        /// <summary>
        /// Using information stored within the passed result object, the statsheet will be updated with the pertaining information.
        /// The statsheet will also link to the appropriate result entry within the database using the passed resultID (database uid) parameter.
        /// </summary>
        /// <param name="result"></param>
        /// <param name="resultID"></param>
        public void LogRun(Result result, int resultID)
        {
            if (result == null)
                return;

            if (resultID <= 0)
                return;

            if (result.LessonType != this.LessonType)
            {
                MessageBox.Show(String.Format("The result of type {0} could not be stored on a statsheet of type {1}", result.LessonType, this.LessonType));
                return;
            }

            switch (result.TaskMode)
            {
                case TaskMode.Practice:
                {
                    this.TimeLastPracticed = result.Date;
                    this.TimesPracticed++;

                    if (resultID > 0)
                        this.LastPracticeResultID = resultID;

                    switch (result.MasteryLevel)
                    {
                        case MasteryLevel.Novice:
                            this.TimesPracticedNovice++;
                            this.AverageNovicePracticeScore += result.TotalScore;
                            this.AverageNovicePracticeScore /= this.TimesPracticedNovice;

                            if (resultID > 0)
                                this.LastNovicePracticeResultID = resultID;
                            break;
                        case MasteryLevel.Intermediate:
                            this.TimesPracticedIntermediate++;
                            this.AverageIntermediatePracticeScore += result.TotalScore;
                            this.AverageIntermediatePracticeScore /= this.TimesPracticedIntermediate;

                            if (resultID > 0)
                                this.LastIntermediatePracticeResultID = resultID;
                            break;
                        case MasteryLevel.Master:
                            this.TimesPracticedMaster++;
                            this.AverageMasterPracticeScore += result.TotalScore;
                            this.AverageMasterPracticeScore /= this.TimesPracticedMaster;

                            if (resultID > 0)
                                this.LastMasterPracticeResultID = resultID;
                            break;
                    }

                    break;
                }
                case TaskMode.Test:
                {
                    this.TimeLastTested = result.Date;
                    this.TimesTested++;

                    if (resultID > 0)
                        this.LastTestResultID = resultID;

                    switch (result.MasteryLevel)
                    {
                        case MasteryLevel.Novice:
                            this.TimesTestedNovice++;
                            this.AverageNoviceTestScore += result.TotalScore;
                            this.AverageNoviceTestScore /= this.TimesTestedNovice;

                            if (resultID > 0)
                                this.LastNoviceTestResultID = resultID;
                            break;
                        case MasteryLevel.Intermediate:
                            this.TimesTestedIntermediate++;
                            this.AverageIntermediateTestScore += result.TotalScore;
                            this.AverageIntermediateTestScore /= this.TimesTestedIntermediate;

                            if (resultID > 0)
                                this.LastIntermediateTestResultID = resultID;
                            break;
                        case MasteryLevel.Master:
                            this.TimesTestedMaster++;
                            this.AverageMasterTestScore += result.TotalScore;
                            this.AverageMasterTestScore /= this.TimesTestedMaster;

                            if (resultID > 0)
                                this.LastMasterTestResultID = resultID;
                            break;
                    }

                    break;
                }
            }
        }

        public void UpdateInDatabase()
        {
            if (this.ID <= 0)   // Cannot update a non-existing statsheet..
                return;

            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandText = "UPDATE lesson_statsheets SET timespracticed = :timespracticed, timestested = :timestested, masterylevel = :masterylevel, points = :points, averagenovicetestscore = :averagenovicetestscore, averagenovicepracticescore = :averagenovicepracticescore, averageintermediatetestscore = :averageintermediatetestscore, averageintermediatepracticescore = :averageintermediatepracticescore, averagemastertestscore = :averagemastertestscore, averagemasterpracticescore = :averagemasterpracticescore, timestestednovice = :timestestednovice, timespracticednovice = :timespracticednovice, timestestedintermediate = :timestestedintermediate, timespracticedintermediate = :timespracticedintermediate, timestestedmaster = :timestestedmaster, timespracticedmaster = :timespracticedmaster, lasttestresultid = :lasttestresultid, lastpracticeresultid = :lastpracticeresultid, lastnovicetestresultid = :lastnovicetestresultid, lastnovicepracticeresultid = :lastnovicepracticeresultid, lastintermediatetestresultid = :lastintermediatetestresultid, lastintermediatepracticeresultid = :lastintermediatepracticeresultid, lastmastertestresultid = :lastmastertestresultid, lastmasterpracticeresultid = :lastmasterpracticeresultid, lessontype = :lessontype WHERE uid = :statsheetid";
            command.Parameters.Add("timespracticed", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.TimesPracticed;
            command.Parameters.Add("timestested", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.TimesTested;
            command.Parameters.Add("masterylevel", NpgsqlTypes.NpgsqlDbType.Integer).Value = (int)this.MasteryLevel;
            command.Parameters.Add("points", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.Points;
            command.Parameters.Add("averagenovicetestscore", NpgsqlTypes.NpgsqlDbType.Double).Value = this.AverageNoviceTestScore;
            command.Parameters.Add("averagenovicepracticescore", NpgsqlTypes.NpgsqlDbType.Double).Value = this.AverageNovicePracticeScore;
            command.Parameters.Add("averageintermediatetestscore", NpgsqlTypes.NpgsqlDbType.Double).Value = this.AverageIntermediateTestScore;
            command.Parameters.Add("averageintermediatepracticescore", NpgsqlTypes.NpgsqlDbType.Double).Value = this.AverageIntermediatePracticeScore;
            command.Parameters.Add("averagemastertestscore", NpgsqlTypes.NpgsqlDbType.Double).Value = this.AverageMasterTestScore;
            command.Parameters.Add("averagemasterpracticescore", NpgsqlTypes.NpgsqlDbType.Double).Value = this.AverageMasterPracticeScore;
            command.Parameters.Add("timestestednovice", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.TimesTestedNovice;
            command.Parameters.Add("timespracticednovice", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.TimesPracticedNovice;
            command.Parameters.Add("timestestedintermediate", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.TimesTestedIntermediate;
            command.Parameters.Add("timespracticedintermediate", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.TimesPracticedIntermediate;
            command.Parameters.Add("timestestedmaster", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.TimesTestedMaster;
            command.Parameters.Add("timespracticedmaster", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.TimesPracticedMaster;
            command.Parameters.Add("lasttestresultid", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.LastTestResultID;
            command.Parameters.Add("lastpracticeresultid", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.LastPracticeResultID;
            command.Parameters.Add("lastnovicetestresultid", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.LastNoviceTestResultID;
            command.Parameters.Add("lastnovicepracticeresultid", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.LastNovicePracticeResultID;
            command.Parameters.Add("lastintermediatetestresultid", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.LastIntermediateTestResultID;
            command.Parameters.Add("lastintermediatepracticeresultid", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.LastIntermediatePracticeResultID;
            command.Parameters.Add("lastmastertestresultid", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.LastMasterTestResultID;
            command.Parameters.Add("lastmasterpracticeresultid", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.LastMasterPracticeResultID;
            command.Parameters.Add("statsheetid", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.ID;
            command.Parameters.Add("lessontype", NpgsqlTypes.NpgsqlDbType.Integer).Value = (int)this.LessonType;
            command.Parameters.Add("novicetestsinsuccession", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.NoviceTestsInSuccession;
            command.Parameters.Add("novicepracticesinsuccession", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.NovicePracticesInSuccession;
            command.Parameters.Add("intermediatetestsinsuccession", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.IntermediateTestsInSuccession;
            command.Parameters.Add("intermediatepracticesinsuccession", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.IntermediatePracticesInSuccession;
            command.Parameters.Add("mastertestsinsuccession", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.MasterTestsInSuccession;
            command.Parameters.Add("masterpracticesinsuccession", NpgsqlTypes.NpgsqlDbType.Integer).Value = this.MasterPracticesInSuccession;

            int enq = 0;

            try
            {
                enq = command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            DatabaseManager.CloseConnection();

            if (enq > 0)
            {
                // Update Succeeded.
            }
            else
            {
                // Update failed.
            }
        }
    }
}
