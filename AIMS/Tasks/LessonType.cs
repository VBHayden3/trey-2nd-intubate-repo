﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIMS
{
    /// <summary>
    /// Enum of implemented lessons. 
    /// This list will grow as more lessons are added.
    /// 
    /// The order of this enum has significant importance because these values are casted and stored in the database.
    /// 0 = Intubation
    /// 1 = Vertical Lift
    /// 2 = Max Apex
    /// 3 = CPR
    /// 4 = Catheter Insertion
    /// 5 = IV Insertion
    /// </summary>
    public enum LessonType
    {
        Intubation,
        VerticalLift,
        MaxApex,
        CPR,
        CatheterInsertion,
        IVInsertion,
        LateralTransfer
    }
}
