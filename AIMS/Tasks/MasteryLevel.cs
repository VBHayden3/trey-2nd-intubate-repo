﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIMS
{
    public enum MasteryLevel
    {
        Novice,
        Intermediate,
        Master
    }
}
