﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using Microsoft.Kinect;
using System.Windows;
using System.Diagnostics;

namespace AIMS
{
    /// <summary>
    /// A Zone is a class object which represents a region in 3D space.
    /// 
    /// A Zone can calculate whether or not a Point, Point3D, or Joint is within it's 3D-Extruded-region.
    /// Zone supports concave and self-intersecting polygons.
    /// 
    /// A Zone will track the total amount of time a point has been found within the Zone (TotalTimeWithinZone),
    /// as well as the length of time the current timespan has been within the zone (TimeWithinZone).
    /// 
    /// Every zone has it's own meaning and purpose. 
    /// 
    /// If you want to check both a Point3D and a Joint within the same position,
    /// make two Zones at the same position and check one for the Point3D and the other for the Joint.
    /// 
    /// A PolyZone is specified as a list of points. The points are then translated and scaled based upon the position and scale
    /// to the final position that is used for testing.
    /// Final Point = Point * Scale + Position
    /// Ex: points = (0,0), (1,0), (0.5, 1)
    ///     Position = (3,5)
    ///     ScaleX = 2
    ///     ScaleY = 1
    ///     Final Position = (3,5), (5,5), (4,6)
    /// </summary>
    class PolyZone : Zone
    {
        private List<Point> points;

        private double scaleX = 1.0;
        private double scaleY = 1.0;
        
        /// <summary>
        /// Get or set the list of points.
        /// </summary>
        public List<Point> Vertecies { get { return points; } set { pointCollectionIsDirty = true; BuildZone(value); } }
        /// <summary>
        /// Get or set the width of the zone (along the X axis).
        /// </summary>
        public override double Width { get { return width * scaleX; } set { pointCollectionIsDirty = true; ChangeWidth(value); } }
        /// <summary>
        /// Get or set the height of the zone (along the Y axis).
        /// </summary>
        public override double Height { get { return height * scaleY; } set { pointCollectionIsDirty = true; ChangeHeight(value); } }

        /// <summary>
        /// Get or set the scale along the x axis.
        /// </summary>
        public double ScaleX { get { return scaleX; } set { pointCollectionIsDirty = true;  ChangeScale(value, scaleY); } }

        /// <summary>
        /// Get or set the scale along the y axis.
        /// </summary>
        public double ScaleY { get { return scaleY; } set { pointCollectionIsDirty = true;  ChangeScale(scaleX, value); } }

        /// <summary>
        /// Rectangle created from the X, Y, Width, Height of the zone.
        /// </summary>
        public override Int32Rect Rectangle
        {
            get
            {
                return new Int32Rect((int)this.Location.X, (int)this.Location.Y,
                    (int)(this.width * ScaleX), (int)(this.Height * scaleY));
            }
        }

        /// <summary>
        /// Get a point collection. intended to be used for displaying the zone.
        /// </summary>
        public override System.Windows.Media.PointCollection Points
        {
            get
            {
                if (pointCollectionIsDirty)
                {
                    pointCollection = new System.Windows.Media.PointCollection(points.Count);
                    foreach (Point p in points)
                    {
                        pointCollection.Add(new Point(p.X * scaleX + location.X, p.Y * scaleY + location.Y));
                    }
                    pointCollection.Freeze();

                    pointCollectionIsDirty = false;

                    return pointCollection;
                }
                else
                {
                    return pointCollection;
                }

            }
        }
        
        /// <summary>
        /// Creates a zone based on a list of points. The list must have at least 3 points to be able to make a polygon.
        /// </summary>
        /// <param name="points">The points either clockwise or counter-clockwise around the polygon.</param>
        public PolyZone(List<Point> points)
        {
            // Check to make sure the passed in points can build a polygon
            if (points != null && points.Count > 2)
            {
                BuildZone(points);
            }
            else
            {
                isImproperZone = true;
            }
        }

        public PolyZone(List<Point> points, double z, double depth) : this(points)
        {
            location.Z = z;
            this.depth = depth;
        }

        /// <summary>
        /// Changes the height and updates the scale acordingly 
        /// </summary>
        /// <param name="newHeight">New height of the polygon. Should be greater than zero</param>
        private double ChangeHeight(double newHeight)
        {
            if (height != 0)
            {
                scaleY = newHeight / height;
                return newHeight;
            }
            else
            {
                return 0;
            }            
        }

        /// <summary>
        /// Changes the width and updates the scale acordingly
        /// </summary>
        /// <param name="newWidth">New width of the polygon. Should be greater than zero</param>
        private double ChangeWidth(double newWidth)
        {
            if (width != 0)
            {
                scaleX = newWidth / width;
                return newWidth;
            }
            else
            {
                return 0;
            }            
        }

        /// <summary>
        /// Changes the scale and updates the height/width acordingly
        /// </summary>
        /// <param name="scaleX"></param>
        /// <param name="scaleY"></param>
        private void ChangeScale(double scaleX, double scaleY)
        {
            this.scaleX = scaleX;
            this.scaleY = scaleY;
        }

        /// <summary>
        /// Builds the zone from the list of points. It removes the last point if it is the same as the first point.
        /// Also builds the bounding box and sets the location to the top left corner of the bounding box
        /// </summary>
        private void BuildZone(List<Point> points)
        {
            this.points = new List<Point>(points);

            // If the first and last point are the same, remove the last one.
            if (points.First().X == points.Last().X &&
                points.First().Y == points.Last().Y)
            {
                this.points.RemoveAt(points.Count - 1);
            }

            // Build the bounding box
            double minX = points[0].X;
            double minY = points[0].Y;
            double maxX = points[0].X;
            double maxY = points[0].Y;

            foreach (Point p in points)
            {
                minX = System.Math.Min(minX, p.X);
                minY = System.Math.Min(minY, p.Y);

                maxX = System.Math.Max(maxX, p.X);
                maxY = System.Math.Max(maxY, p.Y);
            }

            this.width = maxX - minX;
            this.height = maxY - minY;
        }
        
        public override bool IsPoint2DInside(Point point)
        {
            // The number of times the ray intersects the polygon to the left of the point being checked
            bool oddNodes = false;

            // count the intersections along the lines. Start with the line between the last point and the first.
            Point p1;
            Point p2 = new Point(points[points.Count - 1].X * scaleX + location.X, points[points.Count - 1].Y * scaleY + location.Y);

            for (int i = 0; i < points.Count; i++)
            {
                p1 = new Point(points[i].X * scaleX + location.X, points[i].Y * scaleY + location.Y);

                // Check if a ray cast at y = point.y will intersect the line segment
                if (p1.Y < point.Y && p2.Y >= point.Y || p2.Y < point.Y && p1.Y >= point.Y)
                {
                    // If the intersection on the ray is < x value of point, then count it
                    if (p1.X + (p2.X - p1.X) * (point.Y - p1.Y) / (p2.Y - p1.Y) < point.X)
                        oddNodes = !oddNodes; // alternate between even and odd
                }

                p2 = p1;
            }

            // if the ray intersected and odd number of time, the point is inside the polygon
            return oddNodes;
        }

        /// <returns></returns>
        public override string ToString()
        {
            //return String.Format("{0}\r{1}\r{2}", String.Format("{0}:", (!String.IsNullOrWhiteSpace(zoneName) ? zoneName : zoneType == ZoneType.None ? "Zone" : zoneType.ToString())), String.Format("({0:#.###},{1:#.###},{2:#.###})", location.X, location.Y, location.Z), String.Format(" W:{0:#.###}, H:{1:#.###}, D:{2:#.###})", width, height, depth));
            return String.Format("{0}\n{1:#.##}\n{2:0.##}", String.Format("{0}:", (!String.IsNullOrWhiteSpace(name) ? name : "Zone")),
                "Total: " + this.TotalTimeWithinZone.TotalSeconds,
                "Current: " + this.TimeWithinZone.TotalSeconds);
        }
    }
}
