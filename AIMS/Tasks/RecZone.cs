﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using Microsoft.Kinect;
using System.Windows;
using System.Diagnostics;

namespace AIMS
{
    /// <summary>
    /// A Zone is a class object which represents a region in 3D space.
    /// 
    /// A Zone can calculate whether or not a Point, Point3D, or Joint is within it's 3D-Cube-region.
    /// 
    /// A Zone will track the total amount of time a point has been found within the Zone (TotalTimeWithinZone),
    /// as well as the length of time the current timespan has been within the zone (TimeWithinZone).
    /// 
    /// Every zone has it's own meaning and purpose. 
    /// 
    /// If you want to check both a Point3D and a Joint within the same position,
    /// make two Zones at the same position and check one for the Point3D and the other for the Joint.
    /// </summary>
    public class RecZone : Zone
    {
        /// <summary>
        /// Get or set the width of the zone (along the X axis).
        /// </summary>
        public override double Width { get { return width; } set { pointCollectionIsDirty = true;  width = value; } }
        /// <summary>
        /// Get or set the height of the zone (along the Y axis).
        /// </summary>
        public override double Height { get { return height; } set { pointCollectionIsDirty = true; height = value; } }

        public override Int32Rect Rectangle { get { return new Int32Rect((int)this.X, (int)this.Y, (int)this.Width, (int)this.Height); } }

        public RecZone()
        {
        }

        public RecZone(Point3D location)
            : this()
        {
            this.location = location;
        }

        /// Get a point collection. intended to be used for displaying the zone.
        /// </summary>
        public override System.Windows.Media.PointCollection Points
        {
            get
            {
                if (pointCollectionIsDirty)
                {
                    pointCollection = new System.Windows.Media.PointCollection(4);
                    pointCollection.Add(new Point(location.X, location.Y));
                    pointCollection.Add(new Point(location.X + width, location.Y));
                    pointCollection.Add(new Point(location.X + width, location.Y + height));
                    pointCollection.Add(new Point(location.X, location.Y + height));

                    pointCollectionIsDirty = false;

                    return pointCollection;
                }
                else
                {
                    return pointCollection;
                }

            }
        }

        /// <summary>
        /// Returns true if the point passed into the method are within the zone, using the zone's diminsions.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public override bool IsPoint2DInside(Point point)
        {
            System.Diagnostics.Debug.WriteLine("RecZone-> IsPoint2DInside - Checking if point is inside: point=" + point.X + "," + point.Y + " to " + location.X + "," + location.Y);
            if (point.X > location.X && point.Y > location.Y)
            {
                if (point.X < location.X + this.width && point.Y < location.Y + this.height)
                {
                    return true;
                }
            }
            
            return false;
        }

        /// <returns></returns>
        public override string ToString()
        {
            //return String.Format("{0}\r{1}\r{2}", String.Format("{0}:", (!String.IsNullOrWhiteSpace(zoneName) ? zoneName : zoneType == ZoneType.None ? "Zone" : zoneType.ToString())), String.Format("({0:#.###},{1:#.###},{2:#.###})", location.X, location.Y, location.Z), String.Format(" W:{0:#.###}, H:{1:#.###}, D:{2:#.###})", width, height, depth));
            return String.Format("{0}\n{1:#.##}\n{2:0.##}", String.Format("{0}:", (!String.IsNullOrWhiteSpace(name) ? name : "Zone")),
                "Total: " + this.TotalTimeWithinZone.TotalSeconds,
                "Current: " + this.TimeWithinZone.TotalSeconds);
        }
    }
}
