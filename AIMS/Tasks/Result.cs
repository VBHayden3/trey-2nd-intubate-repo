﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using System.Windows;

namespace AIMS
{
    /// <summary>
    /// Result is a class object which is represents the overall result of a Lesson.
    /// This is calculated by the lesson, and is stored within the database - which can later be pulled and reviewed per request.
    /// </summary>
    public class Result
    {
        private int id;
        private int studentID;
        private LessonType lessonType = LessonType.Intubation;
        private TaskMode taskMode = TaskMode.Practice;
        private MasteryLevel masteryLevel = MasteryLevel.Novice;
        private DateTime date;
        private TimeSpan totalTime;
        private double totalScore;
        private List<StageScore> stageScores;
        private bool passed;

        public int ID { get { return id; } set { id = value; } }
        public int StudentID { get { return studentID; } set { studentID = value; } }
        public DateTime Date { get { return date; } set { date = value; } }
        public LessonType LessonType { get { return lessonType; } set { lessonType = value; } }
        public TaskMode TaskMode { get { return taskMode; } set { taskMode = value; } }
        public MasteryLevel MasteryLevel { get { return masteryLevel; } set { masteryLevel = value; } }
        public TimeSpan TotalTime { get { return totalTime; } set { totalTime = value; } }
        public double TotalScore { get { return totalScore; } set { totalScore = value; } }
        public bool Passed { get { return passed; } set { passed = value; } }
        public List<StageScore> StageScores { get { return stageScores; } set { stageScores = value; } }

        public Result()
        {
            stageScores = new List<StageScore>();
        }

        public Result(int resultID, int studentID, LessonType lessonType, TaskMode taskMode, MasteryLevel masteryLevel, DateTime date, bool passed, TimeSpan totaltime, double totalscore)
            : base()
        {
            this.ID = resultID;
            this.StudentID = studentID;
            this.LessonType = lessonType;
            this.TaskMode = taskMode;
            this.MasteryLevel = masteryLevel;
            this.Date = date;
            this.Passed = passed;
            this.TotalTime = totaltime;
            this.TotalScore = totalscore;
        }

        public static Result GetResultFromResultID(int resultid, bool gatherStagescores, bool gatherScreenshots)
        {
            if( resultid <= 0 )
                return null;

            DatabaseManager.OpenConnection();

            ////MessageBox.Show( String.Format( "CurrentUserID: {0}, CurrentTask#: {1}", ((App)App.Current).CurrentUserID, (int)((App)App.Current).CurrentTask ) );
            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            
            command.CommandText = "SELECT studentid, tasktype, taskmode, masterylevel, date, passed, totalscore, totaltime FROM student_results where uid = :resultid";
            command.Parameters.Add( "resultid", NpgsqlTypes.NpgsqlDbType.Integer ).Value = resultid;
            
            NpgsqlDataReader reader = command.ExecuteReader();

            int studentid = 0;
            int lessontypeint = 0;
            int taskmodeint = 0;
            int masterylevelint = 0;
            DateTime date = DateTime.MinValue;
            bool passed = false;
            NpgsqlTypes.NpgsqlInterval totaltime = TimeSpan.Zero;
            double totalscore = 0;

            while(reader.Read())
            {
                studentid = reader.GetInt32(reader.GetOrdinal("studentid"));
                lessontypeint = reader.GetInt32( reader.GetOrdinal("tasktype") );
                taskmodeint = reader.GetInt32( reader.GetOrdinal("taskmode"));
                masterylevelint = reader.GetInt32( reader.GetOrdinal("masterylevel"));
                date = reader.GetTimeStamp( reader.GetOrdinal( "date" ));
                passed = reader.GetBoolean( reader.GetOrdinal("passed"));
                totalscore = reader.GetDouble( reader.GetOrdinal("totalscore"));

                try
                {
                    totaltime = (TimeSpan)reader.GetInterval(reader.GetOrdinal("totaltime"));
                }
                catch( Exception )
                {
                }
            }

            Result result = new Result(resultid, studentid, (LessonType)lessontypeint, (TaskMode)taskmodeint, (MasteryLevel)masterylevelint, date, passed, (TimeSpan)totaltime, totalscore);

            reader.Dispose();

            DatabaseManager.CloseConnection();

            if (gatherStagescores)
            {
                DatabaseManager.OpenConnection();

                command = DatabaseManager.Connection.CreateCommand();
                command.CommandText = "SELECT uid, stagescore FROM student_results_stage_scores WHERE resultid = :resultnumber";
                command.Parameters.Add("resultnumber", NpgsqlTypes.NpgsqlDbType.Integer).Value = resultid;

                NpgsqlDataReader sqlresult = command.ExecuteReader();

                List<StageScore> stageScoreList = new List<StageScore>();

                int uid;
                byte[] stagescore = null;

                try
                {
                    while (sqlresult.Read())
                    {
                        uid = sqlresult.GetInt32(sqlresult.GetOrdinal("uid"));
                        stagescore = (byte[])sqlresult.GetValue(sqlresult.GetOrdinal("stagescore"));

                        StageScore stageScore = null;

                        try
                        {
                            stageScore = ByteArrayToObject(stagescore) as StageScore;   // deserialize the stageScore byteArray data read from the db.
                        }
                        catch
                        {
                            //MessageBox.Show(exception.ToString());  // error deserializing..
                        }

                        if (stageScore != null)    // if the stage score exists
                        {
                            stageScore.ID = uid;
                            stageScoreList.Add(stageScore); // add it to the list of stage scores.
                        }
                    }
                }
                catch (Exception e)
                {
                    
                }

                DatabaseManager.CloseConnection();

                if (result.StageScores == null)
                    result.StageScores = new List<StageScore>();

                foreach (StageScore ss in stageScoreList)
                {
                    result.StageScores.Add(ss);
                }

                if (gatherScreenshots)
                {
                    foreach (StageScore ss in result.StageScores)
                    {
                        if (ss.HasScreenshot && ss.ID > 0)
                        {
                            DatabaseManager.OpenConnection();

                            command = DatabaseManager.Connection.CreateCommand();
                            command.CommandText = String.Format("SELECT uid, coloranddepthdata FROM student_results_3dscreenshots WHERE stagescoreid = :scoreID");
                            command.Parameters.Add("scoreID", NpgsqlTypes.NpgsqlDbType.Integer).Value = ss.ID;

                            reader = command.ExecuteReader();

                            int screenshotID;
                            byte[] coloranddepthdata = null;

                            try
                            {
                                while (reader.Read())
                                {
                                    screenshotID = reader.GetInt32(reader.GetOrdinal("uid"));
                                    coloranddepthdata = (byte[])reader.GetValue(reader.GetOrdinal("coloranddepthdata"));

                                    StageScore.Screenshot screenshot = null;

                                    try
                                    {
                                        screenshot = ByteArrayToObject(coloranddepthdata) as StageScore.Screenshot;   // deserialize the stageScore byteArray data read from the db.
                                    }
                                    catch (Exception exception)
                                    {
                                        //MessageBox.Show(exception.ToString());  // error deserializing..
                                    }

                                    if (screenshot != null)    // if the stage score exists
                                    {
                                        ss.ScreenShot = screenshot;
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                //MessageBox.Show(e.ToString());
                            }

                            DatabaseManager.CloseConnection();
                        }
                    }
                }
            }



            return result;
        }

        /// <summary>
        /// Function to get object from byte array
        /// </summary>
        /// <param name="_ByteArray">byte array to get object</param>
        /// <returns>object</returns>
        public static object ByteArrayToObject(byte[] _ByteArray)
        {
            try
            {
                // convert byte array to memory stream
                System.IO.MemoryStream _MemoryStream = new System.IO.MemoryStream(_ByteArray);

                // create new BinaryFormatter
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter _BinaryFormatter
                            = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                // set memory stream position to starting point
                _MemoryStream.Position = 0;

                // Deserializes a stream into an object graph and return as a object.
                return _BinaryFormatter.Deserialize(_MemoryStream);
            }
            catch (Exception _Exception)
            {
                // Error
                Console.WriteLine("Exception caught in process: {0}", _Exception.ToString());
                ////MessageBox.Show(_Exception.ToString());
            }

            // Error occured, return null
            return null;
        }

        /// <summary>
        /// Clears the Result object back to it's initial values. The individual StageScores are not "Wiped", but the collection itself is cleared and re-instantiated.
        /// </summary>
        public void WipeClean()
        {
            date = DateTime.Now;
            lessonType = LessonType.Intubation;
            taskMode = TaskMode.Practice;
            masteryLevel = MasteryLevel.Novice;
            totalTime = TimeSpan.Zero;
            totalScore = 0;
            passed = true;

            if (stageScores != null)
            {
                stageScores.Clear();
                stageScores = null;
                stageScores = new List<StageScore>();
            }
        }
    }
}
