﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIMS
{
    [Serializable()]
    public class Screenshot
    {
        private byte[] compressedColor;
        private byte[] compressedDepth;

        [field: NonSerialized]
        private int id;
        [field: NonSerialized]
        private string description;
        [field: NonSerialized]
        private DateTime timestamp;

        /// <summary>
        /// ID of the screenshot - this is set by the database. If it's not in the database, it's value is 0.
        /// </summary>
        public int ID { get { return id; } set { id = value; } }
        /// <summary>
        /// Description of the screenshot. This descriptioin is stored inside of the database, and is displayed OnMouseOver.
        /// </summary>
        public string Description { get { return description; } set { description = value; } }
        /// <summary>
        /// DateTime which that screenshot was taken.
        /// </summary>
        public DateTime Timestamp { get { return timestamp; } set { timestamp = value; } }

        public Screenshot()
        {
        }

        /// <summary>
        /// This constructor of Screenshot accepts "raw"-uncompressed colorData and depthData and then converts them into the Compressed format before storing them.
        /// </summary>
        /// <param name="colorData"></param>
        /// <param name="depthData"></param>
        public Screenshot(byte[] colorData, ushort[] depthData)
        {
            this.colorData = colorData; // Setting this as a property will take the uncompressed colorData and compress it into the compressedColor feild.
            this.depthData = depthData; // Setting this as a property will take the uncompressed depthData and compress it into the compressedDepth feild.
        }

        public byte[] colorData
        {
            get { return Compressor.Compressor.Decompress(compressedColor); }
            set { compressedColor = Compressor.Compressor.Compress(value); }
        }

        public ushort[] depthData
        {
            get
            {
                byte[] uncompressedBytes = Compressor.Compressor.Decompress(compressedDepth);

                ushort[] shorts = new ushort[uncompressedBytes.Length / 2];
                for (int i = 0; i < shorts.Length; i++)
                {
                    shorts[i] = (ushort)((((int)uncompressedBytes[2 * i]) << 8) + uncompressedBytes[2 * i + 1]);
                }

                return shorts;
            }
            set
            {
                ushort[] shorts = value;
                byte[] convertedBytes = new byte[shorts.Length * 2];

                for (int i = 0; i < shorts.Length; i++)
                {
                    convertedBytes[2 * i] = (byte)(shorts[i] >> 8);
                    convertedBytes[2 * i + 1] = (byte)(shorts[i]);
                }

                compressedDepth = Compressor.Compressor.Compress(convertedBytes);
            }
        }

        public void WipeClean()
        {
            this.compressedColor = null;
            this.compressedDepth = null;
        }

        public static Screenshot TakeKinectScreenshot( string description, DateTime timestamp )
        {
            if( KinectManager.Kinect == null )
                return null;

            if (KinectManager.ColorData == null)
                return null;

            if (KinectManager.DepthFrameData == null)
                return null;

            Screenshot screenshot = new Screenshot();

            screenshot.colorData = KinectManager.ColorData;
            screenshot.depthData = KinectManager.DepthFrameData;

            screenshot.Description = description;
            screenshot.Timestamp = timestamp;

            return screenshot;
        }
    }
}
