﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Kinect;

namespace AIMS
{
    /// <summary>
    /// A custom class which links onto a Kinect Skeleton and adds onto the data which is already provided.
    /// 
    /// The SkeletonProfile stores:
    /// * Previous postion.
    /// * Analytics based upon the change in previous position
    ///     - Displacement
    ///         - Magnitude of displacement.
    ///         - Direction of displacement.
    /// * Path over time.
    /// 
    /// The SkeletonProfile can also persist over time: 
    /// If a Kinect Skeleton is lost temporarily, and a new Kinect Skeleton is found in the same location,
    /// the SkeletonProfile profile may be able to attach onto the new Kinect Skeleton and continue on.
    /// </summary>
    public class SkeletonProfile : IDisposable
    {
        //private int? linkedSkeletonId;
        private List<JointProfile> trackedJoints;
        private CameraSpacePoint currentPosition;
        private CameraSpacePoint previousPosition;
        private bool currentTrackingState;
        private bool previousTrackingState;
        private ulong? currentTrackingID;
        private ulong? previousTrackingID;
        private FrameEdges currentClippedEdges;
        private FrameEdges previousClippedEdges;
        /* @todo update if used
        private BoneOrientationCollection currentBoneOrientations;
        private BoneOrientationCollection previousBoneOrientations;
        */
        private bool isPrimary = false;
        private bool isSticky = false;
        private int timeToLive = 30;   // Time To Live
        private DateTime currentTimestamp;  // Time of current frame update (used for obtaining time delta).
        private DateTime previousTimestamp; // Time of last frame update (used for obtaining time delta).

        private MovementAnalysis movementAnalysis;

        public MovementAnalysis MovementAnalysis { get { return movementAnalysis; } set { movementAnalysis = value; } }

        /// <summary>
        /// Gets or sets a list of JointProfile objects representing each JointType in the JointType enum for the current skeleton.
        /// </summary>
        public List<JointProfile> JointProfiles { get { return trackedJoints; } set { trackedJoints = value; } }
        
        /// <summary>
        /// Gets or sets the current 3D point of the linked skeleton in skeletal space.
        /// Setting this value will first set the PreviousPosition to the CurrentPosition.
        /// </summary>
        public CameraSpacePoint CurrentPosition
        {
            get { return currentPosition; } 
            set 
            {
                this.PreviousPosition = currentPosition;
                currentPosition = value;
            } }
        /// <summary>
        /// Gets the 3D point of the linked skeleton in skeletal space from the previous frame.
        /// </summary>
        public CameraSpacePoint PreviousPosition { get { return previousPosition; } private set { previousPosition = value; } }
        /// <summary>
        /// Gets or sets the current tracking state of the linked skeleton.
        /// Setting this value will first set the PreviousTrackingState to the CurrentTrackingState.
        /// </summary>
        public bool CurrentTrackingState { get { return currentTrackingState; } 
            set 
            {
                this.PreviousTrackingState = currentTrackingState;
                currentTrackingState = value;
            } }
        /// <summary>
        /// Gets the tracking state of the linked skeleton from the previous frame.
        /// </summary>
        public bool PreviousTrackingState { get { return previousTrackingState; } private set { previousTrackingState = value; } }
        /// <summary>
        /// Gets or sets the current trackingID# of the linked skeleton.
        /// Setting this value will first set the PreviousTrackingID to the CurrentTrackingID.
        /// </summary>
        public ulong? CurrentTrackingID { get { return currentTrackingID; } 
            set 
            {
                this.PreviousTrackingID = currentTrackingID;
                currentTrackingID = value;
            } }
        /// <summary>
        /// Gets the trackingID# of the linked skeleton from the previous frame.
        /// </summary>
        public ulong? PreviousTrackingID { get { return previousTrackingID; } private set { previousTrackingID = value; } }
        /// <summary>
        /// Gets or sets the current clipped edges of the linked skeleton.
        /// Setting this value will first set the PreviousClippedEdges to the CurrentClippedEdges.
        /// </summary>
        public FrameEdges CurrentClippedEdges { get { return currentClippedEdges; } 
            set 
            {
                this.PreviousClippedEdges = currentClippedEdges;
                currentClippedEdges = value; 
            } }
        /// <summary>
        /// Gets the clipped edges of the linked skeleton from the previous frame.
        /// </summary>
        public FrameEdges PreviousClippedEdges { get { return previousClippedEdges; } private set { previousClippedEdges = value; } }

        /// <summary>
        /// Gets or sets the current BoneOrientationCollection of the linked skeleton.
        /// Setting this value will first set the PreviousBoneOrientations to the CurrentBoneOrientations.
        /// </summary>
        /* @todo update
        public BoneOrientationCollection CurrentBoneOrientations { get { return currentBoneOrientations; } 
            set 
            {
                this.PreviousBoneOrientations = currentBoneOrientations;
                currentBoneOrientations = value; 
            } }
        
        /// <summary>
        /// Gets the BoneOrientationCollection of the linked skeleton from the previous frame.
        /// </summary>
        public BoneOrientationCollection PreviousBoneOrientations { get { return previousBoneOrientations; } private set { previousBoneOrientations = value; } }
         */
        /// <summary>
        /// Gets or sets the timestamp of the current skeleton update.
        /// Setting this value will first set the PreviousTimestamp to the CurrentTimestamp.
        /// </summary>
        public DateTime CurrentTimestamp { get { return currentTimestamp; } 
            set 
            {
                this.PreviousTimestamp = currentTimestamp;
                currentTimestamp = value; 
            } }
        /// <summary>
        /// Gets the timestamp of the skeleton update of the previous frame.
        /// This value is used in obtaining the time delta between updates.
        /// </summary>
        public DateTime PreviousTimestamp { get { return previousTimestamp; } private set { previousTimestamp = value; } }

        public bool IsSticky { get { return isSticky; } set { isSticky = value; } }
        public bool IsPrimary { get { return isPrimary; } set { isPrimary = value; } }
        /// <summary>
        /// Indicates the frames remaining before the SkeletonProfile is destroyed.
        /// This value decrements at a rate of 1 per frame update until it reaches 0.
        /// 
        /// TTL will only decrement if the skeleton linked to the skeleton is lost.
        /// This gives the scene 30 (#NUM_TTL#) frames to re-attach the SkeletonProfile 
        /// to a new skeleton to maintain persistence before it is lost.
        /// </summary>
        public int TimeToLive { get { return timeToLive; } set { timeToLive = value; } }

        public delegate void OnProfileDestroyHandler(object sender, OnProfileDestroyEventArgs args);
        public event OnProfileDestroyHandler OnProfileDestroy;

        public class OnProfileDestroyEventArgs : EventArgs
        {
            public OnProfileDestroyEventArgs()
            {

            }
        }

        public bool Disposed { get { return disposed; } }

        public SkeletonProfile( ulong? skeletonid )
        {
            this.CurrentTrackingID = skeletonid;

            trackedJoints = new List<JointProfile>();

            foreach (JointType type in Enum.GetValues(typeof(JointType)))
            {
                trackedJoints.Add(new JointProfile(type, this)); // Add all of the Joints.
            }

            if (this.CurrentTrackingID.HasValue)
            {
                Body linkedSkele = KinectManager.GetSkeleton((ulong)this.CurrentTrackingID, true);
                if (linkedSkele != null)
                {
                    UpdateSkeletonData(linkedSkele);
                }
            }
        }

        ~SkeletonProfile()
        {
            Dispose(false);
        }

     
        public void UpdateSkeletonData( Body skele )
        {
            if (skele != null)
            {
                this.CurrentTimestamp = DateTime.Now;
                this.CurrentPosition = skele.Joints[JointType.SpineMid].Position;
                this.CurrentTrackingID = skele.TrackingId;
                this.CurrentTrackingState = skele.IsTracked;
                this.CurrentClippedEdges = skele.ClippedEdges;
                //this.CurrentBoneOrientations = skele.BoneOrientations;

                this.MovementAnalysis = MovementAnalysis.ComparePoints(this.PreviousPosition, this.CurrentPosition);

                foreach (JointProfile trackedJoint in this.JointProfiles)
                {
                    trackedJoint.UpdateJointData( skele );
                }
            }
        }
        

        #region Disposing
        private bool disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    #region dispose cleanup

                    if (JointProfiles != null)
                    {
                        foreach (JointProfile prof in JointProfiles)
                        {
                            prof.Dispose();
                        }

                        JointProfiles = null;
                    }

                    /* @todo update
                    if (CurrentBoneOrientations != null)
                    {
                        this.CurrentBoneOrientations = null;
                    }
                    if (previousBoneOrientations != null)
                    {
                        this.PreviousBoneOrientations = null;
                    }
                    */

                    #endregion dispose cleanup

                    disposed = true;
                }
            }
        }
        #endregion Disposing
    }
}
