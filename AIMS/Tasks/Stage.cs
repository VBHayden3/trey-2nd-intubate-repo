﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Windows;

namespace AIMS
{
    public abstract class Stage : DependencyObject
    {
        /*
        protected int stageNumber;
        protected DateTime startTime;
        protected DateTime endTime;
        protected StageScore stageScore;
        protected string imagePath;
        protected string instruction;
        protected string name;
        protected bool active;
        protected bool complete;
        protected TimeSpan targetTime;
        protected double scoreWeight;
        protected List<Zone> zones;
        protected bool isSetup;
        protected Stopwatch stopwatch;
        */

        /// <summary>
        /// The number of the stage within the Lesson.
        /// </summary>
        public int StageNumber { get { return (int)this.GetValue(StageNumberProperty); } set { this.SetValue(StageNumberProperty, value); } }
        public static readonly DependencyProperty StageNumberProperty = DependencyProperty.RegisterAttached("StageNumber", typeof(int), typeof(Stage), new FrameworkPropertyMetadata(0));
        /// <summary>
        /// Time of which the stage is started.
        /// </summary>
        public DateTime StartTime { get { return (DateTime)this.GetValue(StartTimeProperty); } set { this.SetValue(StartTimeProperty, value); } }
        public static readonly DependencyProperty StartTimeProperty = DependencyProperty.RegisterAttached("StartTime", typeof(DateTime), typeof(Stage), new FrameworkPropertyMetadata(DateTime.MinValue));
        /// <summary>
        /// Time of which the stage is ended.
        /// </summary>
        public DateTime EndTime { get { return (DateTime)this.GetValue(EndTimeProperty); } set { this.SetValue(EndTimeProperty, value); } }
        public static readonly DependencyProperty EndTimeProperty = DependencyProperty.RegisterAttached("EndTime", typeof(DateTime), typeof(Stage), new FrameworkPropertyMetadata(DateTime.MaxValue));
        /// <summary>
        /// The linked StageScore object which stores the stages individual result.
        /// </summary>
        public StageScore StageScore { get { return (StageScore)this.GetValue(StageScoreProperty); } set { this.SetValue(StageScoreProperty, value); } }
        public static readonly DependencyProperty StageScoreProperty = DependencyProperty.RegisterAttached("StageScore", typeof(StageScore), typeof(Stage), new FrameworkPropertyMetadata(null));
        /// <summary>
        /// The relative string path to the Image representing the stage.
        /// </summary>
        public string ImagePath { get { return (string)this.GetValue(ImagePathProperty); } set { this.SetValue(ImagePathProperty, value); } }
        public static readonly DependencyProperty ImagePathProperty = DependencyProperty.RegisterAttached("ImagePath", typeof(string), typeof(Stage), new FrameworkPropertyMetadata(null));
        /// <summary>
        /// The written instruction for the stage.
        /// </summary>
        public string Instruction { get { return (string)this.GetValue(InstructionProperty); } set { this.SetValue(InstructionProperty, value); } }
        public static readonly DependencyProperty InstructionProperty = DependencyProperty.RegisterAttached("Instruction", typeof(string), typeof(Stage), new FrameworkPropertyMetadata(null));
        /// <summary>
        /// The minimalistic name of the stage.
        /// </summary>
        public string Name { get { return (string)this.GetValue(NameProperty); } set { this.SetValue(NameProperty, value); } }
        public static readonly DependencyProperty NameProperty = DependencyProperty.RegisterAttached("Name", typeof(string), typeof(Stage), new FrameworkPropertyMetadata(null));
        /// <summary>
        /// Whether or not the stage is active. Active stages are available for updates.
        /// </summary>
        public bool Active { get { return (bool)this.GetValue(ActiveProperty); } set { this.SetValue(ActiveProperty, value); } }
        public static readonly DependencyProperty ActiveProperty = DependencyProperty.RegisterAttached("Active", typeof(bool), typeof(Stage), new FrameworkPropertyMetadata(false));
        /// <summary>
        /// Whether or not the stage has been completed. Completed stages no longer will update. Stages are completed either by passing stage checks, or timing out.
        /// </summary>
        public bool Complete { get { return (bool)this.GetValue(CompleteProperty); } set { this.SetValue(CompleteProperty, value); } }
        public static readonly DependencyProperty CompleteProperty = DependencyProperty.RegisterAttached("Complete", typeof(bool), typeof(Stage), new FrameworkPropertyMetadata(false));
        /// <summary>
        /// The target TimeSpan which the stage is expected to take at Mastery level.
        /// </summary>
        public TimeSpan TargetTime { get { return (TimeSpan)this.GetValue(TargetTimeProperty); } set { this.SetValue(TargetTimeProperty, value); } }
        public static readonly DependencyProperty TargetTimeProperty = DependencyProperty.RegisterAttached("TargetTime", typeof(TimeSpan), typeof(Stage), new FrameworkPropertyMetadata(TimeSpan.Zero));
        /// <summary>
        /// The weight of the stage's score when totaled and calculated into the entire lesson's Result.
        /// </summary>
        public double ScoreWeight { get { return (double)this.GetValue(ScoreWeightProperty); } set { this.SetValue(ScoreWeightProperty, value); } }
        public static readonly DependencyProperty ScoreWeightProperty = DependencyProperty.RegisterAttached("ScoreWeight", typeof(double), typeof(Stage), new FrameworkPropertyMetadata(0.0));
        /// <summary>
        /// List of used within the stage.
        /// </summary>
        public List<Zone> Zones { get { return (List<Zone>)this.GetValue(ZonesProperty); } set { this.SetValue(ZonesProperty, value); } }
        public static readonly DependencyProperty ZonesProperty = DependencyProperty.RegisterAttached("Zones", typeof(List<Zone>), typeof(Stage), new FrameworkPropertyMetadata(null));
        /// <summary>
        /// Flag to determine whether the setup method of the stage has been called.
        /// </summary>
        public bool IsSetup { get { return (bool)this.GetValue(IsSetupProperty); } set { this.SetValue(IsSetupProperty, value); } }
        public static readonly DependencyProperty IsSetupProperty = DependencyProperty.RegisterAttached("IsSetup", typeof(bool), typeof(Stage), new FrameworkPropertyMetadata(false));
        /// <summary>
        /// The stage's Stopwatch object which keeps track of elapsed time.
        /// </summary>
        public Stopwatch Stopwatch { get { return (Stopwatch)this.GetValue(StopwatchProperty); } set { this.SetValue(StopwatchProperty, value); } }
        public static readonly DependencyProperty StopwatchProperty = DependencyProperty.RegisterAttached("Stopwatch", typeof(Stopwatch), typeof(Stage), new FrameworkPropertyMetadata(null));


        /// <summary>
        /// Flag to determine whether the stage can time out. Essential stages - such as Callibration - shouldn't time out.
        /// </summary>
        public bool CanTimeOut { get { return (bool)this.GetValue(CanTimeOutProperty); } set { this.SetValue(CanTimeOutProperty, value); } }
        public static readonly DependencyProperty CanTimeOutProperty = DependencyProperty.RegisterAttached("CanTimeOut", typeof(bool), typeof(Stage), new FrameworkPropertyMetadata(false));

        /// <summary>
        /// Flag to determine whether the stage has timed out. This flag can then be checked by other stages, or during result calculation - for example.
        /// </summary>
        public bool TimedOut { get { return (bool)this.GetValue(TimedOutProperty); } set { this.SetValue(TimedOutProperty, value); } }
        public static readonly DependencyProperty TimedOutProperty = DependencyProperty.RegisterAttached("TimedOut", typeof(bool), typeof(Stage), new FrameworkPropertyMetadata(false));

        /// <summary>
        /// The stage's Stopwatch object which keeps track of elapsed time.
        /// </summary>
        public Stopwatch TimeoutStopwatch { get { return (Stopwatch)this.GetValue(TimeoutStopwatchProperty); } set { this.SetValue(TimeoutStopwatchProperty, value); } }
        public static readonly DependencyProperty TimeoutStopwatchProperty = DependencyProperty.RegisterAttached("TimeoutStopwatch", typeof(Stopwatch), typeof(Stage), new FrameworkPropertyMetadata(null));

        /// <summary>
        /// The time after which the stage will time out.
        /// </summary>
        public TimeSpan TimeOutTime { get { return (TimeSpan)this.GetValue(TimeOutTimeProperty); } set { this.SetValue(TimeOutTimeProperty, value); } }
        public static readonly DependencyProperty TimeOutTimeProperty = DependencyProperty.RegisterAttached("TimeOutTime", typeof(TimeSpan), typeof(Stage), new FrameworkPropertyMetadata(TimeSpan.Zero));

        public List<BaseWatcher> CurrentStageWatchers { get { return (List<BaseWatcher>)this.GetValue(CurrentStageWatchersProperty); } set { this.SetValue(CurrentStageWatchersProperty, value); } }
        public static readonly DependencyProperty CurrentStageWatchersProperty = DependencyProperty.RegisterAttached("CurrentStageWatchers", typeof(List<BaseWatcher>), typeof(Stage), new FrameworkPropertyMetadata(null));

        public List<BaseWatcher> PeripheralStageWatchers { get { return (List<BaseWatcher>)this.GetValue(PeripheralStageWatchersProperty); } set { this.SetValue(PeripheralStageWatchersProperty, value); } }
        public static readonly DependencyProperty PeripheralStageWatchersProperty = DependencyProperty.RegisterAttached("PeripheralStageWatchers", typeof(List<BaseWatcher>), typeof(Stage), new FrameworkPropertyMetadata(null));

        public List<BaseWatcher> OutlyingStageWatchers { get { return (List<BaseWatcher>)this.GetValue(OutlyingStageWatchersProperty); } set { this.SetValue(OutlyingStageWatchersProperty, value); } }
        public static readonly DependencyProperty OutlyingStageWatchersProperty = DependencyProperty.RegisterAttached("OutlyingStageWatchers", typeof(List<BaseWatcher>), typeof(Stage), new FrameworkPropertyMetadata(null));

        public abstract void SetupStage();
        public abstract void UpdateStage();
        public abstract void EndStage( bool playSound = true ); // Default behavior is for a stage to play a sound, if set to false, then the EndStage implementation should end silently. Particularly useful when forcefully ending a stage to keep up the UI.
        public abstract void ResetStage();
        public abstract void PauseStage();
        public abstract void ResumeStage();
        public abstract void WipeClean();

        public abstract void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier);

        /// <summary>
        /// Calls the stage's OnStageTimeout method.
        /// </summary>
        public void TimeoutStage()
        {
            this.OnStageTimeout();
        }

        /// <summary>
        /// This method is called by TimeoutStage. Each stage is responsible for providing its own stage timeout logic.
        /// This may include refusing the timeout completely (if the stage is essential, for instance), or (most likely) various timeout techniques.
        /// </summary>
        public abstract void OnStageTimeout();

        public void RequestStageVideo()
        {
            this.OnStageVideoRequest();
        }

        /// <summary>
        /// This method is called by RequestStageVideo(). Each stage is responsible for providing its own stage help request logic.
        /// This may include refusing to help completely (if it makes sense to do so), or (most likely) various stage help techniques.
        /// </summary>
        public abstract void OnStageVideoRequest();
    }
}
