﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Runtime.Serialization;

namespace AIMS
{
    [Serializable()]
    public class StageScore
    {
        private int stageNumber = 0;
        private string stageName = "";
        private string result = "";
        private string target = "";
        private double score = 0;
        private bool hasScreenshot = false;
        private TimeSpan timeInStage = new TimeSpan();
        private bool passed = false;
        private int id = 0;

        [field: NonSerialized()]
        private Screenshot screenshot;

        public int ID { get { return id; } set { id = value; } }
        public int StageNumber { get { return stageNumber; } set { stageNumber = value; } }
        public string StageName { get { return stageName; } set { stageName = value; } }
        public string Result { get { return result; } set { result = value; } }
        public string Target { get { return target; } set { target = value; } }
        public double Score { get { return score; } set { score = value; } }
        public bool HasScreenshot { get { return hasScreenshot; } set { hasScreenshot = value; } }
        public TimeSpan TimeInStage { get { return timeInStage; } set { timeInStage = value; } }

        public Screenshot ScreenShot { get { return screenshot; } set { screenshot = value; } }

        public void Serialize(BinaryFormatter binaryFormatter, MemoryStream memoryStream)
        {
            binaryFormatter.Serialize(memoryStream, ObjectToByteArray(this));
        }

        public byte[] ObjectToByteArray(object _Object)
        {
            try
            {
                // create new memory stream
                System.IO.MemoryStream _MemoryStream = new System.IO.MemoryStream();

                // create new BinaryFormatter
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter _BinaryFormatter
                            = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                // Serializes an object, or graph of connected objects, to the given stream.
                _BinaryFormatter.Serialize(_MemoryStream, _Object);

                // convert stream to byte array and return
                return _MemoryStream.ToArray();
            }
            catch (Exception _Exception)
            {
                // Error
                Console.WriteLine("Exception caught in process: {0}", _Exception.ToString());
                MessageBox.Show(_Exception.ToString());
            }

            // Error occured, return null
            return null;
        }

        /// <summary>
        /// Function to get object from byte array
        /// </summary>
        /// <param name="_ByteArray">byte array to get object</param>
        /// <returns>object</returns>
        public object ByteArrayToObject(byte[] _ByteArray)
        {
            try
            {
                // convert byte array to memory stream
                System.IO.MemoryStream _MemoryStream = new System.IO.MemoryStream(_ByteArray);

                // create new BinaryFormatter
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter _BinaryFormatter
                            = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                // set memory stream position to starting point
                _MemoryStream.Position = 0;

                // Deserializes a stream into an object graph and return as a object.
                return _BinaryFormatter.Deserialize(_MemoryStream);
            }
            catch (Exception _Exception)
            {
                // Error
                Console.WriteLine("Exception caught in process: {0}", _Exception.ToString());
                MessageBox.Show(_Exception.ToString());
            }

            // Error occured, return null
            return null;
        }

        public void WipeClean()
        {
            id = 0;
            stageNumber = 0;
            stageName = null;
            result = null;
            target = null;
            score = 0;
            hasScreenshot = true;
            timeInStage = TimeSpan.Zero;
            passed = true;

            if (screenshot != null)
            {
                screenshot.WipeClean();
                screenshot = null;
            }
        }

        [Serializable()]
        public class Screenshot
        {
            private byte[] compressedColor;
            private byte[] compressedDepth;

            [field: NonSerialized]
            private int id;
            [field: NonSerialized]
            private string description;
            [field: NonSerialized]
            private DateTime timestamp;

            /// <summary>
            /// ID of the screenshot - this is set by the database. If it's not in the database, it's value is 0.
            /// </summary>
            public int ID { get { return id; } set { id = value; } }
            /// <summary>
            /// Description of the screenshot. This descriptioin is stored inside of the database, and is displayed OnMouseOver.
            /// </summary>
            public string Description { get { return description; } set { description = value; } }
            /// <summary>
            /// DateTime which that screenshot was taken.
            /// </summary>
            public DateTime Timestamp { get { return timestamp; } set { timestamp = value; } }

            public Screenshot()
            {
            }

            /// <summary>
            /// This constructor of Screenshot accepts "raw"-uncompressed colorData and depthData and then converts them into the Compressed format before storing them.
            /// </summary>
            /// <param name="colorData"></param>
            /// <param name="depthData"></param>
            public Screenshot(byte[] colorData, ushort[] depthFrameData)
            {
                this.colorData = colorData; // Setting this as a property will take the uncompressed colorData and compress it into the compressedColor feild.
                this.depthData = depthFrameData; // Setting this as a property will take the uncompressed depthData and compress it into the compressedDepth feild.
            }

            public byte[] colorData
            {
                get { return Compressor.Compressor.Decompress(compressedColor); }
                set { compressedColor = Compressor.Compressor.Compress(value); }
            }

            public ushort[] depthData
            {
                get
                {
                    byte[] uncompressedBytes = Compressor.Compressor.Decompress(compressedDepth);

                    ushort[] shorts = new ushort[uncompressedBytes.Length / 2];
                    for (int i = 0; i < shorts.Length; i++)
                    {
                        shorts[i] = (ushort)((((int)uncompressedBytes[2 * i]) << 8) + uncompressedBytes[2 * i + 1]);
                    }

                    return shorts;
                }
                set
                {
                    ushort[] shorts = value;
                    byte[] convertedBytes = new byte[shorts.Length * 2];

                    for (int i = 0; i < shorts.Length; i++)
                    {
                        convertedBytes[2 * i] = (byte)(shorts[i] >> 8);
                        convertedBytes[2 * i + 1] = (byte)(shorts[i]);
                    }

                    compressedDepth = Compressor.Compressor.Compress(convertedBytes);
                }
            }

            public void WipeClean()
            {
                this.compressedColor = null;
                this.compressedDepth = null;
            }

            public static Screenshot TakeKinectScreenshot(string description, DateTime timestamp)
            {
                if (KinectManager.Kinect == null)
                    return null;

                if (KinectManager.ColorData == null)
                    return null;

                if (KinectManager.DepthFrameData == null)
                    return null;

                Screenshot screenshot = new Screenshot();

                screenshot.colorData = KinectManager.ColorData;
                screenshot.depthData = KinectManager.DepthFrameData;

                screenshot.Description = description;
                screenshot.Timestamp = timestamp;

                return screenshot;
            }
        }
    }
}
