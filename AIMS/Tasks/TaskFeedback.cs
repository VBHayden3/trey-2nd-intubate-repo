﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIMS.Tasks
{
    public enum FeedbackType
    {
        None,
        Good,
        Bad,
        Neutral
    }

    public class TaskFeedback
    {
        private const string TASK_FEEDBACK_TABLE_NAME = "taskresults_feedback";
        private const string TASK_ADVICE_TABLE_NAME = "taskresults_advice";

        private int uid = 0;
        private Task taskType = Task.None;
        private string feedback = null;
        private FeedbackType feedbackType = FeedbackType.None;
        //public int? impactID = null;
        public int[] adviceIDs = null;
        public string impact = null;
        public List<TaskAdvice> advice = null;

        public int UID { get { return uid; } set { uid = value; } }
        public Task TaskType { get { return taskType; } set { taskType = value; } }
        public string Feedback { get { return feedback; } set { this.feedback = value; } }
        public FeedbackType FeedbackType { get { return feedbackType; } set { feedbackType = value; } }

        //public int? ImpactID { get { return impactID; } set { impactID = value; } }
        //public int[] AdviceIDs { get { return adviceID; } set { adviceID = value; } }
        public string Impact { get { return impact; } set { impact = value; } }
        public List<TaskAdvice> Advice { get { return advice; } set { advice = value; } }

        public static TaskFeedback GetFeedbackFromID(int uid, Task tasktype)
        {
            if (uid <= 0)
                return null;

            if (tasktype == Task.None)
                return null;

            TaskFeedback feedback = new TaskFeedback();

            feedback.UID = uid;
            feedback.TaskType = tasktype;

            DatabaseManager.OpenConnection();
            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = @"SELECT feedbacktype, feedback FROM " + TASK_FEEDBACK_TABLE_NAME + " WHERE uid = :uid AND tasktype = CAST( :tasktype as \"TaskType\");";

            command.Parameters.Add("uid", NpgsqlTypes.NpgsqlDbType.Integer).Value = uid;
            command.Parameters.Add("tasktype", NpgsqlTypes.NpgsqlDbType.Text).Value = tasktype.ToString();

            NpgsqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                feedback.FeedbackType = (FeedbackType)Enum.Parse(typeof(FeedbackType), reader.GetString(reader.GetOrdinal("feedbacktype")));
                feedback.Feedback = reader.GetString(reader.GetOrdinal("feedback"));
            }

            reader.Dispose();

            DatabaseManager.CloseConnection();

            return feedback;
        }

        public static TaskFeedback[] GetFeedbackFromIDs(List<int> Uids, Task TaskType)
        {
            if (TaskType == Task.None || Uids.Count == 0)
                return null;
            
            TaskFeedback[] feedbacks = new TaskFeedback[Uids.Count];

            DatabaseManager.OpenConnection();
            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;

            // Build query
            string commandText = "SELECT uid, feedbacktype, feedback FROM " + TASK_FEEDBACK_TABLE_NAME + " WHERE uid IN (";

            for (int i = 0; i < Uids.Count; i++)
            {
                if (Uids[i] <= 0)
                    return null;

                commandText += String.Format("'{0}'", Uids[i]);

                if (i < Uids.Count - 1)
                    commandText += ",";
            }

            commandText += ") ORDER BY uid;";
            command.CommandText = commandText;
            
            try
            {
                using (NpgsqlDataReader reader = command.ExecuteReader())
                {
                    int cnt = 0;
                    while (reader.Read())
                    {
                        if (feedbacks.Length > cnt)
                        {
                            feedbacks[cnt] = new TaskFeedback()
                            {
                                UID = reader.GetInt32(reader.GetOrdinal("uid")),
                                FeedbackType = (FeedbackType)Enum.Parse(typeof(FeedbackType), reader.GetString(reader.GetOrdinal("feedbacktype"))),
                                Feedback = reader.GetString(reader.GetOrdinal("feedback"))
                            };
                            cnt++;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print("TaskFeedback GetFeedbackFromIDs ERROR: {0} \n {1}", exc.Message, command.CommandText);
            }

            DatabaseManager.CloseConnection();

            return feedbacks;
        }

        public static List<TaskAdvice> GetAdviceForFeedback(int feedbackID, Task tasktype)
        {
            if (feedbackID <= 0)
                return null;

            List<TaskAdvice> advices = new List<TaskAdvice>();

            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = @"SELECT uid, advice FROM " + TASK_FEEDBACK_TABLE_NAME + " WHERE feedbackid = :feedbackid AND tasktype = ( :tasktype as \"TaskType\");";
            command.Parameters.Add("feedbackid", NpgsqlTypes.NpgsqlDbType.Integer).Value = feedbackID;
            command.Parameters.Add("tasktype", NpgsqlTypes.NpgsqlDbType.Text).Value = tasktype.ToString();

            NpgsqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                TaskAdvice advice = new TaskAdvice();
                
                advice.FeedbackID = feedbackID;
                advice.TaskType = tasktype;
                advice.UID = reader.GetInt32(reader.GetOrdinal("uid"));
                advice.Advice = reader.GetString(reader.GetOrdinal("advice"));

                advices.Add(advice);
            }

            reader.Dispose();

            DatabaseManager.CloseConnection();

            return advices;
        }
    }

    public class TaskAdvice
    {
        private int uid = 0;
        private int feedbackID = 0;
        private Task taskType = Task.None;
        private string advice = null;

        public int UID { get { return uid; } set { uid = value; } }
        public int FeedbackID { get { return feedbackID; } set { feedbackID = value; } }
        public Task TaskType { get { return taskType; } set { taskType = value; } }
        public string Advice { get { return advice; } set { advice = value; } }
    }
}
