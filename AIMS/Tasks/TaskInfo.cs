﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AIMS
{
	public class TaskInfo
	{
		public TaskInfo()
		{
			
			
		}
		
		private Dictionary<string,int> TaskDictionary = new Dictionary<string, int>()
		{
			{"Intubation", 0},
			{"CPR", 1},
			{"Vertical Lift", 2},
			{"Lateral Transfer", 3}
		};
		
		//------------------------------------------------------------------------------------------------------------
			//INTUBATION DICTIONARIES
		
		private Dictionary<int, string> IntubationStageName = new Dictionary<int,string>()
			{
				{0, "Calibration"},
				{1, "Tilt Head"},
				{2, "Pick Up Lscope"},
				{3, "Insert Lscope"},
				{4, "Visualize Airway"},
				{5, "Grab Tube"},
				{6, "Insert Tube"},
				{7, "Remove Lscope"},
				{8, "Remove Stylet"},
				{9, "Syringe"},
				{10, "Pump Balloon"}
			};
		
		private Dictionary<int, string> IntubationStageInfo = new Dictionary<int, string>()
			{
				{0, "In this stage you must calibrate with AIMS by raising your right hand and placing your left hand on the mannequin's face."},
				{1, "In this stage you must tilt the patient's head back to be properly intubated."},
				{2, "In this stage you must pick up the laryngoscope and lock it into position."},
				{3, "In this stage you must Insert the laryngoscope into the right side of the mouth. Make sure not to hit the teeth."},
				{4, "In this stage you must bend down to view the vocal chords."},
				{5, "In this stage you must pick up the Endotracheal Tube."},
				{6, "In this stage you must insert the Endotracheal tube into the patient's trachea until it is just beyond the vocal chords."},
				{7, "In this stage you must use your right hand to stabilize the tube while removing the laryngoscope with your left. "},
				{8, "In this stage you must stabilize the tube with your left hand and use your right hand to remove the stylet from the tube and set it down"},
				{9, "In this stage you must pick up the syringe with your right hand and compress it to inflate the balloon cuff. This will keep the tube from slipping out of position."},
				{10, "In this stage you must Insert the laryngoscope into the right side of the mouth. Make sure not to hit the teeth."}
			};
		//------------------------------------------------------------------------------------------------------------
			//VERTICAL LIFT DICTIONARIES
			
		private Dictionary<int, string> VerticalLiftStageName = new Dictionary<int, string>()
			{
				{0,"Calibration"},
				{1,"Squat"},
				{2,"Stand"},
				{3,"Put Down"}
			};
			
		private Dictionary<int, string> VerticalLiftStageInfo = new Dictionary<int, string>()
			{
				{0, "In this stage you must stand in front of the object and raise your right hand to calibrate."},
				{1, "In this stage you must squat down, keeping your back straight to pick up the object."},
				{2, "In this stage you must grab the object and lift while keeping your back straight."},
				{3, "In this stage you must squat back down and put down the object."}
			};
		
		//------------------------------------------------------------------------------------------------------------
			//LATERAL TRANSFER DICTIONARIES
			
		private Dictionary<int, string> LateralTransferStageName = new Dictionary<int, string>()
			{
				{0, "Calibration"},
				{1, "Prepare Movement"},
				{2, "Count Down"},
				{3, "Move Patient"}
			};
			
		private Dictionary<int, string> LateralTransferStageInfo = new Dictionary<int, string>()
			{
				{0, "In this stage you must calibrate by standing behind the patient and raising your right hand."},
				{1, "In this stage you must lean over the bed and grasp the sheet under the patient."},
				{2, "In this stage you must work in unison in preparing to pull; Either by counting down or 'Ready,Set,Pull'."},
				{3, "In this stage you must move the patient onto the new bed."}
			};
		
		//------------------------------------------------------------------------------------------------------------
			//CPR DICTIONARIES
			
		private Dictionary<int, string> CprStageName = new Dictionary<int, string>()
			{
				{0, "Calibration"},
				{1, "Check Patient"},
				{2, "Check Pulse"},
				{3, "Call 911"},
				{4, "Call for Tools"},
				{5, "Place Hands"},
				{6, "Body Postion"},
				{7, "Pump Chest"}
			};
			
		private Dictionary<int, string> CprStageInfo = new Dictionary<int, string>()
			{
				{0, "In this stage you must calibrate by kneeling behind the mannequin, placing your left hand in the center of the chest, and raising your right hand."},
				{1, "In this stage you must check for a response from the patient"},
				{2, "In this stage you must check the patient's pulse."},
				{3, "In this stage you must tell AIMI to call 911."},
				{4, "In this stage you must tell AIMI to get a defibrulator"},
				{5, "In this stage you must place your hands in the correct position on the chest."},
				{6, "In this stage you must make sure your shoulders are directly over your hands, with arms straight down and locked."},
				{7, "In this stage you must push down on the chest 2 inches and pump the chest for 2 minutes."}
			};
			
			
		//-------------------------------------------------------------------------------------------------------------
			//FUNCTIONS AND CALLS
			
		/// <summary>
		/// Builds out the image path based on the current task and stage
		/// </summary>
		/// <returns></returns>
		public string buildTaskImagePath(string Task,int Stage)
		{
			string tempStageNum,path;
			
			if(Stage < 10)
				tempStageNum = "00" + Stage.ToString();
			else
				tempStageNum = "0" + Stage.ToString();
			
			//in case we can't find the image, just give back the logo
			path = "../../Assets/Graphics/AIMSnewLogo_CleanTagline.png";

			//intubation
			if(CheckTask(Task) == 0)
			{
				path = "../../Tasks/Intubation/Images/Intubation" + tempStageNum + ".png";
			}
			//CPR
			if(CheckTask(Task) == 1)
			{
				path = "../../Tasks/Cpr/Images/CPR" + tempStageNum + ".png";
			}
			//Lateral Transfer
			if(CheckTask(Task) == 2)
			{
				path = "../../Tasks/Lateral Transfer/Images/LateralTransfer" + tempStageNum + ".png";
			}
			//Vertical Lift
			if(CheckTask(Task) == 3)
			{
				path = "../../Tasks/Vertical Lift/Images/VerticalLift" + tempStageNum + ".png";
			}
			
			return path;
			
		}	
		
		
		/// <summary>
		/// builds out the video path based on the task and stage given
		/// </summary>
		/// <param name="Task"></param>
		/// <param name="Stage"></param>
		/// <returns></returns>
		public string buildTaskVideoPath(string Task, int Stage)
		{
			string tempStageNum,path;
			
			if(Stage < 10)
				tempStageNum = "00" + Stage.ToString();
			else
				tempStageNum = "0" + Stage.ToString();
			
			path = "../../Tasks/Intubation/Videos/Stage 000.wmv";
			
			//intubation
			if(CheckTask(Task) == 0)
			{
				path = "../../Tasks/Intubation/Videos/Stage " + tempStageNum + ".wmv";
			}
			//CPR
			if(CheckTask(Task) == 1)
			{
				path = "../../Tasks/Cpr/Videos/Stage " + tempStageNum + ".wmv";
			}
			//Lateral Transfer
			if(CheckTask(Task) == 2)
			{
				path = "../../Tasks/Lateral Transfer/Videos/Stage " + tempStageNum + ".wmv";
			}
			//Vertical Lift
			if(CheckTask(Task) == 3)
			{
				path = "../../Tasks/Vertical Lift/Videos/Stage " + tempStageNum + ".wmv";
			}
			
			return path;
		}
		
		/// <summary>
		/// Check the Task string given to see if its in the dictionary, returns -1 if not
		/// </summary>
		/// <param name="Task"></param>
		/// <returns></returns>
		private int CheckTask(string Task)
		{
			int keyVal;
			
			if(TaskDictionary.TryGetValue(Task,out keyVal) == true)
				return keyVal;
			else
				return -1;
		}
		
		
		/// <summary>
		/// Returns the stage name of the current task
		/// </summary>
		/// <param name="Task"></param>
		/// <param name="stage"></param>
		/// <returns></returns>
		public string getTaskName( string Task, int Stage)
		{
			string keyVal;
			//Intubation
			if(CheckTask(Task) == 0)
			{
				IntubationStageName.TryGetValue(Stage,out keyVal);
				return keyVal;
			}
			//Cpr
			if(CheckTask(Task) == 1)
			{
				CprStageName.TryGetValue(Stage,out keyVal);
				return keyVal;
			}
			//vertical lift
			if(CheckTask(Task) == 2)
			{
				VerticalLiftStageName.TryGetValue(Stage,out keyVal);
				return keyVal;
			}
			//lateral transfer
			if(CheckTask(Task) == 3)
			{
				LateralTransferStageName.TryGetValue(Stage,out keyVal);
				return keyVal;
			}
			
			return null;
			
		}
		
		
		
		
		
		
		
		
	}
}