﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace AIMS.Tasks
{
    /// <summary>
    /// Parent class from which all task results inherit from.
    /// </summary>
    public abstract class TaskResult
    {
        protected int uid = 0;  // unique identifier in the database. (set by the database)
        protected int studentID = 0;    // id of the student who completed the task.
        protected int? assignmentID = null;
        protected Task taskType = Task.Intubation;
        protected TaskMode taskMode = TaskMode.Practice;
        protected MasteryLevel masteryLevel = MasteryLevel.Novice;
        protected double score = 0;
        protected DateTime startTime = DateTime.MinValue;
        protected DateTime endTime = DateTime.MaxValue;
        protected DateTime submissionTime = DateTime.MinValue;
        private int index = 0;

        /// <summary>
        /// Unique identifier in the database. (Set by the database)
        /// </summary>
        public int UID { get { return uid; } set { uid = value; } }
        /// <summary>
        /// Unique ID of the student who completed the task.
        /// </summary>
        public int StudentID { get { return studentID; } set { studentID = value; } }
        /// <summary>
        /// Unique ID of the assignment which this result was made apart of.
        /// </summary>
        public int? AssignmentID { get { return assignmentID; } set { assignmentID = value; } }
        /// <summary>
        /// Type of task in the Task enum. Examples: Task.Intubation, Task.VerticalLift, Task.LateralTransfer, Task.CPR, etc.
        /// </summary>
        public Task TaskType { get { return taskType; } set { taskType = value; } }
        /// <summary>
        /// Mode of the task's completion. Examples: TaskMode.Practice, TaskMode.Test.
        /// </summary>
        public TaskMode TaskMode { get { return taskMode; } set { taskMode = value; } }
        /// <summary>
        /// MasteryLevel of the task's completion. Examples: MasteryLevel.Novice, MasteryLevel.Intermediate, MasteryLevel.Master.
        /// </summary>
        public MasteryLevel MasteryLevel { get { return masteryLevel; } set { masteryLevel = value; } }
        /// <summary>
        /// Score value representing the overall score for the task completion performace.
        /// </summary>
        public double Score { get { return score; } set { score = Double.IsNaN(value) ? 0 : value; } }
        /// <summary>
        /// Time of task start.
        /// </summary>
        public DateTime StartTime { get { return startTime; } set { startTime = value; } }
        /// <summary>
        /// Time of task end.
        /// </summary>
        public DateTime EndTime { get { return endTime; } set { endTime = value; } }
        /// <summary>
        /// DateTime submission received by the database. (Set by the database)
        /// </summary>
        public DateTime SubmissionTime { get { return submissionTime; } set { submissionTime = value; } }
        /// <summary>
        /// Unique identifier in the database. (Set by the database)
        /// </summary>
        public int Index { get { return index; } set { index = value; } }

        public TaskResult()
        {
            uid = 0;  // unique identifier in the database. (set by the database)
            assignmentID = null;
            studentID = 0;    // id of the student who completed the task.
            taskType = Task.Intubation;
            taskMode = TaskMode.Practice;
            masteryLevel = MasteryLevel.Novice;
            score = 0;
            startTime = DateTime.MinValue;
            endTime = DateTime.MaxValue;
            submissionTime = DateTime.MinValue;
        }
    }
}
