﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.IO;
using AIMS.Speech;

namespace AIMS.Tasks
{
    /// <summary>
    /// Interaction logic for LessonControl.xaml
    /// </summary>
    public partial class LessonControl
    {
        public LessonControl()
        {
            InitializeComponent();

            Loaded += LessonPage_Loaded;
            Unloaded += LessonPage_Unloaded;
        }

        private DispatcherTimer showResultDelayTimer = new DispatcherTimer();
        private readonly TimeSpan showResultDelay = TimeSpan.FromSeconds(3.0);

        private AIMS.Lesson lesson = null;
        public AIMS.Lesson Lesson { get { return lesson; } set { lesson = value; } }

        private void LessonPage_Loaded(object sender, RoutedEventArgs e)
        {
     //       LessonStatsheet statsheet = null;

   /*         if (lesson != null)
            {
                lesson.WipeClean();
                lesson = null;
            } */

   /*         if (((App)App.Current).CurrentStudent != null)
            {
                statsheet = LessonStatsheet.GetOrCreateStatsheetForStudent(((App)App.Current).CurrentStudent.ID, LessonType.CPR);
            }

            if (statsheet != null)
            {
                ((App)App.Current).CurrentMasteryLevel = statsheet.MasteryLevel;

                lesson = new AIMS.Tasks.Cpr.Cpr(this, ((App)App.Current).CurrentTaskMode, statsheet.MasteryLevel);
            }
            else
            {
                // Instantiate the lesson.
                lesson = new AIMS.Tasks.Cpr.Cpr(this, ((App)App.Current).CurrentTaskMode, ((App)App.Current).CurrentMasteryLevel);
            }*/

    /*        try
            {
                if (KinectManager.KinectSpeechCommander != null)
                {
                    KinectManager.KinectSpeechCommander.SetModuleGrammars(new Uri("pack://application:,,,/AIMS;Component/Tasks/Cpr/CprSpeechGrammar.xml"),
                        "activeListen", "defaultListen");
                    KinectManager.KinectSpeechCommander.SpeechRecognized += new EventHandler<SpeechCommanderEventArgs>(KinectSpeechCommander_SpeechRecognized);
                }
            }
            catch(Exception exc)
            {
                System.Diagnostics.Debug.Print("CprPage.xaml.cs ERROR: {0}", exc.Message);
            } */

            this.CurrentTaskModeTB.Text = ((App)App.Current).CurrentTaskMode.ToString();
            this.CurrentTaskTB.Text = ((App)App.Current).CurrentTask.ToString();

      /*      if (lesson != null)
                lesson.StartLesson();*/
        }

        private void LessonPage_Unloaded(object sender, RoutedEventArgs e)
        {
  /*          if (lesson != null)
            {
                lesson.WipeClean();
                lesson = null;
            }*/

            try
            {
                KinectManager.KinectSpeechCommander.SpeechRecognized -= KinectSpeechCommander_SpeechRecognized;
                KinectManager.KinectSpeechCommander.ClearModuleGrammars();
            }
            catch
            {
            }
        }

        #region Kinect Speech processing
        void KinectSpeechCommander_SpeechRecognized(object sender, SpeechCommanderEventArgs e)
        {
            switch (e.Command)
            {
                case "HELP":
                    OnSaid_Help();
                    break;
                case "VIDEO":
                    OnSaid_Video();
                    break;
                case "TUTORIAL":
                    OnSaid_Tutorial();
                    break;
            }
        }

        /// <summary>
        /// "Help" was said, display the help menu and/or give auditory response. 
        /// </summary>
        private void OnSaid_Help()
        {
    /*        if( lesson != null )
            {
                if (lesson.Stages != null && lesson.CurrentStageIndex < lesson.Stages.Count)
                {
                    switch (lesson.CurrentStageIndex)
                    {
                        case 0:

                            break;
                        case 1:
                    //        tiltHead.Play();
                            break;
                        case 2:
                    //        pickUpLaryngoscopeAndInsert.Play();
                            break;
                        case 3:
                    //        sweepTheLaryngoscope.Play();
                            break;
                        case 4:
                    //        bendDown.Play();
                            break;
                        case 5:
                   //         pickUpTube.Play();
                            break;
                        case 6:
                   //         insertTube.Play();
                            break;
                        case 7:
                    //        removeLaryngoscope.Play();
                            break;
                        case 8:
                    //        removeStylet.Play();
                            break;
                        case 9:
                    //        useSyringe.Play();
                            break;
                    }
                }
            }*/
        }

        private void OnSaid_Video()
        {
      /*      if (lesson != null)
            {
                if (lesson.Stages != null && lesson.CurrentStageIndex < lesson.Stages.Count)
                {
                    lesson.Stages[lesson.CurrentStageIndex].RequestStageVideo();
                }
            }*/
        }

        private void OnSaid_Tutorial()
        {
            /*if (lesson != null)
            {
                lesson.RequestTutorialVideo();
            }*/
        }
        
        #endregion Kinect Speech processing

        public void ShowResults(Result result)
        {
            // make a ShowResults function in code-behind like lateral transfer
           // this.ResultsComponent.IsEnabled = true;
          //  this.ResultsComponent.ShowResults(result, true);
          //  this.ResultsComponent.Visibility = System.Windows.Visibility.Visible;
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            // Reset the lesson
     //       this.lesson.ResetLesson();
        }

        private void SidePanelExpansionButton_Click(object sender, RoutedEventArgs e)
        {
            if (SidePanelContentGrid.Visibility == System.Windows.Visibility.Collapsed)
            {
                SidePanelContentGrid.Visibility = System.Windows.Visibility.Visible;
                (sender as Button).Content = "<";
                SidePanelContentGrid.Focus();
            }
            else
            {
                SidePanelContentGrid.Visibility = System.Windows.Visibility.Collapsed;
                (sender as Button).Content = ">";
                SidePanelContentGrid.Focus();
            }
        }

        private void HeaderButtonsListBox_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                HeaderButtonsListBox.SelectedIndex = 0;
            }
            catch
            {
            }
        }

        private void ProceduralTaskButtonsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            try
            {
                switch (ProceduralTaskButtonsListBox.SelectedIndex)
                {
                    case 0:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    case 1:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    case 2:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    case 3:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    case 4:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    default:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                }
            }
            catch
            {
            }
        }

        private void HeaderButtonsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (HeaderButtonsListBox.SelectedIndex == -1)
            {
                return;
            }

            try
            {
                switch (HeaderButtonsListBox.SelectedIndex)
                {
                    case 0:

                        break;
                    case 1:
                        //  HeaderButtonsListBox.SelectedIndex = -1;    // Deselect
                        ProceduralTaskButtonsListBox.SelectedIndex = -1;    // Deselect
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Visible;
                        break;
                    case 2:

                        break;
                    case 3:

                        break;
                    case 4:

                        break;
                    default:

                        break;
                }
            }
            catch
            {
            }
        }

        private void ProceduralTaskButtonsListBox_MouseLeave(object sender, MouseEventArgs e)
        {
            ProceduralTaskButtonsListBox.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void TopPanelExpansionButton_Click(object sender, RoutedEventArgs e)
        {
            if (TopPanelContentGrid.Visibility == System.Windows.Visibility.Collapsed)
            {
                TopPanelContentGrid.Visibility = System.Windows.Visibility.Visible;
                (sender as Button).Content = "^";
                TopPanelContentGrid.Focus();
            }
            else
            {
                TopPanelContentGrid.Visibility = System.Windows.Visibility.Collapsed;
                (sender as Button).Content = "v";
                TopPanelContentGrid.Focus();
            }
        }

        private void ResultsComponent_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue.Equals(true))
                this.BlurEffect.Radius = 25;
            else
                this.BlurEffect.Radius = 0;
        }
    }
}
