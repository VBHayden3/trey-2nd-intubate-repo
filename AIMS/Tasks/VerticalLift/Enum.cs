﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIMS.Tasks.VerticalLift
{
    public enum StageEnum { Calibration, Down, LiftObject, LowerObject, Rise, Done }
    public enum FootSpacingEnum { Ok, Narrow, Wide };
}
