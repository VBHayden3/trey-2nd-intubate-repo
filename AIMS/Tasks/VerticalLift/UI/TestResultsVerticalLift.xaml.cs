﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;

namespace AIMS.Tasks.VerticalLift
{
	public partial class TestResultsVerticalLift : INotifyPropertyChanged
	{
        private int index = 0;
        private VerticalLiftResult vertLiftResult = new VerticalLiftResult();
        private List<VerticalLiftRep> results = null;
		
		private double vertLiftGrade = 0.95*100;
		public double VertLiftGrade
		{
			get { return vertLiftGrade; }
			set
			{
				if ( vertLiftGrade == value )
					 vertLiftGrade = value;
				else
				{
					 vertLiftGrade = value;
					 NotifyPropertyChanged("VertLiftGrade");
				}	
			}
		}

        private double repGrade = 1.0 * 100;
        public double RepGrade
        {
            get { return repGrade; }
            set
            {
                if (repGrade == value)
                    repGrade = value;
                else
                {
                    repGrade = value;
                    NotifyPropertyChanged("RepGrade");
                }
            }
        }
				
		
		public event PropertyChangedEventHandler PropertyChanged;
		
        private void NotifyPropertyChanged( string propertyName )
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
	
		public TestResultsVerticalLift()
		{
			this.InitializeComponent();
			this.DataContext = this;

            this.results = new List<VerticalLiftRep>();
            results.Add(new VerticalLiftRep());
            this.VertLiftGrade = FindAverage();
            RepGrade = results[index].Score;
		}

        // Just trying to test Storing. Will need to be cleaned up.
        public TestResultsVerticalLift(VerticalLiftResult Results)
        {
            this.InitializeComponent();
            this.DataContext = this;

            this.vertLiftResult = Results;
            this.results = this.vertLiftResult.Repetetions;
            this.VertLiftGrade = FindAverage();
            RepGrade = this.results[index].Score * 100;

            UpdateView();
        }

        public TestResultsVerticalLift(List<VerticalLiftRep> results)
        {
            this.InitializeComponent();
            this.DataContext = this;

            this.results = results;
            this.VertLiftGrade = FindAverage();
            RepGrade = results[index].Score * 100;

            UpdateView();
        }

        private void PreviousRep_Click(object sender, RoutedEventArgs e)
        {
            index = index - 1;
            if(index < 0)
                index = 0;

            RepGrade = results[index].Score * 100;

            UpdateView();
        }

        private void NextRep_Click(object sender, RoutedEventArgs e)
        {
            index = index + 1;
            if(index == results.Count)
                index = index - 1;

            RepGrade = results[index].Score * 100;

            UpdateView();
        }

        private void TryAgain_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new VerticalLiftPage());
        }

        // thrown together to test vert lift storage. May need to be updated.
        private void btn_Submit_Click(object sender, RoutedEventArgs e)
        {
            if (this.results == null)
                return;

            this.btn_submit.IsEnabled = false;
            this.btn_submit.Content = "Submitting...";

            // Disable the TryAgainButton and ExitButton to be reenabled upon submission (otherwise this was causing a null reference crash before)
            this.btn_return.IsEnabled = false;
            this.btn_tryAgain.IsEnabled = false;

            if (((App)App.Current).CurrentAccount == null || ((App)App.Current).CurrentStudent == null || ((App)App.Current).CurrentUserID <= 0)
            {
                MessageBox.Show("You cannot submit results to the database unless you are logged in as a student.");
                return;
            }

            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                VerticalLiftResult.StoreResult(this.vertLiftResult);
            };

            worker.RunWorkerCompleted += delegate(object s, RunWorkerCompletedEventArgs args)
            {
                this.btn_submit.Content = "Success";
                this.btn_tryAgain.IsEnabled = true;
                this.btn_return.IsEnabled = true;
            };

            worker.RunWorkerAsync();

            e.Handled = true;
        }

        private double FindAverage()
        {
            double sum = 0.0;

            foreach(VerticalLiftRep rep in results)
            {
                sum += rep.Score;
            }

            return 100 * sum / results.Count;
        }

        private void UpdateView()
        {
            RepCountTextBlock.Text = "Rep " + (index + 1);

            RepScoreTextBlock.Text = "" + (int)(results[index].Score * 100);

            ObjectCheckBox.IsChecked = results[index].CloseToBody;
            FeetCheckBox.IsChecked = results[index].FootSpacing == FootSpacingEnum.Ok;
            BackCheckBox.IsChecked = results[index].BackStraight;
        }
	}
}