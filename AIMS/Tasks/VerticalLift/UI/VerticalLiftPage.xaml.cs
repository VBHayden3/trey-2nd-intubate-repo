﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;

namespace AIMS.Tasks.VerticalLift
{
    /// <summary>
    /// Interaction logic for VerticalLiftPage.xaml
    /// </summary>
    public partial class VerticalLiftPage : Page
    {
        public static readonly DependencyProperty CompletedRepsProperty =
            DependencyProperty.Register("CompletedReps", typeof(int), typeof(VerticalLiftPage), new PropertyMetadata(0, RepsChangedCallback));

        public static readonly DependencyProperty MinRepsProperty =
            DependencyProperty.Register("MinReps", typeof(int), typeof(VerticalLiftPage), new PropertyMetadata(3, RepsChangedCallback));

        public static readonly DependencyProperty BoxPositionProperty =
            DependencyProperty.Register("BoxPosition", typeof(Point3D), typeof(VerticalLiftPage), new PropertyMetadata(new Point3D(0, 0, 0), BoxPositionChangedCallback));

        public int CompletedReps
        {
            get { return (int)GetValue(CompletedRepsProperty); }
            set { SetValue(CompletedRepsProperty, value); }
        }

        public int MinReps
        {
            get { return (int)GetValue(MinRepsProperty); }
            set { SetValue(MinRepsProperty, value); }
        }

        public Point3D BoxPosition
        {
            get { return (Point3D)GetValue(BoxPositionProperty); }
            set { SetValue(BoxPositionProperty, value); } 
        }

        private static void RepsChangedCallback(object sender, DependencyPropertyChangedEventArgs e)
        {
            VerticalLiftPage obj = sender as VerticalLiftPage;

            if (obj != null)
            {
                obj.RepCounterTextBlock.Text = "(" + obj.CompletedReps + "/" + obj.MinReps + ")";
            }
        }

        private static void BoxPositionChangedCallback(object sender, DependencyPropertyChangedEventArgs e)
        {
            VerticalLiftPage obj = sender as VerticalLiftPage;

            if (obj != null)
            {
                obj.IntubationToolsOverlay.Laryngoscope3DPosition = obj.BoxPosition;
            }
        }

        private VerticalLift lesson = null;        

        public VerticalLiftPage()
        {
            InitializeComponent();

            Loaded += new RoutedEventHandler(VerticalLiftPage_Loaded);
            Unloaded += new RoutedEventHandler(VerticalLiftPage_Unloaded);
        }

        public VerticalLiftPage(VerticalLiftResults results)
        {
            InitializeComponent();
            
            Loaded += new RoutedEventHandler(VerticalLiftPage_Loaded);
            Unloaded += new RoutedEventHandler(VerticalLiftPage_Unloaded);
        }

        void VerticalLiftPage_Loaded(object sender, RoutedEventArgs e)
        {
            LessonStatsheet statSheet = null;
            MasteryLevel masteryLevel = ((App)App.Current).CurrentMasteryLevel;

            if (lesson != null)
            {
                lesson.WipeClean();
                lesson = null;
            }

            if (((App)App.Current).CurrentStudent != null)
            {
                statSheet = LessonStatsheet.GetOrCreateStatsheetForStudent(((App)App.Current).CurrentStudent.ID, LessonType.VerticalLift);
            }

            if (statSheet != null)
            {
                this.CurrentMasteryLevelTB.Text = statSheet.MasteryLevel.ToString();
                ((App)App.Current).CurrentMasteryLevel = statSheet.MasteryLevel;
                masteryLevel = statSheet.MasteryLevel;
            }

            lesson = new VerticalLift(this);

            this.CurrentTaskModeTB.Text = ((App)App.Current).CurrentTaskMode.ToString();
            this.CurrentTaskTB.Text = ((App)App.Current).CurrentTask.ToString();

            Binding binding = new Binding("StageName");
            binding.Source = lesson;
            binding.Mode = BindingMode.OneWay;
            StageInfoTextBlock.SetBinding(TextBlock.TextProperty, binding);

            binding = new Binding("StageInstruction");
            binding.Source = lesson;
            binding.Mode = BindingMode.OneWay;
            InstructionTextBlock.SetBinding(TextBlock.TextProperty, binding);

            binding = new Binding("StageImagePath");
            binding.Source = lesson;
            binding.Mode = BindingMode.OneWay;
            CurrentStageImage.SetBinding(Image.SourceProperty, binding);

            binding = new Binding("CompletedReps");
            binding.Source = lesson;
            binding.Mode = BindingMode.OneWay;
            this.SetBinding(VerticalLiftPage.CompletedRepsProperty, binding);

            binding = new Binding("MinReps");
            binding.Source = lesson;
            binding.Mode = BindingMode.OneWay;
            this.SetBinding(VerticalLiftPage.MinRepsProperty, binding);

            binding = new Binding("BoxPosition");
            binding.Source = lesson;
            binding.Mode = BindingMode.OneWay;
            this.SetBinding(VerticalLiftPage.BoxPositionProperty, binding);

            if (lesson != null)
            {
                lesson.StartLesson();
            }

            KinectManager.AllDataReady += new AllDataReadyEventHandler(KinectManager_AllDataReady);
        }

        void KinectManager_AllDataReady()
        {
            System.Diagnostics.Debug.WriteLine("VerticalLiftPage -> executing AllDataReady()");
            skeletonOverlay.UpdateSkeleton();
        }

        void VerticalLiftPage_Unloaded(object sender, RoutedEventArgs e)
        {
            if (lesson != null)
            {
                lesson.WipeClean();
                lesson = null;
            }

            KinectManager.AllDataReady -= new AllDataReadyEventHandler(KinectManager_AllDataReady);
        }

        public void GoHome()
        {
            System.Uri uri = new System.Uri("../MainPage3.xaml", System.UriKind.Relative);
            this.NavigationService.Navigate(uri);
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            lesson.ResetLesson();// Restart();
        }

        private void GetJpgButton_Click(object sender, RoutedEventArgs e)
        {
            byte[] imageBytes = JpgScreenshot.GetJpgImage(this.OverlayGrid, 0.75, 100);

            // Convert Byte-Array in Base64String --> Image as String
            string bildBase64 = Convert.ToBase64String(imageBytes);

            // save uncompressed bitmap to disk
            string fileName = String.Format("{0}{1}-{2}-{3}_{4}-{5}-{6}.jpg", "C:\\TestJPG_", DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);

            try
            {
                System.IO.FileStream fs = System.IO.File.Open(fileName, System.IO.FileMode.OpenOrCreate);
                fs.Write(imageBytes, 0, imageBytes.Length);
                fs.Close();
            }
            catch
            {
            }
        }

        private void SidePanelExpansionButton_Click(object sender, RoutedEventArgs e)
        {
            if (SidePanelContentGrid.Visibility == System.Windows.Visibility.Collapsed)
            {
                SidePanelContentGrid.Visibility = System.Windows.Visibility.Visible;
                (sender as Button).Content = "<";
                SidePanelContentGrid.Focus();
            }
            else
            {
                SidePanelContentGrid.Visibility = System.Windows.Visibility.Collapsed;
                (sender as Button).Content = ">";
                SidePanelContentGrid.Focus();
            }
        }

        private void HeaderButtonsListBox_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                HeaderButtonsListBox.SelectedIndex = 0;
            }
            catch
            {
            }
        }

        private void ProceduralTaskButtonsListBox_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                
            }
            catch
            {
            }
        }

        private void ProceduralTaskButtonsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            try
            {
                switch (ProceduralTaskButtonsListBox.SelectedIndex)
                {
                    case 0:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    case 1:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    case 2:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    case 3:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    case 4:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                    default:
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Collapsed;
                        break;
                }
            }
            catch
            {
            }
        }

        private void HeaderButtonsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (HeaderButtonsListBox.SelectedIndex == -1)
            {
                return;
            }

            try
            {
                switch (HeaderButtonsListBox.SelectedIndex)
                {
                    case 0:
                        
                        break;
                    case 1:
                      //  HeaderButtonsListBox.SelectedIndex = -1;    // Deselect
                        ProceduralTaskButtonsListBox.SelectedIndex = -1;    // Deselect
                        ProceduralTaskButtonsListBox.Visibility = Visibility.Visible;
                        break;
                    case 2:
                        
                        break;
                    case 3:
                        
                        break;
                    case 4:
                        
                        break;
                    default:
                        
                        break;
                }
            }
            catch
            {
            }
        }

        private void ProceduralTaskButtonsListBox_MouseLeave(object sender, MouseEventArgs e)
        {
            ProceduralTaskButtonsListBox.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void TopPanelExpansionButton_Click(object sender, RoutedEventArgs e)
        {
            if (TopPanelContentGrid.Visibility == System.Windows.Visibility.Collapsed)
            {
                TopPanelContentGrid.Visibility = System.Windows.Visibility.Visible;
                (sender as Button).Content = "^";
                TopPanelContentGrid.Focus();
            }
            else
            {
                TopPanelContentGrid.Visibility = System.Windows.Visibility.Collapsed;
                (sender as Button).Content = "v";
                TopPanelContentGrid.Focus();
            }
        }

        private void PatientHealthBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }

        private void ResultsComponent_Loaded(object sender, RoutedEventArgs e)
        {

        }

        public void ShowResults(VerticalLiftResult Results)
        {
            //this.ResultsComponent.ShowResults(result);
            NavigationService.Navigate(new TestResultsVerticalLift(Results));
        }
    }
}
