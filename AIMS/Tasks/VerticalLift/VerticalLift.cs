﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Media.Media3D;
using System.Windows;
using Microsoft.Kinect;
using AIMS.ODU_Dev.Utilities;

namespace AIMS.Tasks.VerticalLift
{
    public class VerticalLift : Lesson, INotifyPropertyChanged
    {
        private static readonly int MIN_REPS = 2;
        public static readonly int NUM_STAGES = 4; // The number of stages (excluding calibration, but excluding Done)
        public static readonly int SMOOTHING_FACTOR = 5; // The number of skeletons to store for smoothing

        public delegate void UpdateCompleteEventHandler(object sender, EventArgs e);

        public event PropertyChangedEventHandler PropertyChanged;
        public event UpdateCompleteEventHandler UpdateComplete;

        // Properties that notify when changed or are unchangeable
        public string StageName
        {
            get
            { 
                return stageName;
            }
            set
            {
                stageName = value;
                NotifyPropertyChanged("StageName");
            }
        }
        public string StageInstruction
        {
            get
            {
                return stageInstruction;
            }
            set
            {
                stageInstruction = value;
                NotifyPropertyChanged("StageInstruction");
            }
        }
        public Uri StageImagePath
        {
            get
            {
                return stageImagePath;
            }
            set
            {
                stageImagePath = value;
                NotifyPropertyChanged("StageImagePath");
            }
        }
        public int CompletedReps
        {
            get { return reps; }
            private set
            {
                reps = value;
                NotifyPropertyChanged("CompletedReps");
            }
        }
        public int MinReps
        {
            get { return MIN_REPS; }
        }

        // Properties that do not notify when changed
        public Point3D StartingBase
        {
            get { return startingBase; }
            set { startingBase = value; }
        }
        public Point3D StartingHeight
        {
            get { return startingHeight; }
            set { startingHeight = value; }
        }
        public Point3D PreviousPosition
        {
            get { return previousPosition; }
            set { previousPosition = value; }
        }
        public Point3D StartingHipHeight
        {
            get { return startingHipHeight; }
            set { startingHipHeight = value; }
        }
        public Point3D BoxPosition
        {
            get { return boxPosition; }
            set
            {
                boxPosition = value;
                NotifyPropertyChanged("BoxPosition");
            }
        }
        public Point3D BoxStart
        {
            get { return boxStart; }
            set { boxStart = value; }
        }
        public VerticalLiftPage VerticalLiftPage
        {
            get { return verticalLiftPage; }
            set { verticalLiftPage = value; }
        }
        public VerticalLiftResult VertLiftResult
        {
            get { return this.vertLiftResult; }
        }
    /*    public List<VerticalLiftRep> RepResults
        {
            get { return repResults; }
            set { repResults = value; }
        }
*/        
        public Queue<Body> SkeletonQueue
        {
            get { return skeletonQueue; }
            private set { skeletonQueue = value; }
        }

        private StageEnum currentStage = 0;
        private int reps = 0;
        private Point3D startingBase = new Point3D();
        private Point3D startingHeight = new Point3D();
        private Point3D startingHipHeight = new Point3D();
        private Point3D previousPosition = new Point3D();
        private Point3D boxPosition = new Point3D();
        private Point3D boxStart = new Point3D();
        private Queue<Body> skeletonQueue = new Queue<Body>(SMOOTHING_FACTOR);
        private VerticalLiftPage verticalLiftPage = null;
        private VerticalLiftResult vertLiftResult = new VerticalLiftResult();
       // private List<VerticalLiftRep> repResults = new List<VerticalLiftRep>();

        // Stage info
        private string stageName = "";
        private string stageInstruction = "";
        private Uri stageImagePath = null;

        public VerticalLift(VerticalLiftPage page)
        {
            this.verticalLiftPage = page;

            this.vertLiftResult.AddRepetition(new VerticalLiftRep());

            Stages = new List<Stage>();
            Stages.Add(new VerticalLiftCalibration(this));
            Stages.Add(new VerticalLiftDown(this));
            Stages.Add(new VerticalLiftLiftObject(this));
            Stages.Add(new VerticalLiftLowerObject(this));
            Stages.Add(new VerticalLiftRise(this));
            Stages.Add(new VerticalLiftResults(this));

            Stopwatch = new System.Diagnostics.Stopwatch();

            Result = new Result();
        }

        public override void StartLesson()
        {
            if (!KinectManager.HasKinect)   // Kinect sensor does not exist, abort the lesson.
            {
                this.AbortLesson();
            }
            else
            {
                //KinectManager.Kinect.ElevationAngle = 0;
                KinectManager.AllDataReady += KinectManager_AllDataReady;
                KinectManager.SensorNotReady += KinectManager_SensorNotReady;

                StartTime = DateTime.Now;
                this.Stopwatch.Start();

                KinectManager.NumStickySkeletons = 1;
            }

        }

        public override void UpdateLesson()
        {
            if (this.Complete == false)
            {
                // Update the stages watchers
                Stages[(int)currentStage].UpdateWatcherStates(true, false, false);

                Stages[(int)currentStage].UpdateStage();

                if (Stages[(int)currentStage].Complete)
                {
                    currentStage = NextStage();

                    if(currentStage != StageEnum.Done)
                        Stages[(int)currentStage].WipeClean();
                }
            }

            // Update the previous position
            if (KinectManager.StickySkeletonIDs[0] != null)
            {
                Body s = KinectManager.GetSkeleton(KinectManager.StickySkeletonIDs[0].Value, true);

                if (s != null)
                {
                    Joint thisJoint = s.Joints[JointType.SpineBase];
                    if (thisJoint != null)
                    {
                        CameraSpacePoint p = thisJoint.Position;
                        PreviousPosition = new Point3D(p.X, p.Y, p.Z);
                    }
                }
            }

            if (UpdateComplete != null)
            {
                UpdateComplete(this, null);
            }

        }

        public override void EndLesson()
        {
            if (this.Complete != true)
            {
                this.Complete = true;

                this.EndTime = DateTime.Now;
                this.Running = false;

                this.Stopwatch.Stop();

                // Update the rest of the known CprResults fields.
                this.vertLiftResult.StartTime = this.StartTime;
                this.vertLiftResult.EndTime = this.EndTime;
                this.vertLiftResult.MasteryLevel = this.MasteryLevel;
                this.vertLiftResult.TaskMode = this.TaskMode;
                this.vertLiftResult.StudentID = ((App)App.Current).CurrentStudent.ID;

                this.ShowResults();
            }
        }

        public override void AbortLesson()
        {
            // Stop calls to update the lesson.
            KinectManager.AllDataReady -= KinectManager_AllDataReady;

            // Stop the Overlays that use the Kinect sensor.
            this.verticalLiftPage.skeletonOverlay.ShowBoneOverlays = false;
            this.verticalLiftPage.skeletonOverlay.ShowJointOverlays = false;
            this.verticalLiftPage.skeletonOverlay.ShowJointTextOverlays = false;

            // Pause and cleanout the lesson. 
            this.PauseLesson();
            this.WipeClean();

            // Go back to the Main Page.  This will trigger VerticalLiftPage Unloaded
            if (this.verticalLiftPage != null)
                this.verticalLiftPage.GoHome();
        }

        private void ShowResults()
        {
            //this.Result = CreateResultFromStageScores();

            if (this.VerticalLiftPage != null)
            {
                this.VerticalLiftPage.ShowResults(this.vertLiftResult);
            }
        }

        public override void PauseLesson()
        {
            this.Paused = true;
            this.Stopwatch.Stop();

            if (this.Stages != null && this.Stages.Count > this.CurrentStageIndex)
            {
                try
                {
                    this.Stages[this.CurrentStageIndex].PauseStage();
                }
                catch
                {
                }
            }
        }

        public override void ResumeLesson()
        {
            this.Paused = false;
            this.Stopwatch.Start();

            if (this.Stages != null && this.Stages.Count > this.CurrentStageIndex)
            {
                this.Stages[this.CurrentStageIndex].ResumeStage();
            }
        }

        public override void ResetLesson()
        {
            this.WipeClean();
            this.StartLesson();
        }

        public override void WipeClean()
        {
            CurrentStageIndex = 0;
            currentStage = StageEnum.Calibration;
            CompletedReps = 0;
            this.Complete = false;
            startingBase = new Point3D();
            startingHeight = new Point3D();

            this.vertLiftResult = new VerticalLiftResult();
            this.vertLiftResult.AddRepetition(new VerticalLiftRep());

            KinectManager.AllDataReady -= KinectManager_AllDataReady;
            KinectManager.SensorNotReady -= KinectManager_SensorNotReady;

            foreach (Stage stage in Stages)
            {
                stage.WipeClean();
            }
        }

        public override void OnTutorialVideoRequest()
        {
            throw new NotImplementedException();
        }

        // Figures out what the next stage should be.
        private StageEnum NextStage()
        {
            int nextStage = (int)currentStage;

            // If the user was not already done
            if ((int)currentStage < (int)StageEnum.Done)
            {                
                nextStage = nextStage + 1;

                // Go back to the first stage if the minimum number of reps have not been completed
                if (nextStage == (int)StageEnum.Done)
                {
                    this.vertLiftResult[this.CompletedReps].CalculateScore();

                    CompletedReps++;

                    if (CompletedReps < MIN_REPS)
                    {
                        nextStage = (int)StageEnum.Down;
                        this.vertLiftResult.AddRepetition(new VerticalLiftRep());
                    }
                }                
            }
            
            return (StageEnum)nextStage;
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void KinectManager_AllDataReady()
        {
            // Add the skeleton to the queue for the next frame
            Body skeleton = null;

            if (KinectManager.StickySkeletonIDs[0] != null)
                skeleton = KinectManager.GetSkeleton(KinectManager.StickySkeletonIDs[0].Value, true);

            if (skeleton != null)
            {
                if (skeletonQueue.Count == SMOOTHING_FACTOR)
                    skeletonQueue.Dequeue();

                skeletonQueue.Enqueue(skeleton);
            }

            UpdateLesson();
        }

        private void KinectManager_SensorNotReady()
        {
            if (!this.Complete)
            {
                this.AbortLesson();
            }
        }
    }
}
