﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Kinect;
using System.Windows;
using System.ComponentModel;

namespace AIMS.Tasks.VerticalLift
{
    /// <summary>
    /// The calibration stage for vertical lift. Calibration makes sure the entire body is in view
    /// and finds the starting height of the user.
    /// </summary>
    class VerticalLiftCalibration : Stage
    {
        private VerticalLift lesson;
        private ulong? currentStuckSkeleton = null;

        public VerticalLiftCalibration(VerticalLift lesson)
        {
            this.lesson = lesson;

            StageNumber = 0;
            Name = "Calibration";
            Instruction = "Make sure your whole body is in view of the camera and raise your left hand.";
            TargetTime = TimeSpan.MaxValue;
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Assets/Graphics/Intubation/Intubation_Calibration_640.jpg";
            Active = false;
            Complete = false;
            ScoreWeight = 0;
            Zones = new List<Zone>();
            Stopwatch = new System.Diagnostics.Stopwatch();
            TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            TimeOutTime = TimeSpan.FromSeconds(30.0);
            CanTimeOut = false;
        }

        public override void SetupStage()
        {
            lesson.StageName = Name;
            lesson.StageInstruction = Instruction;
            lesson.StageImagePath = new Uri(ImagePath, UriKind.Relative);

            Zones.Clear();

            CurrentStageWatchers = new List<BaseWatcher>();
            PeripheralStageWatchers = new List<BaseWatcher>();
            OutlyingStageWatchers = new List<BaseWatcher>();

            CreateStageWatchers();

            StartTime = DateTime.Now;
            this.Stopwatch.Restart();
            this.TimeoutStopwatch.Restart();
            IsSetup = true;
            
        }

        private void CreateStageWatchers()
        {
            #region Sticky Watcher
            // Watch for a skeleton to get stuck
            Watcher stickyWatcher = new Watcher();
            stickyWatcher.CheckForActionDelegate = () =>
            {
                if (KinectManager.Bodies != null && KinectManager.Bodies.Length > 0)
                {
                    if (KinectManager.SingleStickySkeletonProfile != null)
                    {
                        currentStuckSkeleton = KinectManager.SingleStickySkeletonProfile.CurrentTrackingID;
                        stickyWatcher.IsTriggered = true;
                    }
                    else
                    {
                        currentStuckSkeleton = null;
                        stickyWatcher.IsTriggered = false;
                    }
                }
            };
            this.CurrentStageWatchers.Add(stickyWatcher);
            #endregion

            #region Hand Raised
            // Watch for the left hand to be raised
            Watcher handRaisedWatcher = new Watcher();
            handRaisedWatcher.CheckForActionDelegate = () =>
            {
                if (KinectManager.Bodies != null && KinectManager.Bodies.Length > 0)
                {
                    if (currentStuckSkeleton.HasValue)
                    {
                        Body skeleton = KinectManager.GetSkeleton(currentStuckSkeleton.Value, true);

                        if (skeleton != null)
                        {
                            // Check if the right hand is raised
                            bool right = skeleton.Joints[JointType.HandRight].Position.Y > skeleton.Joints[JointType.ShoulderRight].Position.Y;

                            // Check if the left hand is raised
                            bool left = skeleton.Joints[JointType.HandLeft].Position.Y > skeleton.Joints[JointType.ShoulderLeft].Position.Y;

                            // One hand up, but not both
                            handRaisedWatcher.IsTriggered = left || right && !(left && right);
                        }
                    }
                    else
                    {
                        handRaisedWatcher.IsTriggered = false;
                    }
                }
                else
                {
                    handRaisedWatcher.IsTriggered = false;
                }
            };
            this.CurrentStageWatchers.Add(handRaisedWatcher);
            #endregion

            #region Body Visible
            // Watch for full body visibility
            Watcher bodyVisible = new Watcher();
            bodyVisible.CheckForActionDelegate = () =>
            {
                bool allVisible = false;
                if (KinectManager.Bodies != null && KinectManager.Bodies.Length > 0)
                {
                    if (currentStuckSkeleton.HasValue)
                    {
                        Body skeleton = KinectManager.GetSkeleton(currentStuckSkeleton.Value, true);

                        if (skeleton != null)
                        {
                            allVisible = (KinectManager.ClosestBody.Joints[JointType.Head].TrackingState == TrackingState.Tracked)
                                && (KinectManager.ClosestBody.Joints[JointType.AnkleRight].TrackingState == TrackingState.Tracked)
                                && (KinectManager.ClosestBody.Joints[JointType.AnkleLeft].TrackingState == TrackingState.Tracked)
                                && (KinectManager.ClosestBody.Joints[JointType.HandRight].TrackingState == TrackingState.Tracked)
                                && (KinectManager.ClosestBody.Joints[JointType.HandLeft].TrackingState == TrackingState.Tracked);
                        }
                    }
                }

                bodyVisible.IsTriggered = allVisible;
            };
            this.CurrentStageWatchers.Add(bodyVisible);
            #endregion

            #region Box Color
            Watcher boxWatcher = new Watcher();
            // Searches for the box around/under the feet
            boxWatcher.CheckForActionDelegate = () =>
            {
                Body skeletonBW = null;
                Point point = new Point();

                if (KinectManager.StickySkeletonIDs[0] != null)
                    skeletonBW = KinectManager.GetSkeleton(KinectManager.StickySkeletonIDs[0].Value, true);

                if (skeletonBW != null)
                {
                    DepthSpacePoint depthPoint = KinectManager.CoordinateMapper.MapCameraPointToDepthSpace(skeletonBW.Joints[JointType.FootRight].Position);

                    //if (KinectManager.LastImageFormat.Equals(DepthImageFormat.Resolution640x480Fps30))
                    //{
                        point.X = (double)depthPoint.X;
                        point.Y = (double)depthPoint.Y;
                    //}

                    Point box = AIMS.Assets.Lessons2.ColorTracker.find(AIMS.Assets.Lessons2.ColorTracker.YellowTape,
                        new Int32Rect((int)point.X - 48, (int)point.Y, 48, 96), KinectManager.MappedColorData);

                    if (!Double.IsNaN(box.X) && !Double.IsNaN(box.Y))
                    {
                        lesson.BoxPosition = new System.Windows.Media.Media3D.Point3D(box.X, box.Y, 0.0);
                        lesson.BoxStart = new System.Windows.Media.Media3D.Point3D(box.X, box.Y, 0.0);
                        boxWatcher.IsTriggered = true;
                    }
                }
            };
            CurrentStageWatchers.Add(boxWatcher);


            #endregion
        }

        public override void UpdateStage()
        {
            if (!IsSetup)
            {
                SetupStage();
            }

            bool startTimer = true;
            foreach (Watcher w in CurrentStageWatchers)
            {
                startTimer = startTimer && w.IsTriggered;
            }

            if (startTimer)
            {
                Body skeleton = KinectManager.GetSkeleton(currentStuckSkeleton.Value, true);
                if (skeleton != null)
                {
                    // Starting height from the head position
                    CameraSpacePoint p = skeleton.Joints[JointType.Head].Position;
                    lesson.StartingHeight = new System.Windows.Media.Media3D.Point3D(p.X, p.Y, p.Z);

                    // Base is the average of the two ankles
                    p = skeleton.Joints[JointType.AnkleLeft].Position;
                    CameraSpacePoint p2 = skeleton.Joints[JointType.AnkleRight].Position;                    
                    lesson.StartingBase = new System.Windows.Media.Media3D.Point3D((p.X + p2.X) / 2, (p.Y + p2.Y) / 2, (p.Z + p2.Z) / 2);

                    //p = skeleton.Position;
                    p = skeleton.Joints[JointType.SpineBase].Position;
                    lesson.PreviousPosition = new System.Windows.Media.Media3D.Point3D(p.X, p.Y, p.Z);

                    DepthSpacePoint dp = KinectManager.CoordinateMapper.MapCameraPointToDepthSpace(skeleton.Joints[JointType.SpineMid].Position);
                    //v2 sample the depth image at the row/column in question, and use that value (which is depth in millimeters) directly as z.
                    lesson.StartingHipHeight = new System.Windows.Media.Media3D.Point3D(dp.X, dp.Y, (skeleton.Joints[JointType.SpineMid].Position.Z * 1000));

                    this.Complete = true;
                    EndStage();
                }
            }
        }

        public override void EndStage( bool playSound = true )
        {
            Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            this.EndTime = DateTime.Now;

            this.StageScore = new StageScore();

            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                StageScore ss = (StageScore)args.Argument;

                ss.ScreenShot = KinectManager.CreateScreenshot();
                // Set the hasScreenshot variable if the screenshot was created successfully.
                if (ss.ScreenShot == null)
                    ss.HasScreenshot = false;
                else ss.HasScreenshot = true;
            };

            worker.RunWorkerAsync(this.StageScore);

            this.StageScore.StageNumber = this.StageNumber + lesson.CompletedReps * VerticalLift.NUM_STAGES;
            this.StageScore.StageName = this.Name;
            this.StageScore.Score = 1.00;

            // store how much time was spent in this stage.
            this.StageScore.TimeInStage = TimeSpan.Zero;

            this.StageScore.Target = this.Instruction;
            this.lesson.Result.StageScores.Add(this.StageScore);
            this.lesson.Running = true;
        }

        public override void ResetStage()
        {
            throw new NotImplementedException();
        }

        public override void PauseStage()
        {
            //throw new NotImplementedException();
        }

        public override void ResumeStage()
        {
            //throw new NotImplementedException();
        }

        public override void WipeClean()
        {
            this.IsSetup = false;
            this.Complete = false;
        }

        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }

        public override void OnStageTimeout()
        {
            throw new NotImplementedException();
        }

        public override void OnStageVideoRequest()
        {
            throw new NotImplementedException();
        }
    }
}
