﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Kinect;
using System.Windows.Media.Media3D;
using System.Windows;
using System.ComponentModel;

namespace AIMS.Tasks.VerticalLift
{
    /// <summary>
    /// The user needs to bend down to pick up the object. Makes sure the back stays straight.
    /// </summary>
    class VerticalLiftDown : Stage
    {
        private VerticalLift lesson;
        private bool bentBack = false;

        public VerticalLiftDown(VerticalLift lesson)
        {
            this.lesson = lesson;

            StageNumber = 1;
            Name = "1. Down";
            Instruction = "Keep your back straight";
            TargetTime = TimeSpan.MaxValue;
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Tasks/VerticalLift/Graphics/VerticalLift001.png";
            Active = false;
            Complete = false;
            ScoreWeight = 0;
            Zones = new List<Zone>();
            Stopwatch = new System.Diagnostics.Stopwatch();
            TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            TimeOutTime = TimeSpan.FromSeconds(30.0);
            CanTimeOut = false;
        }

        public override void SetupStage()
        {
            lesson.StageName = Name;
            lesson.StageInstruction = Instruction;
            lesson.StageImagePath = lesson.StageImagePath = new Uri(ImagePath, UriKind.Relative);

            Zones.Clear();

            CurrentStageWatchers = new List<BaseWatcher>();
            PeripheralStageWatchers = new List<BaseWatcher>();
            OutlyingStageWatchers = new List<BaseWatcher>();

            CreateStageWatchers();

            StartTime = DateTime.Now;
            this.Stopwatch.Restart();
            this.TimeoutStopwatch.Restart();
            IsSetup = true;
        }

        private void CreateStageWatchers()
        {
            // Watches for the hands to get close to the feet
            #region Hand Height Watcher
            Watcher HandHeight = new Watcher();
            HandHeight.IsTriggered = false;
            HandHeight.CheckForActionDelegate = () =>
            {
                // Smooth out the skeleton movements by finding the average over the last few frames
                if (lesson.SkeletonQueue.Count == VerticalLift.SMOOTHING_FACTOR)
                {
                    Point3D leftHand = new Point3D();
                    Point3D rightHand = new Point3D();

                    foreach (Body skeleton in lesson.SkeletonQueue)
                    {
                        leftHand.X += skeleton.Joints[JointType.HandLeft].Position.X;
                        leftHand.Y += skeleton.Joints[JointType.HandLeft].Position.Y;
                        leftHand.Z += skeleton.Joints[JointType.HandLeft].Position.Z;

                        rightHand.X += skeleton.Joints[JointType.HandRight].Position.X;
                        rightHand.Y += skeleton.Joints[JointType.HandRight].Position.Y;
                        rightHand.Z += skeleton.Joints[JointType.HandRight].Position.Z;
                    }

                    // Divide by the smoothing factor to get the average
                    leftHand.X /= VerticalLift.SMOOTHING_FACTOR;
                    leftHand.Y /= VerticalLift.SMOOTHING_FACTOR;
                    leftHand.Z /= VerticalLift.SMOOTHING_FACTOR;

                    rightHand.X /= VerticalLift.SMOOTHING_FACTOR;
                    rightHand.Y /= VerticalLift.SMOOTHING_FACTOR;
                    rightHand.Z /= VerticalLift.SMOOTHING_FACTOR;

                    // Average the height of the hands
                    Point3D hands = new Point3D((leftHand.X + rightHand.X) / 2, (leftHand.Y + rightHand.Y) / 2, (leftHand.Z + rightHand.Z) / 2);

                    HandHeight.IsTriggered = hands.Y - lesson.StartingBase.Y < 0.25;                    
                }
                else
                    HandHeight.IsTriggered = false;
            };
            CurrentStageWatchers.Add(HandHeight);
            #endregion

            Watcher BackAngle = new Watcher();
            BackAngle.CheckForActionDelegate = () =>
            {
                if (BackAngle.IsTriggered == false)
                {
                    // Smooth the skeleton motion by taking the average of the past few positions
                    double shoulderCenter = 0.0;
                    double kneeCenter = 0.0;
                    foreach (Body skeleton in lesson.SkeletonQueue)
                    {
                        shoulderCenter = shoulderCenter + skeleton.Joints[JointType.SpineShoulder].Position.Z;

                        kneeCenter = kneeCenter + skeleton.Joints[JointType.KneeLeft].Position.Z;
                        kneeCenter = kneeCenter + skeleton.Joints[JointType.KneeRight].Position.Z;
                    }
                    shoulderCenter = shoulderCenter / lesson.SkeletonQueue.Count;
                    kneeCenter = kneeCenter / (lesson.SkeletonQueue.Count * 2);

                    // Find the difference in Z depth 
                    if (shoulderCenter - kneeCenter < -0.1)
                    {
                        BackAngle.IsTriggered = true;
                        bentBack = true;
                    }
                }
            };
            CurrentStageWatchers.Add(BackAngle);
        }

        public override void UpdateStage()
        {
            if (!IsSetup)
            {
                SetupStage();
            }

            // Check that there is a skeleton, use the closest one to the skeleton's last know position if there is not one
            if (KinectManager.StickySkeletonIDs[0] == null)
            {
                for (int i = 0; i < KinectManager.Bodies.Length; i++)
                {
                    CameraSpacePoint p = KinectManager.Bodies[i].Joints[JointType.SpineShoulder].Position;
                    Point3D offset = new Point3D(p.X - lesson.PreviousPosition.X, p.Y - lesson.PreviousPosition.Y, p.Z - lesson.PreviousPosition.Z);
                    double distance = System.Math.Sqrt(offset.X * offset.X + offset.Y * offset.Y + offset.Z * offset.Z);

                    if (distance < 0.5 && KinectManager.Bodies[i].IsTracked)
                    {
                        KinectManager.NumStickySkeletons = 0;
                        KinectManager.NumStickySkeletons = 1;
                        KinectManager.ForceStickySkeleton(KinectManager.Bodies[i].TrackingId);
                        break;
                    }
                }
            }

            foreach (Watcher w in CurrentStageWatchers)
            {
            }

            if (CurrentStageWatchers[0].IsTriggered)
            {
                this.Complete = true;
                EndStage();
            }
        }

        public override void EndStage( bool playSound = true )
        {
            Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            this.EndTime = DateTime.Now;

            StageScore stagescore = new StageScore();

            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                StageScore ss = (StageScore)args.Argument;

                ss.ScreenShot = KinectManager.CreateScreenshot();
                // Set the hasScreenshot variable if the screenshot was created successfully.
                if (ss.ScreenShot == null)
                    ss.HasScreenshot = false;
                else ss.HasScreenshot = true;
            };

            worker.RunWorkerAsync(this.StageScore);
                        
            this.StageScore.StageNumber = this.StageNumber + lesson.CompletedReps * VerticalLift.NUM_STAGES;
            this.StageScore.StageName = this.Name;

            if (bentBack == true)
            {
                this.StageScore.Score = 0.0;
                this.StageScore.Result = "Bent Back";
                //lesson.RepResults[lesson.CompletedReps].BackStraight = false;
                lesson.VertLiftResult[lesson.CompletedReps].BackStraight = false;
            }
            else
                this.StageScore.Score = 1.0;

            // store how much time was spent in this stage.
            this.StageScore.TimeInStage = TimeSpan.Zero;

            this.StageScore.Target = this.Instruction;
            this.lesson.Result.StageScores.Add(this.StageScore);
            this.lesson.Running = true;
        }

        public override void ResetStage()
        {
            throw new NotImplementedException();
        }

        public override void PauseStage()
        {
            throw new NotImplementedException();
        }

        public override void ResumeStage()
        {
            throw new NotImplementedException();
        }

        public override void WipeClean()
        {
            this.bentBack = false;
            this.IsSetup = false;
            this.Complete = false;
        }

        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }

        public override void OnStageTimeout()
        {
            throw new NotImplementedException();
        }

        public override void OnStageVideoRequest()
        {
            throw new NotImplementedException();
        }
    }
}
