﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Kinect;
using System.Windows;
using System.ComponentModel;
using System.Windows.Media.Media3D;

namespace AIMS.Tasks.VerticalLift
{
    /// <summary>
    /// The user lifts the object
    /// </summary>
    class VerticalLiftLiftObject : Stage
    {
        private VerticalLift lesson;
        private bool bentBack = false;
        private bool boxToFar = false;
        private FootSpacingEnum footSpacing = FootSpacingEnum.Ok;

        public VerticalLiftLiftObject(VerticalLift lesson)
        {
            this.lesson = lesson;

            StageNumber = 2;
            Name = "2. Lift Object";
            Instruction = "Keep your back straight";
            TargetTime = TimeSpan.MaxValue;
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Tasks/VerticalLift/Graphics/VerticalLift002.png";
            Active = false;
            Complete = false;
            ScoreWeight = 0;
            Zones = new List<Zone>();
            Stopwatch = new System.Diagnostics.Stopwatch();
            TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            TimeOutTime = TimeSpan.FromSeconds(30.0);
            CanTimeOut = false;
        }
        public override void SetupStage()
        {
            lesson.StageName = Name;
            lesson.StageInstruction = Instruction;
            lesson.StageImagePath = lesson.StageImagePath = new Uri(ImagePath, UriKind.Relative);

            Zones.Clear();

            CurrentStageWatchers = new List<BaseWatcher>();
            PeripheralStageWatchers = new List<BaseWatcher>();
            OutlyingStageWatchers = new List<BaseWatcher>();

            CreateStageWatchers();

            StartTime = DateTime.Now;
            this.Stopwatch.Restart();
            this.TimeoutStopwatch.Restart();
            IsSetup = true;
        }

        private void CreateStageWatchers()
        {

            #region Box Height Watcher
            // Watches for the box to rise to hip level
            Watcher boxHeight = new Watcher();
            boxHeight.CheckForActionDelegate = () =>
            {
                Point box = AIMS.Assets.Lessons2.ColorTracker.find(AIMS.Assets.Lessons2.ColorTracker.YellowTape,
                    new Int32Rect((int)lesson.BoxPosition.X - 32, (int)lesson.BoxPosition.Y - 64, 64, 128), KinectManager.MappedColorData);

                System.Diagnostics.Debug.WriteLine("Box Position {0}, {1}. Starting hip height {2}", box.X, box.Y, lesson.StartingHipHeight.Y);

                if (!Double.IsNaN(box.X) && !Double.IsNaN(box.Y))
                {
                    double zValue = -1.0;                   

                    int zIndex = (int)(box.Y * 640 + box.X);

                    // Get depth
                    double value = KinectManager.GetDepthValue((int)box.X, (int)box.Y);
                    if (value >= KinectManager.ReliableMinDepth && value <= KinectManager.ReliableMaxDepth)
                    {
                        zValue = value;
                    }
                    lesson.BoxPosition = new System.Windows.Media.Media3D.Point3D(box.X, box.Y, zValue);

                    boxHeight.IsTriggered = lesson.BoxPosition.Y - lesson.StartingHipHeight.Y < 70;
                }
            };
            CurrentStageWatchers.Add(boxHeight);
            #endregion

            #region Back Angle Watcher
            Watcher BackAngle = new Watcher();
            BackAngle.CheckForActionDelegate = () =>
            {
                if (BackAngle.IsTriggered == false)
                {
                    // Smooth the skeleton motion by taking the average of the past few positions
                    double shoulderCenter = 0.0;
                    double kneeCenter = 0.0;
                    foreach (Body skeleton in lesson.SkeletonQueue)
                    {
                        shoulderCenter = shoulderCenter + skeleton.Joints[JointType.SpineShoulder].Position.Z;

                        kneeCenter = kneeCenter + skeleton.Joints[JointType.KneeLeft].Position.Z;
                        kneeCenter = kneeCenter + skeleton.Joints[JointType.KneeRight].Position.Z;
                    }
                    shoulderCenter = shoulderCenter / lesson.SkeletonQueue.Count;
                    kneeCenter = kneeCenter / (lesson.SkeletonQueue.Count * 2);

                    // Find the difference in Z depth 
                    if (shoulderCenter - kneeCenter < -0.1)
                    {
                        BackAngle.IsTriggered = true;
                        bentBack = true;
                    }
                }
            };
            CurrentStageWatchers.Add(BackAngle);
            #endregion

            #region Box depth watcher
            Watcher BoxDepth = new Watcher();
            BoxDepth.CheckForActionDelegate = () =>
            {
                if ((lesson.SkeletonQueue.Last().Joints[JointType.SpineShoulder].Position.Z - (lesson.BoxPosition.Z / 1000)) > 0.30)
                {
                    BoxDepth.IsTriggered = true;
                    boxToFar = true;
                }
            };
            CurrentStageWatchers.Add(BoxDepth);
            #endregion
        }

        private double findAngle2(CameraSpacePoint a, CameraSpacePoint b, CameraSpacePoint c)
        {
            Point3D v1 = new Point3D(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
            Point3D v2 = new Point3D(c.X - b.X, c.Y - b.Y, c.Z - b.Z);

            v1 = normalize(v1);
            v2 = normalize(v2);

            double dotProduct = v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;

            return (System.Math.Acos(dotProduct) * 180 / System.Math.PI);
        }

        private double findAngle3(CameraSpacePoint a, CameraSpacePoint b, CameraSpacePoint c)
        {
            Point3D v1 = new Point3D(b.X - a.X, b.Y - a.Y, b.Z - a.Z);
            Point3D v2 = new Point3D(c.X - b.X, c.Y - b.Y, c.Z - b.Z);

            v1 = normalize(v1);
            v2 = normalize(v2);

            double dotProduct = v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;

            return (System.Math.Acos(dotProduct) * 180 / System.Math.PI);
        }

        private Point3D normalize(Point3D vector)
        {
            double magnitude = vector.X * vector.X + vector.Y * vector.Y + vector.Z * vector.Z;
            magnitude = System.Math.Sqrt(magnitude);

            return new Point3D(vector.X / magnitude, vector.Y / magnitude, vector.Z / magnitude);
        }

        public override void UpdateStage()
        {
            if (!IsSetup)
            {
                SetupStage();
            }

            // Check that there is a skeleton, use the closest one to the skeleton's last known position if there is not one
            if (KinectManager.StickySkeletonIDs[0] == null)
            {
                for (int i = 0; i < KinectManager.Bodies.Length; i++)
                {
                    CameraSpacePoint p = KinectManager.Bodies[i].Joints[JointType.SpineBase].Position;
                    Point3D offset = new Point3D(p.X - lesson.PreviousPosition.X, p.Y - lesson.PreviousPosition.Y, p.Z - lesson.PreviousPosition.Z);
                    double distance = System.Math.Sqrt(offset.X * offset.X + offset.Y * offset.Y + offset.Z * offset.Z);

                    if (distance < 0.2)
                    {
                        KinectManager.NumStickySkeletons = 0;
                        KinectManager.NumStickySkeletons = 1;
                        KinectManager.ForceStickySkeleton(KinectManager.Bodies[i].TrackingId);
                        break;
                    }
                }
            }

            foreach (Watcher w in CurrentStageWatchers)
            {
            }

            if (CurrentStageWatchers[0].IsTriggered)
            {
                this.Complete = true;
                EndStage();
            }
        }

        public override void EndStage( bool playSound = true )
        {
            Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            this.EndTime = DateTime.Now;

            StageScore stagescore = new StageScore();

            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                StageScore ss = (StageScore)args.Argument;

                ss.ScreenShot = KinectManager.CreateScreenshot();
                // Set the hasScreenshot variable if the screenshot was created successfully.
                if (ss.ScreenShot == null)
                    ss.HasScreenshot = false;
                else ss.HasScreenshot = true;
            };

            worker.RunWorkerAsync(this.StageScore);
            
            this.StageScore.StageNumber = this.StageNumber + lesson.CompletedReps * VerticalLift.NUM_STAGES;
            this.StageScore.StageName = this.Name;
            this.StageScore.Score = 1.0;

            if (bentBack)
            {
                stagescore.Score -= 0.5;
                this.lesson.VertLiftResult[this.lesson.CompletedReps].BackStraight = false;
          //      lesson.RepResults[lesson.CompletedReps].BackStraight = false;
                this.StageScore.Result = "Bent Back";
            }

            // Check the foot width
            double footWidth = 0.0;
            double shoulderWidth = 0.0;
            int count = 0;
            foreach (Body skeleton in lesson.SkeletonQueue)
            {
                // Make sure all relevant joints are tracked
                if (skeleton.Joints[JointType.AnkleRight].TrackingState == TrackingState.Tracked &&
                    skeleton.Joints[JointType.AnkleLeft].TrackingState == TrackingState.Tracked &&
                    skeleton.Joints[JointType.ShoulderRight].TrackingState == TrackingState.Tracked &&
                    skeleton.Joints[JointType.ShoulderLeft].TrackingState == TrackingState.Tracked)
                {
                    footWidth = footWidth + (skeleton.Joints[JointType.AnkleRight].Position.X - skeleton.Joints[JointType.AnkleLeft].Position.X);
                    shoulderWidth = shoulderWidth + (skeleton.Joints[JointType.ShoulderRight].Position.X - skeleton.Joints[JointType.ShoulderLeft].Position.X);
                    count++;
                }

            }
            footWidth = footWidth / count;
            shoulderWidth = shoulderWidth / count;
            double ratio = footWidth / shoulderWidth;

            if (ratio > 1.65)
                footSpacing = FootSpacingEnum.Wide;
            else if (ratio < 0.95)
                footSpacing = FootSpacingEnum.Narrow;

            this.lesson.VertLiftResult[this.lesson.CompletedReps].FootSpacing = this.footSpacing;
        //    lesson.RepResults[lesson.CompletedReps].FootSpacing = footSpacing;

            if (boxToFar)
            {
                this.lesson.VertLiftResult[this.lesson.CompletedReps].CloseToBody = false;
            //    lesson.RepResults[lesson.CompletedReps].CloseToBody = false;
            }
            else
            {
                this.lesson.VertLiftResult[this.lesson.CompletedReps].CloseToBody = true;
         //       lesson.RepResults[lesson.CompletedReps].CloseToBody = true;
            }

            // store how much time was spent in this stage.
            this.StageScore.TimeInStage = TimeSpan.Zero;

            this.StageScore.Target = this.Instruction;
            this.lesson.Result.StageScores.Add(this.StageScore);
            this.lesson.Running = true;
        }

        public override void ResetStage()
        {
            throw new NotImplementedException();
        }

        public override void PauseStage()
        {
            throw new NotImplementedException();
        }

        public override void ResumeStage()
        {
            throw new NotImplementedException();
        }

        public override void WipeClean()
        {
            footSpacing = FootSpacingEnum.Ok;
            this.bentBack = false;
            this.IsSetup = false;
            this.Complete = false;
        }

        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }

        public override void OnStageTimeout()
        {
            throw new NotImplementedException();
        }

        public override void OnStageVideoRequest()
        {
            throw new NotImplementedException();
        }
    }
}
