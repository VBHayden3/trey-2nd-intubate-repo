﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIMS.Tasks.VerticalLift
{
    public class VerticalLiftRep
    {
        #region Public Properties

        /// <summary>
        /// Whether or not the user kept their back straight during a vertical lift.
        /// </summary>
        public bool BackStraight { get { return backStraight; } set { backStraight = value; } }
        /// <summary>
        /// Whether or not the user kept the object close to their body during a vertical lift.
        /// </summary>
        public bool CloseToBody { get { return closeToBody; } set { closeToBody = value; } }
        /// <summary>
        /// The stance the user's feet were in during a vertical lift.
        /// </summary>
        public FootSpacingEnum FootSpacing { get { return footSpacing; } set { footSpacing = value; } }
        /// <summary>
        /// Overall score of a vertical lift.
        /// </summary>
        public double Score { get { return score; } set { score = value; } }
        /// <summary>
        /// Image ID for the image taken during the Down stage of Vertical Lift.
        /// </summary>
        public int? DownImageID { get { return this.downImageID; } set { this.downImageID = value; } }
        /// <summary>
        /// Image for the Down stage of Vertical Lift.
        /// </summary>
        public KinectImage DownImage { get { return this.downImage; } set { this.downImage = value; } }
        /// <summary>
        /// Image ID for the image taken during the LiftObject stage of Vertical Lift.
        /// </summary>
        public int? LiftObjectImageID { get { return this.liftObjectImageID; } set { this.liftObjectImageID = value; } }
        /// <summary>
        /// Image for the LiftObject stage of Vertical Lift.
        /// </summary>
        public KinectImage LiftObjectImage { get { return this.liftObjectImage; } set { this.liftObjectImage = value; } }
        /// <summary>
        /// Image ID for the image taken during the LowerObject stage of Vertical Lift.
        /// </summary>
        public int? LowerObjectImageID { get { return this.lowerObjectImageID; } set { this.lowerObjectImageID = value; } }
        /// <summary>
        /// Image for the LowerObject stage of Vertical Lift.
        /// </summary>
        public KinectImage LowerObjectImage { get { return this.lowerObjectImage; } set { this.lowerObjectImage = value; } }
        /// <summary>
        /// Image ID for the image taken during the Rise stage of Vertical Lift.
        /// </summary>
        public int? RiseImageID { get { return this.riseImageID; } set { this.riseImageID = value; } }
        /// <summary>
        /// Image for the Rise stage of Vertical Lift.
        /// </summary>
        public KinectImage RiseImage { get { return this.riseImage; } set { this.riseImage = value; } }
        /// <summary>
        /// Collection of feedback IDs associated with this repetition of Vertical Lift.
        /// </summary>
        public List<int> RepFeedbackIDs { get { return this.repFeedbackIDs; } set { this.repFeedbackIDs = value; } }

        #endregion

        #region Private Fields

        private bool closeToBody = true;
        private bool backStraight = true;
        private FootSpacingEnum footSpacing = FootSpacingEnum.Ok;
        private double score = 0.0;
        private int? downImageID = 0;
        private KinectImage downImage = null;
        private int? liftObjectImageID = 0;
        private KinectImage liftObjectImage = null;
        private int? lowerObjectImageID = 0;
        private KinectImage lowerObjectImage = null;
        private int? riseImageID = 0;
        private KinectImage riseImage = null;
        private List<int> repFeedbackIDs = new List<int>();

        #endregion

        #region Public Functions

        /// <summary>
        /// Updates the score based on values inherent to this repetition.
        /// </summary>
        public void CalculateScore()
        {
            int correct = 0;

            if (closeToBody)
                correct++;
            if (backStraight)
                correct++;
            if (footSpacing == FootSpacingEnum.Ok)
                correct++;

            Score = correct / 3.0;
        }

        #endregion
    }
}
