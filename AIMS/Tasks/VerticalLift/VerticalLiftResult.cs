﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using NpgsqlTypes;


namespace AIMS.Tasks.VerticalLift
{
    public class VerticalLiftResult : TaskResult
    {
        #region Public Properties

        /// <summary>
        /// The percentage that the back was straight across all repetitions of vertical lift.
        /// </summary>
        public double AvgBackStraight { get { return this.avgBackStraight; } set { this.avgBackStraight = value; } }
        /// <summary>
        /// The percentage that the lifted object was close enough to the body across all repetitions of vertical lift.
        /// </summary>
        public double AvgCloseToBody { get { return this.avgCloseToBody; } set { this.avgCloseToBody = value; } }
        /// <summary>
        /// The percentage that the user's feet were in a good stance across all repetitions of vertical lift.
        /// </summary>
        public double AvgFootSpacing { get { return this.avgFootSpacing; } set { this.avgFootSpacing = value; } }
        /// <summary>
        /// uid is given by postgresql upon insertion using a self-incrementing "serial" int. It will be 0, unless otherwise specified - indicating it's uid in the postgresql database.
        /// </summary>
        public int? CalibrationImageID { get { return calibrationImageID; } set { calibrationImageID = value; } }
        /// <summary>
        /// Calibration Image. The image taken at calibration of the vertical lift task.
        /// </summary>
        public KinectImage CalibrationImage { get { return calibrationImage; } set { calibrationImage = value; } }
        /// <summary>
        /// Holds feedback uid's pertaining to the feedback strings relevant to this run - which are being stored in the database's lookup table.
        /// </summary>
        public List<int> OverallFeedbackIDs { get { return overallFeedbackIDs; } set { overallFeedbackIDs = value; } }
        /// <summary>
        /// VerticalLiftResult.Repetitions accessed with an index. Each rep contains details for a vertical lift.
        /// </summary>
        /// <param name="Indx">Which repetition to return.</param>
        /// <returns>A repetition of vertical lift, identified by indx.</returns>
        public VerticalLiftRep this[int Indx]
        {
            get
            {
                if (Indx < this.repetitions.Count && Indx >= 0)
                {
                    if (this.repetitions[Indx] != null)
                        return this.repetitions[Indx];
                }
                return null;
            }
        }
        /// <summary>
        /// Returns the size of the Repetitions list.
        /// </summary>
        public int RepetitionCount { get { return this.repetitions.Count; } }
        /// <summary>
        /// Gives access to get / set the Repetitions list directly. The repetitions list is the collection of vertical lift repetitions.
        /// </summary>
        public List<VerticalLiftRep> Repetetions { get { return this.repetitions; } set { this.repetitions = value; } }

        #endregion
        
        #region Private Fields and Constants

        private const string OVERALL_RESULTS_TABLE_NAME = "taskresults";
        private const string VERTICAL_LIFT_RESULTS_TABLE_NAME = "taskresults_verticallift";
        private const string VERTICAL_LIFT_RESULTS_REPS_TABLE_NAME = "taskresults_verticallift_reps";
        private const string IMAGES_TABLE_NAME = "kinect_images";

        private double avgBackStraight = 0;
        private double avgCloseToBody = 0;
        private double avgFootSpacing = 0;
        private int? calibrationImageID = 0;
        private KinectImage calibrationImage = null;
        private List<int> overallFeedbackIDs = new List<int>();
        private List<VerticalLiftRep> repetitions = new List<VerticalLiftRep>();

        #endregion

        /// <summary>
        /// Create a new VerticalLiftResult.
        /// </summary>
        public VerticalLiftResult()
            :base()
        {
            this.taskType = Task.VerticalLift;
        }

        #region Public Functions

        /// <summary>
        /// Add a repetition to the vertical list result.
        /// </summary>
        /// <param name="Rep"></param>
        public void AddRepetition(VerticalLiftRep Rep)
        {
            this.repetitions.Add(Rep);
        }

        #region Database Interaction Methods

        public static int StoreResult(VerticalLiftResult Result)
        {
            int id = 0;

            if (Result == null)
                return id;

            // Submit the overall result information to the overall results table - returning it's generated uid.
            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "INSERT INTO " + OVERALL_RESULTS_TABLE_NAME +
                " ( studentid, assignmentid, tasktype, masterylevel, " +
                "taskmode, score, starttime, endtime ) " +
                "VALUES ( :studentid, :assignmentid, CAST(:tasktype as \"TaskType\"), CAST(:masterylevel as \"MasteryLevel\"), " +
                "CAST(:taskmode as \"TaskMode\"), :score, :starttime, :endtime ) RETURNING uid;";
            command.Parameters.Add("studentid", NpgsqlDbType.Integer).Value = Result.StudentID;
            command.Parameters.Add("assignmentid", NpgsqlDbType.Integer).Value = Result.AssignmentID;
            command.Parameters.Add("tasktype", NpgsqlDbType.Text).Value = Result.TaskType.ToString();
            command.Parameters.Add("masterylevel", NpgsqlDbType.Text).Value = Result.MasteryLevel.ToString();
            command.Parameters.Add("taskmode", NpgsqlDbType.Text).Value = Result.TaskMode.ToString();
            command.Parameters.Add("score", NpgsqlDbType.Double).Value = Result.Score;
            command.Parameters.Add("starttime", NpgsqlDbType.Timestamp).Value = Result.StartTime;
            command.Parameters.Add("endtime", NpgsqlDbType.Timestamp).Value = Result.EndTime;
            try
            {
                // Execute the query, attempting to store the result object, and returning the UID of the created database entry for additional relational usage.
                id = (int)command.ExecuteScalar();
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print("VerticalLiftResult StoreResult {1} ERROR: {0} \n{2}", exc.Message, OVERALL_RESULTS_TABLE_NAME, command.CommandText);
            }

            // Main VerticalLift result info stored.
            if (id > 0)
            {
                try
                {
                    // Store the KinectImages, if they exist, to the images table.
                    Result.CalibrationImageID = KinectImage.StoreImage(Result.CalibrationImage, IMAGES_TABLE_NAME, false);
                }
                catch (Exception exc)
                {
                    System.Diagnostics.Debug.Print("VerticalLiftResult StoreResult {0} ERROR: {1}", IMAGES_TABLE_NAME, exc.Message);
                }
                finally
                {
                    if (Result.CalibrationImageID == 0)
                        Result.CalibrationImageID = null;

                    //System.Diagnostics.Debug.Print("Calibration Image ID {0}", Result.CalibrationImageID);
                }

                int vertLiftResultID = 0;

                command = DatabaseManager.Connection.CreateCommand();
                command.CommandType = System.Data.CommandType.Text;
                command.CommandText = "INSERT INTO " + VERTICAL_LIFT_RESULTS_TABLE_NAME +
                    " (resultid, avgbackstraight, avgclosetobody, avgfootspacing, calibrationimageid, feedbackids ) " +
                    " VALUES (:resultid, :avgbackstraight, :avgclosetobody, :avgfootspacing, :calibrationimageid, :feedbackids ) RETURNING uid;";
                command.Parameters.Add("resultid", NpgsqlDbType.Integer).Value = id;
                command.Parameters.Add("avgbackstraight", NpgsqlDbType.Double).Value = Result.AvgBackStraight;
                command.Parameters.Add("avgclosetobody", NpgsqlDbType.Double).Value = Result.AvgCloseToBody;
                command.Parameters.Add("avgfootspacing", NpgsqlDbType.Double).Value = Result.AvgFootSpacing;
                command.Parameters.Add("calibrationimageid", NpgsqlDbType.Integer).Value = Result.CalibrationImageID;
                command.Parameters.Add("feedbackids", NpgsqlDbType.Integer | NpgsqlDbType.Array).Value = Result.OverallFeedbackIDs.ToArray();

                try
                {
                    // Execute the query, attempting to store the result object, and returning the UID of the created database entry for additional relational usage.
                    vertLiftResultID = (int)command.ExecuteScalar();
                }
                catch (Exception exc)
                {
                    System.Diagnostics.Debug.Print("VerticalLift StoreResult {1} ERROR: {0}", exc.Message, VERTICAL_LIFT_RESULTS_TABLE_NAME);
                }

                // Store the VerticalLift Repetition data for the result.
                if (vertLiftResultID > 0)
                {
                    for (int i = 0; i < Result.RepetitionCount; i++)
                    {
                        // Attempt to store the repetition images.
                        Result[i].DownImageID = KinectImage.StoreImage(Result[i].DownImage, IMAGES_TABLE_NAME, false);
                        Result[i].LiftObjectImageID = KinectImage.StoreImage(Result[i].LiftObjectImage, IMAGES_TABLE_NAME, false);
                        Result[i].LowerObjectImageID = KinectImage.StoreImage(Result[i].LowerObjectImage, IMAGES_TABLE_NAME, false);
                        Result[i].RiseImageID = KinectImage.StoreImage(Result[i].RiseImage, IMAGES_TABLE_NAME, false);

                        // StoreImage returns 0 on error, so if it is 0 make it null.
                        if (Result[i].DownImageID == 0) Result[i].DownImageID = null;
                        if (Result[i].LiftObjectImageID == 0) Result[i].LiftObjectImageID = null;
                        if (Result[i].LowerObjectImageID == 0) Result[i].LowerObjectImageID = null;
                        if (Result[i].RiseImageID == 0) Result[i].RiseImageID = null;

                        command = DatabaseManager.Connection.CreateCommand();
                        command.CommandType = System.Data.CommandType.Text;
                        command.CommandText = "INSERT INTO " + VERTICAL_LIFT_RESULTS_REPS_TABLE_NAME +
                            " (taskresults_verticallift_uid, repid, backstraight, closetobody, footspacing, score, downimageid, " + 
                            " liftobjectimageid, lowerobjectimageid, riseimageid, feedbackids ) " +
                            " VALUES (:taskresults_verticallift_uid, :repid, :backstraight, :closetobody, CAST(:footspacing AS \"FootSpacing\"), :score, :downimageid, " +
                            " :liftobjectimageid, :lowerobjectimageid, :riseimageid, :feedbackids );";
                        command.Parameters.Add("taskresults_verticallift_uid", NpgsqlDbType.Integer).Value = vertLiftResultID;
                        command.Parameters.Add("repid", NpgsqlDbType.Integer).Value = i;
                        command.Parameters.Add("backstraight", NpgsqlDbType.Boolean).Value = Result[i].BackStraight;
                        command.Parameters.Add("closetobody", NpgsqlDbType.Boolean).Value = Result[i].CloseToBody;
                        command.Parameters.Add("footspacing", NpgsqlDbType.Varchar).Value = Result[i].FootSpacing.ToString();
                        command.Parameters.Add("score", NpgsqlDbType.Double).Value = Result[i].Score;
                        command.Parameters.Add("downimageid", NpgsqlDbType.Double).Value = Result[i].DownImageID;
                        command.Parameters.Add("liftobjectimageid", NpgsqlDbType.Integer).Value = Result[i].LiftObjectImageID;
                        command.Parameters.Add("lowerobjectimageid", NpgsqlDbType.Integer).Value = Result[i].LowerObjectImageID;
                        command.Parameters.Add("riseimageid", NpgsqlDbType.Integer).Value = Result[i].RiseImageID;
                        command.Parameters.Add("feedbackids", NpgsqlDbType.Integer | NpgsqlDbType.Array).Value = Result[i].RepFeedbackIDs.ToArray();

                        try
                        {
                            command.ExecuteScalar();
                        }
                        catch (Exception exc)
                        {
                            System.Diagnostics.Debug.Print("VerticalLiftResult StoreResult {1} ERROR: {0}", exc.Message, VERTICAL_LIFT_RESULTS_REPS_TABLE_NAME);
                        }
                    }
                }
            }

            DatabaseManager.CloseConnection();

            return id;
        }

        /// <summary>
        /// Retrieves a result from a resultid.
        /// If retrieveImages is set to true, the result's images will also be pulled and stored in the result object.
        /// </summary>
        /// <param name="resultid"></param>
        /// <param name="retrieveImages"></param>
        /// <returns></returns>
        public static VerticalLiftResult RetrieveResult(int resultid, bool retrieveImages = false)
        {
            if (resultid <= 0)
                return null;

            VerticalLiftResult result = new VerticalLiftResult() { UID = resultid };

            #region Retrieve the basic result information.
            bool resultFound = true;
            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "SELECT studentid, assignmentid, tasktype, masterylevel, taskmode, score, submissiontime, starttime, endtime " + 
                " FROM " + OVERALL_RESULTS_TABLE_NAME + " WHERE uid=:id";

            command.Parameters.Add("id", NpgsqlTypes.NpgsqlDbType.Integer).Value = resultid;

            using (NpgsqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    try
                    {
                        result.StudentID = reader.GetInt32(reader.GetOrdinal("studentid"));
                    }
                    catch (Exception exc) { System.Diagnostics.Debug.Print("VerticalLiftResult RetrieveResult ERROR: {0}", exc.Message); }
                    try
                    {
                        result.AssignmentID = reader.GetInt32(reader.GetOrdinal("assignmentid"));
                    }
                    catch (Exception exc) { System.Diagnostics.Debug.Print("VerticalLiftResult RetrieveResult ERROR: {0}", exc.Message); }
                    try
                    {
                        result.TaskType = (Task)Enum.Parse(typeof(Task), reader.GetString(reader.GetOrdinal("tasktype")));
                    }
                    catch (Exception exc) { System.Diagnostics.Debug.Print("VerticalLiftResult RetrieveResult ERROR: {0}", exc.Message); }
                    try
                    {
                        result.TaskMode = (TaskMode)Enum.Parse(typeof(TaskMode), reader.GetString(reader.GetOrdinal("taskmode")));
                    }
                    catch (Exception exc) { System.Diagnostics.Debug.Print("VerticalLiftResult RetrieveResult ERROR: {0}", exc.Message); }
                    try
                    {
                        result.MasteryLevel = (MasteryLevel)Enum.Parse(typeof(MasteryLevel), reader.GetString(reader.GetOrdinal("masterylevel")));
                    }
                    catch (Exception exc) { System.Diagnostics.Debug.Print("VerticalLiftResult RetrieveResult ERROR: {0}", exc.Message); }
                    try
                    {
                        result.Score = reader.GetDouble(reader.GetOrdinal("score"));
                    }
                    catch (Exception exc) { System.Diagnostics.Debug.Print("VerticalLiftResult RetrieveResult ERROR: {0}", exc.Message); }
                    try
                    {
                        result.StartTime = reader.GetTimeStamp(reader.GetOrdinal("starttime"));
                    }
                    catch (Exception exc) { System.Diagnostics.Debug.Print("VerticalLiftResult RetrieveResult ERROR: {0}", exc.Message); }
                    try
                    {
                        result.EndTime = reader.GetTimeStamp(reader.GetOrdinal("endtime"));
                    }
                    catch (Exception exc) { System.Diagnostics.Debug.Print("VerticalLiftResult RetrieveResult ERROR: {0}", exc.Message); }
                    try
                    {
                        result.SubmissionTime = reader.GetTimeStamp(reader.GetOrdinal("submissiontime"));
                    }
                    catch (Exception exc) { System.Diagnostics.Debug.Print("VerticalLiftResult RetrieveResult ERROR: {0}", exc.Message); }
                }
                // Where results returned?
                if (!reader.HasRows)
                    resultFound = false;
            }

            #endregion

            #region Retrieve the vertical lift specific content.
            // Pull the VerticalLift Result
            if (resultFound)
            {
                int vertLiftUID = 0;

                command = DatabaseManager.Connection.CreateCommand();
                command.CommandType = System.Data.CommandType.Text;
                command.CommandText = "SELECT uid, resultid, avgbackstraight, avgclosetobody, avgfootspacing, calibrationimageid, feedbackids " +
                    " FROM " + VERTICAL_LIFT_RESULTS_TABLE_NAME +
                    " WHERE resultid = :id;";

                command.Parameters.Add("id", NpgsqlTypes.NpgsqlDbType.Integer).Value = resultid;

                using (NpgsqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        try
                        {
                            vertLiftUID = reader.GetInt32(reader.GetOrdinal("uid"));
                        }
                        catch (Exception exc) { System.Diagnostics.Debug.Print("VerticalLiftResult RetrieveResult ERROR: {0}", exc.Message); }
                        try
                        {
                            result.AvgBackStraight = reader.GetDouble(reader.GetOrdinal("avgbackstraight"));
                        }
                        catch (Exception exc) { System.Diagnostics.Debug.Print("VerticalLiftResult RetrieveResult ERROR: {0}", exc.Message); }
                        try
                        {
                            result.AvgCloseToBody = reader.GetDouble(reader.GetOrdinal("avgclosetobody"));
                        }
                        catch (Exception exc) { System.Diagnostics.Debug.Print("VerticalLiftResult RetrieveResult ERROR: {0}", exc.Message); }
                        try
                        {
                            result.AvgFootSpacing = reader.GetDouble(reader.GetOrdinal("avgfootspacing"));
                        }
                        catch (Exception exc) { System.Diagnostics.Debug.Print("VerticalLiftResult RetrieveResult ERROR: {0}", exc.Message); }
                        try
                        {
                            result.CalibrationImageID = reader.GetInt32(reader.GetOrdinal("calibrationimageid"));
                        }
                        catch (Exception exc) { System.Diagnostics.Debug.Print("VerticalLiftResult RetrieveResult ERROR: {0}", exc.Message); }
                        try
                        {
                            result.OverallFeedbackIDs.AddRange(reader.GetValue(reader.GetOrdinal("feedbackids")) as int[]);
                        }
                        catch (Exception exc) { System.Diagnostics.Debug.Print("VerticalLiftResult RetrieveResult ERROR: {0}", exc.Message); }
                    }
                    if (!reader.HasRows)
                        resultFound = false;
                }

                // Pull the repetitions
                if (resultFound)
                {
                    command = DatabaseManager.Connection.CreateCommand();
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = " SELECT repid, backstraight, closetobody, footspacing, score, downimageid, liftobjectimageid, lowerobjectimageid, " +
                        " riseimageid, feedbackids " +
                        " FROM " + VERTICAL_LIFT_RESULTS_REPS_TABLE_NAME +
                        " WHERE taskresults_verticallift_uid = :taskresults_verticallift_uid " +
                        " ORDER BY repid ASC;";
                    command.Parameters.Add("taskresults_verticallift_uid", NpgsqlDbType.Integer).Value = vertLiftUID;

                    using (Npgsql.NpgsqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            try
                            {
                                VerticalLiftRep vertLiftRepetition = new VerticalLiftRep()
                                {
                                    BackStraight = reader.GetBoolean(reader.GetOrdinal("backstraight")),
                                    CloseToBody = reader.GetBoolean(reader.GetOrdinal("closetobody")),
                                    FootSpacing = (FootSpacingEnum)Enum.Parse(typeof(FootSpacingEnum), reader.GetString(reader.GetOrdinal("footspacing"))),
                                    Score = reader.GetDouble(reader.GetOrdinal("score")),
                                    DownImageID = reader.IsDBNull(reader.GetOrdinal("downimageid")) ? null : (int?)reader.GetInt32(reader.GetOrdinal("downimageid")),
                                    LiftObjectImageID = reader.IsDBNull(reader.GetOrdinal("liftobjectimageid")) ? null :
                                        (int?)reader.GetInt32(reader.GetOrdinal("liftobjectimageid")),
                                    LowerObjectImageID = reader.IsDBNull(reader.GetOrdinal("lowerobjectimageid")) ? null :
                                        (int?)reader.GetInt32(reader.GetOrdinal("lowerobjectimageid")),
                                    RiseImageID = reader.IsDBNull(reader.GetOrdinal("riseimageid")) ? null :
                                        (int?)reader.GetInt32(reader.GetOrdinal("riseimageid"))
                                };
                                vertLiftRepetition.RepFeedbackIDs.AddRange(reader.GetValue(reader.GetOrdinal("feedbackids")) as int[]);
                                result.AddRepetition(vertLiftRepetition);
                            }
                            catch (Exception exc) 
                            { 
                                System.Diagnostics.Debug.Print("VerticalLiftResult RetrieveResult {0} ERROR: {1}", VERTICAL_LIFT_RESULTS_REPS_TABLE_NAME, exc.Message); 
                            }
                        }
                    }
                }
            }

            #endregion

            // Pull images if retrieveImages parameter is true.
            if (retrieveImages)
                VerticalLiftResult.RetrieveImages(ref result);

            DatabaseManager.CloseConnection();

            return result;
        }

        /// <summary>
        /// Retrieve images from the database and store them in the passed VerticalLiftResult object.
        /// </summary>
        /// <param name="Result">VerticalLiftResult object to retrieve images for.</param>
        public static void RetrieveImages(ref VerticalLiftResult Result)
        {
            List<int> imageIDs = new List<int>();

            // Collect IDs of images to retrieve.
            if (Result.CalibrationImage == null && Result.CalibrationImageID.HasValue)
                imageIDs.Add(Result.CalibrationImageID.Value);
            for (int i = 0; i < Result.RepetitionCount; i++)
            {
                if (Result[i].DownImage == null && Result[i].DownImageID.HasValue)
                    imageIDs.Add(Result[i].DownImageID.Value);
                if (Result[i].LiftObjectImage == null && Result[i].LiftObjectImageID.HasValue)
                    imageIDs.Add(Result[i].LiftObjectImageID.Value);
                if (Result[i].LowerObjectImage == null && Result[i].LowerObjectImageID.HasValue)
                    imageIDs.Add(Result[i].LowerObjectImageID.Value);
                if (Result[i].RiseImage == null && Result[i].RiseImageID.HasValue)
                    imageIDs.Add(Result[i].RiseImageID.Value);
            }

            // Retrieve images.
            Dictionary<int, KinectImage> images = KinectImage.RetreiveImages(imageIDs.ToArray(), IMAGES_TABLE_NAME);

            // Put images in the result object.
            if (Result.CalibrationImage == null && Result.CalibrationImageID.HasValue)
            {
                try
                {
                    imageIDs.Add(Result.CalibrationImageID.Value);
                }
                catch(Exception exc) { System.Diagnostics.Debug.Print("VerticalLiftResult RetrieveImages ERROR: {0}", exc.Message); }
            }
            for (int i = 0; i < Result.RepetitionCount; i++)
            {
                if (Result[i].DownImage == null && Result[i].DownImageID.HasValue)
                {
                    try
                    {
                        imageIDs.Add(Result[i].DownImageID.Value);
                    }
                    catch(Exception exc) { System.Diagnostics.Debug.Print("VerticalLiftResult RetrieveImages ERROR: {0}", exc.Message); }
                }
                if (Result[i].LiftObjectImage == null && Result[i].LiftObjectImageID.HasValue)
                {
                    try
                    {
                        imageIDs.Add(Result[i].LiftObjectImageID.Value);
                    }
                    catch(Exception exc) { System.Diagnostics.Debug.Print("VerticalLiftResult RetrieveImages ERROR: {0}", exc.Message); }
                }
                if (Result[i].LowerObjectImage == null && Result[i].LowerObjectImageID.HasValue)
                {
                    try
                    {
                        imageIDs.Add(Result[i].LowerObjectImageID.Value);
                    }
                    catch(Exception exc) { System.Diagnostics.Debug.Print("VerticalLiftResult RetrieveImages ERROR: {0}", exc.Message); }
                }
                if (Result[i].RiseImage == null && Result[i].RiseImageID.HasValue)
                {
                    try
                    {
                        imageIDs.Add(Result[i].RiseImageID.Value);
                    }
                    catch (Exception exc) { System.Diagnostics.Debug.Print("VerticalLiftResult RetrieveImages ERROR: {0}", exc.Message); }
                }
            }
        }
        
        #endregion

        #endregion

    } // End class VerticalLiftResult


} // End namespace AIMS.Tasks.VerticalLift2
