﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using Microsoft.Kinect;

namespace AIMS.Tasks.VerticalLift
{
    public class VerticalLiftResults : Stage
    {
        private VerticalLift lesson;
        public VerticalLift Lesson { get { return lesson; } set { lesson = value; } }

        private CameraSpacePoint initialSkeletonDepth = new CameraSpacePoint();
        public CameraSpacePoint InitialSkeletonDepth { get { return initialSkeletonDepth; } set { initialSkeletonDepth = value; } }

        public VerticalLiftResults(VerticalLift lesson)
        {
            Lesson = lesson;

            StageNumber = 10;
            Name = "View Results";
            Instruction = "Lesson complete. Step back to view your results.";
            TargetTime = TimeSpan.MaxValue;
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Assets/Graphics/checklist-icon-psd.jpeg";
            Active = false;
            Complete = false;
            ScoreWeight = 0;
            Zones = new List<Zone>();
            IsSetup = false;
            Stopwatch = new System.Diagnostics.Stopwatch();
            this.TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            this.CanTimeOut = false;
            this.TimeOutTime = TimeSpan.FromSeconds(10.0);
        }

        public override void SetupStage()
        {
            lesson.StageName = Name;
            lesson.StageInstruction = Instruction;
            lesson.StageImagePath = lesson.StageImagePath = new Uri(ImagePath, UriKind.Relative);

            Zones.Clear();

            if (KinectManager.ClosestSkeletonProfile != null)
            {
                // Check for step back (get initial skeleton depth).
                this.InitialSkeletonDepth = KinectManager.ClosestSkeletonProfile.CurrentPosition;
            }

            CurrentStageWatchers = new List<BaseWatcher>();
            PeripheralStageWatchers = new List<BaseWatcher>();
            OutlyingStageWatchers = new List<BaseWatcher>();

            StartTime = DateTime.Now;
            this.Stopwatch.Restart();
            this.TimeoutStopwatch.Restart();
            IsSetup = true;
        }

        public override void UpdateStage()
        {
            if (!IsSetup)
            {
                SetupStage();
            }

            if (this.CanTimeOut && this.TimeoutStopwatch.Elapsed >= this.TimeOutTime)
                this.TimeoutStage();

            // Wait up to 3 seconds or for the user to step back to display the results.
            if (this.Stopwatch.Elapsed > TimeSpan.FromSeconds(3.0) || (KinectManager.ClosestSkeletonProfile != null && KinectManager.ClosestSkeletonProfile.CurrentPosition.Z >= initialSkeletonDepth.Z + 40))
            {
                if (this.Complete != true)
                {
                    this.Complete = true;
                    this.EndStage();
                }
            }
        }

        public override void EndStage( bool playSound = true )
        {
            this.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            // Get the current time to signify the time the stage ended.
            EndTime = DateTime.Now;

            StageScore stagescore = new StageScore();

            // Create a screenshot for the current stage.

            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                StageScore ss = (StageScore)args.Argument;

                //do something
                ss.ScreenShot = KinectManager.CreateScreenshot();
                // Set the hasScreenshot variable if the screenshot was created successfully.
                if (ss.ScreenShot == null)
                    ss.HasScreenshot = false;
                else ss.HasScreenshot = true;

            };

            worker.RunWorkerAsync(stagescore);

            stagescore.StageNumber = this.StageNumber;

            stagescore.StageName = this.Name;

            // store how much time was spent in this stage.
            stagescore.TimeInStage = this.Stopwatch.Elapsed;

            stagescore.Target = this.Instruction;

            stagescore.Score = 1;

            this.StageScore = stagescore;

            System.Diagnostics.Debug.WriteLine("Results count: {0}", lesson.Result.StageScores.Count);
            this.lesson.Result.StageScores.Add(this.StageScore);
            System.Diagnostics.Debug.WriteLine("Results count post add: {0}", lesson.Result.StageScores.Count);

            //this.Lesson.CurrentStageIndex++;

            //if( playSound ) Lesson.PlayNextStageSound();

            this.Lesson.EndLesson();
        }

        public override void ResetStage()
        {
            throw new NotImplementedException();
        }

        public override void PauseStage()
        {
            this.Lesson.Paused = true;
            this.Lesson.Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();
            this.Stopwatch.Stop();
        }

        public override void ResumeStage()
        {
            this.Lesson.Paused = false;
            this.Lesson.Stopwatch.Start();
            this.TimeoutStopwatch.Start();
            this.Stopwatch.Start();
        }

        public override void WipeClean()
        {
            this.IsSetup = false;
            this.Complete = false;

            if (this.StageScore != null)
            {
                this.StageScore.WipeClean();
                this.StageScore = null;
                this.StageScore = new StageScore();
            }
            else
            {
                this.StageScore = new StageScore();
            }

            if (this.Stopwatch != null)
            {
                this.Stopwatch.Reset();
            }
            else
            {
                this.Stopwatch = new System.Diagnostics.Stopwatch();
            }
        }

        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }

        public override void OnStageTimeout()
        {
            throw new NotImplementedException();
        }

        public override void OnStageVideoRequest()
        {
            throw new NotImplementedException();
        }
    }
}
