﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Kinect;
using System.ComponentModel;
using System.Windows.Media.Media3D;

namespace AIMS.Tasks.VerticalLift
{
    /// <summary>
    /// The user lifts the object
    /// </summary>
    class VerticalLiftRise : Stage
    {
        private VerticalLift lesson;
        private bool bentBack = false;

        public VerticalLiftRise(VerticalLift lesson)
        {
            this.lesson = lesson;

            StageNumber = 4;
            Name = "4. Rise";
            Instruction = "Stand back up";
            TargetTime = TimeSpan.MaxValue;
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
            StageScore = new StageScore();
            ImagePath = "/Tasks/VerticalLift/Graphics/VerticalLift000.png";
            Active = false;
            Complete = false;
            ScoreWeight = 0;
            Zones = new List<Zone>();
            Stopwatch = new System.Diagnostics.Stopwatch();
            TimeoutStopwatch = new System.Diagnostics.Stopwatch();
            TimeOutTime = TimeSpan.FromSeconds(30.0);
            CanTimeOut = false;
        }
        public override void SetupStage()
        {
            lesson.StageName = Name;
            lesson.StageInstruction = Instruction;
            lesson.StageImagePath = lesson.StageImagePath = new Uri(ImagePath, UriKind.Relative);

            Zones.Clear();

            CurrentStageWatchers = new List<BaseWatcher>();
            PeripheralStageWatchers = new List<BaseWatcher>();
            OutlyingStageWatchers = new List<BaseWatcher>();

            CreateStageWatchers();

            StartTime = DateTime.Now;
            this.Stopwatch.Restart();
            this.TimeoutStopwatch.Restart();
            IsSetup = true;
        }

        private void CreateStageWatchers()
        {
            // Watch for the head to return to the standing position
            Watcher HeadHeight = new Watcher();
            HeadHeight.IsTriggered = false;
            HeadHeight.CheckForActionDelegate = () =>
            {
                Body skeletonHH = null;
                if (KinectManager.StickySkeletonIDs[0] != null)
                    skeletonHH = KinectManager.GetSkeleton(KinectManager.StickySkeletonIDs[0].Value, true);

                if (skeletonHH != null)
                    HeadHeight.IsTriggered = skeletonHH.Joints[JointType.Head].Position.Y > lesson.StartingHeight.Y - 0.1;
            };
            CurrentStageWatchers.Add(HeadHeight);

            Watcher BackAngle = new Watcher();
            BackAngle.CheckForActionDelegate = () =>
            {
                if (BackAngle.IsTriggered == false)
                {
                    // Smooth the skeleton motion by taking the average of the past few positions
                    double shoulderCenter = 0.0;
                    double kneeCenter = 0.0;
                    foreach (Body skeleton in lesson.SkeletonQueue)
                    {
                        shoulderCenter = shoulderCenter + skeleton.Joints[JointType.SpineShoulder].Position.Z;

                        kneeCenter = kneeCenter + skeleton.Joints[JointType.KneeLeft].Position.Z;
                        kneeCenter = kneeCenter + skeleton.Joints[JointType.KneeRight].Position.Z;
                    }
                    shoulderCenter = shoulderCenter / lesson.SkeletonQueue.Count;
                    kneeCenter = kneeCenter / (lesson.SkeletonQueue.Count * 2);

                    // Find the difference in Z depth 
                    if (shoulderCenter - kneeCenter < -0.1)
                    {
                        BackAngle.IsTriggered = true;
                    }
                }
            };
            CurrentStageWatchers.Add(BackAngle);
        }

        private double findAngle2(CameraSpacePoint a, CameraSpacePoint b, CameraSpacePoint c)
        {
            Point3D v1 = new Point3D(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
            Point3D v2 = new Point3D(c.X - b.X, c.Y - b.Y, c.Z - b.Z);

            v1 = normalize(v1);
            v2 = normalize(v2);

            double dotProduct = v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;

            return (System.Math.Acos(dotProduct) * 180 / System.Math.PI);
        }

        private double findAngle3(CameraSpacePoint a, CameraSpacePoint b, CameraSpacePoint c)
        {
            Point3D v1 = new Point3D(b.X - a.X, b.Y - a.Y, b.Z - a.Z);
            Point3D v2 = new Point3D(c.X - b.X, c.Y - b.Y, c.Z - b.Z);

            v1 = normalize(v1);
            v2 = normalize(v2);

            double dotProduct = v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;

            return (System.Math.Acos(dotProduct) * 180 / System.Math.PI);
        }

        private Point3D normalize(Point3D vector)
        {
            double magnitude = vector.X * vector.X + vector.Y * vector.Y + vector.Z * vector.Z;
            magnitude = System.Math.Sqrt(magnitude);

            return new Point3D(vector.X / magnitude, vector.Y / magnitude, vector.Z / magnitude);
        }

        public override void UpdateStage()
        {
            if (!IsSetup)
            {
                SetupStage();
            }

            // Check that there is a skeleton, use the closest one to the skeleton's last know position if there is not one
            if (KinectManager.StickySkeletonIDs[0] == null)
            {
                for (int i = 0; i < KinectManager.Bodies.Length; i++)
                {
                    CameraSpacePoint p = KinectManager.Bodies[i].Joints[JointType.SpineBase].Position;
                    Point3D offset = new Point3D(p.X - lesson.PreviousPosition.X, p.Y - lesson.PreviousPosition.Y, p.Z - lesson.PreviousPosition.Z);
                    double distance = System.Math.Sqrt(offset.X * offset.X + offset.Y * offset.Y + offset.Z * offset.Z);

                    if (distance < 0.5)
                    {
                        KinectManager.NumStickySkeletons = 0;
                        KinectManager.NumStickySkeletons = 1;
                        KinectManager.ForceStickySkeleton(KinectManager.Bodies[i].TrackingId);
                        break;
                    }
                }
            }

            foreach (Watcher w in CurrentStageWatchers)
            {
                
            }

            if (CurrentStageWatchers[0].IsTriggered)
            {
                this.Complete = true;
                EndStage();
            }
        }

        public override void EndStage( bool playSound = true )
        {
            Stopwatch.Stop();
            this.TimeoutStopwatch.Stop();

            this.EndTime = DateTime.Now;

            StageScore stagescore = new StageScore();

            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                StageScore ss = (StageScore)args.Argument;

                ss.ScreenShot = KinectManager.CreateScreenshot();
                // Set the hasScreenshot variable if the screenshot was created successfully.
                if (ss.ScreenShot == null)
                    ss.HasScreenshot = false;
                else ss.HasScreenshot = true;
            };

            worker.RunWorkerAsync(this.StageScore);
            
            this.StageScore.StageNumber = this.StageNumber + lesson.CompletedReps * VerticalLift.NUM_STAGES;
            this.StageScore.StageName = this.Name;
            this.StageScore.Score = 0.2;

            // store how much time was spent in this stage.
            this.StageScore.TimeInStage = TimeSpan.Zero;

            this.StageScore.Target = this.Instruction;
            this.lesson.Result.StageScores.Add(this.StageScore);
            this.lesson.Running = true;
        }

        public override void ResetStage()
        {
            throw new NotImplementedException();
        }

        public override void PauseStage()
        {
            throw new NotImplementedException();
        }

        public override void ResumeStage()
        {
            throw new NotImplementedException();
        }

        public override void WipeClean()
        {
            this.bentBack = false;
            this.IsSetup = false;
            this.Complete = false;
        }

        public override void UpdateWatcherStates(bool isCurrent, bool isPeripheral, bool isOutlier)
        {
            if (!this.IsSetup)
                return;

            if (isCurrent)
            {
                // Handle updating watchers which should update when this stage is the lesson's current stage.
                if (this.CurrentStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.CurrentStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isPeripheral)
            {
                // Handle updating of watchers which should update when we are neighboring the lesson's current stage.
                if (this.PeripheralStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.PeripheralStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
            else if (isOutlier)
            {
                // handle updating of watchers which should update when we aren't nearby the lesson's current stage.
                if (this.OutlyingStageWatchers != null)
                {
                    foreach (BaseWatcher watcher in this.OutlyingStageWatchers)
                    {
                        watcher.UpdateWatcher();
                    }
                }
            }
        }

        public override void OnStageTimeout()
        {
            throw new NotImplementedException();
        }

        public override void OnStageVideoRequest()
        {
            throw new NotImplementedException();
        }
    }
}
