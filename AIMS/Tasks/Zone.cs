﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using Microsoft.Kinect;
using System.Windows;
using System.Diagnostics;
using System.Windows.Media;

namespace AIMS
{
    public abstract class Zone
    {
        protected Point3D location = new Point3D();
        protected double width;
        protected double height;
        protected double depth;
        protected string name;
        protected Stopwatch currentTimespanStopwatch = new Stopwatch();
        protected Stopwatch totalTimeStopwatch = new Stopwatch();
        protected bool isWithinZone = false;
        protected bool doesTrackJoint = true;
        protected bool isImproperZone = false;
        protected bool active = true;
        protected bool pointCollectionIsDirty = true; // If the zone changes, this needs to be set to true so that the point collection can be updated
        protected JointType acceptedJointType;
        protected PointCollection pointCollection;

        /// <summary>
        /// The X, Y, Z cooridate location of the zone (front-top-left corner).
        /// </summary>
        public Point3D Location { get { return location; } set { pointCollectionIsDirty = true; location = value; } }
        /// <summary>
        /// Get or set only the X coordinate of the zone.
        /// </summary>
        public double X { get { return location.X; } set { pointCollectionIsDirty = true; location.X = value; } }
        /// <summary>
        /// Get or set only the Y coordinate of the zone.
        /// </summary>
        public double Y { get { return location.Y; } set { pointCollectionIsDirty = true; location.Y = value; } }
        /// <summary>
        /// Get or set only the Z coordinate of the zone.
        /// </summary>
        public double Z { get { return location.Z; } set { location.Z = value; } }
        /// <summary>
        /// Get or set the Center X coordinate of the zone.
        /// </summary>
        public double CenterX { get { return location.X + (this.Width / 2); } set { pointCollectionIsDirty = true; location.X = value - (this.Width / 2); } }
        /// <summary>
        /// Get or set the Center Y coordinate of the zone.
        /// </summary>
        public double CenterY { get { return location.Y + (this.Height / 2); } set { pointCollectionIsDirty = true; location.Y = value - (this.Height / 2); } }
        /// <summary>
        /// Get or set the Center Z coordinate of the zone.
        /// </summary>
        public double CenterZ { get { return location.Z + (this.depth / 2); } set { location.Z = value - (this.depth / 2); } }
        /// <summary>
        /// Get or set the width of the zone (along the X axis).
        /// </summary>
        public abstract double Width { get; set;}
        /// <summary>
        /// Get or set the height of the zone (along the Y axis).
        /// </summary>
        public abstract double Height { get; set;}
        /// <summary>
        /// Get or set the depth of the zone (along the Z axis).
        /// </summary>
        public double Depth { get { return depth; } set { depth = value; } }
        /// <summary>
        /// Get or set the Name of the zone. This can be used to identify the zone. Note that two zones can have the same name at the same time.
        /// </summary>
        public string Name { get { return name; } set { name = value; } }
        /// <summary>
        /// Get or set the flag which can be used to store whether or not the point was currently within the zone. Setting IsWithinZone sets Zone to Active state.
        /// </summary>
        public bool IsWithinZone { get { return isWithinZone; } set { this.isWithinZone = value; this.updateWatches(value); this.active = true; } }
        /// <summary>
        /// Get or set the JointType which the zone will accept as being "within the zone".
        /// </summary>
        public JointType AcceptedJointType { get { return acceptedJointType; } set { acceptedJointType = value; } }
        /// <summary>
        /// The total amount of time the accepted joint has been within the zone.
        /// </summary>
        public TimeSpan TimeWithinZone { get { return currentTimespanStopwatch.Elapsed; } }
        /// <summary>
        /// Set this to false if the zone isn't used to track a joint. 
        /// This is useful for zones designed for tool positions from the color tracker, etc.
        /// </summary>
        public bool DoesTrackJoint { get { return doesTrackJoint; } set { doesTrackJoint = value; } }
        /// <summary>
        /// The total amount of time the tracked point has been within the zone since the zone was created.
        /// </summary>
        public TimeSpan TotalTimeWithinZone { get { return totalTimeStopwatch.Elapsed; } }
        /// <summary>
        /// Whether or not the Zone is active. Inactive Zones set IsWithinZone to false and do not display on overlay.
        /// </summary>
        public bool Active { get { return active; } set { active = value; if (!value) { this.isWithinZone = value; this.updateWatches(value); } } }
        /// <summary>
        /// Whether or not the Zone represents an improper action. Improper zones will glow red instead of green when a point is within it's boundaries.
        /// </summary>
        public bool IsImproperZone { get { return isImproperZone; } set { isImproperZone = value; } }
        /// <summary>
        /// Rectangle created from the X, Y, Width, Height of the zone.
        /// </summary>
        public abstract Int32Rect Rectangle { get; }
        /// <summary>
        /// Returns a point collection that can be used in a polygon for displaying the zone.
        /// </summary>

        public abstract PointCollection Points { get; }

        /// <returns></returns>
        public override string ToString()
        {
            //return String.Format("{0}\r{1}\r{2}", String.Format("{0}:", (!String.IsNullOrWhiteSpace(zoneName) ? zoneName : zoneType == ZoneType.None ? "Zone" : zoneType.ToString())), String.Format("({0:#.###},{1:#.###},{2:#.###})", location.X, location.Y, location.Z), String.Format(" W:{0:#.###}, H:{1:#.###}, D:{2:#.###})", width, height, depth));
            return String.Format("{0}\n{1:#.##}\n{2:0.##}\nCenterX:{3:#.##} CenterY:{4:#.##} CenterZ:{5:#.##}\n Height:{6:#.##} Width:{7:#.##} Depth:{8:#.##}",
                String.Format("{0}:", (!String.IsNullOrWhiteSpace(name) ? name : "Zone" )),
                "Total: " + this.TotalTimeWithinZone.TotalSeconds, "Current: " + this.TimeWithinZone.TotalSeconds,
                this.CenterX, this.CenterY, this.CenterZ, this.Height, this.Width, this.Depth);
        }

        public bool IsPoint3DInside(double x, double y, double z)
        {
            return IsPoint3DInside(new Point3D(x, y, z));
        }

        public virtual bool IsPoint3DInside(Point3D p)
        {
            // If depth is not used, or depth is within the depth range
            System.Diagnostics.Debug.WriteLine("Zone-> IsPoint3DInside - Checking if point is inside: p=" + p.X + "," + p.Y + "," + p.Z + " loc=" + location.Z + " for depth: " + depth);

            if (depth == 0 || ( p.Z > location.Z && p.Z < location.Z + depth))
            {
                return IsPoint2DInside(new Point(p.X, p.Y));
            }
            else
            {
                return false;
            }
        }

        public bool IsPoint2DInside(double x, double y)
        {
            return IsPoint2DInside(new Point(x, y));
        }

        public abstract bool IsPoint2DInside(Point point);

        public bool IsJoint2DInside(Joint joint)
        {
            Point point = Get2DPosition(joint);

            return IsPoint2DInside(point);
        }

        public bool IsJoint3DInside(Joint joint)
        {
            Point3D point = Get3DPosition(joint);

            return IsPoint3DInside(point);
        }

        public static Point Get2DPosition(Joint joint)
        {
            Point point = new Point();

            if (KinectManager.DepthWidth > 0)
            {
                ColorSpacePoint colorPoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(joint.Position);
                System.Diagnostics.Debug.WriteLine("Zone -> Get2DPosition set to " + colorPoint.X + "," + colorPoint.Y);
                point.X = colorPoint.X;
                point.Y = colorPoint.Y;
            }

            return point;
        }

        
        public static Point3D Get3DPosition(Joint joint)
        {
            Point3D point = new Point3D();

            if (KinectManager.DepthWidth > 0)
            {
                point.X = joint.Position.X;
                point.Y = joint.Position.Y;
                point.Z = joint.Position.Z * 1000;
            }

            return point;
        }
        
        public void UpdateJointWithinZone()
        {
            if (KinectManager.ClosestBody == null)
                return;

            if (this.DoesTrackJoint)
            {
                Point scaled2Dxy = Get2DPosition(KinectManager.ClosestBody.Joints[acceptedJointType]);
                double z = KinectManager.ClosestBody.Joints[acceptedJointType].Position.Z * 1000;

                CheckPointWithinZone(new Point3D(scaled2Dxy.X, scaled2Dxy.Y, z));
            }
        }

        public void UpdatePointWithinZone(Point3D point)
        {
            CheckPointWithinZone(point);
        }

        public void UpdatePointWithinZone(Point point)
        {
            CheckPointWithinZone(point);
        }

        public void CheckPointWithinZone(Point p)
        {
            if (this.IsPoint2DInside(p))
                this.IsWithinZone = true;

            else
                this.IsWithinZone = false;
        }

        public void CheckPointWithinZone(Point3D p)
        {
            if (this.IsPoint3DInside(p))
                this.IsWithinZone = true;
            else
            {
                this.IsWithinZone = false;

                totalTimeStopwatch.Stop();
                currentTimespanStopwatch.Reset();
            }
        }

        /// <summary>
        /// Update Zone totalTimeStopwatch and currentTimespanStopwatch
        /// </summary>
        /// <param name="run">True: Starts/Keeps both stopwatches running. False: Stops the totalTimeStopwatch and Resets the currentTimespanStopwatch.</param>
        private void updateWatches(bool run)
        {
            if (run)
            {
                if (!this.totalTimeStopwatch.IsRunning) // Start totalTimeStopwatch if not already running.
                    this.totalTimeStopwatch.Start();

                if (!this.currentTimespanStopwatch.IsRunning) // Start currentTimespanStopwatch if not already running.
                    this.currentTimespanStopwatch.Start();
            }
            else
            {
                this.totalTimeStopwatch.Stop(); // Pauses the stopwatch allowing the next Start to accumulate the total Elapsed time.
                this.currentTimespanStopwatch.Reset(); // Stops and Reset's the Elapsed time so the next Start will begin at Timespan.Zero
            }
        }
    }
}
