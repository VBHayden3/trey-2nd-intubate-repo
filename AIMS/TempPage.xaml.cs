﻿using AIMS.Tasks.Intubation;
using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace AIMS
{
    /// <summary>
    /// Interaction logic for TempPage.xaml
    /// </summary>
    public partial class TempPage : Page
    {
        private List<Result> oldIntubationResults = new List<Result>();

        private List<IntubationResult> newIntubationResults = new List<IntubationResult>();

        private DispatcherTimer timer;

        public TempPage()
        {
            InitializeComponent();

            Loaded += TempPage_Loaded;
        }

        private void TempPage_Loaded(object sender, RoutedEventArgs e)
        {
            // Retreive and Convert StageScores and StageScore Images.+
            timer = new DispatcherTimer();


            timer.Interval = TimeSpan.FromSeconds(5);

            timer.Tick += timer_Tick;

            timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            timer.Stop();

            //ConvertOldResults();  // Note: Uncomment this out to begin converting old saved results for intubation into new ones.
        }

        private void ConvertOldResults()
        {
            for (int i = 1; i <= 209; i++)
            {
                Result res = Result.GetResultFromResultID(i, true, true);

                if (res == null)
                    continue;
                if (res.LessonType != LessonType.VerticalLift && res.LessonType != LessonType.Intubation)
                    continue;

                if (res.StageScores != null)
                {
                    IntubationResult newres = new IntubationResult();

                    newres.Score = res.TotalScore;
                    newres.SubmissionTime = res.Date;
                    newres.TaskMode = res.TaskMode;
                    newres.MasteryLevel = res.MasteryLevel;
                    newres.TaskType = Task.Intubation;

                    newres.StudentID = res.StudentID;

                    if (res.StageScores.Count > 0)
                    {
                        newres.CalibrationImage = ConvertScreenshotToKinectImage(res.StageScores[0].ScreenShot);
                    }
                    if (res.StageScores.Count > 1)
                    {
                        newres.TiltHeadImage = ConvertScreenshotToKinectImage(res.StageScores[1].ScreenShot);
                        newres.TiltHeadScore = res.StageScores[1].Score;
                    }
                    if (res.StageScores.Count > 2)
                    {
                        newres.GrabLaryngoscopeImage = ConvertScreenshotToKinectImage(res.StageScores[2].ScreenShot);
                        newres.GrabLaryngoscopeScore = res.StageScores[2].Score;
                    }
                    if (res.StageScores.Count > 3)
                    {
                        newres.InsertLaryngoscopeImage = ConvertScreenshotToKinectImage(res.StageScores[3].ScreenShot);
                        newres.InsertLaryngoscopeScore = res.StageScores[3].Score;
                    }
                    if (res.StageScores.Count > 4)
                    {
                        newres.ViewVocalChordsImage = ConvertScreenshotToKinectImage(res.StageScores[4].ScreenShot);
                        newres.ViewVocalChordsScore = res.StageScores[4].Score;
                    }
                    if (res.StageScores.Count > 5)
                    {
                        newres.GrabEndotrachealTubeImage = ConvertScreenshotToKinectImage(res.StageScores[5].ScreenShot);
                        newres.GrabEndotrachealTubeScore = res.StageScores[5].Score;
                    }
                    if (res.StageScores.Count > 6)
                    {
                        newres.InsertEndotrachealTubeImage = ConvertScreenshotToKinectImage(res.StageScores[6].ScreenShot);
                        newres.InsertEndotrachealTubeScore = res.StageScores[6].Score;
                    }
                    if (res.StageScores.Count > 7)
                    {
                        newres.RemoveLaryngoscopeImage = ConvertScreenshotToKinectImage(res.StageScores[7].ScreenShot);
                        newres.RemoveLaryngoscopeScore = res.StageScores[7].Score;
                    }
                    if (res.StageScores.Count > 8)
                    {
                        newres.RemoveStyletImage = ConvertScreenshotToKinectImage(res.StageScores[8].ScreenShot);
                        newres.RemoveStyletScore = res.StageScores[8].Score;
                    }
                    if (res.StageScores.Count > 9)
                    {
                        newres.UseSyringeImage = ConvertScreenshotToKinectImage(res.StageScores[9].ScreenShot);
                        newres.UseSyringeScore = res.StageScores[9].Score;
                    }
                    if (res.StageScores.Count > 10)
                    {
                        newres.BaggingImage = ConvertScreenshotToKinectImage(res.StageScores[10].ScreenShot);
                        newres.BaggingScore = res.StageScores[10].Score;
                    }
                    if (res.StageScores.Count > 11)
                    {
                        newres.ChestRiseImage = ConvertScreenshotToKinectImage(res.StageScores[11].ScreenShot);
                        newres.ChestRiseScore = res.StageScores[11].Score;
                    }

                    if (newres.Score >= 1)
                    {
                        // Proper performance.
                        newres.OverallFeedbackIDs.Add(18);  // You have properly performed Intubation.
                    }
                    else
                    {
                        // The performance wasn't 100%.

                        if (newres.ChestRiseScore == 1.0 || res.StageScores.Count < 12)
                        {
                            // the intubation was effective.. now to determine why the score went ary.

                            if (newres.GrabLaryngoscopeScore < 1 || newres.GrabEndotrachealTubeScore < 1 || newres.UseSyringeScore < 1 || newres.RemoveStyletScore < 1 || newres.RemoveLaryngoscopeScore < 1 || newres.TiltHeadScore < 1 || newres.ViewVocalChordsScore < 1)
                            {
                                newres.OverallFeedbackIDs.Add(26); // You did not use proper technique while Intubating.
                            }

                            if (newres.InsertLaryngoscopeScore < 1 || newres.InsertEndotrachealTubeScore < 1)
                            {
                                newres.OverallFeedbackIDs.Add(27); // You did not use proper tool positioning while Intubating.
                            }
                        }
                        else
                        {
                            newres.OverallFeedbackIDs.Add(19); // You have improperly performed Intubation.
                        }
                    }

                    IntubationResult.StoreResult(newres);   // Now, store the new result object.
                }
            }

            return;
        }

        private KinectImage ConvertScreenshotToKinectImage(Screenshot screenshot)
        {
            if (screenshot != null)
            {
                KinectImage ki = new KinectImage();

                ki.ColorData = screenshot.colorData;
                ki.DepthData = screenshot.depthData;
                ki.Timestamp = screenshot.Timestamp;
                //ki.DepthImageFormat = DepthImageFormat.Resolution640x480Fps30;
                //ki.ColorImageFormat = ColorImageFormat.YuvResolution640x480Fps15;
                ki.HasColor = ki.ColorData != null;
                ki.HasDepth = ki.DepthData != null;

                return ki;
            }

            return null;
        }

        private KinectImage ConvertScreenshotToKinectImage(StageScore.Screenshot screenshot)
        {
            if (screenshot != null)
            {
                KinectImage ki = new KinectImage();

                ki.ColorData = screenshot.colorData;
                ki.DepthData = screenshot.depthData;
                ki.Timestamp = screenshot.Timestamp;
                //ki.DepthImageFormat = DepthImageFormat.Resolution640x480Fps30;
                //ki.ColorImageFormat = ColorImageFormat.YuvResolution640x480Fps15;
                ki.HasColor = ki.ColorData != null;
                ki.HasDepth = ki.DepthData != null;

                return ki;
            }

            return null;
        }
    }
}
