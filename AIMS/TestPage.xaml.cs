﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using AIMS.Assets.Lessons2;
using AIMS.Speech;
using AIMS.Tasks;
using Microsoft.Kinect;
using System.Diagnostics;
using Npgsql;

namespace AIMS
{
    /// <summary>
    /// Interaction logic for TestPage.xaml
    /// </summary>
    public partial class TestPage : Page
    {
        public TestPage()
        {
            InitializeComponent();

            Loaded += TestPage_Loaded;
            Unloaded += TestPage_Unloaded;
        }

        private void TestPage_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void TestPage_Unloaded(object sender, RoutedEventArgs e)
        {
        }
    }
}
