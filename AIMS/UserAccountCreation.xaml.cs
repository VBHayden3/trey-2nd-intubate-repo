﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Text.RegularExpressions;
using Npgsql;
using System.Data;
using System.Globalization;

namespace AIMS
{
	public partial class UserAccountCreation
	{
		public UserAccountCreation()
		{
			this.InitializeComponent();

            FirstNameBox.Focus();

            CheckCapsLock();
            CheckFirstNameEmpty();
            CheckLastNameEmpty();
            CheckYourEmailEmpty();
            CheckConfirmEmailEmpty();
            CheckNewPasswordEmpty();
            CheckConfirmPasswordEmpty();
            CheckSecurityQuestionEmpty();
            CheckConfirmQuestionEmpty();
            CheckQuestionMismatch();
            CheckSecurityAnswerEmpty();
            CheckConfirmAnswerEmpty();
            CheckAnswerMismatch();
		}


        // *************** EVENT HANDLERS ***************

        /// <summary>
        /// Logic for when the user sign up "Sign Up" button has been clicked.
        /// 
        /// - Checks that all form fields have been completed, are valid, and match
        /// - If incorrect -> display warning label to complete fields
        /// - If correct -> check that email address does not already exist in database
        /// - If incorrect -> display warning label to change email
        /// - If correct -> add account and student to database
        /// </summary>
        private void SignUpButtonClicked(object sender, RoutedEventArgs e)
        {
            if (!CheckFirstNameEmpty() && !CheckLastNameEmpty() && !CheckYourEmailEmpty() && !CheckConfirmEmailEmpty() && !CheckEmailMismatch() 
                && !CheckEmailInvalid() && !CheckNewPasswordEmpty() && !CheckConfirmPasswordEmpty() && !CheckPasswordMismatch() 
                && !CheckSecurityQuestionEmpty() && !CheckConfirmQuestionEmpty() && !CheckQuestionMismatch() && !CheckSecurityAnswerEmpty()
                && !CheckConfirmAnswerEmpty() && !CheckAnswerMismatch())
            {
                emptyFieldWarningLabel.Visibility = Visibility.Hidden;

                DatabaseManager.OpenConnection();

                NpgsqlCommand command = DatabaseManager.Connection.CreateCommand(); //new NpgsqlCommand(query, DatabaseManager.Connection);
                command.CommandType = CommandType.Text;
                command.CommandText = @"SELECT uid FROM accounts WHERE email = :email;";
                command.Parameters.Add("email", NpgsqlTypes.NpgsqlDbType.Text).Value = ConfirmEmailBox.Text;

                int id = 0;

                try
                {
                    id = (int)command.ExecuteScalar();  // read the selected id
                    DatabaseManager.CloseConnection();
                }
                catch
                {
                    // no account with that username/password is in our database
                    duplicateEmailWarningLabel.Visibility = Visibility.Hidden;
                    Organization organization = new Organization();
                    organization.ID = 1;
                    Account acct = Account.CreateAccount(ConfirmEmailBox.Text, ConfirmPasswordBox.Password, 0, DateTime.Now, DateTime.MaxValue, false, 
                        ConfirmEmailBox.Text, SecurityQuestionBox.Text, SecurityAnswerBox.Text, organization);
                    Student.CreateStudentLinkedToAccount(acct.ID, FirstNameBox.Text, LastNameBox.Text, organization.ID);
                    // navigate to login page
                    System.Uri uri = new System.Uri(@"..\..\LoginPage.xaml", System.UriKind.Relative);
                    NavigationService.Navigate(uri);
                }

                // account with that email address already exists
                duplicateEmailWarningLabel.Visibility = Visibility.Visible;
                YourEmailBox.Clear();
                ConfirmEmailBox.Clear();
            }
            else
            {
                emptyFieldWarningLabel.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Button handler, checks for Caps Lock and checks if the Enter button has been hit.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.CapsLock)  // CapsLock key toggles on/off the caps lock warning message.
            {
                CheckCapsLock();
            }
            if (e.Key == Key.Enter) // Enter key submits the current password input.
            {
                SignUpButtonClicked(null, null);
            }
        }

        private void FirstNameChangedHandler(object sender, RoutedEventArgs e)
        {
            CheckFirstNameEmpty();
        }

        private void LastNameChangedHandler(object sender, RoutedEventArgs e)
        {
            CheckLastNameEmpty();
        }

        private void YourEmailChangedHandler(object sender, RoutedEventArgs e)
        {
            CheckYourEmailEmpty();
            CheckEmailMismatch();
            CheckEmailInvalid();
        }

        private void ConfirmEmailChangedHandler(object sender, RoutedEventArgs e)
        {
            CheckConfirmEmailEmpty();
            CheckEmailMismatch();
            CheckEmailInvalid();
        }

        private void NewPasswordChangedHandler(object sender, RoutedEventArgs e)
        {
            CheckNewPasswordEmpty();
            CheckPasswordMismatch();
        }

        private void ConfirmPasswordChangedHandler(object sender, RoutedEventArgs e)
        {
            CheckConfirmPasswordEmpty();
            CheckPasswordMismatch();
        }

        private void SecurityQuestionChangedHandler(object sender, RoutedEventArgs e)
        {
            CheckSecurityQuestionEmpty();
            CheckQuestionMismatch();
        }

        private void ConfirmQuestionChangedHandler(object sender, RoutedEventArgs e)
        {
            CheckConfirmQuestionEmpty();
            CheckQuestionMismatch();
        }

        private void SecurityAnswerChangedHandler(object sender, RoutedEventArgs e)
        {
            CheckSecurityAnswerEmpty();
            CheckAnswerMismatch();
        }

        private void ConfirmAnswerChangedHandler(object sender, RoutedEventArgs e)
        {
            CheckConfirmAnswerEmpty();
            CheckAnswerMismatch();
        }


        // *************** FUNCTIONS ***************

        /// <summary>
        /// Checks if the CapsLock key is on
        /// </summary>
        /// <returns></returns>
        private void CheckCapsLock()
        {
            if (Console.CapsLock)   
            {
                // Warn that the caps lock key is enabled.
                capsLockWarningLabel.Content = "* Caps Lock enabled";
                capsLockWarningLabel.Visibility = Visibility.Visible;
            }
            else
            {
                capsLockWarningLabel.Content = "";
                capsLockWarningLabel.Visibility = Visibility.Hidden;
            }
        }

        /// <summary>
        /// Checks if the first name box was left empty, or if it is simply white space.
        /// </summary>
        /// <returns></returns>
        private bool CheckFirstNameEmpty()
        {
            if (emptyFirstNameWarningLabel == null)
                return true;
            if (String.IsNullOrWhiteSpace(FirstNameBox.Text))
            {
                emptyFirstNameWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                emptyFirstNameWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// Checks if the last name box was left empty, or if it is simply white space.
        /// </summary>
        /// <returns></returns>
        private bool CheckLastNameEmpty()
        {
            if (emptyLastNameWarningLabel == null)
                return true;
            if (String.IsNullOrWhiteSpace(LastNameBox.Text))
            {
                emptyLastNameWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                emptyLastNameWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// Checks if the your email box was left empty, or if it is simply white space.
        /// </summary>
        /// <returns></returns>
        private bool CheckYourEmailEmpty()
        {
            if (emptyYourEmailWarningLabel == null)
                return true;
            if (String.IsNullOrWhiteSpace(YourEmailBox.Text))
            {
                emptyYourEmailWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                emptyYourEmailWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// Checks if the confirm email box was left empty, or if it is simply white space.
        /// </summary>
        /// <returns></returns>
        private bool CheckConfirmEmailEmpty()
        {
            if (emptyConfirmEmailWarningLabel == null)
                return true;
            if (String.IsNullOrWhiteSpace(ConfirmEmailBox.Text))
            {
                emptyConfirmEmailWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                emptyConfirmEmailWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// Checks if the email boxes match
        /// </summary>
        /// <returns></returns>
        private bool CheckEmailMismatch()
        {
            if (mismatchEmailWarningLabel == null)
                return true;
            if (!YourEmailBox.Text.Equals(ConfirmEmailBox.Text) && !String.IsNullOrWhiteSpace(YourEmailBox.Text) && !String.IsNullOrWhiteSpace(ConfirmEmailBox.Text))
            {
                mismatchEmailWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                mismatchEmailWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// method for determining is the user provided an invalid email address
        /// We use regular expressions in this check, as it is a more thorough
        /// way of checking the address provided
        /// </summary>
        /// <returns>true is invalid, false if valid</returns>
        public bool CheckEmailInvalid()
        {
            if (invalidEmailWarningLabel == null)
                return true;
            if (!String.IsNullOrWhiteSpace(YourEmailBox.Text))
            {
                //regular expression pattern for valid email
                //addresses, allows for the following domains:
                //com,edu,info,gov,int,mil,net,org,biz,name,museum,coop,aero,pro,tv
                string pattern = @"^[-a-zA-Z0-9][-.a-zA-Z0-9]*@[-.a-zA-Z0-9]+(\.[-.a-zA-Z0-9]+)*\.(com|edu|info|gov|int|mil|net|org|biz|name|museum|coop|aero|pro|tv|[a-zA-Z]{2})$";
                //Regular expression object
                Regex check = new Regex(pattern, RegexOptions.IgnorePatternWhitespace);
                //boolean variable to return to calling method
                bool valid;
                //use IsMatch to validate the address
                valid = check.IsMatch(YourEmailBox.Text);
                if (!valid)
                {
                    invalidEmailWarningLabel.Visibility = Visibility.Visible;
                    return true;
                }
                else
                {
                    invalidEmailWarningLabel.Visibility = Visibility.Hidden;
                    return false;
                }
            }
            else
            {
                invalidEmailWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// Checks if the new password box was left empty, or if it is simply white space.
        /// </summary>
        /// <returns></returns>
        private bool CheckNewPasswordEmpty()
        {
            if (emptyNewPasswordWarningLabel == null)
                return true;
            if (String.IsNullOrWhiteSpace(NewPasswordBox.Password))
            {
                emptyNewPasswordWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                emptyNewPasswordWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// Checks if the confirm password box was left empty, or if it is simply white space.
        /// </summary>
        /// <returns></returns>
        private bool CheckConfirmPasswordEmpty()
        {
            if (emptyConfirmPasswordWarningLabel == null)
                return true;
            if (String.IsNullOrWhiteSpace(ConfirmPasswordBox.Password))
            {
                emptyConfirmPasswordWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                emptyConfirmPasswordWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// Checks if the password boxes match
        /// </summary>
        /// <returns></returns>
        private bool CheckPasswordMismatch()
        {
            if (mismatchPasswordWarningLabel == null)
                return true;
            if (!NewPasswordBox.Password.Equals(ConfirmPasswordBox.Password) && !String.IsNullOrWhiteSpace(NewPasswordBox.Password) && !String.IsNullOrWhiteSpace(ConfirmPasswordBox.Password))
            {
                mismatchPasswordWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                mismatchPasswordWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// Checks if the security question box was left empty, or if it is simply white space.
        /// </summary>
        /// <returns></returns>
        private bool CheckSecurityQuestionEmpty()
        {
            if (emptySecurityQuestionWarningLabel == null)
                return true;
            if (String.IsNullOrWhiteSpace(SecurityQuestionBox.Text))
            {
                emptySecurityQuestionWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                emptySecurityQuestionWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// Checks if the confirm question box was left empty, or if it is simply white space.
        /// </summary>
        /// <returns></returns>
        private bool CheckConfirmQuestionEmpty()
        {
            if (emptyConfirmQuestionWarningLabel == null)
                return true;
            if (String.IsNullOrWhiteSpace(ConfirmQuestionBox.Text))
            {
                emptyConfirmQuestionWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                emptyConfirmQuestionWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// Checks if the question boxes match
        /// </summary>
        /// <returns></returns>
        private bool CheckQuestionMismatch()
        {
            if (mismatchQuestionWarningLabel == null)
                return true;
            if (!SecurityQuestionBox.Text.Equals(ConfirmQuestionBox.Text) && !String.IsNullOrWhiteSpace(SecurityQuestionBox.Text) && !String.IsNullOrWhiteSpace(ConfirmQuestionBox.Text))
            {
                mismatchQuestionWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                mismatchQuestionWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// Checks if the security answer box was left empty, or if it is simply white space.
        /// </summary>
        /// <returns></returns>
        private bool CheckSecurityAnswerEmpty()
        {
            if (emptySecurityAnswerWarningLabel == null)
                return true;
            if (String.IsNullOrWhiteSpace(SecurityAnswerBox.Text))
            {
                emptySecurityAnswerWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                emptySecurityAnswerWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// Checks if the confirm answer box was left empty, or if it is simply white space.
        /// </summary>
        /// <returns></returns>
        private bool CheckConfirmAnswerEmpty()
        {
            if (emptyConfirmAnswerWarningLabel == null)
                return true;
            if (String.IsNullOrWhiteSpace(ConfirmAnswerBox.Text))
            {
                emptyConfirmAnswerWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                emptyConfirmAnswerWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        /// <summary>
        /// Checks if the answer boxes match
        /// </summary>
        /// <returns></returns>
        private bool CheckAnswerMismatch()
        {
            if (mismatchAnswerWarningLabel == null)
                return true;
            if (!SecurityAnswerBox.Text.Equals(ConfirmAnswerBox.Text) && !String.IsNullOrWhiteSpace(SecurityAnswerBox.Text) && !String.IsNullOrWhiteSpace(ConfirmAnswerBox.Text))
            {
                mismatchAnswerWarningLabel.Visibility = Visibility.Visible;
                return true;
            }
            else
            {
                mismatchAnswerWarningLabel.Visibility = Visibility.Hidden;
                return false;
            }
        }

        private void CancelButton_Clicked(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new System.Uri(@"..\..\LoginPage.xaml", System.UriKind.Relative));   // Leave this page.
        }
	}
}