﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using System.Windows;

namespace AIMS
{
    public static class DatabaseManager
    {
        //public const bool UseLocalhostDB = true;
        //public static string Host = UseLocalhostDB ? "127.0.0.1" : "172.16.10.83";
        //public static string Port = UseLocalhostDB ? "5432" : "5432";
        //public static string DatabaseName = "aims";
        //public static string Username = "postgres";
        //public static string Password = "postgres";

        public static string DatabaseName = AIMS.Utilities.SystemSettings.DatabaseName;
        public static string Host = AIMS.Utilities.SystemSettings.DatabaseIP;
        public static string Port = AIMS.Utilities.SystemSettings.DatabasePortNumber;
        public static string Username = AIMS.Utilities.SystemSettings.DatabaseUsername;
        public static string Password = AIMS.Utilities.SystemSettings.DatabasePassword;

        public static string ConnectionString = String.Format("Server={0};Port={1};" + "User Id={2};Password={3};Database={4};", DatabaseManager.Host, DatabaseManager.Port, DatabaseManager.Username, DatabaseManager.Password, DatabaseManager.DatabaseName);
        public static NpgsqlConnection Connection = new NpgsqlConnection(DatabaseManager.ConnectionString);

        public static void OpenConnection()
        {
            if (DatabaseManager.Connection != null && DatabaseManager.Connection.State == System.Data.ConnectionState.Open)
                return;

            Console.WriteLine(String.Format("Attempting to connect to server:port {0}:{1} using user {2} password {3} to access database {4}", DatabaseManager.Host, DatabaseManager.Port, DatabaseManager.Username, DatabaseManager.Password, DatabaseManager.DatabaseName));

            // try to open the connection
            try
            {
                /*
                #if DEBUG   // For Testing Only, OK to Remove
                    string file = System.Reflection.Assembly.GetExecutingAssembly().Location + ".NpgsqlEventLog.txt";
                    NpgsqlEventLog.Level = LogLevel.Debug;
                    NpgsqlEventLog.LogName = file;
                    NpgsqlEventLog.EchoMessages = false;
                #endif
                */
                
                DatabaseManager.Connection.Open();
                Console.WriteLine("Connection established.");
            }
            catch( Exception e )
            {
                //MessageBox.Show(e.ToString());
                System.Diagnostics.Debug.Print("Connection failed to establish. {0} {1} {2} {3} \n{4}", DatabaseName, Host, Port, e.Message, e.ToString());
                MessageBox.Show("Could not establish connection to the database.");
            }
        }

        public static void CloseConnection()
        {
            if (DatabaseManager.Connection == null || DatabaseManager.Connection.State == System.Data.ConnectionState.Closed)
                return;

            try
            {
                Connection.Close();
                Console.WriteLine("Connection closed.");
            }
            catch
            {
                Console.WriteLine("Connection failed to close.");
                MessageBox.Show("Connection failed to close.");
            }
        }
    }
}
