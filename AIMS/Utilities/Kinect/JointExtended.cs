﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Kinect;

namespace AIMS.Utilities.Kinect
{
    /// <summary>
    /// Class to extend the Microsoft.Kinect.Joint Struct. 
    /// Adds Trends in 3 dimensions and Time at which JointExtended was initialized.
    /// </summary>
    public class JointExtended
    {
        #region Public Properties

        /// <summary>
        /// Gets the joint type, which identifies a portion of the skeleton.
        /// </summary>
        public JointType JointType { get { return this.joint.JointType; } }
        /// <summary>
        /// Gets the SkeletonPoint position of the joint.
        /// </summary>
        public CameraSpacePoint Position { get { return this.joint.Position; } }
        /// <summary>
        /// Gets the tracking state of the joint, which indicates the quality of the joint position that 
        /// was produced by the skeletonstream.
        /// </summary>
        public TrackingState TrackingState { get { return this.joint.TrackingState; } }
        /// <summary>
        /// Gets the time of which the joint was declared.
        /// </summary>
        public DateTime Time { get { return this.time; } }
        /// <summary>
        /// Gets the trend of the joint in the X dimension compared to another point.
        /// </summary>
        public Trend TrendX { get { return this.trendX; } }
        /// <summary>
        /// Gets the trend of the joint in the Y dimension compared to another point.
        /// </summary>
        public Trend TrendY { get { return this.trendY; } }
        /// <summary>
        /// Gets the trend of the joint in the Z dimension compared to another point.
        /// </summary>
        public Trend TrendZ { get { return this.trendZ; } }
        /// <summary>
        /// Gets a 3-dimensional trend Struct which specifies trends in X, Y, and Z compared to another point.
        /// </summary>
        public Trend3D Trends { get { return new Trend3D(this.trendX, this.trendY, this.trendZ); } }

        #endregion

        #region Private Fields

        // Joint for which this class extends.
        private Joint joint;
        // Trend of the X position for this joint.
        private Trend trendX;
        // Trend of the Y position for this joint.
        private Trend trendY;
        // Trend of the Z position for this joint.
        private Trend trendZ;
        // Time at which this extended joint was declared.
        private DateTime time;
        // A trend can only exist if the compared difference is greater than this threshold.
        private float trendThreshold = 0.0035f; // 0.0035 m or 3.5 mm

        #endregion

        #region Constructor

        /// <summary>
        /// Create a JointExtended object. Contains all accessable properties of a Joint Struct as well as 
        /// Trend propreties for X, Y, Z and a Time property of when the JointExtended was initialized.
        /// </summary>
        /// <param name="Joint">Joint to inherit all Joint information from.</param>
        /// <param name="PreviousPoint">SkeletonPoint from previous Joint to perform Trend analysis. 
        /// If none specified, Trends will default to Trend.None.</param>
        public JointExtended(Joint Joint, CameraSpacePoint? PreviousPoint = null)
        {
            this.joint = Joint;

            // Set time to when this joint was 'made'
            this.time = DateTime.Now;

            // If Previous Point was passed in, figure out trend
            if (PreviousPoint.HasValue)
            {
                this.trendX = this.DiscoverTrend(this.joint.Position.X, PreviousPoint.Value.X);
                this.trendY = this.DiscoverTrend(this.joint.Position.Y, PreviousPoint.Value.Y);
                this.trendZ = this.DiscoverTrend(this.joint.Position.Z, PreviousPoint.Value.Z);
            }
            // Otherwise default to none
            else
            {
                this.trendX = Trend.None;
                this.trendY = Trend.None;
                this.trendZ = Trend.None;
            }
        }

        #endregion

        #region Public Functions

        /// <summary>
        /// Update joints trend by comparing aganist supplied older point.
        /// </summary>
        /// <param name="Point">Older point to compare joint against, used to discover trend.</param>
        public void UpdateTrend(CameraSpacePoint Point)
        {
            // Set trends.
            this.trendX = this.DiscoverTrend(this.joint.Position.X, Point.X);
            this.trendY = this.DiscoverTrend(this.joint.Position.Y, Point.Y);
            this.trendZ = this.DiscoverTrend(this.joint.Position.Z, Point.Z);
        }

        /// <summary>
        /// Returns a string with X, Y, Z positions.
        /// </summary>
        /// <returns>String of X Y Z positions.</returns>
        public override string ToString()
        {
            return String.Format("({0}, {1}, {2}", this.Position.X, this.Position.Y, this.Position.Z);
        }

        #endregion

        #region Private Functions

        /// <summary>
        /// Compares two float values and returns trend of those values.
        /// </summary>
        /// <param name="Current">Newer value to compare.</param>
        /// <param name="Past">Older value to compare.</param>
        /// <returns>If the difference is within trendThreshold, Trend.None is returned. 
        /// If the difference is positive, Trend.Positive is returned. 
        /// If the difference is negative, Trend.Negative is returned.</returns>
        private Trend DiscoverTrend(float Current, float Past)
        {
            // Difference between Current and Past are within trendThresold, so no trend exists.
            if (System.Math.Abs(Current - Past) <= this.trendThreshold)
                return Trend.None;

            // Current is greater than Past, so the trend is positive.
            else if (Current > Past)
                return Trend.Positive;

            // Past is greater than Current, so the trend is negative. 
            // ( Past cannot be equal to Current, it would be within the trendThreshold. )
            else
                return Trend.Negative;
        }

        #endregion
    }
}
