﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using Microsoft.Kinect;
using System.Diagnostics;

namespace AIMS.Utilities.Kinect
{
    /// <summary>
    /// Class to collect history of JointExtended objects. Joints to track are specified by JointType 
    /// and updated with a Microsoft.Kinect.Skeleton; should be updated every frame.
    /// Provides trending for specified portions of the history.
    /// </summary>
    class JointHistory
    {
        #region Public Properties

        /// <summary>
        /// Length of history to store for each Joint. WARNING: If HistoryLength is altered, all previous history is lost.
        /// </summary>
        public int HistoryLength { get { return this.historyLength; } set { this.historyLength = value > 0 ? value : 1; this.UpdateJointHistSize(); } }

        /// <summary>
        /// Get an array of joint history for a joint specified by a JointType index.
        /// </summary>
        /// <param name="joint">Joint to retrieve history of.</param>
        /// <returns>Joint array of size JointHistory.HistoryLength.</returns>
        public JointExtended[] this[JointType joint]
        {
            get
            {
                try { return (JointExtended[])this.jointHist[(JointType)joint].Clone(); }
                catch (Exception exc)
                {
                    System.Diagnostics.Debug.Print("JointHistory[JointType Joint] ERROR:", exc.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Get an array of joint history up to a specified depth for a joint specified by a JointType index and depth.
        /// </summary>
        /// <param name="Joint">Joint to retrieve history of.</param>
        /// <param name="Depth">Amount of history to return, starting from the newest to depth.</param>
        /// <returns>Joint array of size Depth, unless depth is greater than HistoryLength, then size HistoryLength,
        /// or less than / equal to 0, then size 1.</returns>
        public JointExtended[] this[JointType Joint, int Depth]
        {
            get
            {
                // Ensure Depth is a usable value. If greater than historyLength, set to historyLength. If <= 0, set to 1.
                if (Depth > this.historyLength)
                    Depth = this.historyLength;

                else if (Depth <= 0)
                    Depth = 1;

                try
                {
                    // Create array to return and copy over values.
                    JointExtended[] joints = new JointExtended[Depth];
                    for (int i = 0; i < Depth; i++)
                        joints[i] = this.jointHist[Joint][i];

                    return joints;
                }
                catch (Exception exc)
                {
                    System.Diagnostics.Debug.Print("JointHistory[JointType Joint, int Depth] ERROR: {0}", exc.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Get a collection of joints being tracked by JointHistory.
        /// </summary>
        /// <returns>List of JointType, unsorted.</returns>
        public List<JointType> TrackedJoints { get { return new List<JointType>(this.trackedJoints); } }

        #endregion

        #region Private Fields

        /// <summary>
        /// Collection of joints to track.
        /// </summary>
        private List<JointType> trackedJoints;
        /// <summary>
        /// History of a given joint.
        /// </summary>
        private Dictionary<JointType, JointExtended[]> jointHist;
        /// <summary>
        /// Length of Joint History. Defaults to 60
        /// </summary>
        private int historyLength = 60;

        // Flag whether object has been disposed or not.
        private bool disposed;

        #endregion

        #region Constructor / Destructor

        /// <summary>
        /// Constructor for JointHistory object which initializes internal fields.
        /// </summary>
        public JointHistory()
        {
            // Initialize trackedJoints list to size 8 ( grows if more joints are added ).
            this.trackedJoints = new List<JointType>(8);
            // Initialize jointHist, will need a new Joint[] for every JointType key added. Initial size of 8, grows if more added.
            this.jointHist = new Dictionary<JointType, JointExtended[]>(8);
        }

        #endregion

        #region Public Functions
        #region Add Remove and Update Functions

        /// <summary>
        /// Specify a Joint to track, adds to a collection of tracked joints.
        /// </summary>
        /// <param name="Joint">Joint to track.</param>
        /// <returns>True: Joint was added. False: Joint was not added ( it was already being tracked ).</returns>
        public bool AddJoint(JointType Joint)
        {
            try
            {
                // Make sure not already tracking Joint.
                foreach (JointType joint in this.trackedJoints)
                {
                    if (joint == Joint)
                        return false;
                }
                // Add to trackedJoints if not already being tracked
                this.trackedJoints.Add(Joint);
                // Initialize spot in jointHist Dictionary.
                this.jointHist.Add(Joint, new JointExtended[this.historyLength]);
                return true;
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print("JointHistory AddJoint ERROR: {0}", exc.Message);
                return false;
            }
        }

        /// <summary>
        /// Adds all joints to the collection of tracked joints.
        /// </summary>
        public void AddAllJoints()
        {
            try
            {
                foreach (String jointType in Enum.GetNames(typeof(JointType)))
                {
                    this.AddJoint((JointType)Enum.Parse(typeof(JointType), jointType));
                }
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print("JointHistory AddJoints() ERROR: {0}", exc.Message);
            }
        }

        /// <summary>
        /// Specify a joint to stop tracking. Removes joint from a collection of tracked joints.
        /// </summary>
        /// <param name="Joint">Joint to stop tracking.</param>
        /// <returns>True: Joint was removed. False: Joint was not removed ( it was not being tracked ).</returns>
        public bool RemoveJoint(JointType Joint)
        {
            try
            {
                // Look for joint to remove
                for (int i = 0; i < this.trackedJoints.Count; i++)
                {
                    if (this.trackedJoints[i] == Joint)
                    {
                        // Joint was found, remove it from trackedJoints and jointHist, return true.
                        this.trackedJoints.RemoveAt(i);
                        this.jointHist.Remove(Joint);
                        return true;
                    }
                }

                // Joint was not found, return false.
                return false;
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print("JointHistory RemoveJoint ERROR: {0}", exc.Message);
                return false;
            }
        }

        /// <summary>
        /// Removes all joints from a collection of tracked joints.
        /// </summary>
        public void RemoveAllJoints()
        {
            try
            {
                foreach (String jointType in Enum.GetNames(typeof(JointType)))
                {
                    this.RemoveJoint((JointType)Enum.Parse(typeof(JointType), jointType));
                }
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print("JointHistory RemoveJoints() ERROR: {0}", exc.Message);
            }
        }

        /// <summary>
        /// Updates the history of tracked joints from provided skeleton.
        /// </summary>
        /// <param name="skel">Skeleton used to update skeleton joint history.</param>
        public void UpdateJointHistory(Body skel)
        {
            // Make room in jointHist for next point.
            this.PushJointHist();

            // Update jointHist from passed Skeleton.
            foreach (JointType jointType in this.trackedJoints)
            {
                try
                {
                    this.jointHist[jointType][0] = new JointExtended(skel.Joints[jointType]);
                    if (this.historyLength > 1)
                    {
                        // If older history exists, update trend of joint with that history.
                        if (this.jointHist[jointType][1] != null)
                            this.jointHist[jointType][0].UpdateTrend(this.jointHist[jointType][1].Position);
                    }
                }
                catch (Exception exc)
                {
                    System.Diagnostics.Debug.Print("JointHistory UpdateJointHistory ERROR: {0}", exc.Message);
                }
            }
        }

        #endregion

        #region Trending Functions
        /// <summary>
        /// Discovers the trend in X, Y, and Z over the specified inclusive range.
        /// </summary>
        /// <param name="Joint">Joint to get trend of.</param>
        /// <param name="StartOfRange">Inclusive start of range. Cannot be less than 0. Defaults to 0</param>
        /// <param name="EndOfRange">Inclusive end of range. Cannot be greater than or equal to HistoryLength. Defaults to HistoryLength - 1</param>
        /// <returns>Trend3D struct of trends in X, Y, and Z dimensions.</returns>
        public Trend3D TrendOverRange(JointType Joint, int StartOfRange, int EndOfRange)
        {
            // Trend counters to discover trend over range.
            int positiveTrendsX = 0;
            int negativeTrendsX = 0;
            int noneTrendsX = 0;
            int positiveTrendsY = 0;
            int negativeTrendsY = 0;
            int noneTrendsY = 0;
            int positiveTrendsZ = 0;
            int negativeTrendsZ = 0;
            int noneTrendsZ = 0;

            // Make sure parameters are within range.
            if (StartOfRange < 0 || StartOfRange >= this.historyLength)
            {
                System.Diagnostics.Debug.Print("StartOfRange cannot be less than 0 or greater than or equal to HistoryLength.");
                return new Trend3D(Trend.None, Trend.None, Trend.None);
            }
            // Throw OutOfRange exception
            if (EndOfRange >= this.historyLength || EndOfRange < 0)
            {
                System.Diagnostics.Debug.Print("EndOfrange cannot be greater than or equal to HistoryLength or less than 0.");
                return new Trend3D(Trend.None, Trend.None, Trend.None);
            }
            // Make sure range is from front to back
            if (StartOfRange > EndOfRange)
            {
                System.Diagnostics.Debug.Print("StartOfRange cannot be greater than EndOfRange.");
                return new Trend3D(Trend.None, Trend.None, Trend.None);
            }

            // Dimension trends to return.
            Trend trendX, trendY, trendZ;

            // Double check that Joint is actually being tracked
            if (!this.IsJointTracked(Joint))
            {
                System.Diagnostics.Debug.Print("Joint to trend is not being tracked.");
                return new Trend3D(Trend.None, Trend.None, Trend.None);
            }

            try
            {
                // Iterate over inclusive range, count up trends.
                for (int rangeIndx = StartOfRange; rangeIndx <= EndOfRange; rangeIndx++)
                {
                    // Make sure the JointExtended exists
                    if (this.jointHist[Joint][rangeIndx] != null)
                    {
                        // Update trend counters for X dimension.
                        switch (this.jointHist[Joint][rangeIndx].TrendX)
                        {
                            case Trend.None:
                                noneTrendsX++;
                                break;
                            case Trend.Positive:
                                positiveTrendsX++;
                                break;
                            case Trend.Negative:
                                negativeTrendsX++;
                                break;
                        }
                        // Update trend counters for Y dimension.
                        switch (this.jointHist[Joint][rangeIndx].TrendY)
                        {
                            case Trend.None:
                                noneTrendsY++;
                                break;
                            case Trend.Positive:
                                positiveTrendsY++;
                                break;
                            case Trend.Negative:
                                negativeTrendsY++;
                                break;
                        }
                        // Update trend counters for Z dimension.
                        switch (this.jointHist[Joint][rangeIndx].TrendZ)
                        {
                            case Trend.None:
                                noneTrendsZ++;
                                break;
                            case Trend.Positive:
                                positiveTrendsZ++;
                                break;
                            case Trend.Negative:
                                negativeTrendsZ++;
                                break;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print("JointHistory TrendOverRange ERROR: {0}", exc.Message);
                throw;
            }

            // Discover trend in X.
            if (positiveTrendsX > negativeTrendsX && positiveTrendsX > noneTrendsX)
                trendX = Trend.Positive;

            else if (negativeTrendsX > positiveTrendsX && negativeTrendsX > noneTrendsX)
                trendX = Trend.Negative;

            else
                trendX = Trend.None;

            // Discover trend in Y.
            if (positiveTrendsY > negativeTrendsY && positiveTrendsY > noneTrendsY)
                trendY = Trend.Positive;

            else if (negativeTrendsY > positiveTrendsY && negativeTrendsY > noneTrendsY)
                trendY = Trend.Negative;

            else
                trendY = Trend.None;

            // Discover trend in Z.
            if (positiveTrendsZ > negativeTrendsZ && positiveTrendsZ > noneTrendsZ)
                trendZ = Trend.Positive;

            else if (negativeTrendsZ > positiveTrendsZ && negativeTrendsZ > noneTrendsZ)
                trendZ = Trend.Negative;

            else
                trendZ = Trend.None;

            // Return Trend3D struct comprised of discovered trends.
            return new Trend3D(trendX, trendY, trendZ);
        }

        /// <summary>
        /// Discovers the trend in X, Y, and Z over the entire history of joints for supplied JointType.
        /// </summary>
        /// <param name="Joint">JointType to get trend of.</param>
        /// <returns>Trend3D struct of trends in X, Y, and Z dimensions.</returns>
        public Trend3D TrendOfAll(JointType Joint)
        {
            return this.TrendOverRange(Joint, 0, this.historyLength);
        }

        /// <summary>
        /// Discovers the trend in X, Y, and Z over RangeLength points starting from the newest point.
        /// </summary>
        /// <param name="Joint">JointType to get trend of.</param>
        /// <param name="RangeLength">Amount of joints to analyze trend of.</param>
        /// <returns>Trend3D struct of trends in X, Y, and Z dimensions.</returns>
        public Trend3D TrendFromLatest(JointType Joint, int RangeLength)
        {
            return this.TrendOverRange(Joint, 0, RangeLength - 1);
        }

        #endregion

        #region IsJointTracked and ToString Functions

        /// <summary>
        /// Returns whether or not a JointType is tracked by JointHistory.
        /// </summary>
        /// <param name="JointType"></param>
        /// <returns>True: Joint is being tracked. False: Joint is not being tracked.</returns>
        public bool IsJointTracked(JointType JointType)
        {
            foreach (JointType jointType in this.trackedJoints)
            {
                if (jointType == JointType)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Returns a string containing all tracked joints and their indexed history.
        /// </summary>
        /// <returns>String of all tracked joints and their indexed history.</returns>
        public override string ToString()
        {
            string output = "";

            foreach (JointType jointType in this.trackedJoints)
            {
                output += String.Format("Joint: {0} - ", jointType);

                for (int i = 0; i < this.jointHist[jointType].Length; i++)
                {
                    // Display index and X,Y,Z in mm and Skeletal Space with up to 2 decimal places.
                    CameraSpacePoint jointPos = this.jointHist[jointType][i].Position;
                    output += String.Format("[{0}]({1:#.##},{2:#.##},{3:#.##})", i, jointPos.X * 1000, jointPos.Y * 1000, jointPos.Z * 1000);
                }
                output += "\n";
            }

            return output;
        }

        #endregion
        #endregion // end Public Functions

        #region Private Functions

        /// <summary>
        /// Updates the length of the history associated with each tracked joint stored in jointHist.
        /// </summary>
        private void UpdateJointHistSize()
        {
            foreach (JointType joint in this.trackedJoints)
            {
                try
                {
                    this.jointHist.Remove(joint);
                    this.jointHist.Add(joint, new JointExtended[this.historyLength]);
                }
                catch (Exception exc)
                {
                    System.Diagnostics.Debug.Print("JointHistory UpdateJointHistSize ERROR: {0}  historyLength: {1}", exc.Message, this.historyLength);
                }
            }
        }

        /// <summary>
        /// Push the joint positions down the jointHist array.
        /// </summary>
        /// <param name="indx">Specifies from where to push history down. Defaults at index 0.</param>
        private void PushJointHist(int indx = 0)
        {
            try
            {
                if (indx < this.historyLength - 1)
                {
                    // Recursive call to update older joint positions
                    this.PushJointHist(indx + 1);

                    foreach (JointType jointType in this.trackedJoints)
                    {
                        // Set the joint position directly older than this one, to this one.
                        if (this.jointHist[jointType][indx] != null)
                            this.jointHist[jointType][indx + 1] = this.jointHist[jointType][indx];
                    }
                }
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print("JointHistory PushJointHist ERROR: {0}", exc.Message);
            }
        }

        #endregion
    }
}
