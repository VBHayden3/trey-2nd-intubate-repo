﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using Microsoft.Kinect;
using System.Windows.Media.Imaging;
using System.Linq.Expressions;
using System.ComponentModel;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows;
using System.Diagnostics;
using System.Collections.Generic;
using AIMS.Utilities.Kinect;
using AIMS.Components;

namespace AIMS
{

    // Data Ready events
    public delegate void ColorDataReadyEventHandler();
    public delegate void DepthDataReadyEventHandler();
    public delegate void SkeletonDataReadyEventHandler();

    // Frame data events
    public delegate void ColorFrameReadyEventHandler(ColorFrame colorFrame);
    public delegate void DepthFrameReadyEventHandler(DepthFrame depthFrame);
    public delegate void BodyFrameReadyEventHandler(BodyFrame bodyFrame);
    public delegate void AllFramesReadyEventHandler();
    public delegate void AllDataReadyEventHandler();

    // Sensor events
    public delegate void SensorNotReadyEventHandler();
    public delegate void SensorReadyEventHandler();
    public delegate void SensorChangedEventHandler(string sensorStatus);

    /// <summary>
    /// KinectManager manages a single kinect for use across an entire application.
    /// 
    /// Call KinectManager.Shutdown() at application shutdown.
    /// </summary>
    /// <remarks>
    /// To take advantage of the KinectManager copying the data an event has been provided for each of the kinect's events.
    /// Instead of using the Kinect's events, use the equivalent KinectManager event to receive when the updates happen, then
    /// access the appropriate property to access the data.
    /// </remarks>
    public class KinectManager : INotifyPropertyChanged
    {
        /// <summary>
        /// Returns an instance of the KinectManager for use in binding, but allowing for property change notifications.
        /// </summary>
        public static KinectManager Instance
        {
            get
            {
                System.Diagnostics.Debug.WriteLine("KinectManager -> Creating new instance of KinectManager");
                return new KinectManager();
            }
        }

        public static KinectSensorChooser SensorChooser { get { return sensorChooser; } }
        private static readonly KinectSensorChooser sensorChooser = new KinectSensorChooser();

        // Constants
        //public static double DPI = 96.0;
        //public static PixelFormat FORMAT;
        //public static int BYTES_PER_PIXEL;

        public static readonly double DPI = 96.0;
        public static readonly PixelFormat FORMAT = PixelFormats.Bgr32;
        public static readonly int BYTES_PER_PIXEL = (FORMAT.BitsPerPixel + 7) / 8;

        // Properties
        private static KinectSensor kinect = null;
        private static MultiSourceFrameReader reader = null;
        private static bool hasKinect = false;
        
        private static byte[] colorData = null;
        private static int colorWidth = 0;
        private static int colorHeight = 0;
        private static TimeSpan colorFrameTime = new TimeSpan(0, 0, 0, 1);

        private static ushort[] depthFrameData = null;
        private static int depthWidth = 0;
        private static int depthHeight = 0;
        private static int reliableMinDepth = 0;
        private static int reliableMaxDepth = 99999;

        private static int activeBodyIndex = -1;
        private static Body[] bodies = null;
        private static Body closestBody = null;

        private static CoordinateMapper coordinateMapper = null;
        //private static ColorSpacePoint[] colorPoints = null;
        //private static CameraSpacePoint[] cameraPoints = null;
        private static byte[] mappedColorData = null;

        // Get and Set
        public static KinectSensor Kinect { get { return kinect; } }
        public static Speech.KinectSpeechCommander KinectSpeechCommander { get { return KinectManager.kinectSpeechCommander; } }
        public static bool HasKinect { get { return hasKinect; } }

        // Represents a color frame
        //public static ColorFrame ColorFrame { get { return colorFrame; } }
        public static byte[] ColorData { get { return colorData; } }
        public static int ColorWidth { get { return colorWidth; } }
        public static int ColorHeight { get { return colorHeight; } }
        public static TimeSpan ColorFrameTime { get { return colorFrameTime; } }
        // The data for this frame is stored as 16-bit unsigned integers, where each value represents the distance in millimeters.
        // The maximum depth distance is 8 meters, although reliability starts to degrade at around 4.5 meters.
        public static ushort[] DepthFrameData { get { return depthFrameData; } }
        public static int DepthWidth { get { return depthWidth; } }
        public static int DepthHeight { get { return depthHeight; } }
        public static int ReliableMinDepth { get { return reliableMinDepth; } }
        public static int ReliableMaxDepth { get { return reliableMaxDepth; } }
        // The computed data provided by this frame type includes skeletal joints and orientations, hand states, and more for up to 6 people at a time.
        public static Body[] Bodies { get { return bodies; } }

        public static Body ClosestBody { get { return closestBody; } }
        public static CoordinateMapper CoordinateMapper { get { return coordinateMapper; } }
        public static byte[] MappedColorData { get { return mappedColorData; } }
        private static List<SkeletonProfile> trackedSkeletons = new List<SkeletonProfile>();
        public static List<SkeletonProfile> SkeletonProfiles { get { return trackedSkeletons; } set { trackedSkeletons = value; } }

        /// <summary>
        /// Returns a reference to the KinectManager's SkeletonManager. Null when the Kinect is not initialized.
        /// </summary>
        public static SkeletonManager SkeletonManager { get { return KinectManager.skeletonManager; } }

        /// <summary>
        /// Maximum number of trackable bodies. ('skeleton'.TrackingState == SkeletonTrackingState.Tracked)
        /// </summary>
        public const int MaxTrackableSkeletons = 2;

        private const int RedIndex = 2;
        private const int GreenIndex = 1;
        private const int BlueIndex = 0;
        private static readonly int Bgra32BytesPerPixel = (PixelFormats.Bgra32.BitsPerPixel + 7) / 8;

        private static SkeletonManager skeletonManager;

        private static int counter = 0;

        #region Sticky Skeleton Properties

        private static int numStickySkeletons = 0;
        /// <summary>
        /// Value between 0 and KinectManager.MaxTrackableSkeletons. If 0, Sticky Skeletons feature is turned off, otherwise the value corresponds to the number
        /// of sticky skeletons for the KinectManager to acquire/maintain. When NumStickySkeletons value is changed, all sticky skeleton ID's are reset.
        /// </summary>
        public static int NumStickySkeletons
        {
            get { return numStickySkeletons; }
            set
            {
                if (value <= KinectManager.MaxTrackableSkeletons && value >= 0)
                {
                    numStickySkeletons = value;
                }
                else
                {
                    ArgumentOutOfRangeException aoore = new ArgumentOutOfRangeException("StickySkeletons",
                        "KinectManager.StickySkeletons value must be between 0 and " + KinectManager.MaxTrackableSkeletons.ToString() + ".");
                    throw aoore;
                }
                ResetStickySkeletonIDs();
                KinectManager.singleStickySkeletonProfile = null;
            }
        }

        private static StickySkelInfo[] stickySkelIDs = new StickySkelInfo[KinectManager.MaxTrackableSkeletons];
        //private static ulong?[] stickySkeletonIDs = new ulong?[KinectManager.MaxTrackableSkeletons];
        /// <summary>
        /// Return nullable integer array of StickySkeletonIDs. null if not set, only up to KinectManager.StickySkeletons sticky
        /// skeletons available.
        /// </summary>
        public static ulong?[] StickySkeletonIDs
        {
            get
            {
                ulong?[] SkelIDs = new ulong?[KinectManager.stickySkelIDs.Length];
                for (int i = 0; i < KinectManager.stickySkelIDs.Length; i++)
                    SkelIDs[i] = KinectManager.stickySkelIDs[i].SkelID;

                return SkelIDs;
            }
        }

        //public static StickySkelInfo[] stickySkelIDs { get { return (StickySkelInfo[])KinectManager.stickySkelIDs.Clone(); } }

        /// <summary>
        /// Sticky Skeleton Comparison Queue. Used to iterate through tracked skeletons over frames ( limited amount able to compare per frame ).
        /// </summary>
        private static Queue stickySkelCompQueue = new Queue();

        private static SkeletonProfile singleStickySkeletonProfile = null;
        public static SkeletonProfile SingleStickySkeletonProfile { get { return singleStickySkeletonProfile; } }

        #endregion // /Sticky Skeleton Properties

        public event PropertyChangedEventHandler PropertyChanged;

        public WriteableBitmap Silhouette
        {
            get
            {
                return silhouette;
            }

            private set
            {
                if (silhouette != value)
                {
                    silhouette = value;
                    this.OnPropertyChanged(() => this.Silhouette);
                }
            }
        }        

        private static SkeletonProfile closestSkeletonProfile = null;
        public static SkeletonProfile ClosestSkeletonProfile { get { return closestSkeletonProfile; } set { closestSkeletonProfile = value; } }

        /// <summary>
        /// Fired when ColorData has been updated
        /// Function Signature: void ColorDataReady()
        /// </summary>
        ///  @todo see if the argument needs changing
        //public static event EventHandler<ColorImageFrameReadyEventArgs> ColorDataReady;
        public static event ColorDataReadyEventHandler ColorDataReady;

        public static event ColorFrameReadyEventHandler ColorFrameReady;

        /// <summary>
        /// Fired when DepthData has been updated
        /// Function Signature: void DepthDataReady()
        /// </summary>
        public static event DepthDataReadyEventHandler DepthDataReady;

        /// <summary>
        /// Fired when BodyData has been updated
        /// Function Signature: void BodyDataReady()
        /// </summary>
        public static event BodyFrameReadyEventHandler BodyFrameReady;

        /// <summary>
        /// Fired when AllFramesReady has been called
        /// Function Signature: void AllFramesReady()
        /// </summary>
        public static event AllFramesReadyEventHandler AllFramesReady;
        /// <summary>
        /// Fired when AllDataReady has been called
        /// Function Signature: void AllDataReady()
        /// </summary>
        public static event AllDataReadyEventHandler AllDataReady;
        /// <summary>
        /// Fired when the SensorReady has been called. Indicates that the Kinect sensor is available to the system.
        /// Function Signature: void SensorReady()
        /// </summary>
        public static event SensorReadyEventHandler SensorReady;

        /// <summary>
        /// Fired when the SensorNotReady has been called. Indicates that the Kinect sensor is no longer available to the system.
        /// Function Signature: void SensorNotReady()
        /// </summary>
        public static event SensorNotReadyEventHandler SensorNotReady;

        /// <summary>
        /// Fired when the SensorChanged has been called. Indicates that the Kinect sensor status has changed and passes a string message to the subscriber.
        /// Function Signature: void SensorReady()
        /// </summary>
        public static event SensorChangedEventHandler SensorChanged;

        private static WriteableBitmap silhouette;
        private static Speech.KinectSpeechCommander kinectSpeechCommander = null;


        /// <summary>
        /// static KinectManager Constructor. This is executed the first time KinectManager is referenced.
        /// </summary>
        static KinectManager()
        {
            // Initialize all of the StickySkelInfo items in stickySkelIDs.
            for (int i = 0; i < KinectManager.stickySkelIDs.Length; i++)
                KinectManager.stickySkelIDs[i] = new StickySkelInfo();

            // Set all SkeletonIDs to null. This may belong in initializeKinect?
            ResetStickySkeletonIDs();
        }


        /// <summary>
        /// static KinectManager Startup. Initialize the KinectSensorChooser.  Call this at application start.
        /// </summary>
        public static void Startup()
        {

            System.Diagnostics.Debug.WriteLine("KinectManager -> Startup()");

            // Register KinectSensorChooser event
            KinectManager.sensorChooser.KinectSensorChanged += KinectSensorChanged;

            // Start the KinectSensorChooser
            kinect = KinectSensor.GetDefault();
            if (kinect != null)
            {
                // Open the KinectSensor for use
                kinect.Open();

                KinectManager.InitializeKinect(kinect);
            }
        }


        /// <summary>
        /// static KinectManager Shutdown. Deinitialize the current Kinect sensor and Stops the KinectSensorChooser.  Call this at application exit.
        /// </summary>
        public static void Shutdown()
        {
            System.Diagnostics.Debug.WriteLine("KinectManager -> Shutdown()");

            // Deinitialize current Kinect sensor. Unplug registered events and disable data streams.
            KinectManager.DeinitializeKinect(KinectManager.kinect, false);

            if (KinectManager.sensorChooser != null)
            {
                // Unregister KinectSensorChooser event
                KinectManager.sensorChooser.KinectSensorChanged -= KinectSensorChanged;
            }
        }


        /// <summary>
        /// Event handler for KinectSensorChooser's KinectChanged event.
        /// </summary>
        /// <param name="sender">
        /// Object sending the event.
        /// </param>
        /// <param name="e">
        /// Event arguments.
        /// </param>
        private static void KinectSensorChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("KinectManager -> KinectSensorChanged()");

                // Get the default sensor
                KinectSensor sensor = KinectSensor.GetDefault();
                if (sensor != null)
                {
                    KinectManager.DeinitializeKinect(sensor);
                    KinectManager.InitializeKinect(sensor);
                }
            }
            catch (InvalidOperationException)
            {
                // KinectSensor might enter an invalid state while enabling/disabling streams or stream features.
                // E.g.: sensor might be abruptly unplugged.
            }
        }


        /// <summary>
        /// Initialize a new Kinect sensor. Enable streams and register events. Starting the Kinect is handled by the KinectSensorChooser.
        /// </summary>
        private static void InitializeKinect(KinectSensor newSensor)
        {

            System.Diagnostics.Debug.WriteLine("KinectManager -> InitializeKinect");

            if (newSensor == null)
            {
                KinectManager.kinect = null;
                KinectManager.hasKinect = false;
                KinectManager.coordinateMapper = null;
                return;
            }

            if (!newSensor.IsOpen)
            {
                newSensor.Open();
            }

            // Specify which streams are needed
            if (reader == null)
            {
                System.Diagnostics.Debug.WriteLine("KinectManager -> InitializeKinect add reader...");

                // Plug into the kinect frame events
                reader = newSensor.OpenMultiSourceFrameReader(
                                         FrameSourceTypes.Color |
                                         FrameSourceTypes.Depth |
                                         FrameSourceTypes.Body);

                // Setup a handler for when a frame arrives
                reader.MultiSourceFrameArrived += Reader_MultiSourceFrameArrived;
            }            

            try
            {
                KinectManager.kinect = newSensor;   // set a reference to the found Kinect sensor.
                KinectManager.hasKinect = true;
                KinectManager.coordinateMapper = newSensor.CoordinateMapper;

                if (SensorReady != null)
                    SensorReady();
                if (SensorChanged != null)
                    SensorChanged("SensorReady");

                // Setup SkeletonManager
                KinectManager.skeletonManager = new SkeletonManager();
            }
            catch (InvalidOperationException exception)
            {
                // Kinect sensor entered an invalid state while initializing.
                // This exception can be thrown when we are trying to enable streams on a device that has gone away.
                // E.g.: sensor might be abruptly unplugged.
                // Catch the exception and assume another notification will come along if a sensor comes back.
                MessageBox.Show(exception.Message);
                KinectManager.kinect = null;
                KinectManager.hasKinect = false;
                KinectManager.coordinateMapper = null;
            }

            try
            {
                // Add KinectSpeechCommander (it's not required)
                System.Diagnostics.Debug.WriteLine("KinectManager -> InitializeKinect add speech commander...");
                KinectManager.kinectSpeechCommander = new Speech.KinectSpeechCommander()
                {
                    ActiveListeningTimeout = 5,
                    DefaultConfidenceThreshold = 0.65F,
                    AimsDefaultListeningGrammarUri = new System.Uri("pack://application:,,,/AIMS;component/Assets/AIMSSpeechGrammar.xml"),
                    AimsDefaultListeningGrammarRule = "defaultListen",
                    AimsActiveListeningGrammarUri = new System.Uri("pack://application:,,,/AIMS;component/Assets/AIMSSpeechGrammar.xml"),
                    AimsActiveListeningGrammarRule = "activeListen"
                };
                KinectManager.kinectSpeechCommander.SpeechRecognized += new EventHandler<Speech.SpeechCommanderEventArgs>(kinectSpeechCommander_SpeechRecognized);
                KinectManager.kinectSpeechCommander.Start(newSensor);
            }
            catch (InvalidOperationException exception)
            {
                Debug.Print("KinectManager-> Error with kinectSpeechCommander: " + exception.Message);
            }

        }

        private static void Reader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            // Get a reference to the multi-frame
            var multiSourceFrame = e.FrameReference.AcquireFrame();

            // If the Frame has expired by the time we process this event, return
            if (multiSourceFrame == null)
            {
                System.Diagnostics.Debug.WriteLine("KinectManager -> null frame arrived");
                return;
            }

            // Purposely slow things down for testing... either throw more hardware or use a smaller frame for work
            if (counter < 1) {
                ++counter;
                return;
            }
            counter = 0;

            ColorFrame colorFrame = null;
            DepthFrame depthFrame = null;
            BodyFrame bodyFrame = null;

            try
            {
                //System.Diagnostics.Debug.WriteLine("KinectManager -> acquired multi frame");
        
                colorFrame = multiSourceFrame.ColorFrameReference.AcquireFrame();
                depthFrame = multiSourceFrame.DepthFrameReference.AcquireFrame();
                bodyFrame = multiSourceFrame.BodyFrameReference.AcquireFrame();

                //System.Diagnostics.Debug.WriteLine("KinectManager -> processing frames...");
                if (colorFrame != null)
                {
                    KinectColorImageFrameReady(colorFrame);
                }
                if (depthFrame != null)
                {
                    KinectDepthImageFrameReady(depthFrame);
                }
                if (bodyFrame != null)
                {
                    KinectBodyFrameReady(bodyFrame);
                }
                if ((colorFrame != null)
                   && (depthFrame != null)
                   && (bodyFrame != null))
                {
                    KinectAllFramesReady();
                }
                //System.Diagnostics.Debug.WriteLine("KinectManager -> done.");
            }
            finally
            {
                //System.Diagnostics.Debug.WriteLine("KinectManager -> dispose of frames...");
                if (colorFrame != null)
                {
                    colorFrame.Dispose();
                }
                if (depthFrame != null)
                {
                    depthFrame.Dispose();
                }
                if (bodyFrame != null)
                {
                    bodyFrame.Dispose();
                }
            }        
        }


        /// <summary>
        /// Deinitialize the current or old Kinect sensor by unplugging the event handlers and disabling the streams. Stopping the Kinect is handled by the KinectSensorChooser.
        /// </summary>
        /// <param name="dispose">
        /// Optional parameter to specify that the SpeechEngine should call Dispose.
        /// </param>
        private static void DeinitializeKinect(KinectSensor sensor, bool dispose = true)
        {

            if (reader != null)
            {
                reader.Dispose();
                reader = null;
            }

            if (sensor != null)
            {
                if (SensorNotReady != null)
                    SensorNotReady();
                if (SensorChanged != null)
                    SensorChanged("SensorNotReady");

                // Unset the skeleton manager.
                KinectManager.skeletonManager = null;

                if (KinectManager.kinectSpeechCommander != null)
                {
                    KinectManager.kinectSpeechCommander.Stop();
                    KinectManager.kinectSpeechCommander.SpeechRecognized -= kinectSpeechCommander_SpeechRecognized; // Unplug from the Speech Recognized event
                    if (dispose)
                        KinectManager.kinectSpeechCommander.Dispose();
                    KinectManager.kinectSpeechCommander = null;
                }

                if (sensor.AudioSource != null)
                {
                    // @todo fix
                    //sensor.AudioSource.Stop();
                }

                if (sensor.IsOpen)
                {
                    sensor.Close();
                }
                KinectManager.kinect = null;
                KinectManager.hasKinect = false;
                KinectManager.coordinateMapper = null;
            }
        }


        static void kinectSpeechCommander_SpeechRecognized(object sender, Speech.SpeechCommanderEventArgs e)
        {
            switch (e.Command)
            {
                case "LOGOUT":
                    break;
                default:
                    break;
            }
        }

        // @todo verify all uses of this method for the right point scale
        public static int GetDepthValue(int x, int y)
        {
            try
            {
                return depthFrameData[y * 512 + x];
            }
            catch
            {
                System.Diagnostics.Debug.WriteLine("KinectManager -> could not get depth for depth point (" + x + "," + y + ")");
                return 0;
            }
        }

        public static int GetDepthValueForColorPoint(Point p)
        {
            try
            {
                // Returns the depth in mm
                DepthSpacePoint[] depthPoints = new DepthSpacePoint[colorWidth * colorHeight];
                coordinateMapper.MapColorFrameToDepthSpace(depthFrameData, depthPoints);
                DepthSpacePoint thisPoint = depthPoints[((int)p.Y * 1920 + (int)p.X)];
                return depthFrameData[(int)thisPoint.Y * 512 + (int)thisPoint.X];
            }
            catch
            {
                System.Diagnostics.Debug.WriteLine("KinectManager -> could not get depth for color point (" + (int)p.X + "," + (int)p.Y + ")");
                return 0;
            }
        }

        public static Point3D Get3DPointForColorPoint(Point p2, int depth)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("KinectManager -> Get3DPointForColorPoint for (" + p2.X + "," + p2.Y + "," + depth + "mm)");
                DepthSpacePoint[] depthPoints = new DepthSpacePoint[colorWidth * colorHeight];
                coordinateMapper.MapColorFrameToDepthSpace(depthFrameData, depthPoints);
                DepthSpacePoint thisPoint = depthPoints[((int)p2.Y * 1920 + (int)p2.X)];
                ushort z = depthFrameData[(int)thisPoint.Y * 512 + (int)thisPoint.X];
                System.Diagnostics.Debug.WriteLine("KinectManager -> Get3DPointForColorPoint for (z=" + z + "mm)");
                CameraSpacePoint p = coordinateMapper.MapDepthPointToCameraSpace(thisPoint, z);
                System.Diagnostics.Debug.WriteLine("KinectManager -> Get3DPointForColorPoint CameraSpacePoint = (" + p.X + "," + p.Y + "," + p.Z + "m)");
                // Creating Point3D... z should be in mm, not m
//                return new Point3D(p.X, p.Y, (int) p.Z * 1000);
                return new Point3D(p.X, p.Y, p.Z * 1000);
            }
            catch
            {
                System.Diagnostics.Debug.WriteLine("KinectManager -> could not get 3D point for (" + p2.X + "," + p2.Y + ")");
                return new Point3D();
            }
        }

        /// <summary>
        /// Finds the distance between two skeleton points. Uses X, Y
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double Distance2D(CameraSpacePoint a, CameraSpacePoint b)
        {
            return System.Math.Sqrt(System.Math.Pow((a.X - b.X), 2) + System.Math.Pow((a.Y - b.Y), 2));
        }

        /// <summary>
        /// Finds the distance between two skeleton points. Uses X, Y, Z
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double Distance3D(CameraSpacePoint a, CameraSpacePoint b)
        {
            return System.Math.Sqrt(System.Math.Pow((a.X - b.X), 2) + System.Math.Pow((a.Y - b.Y), 2) + System.Math.Pow((a.Z - b.Z), 2));
        }

        /// <summary>
        /// Finds the angle in degrees for abc.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <returns>Angle in degrees between -180 and 180</returns>
        public static double Angle2D(CameraSpacePoint a, CameraSpacePoint b, CameraSpacePoint c)
        {
            System.Windows.Vector AB = new System.Windows.Vector(a.X - b.X, a.Y - b.Y);
            System.Windows.Vector BC = new System.Windows.Vector(c.X - b.X, c.Y - b.Y);

            return System.Windows.Vector.AngleBetween(AB, BC);
        }

        /// <summary>
        /// Finds the angle in degrees for abc.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <returns>Angle in degrees between 0 and 180</returns>
        public static double Angle3D(CameraSpacePoint a, CameraSpacePoint b, CameraSpacePoint c)
        {
            System.Windows.Media.Media3D.Vector3D AB = new System.Windows.Media.Media3D.Vector3D(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
            System.Windows.Media.Media3D.Vector3D BC = new System.Windows.Media.Media3D.Vector3D(c.X - b.X, c.Y - b.Y, c.Z - b.Z);

            return System.Windows.Media.Media3D.Vector3D.AngleBetween(AB, BC);
        }

        public static double AltitudeAngle(CameraSpacePoint a, CameraSpacePoint b)
        {
            double x = a.X - b.X;
            double y = a.Y - b.Y;
            double z = a.Z - b.Z;

            //According to google: 1 radian = 57.2957795 degrees
            double altitude = System.Math.Atan2(y, System.Math.Sqrt(x * x + z * z)) * 57.2957795;

            return altitude;
        }

        public static double AzimuthAngle(CameraSpacePoint a, CameraSpacePoint b)
        {
            double x = a.X - b.X;
            double y = a.Y - b.Y;
            double z = a.Z - b.Z;

            //According to google: 1 radian = 57.2957795 degrees
            double azimuth = System.Math.Atan2(-x, -z) * 57.2957795;

            return azimuth;
        }

        /// <summary>
        /// Returns the skeleton associated with a passed in Skeleton Tracking ID.
        /// </summary>
        /// <param name="trackingId">Skeleton Tracking ID associated with desired Skeleton.</param>
        /// <param name="stickyOnly">TRUE: Only returns sticky skeletons. FALSE: Returns null if not a sticky skeleton. DEFAULT: FALSE</param>
        /// <returns>If the Skeleton with the passed in trackingId is found, it is returned. Otherwise, null is returned.</returns>
        public static Body GetSkeleton(ulong trackingId, bool stickyOnly = false)
        {
            if (bodies != null && bodies.Length != 0)
            {
                foreach (Body body in bodies)
                {
                    if (body.TrackingId == trackingId)
                    {
                        if (stickyOnly)
                        {
                            // Check if it's in the sticky array.
                            foreach (StickySkelInfo stickyid in KinectManager.stickySkelIDs)
                            {
                                if (stickyid.SkelID.HasValue && (ulong)stickyid.SkelID == trackingId)
                                {
                                    return body;
                                }
                            }
                        }
                        else
                        {
                            return body;
                        }
                    }
                }
            }
            return null;
        }
        

        /// <summary>
        /// Returns the SkeletonProfile associated with a passed in Skeleton Tracking ID.
        /// </summary>
        /// <param name="trackingId">Skeleton Tracking ID associated with desired Skeleton.</param>
        /// <param name="stickyOnly">TRUE: Only returns sticky skeletons. FALSE: Returns null if not a sticky skeleton. DEFAULT: FALSE</param>
        /// <returns>If the Skeleton with the passed in trackingId is found, it is returned. Otherwise, null is returned.</returns>
        public static SkeletonProfile GetSkeletonProfile(ulong? trackingid, bool stickyOnly = false)
        {
            if (!trackingid.HasValue)
                return null;

            foreach (SkeletonProfile profile in KinectManager.SkeletonProfiles)
            {
                if (profile.Disposed)
                    continue;

                if (profile.CurrentTrackingID == trackingid)
                {
                    if (stickyOnly)
                    {
                        // Check that it's a sticky.
                        foreach (StickySkelInfo stickyid in KinectManager.stickySkelIDs)
                        {
                            if (stickyid.SkelID.HasValue && (ulong)stickyid.SkelID == trackingid)
                            {
                                return profile;
                            }
                        }

                        return null;    // return null if it was stickyOnly but no sticky was found.
                    }
                    else
                    {
                        return profile;
                    }
                }
            }

            return null;
        }

        public static SkeletonProfile GetSkeletonProfile(ulong? trackingid, bool stickyonly, out int stickyIndex)
        {
            stickyIndex = -1;

            if (!trackingid.HasValue)
                return null;

            foreach (SkeletonProfile profile in KinectManager.SkeletonProfiles)
            {
                if (profile.CurrentTrackingID == trackingid)
                {
                    if (stickyonly)
                    {
                        int index = 0;

                        // Check that it's a sticky.
                        foreach (StickySkelInfo stickyid in KinectManager.stickySkelIDs)
                        {
                            if (stickyid.SkelID.HasValue && (ulong)stickyid.SkelID == trackingid)
                            {
                                stickyIndex = index;
                                return profile;
                            }

                            index++;
                        }

                        return null;    // return null if it was stickyOnly but no sticky was found.
                    }
                    else
                    {
                        return profile;
                    }
                }
            }
            return null;
        }

        private static void KinectColorImageFrameReady(ColorFrame frame)
        {
            if (frame == null)
                    return;

            colorFrameTime = frame.RelativeTime;

            if (ColorFrameReady != null)
            {
                ColorFrameReady(frame);
            }

            if (ColorDataReady != null)
            {
                if (colorData == null)
                {
                    colorWidth = frame.FrameDescription.Width;
                    colorHeight = frame.FrameDescription.Height;
                    colorData = new byte[colorWidth * colorHeight * ((PixelFormats.Bgr32.BitsPerPixel + 7) / 8)];
                    System.Diagnostics.Debug.WriteLine("KinectManager -> KinectColorImageFrameReady() generating ColorData... (" + colorWidth + "x" + colorHeight + ")");
                }

                if (frame.RawColorImageFormat == ColorImageFormat.Bgra)
                {
                    frame.CopyRawFrameDataToArray(colorData);
                }
                else
                {
                    frame.CopyConvertedFrameDataToArray(colorData, ColorImageFormat.Bgra);
                }
           
                //System.Diagnostics.Debug.WriteLine("KinectManager -> executing ColorDataReady()");
                ColorDataReady();
            }
        }

        private static void KinectDepthImageFrameReady(DepthFrame frame)
        {
            if (frame == null)
                    return;

            if (depthWidth == 0)
            {
                depthWidth = frame.FrameDescription.Width;
                depthHeight = frame.FrameDescription.Height;
            }

            // In mm
            reliableMinDepth = frame.DepthMinReliableDistance;
            reliableMaxDepth = frame.DepthMaxReliableDistance;

            if (depthFrameData == null)
            {
                depthFrameData = new ushort[depthWidth * depthHeight];
            }
            frame.CopyFrameDataToArray(depthFrameData);

            if (DepthDataReady != null)
            {
                //System.Diagnostics.Debug.WriteLine("KinectManager -> executing DepthDataReady()");
                DepthDataReady();
            }
        }

        private static void KinectBodyFrameReady(BodyFrame frame)
        {
            if (frame == null)
                return;

            bodies = new Body[frame.BodyCount];

            frame.GetAndRefreshBodyData(bodies);

            for (int i = 0; i < bodies.Length; i++)
            {
                Body body = bodies[i];
                if (body.IsTracked)
                {
                    bool alreadyKnown = false;  // Flag to determine if the skeleton is new or already known.

                    foreach (SkeletonProfile profile in KinectManager.SkeletonProfiles) // Check each of the known SkeletonProfiles.
                    {
                        if (profile.CurrentTrackingID.HasValue && profile.CurrentTrackingID == body.TrackingId)    // Does the skeleton's tracking id match any of the profiles?
                        {
                            //profile.UpdateSkeletonData(skele);  // If yes, update the profile with the new skeleton data. (COMMENTED OUT --> Update all afterwards).
                            alreadyKnown = true;    // Set the flag.
                            break;  // Don't check the rest of the profiles.
                        }
                    }
                    if (!alreadyKnown)  // No profile exists.
                    {
                        KinectManager.SkeletonProfiles.Add(new SkeletonProfile(body.TrackingId));  // Create a new SkeletonProfile for this skeleton.
                    }

                    // Update the LastPosition in stickySkelIDs
                    foreach (StickySkelInfo skelInfo in KinectManager.stickySkelIDs)
                    {
                        if (skelInfo.SkelID == body.TrackingId)
                            skelInfo.LastPosition = body.Joints[JointType.SpineMid].Position;
                    }
                }
            }

            List<SkeletonProfile> toDispose = new List<SkeletonProfile>();

            foreach (SkeletonProfile profile in KinectManager.SkeletonProfiles) // Go through each of the profiles.
            {
                bool foundProfileSkeleton = false;
                foreach (Body skele in Bodies)    // Check if any of the skeletons are linked to that profile.
                {
                    if (profile.CurrentTrackingID == skele.TrackingId)
                    {
                        // Match found.
                        profile.UpdateSkeletonData(skele);

                        foundProfileSkeleton = true;    // Indicate the profile has found it's skeleton.

                        if (profile.TimeToLive != 30)
                        {
                            profile.TimeToLive = 30;
                        }

                        break;  // don't check the rest of the skeletons.
                    }
                }

                if (!foundProfileSkeleton)
                {
                    profile.TimeToLive--;

                    if (profile.TimeToLive <= 0)
                        toDispose.Add(profile);
                }
            }

            foreach (SkeletonProfile profile in toDispose)
            {
                KinectManager.SkeletonProfiles.Remove(profile);

                if (KinectManager.SingleStickySkeletonProfile == profile)
                    KinectManager.singleStickySkeletonProfile = null;

                profile.Dispose();
            }

            toDispose.Clear();

            // Find / set the closestSkeleton
            closestBody = KinectManager.GetClosestSkeleton(bodies);
            closestSkeletonProfile = KinectManager.GetClosestSkeletonProfile(KinectManager.SkeletonProfiles);

            // Find / set the Sticky Skeletons ( if feature is enabled )
            if (KinectManager.numStickySkeletons > 0)
            {
                KinectManager.SetStickySkeletons(bodies);

                // Set KinectManager.singleStickySkeletonProfile to existing sticky skeleton if KinectManager.numStickSkeletons == 1
                if (KinectManager.numStickySkeletons == 1)
                {
                    foreach (StickySkelInfo stickySkelId in KinectManager.stickySkelIDs)
                    {
                        if (stickySkelId.SkelID.HasValue)
                        {
                            KinectManager.singleStickySkeletonProfile = KinectManager.GetSkeletonProfile(stickySkelId.SkelID.Value, true);
                            break;
                        }
                    }
                }
                else
                    KinectManager.singleStickySkeletonProfile = null;
            }

            //debugOutSkelProfiles();

            if (BodyFrameReady != null)
                BodyFrameReady(frame);
        }

        private static void debugOutSkelProfiles()
        {
            string output = "SkeletonProfiles: ";

            output += "Total: " + SkeletonProfiles.Count + ".";
            foreach (SkeletonProfile prof in SkeletonProfiles)
            {
                output += " " + prof.CurrentTrackingID;
            }
            output += ".";
            
            System.Diagnostics.Debug.WriteLine(output);
        }

        private static void KinectAllFramesReady()
        {

            if (AllFramesReady != null)
            {
                //System.Diagnostics.Debug.WriteLine("KinectManager -> executing AllFramesReady()");
                AllFramesReady();
            }

            if (AllDataReady != null)
            {

                //System.Diagnostics.Debug.WriteLine("KinectManager -> KinectAllFramesReady() preparing data...");

                ColorSpacePoint[] colorPoints = new ColorSpacePoint[depthWidth * depthHeight];

                // map ushort[] to ColorSpacePoint[] 
                coordinateMapper.MapDepthFrameToColorSpace(depthFrameData, colorPoints);

                if (mappedColorData == null)
                {
                    mappedColorData = new byte[depthWidth * depthHeight * BYTES_PER_PIXEL];
                }

                // round down to the nearest pixel
                for (int depthIndex = 0; depthIndex < depthFrameData.Length; ++depthIndex)
                {
                    ColorSpacePoint point = colorPoints[depthIndex];

                    int colorX = (int)System.Math.Floor(point.X + 0.5);
                    int colorY = (int)System.Math.Floor(point.Y + 0.5);
                    if ((colorX >= 0) && (colorX < colorWidth) && (colorY >= 0) && (colorY < colorHeight))
                    {
                        int colorImageIndex = ((colorWidth * colorY) + colorX) * BYTES_PER_PIXEL;
                        int depthPixel = depthIndex * BYTES_PER_PIXEL;

                        mappedColorData[depthPixel] = colorData[colorImageIndex];
                        mappedColorData[depthPixel + 1] = colorData[colorImageIndex + 1];
                        mappedColorData[depthPixel + 2] = colorData[colorImageIndex + 2];
                        mappedColorData[depthPixel + 3] = 255;
                    }
                }

                //System.Diagnostics.Debug.WriteLine("KinectManager -> executing AllDataReady()");
                AllDataReady();
            }
        }
        

        /// <summary>
        /// Set all StickySkeletonIDs to null.
        /// </summary>
        private static void ResetStickySkeletonIDs()
        {
            for (int i = 0; i < stickySkelIDs.Length; i++)
                stickySkelIDs[i] = new StickySkelInfo();
        }

        /// <summary>
        /// Determines which skeleton should be tracked for hands-free button clicking.
        /// It will select the closest skeleton to the Kinect sensor.
        /// </summary>
        /// <param name="skeletons"></param>
        /// <returns></returns>
        private static Body GetClosestSkeleton(Body[] skeletons)
        {
            Body skeleton = null;

            if (skeletons != null)
            {
                for (int i = 0; i < skeletons.Length; i++)
                {
                    // Because of this, we will revert to using the closest known skeleton.
                    if (skeletons[i].IsTracked)
                    {
                        if (skeleton == null)
                        {
                            skeleton = skeletons[i];
                        }
                        else
                        {
                            if (skeleton.Joints[JointType.SpineMid].Position.Z > skeletons[i].Joints[JointType.SpineMid].Position.Z)    // Compare the current skeleton's Z position to that of the previous to identify which is closest.
                            {
                                skeleton = skeletons[i];
                            }
                        }
                    }
                }
            }

            return skeleton;    // return the primary skeleton, being null, or the closest.
        }

        private static SkeletonProfile GetClosestSkeletonProfile( List<SkeletonProfile> skeletonprofiles )
        {
            SkeletonProfile closest = null;

            if (skeletonprofiles != null)
            {
                foreach (SkeletonProfile profile in skeletonprofiles)
                {
                    // @todo review this line... 
                    if (profile.CurrentTrackingState == true)
                    {
                        if (closest == null)
                            closest = profile;
                        else
                        {
                            if (profile.CurrentPosition.Z < closest.CurrentPosition.Z)  // If the profile is closer than the current nearest
                                closest = profile;
                        }
                    }
                }
            }

            return closest; // return the closest skeleton, being null or the 
        }

        /// <summary>
        /// Set Sticky Skeletons, up to KinectManager.StickySkeletons. Sticky Skeleton ID's will be put in the KinectManager.StickySkeletonIDs 
        /// nullable integer array.
        /// </summary>
        /// <param name="skeletons">Collection of Kinect Skeletons to use, from the SkeletonFrame.</param>
        private static void SetStickySkeletons(Body[] skeletons)
        {
            if (skeletons != null && skeletons.Length > 0)
            {
                // Check if previous sticky skeletons are still being tracked or not. Clean sticky skel ID's not being used.
                bool unusedStickySkels = true;
                int trackedStickySkels = 0;
                for (int j = 0; j < KinectManager.numStickySkeletons; j++)
                {
                    unusedStickySkels = true;
                    // Iterate over skeletons
                    foreach (Body skel in skeletons)
                    {
                        // If sticky skeleton still exists in the scene.
                        if (skel.IsTracked && skel.TrackingId == KinectManager.stickySkelIDs[j].SkelID)
                        {
                            trackedStickySkels++;
                            unusedStickySkels = false;
                            break;
                        }
                    }
                    // Only remove sticky skeletons which have left the scene.
                    if (unusedStickySkels)
                    {
                        if(KinectManager.stickySkelIDs[j].SkelID.HasValue)
                            KinectManager.stickySkelIDs[j].NotFound();
                        //KinectManager.stickySkelIDs[j].SkelID = null;
                    }

                    // If space for a skeleton to become sticky, check for stance.
                    if (trackedStickySkels <= j)
                        unusedStickySkels = true;
                }

                // Check skeleton Stances to find sticky skeletons
                if (unusedStickySkels)
                {
                    // Iterate over skeletons
                    foreach (Body skel in skeletons)
                    {
                        bool checkSkel = true;
                        // Skip checking the stance of sticky skeletons.
                        foreach (StickySkelInfo stickySkel in stickySkelIDs)
                        {
                            if (stickySkel.SkelID == skel.TrackingId)
                                checkSkel = false;
                        }
                        // If the skeleton is Tracked or PositionOnly and isn't a sticky skeleton.
                        if (checkSkel && skel.IsTracked)
                        {
                            // Position only skeletons go in the queue to be checked in later frames
                            //if (skel.TrackingState == SkeletonTrackingState.PositionOnly)
                            if (!skel.IsTracked)
                            {
                                if (!KinectManager.stickySkelCompQueue.Contains(skel.TrackingId))
                                    KinectManager.stickySkelCompQueue.Enqueue(skel.TrackingId);
                            }
                            else // Check tracked skeletons for stance. If stance found make sticky, otherwise put on back of queue.
                            {
                                // If either hand is above the head whilst the other is below the head, STANCE FOUND!
                                // set this up with Delegates!
                                if ((skel.Joints[JointType.HandRight].Position.Y > skel.Joints[JointType.Head].Position.Y
                                    && skel.Joints[JointType.HandLeft].Position.Y < skel.Joints[JointType.Head].Position.Y)
                                    || (skel.Joints[JointType.HandLeft].Position.Y > skel.Joints[JointType.Head].Position.Y
                                    && skel.Joints[JointType.HandRight].Position.Y < skel.Joints[JointType.Head].Position.Y))
                                {
                                    for (int k = 0; k < numStickySkeletons; k++)
                                    {
                                        // If the Sticky Skeleton ID is null, then add the new Tracking ID as a Sticky Skeleton ID and exit stickySkeleton loop.
                                        if (KinectManager.stickySkelIDs[k].SkelID == null)
                                        {
                                            KinectManager.stickySkelIDs[k].SkelID = skel.TrackingId;
                                            trackedStickySkels++;
                                            break;
                                        }
                                        // Else if the Sticky Skeletin is not active ( but still living ) check its position vs other skeleton
                                        else if (!KinectManager.stickySkelIDs[k].Active)
                                        {
                                            if (KinectManager.ComparePositions(skel.Joints[JointType.SpineMid].Position, KinectManager.stickySkelIDs[k].LastPosition))
                                            {
                                                KinectManager.stickySkelIDs[k].SkelID = skel.TrackingId;
                                                trackedStickySkels++;
                                                break;
                                    }
                                }
                                    }
                                }
                                else // Stance was not found on tracked skeleton.
                                {
                                    bool enqueue = true;

                                    for (int k = 0; k < numStickySkeletons; k++)
                                    {
                                        if (!KinectManager.stickySkelIDs[k].Active && KinectManager.stickySkelIDs[k].SkelID.HasValue)
                                        {
                                            if (KinectManager.ComparePositions(skel.Joints[JointType.SpineMid].Position, KinectManager.stickySkelIDs[k].LastPosition))
                                            {
                                                KinectManager.stickySkelIDs[k].SkelID = skel.TrackingId;
                                                trackedStickySkels++;
                                                enqueue = false;
                                                break;
                                            }
                                        }
                                    }
                                    // Put tracked skeleton on back of the qeueue.
                                    if (!KinectManager.stickySkelCompQueue.Contains(skel.TrackingId) && enqueue)
                                        KinectManager.stickySkelCompQueue.Enqueue(skel.TrackingId);
                                }
                            }
                        } // /if (skel.TrackingState != SkeletonTrackingState.NotTracked)
                    } // /foreach (Skeleton skel in skeletons)
                } // /if (unusedStickySkels)

                // Verify all sticky skeletons are being tracked ( Chosen ). Important for cases when 
                // sticky skeleton IDs are set outside of SetStickySkeletons, for example ForceStickySkeleton.
                bool rechoose = false;
                foreach (StickySkelInfo stickySkelID in KinectManager.stickySkelIDs)
                {
                    foreach (Body skel in skeletons)
                    {
                        if (stickySkelID.SkelID.HasValue)
                        {
                            if (skel.TrackingId == stickySkelID.SkelID && !skel.IsTracked)
                            {
                                rechoose = true;
                                break;
                            }
                        }
                    }
                }

                // Need to choose new skeletons to track from the SkeletonStream?
                if (unusedStickySkels || rechoose)
	            {
                    // Choose skeletons to track on next frame, from queue. ( Sticky and non-Sticky with Sticky priority )
                    try
                    {
                        // @todo review
                        /*
                        // Both sticky skeletons.
                        if (trackedStickySkels > 1)
                            kinect.SkeletonStream.ChooseSkeletons((int)KinectManager.stickySkelIDs[0].SkelID, (int)KinectManager.stickySkelIDs[1].SkelID);

                        // Sticky and non-sticky skeletons.
                        else if (trackedStickySkels == 1 && KinectManager.stickySkelCompQueue.Count > 0)
                            kinect.SkeletonStream.ChooseSkeletons(KinectManager.stickySkelIDs[0].SkelID ?? (int)KinectManager.stickySkelIDs[1].SkelID,
                                (int)KinectManager.stickySkelCompQueue.Dequeue());

                        // Only sticky skeleton, no other skeletons.
                        else if (trackedStickySkels == 1 && KinectManager.stickySkelCompQueue.Count == 0)
                            kinect.SkeletonStream.ChooseSkeletons(KinectManager.stickySkelIDs[0].SkelID ?? (int)KinectManager.stickySkelIDs[1].SkelID);

                        // Both non-sticky skeletons.
                        else if (KinectManager.stickySkelCompQueue.Count > 1)
                            kinect.SkeletonStream.ChooseSkeletons((int)KinectManager.stickySkelCompQueue.Dequeue(), (int)KinectManager.stickySkelCompQueue.Dequeue());

                        // Only 1 non-sticky skeleton.
                        else if (KinectManager.stickySkelCompQueue.Count > 0)
                            kinect.SkeletonStream.ChooseSkeletons((int)KinectManager.stickySkelCompQueue.Dequeue());
                        */
                    }
                    catch (InvalidOperationException ioe)
                    {
                        // Queue out of elements...
                        System.Diagnostics.Debug.WriteLine("KinectManager.SetStickySkeletons() ERROR: " + ioe.ToString());
                    }
                    catch (Exception exc)
                    {
                        System.Diagnostics.Debug.WriteLine("KinectManager.SetStickySkeletons() ERROR: " + exc.Message);
                    }
                }
            } // /if (skeletons != null && skeletons.Length > 0)
        }        

         /// <summary>
         /// Sets skeleton identified by TrackingID to sticky if the current stuck skeletons is less than NumStickySkeletons.
         /// </summary>
         /// <param name="TrackingID">TrackingID of skeleton to forceSticky.</param>
         public static void ForceStickySkeleton(ulong TrackingID)
         {
             for (int i = 0; i < KinectManager.numStickySkeletons; i++)
             {
                 if (!KinectManager.stickySkelIDs[i].SkelID.HasValue)
                 {
                     KinectManager.stickySkelIDs[i].SkelID = TrackingID;
                 }
             }
         }

        /// <summary>
        /// This will create a new Screenshot (Color/Depth 3D Screenshot), provided that the colorData and depthFrame in KinectManager are not currently null.
        /// All Screenshot's are compressed upon "set", and un-compresed upon "get".
        /// </summary>
        /// <returns></returns>
        public static StageScore.Screenshot CreateScreenshot()
        {
            if (colorData == null || depthFrameData == null)
            {
                // Don't create a screenshot if there isn't enough.
                return null;
            }

            return new StageScore.Screenshot(colorData, depthFrameData);
        }

        /// <summary>
        /// Compares skeleton positions to see if they are nearly identical. ( Identify whether they occupy the same space )
        /// </summary>
        /// <param name="Position1">First skeleton position to compare with.</param>
        /// <param name="Position2">Second skeleton position to compare with.</param>
        /// <returns></returns>
        private static bool ComparePositions(CameraSpacePoint Position1, CameraSpacePoint Position2)
        {
/*            System.Diagnostics.Debug.Print("SKELETON POSITIONS BEING COMPARED! Pos1: {0} {1} {2}  Pos2: {3} {4} {5}",
                Position1.X, Position1.Y, Position1.Z, Position2.X, Position2.Y, Position2.Z);*/

            // 15 cm is ~ 1/2 foot.
            double xThresh = 0.15;
            double yThresh = 0.15;
            double zThresh = 0.15;

            if (System.Math.Abs(Position1.X - Position2.X) <= xThresh &&
                System.Math.Abs(Position1.Y - Position2.Y) <= yThresh &&
                System.Math.Abs(Position1.Z - Position2.Z) <= zThresh)
            {
                return true;
            }
            else
                return false;
        }

        /*
        private static void GetPlayerSilhouette(DepthImageFrame depthFrame, int playerIndex)
        {
            try
            {
                if (depthFrame != null)
                {
                    bool haveNewFormat = LastImageFormat != depthFrame.Format;

                    short[] pixelData = new short[depthImageFrame.PixelDataLength];
                    byte[] depthFrame32 = new byte[depthFrame.Width * depthFrame.Height * Bgra32BytesPerPixel];
                    byte[] convertedDepthBits = new byte[depthFrame32.Length];

                    if (haveNewFormat)
                    {
                        pixelData = new short[depthFrame.PixelDataLength];
                        depthFrame32 = new byte[depthFrame.Width * depthFrame.Height * Bgra32BytesPerPixel];
                        convertedDepthBits = new byte[depthFrame32.Length];
                    }

                    depthFrame.CopyPixelDataTo(pixelData);

                    for (int i16 = 0, i32 = 0; i16 < pixelData.Length && i32 < depthFrame32.Length; i16++, i32 += 4)
                    {
                        int player = pixelData[i16] & DepthImageFrame.PlayerIndexBitmask;

                        if (player == playerIndex)
                        {
                            convertedDepthBits[i32 + RedIndex] = 0x44;
                            convertedDepthBits[i32 + GreenIndex] = 0x23;
                            convertedDepthBits[i32 + BlueIndex] = 0x59;
                            convertedDepthBits[i32 + 3] = 0x66;
                        }
                        else if (player > 0)
                        {
                            convertedDepthBits[i32 + RedIndex] = 0xBC;
                            convertedDepthBits[i32 + GreenIndex] = 0xBE;
                            convertedDepthBits[i32 + BlueIndex] = 0xC0;
                            convertedDepthBits[i32 + 3] = 0x66;
                        }
                        else
                        {
                            convertedDepthBits[i32 + RedIndex] = 0x0;
                            convertedDepthBits[i32 + GreenIndex] = 0x0;
                            convertedDepthBits[i32 + BlueIndex] = 0x0;
                            convertedDepthBits[i32 + 3] = 0x0;
                        }
                    }

                    if (silhouette == null || haveNewFormat)
                    {
                        silhouette = new WriteableBitmap(depthFrame.Width, depthFrame.Height, 96, 96, PixelFormats.Bgra32, null);
                    }

                    silhouette.WritePixels(new Int32Rect(0, 0, depthFrame.Width, depthFrame.Height), convertedDepthBits, depthFrame.Width * Bgra32BytesPerPixel, 0);

                    //lastImageFormat = depthFrame.Format;
                }
            }
            catch
            {
            }
        }
        */

        #region INotifyPropertyChanged Members
        private void OnPropertyChanged<T>(Expression<Func<T>> expression)
        {
            if (PropertyChanged == null)
            {
                return;
            }

            var body = (MemberExpression)expression.Body;
            string propertyName = body.Member.Name;
            var args = new PropertyChangedEventArgs(propertyName);
            this.PropertyChanged(this, args);
        }
        #endregion

        /// <summary>
        /// Class which stores a Skeleton ID with a Last Position for maintaining Sticky Skeletons.
        /// </summary>
        private class StickySkelInfo
        {
            /// <summary>
            /// Skeleton ID for the Sticky Skeleton.
            /// </summary>
            public ulong? SkelID
            {
                get { return this.skelID; }
                set { this.skelID = value; this.ResetTTL(); }
            }
            /// <summary>
            /// Last Known Skeleton.Position of the Sticky Skeleton
            /// </summary>
            public CameraSpacePoint LastPosition
            {
                get { return this.lastPosition; }
                set { this.lastPosition = value; this.ResetTTL(); }
            }
            /// <summary>
            /// Tells whether or not the StickySkeleton ID is active ( found on last frame, or counting down TTL ).
            /// </summary>
            public bool Active { get { return this.active; } }

            private ulong? skelID;
            private CameraSpacePoint lastPosition;
            private int ttl;
            private bool active;
            private readonly int ttlCap;

            /// <summary>
            /// Constructor to set TTL upon creating a new StickySkelInfo.
            /// </summary>
            /// <param name="TTL"></param>
            public StickySkelInfo(int TTL = 90)
            {
                this.skelID = null;
                this.lastPosition = new CameraSpacePoint();
                this.ttlCap = TTL;
                this.ResetTTL();
/*                System.Diagnostics.Debug.Print("StickySkelInfo Constructor Called");*/
            }

            /// <summary>
            /// Alert the StickySkelInfo that the stored sticky skeleton wasn't found.
            /// </summary>
            public void NotFound()
            {
/*                System.Diagnostics.Debug.Print("NotFound Called: ID {0} TTL {1}  LastPosition {2} {3} {4}", this.skelID, this.ttl,
                    this.lastPosition.X, this.lastPosition.Y, this.lastPosition.Z);*/

                this.ttl--;
                if(this.active)
                    this.active = false;

                if (this.ttl <= 0)
                {
                    this.Clear();
                }
            }
            /// <summary>
            /// Clear all of the StickySkelInfo fields to default values.
            /// </summary>
            public void Clear()
            {
                this.skelID = null;
                this.lastPosition = new CameraSpacePoint();
                this.ResetTTL();
            }
            /// <summary>
            /// Reset the TimeToLive and make active if wasn't active.
            /// </summary>
            private void ResetTTL()
            {
                this.ttl = this.ttlCap;
                if(!this.active)
                    this.active = true;
            }
        }
    }
}