﻿//------------------------------------------------------------------------------
// <copyright file="KinectSpeechCommander.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace AIMS.Speech
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Resources;
    using System.Windows.Threading;
    using Microsoft.Kinect;
    using Microsoft.Speech.AudioFormat;
    using Microsoft.Speech.Recognition;
    using Microsoft.Speech.Synthesis;
    using AIMS.Utilities;

    public class SpeechCommanderEventArgs : EventArgs
    {
        public string Command { get; internal set; }

        public string Words { get; internal set; }

        public double ConfidenceLevel { get; internal set; }

        public bool ActiveModeRecognition { get; internal set; }
    }

    public class ActiveListeningModeChangedEventArgs : EventArgs
    {
        public bool ActiveListeningModeEnabled { get; internal set; }
    }

    /// <summary>
    /// This control is used to return speech recognition events
    /// It supports Active Listening mode; using a keyword to enable the recognition of specific commands
    /// Default Listening will simply recognize the text spoken by the user and can be prone to false activation.
    /// Use in conjunction with 2 grammars if you want both modes: one for the Defaul Listening and one for the Active Listening mode.
    /// </summary>
    public class KinectSpeechCommander : Control, IDisposable
    {

        #region binding properties

        /// <summary>
        /// Timeout to go back from Active Listening to Default Listening, in seconds.
        /// </summary>
        public static readonly DependencyProperty ActiveListeningTimeoutProperty =
            DependencyProperty.Register("ActiveListeningTimeout", typeof(int), typeof(KinectSpeechCommander), new FrameworkPropertyMetadata(20, FrameworkPropertyMetadataOptions.None));

        /// <summary>
        /// Path to AIMS Default Listening Grammar.
        /// </summary>
        public static readonly DependencyProperty AIMSDefaultListeningGrammarUriProperty =
            DependencyProperty.Register("AimsDefaultListeningGrammarUri", typeof(Uri), typeof(KinectSpeechCommander), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.None));

        /// <summary>
        /// Name of rule that defines AIMS Default Listening Grammar. May be <code>null</code>.
        /// </summary>
        public static readonly DependencyProperty AIMSDefaultListeningGrammarRuleProperty =
            DependencyProperty.Register("AimsDefaultListeningGrammarRule", typeof(string), typeof(KinectSpeechCommander), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.None));

        /// <summary>
        /// Path to AIMS Active Listening Grammar.
        /// </summary>
        public static readonly DependencyProperty AIMSActiveListeningGrammarUriProperty =
            DependencyProperty.Register("AimsActiveListeningGrammarUri", typeof(Uri), typeof(KinectSpeechCommander), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.None));

        /// <summary>
        /// Name of rule that defines Module Active Listening Grammar. May be <code>null</code>.
        /// </summary>
        public static readonly DependencyProperty AIMSActiveListeningGrammarRuleProperty =
            DependencyProperty.Register("AimsActiveListeningGrammarRule", typeof(string), typeof(KinectSpeechCommander), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.None));

        /// <summary>
        /// Path to Module Default Listening Grammar.
        /// </summary>
        public static readonly DependencyProperty ModuleDefaultListeningGrammarUriProperty =
            DependencyProperty.Register("ModuleDefaultListeningGrammarUri", typeof(Uri), typeof(KinectSpeechCommander), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.None));

        /// <summary>
        /// Name of rule that defines Module Default Listening Grammar. May be <code>null</code>.
        /// </summary>
        public static readonly DependencyProperty ModuleDefaultListeningGrammarRuleProperty =
            DependencyProperty.Register("ModuleDefaultListeningGrammarRule", typeof(string), typeof(KinectSpeechCommander), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.None));

        /// <summary>
        /// Path to Module Active Listening Grammar.
        /// </summary>
        public static readonly DependencyProperty ModuleActiveListeningGrammarUriProperty =
            DependencyProperty.Register("ModuleActiveListeningGrammarUri", typeof(Uri), typeof(KinectSpeechCommander), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.None));

        /// <summary>
        /// Name of rule that defines Module Active Listening Grammar. May be <code>null</code>.
        /// </summary>
        public static readonly DependencyProperty ModuleActiveListeningGrammarRuleProperty =
            DependencyProperty.Register("ModuleActiveListeningGrammarRule", typeof(string), typeof(KinectSpeechCommander), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.None));

        /// <summary>
        /// Default confidence Threshold for recognition.
        /// </summary>
        public static readonly DependencyProperty DefaultConfidenceThresholdProperty =
            DependencyProperty.Register("DefaultConfidenceThreshold", typeof(float), typeof(KinectSpeechCommander), new FrameworkPropertyMetadata(.75f, FrameworkPropertyMetadataOptions.None));

        #endregion

        /// <summary>
        /// <code>true</code> if KinectAudioSource has been started, <code>false</code> otherwise.
        /// </summary>
        private bool started = false;

        /// <summary>
        /// KinectSensor being used.
        /// </summary>
        private KinectSensor kinect = null;

        /// <summary>
        /// <code>true</code> if KinectSpeechCommander is in active listening mode
        /// (i.e.: user has said "kinect" keyword).
        /// <code>false</code> otherwise.
        /// </summary>
        private bool activeListeningMode = false;

        /// <summary>
        /// Stream for 32b-16b conversion.
        /// </summary>
        private KinectAudioStream convertStream = null;

        /// <summary>
        /// Engine used for speech recognition.
        /// </summary>
        private SpeechRecognitionEngine speechRecognitionEngine = null;

        /// <summary>
        /// Timer used to determine if we should get out of active listening mode,
        /// i.e., user hasn't uttered a command since entering active mode.
        /// </summary>
        private DispatcherTimer activeListeningTimer = new DispatcherTimer();

        /// <summary>
        /// Grammar used by AIMS while in active listening mode.
        /// </summary>
        private Grammar aimsActiveListeningGrammar = null;

        /// <summary>
        /// Grammar used by AIMS while in default listening mode.
        /// </summary>
        private Grammar aimsDefaultListeningGrammar = null;

        /// <summary>
        /// Grammar used by module ( Lesson, Results, Dashboards, etc. ) while in active listening mode.
        /// </summary>
        private Grammar moduleActiveListeningGrammar = null;

        /// <summary>
        /// Grammar used by module ( Lesson, Results, Dashboards, etc. ) while in default listening mode.
        /// </summary>
        private Grammar moduleDefaultListeningGrammar = null;

        /// <summary>
        /// Grammar used by Task Stages. Does not rely on listening modes.
        /// </summary>
        private List<Grammar> StageListeningGrammar = null;

        private SpeechSynthesizer speechSynthesizer = null;

        public SpeechSynthesizer SpeechSynthesizer { get { return speechSynthesizer; } private set { speechSynthesizer = value; } }

        /// <summary>
        /// Item recognized.
        /// </summary>
        public event EventHandler<SpeechCommanderEventArgs> SpeechRecognized;

        /// <summary>
        /// Item not recognized.
        /// </summary>
        public event EventHandler<SpeechCommanderEventArgs> SpeechRejected;

        /// <summary>
        /// Active Listening Mode Changed.
        /// </summary>
        public event EventHandler<ActiveListeningModeChangedEventArgs> ActiveListeningModeChanged;

        /// <summary>
        /// Gets or sets the timeout to go back from Active Listening to Default Listening, in seconds.
        /// </summary>
        public int ActiveListeningTimeout
        {
            get { return (int)GetValue(ActiveListeningTimeoutProperty); }
            set { SetValue(ActiveListeningTimeoutProperty, value); }
        }

        /// <summary>
        /// Gets or sets the path to AIMS Default Listening Grammar
        /// </summary>
        public Uri AimsDefaultListeningGrammarUri
        {
            get { return (Uri)GetValue(AIMSDefaultListeningGrammarUriProperty); }
            set { SetValue(AIMSDefaultListeningGrammarUriProperty, value); }
        }

        /// <summary>
        /// Gets or sets the name of rule that defines AIMS Default Listening Grammar. May be <code>null</code>.
        /// </summary>
        public string AimsDefaultListeningGrammarRule
        {
            get { return (string)GetValue(AIMSDefaultListeningGrammarRuleProperty); }
            set { SetValue(AIMSDefaultListeningGrammarRuleProperty, value); }
        }

        /// <summary>
        /// Gets or sets the path to AIMS Active Listening Grammar.
        /// </summary>
        public Uri AimsActiveListeningGrammarUri
        {
            get { return (Uri)GetValue(AIMSActiveListeningGrammarUriProperty); }
            set { SetValue(AIMSActiveListeningGrammarUriProperty, value); }
        }

        /// <summary>
        /// Gets or sets the name of the rule that defines AIMS Active Listening Grammar. May be <code>null</code>.
        /// </summary>
        public string AimsActiveListeningGrammarRule
        {
            get { return (string)GetValue(AIMSActiveListeningGrammarRuleProperty); }
            set { SetValue(AIMSActiveListeningGrammarRuleProperty, value); }
        }

        /// <summary>
        /// Gets or sets the path to Module Default Listening Grammar
        /// </summary>
        public Uri ModuleDefaultListeningGrammarUri
        {
            get { return (Uri)GetValue(ModuleDefaultListeningGrammarUriProperty); }
            set { SetValue(ModuleDefaultListeningGrammarUriProperty, value); }
        }

        /// <summary>
        /// Gets or sets the name of rule that defines Module Default Listening Grammar. May be <code>null</code>.
        /// </summary>
        public string ModuleDefaultListeningGrammarRule
        {
            get { return (string)GetValue(ModuleDefaultListeningGrammarRuleProperty); }
            set { SetValue(ModuleDefaultListeningGrammarRuleProperty, value); }
        }

        /// <summary>
        /// Gets or sets the path to Module Active Listening Grammar.
        /// </summary>
        public Uri ModuleActiveListeningGrammarUri
        {
            get { return (Uri)GetValue(ModuleActiveListeningGrammarUriProperty); }
            set { SetValue(ModuleActiveListeningGrammarUriProperty, value); }
        }

        /// <summary>
        /// Gets or sets the name of the rule that defines Module Active Listening Grammar. May be <code>null</code>.
        /// </summary>
        public string ModuleActiveListeningGrammarRule
        {
            get { return (string)GetValue(ModuleActiveListeningGrammarRuleProperty); }
            set { SetValue(ModuleActiveListeningGrammarRuleProperty, value); }
        }

        /// <summary>
        /// Gets or sets the default confidence Threshold for recognition
        /// </summary>
        public float DefaultConfidenceThreshold
        {
            get { return (float)GetValue(DefaultConfidenceThresholdProperty); }
            set { SetValue(DefaultConfidenceThresholdProperty, value); }
        }

        /// <summary>
        /// Set the module grammars. ( Lesson, Results, Dashboards, etc. )
        /// </summary>
        /// <param name="moduleGrammarUri">URI of the module's grammar file.</param>
        /// <param name="moduleActiveGrammarRule">Active module grammar rule.</param>
        /// <param name="moduleDefaultGrammarRule">Default module grammar rule.</param>
        public void SetModuleGrammars(Uri moduleGrammarUri, string moduleActiveGrammarRule, string moduleDefaultGrammarRule)
        {
            try
            {
                if (this.speechRecognitionEngine == null)
                {
                    return;
                }

                // Unload the old Grammars from the speech recognition engine.
                if (this.moduleActiveListeningGrammar != null)
                {
                    this.speechRecognitionEngine.UnloadGrammar(this.moduleActiveListeningGrammar);
                }
                if (this.moduleDefaultListeningGrammar != null)
                {
                    this.speechRecognitionEngine.UnloadGrammar(this.moduleDefaultListeningGrammar);
                }

                // Create the new Grammars from parameters.
                this.moduleActiveListeningGrammar = CreateGrammar(moduleGrammarUri, moduleActiveGrammarRule);
                this.moduleDefaultListeningGrammar = CreateGrammar(moduleGrammarUri, moduleDefaultGrammarRule);

                // Load the new Grammars in the speech recognition engine
                this.speechRecognitionEngine.LoadGrammar(this.moduleActiveListeningGrammar);
                this.speechRecognitionEngine.LoadGrammar(this.moduleDefaultListeningGrammar);

                this.EnableGrammars();
            }
            catch (Exception exc)
            {
                Debug.Print("KinectSpeechCommander.SetModuleGrammar ERROR: {0}", exc.Message);
                this.moduleActiveListeningGrammar = null;
                this.moduleDefaultListeningGrammar = null;
            }
        }

        /// <summary>
        /// Remove module Grammars from Kinect Speech Commander.
        /// </summary>
        public void ClearModuleGrammars()
        {
            try
            {
                if (this.moduleActiveListeningGrammar != null)
                {
                    this.speechRecognitionEngine.UnloadGrammar(this.moduleActiveListeningGrammar);
                    this.moduleActiveListeningGrammar = null;
                }

                if (this.moduleDefaultListeningGrammar != null)
                {
                    this.speechRecognitionEngine.UnloadGrammar(this.moduleDefaultListeningGrammar);
                    this.moduleDefaultListeningGrammar = null;
                }
            }
            catch (Exception exc)
            {
                Debug.Print("KinectSpeechCommander.ClearModuleGrammar() ERROR: {0}", exc.Message);
            }
        }

        /// <summary>
        /// Add a stage grammar to the collection of KinectSpeechCommander stage grammars.
        /// </summary>
        /// <param name="stageGrammarUri">URI of the stage's grammar file.</param>
        /// <param name="stageGrammarRule">Grammar rule used with stage.</param>
        public void AddStageGrammar(Uri stageGrammarUri, string stageGrammarRule)
        {
            try
            {
                // Add the stage grammar to the list of stage grammars and enable it.
                this.StageListeningGrammar.Add(CreateGrammar(stageGrammarUri, stageGrammarRule));
                this.speechRecognitionEngine.LoadGrammar(this.StageListeningGrammar[this.StageListeningGrammar.Count - 1]);
                this.StageListeningGrammar[this.StageListeningGrammar.Count - 1].Enabled = true;
            }
            catch (Exception exc)
            {
                Debug.Print("KinectSpeechCommander.AddStageGrammar: " + exc.Message);
            }
        }

        /// <summary>
        /// Remove a stage grammar from the Kinect Speech Commander at a specified index. ( Indices for stage grammars grow as stage grammars are added, 
        /// therefore the index for a stage grammar depends on the order it was added.
        /// </summary>
        /// <param name="index">Index used to identify stage grammar to remove.</param>
        public void RemoveStageGrammar(int index)
        {
            try
            {
                this.speechRecognitionEngine.UnloadGrammar(this.StageListeningGrammar[index]);
                this.StageListeningGrammar.RemoveAt(index);
            }
            catch (ArgumentOutOfRangeException aoore)
            {
                Debug.Print("KinectSpeechCommander.RemoveStageGrammar() ~ Supplied index caused an ArgumentOutOfRange Exception. Index: {0} ERROR Message: {1}", index, aoore.Message);
            }
        }
        
        /// <summary>
        /// Remove all stage grammars from the Kinect Speech Commander.
        /// </summary>
        public void ClearStageGrammars()
        {
            try
            {
                // Unload stage Grammars from speech recognition engine.
                foreach (Grammar gram in this.StageListeningGrammar)
                    this.speechRecognitionEngine.UnloadGrammar(gram);

                // Remove stage Grammars from stage grammar collection.
                this.StageListeningGrammar.RemoveRange(0, this.StageListeningGrammar.Count);
            }
            catch (Exception exc)
            {
                Debug.Print("KinectSpeechCommander.ClearStageGrammars() ERROR: {0}", exc.Message);
            }
        }

        /// <summary>
        /// Start loads the grammars, connects to speech engine to the Kinect audio stream
        /// and starts asynchronous speech recognition.
        /// </summary>
        /// <param name="sensor">
        /// KinectSensor that will provide audio stream for which speech will be recognized.
        /// </param>
        public void Start(KinectSensor sensor)
        {
            Debug.Print("KinectSpeechCommander-> Start called...");

            if (this.started)
            {
                Debug.Print("KinectSpeechCommander-> Already started.");
                return;
            }

            try
            {
                if (this.convertStream == null)
                {
                    Debug.Print("KinectSpeechCommander-> getting the kinect audio streaming...");

                    // get the audio stream
                    IReadOnlyList<AudioBeam> audioBeamList = sensor.AudioSource.AudioBeams;
                    System.IO.Stream audioStream = audioBeamList[0].OpenInputStream();

                    // create the convert stream for the recognizer
                    this.convertStream = new KinectAudioStream(audioStream);
                }

                // Load AIMS Grammars from files
                this.aimsDefaultListeningGrammar = CreateGrammar(this.AimsDefaultListeningGrammarUri, this.AimsDefaultListeningGrammarRule);
                this.aimsActiveListeningGrammar = CreateGrammar(this.AimsActiveListeningGrammarUri, this.AimsActiveListeningGrammarRule);

                // If we couldn't find either of the grammars, we can't do any work, so we bail out
                if (this.aimsDefaultListeningGrammar == null || this.aimsActiveListeningGrammar == null)
                {
                    return;
                }

                this.kinect = sensor;
                this.activeListeningTimer = new DispatcherTimer();

                RecognizerInfo recognizerInfo = GetKinectRecognizer();
                this.speechRecognitionEngine = new SpeechRecognitionEngine(recognizerInfo.Id);

                // Load the Grammars in the speech recognition engine
                this.speechRecognitionEngine.LoadGrammar(this.aimsDefaultListeningGrammar);
                this.speechRecognitionEngine.LoadGrammar(this.aimsActiveListeningGrammar);

                this.EnableGrammars();

                // Hook - up speech recognition events.
                this.speechRecognitionEngine.SpeechRecognized += this.Sre_SpeechRecognized;
                this.speechRecognitionEngine.SpeechRecognitionRejected += this.Sre_SpeechRejected;

                // Diagnostic event to tune up the kinect mic volume
                this.speechRecognitionEngine.AudioSignalProblemOccurred += this.Sre_AudioSignalProblemOccurred;

                // Let the convertStream know speech is going active
                this.convertStream.SpeechActive = true;

                // For long recognition sessions (a few hours or more), it may be beneficial to turn off adaptation of the acoustic model. 
                // This will prevent recognition accuracy from degrading over time.
                ////this.speechRecognitionEngine.UpdateRecognizerSetting("AdaptationOn", 0);

                Debug.Print("KinectManager-> Setting the speech input stream...");
                this.speechRecognitionEngine.SetInputToAudioStream(
                    this.convertStream, new SpeechAudioFormatInfo(EncodingFormat.Pcm, 16000, 16, 1, 32000, 2, null));

                System.Diagnostics.Debug.WriteLine("KinectSpeechCommander-> Starting speechRecognitionEngine");
                this.speechRecognitionEngine.RecognizeAsync(RecognizeMode.Multiple);
                System.Diagnostics.Debug.WriteLine("KinectSpeechCommander-> Speech recognition started.");

                this.started = true;

                this.SpeechSynthesizer = new SpeechSynthesizer();
                this.SpeechSynthesizer.SetOutputToDefaultAudioDevice();
                this.SpeechSynthesizer.SelectVoice("Microsoft Server Speech Text to Speech Voice (en-US, ZiraPro)");
                this.SpeechSynthesizer.Volume = 100;
            }
            catch (Exception exception)
            {
                Debug.Print("KinectManager-> Error with kinectSpeechCommander: " + exception.Message);
            }
        }

        /// <summary>
        /// Stops the speech recognition engine and Kinect audio streaming.
        /// </summary>
        public void Stop()
        {
            if (!this.started)
            {
                return;
            }

            // Let the stream know that it is not being used anymore
            if (this.convertStream != null)
            {
                this.convertStream.SpeechActive = false;
            }

            // Cancel the speech recognition
            if (this.speechRecognitionEngine != null)
            {
                this.speechRecognitionEngine.RecognizeAsyncCancel();
                this.speechRecognitionEngine.RecognizeAsyncStop();
            }

            // Remove speech recognition events.
            this.speechRecognitionEngine.SpeechRecognized -= this.Sre_SpeechRecognized;
            this.speechRecognitionEngine.SpeechRecognitionRejected -= this.Sre_SpeechRejected;
            this.speechRecognitionEngine.AudioSignalProblemOccurred -= this.Sre_AudioSignalProblemOccurred;

            this.started = false;
        }

        /// <summary>
        /// Stops speech recognition, if necessary, and disposes all resources used by the
        /// <see cref="KinectSpeechCommander"/>.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Stops speech recognition, if necessary, and disposes all resources used by the
        /// <see cref="KinectSpeechCommander"/>.
        /// </summary>
        /// <param name="disposing">
        /// Specify true to indicate that the class should clean up all resources,
        /// including native resources.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.speechRecognitionEngine != null)
                {
                    this.speechRecognitionEngine.Dispose();
                }

                if (this.SpeechSynthesizer != null)
                {
                    this.SpeechSynthesizer.Dispose();
                    this.SpeechSynthesizer = null;
                }
            }
        }

        /// <summary>
        /// Gets a speech recognizer appropriate for Kinect audio stream.
        /// </summary>
        /// <returns>
        /// <see cref="RecognizerInfo"/> that describes an appropriate speech recognizer.
        /// <code>null</code> if no such recognizer was found.
        /// </returns>
        private static RecognizerInfo GetKinectRecognizer()
        {
            // This is required to catch the case when an expected recognizer is not installed.
            // By default - the x86 Speech Runtime is always expected. 
            Debug.Print("KinectManager-> Searching for KinectRecognizer...");
            try
            {
                Func<RecognizerInfo, bool> matchingFunc = r =>
                {
                    string value;
                    r.AdditionalInfo.TryGetValue("Kinect", out value);
                    return "True".Equals(value, StringComparison.OrdinalIgnoreCase) && "en-US".Equals(r.Culture.Name, StringComparison.OrdinalIgnoreCase);
                };
                return SpeechRecognitionEngine.InstalledRecognizers().Where(matchingFunc).FirstOrDefault();
            }
            catch (COMException exception)
            {
                Debug.Print("KinectManager-> Error with GetKinectRecognizer: " + exception.Message);
                return null;
            }
        }

        /// <summary>
        /// Create a speech recognition grammar from the specified grammar path
        /// and rule name.
        /// </summary>
        /// <param name="grammarUri">
        /// Path to grammar specification to use as source for created grammar.
        /// </param>
        /// <param name="grammarRule">
        /// Name of rule within specification that defines grammar to use. May be <code>null</code>.
        /// </param>
        /// <returns>
        /// A grammar ready to be loaded into a speech recognition engine.
        /// <code>null</code> if grammar specification was not found.
        /// </returns>
        /// <remarks>
        /// This method first attempts to find a grammar file that matches the
        /// specified Uri, and if that fails then attempts to find a resource
        /// that matches the Uri.
        /// </remarks>
        private static Grammar CreateGrammar(Uri grammarUri, string grammarRule)
        {
            if (File.Exists(grammarUri.ToString()))
            {
                // If we're dealing with a file name, create grammar from filename
                return new Grammar(grammarUri.ToString(), grammarRule);
            }
            else
            {
                StreamResourceInfo info = Application.GetResourceStream(grammarUri);
                if (info != null)
                {
                    // If we're dealing with a resource, create grammar from stream
                    return new Grammar(info.Stream, grammarRule);
                }
            }

            return null;
        }

        /// <summary>
        /// Helper to handle event dispatch for rejected speech utterances.
        /// </summary>
        /// <param name="command">
        /// Text of speech command that was rejected. May be <code>null</code>.
        /// </param>
        /// <param name="confidenceLevel">
        /// Value (from 0.0 to 1.0) assigned by the recognizer that represents the likelihood that a
        /// recognized phrase matches a given input.
        /// </param>
        /// <param name="activeModeRecognition">
        /// <code>true</code> if speech commander was in active listening mode when this event came in.
        /// <code>false</code> otherwise.
        /// </param>
        private void OnSpeechRejected(string command, double confidenceLevel, bool activeModeRecognition)
        {
            if (this.SpeechRejected != null)
            {
                SpeechCommanderEventArgs e = new SpeechCommanderEventArgs();
                e.Command = command;
                e.ConfidenceLevel = confidenceLevel;
                e.ActiveModeRecognition = activeModeRecognition;

                this.SpeechRejected(this, e);
            }
        }

        /// <summary>
        /// Helper to handle event dispatch for recognized speech utterances.
        /// </summary>
        /// <param name="command">
        /// Text of semantic speech command that was recognized.
        /// </param>
        /// <param name="words">
        /// Text of speech utterance that was recognized.
        /// </param>
        /// <param name="confidenceLevel">
        /// Value (from 0.0 to 1.0) assigned by the recognizer that represents the likelihood that a
        /// recognized phrase matches a given input.
        /// </param>
        /// <param name="activeModeRecognition">
        /// <code>true</code> if speech commander was in active listening mode when this event came in.
        /// <code>false</code> otherwise.
        /// </param>
        private void OnSpeechRecognized(string command, string words, double confidenceLevel, bool activeModeRecognition)
        {
            SpeechCommanderEventArgs e = new SpeechCommanderEventArgs();
            e.Command = command;
            e.Words = words;
            e.ConfidenceLevel = confidenceLevel;
            e.ActiveModeRecognition = activeModeRecognition;

            if (this.SpeechRecognized != null)
            {
                this.SpeechRecognized(this, e);
            }
        }

        /// <summary>
        /// Helper to handle event dispatch when active listening mode has changed.
        /// </summary>
        private void OnActiveListeningModeChanged()
        {
            if (this.ActiveListeningModeChanged != null)
            {
                ActiveListeningModeChangedEventArgs e = new ActiveListeningModeChangedEventArgs();
                e.ActiveListeningModeEnabled = this.activeListeningMode;

                this.ActiveListeningModeChanged(this, e);
            }
        }

        /// <summary>
        /// Handler for audio signal problem events.
        /// </summary>
        /// <param name="sender">
        /// Object that triggered the event.
        /// </param>
        /// <param name="e">
        /// Event arguments.
        /// </param>
        private void Sre_AudioSignalProblemOccurred(object sender, AudioSignalProblemOccurredEventArgs e)
        {
            // Debug.Print("Audio signal problem occurred: {0}", e.AudioSignalProblem.ToString());
        }

        /// <summary>
        /// Handler for speech recognized events.
        /// </summary>
        /// <param name="sender">
        /// Object that triggered the event.
        /// </param>
        /// <param name="e">
        /// Event arguments.
        /// </param>
        private void Sre_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            if (e.Result.Confidence > this.DefaultConfidenceThreshold)
            {
                Debug.Print("Speech Recognized - Tag: {0} Words: {1} Confidence: {2}", e.Result.Semantics.Value.ToString(), e.Result.Text, e.Result.Confidence);
                this.OnSpeechRecognized(e.Result.Semantics.Value.ToString(), e.Result.Text, e.Result.Confidence, this.activeListeningMode);

                // If the keyword is used, switch to Active Listening Mode
                if (e.Result.Semantics.Value.ToString() == "KEYWORD")
                {
                    this.SetActiveListeningModeOn();
                }
                else
                {
                    this.SetActiveListeningModeOff();
                }
            }
            else
            {
                Debug.Print("Speech Rejected by confidence - Words: {0} Confidence: {1}", e.Result.Text, e.Result.Confidence);
                this.OnSpeechRejected(e.Result.Text, e.Result.Confidence, this.activeListeningMode);
            }
        }

        /// <summary>
        /// Handler for speech rejected events.
        /// </summary>
        /// <param name="sender">
        /// Object that triggered the event.
        /// </param>
        /// <param name="e">
        /// Event arguments.
        /// </param>
        private void Sre_SpeechRejected(object sender, SpeechRecognitionRejectedEventArgs e)
        {
            Debug.Print("Speech Rejected by engine: {0} confidence: {1}", e.Result.Text, e.Result.Confidence);
            this.OnSpeechRejected(e.Result.Text, e.Result.Confidence, this.activeListeningMode);
        }

        /// <summary>
        /// Turn active Listening Mode On.
        /// </summary>
        private void SetActiveListeningModeOn()
        {
            if (this.activeListeningMode)
            {
                return;
            }

            this.activeListeningMode = true;
            this.OnActiveListeningModeChanged();
            this.EnableGrammars();
            this.StartActiveListeningTimer();
        }

        /// <summary>
        /// Turn active Listening Mode Off.
        /// </summary>
        private void SetActiveListeningModeOff()
        {
            if (!this.activeListeningMode)
            {
                return;
            }

            this.activeListeningMode = false;

            // Stop the timer, it's no longer needed.
            this.activeListeningTimer.Stop();

            this.OnActiveListeningModeChanged();
            this.EnableGrammars();
        }

        /// <summary>
        /// Start the active listening timer, to determine if mode should be automatically
        /// turned off.
        /// </summary>
        private void StartActiveListeningTimer()
        {
            this.activeListeningTimer.Stop();
            this.activeListeningTimer.Tick += this.ActiveListeningTimerTick;
            this.activeListeningTimer.Interval = new TimeSpan(0, 0, this.ActiveListeningTimeout);
            this.activeListeningTimer.Start();
        }

        /// <summary>
        /// Handler for active listening timer tick event.
        /// </summary>
        /// <param name="sender">
        /// Object that triggered the event.
        /// </param>
        /// <param name="e">
        /// Event arguments.
        /// </param>
        private void ActiveListeningTimerTick(object sender, EventArgs e)
        {
            this.SetActiveListeningModeOff();
        }

        /// <summary>
        /// Enable Grammars based on the current listening Mode (active vs default).
        /// </summary>
        private void EnableGrammars()
        {
            if (this.aimsDefaultListeningGrammar != null)
                this.aimsDefaultListeningGrammar.Enabled = !this.activeListeningMode;

            if (this.moduleDefaultListeningGrammar != null)
                this.moduleDefaultListeningGrammar.Enabled = !this.activeListeningMode;

            if (this.aimsActiveListeningGrammar != null)
                this.aimsActiveListeningGrammar.Enabled = this.activeListeningMode;

            if (this.moduleActiveListeningGrammar != null)
                this.moduleActiveListeningGrammar.Enabled = this.activeListeningMode;
        }
    }
}
