﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.ComponentModel;
using Microsoft.Kinect;

namespace AIMS.Utilities.Kinect
{
    /// <summary>
    /// Skeletons Updated Event Handler. Delegate for the SkeletonsUpdated event.
    /// </summary>
    /// <param name="sender">Reference to object which threw event.</param>
    public delegate void SkeletonsUpdatedEventHandler(object sender);
    /// <summary>
    /// NumStickySkeletons Updated Event Handler. Delegate for the NumStickySkeletonsUpdated event.
    /// </summary>
    /// <param name="sender">Reference to object which threw event.</param>
    /// <param name="numStickySkeletons">New value for NumStickySkeletons.</param>
    public delegate void NumStickySkeletonsUpdatedEventHandler(object sender, int numStickySkeletons);

    public class SkeletonManager : IDisposable
    {
        #region Public Delegates, Events

        /// <summary>
        /// Delegate to be able to assign StickyGesture. Allows overwriting gesture used to identify a sticky skeleton.
        /// </summary>
        /// <param name="skel">Skeleton used for identifying a correct gesture.</param>
        /// <returns>True if gesture is a sticky gesture, false if not a sticky gesture.</returns>
        public delegate bool StickyGestureMethod(Body Skel);

        /// <summary>
        /// SkeletonManager event which fires whenever the skeletons have been updated.
        /// </summary>
        public event SkeletonsUpdatedEventHandler SkeletonsUpdated;

        /// <summary>
        /// SkeletonManager event which fires whenever the NumStickySkeletons property has been updated.
        /// </summary>
        public event NumStickySkeletonsUpdatedEventHandler NumStickySkeletonsUpdated;

        #endregion

        #region Public Properties, Constants

        /// <summary>
        /// Maximum number of trackable skeletons. ('skeleton'.TrackingState == SkeletonTrackingState.Tracked)
        /// </summary>
        public const int MaxTrackableSkeletons = 2;

        /// <summary>
        /// Returns the closest skeleton to the Kinect based on Position Depth and if the skeleton tracking state is Tracked.
        /// </summary>
        /// <exception cref="System.ObjectDisposedException">Thrown when SkeletonManager has already been disposed.</exception>
        public Body ClosestSkeleton { get { this.CheckDisposed(); return this.closestSkeleton; } }

        /// <summary>
        /// Returns the Sticky Skeleton if a single skeleton is stuck. Otherwise returns null.
        /// </summary>
        /// <exception cref="System.ObjectDisposedException">Thrown when SkeletonManager has already been disposed.</exception>
        public Body StickySkeleton { get { this.CheckDisposed(); return this.stickySkeleton; } }

        /// <summary>
        /// Set the StickyGestureMethod for identifying sticky skeletons.
        /// </summary>
        /// <exception cref="System.ObjectDisposedException">Thrown when SkeletonManager has already been disposed.</exception>
        public StickyGestureMethod StickyGesture { set { this.CheckDisposed(); this.stickyGesture = value; } }

        /// <summary>
        /// Returns a clone of the raw skeleton array.
        /// </summary>
        /// <exception cref="System.ObjectDisposedException">Thrown when SkeletonManager has already been disposed.</exception>
        public Body[] Skeletons { get { this.CheckDisposed(); return this.skeletonData != null ? this.skeletonData.Clone() as Body[] : new Body[0]; } }
        /// <summary>
        /// Returns a clone of the raw sticky skeleton array.
        /// </summary>
        /// <exception cref="System.ObjectDisposedException">Thrown when SkeletonManager has already been disposed.</exception>
        public Body[] StickySkeletons { get { this.CheckDisposed(); return this.stickySkeletons != null && this.stickySkeletons.Values != null ? this.stickySkeletons.Values.ToArray().Clone() as Body[] : new Body[0]; } }

        /// <summary>
        /// Value between 0 and SkeletonManager.MaxTrackableSkeletons. If 0, Sticky Skeletons feature is turned off, otherwise the value cooresponds to the number 
        /// of sticky skeletons for the SkeletonManager to aquire/maintain. When NumStickySkeletons value is changed, all sticky skeleton ID's are reset.
        /// </summary>
        /// <exception cref="System.ObjectDisposedException">Thrown when a public property is accessed and the object has already been disposed.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown when NumStickySkeletons is set to a value less than 0 or greater than 
        /// SkeletonManager.MaxTrackableSkeletons</exception>
        public int NumStickySkeletons
        {
            get { this.CheckDisposed(); return numStickySkeletons; }
            set
            {
                this.CheckDisposed();
                if (this.numStickySkeletons == value)
                {
                    ResetStickySkeletonIDs();
                    return;
                }

                if (value <= KinectManager.MaxTrackableSkeletons && value >= 0)
                {
                    numStickySkeletons = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("StickySkeletons",
                        "KinectManager.StickySkeletons value must be between 0 and " + KinectManager.MaxTrackableSkeletons.ToString() + ".");
                }
                ResetStickySkeletonIDs();
                // Throw NumStickySkeletonsUpdated event
                if (NumStickySkeletonsUpdated != null)
                    NumStickySkeletonsUpdated(this, this.numStickySkeletons);
                // Notify NumStickySkels has changed
            }
        }

        #endregion

        #region Private Fields / Members

        // Latest collection of Skeletons from the SkeletonDataReady event.
        private Body[] skeletonData;

        // Closest skeleton to the kinect based on Depth of Position and Tracked. Set every frame.
        private Body closestSkeleton;

        // Number of sticky skeletons to look for / retain.
        private int numStickySkeletons = 0;
        // Collection of sticky skeleton information ( necessary for persistent sticky skeletons ).
        private StickySkelInfo[] stickySkels = new StickySkelInfo[SkeletonManager.MaxTrackableSkeletons];
        // A single sticky skeleton set only if numStickySkeletons is 1 and a sticky skeleton exists.
        private Body stickySkeleton;
        // Array of sticky skeletons.
        private Dictionary<ulong, Body> stickySkeletons = new Dictionary<ulong, Body>(SkeletonManager.MaxTrackableSkeletons);
        // Delegate method used to identify a skeleton for sticky-ness.
        private StickyGestureMethod stickyGesture;
        /// Sticky Skeleton Comparison Queue. Used to iterate through tracked skeletons over frames ( limited amount able to compare per frame ).
        private Queue<ulong> stickySkelCompQueue = new Queue<ulong>();

        // State of whether or not SkeletonManager has been disposed.
        private bool disposed;

        #endregion

        #region Constructor / Dispose

        /// <summary>
        /// Constructor for SkeletonManager object. SkelStream parameter allows access to the skeleton stream being used 
        /// and subscribes to the KinectManager.SkeletonDataReady event.
        /// </summary>
        /// <param name="SkelStream">SkeletonStream to use.</param>
        public SkeletonManager()
        {
            KinectManager.BodyFrameReady += KinectManager_SkeletonDataReady;

            this.ResetStickySkeletonIDs();

            // Set the default StickyGesture
            this.stickyGesture = this.DefaultStickyGesture;
        }

        /// <summary>
        /// Dispose function for SkeletonManager. Cleans up resources.
        /// </summary>
        public void Dispose()
        {
            // Call the form of dispose which disposes EVERYTHING ( managed and unmanaged ).
            this.Dispose(true);

            // Supress Finalizer in-case sub-classes use finalizers.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Internal Dispose function for SkeletonManager object.
        /// </summary>
        protected virtual void Dispose(bool Disposing)
        {
            if (!this.disposed)
            {
                // Handle Managed Resources.
                if (Disposing)
                {
                    // Unsubscribe from KinectManager SkeletonDataReady event.
                    KinectManager.BodyFrameReady -= KinectManager_SkeletonDataReady;
                }
                // Would Handle Unmanaged Resources Here.
                //...

                this.disposed = true;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Reset the sticky gesture for identifying sticky skeletons to the default gesture.
        /// </summary>
        public void UseDefaultStickyGesture()
        {
            if (this.stickyGesture != this.DefaultStickyGesture)
                this.stickyGesture = this.DefaultStickyGesture;
        }

        /// <summary>
        /// Check whether or not the object has been disposed. True if disposed, False if not.
        /// </summary>
        /// <returns></returns>
        public bool IsDisposed()
        {
            return this.disposed;
        }

        #endregion

        #region Private Functions

        /// <summary>
        /// Set all StickySkeletonIDs to null.
        /// </summary>
        private void ResetStickySkeletonIDs()
        {
            for (int i = 0; i < this.stickySkels.Length; i++)
                this.stickySkels[i] = new StickySkelInfo();
        }

        /// <summary>
        /// Set Sticky Skeletons, up to SkeletonManager.StickySkeletons. Sticky Skeleton ID's will be put in the SkeletonManager.StickySkeletonIDs 
        /// nullable integer array.
        /// </summary>
        /// <param name="skeletons">Collection of Kinect Skeletons to use, from the SkeletonFrame.</param>
        private void SetStickySkeletons(Body[] skeletons, BodyFrame bodyFrame)
        {
            // Only run if Sticky Skeletons is turned on.
            if (this.numStickySkeletons == 0)
                return;

            if (skeletons != null && skeletons.Length > 0)
            {
                // Check if previous sticky skeletons are still being tracked or not. Clean sticky skel ID's not being used.
                bool unusedStickySkels = true;
                int trackedStickySkels = 0;
                for (int j = 0; j < this.numStickySkeletons; j++)
                {
                    unusedStickySkels = true;
                    // Iterate over skeletons
                    foreach (Body skel in skeletons)
                    {
                        // If sticky skeleton still exists in the scnee.
                        if (skel.IsTracked && skel.TrackingId == this.stickySkels[j].SkelID)
                        {
                            trackedStickySkels++;
                            unusedStickySkels = false;
                            break;
                        }
                    }

                    // If space for a skeleton to become sticky, check for stance.
                    if (trackedStickySkels <= j)
                        unusedStickySkels = true;
                }

                // Check skeleton Stances to find sticky skeletons
                if (unusedStickySkels)
                {
                    // Iterate over skeletons
                    foreach (Body skel in skeletons)
                    {
                        bool checkSkel = true;

                        // Skip checking the stance of already stuck skeletons.
                        for (int i = 0; i < this.numStickySkeletons; i++)
                        {
                            if (this.stickySkels[i].SkelID == skel.TrackingId)
                                checkSkel = false;
                        }

                        // If the skeleton is Tracked or PositionOnly and isn't a sticky skeleton.
                        if (checkSkel && skel.IsTracked)
                        {
                            // Position only skeletons go in the queue to be checked in later frames.
                            if (!skel.IsTracked)
                            {
                                if (!this.stickySkelCompQueue.Contains(skel.TrackingId))
                                    this.stickySkelCompQueue.Enqueue(skel.TrackingId);
                            }
                            else // Check tracked skeletons for stance or sticky persistence. If stance found or persistent make sticky, otherwise put on back of queue.
                            {
                                bool enqueue = true;
                                // Check for gesture and put in variable so not running the function more than required.
                                bool gestureFound = this.stickyGesture(skel);

                                for (int k = 0; k < numStickySkeletons; k++)
                                {
                                    // If the Sticky Skeleton ID is null and the gesture is found, then add the Tracking ID as a Sticky Skeleton ID 
                                    // and exit stickySkeleton loop.
                                    // Or if the Sticky Skeleton is not active ( but still living ) check its position vs other skeleton to re-stick.
                                    // this allows sticky skeleton persistence.
                                    if ((gestureFound && this.stickySkels[k].SkelID == null) ||
                                        ((!this.stickySkels[k].Active && this.stickySkels[k].SkelID.HasValue) &&
                                        this.ComparePositions(skel.Joints[JointType.SpineMid].Position, this.stickySkels[k].LastPosition)))
                                    {
                                        this.stickySkels[k].SkelID = skel.TrackingId;
                                        enqueue = false;

                                        break;
                                    }
                                }
                                // Put tracked skeleton on back of the qeueue.
                                if (!this.stickySkelCompQueue.Contains(skel.TrackingId) && enqueue)
                                    this.stickySkelCompQueue.Enqueue(skel.TrackingId);
                            }
                        }
                    }
                    // Choose skeletons to track on next frame. ( Sticky and non-Sticky from queue with Sticky being priority )
                    this.ChooseSkeletonsToTrack(bodyFrame);
                }
            }
        }

        /// <summary>
        /// Set which skeletons to Track in the skeleton stream.
        /// </summary>
        private void ChooseSkeletonsToTrack(BodyFrame bodyFrame)
        {
            // Get a collection of tracked sticky skeleton IDs.
            Queue<ulong> trackedStickySkelIDs = new Queue<ulong>();

            for (int i = 0; i < this.numStickySkeletons; i++)
            {
                if (this.stickySkels[i].Active && this.stickySkels[i].SkelID.HasValue)
                    trackedStickySkelIDs.Enqueue(this.stickySkels[i].SkelID.Value);
            }

            // Firstly track sticky skeletons, secondly track skeletons off of the sticky skeleton comparison queue.
            try
            {
                // SkeletonStream.ChooseSkeletons currently only supports choosing 1 or 2 skeletons to track.
                // This section will need to be updated when the Kinect SDK updates to track more than 2 skeletons at once.
// @todo review
/*
                // Track 2 sticky skeletons.
                if (trackedStickySkelIDs.Count > 1)
                    this.skelStream.ChooseSkeletons(trackedStickySkelIDs.Dequeue(), trackedStickySkelIDs.Dequeue());

                // Overrides the default behavior of tracking the hands of the nearest bodies to track the hands of the specified body. 
                //bodyFrame.BodyFrameSource.OverrideHandTracking(body.TrackingId);

                // Track both sticky and non-sticky skeletons.
                else if (trackedStickySkelIDs.Count == 1 && this.stickySkelCompQueue.Count > 0)
                    this.skelStream.ChooseSkeletons(trackedStickySkelIDs.Dequeue(), this.stickySkelCompQueue.Dequeue());

                // Track only 1 sticky skeleton.
                else if (trackedStickySkelIDs.Count == 1 && this.stickySkelCompQueue.Count == 0)
                    this.skelStream.ChooseSkeletons(trackedStickySkelIDs.Dequeue());

                // Track 2 non-sticky skeletons.
                else if (this.stickySkelCompQueue.Count > 1)
                    this.skelStream.ChooseSkeletons(this.stickySkelCompQueue.Dequeue(), this.stickySkelCompQueue.Dequeue());

                // Track 1 non-sticky skeleton.
                else if (this.stickySkelCompQueue.Count > 0)
                    this.skelStream.ChooseSkeletons(this.stickySkelCompQueue.Dequeue());
*/
            }
            catch (InvalidOperationException ioe)
            {
                // Queue out of elements...
                System.Diagnostics.Debug.WriteLine("SkeletonManager ChooseSkeletonsToTrack() ERROR: " + ioe.ToString());
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.WriteLine("SkeletonManager ChooseSkeletonsToTrack() ERROR: " + exc.Message);
            }
        }

        /// <summary>
        /// Compares skeleton positions to see if they are nearly identical. ( Identify whether they occupy the same space )
        /// </summary>
        /// <param name="Position1">First skeleton position to compare with.</param>
        /// <param name="Position2">Second skeleton position to compare with.</param>
        /// <returns></returns>
        private bool ComparePositions(CameraSpacePoint Position1, CameraSpacePoint Position2)
        {
            /*System.Diagnostics.Debug.Print("SKELETON POSITIONS BEING COMPARED! Pos1: {0} {1} {2}  Pos2: {3} {4} {5}",
                Position1.X, Position1.Y, Position1.Z, Position2.X, Position2.Y, Position2.Z);*/

            // 15 cm is ~ 1/2 foot.
            double xThresh = 0.15;
            double yThresh = 0.15;
            double zThresh = 0.15;

            if (System.Math.Abs(Position1.X - Position2.X) <= xThresh &&
                System.Math.Abs(Position1.Y - Position2.Y) <= yThresh &&
                System.Math.Abs(Position1.Z - Position2.Z) <= zThresh)
            {
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Default gesture for identifying a sticky skeleton.
        /// </summary>
        /// <param name="Skel">Skeleton being analyzed for a sticky gesture.</param>
        /// <returns>True if using the sticky gesture, False if not.</returns>
        private bool DefaultStickyGesture(Body Skel)
        {
            // Either the right hand or the left hand is above the head. Not both.
            if ((Skel.Joints[JointType.HandRight].Position.Y > Skel.Joints[JointType.Head].Position.Y
                && Skel.Joints[JointType.HandLeft].Position.Y < Skel.Joints[JointType.Head].Position.Y)
                || (Skel.Joints[JointType.HandLeft].Position.Y > Skel.Joints[JointType.Head].Position.Y
                && Skel.Joints[JointType.HandRight].Position.Y < Skel.Joints[JointType.Head].Position.Y))
                return true;

            else
                return false;
        }

        /// <summary>
        /// Should be on each public property. Throws an exception that the object has been disposed if disposed.
        /// </summary>
        private void CheckDisposed()
        {
            if (this.disposed)
                throw new ObjectDisposedException("SkeletonManager");
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handles actions associated with the SkeletonFrame.
        /// </summary>
        private void KinectManager_SkeletonDataReady(BodyFrame bodyFrame)
        {
            {

                this.skeletonData = KinectManager.Bodies;

                // Exit if no skeleton frame found
                // Set the closest and sticky skeletons.
                double minZ = Double.MaxValue;
                foreach (Body skel in this.skeletonData)
                {
                    // Closest skeleton.
                    if (skel.IsTracked && skel.Joints[JointType.SpineMid].Position.Z < minZ)
                    {
                        minZ = skel.Joints[JointType.SpineMid].Position.Z;
                        this.closestSkeleton = skel;
                    }

                    // Update the LastPosition in stickySkelIDs
                    foreach (StickySkelInfo skelInfo in this.stickySkels)
                    {
                        if (skelInfo.SkelID == skel.TrackingId)
                            skelInfo.LastPosition = skel.Joints[JointType.SpineMid].Position;
                    }
                }

                // Double check that all current sticky skeletons still exist.
                // If they aren't found, their TTL starts ticking and Active returns false.
                // After TTL is up, they are cleared.
                int numStuck = 0;
                for (int i = 0; i < this.numStickySkeletons; i++)
                {
                    bool stickyFound = false;
                    if (this.stickySkels[i].SkelID.HasValue)
                    {
                        foreach (Body skel in this.skeletonData)
                        {
                            if (this.stickySkels[i].SkelID.Value == skel.TrackingId)
                            {
                                stickyFound = true;
                                numStuck++;

                                // Update stickySkeletons Dictionary and stickySkeleton.
                                if (this.stickySkeletons.ContainsKey(skel.TrackingId))
                                    this.stickySkeletons[skel.TrackingId] = skel;
                                else
                                    this.stickySkeletons.Add(skel.TrackingId, skel);

                                this.stickySkeleton = skel;

                                break;
                            }
                        }

                        if (!stickyFound)
                        {
                            // Update stickySkeletons Dictionary.
                            if (this.stickySkels[i].SkelID.HasValue)
                                this.stickySkeletons.Remove(this.stickySkels[i].SkelID.Value);

                            this.stickySkels[i].NotFound();
                        }
                    }
                }
                if (numStuck != 1)
                    this.stickySkeleton = null;

                // Look for sticky skeletons and set them if found and have space.
                this.SetStickySkeletons(this.skeletonData, bodyFrame);
            }

            // Fire event that skeletons are updated / ready.
            if (this.SkeletonsUpdated != null)
                this.SkeletonsUpdated(this);
        }

        #endregion

        #region Private objects. Classes

        /// <summary>
        /// Stores a Skeleton ID with a Last Position for maintaining Sticky Skeletons.
        /// </summary>
        private class StickySkelInfo
        {
            #region Public Properties

            /// <summary>
            /// Skeleton ID for the Sticky Skeleton.
            /// </summary>
            public ulong? SkelID
            {
                get { return this.skelID; }
                set { this.skelID = value; this.ResetTTL(); }
            }
            /// <summary>
            /// Last Known Skeleton.Position of the Sticky Skeleton
            /// </summary>
            public CameraSpacePoint LastPosition
            {
                get { return this.lastPosition; }
                set { this.lastPosition = value; this.ResetTTL(); }
            }
            /// <summary>
            /// Tells whether or not the StickySkeleton ID is active ( found on last frame, or counting down TTL ).
            /// </summary>
            public bool Active { get { return this.active; } }

            #endregion

            #region Private Fields

            private ulong? skelID;
            private CameraSpacePoint lastPosition;
            private int ttl;
            private bool active;
            private readonly int ttlCap;

            #endregion

            #region Constructor

            /// <summary>
            /// Constructor to set TTL upon creating a new StickySkelInfo.
            /// </summary>
            /// <param name="TTL"></param>
            public StickySkelInfo(int TTL = 90)
            {
                this.skelID = null;
                this.lastPosition = new CameraSpacePoint();
                this.ttlCap = TTL;
                this.ResetTTL();
            }

            #endregion

            #region Public Functions

            /// <summary>
            /// Alert StickySkelInfo that the stored sticky skeleton wasn't found.
            /// </summary>
            public void NotFound()
            {
                // Decrease TimeToLive, set inactive, and blow out data if TTL expires.
                this.ttl--;
                if (this.active)
                    this.active = false;

                if (this.ttl <= 0)
                    this.Clear();
            }
            /// <summary>
            /// Clear all of the StickySkelInfo fields to default values.
            /// </summary>
            public void Clear()
            {
                this.skelID = null;
                this.lastPosition = new CameraSpacePoint();
                this.ResetTTL();
            }

            #endregion

            #region Private Functions

            /// <summary>
            /// Reset the TimeToLive and make active if wasn't active.
            /// </summary>
            private void ResetTTL()
            {
                this.ttl = this.ttlCap;
                if (!this.active)
                    this.active = true;
            }

            #endregion
        }

        #endregion
    }
}
