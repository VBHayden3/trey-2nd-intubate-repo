﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace AIMS.Utilities.Kinect
{
    /// <summary>
    /// Enumerator to clearly identify trends.
    /// </summary>
    public enum Trend
    {
        None,
        Positive,
        Negative
    };

    /// <summary>
    /// Struct to encapsulate Trends in 3 dimensions; X, Y, and Z.
    /// </summary>
    public struct Trend3D
    {
        #region Public Fields

        /// <summary>
        /// Trend in the X dimension.
        /// </summary>
        public Trend X;
        /// <summary>
        /// Trend in the Y dimension.
        /// </summary>
        public Trend Y;
        /// <summary>
        /// Trend in the Z dimension.
        /// </summary>
        public Trend Z;

        #endregion

        #region Constructor

        public Trend3D(Trend X, Trend Y, Trend Z)
        {
            this.X = X;
            this.Y = Y;
            this.Z = Z;
        }

        #endregion

        #region Overriden ToString Function

        public override string ToString()
        {
            string outputString = "";

            try
            {
                outputString = String.Format("(X:{0}, Y:{1}, Z:{2})", this.X, this.Y, this.Z);
            }
            catch (Exception exc)
            {
                Debug.Print("Trend3D ToString() ERROR: {0}", exc.Message);
            }

            return outputString;
        }

        #endregion
    };
}
