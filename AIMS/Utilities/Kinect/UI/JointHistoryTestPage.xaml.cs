﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using AIMS.Assets.Lessons2;
using AIMS.Speech;
using AIMS.Tasks;
using Microsoft.Kinect;
using System.Diagnostics;
using System.ComponentModel;
using System.Timers;

namespace AIMS.Utilities.Kinect
{
    /// <summary>
    /// Interaction logic for TestPage.xaml
    /// </summary>
    public partial class JointHistoryTestPage : Page, INotifyPropertyChanged
    {
        #region Private Fields

        // Bitmap image to display kinect output.
        //private WriteableBitmap bitmap = new WriteableBitmap(width, height, 96, 96, PixelFormats.Bgr32, null);
        private WriteableBitmap bitmap = null;

        // JointHistory class ( Testing THIS! )
        private JointHistory jointHistory = new JointHistory();

        // local field to test changing history size of JointHistory
        private int historySize;

        // Pause State. True: paused False: resumed.
        private bool paused = false;

        #endregion

        #region Public Events / Fields / Properties

        // PropertyChanged event to alert Bindings
        public event PropertyChangedEventHandler PropertyChanged;

        public int HistorySize { get { return this.historySize; } set { this.historySize = value; RaisePropertyChanged("HistorySize"); } }

        #endregion

        public JointHistoryTestPage()
        {
            InitializeComponent();

            this.DataContext = this;

            this.img_kinectDisplay.Source = bitmap;
        }

        #region Private Non-Action Functions

        /// <summary>
        /// Raise Property Changed Event, updates binding.
        /// </summary>
        /// <param name="propertyName">Property to be raised for.</param>
        private void RaisePropertyChanged(string propertyName)
        {
            try
            {
                // take a copy to prevent thread issues
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs(propertyName));
            }
            catch (Exception exc)
            {
                Debug.Print("SkeletonSmoothingControl RaisePropertyChanged ERROR: {0}", exc.Message);
            }
        }

        #endregion

        private void TestPage_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (KinectManager.Kinect != null)
                {
                    // Using 1 sticky skeleton for this test
                    KinectManager.NumStickySkeletons = 1;

                    KinectManager.AllDataReady += KinectManager_AllDataReady;
                }

                this.HistorySize = this.jointHistory.HistoryLength;
            }
            catch { }

            // Load all of the possible joints into the combo-box ( skip trying to figure this out with Binding )
            IEnumerable<string> sortedJoints =
                from joint in Enum.GetNames(typeof(JointType))
                orderby joint ascending
                select joint;
            
            foreach (string jointType in sortedJoints)
            {
                this.cmbx_joints.Items.Add(jointType);
            }
        }

        void KinectManager_AllDataReady()
        {
            // Fill in the ColorStream data
            int width = KinectManager.DepthWidth;
            int height = KinectManager.DepthHeight;
            if (this.bitmap == null)
            {
                this.bitmap = new WriteableBitmap(width, height, 96, 96, PixelFormats.Bgr32, null);
                this.img_kinectDisplay.Source = bitmap;
            }
            //this.bitmap.WritePixels(new Int32Rect(0, 0, 640, 480), KinectManager.MappedColorData, 640 * 4, 0);

            // Update the skeletonOverlay to show skeletons on the bitmap image.
            this.skeletonOverlay.UpdateSkeleton();

            if (!this.paused)
            {
                // Use first occurance found of a sticky skeleton in the StickySkeletonIDs array.
                foreach (ulong? stickySkelId in KinectManager.StickySkeletonIDs)
                {
                    if (stickySkelId.HasValue)
                    {
                        // Update JointHistory Object
                        this.jointHistory.UpdateJointHistory(KinectManager.GetSkeleton(stickySkelId.Value, true));

                        // Only update UI if it's checked.
                        if (this.chkbx_showJointHistory.IsChecked.Value)
                            this.UpdateJointsUI();
                        
                        break;
                    }
                }
            }
        }

        private void btn_addJoint_Click(object sender, RoutedEventArgs e)
        {
            string jointToAdd = (string)this.cmbx_joints.SelectedItem;

            if (jointToAdd != null)
            {
                this.jointHistory.AddJoint((JointType)Enum.Parse(typeof(JointType), jointToAdd));
                this.ResetJointsUI();
            }
        }

        private void btn_removeJoint_Click(object sender, RoutedEventArgs e)
        {
            string jointToRemove = (string)this.cmbx_joints.SelectedItem;

            if (jointToRemove != null)
            {
                this.jointHistory.RemoveJoint((JointType)Enum.Parse(typeof(JointType), jointToRemove));
                this.ResetJointsUI();
            }
        }

        private void ResetJointsUI()
        {
            IEnumerable<JointType> sortedJoints =
                from joint in this.jointHistory.TrackedJoints
                orderby Enum.GetName(typeof(JointType),joint) ascending
                select joint;

            this.stckpnl_trackedJoints.Children.Clear();

            foreach (JointType jointType in sortedJoints)
            {
                // If checkbox checked, make textbox.
                this.stckpnl_trackedJoints.Children.Add(new Expander() {
                    Name = String.Format("exp_{0}", Enum.GetName(typeof(JointType), jointType)),
                    Header = String.Format("{0} Trend: {1}", Enum.GetName(typeof(JointType), jointType), this.jointHistory.TrendOfAll(jointType).ToString()),
                    HorizontalAlignment = System.Windows.HorizontalAlignment.Left,
                    Content = this.chkbx_showJointHistory.IsChecked.Value 
                        ? new TextBox() { 
                        VerticalScrollBarVisibility = ScrollBarVisibility.Auto, 
                        HorizontalScrollBarVisibility = ScrollBarVisibility.Auto,
                        MaxHeight = 300,
                        Name = String.Format("txbx_{0}", Enum.GetName(typeof(JointType), jointType)) } 
                        : null
                });
            }
        }

        private void UpdateJointsUI()
        {
            foreach (var child in this.stckpnl_trackedJoints.Children)
            {
                if (child.GetType() == typeof(Expander))
                {
                    // Update Expander Header
                    ((Expander)child).Header = String.Format("{0} Trend: {1}", (string)((Expander)child).Name.Substring(4),
                        this.jointHistory.TrendOfAll((JointType)Enum.Parse(typeof(JointType), (string)((Expander)child).Name.Substring(4))));

                    // Get jointHistory for the joint of Expander using Name of 
                    JointExtended[] joints = this.jointHistory[(JointType)Enum.Parse(typeof(JointType), (string)((Expander)child).Name.Substring(4))];
                    string blockContent = "";

                    foreach (JointExtended joint in joints)
                    {
                        try
                        {
                            if (joint != null)
                            {
                                // Show XYZ Positions
                                blockContent += String.Format("({0:#.##}, {1:#.##}, {2:#.##})", joint.Position.X * 1000, joint.Position.Y * 1000, joint.Position.Z * 1000);
                                // Show XYZ Trends
                                blockContent += String.Format(" Trends X:{0} Y:{1} Z:{2}", joint.TrendX, joint.TrendY, joint.TrendZ);
                                // Show DateTime
                                blockContent += string.Format(" Time: {0}\n", joint.Time);
                            }
                        }
                        catch (Exception exc)
                        {
                            System.Diagnostics.Debug.Print("JointHistoryTestPage UpdateJointsUI ERROR: {0}", exc.Message);
                        }
                    }

                    ((TextBox)((Expander)child).Content).Text = blockContent;
                }
            }
        }

        private void txbx_jointHistorySize_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                this.HistorySize = int.Parse(((TextBox)e.Source).Text);
                this.jointHistory.HistoryLength = this.HistorySize;
            }
            catch (Exception exc)
            {
                Debug.Print("AIMS.TestPage txbx_jointHistorySize_TextChanged ERROR: {0}", exc.Message);
            }
        }

        private void btn_pause_Click(object sender, RoutedEventArgs e)
        {
            this.TogglePause();
        }

        private void TogglePause()
        {
            this.paused = !this.paused;

            if (paused)
                this.btn_pause.Content = "Resume";
            else
                this.btn_pause.Content = "Pause";
        }

        private void chkbx_showJointHistory_Click(object sender, RoutedEventArgs e)
        {
            this.ResetJointsUI();
        }
    }
}