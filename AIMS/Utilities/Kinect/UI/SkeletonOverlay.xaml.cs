﻿/****************************************************************************
 * 
 *  Class: SkeletonOverlay
 *  Author: Mitchel Weate
 *  
 *  SkeletonOverlay is used to paint tracked joints over the colored image.
 *  TrackedJoint is used to specify which joints should be drawn as well as
 *  how they should be drawn.  
 *   
 * ***************************************************************************/

using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;

namespace AIMS.Utilities.Kinect
{
    /// <summary>
    /// Skeleton overlay will display specified joints for the skeleton that is passed in.
    /// </summary>
    public partial class SkeletonOverlay : UserControl, INotifyPropertyChanged
    {
        #region Public Properties

        public bool ShowJointOverlays
        {
            get { return (bool)GetValue(ShowJointOverlaysProperty); }
            set { SetValue(ShowJointOverlaysProperty, value); this.NotifyPropertyChanged("ShowJointOverlays"); }
        }

        public bool ShowJointTextOverlays
        {
            get { return (bool)GetValue(ShowJointTextOverlaysProperty); }
            set { SetValue(ShowJointTextOverlaysProperty, value); this.NotifyPropertyChanged("ShowJointTextOverlays"); }
        }

        public bool ShowBoneOverlays
        {
            get { return (bool)GetValue(ShowBoneOverlaysProperty); }
            set { SetValue(ShowBoneOverlaysProperty, value); this.NotifyPropertyChanged("ShowBoneOverlays"); }
        }

        public bool ShowSkeletonIdentifier
        {
            get { return (bool)GetValue(ShowSkeletonIdentifierProperty); }
            set { SetValue(ShowSkeletonIdentifierProperty, value); this.NotifyPropertyChanged("ShowSkeletonIdentifier"); }
        }

        /// <summary>
        /// Show the closest skeleton in the overlay.
        /// </summary>
        public bool ShowClosest
        {
            get { return (bool)GetValue(ShowClosestProperty); }
            set { SetValue(ShowClosestProperty, value); this.NotifyPropertyChanged("ShowClosest"); }
        }

        /// <summary>
        /// Show sticky skeletons in the overlay.
        /// </summary>
        public bool ShowSticky
        {
            get { return (bool)GetValue(ShowStickyProperty); }
            set { SetValue(ShowStickyProperty, value); this.NotifyPropertyChanged("ShowSticky"); }
        }

        /// <summary>
        /// When True, SkeletonOverlay updates when the SkeletonFrame is ready instead of when UpdateSkeleton() is called.
        /// </summary>
        public bool RunIndependently
        {
            get { return (bool)GetValue(RunIndependentlyProperty); }
            set { SetValue(RunIndependentlyProperty, value); }
        }

        public bool ShowHeadToCenterShoulderBone
        {
            get { return (bool)GetValue(ShowHeadToCenterShoulderBoneProperty); }
            set { SetValue(ShowHeadToCenterShoulderBoneProperty, value); }
        }

        public bool ShowCenterShoulderToRightShoulderBone
        {
            get { return (bool)GetValue(ShowCenterShoulderToRightShoulderBoneProperty); }
            set { SetValue(ShowCenterShoulderToRightShoulderBoneProperty, value); }
        }

        public bool ShowRightShoulderToRightElbowBone
        {
            get { return (bool)GetValue(ShowRightShoulderToRightElbowBoneProperty); }
            set { SetValue(ShowRightShoulderToRightElbowBoneProperty, value); }
        }

        public bool ShowRightElbowToRightWristBone
        {
            get { return (bool)GetValue(ShowRightElbowToRightWristBoneProperty); }
            set { SetValue(ShowRightElbowToRightWristBoneProperty, value); }
        }

        public bool ShowRightWristToRightHandBone
        {
            get { return (bool)GetValue(ShowRightWristToRightHandBoneProperty); }
            set { SetValue(ShowRightWristToRightHandBoneProperty, value); }
        }

        public bool ShowCenterShoulderToLeftShoulderBone
        {
            get { return (bool)GetValue(ShowCenterShoulderToLeftShoulderBoneProperty); }
            set { SetValue(ShowCenterShoulderToLeftShoulderBoneProperty, value); }
        }

        public bool ShowLeftShoulderToLeftElbowBone
        {
            get { return (bool)GetValue(ShowLeftShoulderToLeftElbowBoneProperty); }
            set { SetValue(ShowLeftShoulderToLeftElbowBoneProperty, value); }
        }

        public bool ShowLeftElbowToLeftWristBone
        {
            get { return (bool)GetValue(ShowLeftElbowToLeftWristBoneProperty); }
            set { SetValue(ShowLeftElbowToLeftWristBoneProperty, value); }
        }

        public bool ShowLeftWristToLeftHandBone
        {
            get { return (bool)GetValue(ShowLeftWristToLeftHandBoneProperty); }
            set { SetValue(ShowLeftWristToLeftHandBoneProperty, value); }
        }

        public bool ShowShoulderCenterToSpineBone
        {
            get { return (bool)GetValue(ShowCenterShoulderToSpineBoneProperty); }
            set { SetValue(ShowCenterShoulderToSpineBoneProperty, value); }
        }

        public bool ShowSpineToCenterHipBone
        {
            get { return (bool)GetValue(ShowSpineToCenterHipBoneProperty); }
            set { SetValue(ShowSpineToCenterHipBoneProperty, value); }
        }

        public bool ShowCenterHipToRightHipBone
        {
            get { return (bool)GetValue(ShowCenterHipToRightHipBoneProperty); }
            set { SetValue(ShowCenterHipToRightHipBoneProperty, value); }
        }

        public bool ShowRightHipToRightKneeBone
        {
            get { return (bool)GetValue(ShowRightHipToRightKneeBoneProperty); }
            set { SetValue(ShowRightHipToRightKneeBoneProperty, value); }
        }

        public bool ShowRightKneeToRightAnkleBone
        {
            get { return (bool)GetValue(ShowRightKneeToRightAnkleBoneProperty); }
            set { SetValue(ShowRightKneeToRightAnkleBoneProperty, value); }
        }

        public bool ShowRightAnkleToRightFootBone
        {
            get { return (bool)GetValue(ShowRightAnkleToRightFootBoneProperty); }
            set { SetValue(ShowRightAnkleToRightFootBoneProperty, value); }
        }

        public bool ShowCenterHipToLeftHipBone
        {
            get { return (bool)GetValue(ShowCenterHipToLeftHipBoneProperty); }
            set { SetValue(ShowCenterHipToLeftHipBoneProperty, value); }
        }

        public bool ShowLeftHipToLeftKneeBone
        {
            get { return (bool)GetValue(ShowLeftHipToLeftKneeBoneProperty); }
            set { SetValue(ShowLeftHipToLeftKneeBoneProperty, value); }
        }

        public bool ShowLeftKneeToLeftAnkleBone
        {
            get { return (bool)GetValue(ShowLeftKneeToLeftAnkleBoneProperty); }
            set { SetValue(ShowLeftKneeToLeftAnkleBoneProperty, value); }
        }

        public bool ShowLeftAnkleToLeftFootBone
        {
            get { return (bool)GetValue(ShowLeftAnkleToLeftFootBoneProperty); }
            set { SetValue(ShowLeftAnkleToLeftFootBoneProperty, value); }
        }

        #endregion

        #region DependencyProperty Registrations

        public static readonly DependencyProperty ShowJointOverlaysProperty =
            DependencyProperty.Register("ShowJointOverlays", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(false, OverlaySettingChanged));

        public static readonly DependencyProperty ShowJointTextOverlaysProperty =
            DependencyProperty.Register("ShowJointTextOverlays", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(false, OverlaySettingChanged));

        public static readonly DependencyProperty ShowBoneOverlaysProperty =
            DependencyProperty.Register("ShowBoneOverlays", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(false, OverlaySettingChanged));
			
		public static readonly DependencyProperty ShowSkeletonIdentifierProperty = 
			DependencyProperty.Register("ShowSkeletonIdentifier", typeof( bool ), typeof( SkeletonOverlay), new PropertyMetadata( true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowClosestProperty =
            DependencyProperty.Register("ShowClosest", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowStickyProperty =
            DependencyProperty.Register("ShowSticky", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty RunIndependentlyProperty =
            DependencyProperty.Register("RunIndependently", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowHeadToCenterShoulderBoneProperty =
            DependencyProperty.Register("ShowHeadToCenterShoulderBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowCenterShoulderToRightShoulderBoneProperty =
            DependencyProperty.Register("ShowCenterShoulderToRightShoulderBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowRightShoulderToRightElbowBoneProperty =
            DependencyProperty.Register("ShowRightShoulderToRightElbowBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowRightElbowToRightWristBoneProperty =
            DependencyProperty.Register("ShowRightElbowToRightWristBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowRightWristToRightHandBoneProperty =
            DependencyProperty.Register("ShowRightWristToRightHandBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowCenterShoulderToLeftShoulderBoneProperty =
            DependencyProperty.Register("ShowCenterShoulderToLeftShoulderBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowLeftShoulderToLeftElbowBoneProperty =
            DependencyProperty.Register("ShowLeftShoulderToLeftElbowBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowLeftElbowToLeftWristBoneProperty =
            DependencyProperty.Register("ShowLeftElbowToLeftWristBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowLeftWristToLeftHandBoneProperty =
            DependencyProperty.Register("ShowLeftWristToLeftHandBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowCenterShoulderToSpineBoneProperty =
            DependencyProperty.Register("ShowCenterShoulderToSpineBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowSpineToCenterHipBoneProperty =
            DependencyProperty.Register("ShowSpineToCenterHipBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowCenterHipToRightHipBoneProperty =
            DependencyProperty.Register("ShowCenterHipToRightHipBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowRightHipToRightKneeBoneProperty =
            DependencyProperty.Register("ShowRightHipToRightKneeBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowRightKneeToRightAnkleBoneProperty =
            DependencyProperty.Register("ShowRightKneeToRightAnkleBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowRightAnkleToRightFootBoneProperty =
            DependencyProperty.Register("ShowRightAnkleToRightFootBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowCenterHipToLeftHipBoneProperty =
            DependencyProperty.Register("ShowCenterHipToLeftHipBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowLeftHipToLeftKneeBoneProperty =
            DependencyProperty.Register("ShowLeftHipToLeftKneeBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowLeftKneeToLeftAnkleBoneProperty =
            DependencyProperty.Register("ShowLeftKneeToLeftAnkleBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        public static readonly DependencyProperty ShowLeftAnkleToLeftFootBoneProperty =
            DependencyProperty.Register("ShowLeftAnkleToLeftFootBone", typeof(bool), typeof(SkeletonOverlay), new PropertyMetadata(true, OverlaySettingChanged));

        #endregion

        #region Public Events, Readonly

        int colorWidth = 1920;
        int colorHeight = 1080;

        /// <summary>
        /// Public event for Binding
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /***  Brushes  ***/
        public readonly Brush primarySkeletonBrush = Brushes.Lime;
        public readonly Brush[] stickySkeletonBrushes = { Brushes.Coral, Brushes.OrangeRed, Brushes.LightCyan, Brushes.LightGoldenrodYellow, 
                                                            Brushes.LightCoral, Brushes.LightSeaGreen };
        public readonly Brush inferredBoneBrush = Brushes.LightGray;
        public readonly Brush trackedBoneBrush = Brushes.White;

        #endregion

        #region Private Fields / Members

        private SkeletonManager skeletonManager;
        private bool isSubscribedToSkeletonsUpdated = false;
        /// <summary>
        /// Dictionary of JointHistories. The Key is the TrackingId of a Trackable Skeleton.
        /// This won't be necessary when updates to the SkeletonManager are complete.
        /// </summary>
        private Dictionary<ulong, JointHistory> jointHistoryDict = new Dictionary<ulong, JointHistory>(KinectManager.MaxTrackableSkeletons);

        /***  Bone Widths  ***/
        private const double primaryBoneWidth = 2.00;
        private const double stickyBoneWidth = 2.5;
        private const double inferredBoneWidth = 0.50;
        private const double trackedBoneWidth = 1.25;

        #endregion
        
        public SkeletonOverlay()
        {
            InitializeComponent();
        }
        
        #region Public Functions

        /// <summary>
        /// Update the skeleton overlay. Unnecessary when then RunIndependently property is True.
        /// </summary>
        public void UpdateSkeleton()
        {
            this.InvalidateVisual();
        }

        #endregion

        #region Protected Functions

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);
            
            double scale = ActualWidth / colorWidth;
            double scaledJointSize = 1.5 * scale;

            // If the skeletonManger isn't attached, return.
            if (this.skeletonManager == null)
                return;

            foreach (Body skeleton in this.skeletonManager.Skeletons)
            {
                if (skeleton == null)
                {
                    continue;
                }

                if (!skeleton.IsTracked)
                    continue;   // Skip this skeletonProfile and move on to the next.

                bool skeletonIsSticky = false;
                bool skeletonIsClosest = false;

                int stickyIndex = 0;

                if (skeleton == this.skeletonManager.ClosestSkeleton)
                    skeletonIsClosest = true;

                foreach (Body stickySkel in this.skeletonManager.StickySkeletons)
                {
                    if (skeleton == stickySkel)
                    {
                        skeletonIsSticky = true;
                        break;
                    }
                }

                // If a tracked skeleton, make sure it's joint history is being calculated.
                if (skeleton.IsTracked)
                {
                    if (!this.jointHistoryDict.ContainsKey(skeleton.TrackingId))
                    {
                        this.jointHistoryDict.Add(skeleton.TrackingId, new JointHistory() { HistoryLength = 5 });
                        this.jointHistoryDict[skeleton.TrackingId].AddAllJoints();
                    }

                    this.jointHistoryDict[skeleton.TrackingId].UpdateJointHistory(skeleton);
                }
                // Not tracked but joint history dictionary contains TrackingId, remove it from the dictionary.
                else if (this.jointHistoryDict.ContainsKey(skeleton.TrackingId))
                {
                    this.jointHistoryDict.Remove(skeleton.TrackingId);
                }

                if (ShowSkeletonIdentifier)
                {
                    if (skeleton != null)
                    {
                        //drawingContext.DrawEllipse(Brushes.Red, null, Get2DPosition(skeletonProfile.CurrentPosition, true), scaledJointSize, scaledJointSize);
                        Point p = Get2DPosition(skeleton.Joints[JointType.SpineMid].Position, false);   // DOESN'T SCALE THIS POINT TO RENDER SIZE!
                        try
                        {
                            drawingContext.DrawText(new FormattedText(skeleton.TrackingId.ToString(), System.Globalization.CultureInfo.InvariantCulture,
                                System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Regular,
                                FontStretches.Normal), 10.0 * ActualHeight / colorHeight, Brushes.GhostWhite), new Point((p.X - 3) * this.ActualWidth / colorWidth,
                                (p.Y - 15) * this.ActualHeight / colorHeight));
                        }
                        catch
                        {
                        }
                    }
                }

                if (ShowBoneOverlays)
                {
                    if (ShowHeadToCenterShoulderBone)
                        DrawBone(ref drawingContext, skeleton.Joints[JointType.Head], skeleton.Joints[JointType.SpineShoulder],
                            ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if (ShowCenterShoulderToRightShoulderBone)
                        DrawBone(ref drawingContext, skeleton.Joints[JointType.SpineShoulder], skeleton.Joints[JointType.ShoulderRight],
                            ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if (ShowCenterShoulderToLeftShoulderBone)
                        DrawBone(ref drawingContext, skeleton.Joints[JointType.SpineShoulder], skeleton.Joints[JointType.ShoulderLeft],
                            ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if (ShowRightShoulderToRightElbowBone)
                        DrawBone(ref drawingContext, skeleton.Joints[JointType.ShoulderRight], skeleton.Joints[JointType.ElbowRight],
                            ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if (ShowLeftShoulderToLeftElbowBone)
                        DrawBone(ref drawingContext, skeleton.Joints[JointType.ShoulderLeft], skeleton.Joints[JointType.ElbowLeft],
                            ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if (ShowRightElbowToRightWristBone)
                        DrawBone(ref drawingContext, skeleton.Joints[JointType.ElbowRight], skeleton.Joints[JointType.WristRight],
                            ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if (ShowLeftElbowToLeftWristBone)
                        DrawBone(ref drawingContext, skeleton.Joints[JointType.ElbowLeft], skeleton.Joints[JointType.WristLeft],
                            ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if (ShowRightWristToRightHandBone)
                        DrawBone(ref drawingContext, skeleton.Joints[JointType.WristRight], skeleton.Joints[JointType.HandRight],
                            ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if (ShowLeftWristToLeftHandBone)
                        DrawBone(ref drawingContext, skeleton.Joints[JointType.WristLeft], skeleton.Joints[JointType.HandLeft],
                            ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if (ShowShoulderCenterToSpineBone)
                        DrawBone(ref drawingContext, skeleton.Joints[JointType.SpineShoulder], skeleton.Joints[JointType.SpineMid],
                            ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if (ShowSpineToCenterHipBone)
                        DrawBone(ref drawingContext, skeleton.Joints[JointType.SpineMid], skeleton.Joints[JointType.SpineBase],
                            ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if (ShowCenterHipToRightHipBone)
                        DrawBone(ref drawingContext, skeleton.Joints[JointType.SpineBase], skeleton.Joints[JointType.HipRight],
                            ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if (ShowCenterHipToLeftHipBone)
                        DrawBone(ref drawingContext, skeleton.Joints[JointType.SpineBase], skeleton.Joints[JointType.HipLeft],
                            ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if (ShowRightHipToRightKneeBone)
                        DrawBone(ref drawingContext, skeleton.Joints[JointType.HipRight], skeleton.Joints[JointType.KneeRight],
                            ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if (ShowLeftHipToLeftKneeBone)
                        DrawBone(ref drawingContext, skeleton.Joints[JointType.HipLeft], skeleton.Joints[JointType.KneeLeft],
                            ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if (ShowRightKneeToRightAnkleBone)
                        DrawBone(ref drawingContext, skeleton.Joints[JointType.KneeRight], skeleton.Joints[JointType.AnkleRight],
                            ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if (ShowLeftKneeToLeftAnkleBone)
                        DrawBone(ref drawingContext, skeleton.Joints[JointType.KneeLeft], skeleton.Joints[JointType.AnkleLeft],
                            ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if (ShowRightAnkleToRightFootBone)
                        DrawBone(ref drawingContext, skeleton.Joints[JointType.AnkleRight], skeleton.Joints[JointType.FootRight],
                            ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                    if (ShowLeftAnkleToLeftFootBone)
                        DrawBone(ref drawingContext, skeleton.Joints[JointType.AnkleLeft], skeleton.Joints[JointType.FootLeft],
                            ref skeletonIsClosest, ref skeletonIsSticky, ref stickyIndex, ref scale);
                }

                if (ShowJointOverlays)
                {
                    foreach (Joint joint in skeleton.Joints.Values)
                    {
                        drawingContext.DrawEllipse(Brushes.Red, null, Get2DPosition(joint.Position, true), scaledJointSize, scaledJointSize);

                        Point p = Get2DPosition(joint.Position, false);   // DOESN'T SCALE THIS POINT TO RENDER SIZE!

                        if (this.jointHistoryDict[skeleton.TrackingId] != null)
                        {
                            //MovementAnalysis analysis = jointProfile.MovementAnalysis;
                            Trend3D trend3D = this.jointHistoryDict[skeleton.TrackingId].TrendFromLatest(joint.JointType, 5);

                            if (trend3D.X == Trend.Positive)
                            {
                                drawingContext.DrawText(new FormattedText("→", System.Globalization.CultureInfo.CurrentCulture,
                                    System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Regular,
                                        FontStretches.Normal), 10.0 * ActualHeight / colorHeight, Brushes.Lime), new Point((p.X + 7) * this.ActualWidth / colorWidth, (p.Y - 6) *
                                            this.ActualHeight / colorHeight));
                            }
                            else if (trend3D.X == Trend.Negative)
                            {
                                drawingContext.DrawText(new FormattedText("←", System.Globalization.CultureInfo.CurrentCulture,
                                    System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Regular,
                                        FontStretches.Normal), 10.0 * ActualHeight / colorHeight, Brushes.Lime), new Point((p.X - 17) * this.ActualWidth / colorWidth, (p.Y - 6) *
                                            this.ActualHeight / colorHeight));
                            }

                            if (trend3D.Y == Trend.Positive)
                            {
                                drawingContext.DrawText(new FormattedText("↑", System.Globalization.CultureInfo.CurrentCulture,
                                    System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Regular,
                                        FontStretches.Normal), 10.0 * ActualHeight / colorHeight, Brushes.Lime), new Point((p.X - 3) * this.ActualWidth / colorWidth, (p.Y - 15) *
                                            this.ActualHeight / colorHeight));
                            }
                            else if (trend3D.Y == Trend.Negative)
                            {
                                drawingContext.DrawText(new FormattedText("↓", System.Globalization.CultureInfo.CurrentCulture,
                                    System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Regular,
                                        FontStretches.Normal), 10.0 * ActualHeight / colorHeight, Brushes.Lime), new Point((p.X - 3) * this.ActualWidth / colorWidth, (p.Y + 5) *
                                            this.ActualHeight / colorHeight));
                            }

                            if (trend3D.Z == Trend.Positive)
                            {
                                drawingContext.DrawText(new FormattedText("+", System.Globalization.CultureInfo.CurrentCulture,
                                    System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Regular,
                                        FontStretches.Normal), 8.0 * ActualHeight / colorHeight, Brushes.Cyan), new Point((p.X - 1) * this.ActualWidth / colorWidth, (p.Y - 4) *
                                            this.ActualHeight / colorHeight));
                            }
                            else if (trend3D.Z == Trend.Negative)
                            {
                                drawingContext.DrawText(new FormattedText("-", System.Globalization.CultureInfo.CurrentCulture,
                                    System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"), FontStyles.Normal, FontWeights.Regular,
                                        FontStretches.Normal), 10.0 * ActualHeight / colorHeight, Brushes.Cyan), new Point((p.X - 1) * this.ActualWidth / colorWidth, (p.Y - 7) *
                                            this.ActualHeight / colorHeight));
                            }
                        }
                    }

                    if (ShowJointTextOverlays)
                    {
                        foreach (Joint joint in skeleton.Joints.Values)
                        {
                            Point p = Get2DPosition(joint.Position, false);   // DOESN'T SCALE THIS POINT TO RENDER SIZE!
                            drawingContext.DrawText(new FormattedText(String.Format("{3} \r{4:0.###} \r({0:0.###},{1:0.###},{2:0.###})",
                                (int)(joint.Position.X * 1000), (int)(joint.Position.Y * 1000),
                                (int)(joint.Position.Z * 1000), joint.JointType, String.Format("ColorXY: ({0:0.###},{1:0.###})", p.X, p.Y)),
                                System.Globalization.CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface(new FontFamily("Arial"),
                                    FontStyles.Normal, FontWeights.Regular, FontStretches.Normal), 5.0 * ActualHeight / colorHeight, Brushes.Lime), new Point((p.X - 10) *
                                        this.ActualWidth / colorWidth, (p.Y - 10) * this.ActualHeight / colorHeight));
                        }
                    }
                }
            }
        }

        #endregion

        #region Private Functions

        /// <summary>
        /// For internal use. Notifies that a DependencyProperty has changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OverlaySettingChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            SkeletonOverlay overlay = sender as SkeletonOverlay;

            // Subscribe to, or unsubscribe from, the SkeletonManager SkeletonsUpdated event based on RunIndependently property and 
            // whether or not already subscribed.
            if (overlay.RunIndependently && !overlay.isSubscribedToSkeletonsUpdated && overlay.skeletonManager != null)
            {
                overlay.isSubscribedToSkeletonsUpdated = true;
                overlay.skeletonManager.SkeletonsUpdated += overlay.skeletonManager_SkeletonsUpdated;
            }
            else if (!overlay.RunIndependently && overlay.isSubscribedToSkeletonsUpdated && overlay.skeletonManager != null)
            {
                overlay.isSubscribedToSkeletonsUpdated = false;
                overlay.skeletonManager.SkeletonsUpdated -= overlay.skeletonManager_SkeletonsUpdated;
            }

            if (overlay != null)
            {
                overlay.UpdateSkeleton();
            }
        }

        /// <summary>
        /// For Binding. Notifies that a property has changed.
        /// </summary>
        /// <param name="propertyName"></param>
        private void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void DrawBone( ref DrawingContext drawingContext, Joint a, Joint b, ref bool skeletonIsPrimary, ref bool skeletonIsSticky, ref int stickyIndex, 
            ref double scale )
        {
            if (null == a || null == b)
                return;

            if (TrackingState.NotTracked == a.TrackingState || TrackingState.NotTracked == b.TrackingState )
                return;

            bool bothFullyTracked = (TrackingState.Tracked == a.TrackingState && TrackingState.Tracked == b.TrackingState);

            if (bothFullyTracked)
            {
                drawingContext.DrawLine(new Pen( ( ShowClosest && skeletonIsPrimary ) ? primarySkeletonBrush : ( ShowSticky && skeletonIsSticky ) ? 
                    stickySkeletonBrushes[stickyIndex] : inferredBoneBrush, (skeletonIsPrimary ? primaryBoneWidth : skeletonIsSticky ? stickyBoneWidth : 
                    trackedBoneWidth) * scale), Get2DPosition(a), Get2DPosition(b));
            }
            else
            {
                drawingContext.DrawLine(new Pen(inferredBoneBrush, inferredBoneWidth * scale), Get2DPosition(a), Get2DPosition(b));
            }
        }

        // Converts the point from the dictionary into pixel coordinates so that it can be drawn.
        private Point Get2DPosition(Point point)
        {
            point.X = point.X * this.RenderSize.Width;
            point.Y = point.Y * this.RenderSize.Height;

            return point;
        }

        /*
        private Point Get2DPosition(DepthImagePoint depthPoint, bool scaleToDisplaySize)
        {
            Point point = new Point();

            point.X = (double)depthPoint.X / KinectManager.DepthImageFrame.Width;
            point.Y = (double)depthPoint.Y / KinectManager.DepthImageFrame.Height;

            point.X = point.X * colorWidth;
            point.Y = point.Y * colorHeight;

            if (scaleToDisplaySize)
            {
                point.X *= this.ActualWidth / colorWidth;
                point.Y *= this.ActualHeight / colorHeight;
            }

            return point;
        }
        */

        // Converts the point from the dictionary into pixel coordinates so that it can be drawn.
        private Point Get2DPosition(Joint joint, bool scaleToDisplaySize = true)
        {
            Point point = new Point();

            if (joint == null || KinectManager.ColorWidth == 0)
                return point;

            ColorSpacePoint colorPoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(joint.Position);
            point.X = (double)colorPoint.X;
            point.Y = (double)colorPoint.Y;

            if (scaleToDisplaySize)
            {
                point.X *= this.ActualWidth / colorWidth;
                point.Y *= this.ActualHeight / colorHeight;
            }

            return point;
        }

        // Converts the point from the dictionary into pixel coordinates so that it can be drawn.
        private Point Get2DPosition(CameraSpacePoint skelepoint, bool scaleToDisplaySize = true)
        {
            Point point = new Point();

            if (KinectManager.ColorWidth == 0)
                return point;

            //DepthImagePoint depthPoint = KinectManager.DepthImageFrame.MapFromSkeletonPoint(joint.Position);
            //DepthImagePoint depthPoint = KinectManager.Kinect.CoordinateMapper.MapSkeletonPointToDepthPoint(skelepoint, KinectManager.DepthImageFrame.Format);
            ColorSpacePoint colorPoint = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(skelepoint);
            point.X = (double)colorPoint.X;
            point.Y = (double)colorPoint.Y;

            if (scaleToDisplaySize)
            {
                point.X *= this.ActualWidth / colorWidth;
                point.Y *= this.ActualHeight / colorHeight;
            }

            return point;
        }
        #endregion

        #region Event Handlers

        /// <summary>
        /// Handler for whenever the SkeletonsUpdated event fires from the SkeletonManager.
        /// </summary>
        /// <param name="sender"></param>
        private void skeletonManager_SkeletonsUpdated(object sender)
        {
            this.UpdateSkeleton();
        }

        /// <summary>
        /// If the SkeletonOverlay is Loaded, subscribe to events.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.skeletonManager = KinectManager.SkeletonManager;

            if (this.skeletonManager != null)
            {
                this.skeletonManager.NumStickySkeletons = 2;

                if (this.RunIndependently && !this.isSubscribedToSkeletonsUpdated)
                {
                    this.isSubscribedToSkeletonsUpdated = true;
                    this.skeletonManager.SkeletonsUpdated += skeletonManager_SkeletonsUpdated;
                }
            }
        }

        /// <summary>
        /// If the SkeletonOverlay is unloaded, unsubscribe from subscribed events.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            if (this.isSubscribedToSkeletonsUpdated && this.skeletonManager != null)
            {
                this.isSubscribedToSkeletonsUpdated = false;
                this.skeletonManager.SkeletonsUpdated -= skeletonManager_SkeletonsUpdated;
            }

            this.skeletonManager = null;
        }

        #endregion
    }
}
