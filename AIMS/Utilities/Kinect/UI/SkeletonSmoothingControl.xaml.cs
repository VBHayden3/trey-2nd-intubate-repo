﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Diagnostics;

/*****************************************************************************************
 *  
 *  SkeletonSmoothingControl is a tool to test the Skeleton Smoothing Parameters.
 *  
 *  Hover over the different properties on the page to get a ToolTip describing each 
 *  of them.
 *  
 *  When the Smoothing checkbox is enabled, smoothing is being used by the SkeletonStream
 *  with the parameter values present on the screen.
 *  
 *****************************************************************************************/

namespace AIMS.Utilities.SkeletonSmoothing
{
    /// <summary>
    /// Interaction logic for SkeletonSmoothingControl.xaml
    /// </summary>
    public partial class SkeletonSmoothingControl : UserControl, INotifyPropertyChanged
    {
        #region Private Fields

        // Bitmap image to display kinect output.
        private WriteableBitmap bitmap = new WriteableBitmap(640, 480, 96, 96, PixelFormats.Bgr32, null);

        // Used to see if the elevation has changed after a duration.
        private System.Timers.Timer elevationTimeCheck = new System.Timers.Timer(500) { AutoReset = false };

        // Stores current kinect elevation to see if changed within a duration.
        private int currentKinectElevation;

        #endregion

        #region Public Events / Fields

        // PropertyChanged event to alert Bindings
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        // Constructor for SkeletonSmoothingControl.
        public SkeletonSmoothingControl()
        {
            InitializeComponent();

            this.DataContext = this;

            this.img_kinectDisplay.Source = bitmap;

            this.elevationTimeCheck.Elapsed += new System.Timers.ElapsedEventHandler(elevationTimeCheck_Elapsed);
        }

        // Destructor for SkeletonSmoothingControl.
        ~SkeletonSmoothingControl()
        {
            this.elevationTimeCheck.Elapsed -= elevationTimeCheck_Elapsed;
        }

        #region Private Non-Action Functions

        /// <summary>
        /// Raise Property Changed Event, updates binding.
        /// </summary>
        /// <param name="propertyName">Property to be raised for.</param>
        private void RaisePropertyChanged(string propertyName)
        {
            try
            {
                // take a copy to prevent thread issues
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs(propertyName));
            }
            catch (Exception exc)
            {
                Debug.Print("SkeletonSmoothingControl RaisePropertyChanged ERROR: {0}", exc.Message);
            }
        }

        /// <summary>
        /// Reset the Skeleton Stream to enable / disable skeleton filtering
        /// </summary>
        /// <param name="filter">True: Use filter. False: Regular skeleton stream.</param>
        private void ResetSkeletonStream(bool filter = true)
        {
            /*
            // Disable skeleton stream if running.
            if (KinectManager.Kinect.SkeletonStream.IsEnabled)
                KinectManager.Kinect.SkeletonStream.Disable();

            // Enable skeleton stream either w/ or w/out filter.
            if (filter)
                KinectManager.Kinect.SkeletonStream.Enable(this.smoothingParams);
            else
                KinectManager.Kinect.SkeletonStream.Enable();
            */
        }

        #endregion

        #region Control Action Functions

        // SkeletonSmoothingControl finished loading.
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (KinectManager.Kinect != null)
                {
                    KinectManager.AllDataReady += new AllDataReadyEventHandler(KinectManager_AllDataReady);
                    // If Kinect Skeleton Stream is off or smoothing is enabled, enable without smoothing.
                    /*
                    if (!KinectManager.Kinect.SkeletonStream.IsEnabled)
                    {
                        KinectManager.Kinect.SkeletonStream.Enable();
                    }
                    else if (KinectManager.Kinect.SkeletonStream.IsSmoothingEnabled)
                    {
                        KinectManager.Kinect.SkeletonStream.Disable();
                        KinectManager.Kinect.SkeletonStream.Enable();
                    }
                    */
                }
            }
            catch (Exception exc)
            {
                Debug.Print("SkeletonSmoothingControl UserControl_Loaded ERROR: {0}", exc.Message);
            }

            try
            {
                // Set Kinect Angle slider max and min.
                /*
                this.sldr_kinectAngle.Maximum = KinectManager.Kinect.MaxElevationAngle;
                this.sldr_kinectAngle.Minimum = KinectManager.Kinect.MinElevationAngle;

                // Update Sliders to initial values.
                this.sldr_smoothing.Value = this.smoothingParams.Smoothing;
                this.sldr_correction.Value = this.smoothingParams.Correction;
                this.sldr_prediction.Value = this.smoothingParams.Prediction;
                this.sldr_jitterRadius.Value = this.smoothingParams.JitterRadius;
                this.sldr_maxDeviationRadius.Value = this.smoothingParams.MaxDeviationRadius;

                // Update the Kinect Label with the current value.
                this.lbl_kinectAngle.Content = KinectManager.Kinect.ElevationAngle;
                this.sldr_kinectAngle.Value = KinectManager.Kinect.ElevationAngle;
                 * */
            }
            catch (Exception exc)
            {
                Debug.Print("UserControl_Loaded SkeletonSmoothingControl ERROR: {0}", exc.Message);
            }
        }

        // Handle streams from Kinect.
        private void KinectManager_AllDataReady()
        {
            // Fill in the ColorStream data
            // @todo update dimensions
            // @todo determine why this has an argument exception
            //this.bitmap.WritePixels(new Int32Rect(0, 0, 640, 480), KinectManager.MappedColorData, 640 * 4, 0);

            // Update the skeletonOverlay to show skeletons on the bitmap image.
           this.skeletonOverlay.UpdateSkeleton();
        }

        // SkeletonSmoothingControl was unloaded.
        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            KinectManager.AllDataReady -= KinectManager_AllDataReady;
        }

        // The timer to alter the Kinect angle or not.
        void elevationTimeCheck_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                //KinectManager.Kinect.ElevationAngle = this.currentKinectElevation;
            }
            catch (Exception exc)
            {
                Debug.Print("SkeletonSmoothingControl elevationTimeCheck_Elapsed ERROR: {0}", exc.Message);
            }
        }

        #endregion

        #region User Action Functions

        private void chkbx_Smooth_Checked(object sender, RoutedEventArgs e)
        {
            this.ResetSkeletonStream(true);
        }

        private void chkbx_Smooth_Unchecked(object sender, RoutedEventArgs e)
        {
            this.ResetSkeletonStream(false);
        }

        private void sldr_smoothing_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                //this.Smoothing = (float)e.NewValue;

                if (this.chkbx_Smooth.IsChecked == true)    
                    this.ResetSkeletonStream();
            }
            catch (Exception exc)
            {
                Debug.Print("Smoothing Slider Error: {0}", exc.Message);
            }
        }

        private void sldr_correction_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                //this.Correction = (float)e.NewValue;

                if (this.chkbx_Smooth.IsChecked == true)
                    this.ResetSkeletonStream();
            }
            catch (Exception exc)
            {
                Debug.Print("Correction Slider Error: {0}", exc.Message);
            }
        }

        private void sldr_prediction_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                //this.Prediction = (float)e.NewValue;

                if (this.chkbx_Smooth.IsChecked == true)
                    this.ResetSkeletonStream();
            }
            catch (Exception exc)
            {
                Debug.Print("Prediction Slider Error: {0}", exc.Message);
            }
        }

        private void sldr_jitterRadius_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                //this.JitterRadius = (float)e.NewValue;

                if (this.chkbx_Smooth.IsChecked == true)
                    this.ResetSkeletonStream();
            }
            catch (Exception exc)
            {
                Debug.Print("Jitter Radius Slider Error: {0}", exc.Message);
            }
        }

        private void sldr_maxDeviationRadius_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                //this.MaxDeviationRadius = (float)e.NewValue;

                if (this.chkbx_Smooth.IsChecked == true)
                    this.ResetSkeletonStream();
            }
            catch (Exception exc)
            {
                Debug.Print("Max Deviation Radius Slider Error: {0}", exc.Message);
            }
        }

        private void sldr_kinectAngle_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                // Update the KinectManager elevation angle half a second after the last time the slider was changed.
                if (this.elevationTimeCheck.Enabled)
                    this.elevationTimeCheck.Stop();

                this.elevationTimeCheck.Start();

                // Store elevation to check later when timer Ticks
                this.currentKinectElevation = (int)e.NewValue;
                // Update UI ( Binding was throwing an error, so doing the clunky way )
                this.lbl_kinectAngle.Content = (int)e.NewValue;
            }
            catch (Exception exc)
            {
                Debug.Print("Kinect Angle Error: {0}", exc.Message);
            }
        }

        #endregion
    }
}
