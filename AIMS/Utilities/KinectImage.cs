﻿using Microsoft.Kinect;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace AIMS
{
    public class KinectImage
    {
        private int uid = 0;

        private DateTime timestamp = DateTime.MinValue;

        private bool hasColor = false;
        private bool hasDepth = false;

        private byte[] compressedColorData; // What's being stored in the Database.
        private byte[] compressedDepthData; // What's being stored in the Database.

        /// <summary>
        /// Unique database identifier. This value is set by the database, but may need to be known for relationing pointers.
        /// </summary>
        public int UID { get { return uid; } private set { uid = value; } }
        /// <summary>
        /// The DateTime of when the kinect image was captured.
        /// </summary>
        public DateTime Timestamp { get { return timestamp; } set { timestamp = value; } }

        /// <summary>
        /// Whether or not the color data exists for this kinect image.
        /// </summary>
        public bool HasColor { get { return hasColor; } set { hasColor = value; } }
        /// <summary>
        /// Whether or not the depth data exists for this kinect image.
        /// </summary>
        public bool HasDepth { get { return hasDepth; } set { hasDepth = value; } }

        /// <summary>
        /// This color data is compressed for database storage.
        /// </summary>
        public byte[] CompressedColorData { get { return compressedColorData; } set { compressedColorData = value; } }
        /// <summary>
        /// This depth data is compressed for database storage.
        /// </summary>
        public byte[] CompressedDepthData { get { return compressedDepthData; } set { compressedDepthData = value; } }

        /// <summary>
        /// Compresses or decompresses the color data to or from its original form.
        /// </summary>
        public byte[] ColorData { get { return KinectImage.Decompress(compressedColorData); } set { compressedColorData = KinectImage.Compress(value); } }
        /// <summary>
        /// Compresses or decompresses the depth data to or from its original form.
        /// </summary>
        public ushort[] DepthData
        { 
            get
            {
                byte[] uncompressedBytes = KinectImage.Decompress(compressedDepthData);
                ushort[] shorts = new ushort[uncompressedBytes.Length / 2];

                for (int i = 0; i < shorts.Length; i++)
                {
                    shorts[i] = (ushort)((((int)uncompressedBytes[2 * i]) << 8) + uncompressedBytes[2 * i + 1]);
                }

                return shorts;
            }
            set
            {
                ushort[] shorts = value;
                byte[] convertedBytes = new byte[shorts.Length * 2];

                for (int i = 0; i < shorts.Length; i++)
                {
                    convertedBytes[2 * i] = (byte)(shorts[i] >> 8);
                    convertedBytes[2 * i + 1] = (byte)(shorts[i]);
                }

                compressedDepthData = KinectImage.Compress(convertedBytes);
            }
        }

        /// <summary>
        /// Creates a snapshot from the current data in KinectManager.
        /// 
        /// Setting useDepth or useColor to false will create a snapshot with only one or the other.
        /// This can be useful if only attempting to capture an image.
        /// </summary>
        /// <param name="useDepth"></param>
        /// <param name="useColor"></param>
        /// <returns></returns>
        public static KinectImage TakeKinectSnapshot(bool useDepth, bool useColor)
        {
            if (KinectManager.Kinect == null)
                return null;

            KinectImage image = new KinectImage();

            image.Timestamp = DateTime.Now;

            if (useColor)
            {
                if (KinectManager.ColorData != null)
                {
                    image.HasColor = true;
                    image.ColorData = KinectManager.ColorData.Clone() as byte[];
                }
                else
                {
                    image.HasColor = false;
                }
            }
            else
            {
                image.HasColor = false;
            }

            if (useDepth)
            {
                if (KinectManager.DepthFrameData != null)
                {
                    image.HasDepth = true;
                    image.DepthData = KinectManager.DepthFrameData.Clone() as ushort[];
                }
                else
                {
                    image.HasDepth = false;
                }
            }
            else
            {
                image.HasDepth = false;
            }

            return image;
        }

        public static int StoreImage(KinectImage image, string tablename, bool closeConnection = true)
        {
            int id = 0;

            if (image == null)
                return id;

            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;

            command.CommandText = "INSERT INTO " + tablename + " (timestamp, hascolor, hasdepth, colordata, depthdata, colorformat, depthformat) VALUES (:timestamp, :hascolor, :hasdepth, :colordata, :depthdata, :colorformat, :depthformat) RETURNING uid;";

            command.Parameters.Add("timestamp", NpgsqlTypes.NpgsqlDbType.Timestamp).Value = image.Timestamp;
            command.Parameters.Add("hascolor", NpgsqlTypes.NpgsqlDbType.Boolean).Value = image.HasColor;
            command.Parameters.Add("hasdepth", NpgsqlTypes.NpgsqlDbType.Boolean).Value = image.HasDepth;
            command.Parameters.Add("colordata", NpgsqlTypes.NpgsqlDbType.Bytea).Value = image.CompressedColorData;
            command.Parameters.Add("depthdata", NpgsqlTypes.NpgsqlDbType.Bytea).Value = image.CompressedDepthData;
            command.Parameters.Add("colorformat", NpgsqlTypes.NpgsqlDbType.Smallint).Value = (int)ColorImageFormat.Bgra;
            command.Parameters.Add("depthformat", NpgsqlTypes.NpgsqlDbType.Smallint).Value = (int)ColorImageFormat.Bgra;

            try
            {
                id = (int)command.ExecuteScalar(); // Execute the query, returning the UID calculated by the database.
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }

            if (closeConnection)
                DatabaseManager.CloseConnection();

            return id;  // Return the calculated UID of the KinectImage so that it can be utilized in a database relationship.
        }

        public static KinectImage RetreiveImage(int imageid, string tablename)
        {
            if (imageid <= 0)
                return null;

            KinectImage image = new KinectImage();

            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = @"SELECT timestamp, hascolor, hasdepth, colordata, depthdata FROM " + tablename + " WHERE uid = :uid";

            command.Parameters.Add("uid", NpgsqlTypes.NpgsqlDbType.Integer).Value = imageid;

            DateTime timestamp = DateTime.MinValue;
            bool hascolor = false;
            bool hasdepth = false;
            byte[] colordata = null;
            byte[] depthdata = null;

            try
            {
                NpgsqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    try
                    {
                        timestamp = reader.GetTimeStamp(reader.GetOrdinal("timestamp"));
                    }
                    catch (Exception e)
                    {
                    }
                    try
                    {
                        hascolor = reader.GetBoolean(reader.GetOrdinal("hascolor"));
                    }
                    catch (Exception e)
                    {
                    }
                    try
                    {
                        hasdepth = reader.GetBoolean(reader.GetOrdinal("hasdepth"));
                    }
                    catch (Exception e)
                    {
                    }
                    try
                    {
                        colordata = reader.GetValue(reader.GetOrdinal("colordata")) as byte[];
                    }
                    catch (Exception e)
                    {
                    }
                    try
                    {
                        depthdata = reader.GetValue(reader.GetOrdinal("depthdata")) as byte[];
                    }
                    catch (Exception e)
                    {
                    }
                }
                reader.Dispose();
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print("KinectImage RetreiveImage ERROR: {0}", exc.Message);
            }

            DatabaseManager.CloseConnection();

            image.Timestamp = timestamp;
            image.HasColor = hascolor;
            image.HasDepth = hasdepth;
            image.CompressedColorData = colordata;
            image.CompressedDepthData = depthdata;

            return image;
        }

        /// <summary>
        /// Retrieve a dictionary of KinectImages from the Images Table using an integer array of Image IDs.
        /// Key to dictionary is the imageID.
        /// </summary>
        /// <param name="ImageIDs">Collection of image ids used to identify the kinect images.</param>
        /// <param name="Tablename">Name of the images table to retrieve from.</param>
        /// <returns>Dictionary of KinectImages keyd by the imageID used to retrieve the image.</returns>
        public static Dictionary<int, KinectImage> RetreiveImages(int[] ImageIDs, string Tablename)
        {
            if (ImageIDs.Length == 0)
                return null;

            Dictionary<int, KinectImage> images_dict = new Dictionary<int, KinectImage>();

            DatabaseManager.OpenConnection();

            NpgsqlCommand command = DatabaseManager.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = @"SELECT uid, timestamp, hascolor, hasdepth, colordata, depthdata FROM " + Tablename + " WHERE uid IN (";

            string uidClause = "";
            for (int i = 0; i < ImageIDs.Length; i++)
            {
                uidClause += String.Format("'{0}'", ImageIDs[i]);
                if (i < ImageIDs.Length - 1)
                    uidClause += ",";
            }
            command.CommandText += uidClause + ") ORDER BY uid;";

            try
            {
                using(NpgsqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        KinectImage image = new KinectImage();

                        image.Timestamp = reader.GetTimeStamp(reader.GetOrdinal("timestamp"));
                        image.HasColor = reader.GetBoolean(reader.GetOrdinal("hascolor"));
                        image.HasDepth = reader.GetBoolean(reader.GetOrdinal("hasdepth"));
                        image.CompressedColorData = reader.GetValue(reader.GetOrdinal("colordata")) as byte[];
                        image.CompressedDepthData = reader.GetValue(reader.GetOrdinal("depthdata")) as byte[];

                        // Add to dictionary (ImageID, KinectImage)
                        images_dict.Add(reader.GetInt32(reader.GetOrdinal("uid")), image);
                    }
                }
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print("KinectImage RetreiveImages ERROR: {0}", exc.Message);
            }

            DatabaseManager.CloseConnection();

            return images_dict;
        }

        #region Compress and Decompress byte[] objects.
        private static byte[] Compress(byte[] buffer)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream zip = new GZipStream(ms, CompressionMode.Compress, true);
            zip.Write(buffer, 0, buffer.Length);
            zip.Close();
            ms.Position = 0;

            MemoryStream outStream = new MemoryStream();

            byte[] compressed = new byte[ms.Length];
            ms.Read(compressed, 0, compressed.Length);

            byte[] gzBuffer = new byte[compressed.Length + 4];
            Buffer.BlockCopy(compressed, 0, gzBuffer, 4, compressed.Length);
            Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gzBuffer, 0, 4);
            return gzBuffer;
        }

        public static byte[] Decompress(byte[] gzBuffer)
        {
            MemoryStream ms = new MemoryStream();
            int msgLength = BitConverter.ToInt32(gzBuffer, 0);
            ms.Write(gzBuffer, 4, gzBuffer.Length - 4);

            byte[] buffer = new byte[msgLength];

            ms.Position = 0;
            GZipStream zip = new GZipStream(ms, CompressionMode.Decompress);
            zip.Read(buffer, 0, buffer.Length);

            return buffer;
        }
        #endregion
    }
}
