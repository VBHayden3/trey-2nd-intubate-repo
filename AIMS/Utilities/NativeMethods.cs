﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows;

namespace AIMS
{
    public static class NativeMethods
    {

        public partial class MouseOperations
        {

            [DllImport("user32.dll")]
            public static extern bool SetCursorPos(int x, int y);


            [DllImport("user32.dll")]

            public static extern bool GetCursorPos(out Point pt);
        }

    }
}
