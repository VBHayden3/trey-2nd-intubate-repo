﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows;

namespace AIMS
{
    public class SerializationManager
    {
        public static byte[] ObjectToByteArray(object obj, MemoryStream memoryStream, BinaryFormatter binaryFormatter)
        {
            try
            {
                // Serializes an object, or graph of connected objects, to the given stream.
                binaryFormatter.Serialize(memoryStream, obj);

                // convert stream to byte array and return
                return memoryStream.ToArray();
            }
            catch (Exception _Exception)
            {
                // Error
                Console.WriteLine("Exception caught in process: {0}", _Exception.ToString());
                MessageBox.Show(_Exception.ToString());
            }

            // Error occured, return null
            return null;
        }
    }
}
