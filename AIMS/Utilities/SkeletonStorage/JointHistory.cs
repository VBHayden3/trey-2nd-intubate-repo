﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using Microsoft.Kinect;

namespace AIMS.Utilities.SkeletonStorage
{
    /// <summary>
    /// Enumerator to clearly identify trends.
    /// </summary>
    public enum Trend
    {
        None,
        Positive,
        Negative
    };

    /// <summary>
    /// Struct to encapsulate Trends in 3 dimensions; X, Y, and Z.
    /// </summary>
    public struct Trend3D
    {
        #region Public Fields

        public Trend X;
        public Trend Y;
        public Trend Z;

        #endregion

        #region Constructor

        public Trend3D(Trend X, Trend Y, Trend Z)
        {
            this.X = X;
            this.Y = Y;
            this.Z = Z;
        }

        #endregion

        #region Overriden ToString Function

        public override string ToString()
        {
            string outputString = String.Format("({0}, {1}, {2})", this.X, this.Y, this.Z);
            return outputString;
        }

        #endregion
    };

    /// <summary>
    /// Class to collect history of JointExtended objects. Joints to track are specified by JointType 
    /// and updated with a Microsoft.Kinect.Skeleton; should be updated every frame.
    /// Provides trending for specified portions of the history.
    /// </summary>
    class JointHistory
    {
        #region Public Properties

        /// <summary>
        /// Length of history to store for each Joint. WARNING: If HistoryLength is altered, all previous history is lost.
        /// </summary>
        public int HistoryLength { get { return this.historyLength; } set { this.historyLength = value > 0 ? value : 1; this.UpdateJointHistSize(); } }

        /// <summary>
        /// Get an array of joint history for a joint specified by a JointType index.
        /// </summary>
        /// <param name="joint">Joint to retrieve history of.</param>
        /// <returns>Joint array of size JointHistory.HistoryLength.</returns>
        public JointExtended[] this[JointType joint] { get 
            {
                try { return (JointExtended[])this.jointHist[(JointType)joint].Clone(); }
                catch (Exception exc) 
                {
                    System.Diagnostics.Debug.Print("JointHistory[JointType Joint] ERROR:", exc.Message);
                    return null; 
                }
            }
        }

        /// <summary>
        /// Get an array of joint history up to a specified depth for a joint specified by a JointType index and depth.
        /// </summary>
        /// <param name="Joint">Joint to retrieve history of.</param>
        /// <param name="Depth">Amount of history to return, starting from the newest to depth.</param>
        /// <returns>Joint array of size Depth, unless depth is greater than HistoryLength, then size HistoryLength,
        /// or less than / equal to 0, then size 1.</returns>
        public JointExtended[] this[JointType Joint, int Depth]
        {
            get
            {
                // Ensure Depth is a usable value. If greater than historyLength, set to historyLength. If <= 0, set to 1.
                if (Depth > this.historyLength)
                    Depth = this.historyLength;

                else if (Depth <= 0)
                    Depth = 1;

                try
                {
                    // Create array to return and copy over values.
                    JointExtended[] joints = new JointExtended[Depth];
                    for (int i = 0; i < Depth; i++)
                        joints[i] = this.jointHist[Joint][i];

                    return joints;
                }
                catch (Exception exc)
                {
                    System.Diagnostics.Debug.Print("JointHistory[JointType Joint, int Depth] ERROR: {0}", exc.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Get collection of joints being tracked by JointHistory.
        /// </summary>
        /// <returns>List of JointType, unsorted.</returns>
        public List<JointType> TrackedJoints { get { return new List<JointType>(this.trackedJoints); } }

        #endregion

        #region Private Fields

        /// <summary>
        /// Collection of joints to track.
        /// </summary>
        private List<JointType> trackedJoints;
        /// <summary>
        /// History of a given joint.
        /// </summary>
        private Dictionary<JointType, JointExtended[]> jointHist;
        /// <summary>
        /// Length of Joint History. Defaults to 60
        /// </summary>
        private int historyLength = 60;

        // Flag whether object has been disposed or not.
        private bool disposed;

        #endregion

        #region Constructor / Destructor

        public JointHistory()
        {
            // Initialize trackedJoints list to size 8 ( grows if more joints are added ).
            this.trackedJoints = new List<JointType>(8);
            // Initialize jointHist, will need a new Joint[] for every JointType key added. Initial size of 8, grows if more added.
            this.jointHist = new Dictionary<JointType, JointExtended[]>(8);
        }

        #endregion

        #region Public Functions
        #region Add Remove and Update Functions

        /// <summary>
        /// Specify a Joint to track, adds to a collection of tracked joints.
        /// </summary>
        /// <param name="Joint">Joint to track.</param>
        /// <returns>True: Joint was added. False: Joint was not added ( it was already being tracked ).</returns>
        public bool AddJoint(JointType Joint)
        {
            try
            {
                // Make sure not already tracking Joint.
                foreach (JointType joint in this.trackedJoints)
                {
                    if (joint == Joint)
                        return false;
                }
                // Add to trackedJoints if not already being tracked
                this.trackedJoints.Add(Joint);
                // Initialize spot in jointHist Dictionary.
                this.jointHist.Add(Joint, new JointExtended[this.historyLength]);
                return true;
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print("JointHistory AddJoint ERROR: {0}", exc.Message);
                return false;
            }
        }

        /// <summary>
        /// Specify a joint to stop tracking. Removes joint from a collection of tracked joints.
        /// </summary>
        /// <param name="Joint">Joint to stop tracking.</param>
        /// <returns>True: Joint was removed. False: Joint was not removed ( it was not being tracked ).</returns>
        public bool RemoveJoint(JointType Joint)
        {
            try
            {
                // Look for joint to remove
                for (int i = 0; i < this.trackedJoints.Count; i++)
                {
                    if (this.trackedJoints[i] == Joint)
                    {
                        // Joint was found, remove it from trackedJoints and jointHist, return true.
                        this.trackedJoints.RemoveAt(i);
                        this.jointHist.Remove(Joint);
                        return true;
                    }
                }

                // Joint was not found, return false.
                return false;
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print("JointHistory RemoveJoint ERROR: {0}", exc.Message);
                return false;
            }
        }

        /// <summary>
        /// Updates the history of tracked joints from provided skeleton.
        /// </summary>
        /// <param name="skel">Skeleton used to update skeleton joint history.</param>
        public void UpdateJointHistory(Body skel)
        {
            // Make room in jointHist for next point.
            this.PushJointHist();

            // Update jointHist from passed Skeleton.
            foreach (JointType jointType in this.trackedJoints)
            {
                try
                {
                    this.jointHist[jointType][0] = new JointExtended(skel.Joints[jointType]);
                    if (this.historyLength > 1)
                    {
                        // If older history exists, update trend of joint with that history.
                        if (this.jointHist[jointType][1] != null)
                            this.jointHist[jointType][0].UpdateTrend(this.jointHist[jointType][1].Position);
                    }
                }
                catch (Exception exc)
                {
                    System.Diagnostics.Debug.Print("JointHistory UpdateJointHistory ERROR: {0}", exc.Message);
                }
            }
        }

        #endregion

        #region Trending Functions
        /// <summary>
        /// Discovers the trend in X, Y, and Z over the specified inclusive range.
        /// </summary>
        /// <param name="Joint">Joint to get trend of.</param>
        /// <param name="StartOfRange">Inclusive start of range. Cannot be less than 0. Defaults to 0</param>
        /// <param name="EndOfRange">Inclusive end of range. Cannot be greater than or equal to HistoryLength. Defaults to HistoryLength - 1</param>
        /// <returns>Trend3D struct of trends in X, Y, and Z dimensions.</returns>
        public Trend3D TrendOverRange(JointType Joint, int StartOfRange, int EndOfRange)
        {
            // Trend counters to discover trend over range.
            int positiveTrendsX = 0;
            int negativeTrendsX = 0;
            int noneTrendsX = 0;
            int positiveTrendsY = 0;
            int negativeTrendsY = 0;
            int noneTrendsY = 0;
            int positiveTrendsZ = 0;
            int negativeTrendsZ = 0;
            int noneTrendsZ = 0;

            // Make sure parameters are within range.
            if (StartOfRange < 0 || StartOfRange >= this.historyLength)
            {
                System.Diagnostics.Debug.Print("StartOfRange cannot be less than 0 or greater than or equal to HistoryLength.");
                return new Trend3D(Trend.None, Trend.None, Trend.None);
            }
            // Throw OutOfRange exception
            if (EndOfRange >= this.historyLength || EndOfRange < 0)
            {
                System.Diagnostics.Debug.Print("EndOfrange cannot be greater than or equal to HistoryLength or less than 0.");
                return new Trend3D(Trend.None, Trend.None, Trend.None);
            }
            // Make sure range is from front to back
            if (StartOfRange > EndOfRange)
            {
                System.Diagnostics.Debug.Print("StartOfRange cannot be greater than EndOfRange.");
                return new Trend3D(Trend.None, Trend.None, Trend.None);
            }

            // Dimension trends to return.
            Trend trendX, trendY, trendZ;

            // Double check that Joint is actually being tracked
            if (!this.IsJointTracked(Joint))
            {
                System.Diagnostics.Debug.Print("Joint to trend is not being tracked.");
                return new Trend3D(Trend.None, Trend.None, Trend.None);
            }

            try
            {
                // Iterate over inclusive range, count up trends.
                for (int rangeIndx = StartOfRange; rangeIndx <= EndOfRange; rangeIndx++)
                {
                    // Make sure the JointExtended exists
                    if (this.jointHist[Joint][rangeIndx] != null)
                    {
                        // Update trend counters for X dimension.
                        switch (this.jointHist[Joint][rangeIndx].TrendX)
                        {
                            case Trend.None:
                                noneTrendsX++;
                                break;
                            case Trend.Positive:
                                positiveTrendsX++;
                                break;
                            case Trend.Negative:
                                negativeTrendsX++;
                                break;
                        }
                        // Update trend counters for Y dimension.
                        switch (this.jointHist[Joint][rangeIndx].TrendY)
                        {
                            case Trend.None:
                                noneTrendsY++;
                                break;
                            case Trend.Positive:
                                positiveTrendsY++;
                                break;
                            case Trend.Negative:
                                negativeTrendsY++;
                                break;
                        }
                        // Update trend counters for Z dimension.
                        switch (this.jointHist[Joint][rangeIndx].TrendZ)
                        {
                            case Trend.None:
                                noneTrendsZ++;
                                break;
                            case Trend.Positive:
                                positiveTrendsZ++;
                                break;
                            case Trend.Negative:
                                negativeTrendsZ++;
                                break;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print("JointHistory TrendOverRange ERROR: {0}", exc.Message);
                throw;
            }

            // Discover trend in X.
            if (positiveTrendsX > negativeTrendsX && positiveTrendsX > noneTrendsX)
                trendX = Trend.Positive;

            else if (negativeTrendsX > positiveTrendsX && negativeTrendsX > noneTrendsX)
                trendX = Trend.Negative;

            else
                trendX = Trend.None;

            // Discover trend in Y.
            if (positiveTrendsY > negativeTrendsY && positiveTrendsY > noneTrendsY)
                trendY = Trend.Positive;

            else if (negativeTrendsY > positiveTrendsY && negativeTrendsY > noneTrendsY)
                trendY = Trend.Negative;

            else
                trendY = Trend.None;

            // Discover trend in Z.
            if (positiveTrendsZ > negativeTrendsZ && positiveTrendsZ > noneTrendsZ)
                trendZ = Trend.Positive;

            else if (negativeTrendsZ > positiveTrendsZ && negativeTrendsZ > noneTrendsZ)
                trendZ = Trend.Negative;

            else
                trendZ = Trend.None;

            // Return Trend3D struct comprised of discovered trends.
            return new Trend3D(trendX, trendY, trendZ);
        }

        /// <summary>
        /// Discovers the trend in X, Y, and Z over the entire history of joints for supplied JointType.
        /// </summary>
        /// <param name="Joint">JointType to get trend of.</param>
        /// <returns>Trend3D struct of trends in X, Y, and Z dimensions.</returns>
        public Trend3D TrendOfAll(JointType Joint)
        {
            return this.TrendOverRange(Joint, 0, this.historyLength);
        }

        /// <summary>
        /// Discovers the trend in X, Y, and Z over RangeLength points starting from the newest point.
        /// </summary>
        /// <param name="Joint">JointType to get trend of.</param>
        /// <param name="RangeLength">Amount of joints to analyze trend of.</param>
        /// <returns>Trend3D struct of trends in X, Y, and Z dimensions.</returns>
        public Trend3D TrendFromLatest(JointType Joint, int RangeLength)
        {
            return this.TrendOverRange(Joint, 0, RangeLength - 1);
        }

        #endregion

        #region IsJointTracked and ToString Functions

        /// <summary>
        /// Returns whether or not a JointType is tracked by JointHistory.
        /// </summary>
        /// <param name="JointType"></param>
        /// <returns>True: Joint is being tracked. False: Joint is not being tracked.</returns>
        public bool IsJointTracked(JointType JointType)
        {
            foreach (JointType jointType in this.trackedJoints)
            {
                if (jointType == JointType)
                    return true;
            }
            return false;
        }

        

        /// <summary>
        /// Returns a string containing all tracked joints and their indexed history.
        /// </summary>
        /// <returns>String of all tracked joints and their indexed history.</returns>
        public override string ToString()
        {
            string output = "";

            foreach (JointType jointType in this.trackedJoints)
            {
                output += String.Format("Joint: {0} - ", jointType);

                for (int i = 0; i < this.jointHist[jointType].Length; i++)
                {
                    // Display index and X,Y,Z in mm and Skeletal Space with up to 2 decimal places.
                    CameraSpacePoint jointPos = this.jointHist[jointType][i].Position;
                    output += String.Format("[{0}]({1:#.##},{2:#.##},{3:#.##})", i, jointPos.X * 1000, jointPos.Y * 1000, jointPos.Z * 1000);
                }
                output += "\n";
            }

            return output;
        }

        #endregion
        #endregion // end Public Functions

        #region Private Functions

        /// <summary>
        /// Updates the length of the history associated with each tracked joint stored in jointHist.
        /// </summary>
        private void UpdateJointHistSize()
        {
            foreach (JointType joint in this.trackedJoints)
            {
                try
                {
                    this.jointHist.Remove(joint);
                    this.jointHist.Add(joint, new JointExtended[this.historyLength]);
                }
                catch (Exception exc)
                {
                    System.Diagnostics.Debug.Print("JointHistory UpdateJointHistSize ERROR: {0}  historyLength: {1}", exc.Message, this.historyLength);
                }
            }
        }

        /// <summary>
        /// Push the joint positions down the jointHist array.
        /// </summary>
        /// <param name="indx">Specifies from where to push history down. Defaults at index 0.</param>
        private void PushJointHist(int indx = 0)
        {
            try
            {
                if (indx < this.historyLength - 1)
                {
                    // Recursive call to update older joint positions
                    this.PushJointHist(indx + 1);

                    foreach (JointType jointType in this.trackedJoints)
                    {
                        // Set the joint position directly older than this one, to this one.
                        if (this.jointHist[jointType][indx] != null)
                            this.jointHist[jointType][indx + 1] = this.jointHist[jointType][indx];
                    }
                }
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print("JointHistory PushJointHist ERROR: {0}", exc.Message);
            }
        }

        #endregion
    }

    /// <summary>
    /// Class to extend the Microsoft.Kinect.Joint Struct. 
    /// Adds Trends in 3 dimensions and Time at which JointExtended was initialized.
    /// </summary>
    public class JointExtended
    {
        #region Public Properties
        
        /// <summary>
        /// Gets the joint type, which identifies a portion of the skeleton.
        /// </summary>
        public JointType JointType { get { return this.joint.JointType; } }
        /// <summary>
        /// Gets the CameraSpacePoint position of the joint.
        /// </summary>
        public CameraSpacePoint Position { get { return this.joint.Position; } }
        /// <summary>
        /// Gets the tracking state of the joint, which indicates the quality of the joint position that 
        /// was produced by the skeletonstream.
        /// </summary>
        public TrackingState TrackingState { get { return this.joint.TrackingState; } }
        /// <summary>
        /// Gets the time of which the joint was declared.
        /// </summary>
        public DateTime Time { get { return this.time; } }
        /// <summary>
        /// Gets the trend of the joint in the X dimension compared to another point.
        /// </summary>
        public Trend TrendX { get { return this.trendX; } }
        /// <summary>
        /// Gets the trend of the joint in the Y dimension compared to another point.
        /// </summary>
        public Trend TrendY { get { return this.trendY; } }
        /// <summary>
        /// Gets the trend of the joint in the Z dimension compared to another point.
        /// </summary>
        public Trend TrendZ { get { return this.trendZ; } }
        /// <summary>
        /// Gets a 3-dimensional trend Struct which specifies trends in X, Y, and Z compared to another point.
        /// </summary>
        public Trend3D Trends { get { return new Trend3D(this.trendX, this.trendY, this.trendZ); } }

        #endregion

        #region Private Fields

        // Joint for which this class extends.
        private Joint joint;
        // Trend of the X position for this joint.
        private Trend trendX;
        // Trend of the Y position for this joint.
        private Trend trendY;
        // Trend of the Z position for this joint.
        private Trend trendZ;
        // Time at which this extended joint was declared.
        private DateTime time;
        // A trend can only exist if the compared difference is greater than this threshold.
        private float trendThreshold = 0.0035f; // 0.0035 m or 3.5 mm

        #endregion

        #region Constructor

        /// <summary>
        /// Create a JointExtended object. Contains all accessable properties of a Joint Struct as well as 
        /// Trend propreties for X, Y, Z and a Time property of when the JointExtended was initialized.
        /// </summary>
        /// <param name="Joint">Joint to inherit all Joint information from.</param>
        /// <param name="PreviousPoint">CameraSpacePoint from previous Joint to perform Trend analysis. 
        /// If none specified, Trends will default to Trend.None.</param>
        public JointExtended(Joint Joint, CameraSpacePoint? PreviousPoint = null)
        {
            this.joint = Joint;

            // Set time to when this joint was 'made'
            this.time = DateTime.Now;

            // If Previous Point was passed in, figure out trend
            if (PreviousPoint.HasValue)
            {
                this.trendX = this.DiscoverTrend(this.joint.Position.X, PreviousPoint.Value.X);
                this.trendY = this.DiscoverTrend(this.joint.Position.Y, PreviousPoint.Value.Y);
                this.trendZ = this.DiscoverTrend(this.joint.Position.Z, PreviousPoint.Value.Z);
            }
            // Otherwise default to none
            else
            {
                this.trendX = Trend.None;
                this.trendY = Trend.None;
                this.trendZ = Trend.None;
            }
        }

        #endregion

        #region Public Functions

        /// <summary>
        /// Update joints trend by comparing aganist supplied older point.
        /// </summary>
        /// <param name="Point">Older point to compare joint against, used to discover trend.</param>
        public void UpdateTrend(CameraSpacePoint Point)
        {
            // Set trends.
            this.trendX = this.DiscoverTrend(this.joint.Position.X, Point.X);
            this.trendY = this.DiscoverTrend(this.joint.Position.Y, Point.Y);
            this.trendZ = this.DiscoverTrend(this.joint.Position.Z, Point.Z);
        }

        /// <summary>
        /// Returns a string with X, Y, Z positions.
        /// </summary>
        /// <returns>String of X Y Z positions.</returns>
        public override string ToString()
        {
            return String.Format("({0}, {1}, {2}", this.Position.X, this.Position.Y, this.Position.Z);
        }

        #endregion

        #region Private Functions

        /// <summary>
        /// Compares two float values and returns trend of those values.
        /// </summary>
        /// <param name="Current">Newer value to compare.</param>
        /// <param name="Past">Older value to compare.</param>
        /// <returns>If the difference is within trendThreshold, Trend.None is returned. 
        /// If the difference is positive, Trend.Positive is returned. 
        /// If the difference is negative, Trend.Negative is returned.</returns>
        private Trend DiscoverTrend(float Current, float Past)
        {
            // Difference between Current and Past are within trendThresold, so no trend exists.
            if (System.Math.Abs(Current - Past) <= this.trendThreshold)
                return Trend.None;

            // Current is greater than Past, so the trend is positive.
            else if (Current > Past)
                return Trend.Positive;

            // Past is greater than Current, so the trend is negative. 
            // ( Past cannot be equal to Current, it would be within the trendThreshold. )
            else
                return Trend.Negative;
        }

        #endregion
    }
}
