﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Configuration;
using System.Diagnostics;

namespace AIMS.Utilities
{
    public static class SystemSettings
    {
        static SystemSettings()
        {
            //SetConfigEncryption();
        }

        public static string DatabaseIP
        {
            get
            {
                return ReadConnectionString("AIMS.Properties.Settings.DatabaseIP");
            }
            set
            {
                WriteConnectionString("AIMS.Properties.Settings.DatabaseIP", value);
            }
        }


        public static string DatabaseName
        {
            get
            {
                return ReadConnectionString("AIMS.Properties.Settings.DatabaseName");
            }
            set
            {
                WriteConnectionString("AIMS.Properties.Settings.DatabaseName", value);
            }
        }


        public static string DatabaseUsername
        {
            get
            {
                return ReadConnectionString("AIMS.Properties.Settings.DatabaseUsername");
            }
            set
            {
                WriteConnectionString("AIMS.Properties.Settings.DatabaseUsername", value);
            }
        }


        public static string DatabasePassword
        {
            get
            {
                return ReadConnectionString("AIMS.Properties.Settings.DatabasePassword");
            }
            set
            {
                WriteConnectionString("AIMS.Properties.Settings.DatabasePassword", value);
            }
        }


        public static string DatabasePortNumber
        {
            get
            {
                return ReadConnectionString("AIMS.Properties.Settings.DatabasePortNumber");
            }
            set
            {
                WriteConnectionString("AIMS.Properties.Settings.DatabasePortNumber", value);
            }
        }


        public static void SetConfigEncryption()
        {
            try
            {
                // Open the configuration file and retrieve the connectionStrings section.
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                ConnectionStringsSection section = config.GetSection("connectionStrings") as ConnectionStringsSection;

                if (!(section.SectionInformation.IsProtected))
                {
                    // Encrypt the section - DPAPI (Data Protection Application Programming Interface) is a simple cryptographic
                    // application programming interface available as a built-in component in Windows (http://en.wikipedia.org/wiki/Data_Protection_API)
                    section.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");

                    // Save the current configuration
                    config.Save();
                    ConfigurationManager.RefreshSection("connectionStrings");
                }

                Debug.WriteLine("SetConfigEncryption::" + section.SectionInformation.IsProtected);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                System.Windows.MessageBox.Show(ex.Message);
            }

        }


        // For testing purposes only
        public static void RemoveConfigEncryption()
        {
            try
            {
                // Open the configuration file and retrieve the connectionStrings section
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                ConnectionStringsSection section = config.GetSection("connectionStrings") as ConnectionStringsSection;

                if (section.SectionInformation.IsProtected)
                {
                    // Remove encryption.
                    section.SectionInformation.UnprotectSection();

                    // Save the current configuration.
                    config.Save();
                    ConfigurationManager.RefreshSection("connectionStrings");
                }

                Debug.WriteLine("RemoveConfigEncryption::" + section.SectionInformation.IsProtected);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                System.Windows.MessageBox.Show(ex.Message);
            }
        }


        // View the ConnectionStrings section         
        // This function uses the ConnectionStrings property to read the connectionStrings configuration section
        public static void ViewConnectionStrings()
        {
            // Open the configuration file and retrieve the connectionStrings section
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            ConnectionStringSettingsCollection connections = config.ConnectionStrings.ConnectionStrings;

            if (connections.Count != 0)
            {
                Debug.WriteLine("ViewConnectionStrings::Connection Strings:");

                // Get the collection elements
                foreach (ConnectionStringSettings connection in connections)
                {
                    string name = connection.Name;
                    string provider = connection.ProviderName;
                    string connectionString = connection.ConnectionString;

                    Debug.WriteLine("\tName: " + name);
                    Debug.WriteLine("\tConnection String: " + connectionString);
                    Debug.WriteLine("\tProvider: " + provider);
                }
            }
            else
            {
                Debug.WriteLine("No connection string is defined.");
            }
        }


        // Read a specific ConnectionString value
        public static string ReadConnectionString(string connectionString)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            string property = config.ConnectionStrings.ConnectionStrings[connectionString].ToString();

            Debug.WriteLine("ReadConnectionString::" + connectionString + ": " + property);
            return property;
        }


        // Write a specific ConnectionString value
        public static void WriteConnectionString(string connectionString, string value)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            ConnectionStringSettings connection = config.ConnectionStrings.ConnectionStrings[connectionString];

            connection.ConnectionString = value;
            config.Save();
            ConfigurationManager.RefreshSection("connectionStrings");

            Debug.WriteLine("WriteConnectionString::Name: " + connection.Name);
            Debug.WriteLine("WriteConnectionString::Provider: " + connection.ProviderName);
            Debug.WriteLine("WriteConnectionString::Connection String: " + connection.ConnectionString);
        }


        #region Test and Debug Functions

        private static void ToggleConfigEncryption()
        {
            try
            {
                // Open the configuration file and retrieve the connectionStrings section
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                ConnectionStringsSection section = config.GetSection("connectionStrings") as ConnectionStringsSection;

                if (section.SectionInformation.IsProtected)
                {
                    // Remove encryption.
                    section.SectionInformation.UnprotectSection();
                }
                else
                {
                    // Encrypt the section.
                    section.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                }

                // Save the current configuration.
                config.Save();
                ConfigurationManager.RefreshSection("connectionStrings");

                Debug.WriteLine("ToggleConfigEncryption::" + section.SectionInformation.IsProtected);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        // This function shows how to read the key/value pairs (settings collection) contained in the appSettings section. 
        private static void ReadAppSettingsTest()
        {
            try
            {
                // Get the configuration file.
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                // Get the appSettings section.
                AppSettingsSection appSettings = (AppSettingsSection)config.GetSection("appSettings");

                // Get the auxiliary file name.
                Console.WriteLine("Auxiliary file: {0}", config.AppSettings.File);

                // Get the settings collection (key/value pairs). 
                if (appSettings.Settings.Count != 0)
                {
                    foreach (string key in appSettings.Settings.AllKeys)
                    {
                        string value = appSettings.Settings[key].Value;
                        Console.WriteLine("Key: {0} Value: {1}", key, value);
                    }
                }
                else
                    Console.WriteLine("The appSettings section is empty. Write first.");
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception raised: {0}", e.Message);
            }
        }

        // Get the ConnectionStrings section       
        // This function uses the ConnectionStrings property to read the connectionStrings configuration section
        private static void ReadConnectionStringTest()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            string databaseIP = config.ConnectionStrings.ConnectionStrings["AIMS.Properties.Settings.DatabaseIP"].ToString();
            ConnectionStringSettings databaseIPConnection = config.ConnectionStrings.ConnectionStrings["AIMS.Properties.Settings.DatabaseIP"];

            string name = databaseIPConnection.Name;
            string provider = databaseIPConnection.ProviderName;
            string connectionString = databaseIPConnection.ConnectionString;

            Debug.WriteLine("databaseIP: " + databaseIP);
            Debug.WriteLine("Name: " + name);
            Debug.WriteLine("Connection string: " + connectionString);
            Debug.WriteLine("Provider: " + provider);
        }

        private static void WriteConnectionStringTest()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            string databaseIP = config.ConnectionStrings.ConnectionStrings["AIMS.Properties.Settings.DatabaseIP"].ToString();
            ConnectionStringSettings databaseIPConnection = config.ConnectionStrings.ConnectionStrings["AIMS.Properties.Settings.DatabaseIP"];

            databaseIPConnection.ConnectionString = "127.0.0.1";
            config.Save();
            ConfigurationManager.RefreshSection("connectionStrings");

            string name = databaseIPConnection.Name;
            string provider = databaseIPConnection.ProviderName;
            string connectionString = databaseIPConnection.ConnectionString;

            Debug.WriteLine("databaseIP: " + databaseIP);
            Debug.WriteLine("Name: " + name);
            Debug.WriteLine("Connection string: " + connectionString);
            Debug.WriteLine("Provider: " + provider);
        }

        #endregion


    } // End class SystemSettings

} // End Namespace
