﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Diagnostics;

using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit.BackgroundRemoval;

using AIMS.Utilities.Kinect;

namespace AIMS.Core.ViewModels
{
    class BackgroundRemovedColorStreamViewModel : ViewModelBase
    {
        #region Fields

        #endregion // Fields

        #region Constructor

        /// <summary>
        /// Creates a ColorStreamViewModel from a ColorImageStream.
        /// </summary>
        public BackgroundRemovedColorStreamViewModel()
        {
            base.DisplayName = "Background Removed Color Stream";

            KinectManager.BackgroundRemovedColorDataReady += KinectManager_BackgroundRemovedColorDataReady;
        }

        #endregion // Constructor

        #region Public Interface

        /** Data **/

        /// <summary>
        /// WriteableBitmap of color data from the color stream.
        /// </summary>
        public WriteableBitmap BackgroundRemovedColorBitmap { get; private set; }

        /** Properties **/

        /// <summary>
        /// Property to enable or disable BackgroundRemovedColorStream.
        /// </summary>
        public bool BackgroundRemovedColorStreamEnabled
        {
            get { return KinectManager.BackgroundRemovedColorStreamEnabled; }
            set
            {
                if (KinectManager.BackgroundRemovedColorStreamEnabled == value)
                    return;

                KinectManager.BackgroundRemovedColorStreamEnabled = value;
                base.NotifyPropertyChanged("BackgroundRemovedColorStreamEnabled");
            }
        }

        #endregion // Public Interface

        #region Base Class Overrides

        protected override void OnDispose()
        {
            KinectManager.BackgroundRemovedColorDataReady -= KinectManager_BackgroundRemovedColorDataReady;
            this.BackgroundRemovedColorBitmap = null;

            base.OnDispose();
        }

        #endregion // Base Class Overrides

        #region Event Handling Methods

        void KinectManager_BackgroundRemovedColorDataReady(object sender, BackgroundRemovedColorFrameReadyEventArgs e)
        {
            // Open the frame and dispose of it when finished.
            using (var backgroundRemovedFrame = e.OpenBackgroundRemovedColorFrame())
            {
                if (backgroundRemovedFrame != null)
                {
                    // Reset bitmap if it doesn't match up to frame or it's null.
                    if (this.BackgroundRemovedColorBitmap == null ||
                        backgroundRemovedFrame.Width != this.BackgroundRemovedColorBitmap.PixelWidth ||
                        backgroundRemovedFrame.Height != this.BackgroundRemovedColorBitmap.PixelHeight)
                    {
                        this.BackgroundRemovedColorBitmap = new WriteableBitmap(backgroundRemovedFrame.Width,
                            backgroundRemovedFrame.Height, 96.0, 96.0, PixelFormats.Bgra32, null);
                    }

                    // Write the data to the bitmap.
                    this.BackgroundRemovedColorBitmap.WritePixels(
                        new Int32Rect(0, 0, this.BackgroundRemovedColorBitmap.PixelWidth, this.BackgroundRemovedColorBitmap.PixelHeight),
                        backgroundRemovedFrame.GetRawPixelData(),
                        this.BackgroundRemovedColorBitmap.PixelWidth * sizeof(int),
                        0);

                    base.NotifyPropertyChanged("BackgroundRemovedColorBitmap");
                }
            }
        }

        #endregion // Event Handling Methods
    }
}
