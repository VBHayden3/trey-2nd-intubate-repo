﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Diagnostics;

using Microsoft.Kinect;

using AIMS.Utilities.Kinect;

namespace AIMS.ViewModels
{
    class ColorStreamViewModel : ViewModelBase
    {
        #region Fields

        //private ColorImageStream _colorStream;
        //private ColorImageFormat _colorImageFormat;
        private bool _isEnabled;
        private bool _autoExposure;
        private bool _autoWhiteBalance;
        //private BacklightCompensationMode _backlightCompensationMode;
        //private PowerLineFrequency _powerLineFrequency;
        private BitmapSource _transparentBitmapSource = BitmapSource.Create(2, 2, 96.0, 96.0, PixelFormats.Indexed1,
                    new BitmapPalette(new List<Color> { Colors.Transparent }), new byte[] { 0, 0, 0, 0 }, 1);

        #endregion // Fields

        #region Constructor

        /// <summary>
        /// Creates a ColorStreamViewModel from a ColorImageStream.
        /// </summary>
        public ColorStreamViewModel()
        {
            base.DisplayName = "Color Stream";

            //if (KinectManager.HasKinect)
            //    this._colorStream = KinectManager.ColorImageStream;

            KinectManager.ColorDataReady += KinectManager_ColorDataReady;

            //KinectManager.ColorStreamPropertiesUpdated += KinectManager_ColorStreamPropertiesUpdated;

            this.UpdateProperties();
        }

        #endregion // Constructor

        #region Public Interface

        /** Color Data **/

        /// <summary>
        /// WriteableBitmap of color data from the color stream.
        /// </summary>
        public WriteableBitmap ColorBitmap { get; private set; }

        /** Color Settings **/

        /// <summary>
        /// Format of the Color Image.
        /// </summary>
        public ColorImageFormat ColorImageFormat 
        {
            get { return ColorImageFormat.Bgra; }
            private set
            {
//                if (this._colorImageFormat == value)
//                    return;

//                this._colorImageFormat = value;

                // Update the bitmap to the same resolution as the new format.
                this.UpdateBitmapResolution();

                base.NotifyPropertyChanged("ColorImageFormat");
            }
        }
        /// <summary>
        /// Status of the Color Stream.
        /// </summary>
        public bool IsEnabled 
        {
            get { return this._isEnabled; }
            private set
            {
                if (this._isEnabled == value)
                    return;

                this._isEnabled = value;

                base.NotifyPropertyChanged("IsEnabled");
            } 
        }

        /** Camera Settings **/

        /// <summary>
        /// Get or set the AutoExposer setting on or off.
        /// </summary>
        public bool AutoExposure 
        {
            get { return this._autoExposure; }
            set
            {
                if (this._autoExposure == value)
                    return;

                this._autoExposure = value;

                base.NotifyPropertyChanged("AutoExposure");
            }
        }
        /// <summary>
        /// Get or set the AutoWhiteBalance on or off.
        /// </summary>
        public bool AutoWhiteBalance 
        {
            get { return this._autoWhiteBalance; }
            set
            {
                if (this._autoWhiteBalance == value)
                    return;

                this._autoWhiteBalance = value;

                base.NotifyPropertyChanged("AutoWhiteBalance");
            }
        }
        /// <summary>
        /// Get or set the BacklightCompensationMode setting.
        /// </summary>
     
        /// <summary>
        /// Get or set the PowerLineFrequency setting. Only takes effect when AutoExposure is true.
        /// </summary>
        

        #endregion // Public Interface

        #region Base Class Overrides

        protected override void OnDispose()
        {
            KinectManager.ColorDataReady -= KinectManager_ColorDataReady;

            base.OnDispose();
        }

        #endregion // Base Class Overrides

        #region Event Handling Methods

        private void KinectManager_ColorDataReady()
        {
            if (this.ColorBitmap == null)
                this.UpdateBitmapResolution();

            try
            {
                this.ColorBitmap.WritePixels(
                    new Int32Rect(0, 0, this.ColorBitmap.PixelWidth, this.ColorBitmap.PixelHeight),
                    KinectManager.ColorData,
                    this.ColorBitmap.PixelWidth * sizeof(int),
                    0);
            }
            catch (Exception exc)
            {
                Debug.Print("Writing pixels failed... {0}", exc.ToString());
            }
            
            this.UpdateProperties();
            base.NotifyPropertyChanged("ColorBitmap");
        }

        private void KinectManager_ColorStreamPropertiesUpdated(object sender, EventArgs e)
        {
            this.UpdateProperties();
        }

        #endregion // Event Handling Methods

        #region Helper Methods

        /// <summary>
        /// Update the ColorBitmap to the same resolution as the colorstream frame.
        /// </summary>
        private void UpdateBitmapResolution()
        {
            if (KinectManager.ColorData != null)
                this.ColorBitmap = new WriteableBitmap(KinectManager.ColorWidth, KinectManager.ColorHeight, 96.0, 96.0, PixelFormats.Bgr32, null);
            else
                this.ColorBitmap = new WriteableBitmap(this._transparentBitmapSource);
        }

        /// <summary>
        /// Update properties whenever the colorstream changes.
        /// </summary>
        private void UpdateProperties()
        {
                this.IsEnabled = true;
                this.AutoExposure = true;
                this.AutoWhiteBalance = true;
        }

        #endregion // Helper Methods
    }
}
