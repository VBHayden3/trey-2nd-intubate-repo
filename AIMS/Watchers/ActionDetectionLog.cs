﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace AIMS.Watchers
{
    public class ActionDetectionLog : INotifyPropertyChanged
    {
        private ObservableCollection<DetectedAction> detectedActions = new ObservableCollection<DetectedAction>();
        public ObservableCollection<DetectedAction> DetectedActions { get { return detectedActions; } 
            private set 
            {
                if (detectedActions == value)
                    detectedActions = value;
                else
                {
                    detectedActions = value;
                    NotifyPropertyChanged(this, new PropertyChangedEventArgs("DetectedActions"));
                }
            } 
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void LogAction(DetectedAction action)
        {
            if (action == null)
                return;
            if (detectedActions == null)
                detectedActions = new ObservableCollection<DetectedAction>();
            if (detectedActions.Contains(action))
                return;

            detectedActions.Add(action);
        }

        public void LogAction( DetectableAction action, DetectableActionCategory category, DetectableActionGroup group )
        {
            if (action == null)
                return;
            detectedActions.Add( new DetectedAction(DateTime.Now, action.Name, category.Name, String.Format("[{0}]: {1} - {2} {3}.", DateTime.Now, group.Name, action.Name, action.Detected ? "Detected" : "Undetected") ) );
        }

        public void ClearLog()
        {
            if (this.DetectedActions != null)
            {
                this.DetectedActions.Clear();
                //this.DetectedActions = null;
            }

            //this.DetectedActions = new ObservableCollection<DetectedAction>();
        }

        private void NotifyPropertyChanged(ActionDetectionLog log, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(log, propertyChangedEventArgs);
            }
        }
    }
}
