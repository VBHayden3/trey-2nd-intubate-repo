﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIMS.Components;

namespace AIMS
{
    public abstract class BaseWatcher : IDisposable
    {
        private bool disposed;
        private bool running;
        private bool hasTriggered;
        private bool isTriggered;
        private DateTime lastTriggered;
        private bool triggerStateChanged;
        private string name;
        private string description;

        public bool Running { get { return running; } set { running = value; } }
        public bool HasTriggered { get { return hasTriggered; } set { hasTriggered = value; } }
        public bool IsTriggered { get { return isTriggered; } set { UpdateTrigger(value); isTriggered = value;  } }
        public DateTime LastTriggered { get { return lastTriggered; } set { lastTriggered = value; } }
        public string Name { get { return name; } set { name = value; } }
        public string Description { get { return description; } set { description = value; } }
        public bool TriggerStateChanged { get { return triggerStateChanged; } set { triggerStateChanged = value; } }

        public TrackedJoint linkedTrackedJoint;

        public delegate void UpdateWatcherDelegate( Action updateLogic );

        public delegate void ActionDetectedHandler( object sender, ActionDetectedEventArgs args );
        public event ActionDetectedHandler ActionDetected;

        public class ActionDetectedEventArgs : EventArgs
        {
            public ActionDetectedEventArgs( )
            {
                
            }
        }


        private void UpdateTrigger( bool val )
        {
            if (val != isTriggered)
                this.TriggerStateChanged = true;
            else
                this.TriggerStateChanged = false;

            if( val )
            {
                if (!this.HasTriggered)
                {
                    this.HasTriggered = true;
                }

                this.LastTriggered = DateTime.Now;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    // Dispose managed resources.
                }
            }
            // Dispose unmanaged resources.
            this.disposed = true;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
        
        public void UpdateWatcher()
        {
            OnBeforeUpdate();
            OnUpdate();
            OnAfterUpdate();
        }

        protected abstract void OnBeforeUpdate();
        protected virtual void OnUpdate()
        {
            if( triggerStateChanged && isTriggered )
            {
                if( ActionDetected != null )
                {
                    ActionDetected( this, new ActionDetectedEventArgs() );
                }
            }
        }
        protected abstract void OnAfterUpdate();
    }
}
