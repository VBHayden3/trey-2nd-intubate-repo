﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace AIMS.Watchers
{
    public class DetectableAction : INotifyPropertyChanged
    {
        private string name;

        public string Name
        {
            get { return name; }
            set
            {
                if (name == value)
                    name = value;
                else
                {
                    name = value;
                    NotifyPropertyChanged(this, new PropertyChangedEventArgs("Name"));
                }
            }
        }

        private bool detected = false;

        public bool Detected
        {
            get { return detected; }
            set
            {
                if (detected == value)
                    detected = value;
                else
                {
                    detected = value;
                    NotifyPropertyChanged(this, new PropertyChangedEventArgs("Detected"));
                }
            }
        }

        private void NotifyPropertyChanged(DetectableAction detectableAction, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(detectableAction, propertyChangedEventArgs);
            }
        }

        public DetectableAction(string name)
        {
            this.name = name;
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
