﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Media;

namespace AIMS.Watchers
{
    public class DetectableActionCategory: INotifyPropertyChanged
    {
        private DetectableActionGroup parentGroup;
        public DetectableActionGroup ParentGroup { get { return parentGroup; } }

        private string name;

        public string Name
        {
            get { return name; }
            set
            {
                if (name == value)
                    name = value;
                else
                {
                    name = value;
                    NotifyPropertyChanged(this, new PropertyChangedEventArgs("Name"));
                }
            }
        }

        private bool detected = false;

        public bool Detected
        {
            get { return detected; }
            set
            {
                if (detected == value)
                    detected = value;
                else
                {
                    detected = value;
                    NotifyPropertyChanged(this, new PropertyChangedEventArgs("Detected"));
                }
            }
        }

        private FulfillmentTypes fulfillmentType;
        public FulfillmentTypes FulfillmentType
        { 
            get { return fulfillmentType; }
            set
            {
                if (fulfillmentType == value)
                    fulfillmentType = value;
                else
                {
                    fulfillmentType = value;
                    NotifyPropertyChanged(this, new PropertyChangedEventArgs("FulfillmentType"));
                }
            }
        }

        private ObservableCollection<DetectableAction> properActions = new ObservableCollection<DetectableAction>();
        private ObservableCollection<DetectableAction> improperActions = new ObservableCollection<DetectableAction>();
        private ObservableCollection<DetectableAction> inactions = new ObservableCollection<DetectableAction>();

        public ObservableCollection<DetectableAction> ProperActions { get { return properActions; } }
        public ObservableCollection<DetectableAction> ImproperActions { get { return improperActions; } }
        public ObservableCollection<DetectableAction> Inactions { get { return inactions; } }

        public static Brush ProperTriggerBrush = App.Current.Resources["AIMSGreenBrush"] as SolidColorBrush;
        public static Brush ProperUntriggerBrush = App.Current.Resources["AIMSLightGrayBrush"] as SolidColorBrush;
        public static Brush InproperTriggerBrush = App.Current.Resources["AIMSYellowBrush"] as SolidColorBrush;
        public static Brush InproperUntriggerBrush = App.Current.Resources["AIMSLightGrayBrush"] as SolidColorBrush;
        public static Brush InactionTriggerBrush = App.Current.Resources["AIMSRedBrush"] as SolidColorBrush;
        public static Brush InactionUntriggerBrush = App.Current.Resources["AIMSLightGrayBrush"] as SolidColorBrush;


        private ActionDetectionLog detectionLog = new ActionDetectionLog();
        public ActionDetectionLog DetectionLog { get { return detectionLog; } }
        public ObservableCollection<DetectedAction> DetectionLogActions { get { return (detectionLog == null ? null : detectionLog.DetectedActions); } }

        public DetectableActionCategory(DetectableActionGroup parentGroup, string name)
        {
            this.parentGroup = parentGroup;
            this.name = name;
        }

        /// <summary>
        /// Searches through it's set of Proper, Inproper, and Inactions for the DetectableAction with the passed in name.
        /// 
        /// If found, set's it's own Detected flag to true, and additionally set's the found DetectableAction's Detected flag to true.
        /// Also, the FullfillmentType is updated to reflect which type (Proper, Inproper, or Inaction) of action was fulfilled based on the found DetectableAction.
        /// </summary>
        /// <param name="actionName"></param>
        public void DetectAction(string actionName, bool writeToLog)
        {
            DetectableAction foundAction = null;// FindActionInAny(actionName);

            foundAction = FindActionInCollection(actionName, properActions);

            if (foundAction != null)
            {
                foundAction.Detected = true;

                this.Detected = true;
                this.FulfillmentType = FulfillmentTypes.ProperAction;

                if( writeToLog )
                    this.DetectionLog.LogAction(foundAction, this, this.ParentGroup);

                return;
            }

            foundAction = FindActionInCollection(actionName, improperActions);

            if (foundAction != null)
            {
                foundAction.Detected = true;

                this.Detected = true;
                this.FulfillmentType = FulfillmentTypes.ImproperAction;

                if( writeToLog )
                    this.DetectionLog.LogAction(foundAction, this, this.ParentGroup);
                return;
            }

            foundAction = FindActionInCollection(actionName, inactions);

            if (foundAction != null)
            {
                foundAction.Detected = true;

                this.Detected = true;
                this.FulfillmentType = FulfillmentTypes.Inaction;

                if( writeToLog )
                    this.DetectionLog.LogAction(foundAction, this, this.ParentGroup);
                return;
            }

            // If It Reaches Here --> Action Not Found In Any Collection... Was it misspelled in searching?
            // @todo determine if thrown
            //throw new NotImplementedException();
            System.Diagnostics.Debug.WriteLine("DetectableActionCategory not found");
        }


        /// <summary>
        /// Searches through it's set of Proper, Inproper, and Inactions for the DetectableAction passed in.
        /// 
        /// If found, set's it's own Detected flag to true, and additionally set's the found DetectableAction's Detected flag to true.
        /// Also, the FullfillmentType is updated to reflect which type (Proper, Inproper, or Inaction) of action was fulfilled based on the found DetectableAction.
        /// </summary>
        /// <param name="action"></param>
        public void DetectAction(DetectableAction action, bool writeToLog)
        {
            DetectableAction foundAction = null;// FindActionInAny(action);

            foundAction = FindActionInCollection(action, properActions);

            if (foundAction != null)
            {
                foundAction.Detected = true;

                this.Detected = true;
                this.FulfillmentType = FulfillmentTypes.ProperAction;

                if( writeToLog )
                    this.DetectionLog.LogAction(foundAction, this, this.ParentGroup);
                return;
            }

            foundAction = FindActionInCollection(action, improperActions);

            if (foundAction != null)
            {
                foundAction.Detected = true;

                this.Detected = true;
                this.FulfillmentType = FulfillmentTypes.ImproperAction;

                if( writeToLog )
                    this.DetectionLog.LogAction(foundAction, this, this.ParentGroup);
                return;
            }

            foundAction = FindActionInCollection(action, inactions);

            if (foundAction != null)
            {
                foundAction.Detected = true;

                this.Detected = true;
                this.FulfillmentType = FulfillmentTypes.Inaction;

                if( writeToLog )
                    this.DetectionLog.LogAction(foundAction, this, this.ParentGroup);
                return;
            }

            // If It Reaches Here --> Action Not Found In Any Collection... Was it misspelled in searching?
            throw new NotImplementedException();
        }

        /// <summary>
        /// Searches through it's set of Proper, Inproper, and Inactions for the UndetectableAction with the passed in name.
        /// 
        /// If found, set's it's own Undetected flag to true, and additionally set's the found UndetectableAction's Undetected flag to true.
        /// Also, the FullfillmentType is updated to reflect which type (Proper, Inproper, or Inaction) of action was fulfilled based on the found UndetectableAction.
        /// </summary>
        /// <param name="actionName"></param>
        public void UndetectAction(string actionName, bool writeToLog)
        {
            DetectableAction foundAction = null;// FindActionInAny(actionName);

            foundAction = FindActionInCollection(actionName, properActions);

            if (foundAction != null)
            {
                foundAction.Detected = false;

                if( writeToLog )
                    this.DetectionLog.LogAction(foundAction, this, this.ParentGroup);
                return;
            }

            foundAction = FindActionInCollection(actionName, improperActions);

            if (foundAction != null)
            {
                foundAction.Detected = true;

                if( writeToLog )
                    this.DetectionLog.LogAction(foundAction, this, this.ParentGroup);
                return;
            }

            foundAction = FindActionInCollection(actionName, inactions);

            if (foundAction != null)
            {
                foundAction.Detected = false;

                if( writeToLog )
                    this.DetectionLog.LogAction(foundAction, this, this.ParentGroup);
                return;
            }

            // If It Reaches Here --> Action Not Found In Any Collection... Was it misspelled in searching?
            throw new NotImplementedException();
        }


        /// <summary>
        /// Searches through it's set of Proper, Inproper, and Inactions for the UndetectableAction passed in.
        /// 
        /// If found, set's it's own Undetected flag to true, and additionally set's the found UndetectableAction's Undetected flag to true.
        /// Also, the FullfillmentType is updated to reflect which type (Proper, Inproper, or Inaction) of action was fulfilled based on the found UndetectableAction.
        /// </summary>
        /// <param name="action"></param>
        public void UndetectAction(DetectableAction action, bool writeToLog)
        {
            DetectableAction foundAction = null;// FindActionInAny(action);

            foundAction = FindActionInCollection(action, properActions);

            if (foundAction != null)
            {
                foundAction.Detected = false;

                if( writeToLog )
                    this.DetectionLog.LogAction(foundAction, this, this.ParentGroup);
                return;
            }

            foundAction = FindActionInCollection(action, improperActions);

            if (foundAction != null)
            {
                foundAction.Detected = false;

                if( writeToLog )
                    this.DetectionLog.LogAction(foundAction, this, this.ParentGroup);
                return;
            }

            foundAction = FindActionInCollection(action, inactions);

            if (foundAction != null)
            {
                foundAction.Detected = false;

                if( writeToLog )
                    this.DetectionLog.LogAction(foundAction, this, this.ParentGroup);
                return;
            }

            // If It Reaches Here --> Action Not Found In Any Collection... Was it misspelled in searching?
            throw new NotImplementedException();
        }

        public DetectableAction FindActionInAny(DetectableAction actionToFind)
        {
            DetectableAction foundAction = null;

            if (properActions != null)
            {
                foundAction = properActions.SingleOrDefault(o => o.Equals(actionToFind));
            }

            if (foundAction == null)
            {
                if (improperActions != null)
                {
                    foundAction = improperActions.SingleOrDefault(o => o.Equals(actionToFind));
                }
            }

            if (foundAction == null)
            {
                if (inactions != null)
                {
                    foundAction = inactions.SingleOrDefault(o => o.Equals(actionToFind));
                }
            }

            return foundAction;
        }

        public DetectableAction FindActionInAny(string actionNameToFind)
        {
            DetectableAction foundAction = null;

            if (properActions != null)
            {
                foundAction = properActions.SingleOrDefault(o => o.Name.Equals(actionNameToFind));
            }

            if (foundAction == null)
            {
                if (improperActions != null)
                {
                    foundAction = improperActions.SingleOrDefault(o => o.Name.Equals(actionNameToFind));
                }
            }

            if (foundAction == null)
            {
                if (inactions != null)
                {
                    foundAction = inactions.SingleOrDefault(o => o.Name.Equals(actionNameToFind));
                }
            }

            return foundAction;
        }

        public DetectableAction FindActionInCollection(DetectableAction actionToFind, ObservableCollection<DetectableAction> collectionToSearch)
        {
            DetectableAction foundAction = null;

            if (collectionToSearch != null)
            {
                foundAction = collectionToSearch.SingleOrDefault( o => o.Equals( actionToFind ) );
            }

            return foundAction;
        }

        public DetectableAction FindActionInCollection(string actionNameToFind, ObservableCollection<DetectableAction> collectionToSearch)
        {
            DetectableAction foundAction = null;

            if (collectionToSearch != null)
            {
                foundAction = collectionToSearch.SingleOrDefault(o => o.Name.Equals(actionNameToFind));
            }

            return foundAction;
        }

        /// <summary>
        /// Adds a DetectableAction to the list of proper actions.
        /// </summary>
        /// <param name="action"></param>
        public void AddProperAction(DetectableAction action)
        {
            if (action == null)
                return;
            if (properActions == null)
                properActions = new ObservableCollection<DetectableAction>();
            if (properActions.Contains(action))
                return;

            properActions.Add(action);
        }

        /// <summary>
        /// Adds a DetectableAction to the list of improper actions.
        /// </summary>
        /// <param name="action"></param>
        public void AddImproperAction(DetectableAction action)
        {
            if (action == null)
                return;
            if (improperActions == null)
                improperActions = new ObservableCollection<DetectableAction>();
            if (improperActions.Contains(action))
                return;

            improperActions.Add(action);
        }

        /// <summary>
        /// Adds a DetectableAction to the list of inactions.
        /// </summary>
        /// <param name="action"></param>
        public void AddInaction(DetectableAction action)
        {
            if (action == null)
                return;
            if (inactions == null)
                inactions = new ObservableCollection<DetectableAction>();
            if (inactions.Contains(action))
                return;

            inactions.Add(action);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(DetectableActionCategory detectableActionCategory, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(detectableActionCategory, propertyChangedEventArgs);
            }
        }

        public void ResetActions()
        {
            if (this.ProperActions != null)
            {
                foreach (DetectableAction action in this.ProperActions)
                {
                    action.Detected = false;
                }
            }

            if (this.ImproperActions != null)
            {
                foreach (DetectableAction action in this.ImproperActions)
                {
                    action.Detected = false;
                }
            }

            if (this.Inactions != null)
            {
                foreach (DetectableAction action in this.Inactions)
                {
                    action.Detected = false;
                }
            }

            this.Detected = false;

            this.ClearLogs();
        }

        public void ClearLogs()
        {
            if (this.DetectionLog != null)
            {
                this.DetectionLog.ClearLog();
            }
        }
    }
}
