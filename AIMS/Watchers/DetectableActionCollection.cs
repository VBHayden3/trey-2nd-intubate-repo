﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace AIMS.Watchers
{
    public class DetectableActionCollection : INotifyPropertyChanged
    {
        private string name;

        public string Name
        {
            get { return name; }
            set
            {
                if (name == value)
                    name = value;
                else
                {
                    name = value;
                    NotifyPropertyChanged(this, new PropertyChangedEventArgs("Name"));
                }
            }
        }

        private void NotifyPropertyChanged(DetectableActionCollection detectableActionCollection, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(detectableActionCollection, propertyChangedEventArgs);
            }
        }

        private ObservableCollection<DetectableActionGroup> actionGroups = new ObservableCollection<DetectableActionGroup>();
        public ObservableCollection<DetectableActionGroup> ActionGroups { get { return actionGroups; } }

        public DetectableActionCollection(string name)
        {
            this.Name = name;
        }

        /// <summary>
        /// Adds a DetectableActionGroup to the collection.
        /// </summary>
        /// <param name="group"></param>
        public void AddGroup( DetectableActionGroup group )
        {
            if (group == null)
                return;
            if (actionGroups == null)
                actionGroups = new ObservableCollection<DetectableActionGroup>();
            if (actionGroups.Contains(group))
                return;

            actionGroups.Add(group);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Returns the found group if the group exists within this collection, otherwise returns null.
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public DetectableActionGroup FindGroup(DetectableActionGroup group)
        {
            DetectableActionGroup foundGroup = null;

            if (this.ActionGroups != null)
            {
                foundGroup = ActionGroups.SingleOrDefault(o => o.Equals(group));
            }
            else
            {
                throw new KeyNotFoundException();
            }

            return foundGroup;
        }

        /// <summary>
        /// Returns the found group if a group with the passed name exists within this collection, otherwise returns null.
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public DetectableActionGroup FindGroup(string groupName)
        {
            DetectableActionGroup foundGroup = null;

            if (this.ActionGroups != null)
            {
                foundGroup = ActionGroups.SingleOrDefault(o => o.Name.Equals(groupName));
            }
            else
            {
                throw new KeyNotFoundException();
            }

            return foundGroup;
        }

        public void DetectAction(DetectableAction action, DetectableActionCategory category, DetectableActionGroup group, bool writeToLog)
        {
            if (FindGroup(group) != null)
            {
                group.DetectAction(action, category, writeToLog);
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public void DetectAction(string actionName, DetectableActionCategory category, DetectableActionGroup group, bool writeToLog)
        {
            DetectableActionGroup foundGroup = FindGroup(group);

            if (foundGroup != null)
            {
                foundGroup.DetectAction(actionName, category, writeToLog);
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public void DetectAction(DetectableAction action, string categoryName, DetectableActionGroup group, bool writeToLog)
        {
            DetectableActionGroup foundGroup = FindGroup(group);

            if (foundGroup != null)
            {
                foundGroup.DetectAction(action, categoryName, writeToLog);
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public void DetectAction(string actionName, string categoryName, DetectableActionGroup group, bool writeToLog)
        {
            DetectableActionGroup foundGroup = FindGroup(group);

            if (foundGroup != null)
            {
                foundGroup.DetectAction(actionName, categoryName, writeToLog);
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public void DetectAction(string actionName, string categoryName, string groupName, bool writeToLog)
        {
            DetectableActionGroup foundGroup = FindGroup(groupName);

            if (foundGroup != null)
            {
                foundGroup.DetectAction(actionName, categoryName, writeToLog);
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public void DetectAction(DetectableAction action, DetectableActionCategory category, string groupName, bool writeToLog)
        {
            DetectableActionGroup foundGroup = FindGroup(groupName);

            if ( foundGroup != null)
            {
                foundGroup.DetectAction(action, category, writeToLog);
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public void DetectAction(string actionName, DetectableActionCategory category, string groupName, bool writeToLog)
        {
            DetectableActionGroup foundGroup = FindGroup(groupName);

            if (foundGroup != null)
            {
                foundGroup.DetectAction(actionName, category, writeToLog);
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public void DetectAction(DetectableAction action, string categoryName, string groupName, bool writeToLog)
        {
            DetectableActionGroup foundGroup = FindGroup(groupName);

            if (foundGroup != null)
            {
                foundGroup.DetectAction(action, categoryName, writeToLog);
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public void UndetectAction(DetectableAction action, DetectableActionCategory category, DetectableActionGroup group, bool writeToLog)
        {
            if (FindGroup(group) != null)
            {
                group.UndetectAction(action, category, writeToLog);
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public void UndetectAction(string actionName, DetectableActionCategory category, DetectableActionGroup group, bool writeToLog)
        {
            DetectableActionGroup foundGroup = FindGroup(group);

            if (foundGroup != null)
            {
                foundGroup.UndetectAction(actionName, category, writeToLog);
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public void UndetectAction(DetectableAction action, string categoryName, DetectableActionGroup group, bool writeToLog)
        {
            DetectableActionGroup foundGroup = FindGroup(group);

            if (foundGroup != null)
            {
                foundGroup.UndetectAction(action, categoryName, writeToLog);
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public void UndetectAction(string actionName, string categoryName, DetectableActionGroup group, bool writeToLog)
        {
            DetectableActionGroup foundGroup = FindGroup(group);

            if (foundGroup != null)
            {
                foundGroup.UndetectAction(actionName, categoryName, writeToLog);
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public void UndetectAction(string actionName, string categoryName, string groupName, bool writeToLog)
        {
            DetectableActionGroup foundGroup = FindGroup(groupName);

            if (foundGroup != null)
            {
                foundGroup.UndetectAction(actionName, categoryName, writeToLog);
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public void UndetectAction(DetectableAction action, DetectableActionCategory category, string groupName, bool writeToLog)
        {
            DetectableActionGroup foundGroup = FindGroup(groupName);

            if (foundGroup != null)
            {
                foundGroup.UndetectAction(action, category, writeToLog);
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public void UndetectAction(string actionName, DetectableActionCategory category, string groupName, bool writeToLog)
        {
            DetectableActionGroup foundGroup = FindGroup(groupName);

            if (foundGroup != null)
            {
                foundGroup.UndetectAction(actionName, category, writeToLog);
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public void UndetectAction(DetectableAction action, string categoryName, string groupName, bool writeToLog)
        {
            DetectableActionGroup foundGroup = FindGroup(groupName);

            if (foundGroup != null)
            {
                foundGroup.UndetectAction(action, categoryName, writeToLog);
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public void ResetActions()
        {
            if (this.ActionGroups != null)
            {
                foreach (DetectableActionGroup group in this.ActionGroups)
                {
                    group.ResetActions();
                }
            }
        }

        public void ClearLogs()
        {
            if (this.ActionGroups != null)
            {
                foreach (DetectableActionGroup group in this.ActionGroups)
                {
                    group.ClearLogs();
                }
            }
        }
    }
}
