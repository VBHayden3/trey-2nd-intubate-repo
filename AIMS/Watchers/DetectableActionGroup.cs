﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace AIMS.Watchers
{
    public enum FulfillmentTypes
    {
        ProperAction,
        ImproperAction,
        Inaction
    }

    public class DetectableActionGroup : INotifyPropertyChanged
    {
        private string name;

        public string Name
        {
            get { return name; }
            set
            {
                if (name == value)
                    name = value;
                else
                {
                    name = value;
                    NotifyPropertyChanged(this, new PropertyChangedEventArgs("Name"));
                }
            }
        }

        private void NotifyPropertyChanged(DetectableActionGroup detectableActionGroup, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(detectableActionGroup, propertyChangedEventArgs);
            }
        }

        private ObservableCollection<DetectableActionCategory> detectableActionCategories = new ObservableCollection<DetectableActionCategory>();
        public ObservableCollection<DetectableActionCategory> DetectableActionCategories { get { return detectableActionCategories; } }

        public DetectableActionGroup(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Add an action to this group of detectable actions.
        /// </summary>
        /// <param name="action"></param>
        public void AddActionCategory(DetectableActionCategory actionCategory)
        {
            if (actionCategory == null)
                return;
            if (detectableActionCategories == null)
                detectableActionCategories = new ObservableCollection<DetectableActionCategory>();
            if (detectableActionCategories.Contains(actionCategory))
                return;

            detectableActionCategories.Add(actionCategory);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public DetectableActionCategory FindCategory(string name)
        {
            DetectableActionCategory foundCategory = null;

            if (this.DetectableActionCategories != null)
            {
                foundCategory = this.DetectableActionCategories.SingleOrDefault(o => o.Name.Equals(name));
            }

            return foundCategory;
        }

        public DetectableActionCategory FindCategory(DetectableActionCategory category)
        {
            DetectableActionCategory foundCategory = null;

            if (this.DetectableActionCategories != null)
            {
                foundCategory = this.DetectableActionCategories.SingleOrDefault(o => o.Equals(category));
            }

            return foundCategory;
        }

        /// <summary>
        /// Finds the passed category within this group, then calls that categories DetectAction method for the paritcular action.
        /// </summary>
        /// <param name="action"></param>
        public void DetectAction(string actionName, string actionCategory, bool writeToLog)
        {
            DetectableActionCategory category = FindCategory(actionCategory);
            if (category != null)
            {
                category.DetectAction(actionName, writeToLog);
                return;
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        /// <summary>
        /// Finds the passed category within this group, then calls that categories DetectAction method for the paritcular action.
        /// </summary>
        /// <param name="action"></param>
        public void DetectAction(DetectableAction action, string actionCategory, bool writeToLog)
        {
            DetectableActionCategory category = FindCategory(actionCategory);
            if (category != null)
            {
                category.DetectAction(action, writeToLog);
                return;
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        /// <summary>
        /// Finds the passed category within this group, then calls that categories DetectAction method for the paritcular action.
        /// </summary>
        /// <param name="action"></param>
        public void DetectAction(string actionName, DetectableActionCategory actionCategory, bool writeToLog)
        {
            if (FindCategory(actionCategory) != null)
            {
                actionCategory.DetectAction(actionName, writeToLog);
                return;
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }
        
        /// <summary>
        /// Finds the passed category within this group, then calls that categories DetectAction method for the paritcular action.
        /// </summary>
        /// <param name="action"></param>
        public void DetectAction(DetectableAction action, DetectableActionCategory actionCategory, bool writeToLog)
        {
            if (FindCategory(actionCategory) != null)
            {
                actionCategory.DetectAction(action, writeToLog);
                return;
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        /// <summary>
        /// Finds the passed category within this group, then calls that categories UndetectAction method for the paritcular action.
        /// </summary>
        /// <param name="action"></param>
        public void UndetectAction(string actionName, string actionCategory, bool writeToLog)
        {
            DetectableActionCategory category = FindCategory(actionCategory);
            if (category != null)
            {
                category.UndetectAction(actionName, writeToLog);
                return;
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        /// <summary>
        /// Finds the passed category within this group, then calls that categories UndetectAction method for the paritcular action.
        /// </summary>
        /// <param name="action"></param>
        public void UndetectAction(DetectableAction action, string actionCategory, bool writeToLog)
        {
            DetectableActionCategory category = FindCategory(actionCategory);
            if (category != null)
            {
                category.UndetectAction(action, writeToLog);
                return;
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        /// <summary>
        /// Finds the passed category within this group, then calls that categories UndetectAction method for the paritcular action.
        /// </summary>
        /// <param name="action"></param>
        public void UndetectAction(string actionName, DetectableActionCategory actionCategory, bool writeToLog)
        {
            if (FindCategory(actionCategory) != null)
            {
                actionCategory.UndetectAction(actionName, writeToLog);
                return;
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        /// <summary>
        /// Finds the passed category within this group, then calls that categories UndetectAction method for the paritcular action.
        /// </summary>
        /// <param name="action"></param>
        public void UndetectAction(DetectableAction action, DetectableActionCategory actionCategory, bool writeToLog)
        {
            if (FindCategory(actionCategory) != null)
            {
                actionCategory.UndetectAction(action, writeToLog);
                return;
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public void ResetActions()
        {
            if (this.DetectableActionCategories != null)
            {
                foreach (DetectableActionCategory category in this.DetectableActionCategories)
                {
                    category.ResetActions();
                }
            }
        }

        public void ClearLogs()
        {
            if (this.DetectableActionCategories != null)
            {
                foreach (DetectableActionCategory category in this.DetectableActionCategories)
                {
                    category.ClearLogs();
                }
            }
        }
    }
}
