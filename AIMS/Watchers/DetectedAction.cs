﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace AIMS.Watchers
{
    public class DetectedAction : INotifyPropertyChanged
    {
        private DateTime timeDetected = DateTime.Now;
        private string actionName;
        private string groupName;
        private string message;

        public DateTime TimeDetected { get { return timeDetected; } }
        public string ActionName { get { return actionName; } }
        public string GroupName { get { return groupName; } }
        public string Message
        { 
            get { return message; }
            set
            {
                if (message == value)
                    message = value;
                else
                {
                    message = value;
                    NotifyPropertyChanged(this, new PropertyChangedEventArgs("Message"));
                }
            } 
        }

        private void NotifyPropertyChanged(DetectedAction detectedAction, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(detectedAction, propertyChangedEventArgs);
            }
        }

        public DetectedAction(DateTime timedetected, string actionName, string groupName, string message)
        {
            this.timeDetected = timedetected;
            this.actionName = actionName;
            this.groupName = groupName;
            this.message = message;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        
    }
}
