﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using Microsoft.Kinect;

namespace AIMS
{
    public class JointWatcher : BaseWatcher
    {
        private Point3D lastPosition;       // last x, y, z location
        private RelativeDirection lastDirection;    // last x, y, z direction change
        private JointType watchedJoint;     // watched joint type
        private ulong? watchedSkeletonID;     // linked skeletonZ
        private bool hasSkeleton;

        public JointWatcher( JointType jointtype, ulong? skeletonid ) : this ()
        {
            watchedJoint = jointtype;
            watchedSkeletonID = skeletonid;
        }

        public JointWatcher()
        {
            this.Name = "";
            this.Description = "";
            this.IsTriggered = false;
            this.HasTriggered = false;
            this.LastTriggered = DateTime.MinValue;
            hasSkeleton = false;
            this.Running = false;
            lastPosition = new Point3D();
            lastDirection = new RelativeDirection(DeltaX.None, DeltaY.None, DeltaZ.None);
        }

        protected override void OnBeforeUpdate()
        {

        }

        protected override void OnUpdate()
        {
            base.OnUpdate();

            Body skeleton = null;

            if (watchedSkeletonID != null)
            {
                foreach (Body skele in KinectManager.Bodies)
                {
                    if (skele.TrackingId == watchedSkeletonID)
                    {
                        skeleton = skele;
                        break;
                    }
                }
            }

            if (skeleton == null && watchedSkeletonID != null)
            {
                if (hasSkeleton)   // We had a skeleton, and now it's not available.
                {
                    LostSkeleton();
                }
            }
            else
            {
                if (skeleton == null)
                {
                    if (KinectManager.ClosestBody != null)
                    {
                        skeleton = KinectManager.ClosestBody;
                        //System.Diagnostics.Debug.WriteLine("No skeleton set on JointWatcher, defaulting to ClosestSkeleton!");
                    }
                    else
                        return;
                }

                /*
                if (skeleton.TrackingState == SkeletonTrackingState.PositionOnly)
                {
                    System.Diagnostics.Debug.WriteLine(String.Format("[{0}] {1}'s has no joint positions because skeleton id#{2} is PositionOnly.", DateTime.Now, (String.IsNullOrEmpty(this.Name) ? this.ToString() : this.Name), watchedSkeletonID));
                }
                */

                if (skeleton.Joints[this.watchedJoint].TrackingState == TrackingState.Tracked)
                {
                    //AnalyzeNewJoint(skeleton.Joints[this.watchedJoint]);
                }
            }
        }


        protected override void OnAfterUpdate()
        {

        }

        private void LostSkeleton()
        {
            System.Diagnostics.Debug.WriteLine( String.Format( "[{0}] {1} lost it's skeleton of ID {2}.", DateTime.Now, (String.IsNullOrEmpty(this.Name) ? this.ToString() : this.Name), watchedSkeletonID ) );
            hasSkeleton = false;
        }
    }
}
