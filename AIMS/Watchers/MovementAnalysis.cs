﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Media3D;
using Microsoft.Kinect;

namespace AIMS
{
    public class MovementAnalysis
    {
        private RelativeDirection relativeDirection;
        private RelativeDisplacement relativeDisplacement;

        public RelativeDirection RelativeDirection { get { return relativeDirection; } set { relativeDirection = value; } }
        public RelativeDisplacement RelativeDisplacement { get { return relativeDisplacement; } set { relativeDisplacement = value; } }

        public MovementAnalysis()
        {
            relativeDirection = new RelativeDirection(DeltaX.None, DeltaY.None, DeltaZ.None);
            relativeDisplacement = new RelativeDisplacement(0, 0, 0);
        }

        public MovementAnalysis(RelativeDirection relativedirection, RelativeDisplacement relativedisplacement)
        {
            relativeDirection = relativedirection;
            relativeDisplacement = relativedisplacement;
        }

        public override string ToString()
        {
            return String.Format("DIRECTION: ( x: {0}, y: {1}, z: {2} ). DISPLACEMENT: ( x: {3:0.###}, y: {4:0.###}, z: {5:0.###} ).", relativeDirection.X, relativeDirection.Y, relativeDirection.Z, relativeDisplacement.X, relativeDisplacement.Y, relativeDisplacement.Z);
        }

        public static MovementAnalysis ComparePoints(Point3D initial, Point3D current)
        {
            MovementAnalysis analysis = new MovementAnalysis();

            /*if (initial == null || (initial.X == 0 && initial.Y == 0 && initial.Z == 0))
            {
                // there was no initial location - so no "change" has really occured since it's the first record..
            }
            else*/
            {
                // Determine the displacement
                double deltaX = current.X - initial.X;
                double deltaY = current.Y - initial.Y;
                double deltaZ = current.Z - initial.Z;
                // Determine change in location.
                DeltaX directionX = DeltaX.None;
                DeltaY directionY = DeltaY.None;
                DeltaZ directionZ = DeltaZ.None;

                if (deltaX > 0)
                    directionX = DeltaX.Right;
                else if (deltaX < 0)
                    directionX = DeltaX.Left;

                if (deltaY > 0)
                    directionY = DeltaY.Up;
                else if (deltaY < 0)
                    directionY = DeltaY.Down;

                if (deltaZ > 0)
                    directionZ = DeltaZ.Backward;
                else if (deltaZ < 0)
                    directionZ = DeltaZ.Forward;

                analysis.RelativeDirection = new RelativeDirection(directionX, directionY, directionZ);
                analysis.RelativeDisplacement = new RelativeDisplacement(deltaX, deltaY, deltaZ);
            }

            return analysis;
        }

        public static MovementAnalysis ComparePoints(CameraSpacePoint initial, CameraSpacePoint current)
        {
            MovementAnalysis analysis = new MovementAnalysis();

            double movementThreshhold = 0.00125; // Minimal amount of displacement indicating notable movement.
            
            // Determine the displacement
            double deltaX = current.X - initial.X;
            double deltaY = current.Y - initial.Y;
            double deltaZ = current.Z - initial.Z;

            // Determine change in location.
            DeltaX directionX = DeltaX.None;
            DeltaY directionY = DeltaY.None;
            DeltaZ directionZ = DeltaZ.None;

            if (deltaX > movementThreshhold)
                directionX = DeltaX.Right;
            else if (deltaX < -movementThreshhold)
                directionX = DeltaX.Left;

            if (deltaY > movementThreshhold)
                directionY = DeltaY.Up;
            else if (deltaY < -movementThreshhold)
                directionY = DeltaY.Down;

            if (deltaZ > movementThreshhold)
                directionZ = DeltaZ.Backward;
            else if (deltaZ < -movementThreshhold)
                directionZ = DeltaZ.Forward;

            analysis.RelativeDirection = new RelativeDirection(directionX, directionY, directionZ);
            analysis.RelativeDisplacement = new RelativeDisplacement(deltaX, deltaY, deltaZ);

            return analysis;
        }
    }
}
