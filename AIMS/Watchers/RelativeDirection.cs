﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIMS
{
    public enum DeltaX
    {
        None,
        Left,
        Right
    }

    public enum DeltaY
    {
        None,
        Down,
        Up
    }

    public enum DeltaZ
    {
        None,
        Forward,
        Backward
    }

    public class RelativeDirection
    {
        private DeltaX x;
        private DeltaY y;
        private DeltaZ z;

        public DeltaX X { get { return x; } set { x = value; } }
        public DeltaY Y { get { return y; } set { y = value; } }
        public DeltaZ Z { get { return z; } set { z = value; } }

        public RelativeDirection(DeltaX xdelta, DeltaY ydelta, DeltaZ zdelta)
        {
            x = xdelta;
            y = ydelta;
            z = zdelta;
        }

        public RelativeDirection() : this( DeltaX.None, DeltaY.None, DeltaZ.None )
        {
        }
    }
}
