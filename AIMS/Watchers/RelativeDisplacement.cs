﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIMS
{
    public class RelativeDisplacement
    {
        private double x;
        private double y;
        private double z;

        public double X { get { return x; } set { x = value; } }
        public double Y { get { return y; } set { y = value; } }
        public double Z { get { return z; } set { z = value; } }

        public RelativeDisplacement() : this( 0, 0, 0 )
        {
        }

        public RelativeDisplacement(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }
    }
}
