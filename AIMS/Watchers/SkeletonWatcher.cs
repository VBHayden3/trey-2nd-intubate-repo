﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIMS
{
    /// <summary>
    /// SkeletonWatcher is a BaseWatcher which watches for Skeleton based activity.
    /// 
    /// This class is follows a commonly used implementation of the CustomWatcher object.
    /// The intention of this class is to create a basic specialized layout which exposes common and repeating elements of BaseAIMS which are being used to track skeleton actions.
    /// </summary>
    public class SkeletonWatcher : BaseWatcher
    {
        private bool watchSpecificSkeleton;
        private ulong? watchedSkeletonID;
        private bool canFindSkeleton;
        private bool watchedSkeletonLost;

        protected override void OnBeforeUpdate()
        {
            throw new NotImplementedException();
        }

        protected override void OnUpdate()
        {
            base.OnUpdate();
        }

        protected override void OnAfterUpdate()
        {
            throw new NotImplementedException();
        }
    }
}
