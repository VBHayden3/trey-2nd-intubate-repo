﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Microsoft.Kinect;
using System.Windows.Media.Media3D;

namespace AIMS
{
    /// <summary>
    /// Watcher is a BaseWatcher with an open-ended use-case.
    /// 
    /// The implementation of this watcher is not specified, and will be used in a multitude of situations.
    /// </summary>
    public class Watcher : BaseWatcher
    {
        public delegate void UpdateWatcherDelegate();

        private UpdateWatcherDelegate checkForActionDelegate;
        public UpdateWatcherDelegate CheckForActionDelegate { get { return checkForActionDelegate; } set { checkForActionDelegate = value; } }

        public Watcher() : this( null )
        {
        }

        public Watcher(string name) : base()
        {
            this.Name = name;
        }

        protected override void OnBeforeUpdate()
        {
            //throw new NotImplementedException();
        }

        protected override void OnUpdate()
        {
            base.OnUpdate();

            if (CheckForActionDelegate != null)
            {
                CheckForActionDelegate();
            }
        }

        protected override void OnAfterUpdate()
        {
            //throw new NotImplementedException();
        }

        // TREY: Return an Int32Rect object using a colorspacepoint for reference
        public static Int32Rect RectFromReference(ColorSpacePoint referencePoint, int x, int y, int width, int height, bool forceBounds=true)
        {
            // We may want to scale this
            int scale_x = 3;
            int scale_y = 2;

            // Get our top left coordinates with  respect to this reference point
            int default_x = (int)referencePoint.X + x * scale_x;
            int default_y = (int)referencePoint.Y + y * scale_y;

            // Check if  we're supposed to maintain a positive x, y coordinate
            if (forceBounds)
            {
                // Clip to Naturals including Zero
                default_x = (default_x >= 0) ? default_x : 0;
                default_y = (default_y >= 0) ? default_y : 0;
            }

            // Assign  these values to a rect
            Int32Rect rect = new Int32Rect(default_x, default_y, width * scale_x, height * scale_y);

            // Return it
            return rect;
        }

        // TREY: Convert a Point3D to a CameraSpacePoint
        public static CameraSpacePoint CameraSpaceFromPoint3D(Point3D p3d)
        {
            // Take it from Point3D to a CameraSpacePoint
            CameraSpacePoint camSpace = new CameraSpacePoint();
            // Assign them  explicitly
            camSpace.X = (float)p3d.X;
            camSpace.Y = (float)p3d.Y;
            camSpace.Z = (float)p3d.Z / 1000f;

            // Return this
            return camSpace;
        }

        // TREY: Convert a CameraSpacePoint to a ColorSpacePoint
        public static ColorSpacePoint ColorSpaceFromCameraSpace(CameraSpacePoint camSpace)
        {
            // Convert this using Kinect library
            ColorSpacePoint colorSpace = KinectManager.CoordinateMapper.MapCameraPointToColorSpace(camSpace);

            // Return that
            return colorSpace;
        }

        // TREY: Convert a Point3D to a ColorSpacePoint
        public static ColorSpacePoint ColorSpaceFromPoint3D(Point3D p3d)
        {
            // Take it from Point3D to a CameraSpacePoint
            CameraSpacePoint camSpace = CameraSpaceFromPoint3D(p3d);

            // Convert this using Kinect library
            ColorSpacePoint colorSpace = ColorSpaceFromCameraSpace(camSpace);

            // Return that
            return colorSpace;
        }
    }
}
