﻿#pragma checksum "..\..\..\Components\IntubationStatsheetComponent.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "6825831AB398678C67356C801CCF7371"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using AIMS.Components;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Controls.DataVisualization.Charting.Primitives;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace AIMS.Components {
    
    
    /// <summary>
    /// IntubationStatsheetComponent
    /// </summary>
    public partial class IntubationStatsheetComponent : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 34 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LessonNameTB;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock MasteryLevelTB;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock TimesPracticedTB;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock TimesTestedTB;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock PointsTB;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock TimeLastPracticedTB;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock TimeLastTestedTB;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock NovicePracticeCountTB;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock NovicePracticeAverageScoreTB;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock NoviceTestCountTB;
        
        #line default
        #line hidden
        
        
        #line 104 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock NoviceTestAverageScoreTB;
        
        #line default
        #line hidden
        
        
        #line 106 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button LastNovicePracticeButton;
        
        #line default
        #line hidden
        
        
        #line 111 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button LastNoviceTestButton;
        
        #line default
        #line hidden
        
        
        #line 132 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock IntermediatePracticeCountTB;
        
        #line default
        #line hidden
        
        
        #line 135 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock IntermediatePracticeAverageScoreTB;
        
        #line default
        #line hidden
        
        
        #line 138 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock IntermediateTestCountTB;
        
        #line default
        #line hidden
        
        
        #line 141 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock IntermediateTestAverageScoreTB;
        
        #line default
        #line hidden
        
        
        #line 143 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button LastIntermediatePracticeButton;
        
        #line default
        #line hidden
        
        
        #line 148 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button LastIntermediateTestButton;
        
        #line default
        #line hidden
        
        
        #line 169 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock MasterPracticeCountTB;
        
        #line default
        #line hidden
        
        
        #line 172 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock MasterPracticeAverageScoreTB;
        
        #line default
        #line hidden
        
        
        #line 175 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock MasterTestCountTB;
        
        #line default
        #line hidden
        
        
        #line 178 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock MasterTestAverageScoreTB;
        
        #line default
        #line hidden
        
        
        #line 180 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button LastMasterPracticeButton;
        
        #line default
        #line hidden
        
        
        #line 185 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button LastMasterTestButton;
        
        #line default
        #line hidden
        
        
        #line 208 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal AIMS.Components._3DViewer _3DViewer;
        
        #line default
        #line hidden
        
        
        #line 210 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas LastResultDetailsCanvas;
        
        #line default
        #line hidden
        
        
        #line 212 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock DetailsTitle;
        
        #line default
        #line hidden
        
        
        #line 213 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock DetailsText;
        
        #line default
        #line hidden
        
        
        #line 214 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock TimeNumber;
        
        #line default
        #line hidden
        
        
        #line 215 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ScoreNumber;
        
        #line default
        #line hidden
        
        
        #line 217 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock StageDescription;
        
        #line default
        #line hidden
        
        
        #line 218 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock StageName;
        
        #line default
        #line hidden
        
        
        #line 226 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataVisualization.Charting.Chart singleChart;
        
        #line default
        #line hidden
        
        
        #line 287 "..\..\..\Components\IntubationStatsheetComponent.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CloseResultViewButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/AIMS;component/components/intubationstatsheetcomponent.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Components\IntubationStatsheetComponent.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.LessonNameTB = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 2:
            this.MasteryLevelTB = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.TimesPracticedTB = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.TimesTestedTB = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.PointsTB = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.TimeLastPracticedTB = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.TimeLastTestedTB = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.NovicePracticeCountTB = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 9:
            this.NovicePracticeAverageScoreTB = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 10:
            this.NoviceTestCountTB = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 11:
            this.NoviceTestAverageScoreTB = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.LastNovicePracticeButton = ((System.Windows.Controls.Button)(target));
            
            #line 106 "..\..\..\Components\IntubationStatsheetComponent.xaml"
            this.LastNovicePracticeButton.Click += new System.Windows.RoutedEventHandler(this.LastNovicePracticeButton_Clicked);
            
            #line default
            #line hidden
            return;
            case 13:
            this.LastNoviceTestButton = ((System.Windows.Controls.Button)(target));
            
            #line 111 "..\..\..\Components\IntubationStatsheetComponent.xaml"
            this.LastNoviceTestButton.Click += new System.Windows.RoutedEventHandler(this.LastNoviceTestButton_Clicked);
            
            #line default
            #line hidden
            return;
            case 14:
            this.IntermediatePracticeCountTB = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 15:
            this.IntermediatePracticeAverageScoreTB = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 16:
            this.IntermediateTestCountTB = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 17:
            this.IntermediateTestAverageScoreTB = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 18:
            this.LastIntermediatePracticeButton = ((System.Windows.Controls.Button)(target));
            
            #line 143 "..\..\..\Components\IntubationStatsheetComponent.xaml"
            this.LastIntermediatePracticeButton.Click += new System.Windows.RoutedEventHandler(this.LastIntermediatePracticeButton_Clicked);
            
            #line default
            #line hidden
            return;
            case 19:
            this.LastIntermediateTestButton = ((System.Windows.Controls.Button)(target));
            
            #line 148 "..\..\..\Components\IntubationStatsheetComponent.xaml"
            this.LastIntermediateTestButton.Click += new System.Windows.RoutedEventHandler(this.LastIntermediateTestButton_Clicked);
            
            #line default
            #line hidden
            return;
            case 20:
            this.MasterPracticeCountTB = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 21:
            this.MasterPracticeAverageScoreTB = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 22:
            this.MasterTestCountTB = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 23:
            this.MasterTestAverageScoreTB = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 24:
            this.LastMasterPracticeButton = ((System.Windows.Controls.Button)(target));
            
            #line 180 "..\..\..\Components\IntubationStatsheetComponent.xaml"
            this.LastMasterPracticeButton.Click += new System.Windows.RoutedEventHandler(this.LastMasterPracticeButton_Clicked);
            
            #line default
            #line hidden
            return;
            case 25:
            this.LastMasterTestButton = ((System.Windows.Controls.Button)(target));
            
            #line 185 "..\..\..\Components\IntubationStatsheetComponent.xaml"
            this.LastMasterTestButton.Click += new System.Windows.RoutedEventHandler(this.LastMasterTestButton_Clicked);
            
            #line default
            #line hidden
            return;
            case 26:
            this._3DViewer = ((AIMS.Components._3DViewer)(target));
            return;
            case 27:
            this.LastResultDetailsCanvas = ((System.Windows.Controls.Canvas)(target));
            return;
            case 28:
            this.DetailsTitle = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 29:
            this.DetailsText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 30:
            this.TimeNumber = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 31:
            this.ScoreNumber = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 32:
            this.StageDescription = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 33:
            this.StageName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 34:
            this.singleChart = ((System.Windows.Controls.DataVisualization.Charting.Chart)(target));
            return;
            case 35:
            
            #line 228 "..\..\..\Components\IntubationStatsheetComponent.xaml"
            ((System.Windows.Controls.DataVisualization.Charting.ColumnSeries)(target)).SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.ColumnSeries_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 36:
            this.CloseResultViewButton = ((System.Windows.Controls.Button)(target));
            
            #line 287 "..\..\..\Components\IntubationStatsheetComponent.xaml"
            this.CloseResultViewButton.Click += new System.Windows.RoutedEventHandler(this.CloseResultViewButton_Clicked);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

