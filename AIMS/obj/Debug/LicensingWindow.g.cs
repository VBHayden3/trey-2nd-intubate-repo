﻿#pragma checksum "..\..\LicensingWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "F30165C8E6078336D51A4AA405D630F9"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace AIMS {
    
    
    /// <summary>
    /// LicensingWindow
    /// </summary>
    public partial class LicensingWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 5 "..\..\LicensingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid ProductActivationGrid;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\LicensingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LicenseLabel;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\LicensingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock StatusTB;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\LicensingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox ProductActivationKeyTB;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\LicensingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ActivateButton;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\LicensingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ContinueButton;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\LicensingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ProgressBar ProgressBar;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\LicensingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ProgressBar BusyBar;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\LicensingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LicenseLabel_Copy;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/AIMS;component/licensingwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\LicensingWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.ProductActivationGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.LicenseLabel = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.StatusTB = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.ProductActivationKeyTB = ((System.Windows.Controls.TextBox)(target));
            
            #line 20 "..\..\LicensingWindow.xaml"
            this.ProductActivationKeyTB.KeyUp += new System.Windows.Input.KeyEventHandler(this.ProductActivationKeyTB_KeyUp);
            
            #line default
            #line hidden
            return;
            case 5:
            this.ActivateButton = ((System.Windows.Controls.Button)(target));
            
            #line 21 "..\..\LicensingWindow.xaml"
            this.ActivateButton.Click += new System.Windows.RoutedEventHandler(this.ActivateButton_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.ContinueButton = ((System.Windows.Controls.Button)(target));
            
            #line 22 "..\..\LicensingWindow.xaml"
            this.ContinueButton.Click += new System.Windows.RoutedEventHandler(this.ContinueButton_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.ProgressBar = ((System.Windows.Controls.ProgressBar)(target));
            return;
            case 8:
            this.BusyBar = ((System.Windows.Controls.ProgressBar)(target));
            return;
            case 9:
            this.LicenseLabel_Copy = ((System.Windows.Controls.TextBlock)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

