--
-- PostgreSQL database dump
--

-- Dumped from database version 9.2.2
-- Dumped by pg_dump version 9.2.2
-- Started on 2013-06-19 19:07:42

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 212 (class 1259 OID 57449)
-- Name: kinect_images; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kinect_images (
    uid integer NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    hascolor boolean NOT NULL,
    hasdepth boolean NOT NULL,
    colordata bytea NOT NULL,
    depthdata bytea NOT NULL,
    colorformat smallint NOT NULL,
    depthformat smallint NOT NULL
);


ALTER TABLE public.kinect_images OWNER TO postgres;

--
-- TOC entry 2087 (class 0 OID 0)
-- Dependencies: 212
-- Name: TABLE kinect_images; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE kinect_images IS 'Table for storing stage screenshots for all lessons. Color and depth data are in a compressed binary format.';


--
-- TOC entry 2088 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN kinect_images.uid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN kinect_images.uid IS 'Unique Primary Key ID for Table.';


--
-- TOC entry 2089 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN kinect_images."timestamp"; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN kinect_images."timestamp" IS 'The Date Time of when the kinect image was captured.';


--
-- TOC entry 2090 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN kinect_images.hascolor; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN kinect_images.hascolor IS 'Whether or not the color data exists for this kinect image.';


--
-- TOC entry 2091 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN kinect_images.hasdepth; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN kinect_images.hasdepth IS 'Whether or not the depth data exists for this kinect image.';


--
-- TOC entry 2092 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN kinect_images.colordata; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN kinect_images.colordata IS 'Compressed binary color data.';


--
-- TOC entry 2093 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN kinect_images.depthdata; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN kinect_images.depthdata IS 'Compressed binary depth data.';


--
-- TOC entry 2094 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN kinect_images.colorformat; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN kinect_images.colorformat IS 'Color Image Format.';


--
-- TOC entry 2095 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN kinect_images.depthformat; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN kinect_images.depthformat IS 'Depth Image Format.';


--
-- TOC entry 213 (class 1259 OID 57452)
-- Name: kinect_images_uid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE kinect_images_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kinect_images_uid_seq OWNER TO postgres;

--
-- TOC entry 2096 (class 0 OID 0)
-- Dependencies: 213
-- Name: kinect_images_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE kinect_images_uid_seq OWNED BY kinect_images.uid;


--
-- TOC entry 2078 (class 2604 OID 57454)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kinect_images ALTER COLUMN uid SET DEFAULT nextval('kinect_images_uid_seq'::regclass);



--
-- TOC entry 2097 (class 0 OID 0)
-- Dependencies: 213
-- Name: kinect_images_uid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('kinect_images_uid_seq', 1, false);


--
-- TOC entry 2080 (class 2606 OID 57530)
-- Name: kinect_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kinect_images
    ADD CONSTRAINT kinect_images_pkey PRIMARY KEY (uid);


-- Completed on 2013-06-19 19:07:44

--
-- PostgreSQL database dump complete
--

