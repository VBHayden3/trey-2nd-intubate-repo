--
-- PostgreSQL database dump
--

-- Dumped from database version 9.2.2
-- Dumped by pg_dump version 9.2.2
-- Started on 2013-06-19 19:10:29

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 221 (class 1259 OID 57773)
-- Name: student_results_cprlookup; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE student_results_cprlookup (
    uid integer NOT NULL,
    cprid integer NOT NULL,
    key text NOT NULL,
    value text NOT NULL
);


ALTER TABLE public.student_results_cprlookup OWNER TO postgres;

--
-- TOC entry 2088 (class 0 OID 0)
-- Dependencies: 221
-- Name: TABLE student_results_cprlookup; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE student_results_cprlookup IS 'Key-Value Table for storing lookup data per module in a row-oriented configuration.  Table is related to the student_results_cpr table.';


--
-- TOC entry 2089 (class 0 OID 0)
-- Dependencies: 221
-- Name: COLUMN student_results_cprlookup.uid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_cprlookup.uid IS 'Unique Primary Key ID for student_results_cprlookup table.';


--
-- TOC entry 2090 (class 0 OID 0)
-- Dependencies: 221
-- Name: COLUMN student_results_cprlookup.cprid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_cprlookup.cprid IS 'FK to student_results_cpr table uid.';


--
-- TOC entry 2091 (class 0 OID 0)
-- Dependencies: 221
-- Name: COLUMN student_results_cprlookup.key; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_cprlookup.key IS 'Key which labels the parameter being saved.';


--
-- TOC entry 2092 (class 0 OID 0)
-- Dependencies: 221
-- Name: COLUMN student_results_cprlookup.value; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_cprlookup.value IS 'Value which stores the actual parameter.';


--
-- TOC entry 220 (class 1259 OID 57771)
-- Name: student_results_cprlookup_uid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE student_results_cprlookup_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.student_results_cprlookup_uid_seq OWNER TO postgres;

--
-- TOC entry 2093 (class 0 OID 0)
-- Dependencies: 220
-- Name: student_results_cprlookup_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE student_results_cprlookup_uid_seq OWNED BY student_results_cprlookup.uid;


--
-- TOC entry 2078 (class 2604 OID 57776)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_results_cprlookup ALTER COLUMN uid SET DEFAULT nextval('student_results_cprlookup_uid_seq'::regclass);



--
-- TOC entry 2094 (class 0 OID 0)
-- Dependencies: 220
-- Name: student_results_cprlookup_uid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('student_results_cprlookup_uid_seq', 1, false);


--
-- TOC entry 2080 (class 2606 OID 57781)
-- Name: student_results_cprlookup_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY student_results_cprlookup
    ADD CONSTRAINT student_results_cprlookup_pkey PRIMARY KEY (uid);


--
-- TOC entry 2081 (class 2606 OID 57782)
-- Name: student_results_cprlookup_cprid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_results_cprlookup
    ADD CONSTRAINT student_results_cprlookup_cprid_fkey FOREIGN KEY (cprid) REFERENCES student_results_cpr(uid);


-- Completed on 2013-06-19 19:10:30

--
-- PostgreSQL database dump complete
--

