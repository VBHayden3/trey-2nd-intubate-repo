--
-- PostgreSQL database dump
--

-- Dumped from database version 9.2.2
-- Dumped by pg_dump version 9.2.2
-- Started on 2013-06-19 19:11:22

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 217 (class 1259 OID 57744)
-- Name: student_results_intubationlookup; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE student_results_intubationlookup (
    uid integer NOT NULL,
    intubationid integer NOT NULL,
    key text NOT NULL,
    value text NOT NULL
);


ALTER TABLE public.student_results_intubationlookup OWNER TO postgres;

--
-- TOC entry 2088 (class 0 OID 0)
-- Dependencies: 217
-- Name: TABLE student_results_intubationlookup; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE student_results_intubationlookup IS 'Key-Value Table for storing lookup data per module in a row-oriented configuration.  Table is related to the student_results_intubation table.';


--
-- TOC entry 2089 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN student_results_intubationlookup.uid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_intubationlookup.uid IS 'Unique Primary Key ID for student_results_intubationlookup table.';


--
-- TOC entry 2090 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN student_results_intubationlookup.intubationid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_intubationlookup.intubationid IS 'FK to student_results_intubation table uid.';


--
-- TOC entry 2091 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN student_results_intubationlookup.key; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_intubationlookup.key IS 'Key which labels the parameter being saved.';


--
-- TOC entry 2092 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN student_results_intubationlookup.value; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_intubationlookup.value IS 'Value which stores the actual parameter.';


--
-- TOC entry 216 (class 1259 OID 57742)
-- Name: student_results_intubationlookup_uid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE student_results_intubationlookup_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.student_results_intubationlookup_uid_seq OWNER TO postgres;

--
-- TOC entry 2093 (class 0 OID 0)
-- Dependencies: 216
-- Name: student_results_intubationlookup_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE student_results_intubationlookup_uid_seq OWNED BY student_results_intubationlookup.uid;


--
-- TOC entry 2078 (class 2604 OID 57747)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_results_intubationlookup ALTER COLUMN uid SET DEFAULT nextval('student_results_intubationlookup_uid_seq'::regclass);



--
-- TOC entry 2094 (class 0 OID 0)
-- Dependencies: 216
-- Name: student_results_intubationlookup_uid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('student_results_intubationlookup_uid_seq', 1, false);


--
-- TOC entry 2080 (class 2606 OID 57752)
-- Name: student_results_intubationlookup_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY student_results_intubationlookup
    ADD CONSTRAINT student_results_intubationlookup_pkey PRIMARY KEY (uid);


--
-- TOC entry 2081 (class 2606 OID 57753)
-- Name: student_results_intubationlookup_intubationid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_results_intubationlookup
    ADD CONSTRAINT student_results_intubationlookup_intubationid_fkey FOREIGN KEY (intubationid) REFERENCES student_results_intubation(uid);


-- Completed on 2013-06-19 19:11:23

--
-- PostgreSQL database dump complete
--

