--
-- PostgreSQL database dump
--

-- Dumped from database version 9.2.2
-- Dumped by pg_dump version 9.2.2
-- Started on 2013-06-19 19:11:44

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 207 (class 1259 OID 49222)
-- Name: student_results_lateraltransfer; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE student_results_lateraltransfer (
    uid integer NOT NULL,
    studentid integer NOT NULL,
    masterylevel smallint NOT NULL,
    taskmode smallint NOT NULL,
    score double precision NOT NULL,
    lessonstarttime timestamp without time zone NOT NULL,
    lessonendtime timestamp without time zone NOT NULL,
    handplacementscore double precision NOT NULL,
    verbalcount boolean NOT NULL,
    verbalcounttime timestamp without time zone NOT NULL,
    pullstarttime timestamp without time zone NOT NULL,
    pullendtime timestamp without time zone NOT NULL,
    totalsync double precision NOT NULL,
    rhandsync double precision NOT NULL,
    lhandsync double precision NOT NULL,
    rwristsync double precision NOT NULL,
    lwristsync double precision NOT NULL,
    relbowsync double precision NOT NULL,
    lelbowsync double precision NOT NULL,
    rshouldersync double precision NOT NULL,
    lshouldersync double precision NOT NULL,
    manikinsync double precision NOT NULL,
    calibrationimageid integer,
    handplacementimageid integer,
    pullendimageid integer,
    pullstartimageid integer,
    overallfeedbackids integer[],
    pullfeedbackids integer[],
    handplacementfeedbackids integer[]
);


ALTER TABLE public.student_results_lateraltransfer OWNER TO postgres;

--
-- TOC entry 2100 (class 0 OID 0)
-- Dependencies: 207
-- Name: TABLE student_results_lateraltransfer; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE student_results_lateraltransfer IS 'Top-Level Lateral Transfer Results Table. Stores detailed and unique stage results in a column-oriented design.';


--
-- TOC entry 2101 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.uid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.uid IS 'Unique Primary Key ID for student_results_lateraltransfer table.';


--
-- TOC entry 2102 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.studentid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.studentid IS 'FK to student table uid.';


--
-- TOC entry 2103 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.masterylevel; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.masterylevel IS 'MasteryLevel enum (Novice, Intermediate, Master) represented as an integer value.';


--
-- TOC entry 2104 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.taskmode; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.taskmode IS 'TaskMode enum (Practice, Test) represented as an integer value.';


--
-- TOC entry 2105 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.score; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.score IS 'Total computed single number score for lateral transfer stage.';


--
-- TOC entry 2106 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.lessonstarttime; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.lessonstarttime IS 'Lesson Start Date and Time.';


--
-- TOC entry 2107 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.lessonendtime; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.lessonendtime IS 'Lesson End Date and Time.';


--
-- TOC entry 2108 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.handplacementscore; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.handplacementscore IS 'Total computed single number score for lateral transfer hand placement stage.';


--
-- TOC entry 2109 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.verbalcount; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.verbalcount IS 'Boolean indication for lateral transfer vocal pull stage.';


--
-- TOC entry 2110 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.verbalcounttime; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.verbalcounttime IS 'Lateral Transfer vocal pull stage Date and Time.';


--
-- TOC entry 2111 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.pullstarttime; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.pullstarttime IS 'Lateral Transfer pull detected start Date and Time.';


--
-- TOC entry 2112 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.pullendtime; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.pullendtime IS 'Lateral Transfer pull ended Date and Time.';


--
-- TOC entry 2113 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.totalsync; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.totalsync IS 'Lateral Transfer Total Sync value.';


--
-- TOC entry 2114 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.rhandsync; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.rhandsync IS 'Lateral Transfer Right Hand Sync value.';


--
-- TOC entry 2115 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.lhandsync; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.lhandsync IS 'Lateral Transfer Left Hand Sync value.';


--
-- TOC entry 2116 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.rwristsync; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.rwristsync IS 'Lateral Transfer Right Wrist Sync value.';


--
-- TOC entry 2117 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.lwristsync; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.lwristsync IS 'Lateral Transfer Left Wrist Sync value.';


--
-- TOC entry 2118 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.relbowsync; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.relbowsync IS 'Lateral Transfer Right Elbow Sync value.';


--
-- TOC entry 2119 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.lelbowsync; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.lelbowsync IS 'Lateral Transfer Left Elbow Sync value.';


--
-- TOC entry 2120 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.rshouldersync; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.rshouldersync IS 'Lateral Transfer Right Shoulder Sync value.';


--
-- TOC entry 2121 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.lshouldersync; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.lshouldersync IS 'Lateral Transfer Left Shoulder Sync value.';


--
-- TOC entry 2122 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.manikinsync; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.manikinsync IS 'Lateral Transfer Manikin Torso-to-Leg Sync value.';


--
-- TOC entry 2123 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.calibrationimageid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.calibrationimageid IS 'FK to kinect_images table uid.  Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';


--
-- TOC entry 2124 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.handplacementimageid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.handplacementimageid IS 'FK to kinect_images table uid.  Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';


--
-- TOC entry 2125 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.pullendimageid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.pullendimageid IS 'FK to kinect_images table uid.  Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';


--
-- TOC entry 2126 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.pullstartimageid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.pullstartimageid IS 'FK to kinect_images table uid.  Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';


--
-- TOC entry 2127 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.overallfeedbackids; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.overallfeedbackids IS 'List of Feedback IDs. Relates to the feedback strings relevant to the results. Feedback strings are stored in the lateral transfer lookup table.';


--
-- TOC entry 2128 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.pullfeedbackids; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.pullfeedbackids IS 'List of Feedback IDs. Relates to the feedback strings relevant to the results. Feedback strings are stored in the lateral transfer lookup table.';


--
-- TOC entry 2129 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN student_results_lateraltransfer.handplacementfeedbackids; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransfer.handplacementfeedbackids IS 'List of Feedback IDs. Relates to the feedback strings relevant to the results. Feedback strings are stored in the lateral transfer lookup table.';


--
-- TOC entry 206 (class 1259 OID 49220)
-- Name: student_results_lateraltransfer_uid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE student_results_lateraltransfer_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.student_results_lateraltransfer_uid_seq OWNER TO postgres;

--
-- TOC entry 2130 (class 0 OID 0)
-- Dependencies: 206
-- Name: student_results_lateraltransfer_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE student_results_lateraltransfer_uid_seq OWNED BY student_results_lateraltransfer.uid;


--
-- TOC entry 2078 (class 2604 OID 49225)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_results_lateraltransfer ALTER COLUMN uid SET DEFAULT nextval('student_results_lateraltransfer_uid_seq'::regclass);



--
-- TOC entry 2131 (class 0 OID 0)
-- Dependencies: 206
-- Name: student_results_lateraltransfer_uid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('student_results_lateraltransfer_uid_seq', 1, false);


--
-- TOC entry 2080 (class 2606 OID 57582)
-- Name: student_results_lateraltransfer_calibrationimageid_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY student_results_lateraltransfer
    ADD CONSTRAINT student_results_lateraltransfer_calibrationimageid_key UNIQUE (calibrationimageid);


--
-- TOC entry 2082 (class 2606 OID 57609)
-- Name: student_results_lateraltransfer_handplacementimageid_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY student_results_lateraltransfer
    ADD CONSTRAINT student_results_lateraltransfer_handplacementimageid_key UNIQUE (handplacementimageid);


--
-- TOC entry 2084 (class 2606 OID 49227)
-- Name: student_results_lateraltransfer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY student_results_lateraltransfer
    ADD CONSTRAINT student_results_lateraltransfer_pkey PRIMARY KEY (uid);


--
-- TOC entry 2086 (class 2606 OID 57611)
-- Name: student_results_lateraltransfer_pullendimageid_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY student_results_lateraltransfer
    ADD CONSTRAINT student_results_lateraltransfer_pullendimageid_key UNIQUE (pullendimageid);


--
-- TOC entry 2088 (class 2606 OID 57613)
-- Name: student_results_lateraltransfer_pullstartimageid_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY student_results_lateraltransfer
    ADD CONSTRAINT student_results_lateraltransfer_pullstartimageid_key UNIQUE (pullstartimageid);


--
-- TOC entry 2089 (class 2606 OID 57817)
-- Name: student_results_lateraltransfer_calibrationimageid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_results_lateraltransfer
    ADD CONSTRAINT student_results_lateraltransfer_calibrationimageid_fkey FOREIGN KEY (calibrationimageid) REFERENCES kinect_images(uid);


--
-- TOC entry 2090 (class 2606 OID 57822)
-- Name: student_results_lateraltransfer_handplacementimageid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_results_lateraltransfer
    ADD CONSTRAINT student_results_lateraltransfer_handplacementimageid_fkey FOREIGN KEY (handplacementimageid) REFERENCES kinect_images(uid);


--
-- TOC entry 2091 (class 2606 OID 57827)
-- Name: student_results_lateraltransfer_pullendimageid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_results_lateraltransfer
    ADD CONSTRAINT student_results_lateraltransfer_pullendimageid_fkey FOREIGN KEY (pullendimageid) REFERENCES kinect_images(uid);


--
-- TOC entry 2092 (class 2606 OID 57832)
-- Name: student_results_lateraltransfer_pullstartimageid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_results_lateraltransfer
    ADD CONSTRAINT student_results_lateraltransfer_pullstartimageid_fkey FOREIGN KEY (pullstartimageid) REFERENCES kinect_images(uid);


--
-- TOC entry 2093 (class 2606 OID 57837)
-- Name: student_results_lateraltransfer_studentid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_results_lateraltransfer
    ADD CONSTRAINT student_results_lateraltransfer_studentid_fkey FOREIGN KEY (studentid) REFERENCES students(uid);


-- Completed on 2013-06-19 19:11:45

--
-- PostgreSQL database dump complete
--

