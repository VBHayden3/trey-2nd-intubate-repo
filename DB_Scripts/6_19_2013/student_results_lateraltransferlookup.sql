--
-- PostgreSQL database dump
--

-- Dumped from database version 9.2.2
-- Dumped by pg_dump version 9.2.2
-- Started on 2013-06-19 19:12:08

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 211 (class 1259 OID 49282)
-- Name: student_results_lateraltransferlookup; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE student_results_lateraltransferlookup (
    uid integer NOT NULL,
    lateraltransferid integer NOT NULL,
    key text NOT NULL,
    value text NOT NULL
);


ALTER TABLE public.student_results_lateraltransferlookup OWNER TO postgres;

--
-- TOC entry 2088 (class 0 OID 0)
-- Dependencies: 211
-- Name: TABLE student_results_lateraltransferlookup; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE student_results_lateraltransferlookup IS 'Key-Value Table for storing lookup data per module in a row-oriented configuration.  Table is related to the student_results_lateraltransfer table.';


--
-- TOC entry 2089 (class 0 OID 0)
-- Dependencies: 211
-- Name: COLUMN student_results_lateraltransferlookup.uid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransferlookup.uid IS 'Unique Primary Key ID for student_results_lateraltransferlookup table.';


--
-- TOC entry 2090 (class 0 OID 0)
-- Dependencies: 211
-- Name: COLUMN student_results_lateraltransferlookup.lateraltransferid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransferlookup.lateraltransferid IS 'FK to student_results_lateraltransfer table uid.';


--
-- TOC entry 2091 (class 0 OID 0)
-- Dependencies: 211
-- Name: COLUMN student_results_lateraltransferlookup.key; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransferlookup.key IS 'Key which labels the parameter being saved.';


--
-- TOC entry 2092 (class 0 OID 0)
-- Dependencies: 211
-- Name: COLUMN student_results_lateraltransferlookup.value; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_lateraltransferlookup.value IS 'Value which stores the actual parameter.';


--
-- TOC entry 210 (class 1259 OID 49280)
-- Name: student_results_lateraltransferlookup_uid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE student_results_lateraltransferlookup_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.student_results_lateraltransferlookup_uid_seq OWNER TO postgres;

--
-- TOC entry 2093 (class 0 OID 0)
-- Dependencies: 210
-- Name: student_results_lateraltransferlookup_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE student_results_lateraltransferlookup_uid_seq OWNED BY student_results_lateraltransferlookup.uid;


--
-- TOC entry 2078 (class 2604 OID 49285)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_results_lateraltransferlookup ALTER COLUMN uid SET DEFAULT nextval('student_results_lateraltransferlookup_uid_seq'::regclass);



--
-- TOC entry 2094 (class 0 OID 0)
-- Dependencies: 210
-- Name: student_results_lateraltransferlookup_uid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('student_results_lateraltransferlookup_uid_seq', 1, false);


--
-- TOC entry 2080 (class 2606 OID 49290)
-- Name: student_results_lateraltransferlookup_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY student_results_lateraltransferlookup
    ADD CONSTRAINT student_results_lateraltransferlookup_pkey PRIMARY KEY (uid);


--
-- TOC entry 2081 (class 2606 OID 49291)
-- Name: student_results_lateraltransferlookup_lateraltransferid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_results_lateraltransferlookup
    ADD CONSTRAINT student_results_lateraltransferlookup_lateraltransferid_fkey FOREIGN KEY (lateraltransferid) REFERENCES student_results_lateraltransfer(uid);


-- Completed on 2013-06-19 19:12:09

--
-- PostgreSQL database dump complete
--

