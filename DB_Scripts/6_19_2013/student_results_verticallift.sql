--
-- PostgreSQL database dump
--

-- Dumped from database version 9.2.2
-- Dumped by pg_dump version 9.2.2
-- Started on 2013-06-19 19:13:44

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 223 (class 1259 OID 57790)
-- Name: student_results_verticallift; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE student_results_verticallift (
    uid integer NOT NULL,
    studentid integer NOT NULL,
    masterylevel smallint NOT NULL,
    taskmode smallint NOT NULL,
    score double precision NOT NULL,
    lessonstarttime timestamp without time zone NOT NULL,
    lessonendtime timestamp without time zone NOT NULL
);


ALTER TABLE public.student_results_verticallift OWNER TO postgres;

--
-- TOC entry 2088 (class 0 OID 0)
-- Dependencies: 223
-- Name: TABLE student_results_verticallift; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE student_results_verticallift IS 'Top-Level Vertical Lift Results Table. Stores detailed and unique stage results in a column-oriented design.';


--
-- TOC entry 2089 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN student_results_verticallift.uid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_verticallift.uid IS 'Unique Primary Key ID for student_results_verticallift table.';


--
-- TOC entry 2090 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN student_results_verticallift.studentid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_verticallift.studentid IS 'FK to student table uid.';


--
-- TOC entry 2091 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN student_results_verticallift.masterylevel; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_verticallift.masterylevel IS 'MasteryLevel enum (Novice, Intermediate, Master) represented as an integer value.';


--
-- TOC entry 2092 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN student_results_verticallift.taskmode; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_verticallift.taskmode IS 'TaskMode enum (Practice, Test) represented as an integer value.';


--
-- TOC entry 2093 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN student_results_verticallift.score; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_verticallift.score IS 'Total computed single number score for Vertical Lift stage.';


--
-- TOC entry 2094 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN student_results_verticallift.lessonstarttime; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_verticallift.lessonstarttime IS 'Lesson Start Date and Time.';


--
-- TOC entry 2095 (class 0 OID 0)
-- Dependencies: 223
-- Name: COLUMN student_results_verticallift.lessonendtime; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_verticallift.lessonendtime IS 'Lesson End Date and Time.';


--
-- TOC entry 222 (class 1259 OID 57788)
-- Name: student_results_verticallift_uid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE student_results_verticallift_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.student_results_verticallift_uid_seq OWNER TO postgres;

--
-- TOC entry 2096 (class 0 OID 0)
-- Dependencies: 222
-- Name: student_results_verticallift_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE student_results_verticallift_uid_seq OWNED BY student_results_verticallift.uid;


--
-- TOC entry 2078 (class 2604 OID 57793)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_results_verticallift ALTER COLUMN uid SET DEFAULT nextval('student_results_verticallift_uid_seq'::regclass);



--
-- TOC entry 2097 (class 0 OID 0)
-- Dependencies: 222
-- Name: student_results_verticallift_uid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('student_results_verticallift_uid_seq', 1, false);


--
-- TOC entry 2080 (class 2606 OID 57795)
-- Name: student_results_verticallift_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY student_results_verticallift
    ADD CONSTRAINT student_results_verticallift_pkey PRIMARY KEY (uid);


--
-- TOC entry 2081 (class 2606 OID 57796)
-- Name: student_results_verticallift_studentid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_results_verticallift
    ADD CONSTRAINT student_results_verticallift_studentid_fkey FOREIGN KEY (studentid) REFERENCES students(uid);


-- Completed on 2013-06-19 19:13:46

--
-- PostgreSQL database dump complete
--

