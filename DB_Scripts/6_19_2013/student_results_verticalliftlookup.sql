--
-- PostgreSQL database dump
--

-- Dumped from database version 9.2.2
-- Dumped by pg_dump version 9.2.2
-- Started on 2013-06-19 19:14:09

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 225 (class 1259 OID 57803)
-- Name: student_results_verticalliftlookup; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE student_results_verticalliftlookup (
    uid integer NOT NULL,
    verticalliftid integer NOT NULL,
    key text NOT NULL,
    value text NOT NULL
);


ALTER TABLE public.student_results_verticalliftlookup OWNER TO postgres;

--
-- TOC entry 2088 (class 0 OID 0)
-- Dependencies: 225
-- Name: TABLE student_results_verticalliftlookup; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE student_results_verticalliftlookup IS 'Key-Value Table for storing lookup data per module in a row-oriented configuration.  Table is related to the student_results_verticallift table.';


--
-- TOC entry 2089 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN student_results_verticalliftlookup.uid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_verticalliftlookup.uid IS 'Unique Primary Key ID for student_results_verticalliftlookup table.';


--
-- TOC entry 2090 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN student_results_verticalliftlookup.verticalliftid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_verticalliftlookup.verticalliftid IS 'FK to student_results_verticallift table uid.';


--
-- TOC entry 2091 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN student_results_verticalliftlookup.key; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_verticalliftlookup.key IS 'Key which labels the parameter being saved.';


--
-- TOC entry 2092 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN student_results_verticalliftlookup.value; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN student_results_verticalliftlookup.value IS 'Value which stores the actual parameter.';


--
-- TOC entry 224 (class 1259 OID 57801)
-- Name: student_results_verticalliftlookup_uid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE student_results_verticalliftlookup_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.student_results_verticalliftlookup_uid_seq OWNER TO postgres;

--
-- TOC entry 2093 (class 0 OID 0)
-- Dependencies: 224
-- Name: student_results_verticalliftlookup_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE student_results_verticalliftlookup_uid_seq OWNED BY student_results_verticalliftlookup.uid;


--
-- TOC entry 2078 (class 2604 OID 57806)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_results_verticalliftlookup ALTER COLUMN uid SET DEFAULT nextval('student_results_verticalliftlookup_uid_seq'::regclass);



--
-- TOC entry 2094 (class 0 OID 0)
-- Dependencies: 224
-- Name: student_results_verticalliftlookup_uid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('student_results_verticalliftlookup_uid_seq', 1, false);


--
-- TOC entry 2080 (class 2606 OID 57811)
-- Name: student_results_verticalliftlookup_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY student_results_verticalliftlookup
    ADD CONSTRAINT student_results_verticalliftlookup_pkey PRIMARY KEY (uid);


--
-- TOC entry 2081 (class 2606 OID 57812)
-- Name: student_results_verticalliftlookup_verticalliftid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY student_results_verticalliftlookup
    ADD CONSTRAINT student_results_verticalliftlookup_verticalliftid_fkey FOREIGN KEY (verticalliftid) REFERENCES student_results_verticallift(uid);


-- Completed on 2013-06-19 19:14:10

--
-- PostgreSQL database dump complete
--

