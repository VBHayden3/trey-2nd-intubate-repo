﻿-- Add new columns and keys to student_results_cpr table

ALTER TABLE student_results_cpr
ADD COLUMN calibrationimageid integer;

ALTER TABLE student_results_cpr
ADD CONSTRAINT kinect_images_fk
	FOREIGN KEY (calibrationimageid)
	REFERENCES kinect_images(uid);

ALTER TABLE student_results_cpr
ADD COLUMN askedforaed boolean;

ALTER TABLE student_results_cpr
ADD COLUMN askedfor911 boolean;

ALTER TABLE student_results_cpr
ADD COLUMN askedifok boolean;

ALTER TABLE student_results_cpr
ADD COLUMN checkpulseimageid integer;

ALTER TABLE student_results_cpr
ADD CONSTRAINT kinect_images_fk2
	FOREIGN KEY (checkpulseimageid)
	REFERENCES kinect_images(uid);

CREATE TYPE checkpulse AS ENUM ('no', 'incorrectly', 'correctly');

ALTER TABLE student_results_cpr
ADD COLUMN checkedpulse checkpulse;

-- Create child compressions table for student_results_cpr table.

CREATE TABLE student_results_cpr_compressions (
	compressionset integer,
	student_results_cpr_uid integer,
	avgdepth double precision,
	avgrate double precision,
	avgarmslocked double precision,
	avgshouldersovermanikin double precision,
	score double precision,
	compressionimageid integer,
	CONSTRAINT student_results_cpr_compressions_pk PRIMARY KEY (compressionset, student_results_cpr_uid),
	CONSTRAINT student_results_cpr_fk
		FOREIGN KEY (student_results_cpr_uid)
		REFERENCES student_results_cpr(uid),
	CONSTRAINT kinect_images_fk
		FOREIGN KEY (compressionimageid)
		REFERENCES kinect_images(uid)
);

-- Add unique constraints

ALTER TABLE student_results_cpr ADD CONSTRAINT student_results_cpr_unique 
UNIQUE (calibrationimageid, checkpulseimageid);

ALTER TABLE student_results_cpr_compressions ADD CONSTRAINT student_results_cpr_compressions_unique
UNIQUE (compressionimageid);

-- Add comments to columns 

COMMENT ON COLUMN student_results_cpr.calibrationimageid
IS 'FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';

COMMENT ON COLUMN student_results_cpr.askedforaed
IS 'CPR asked for AED value.';

COMMENT ON COLUMN student_results_cpr.askedfor911
IS 'CPR asked for 911 value.';

COMMENT ON COLUMN student_results_cpr.askedifok
IS 'CPR asked if OK value.';

COMMENT ON COLUMN student_results_cpr.checkpulseimageid
IS 'FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';

COMMENT ON COLUMN student_results_cpr.checkedpulse
IS 'CPR checked pulse value. ENUM (no, incorrectly, correctly) ';

COMMENT ON COLUMN student_results_cpr_compressions.compressionset
IS 'Part of the student_results_cpr_compressions PK. Also identifies the ordering of compression sets for a CPR Lesson run.';

COMMENT ON COLUMN student_results_cpr_compressions.student_results_cpr_uid
IS 'FK to student_results_cpr table. Also part of student_results_cpr_compressions PK.';

COMMENT ON COLUMN student_results_cpr_compressions.avgdepth
IS 'Average depth of compressions for a CPR set of compressions.';

COMMENT ON COLUMN student_results_cpr_compressions.avgarmslocked
IS 'Percent average of arms being locked across compressions for a CPR set of compressions.';

COMMENT ON COLUMN student_results_cpr_compressions.avgshouldersovermanikin
IS 'Percent average of shoulders being over the manikin across compressions for a CPR setof compressions.';

COMMENT ON COLUMN student_results_cpr_compressions.score
IS 'Score for a set of CPR compressions.';

COMMENT ON COLUMN student_results_cpr_compressions.compressionimageid
IS 'FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';