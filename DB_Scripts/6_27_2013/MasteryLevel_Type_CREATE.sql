﻿-- Type: "MasteryLevel"

-- DROP TYPE "MasteryLevel";

CREATE TYPE "MasteryLevel" AS ENUM
   ('Novice',
    'Intermediate',
    'Master');
ALTER TYPE "MasteryLevel"
  OWNER TO postgres;
COMMENT ON TYPE "MasteryLevel"
  IS 'AIMS User Training Level.';
