﻿-- Type: "TaskType"

-- DROP TYPE "TaskType";

CREATE TYPE "TaskType" AS ENUM
   ('CPR',
    'Intubation',
    'LateralTransfer',
    'VerticalLift');
ALTER TYPE "TaskType"
  OWNER TO postgres;
COMMENT ON TYPE "TaskType"
  IS 'AIMS Lesson Type.';
