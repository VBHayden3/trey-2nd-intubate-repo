﻿-- Type: checkpulse

-- DROP TYPE checkpulse;

CREATE TYPE checkpulse AS ENUM
   ('no',
    'incorrectly',
    'correctly');
ALTER TYPE checkpulse
  OWNER TO postgres;
