﻿INSERT INTO taskresults(
            uid, studentid, score, submissiontime, starttime, endtime)
  SELECT uid, studentid, totalscore, date, date, date
    FROM student_results WHERE uid > 0;
