﻿-- Table: taskresults_intubation

-- DROP TABLE taskresults_intubation;

CREATE TABLE taskresults_intubation
(
  uid serial NOT NULL, -- Unique Primary Key ID for taskresults_intubation table.
  resultid integer NOT NULL, -- FK to taskresults table uid.
  score1 double precision NOT NULL DEFAULT 0,
  score2 double precision NOT NULL DEFAULT 0,
  score3 double precision NOT NULL DEFAULT 0,
  score4 double precision NOT NULL DEFAULT 0,
  score5 double precision NOT NULL DEFAULT 0,
  score6 double precision NOT NULL DEFAULT 0,
  score7 double precision NOT NULL DEFAULT 0,
  score8 double precision NOT NULL DEFAULT 0,
  score9 double precision NOT NULL DEFAULT 0,
  score10 double precision NOT NULL DEFAULT 0,
  score11 double precision NOT NULL DEFAULT 0,
  score12 double precision NOT NULL DEFAULT 0,
  bool boolean NOT NULL,
  time1 timestamp without time zone NOT NULL DEFAULT now(),
  time2 timestamp without time zone NOT NULL DEFAULT now(),
  time3 timestamp without time zone NOT NULL DEFAULT now(),
  calibrationimageid integer,
  image1id integer,
  image2id integer,
  image3id integer,
  image4id integer,
  image5id integer,
  image6id integer,
  image7id integer,
  image8id integer,
  image9id integer,
  image10id integer,
  image11id integer,
  image12id integer,
  overallfeedbackids integer[],
  feedback1ids integer[],
  feedback2ids integer[],
  CONSTRAINT taskresults_intubation_pkey PRIMARY KEY (uid),
  CONSTRAINT taskresults_intubation_calibrationimageid_fkey FOREIGN KEY (calibrationimageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_intubation_image10id_fkey FOREIGN KEY (image10id)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_intubation_image11id_fkey FOREIGN KEY (image11id)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_intubation_image12id_fkey FOREIGN KEY (image12id)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_intubation_image1id_fkey FOREIGN KEY (image1id)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_intubation_image2id_fkey FOREIGN KEY (image2id)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_intubation_image3id_fkey FOREIGN KEY (image3id)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_intubation_image4id_fkey FOREIGN KEY (image4id)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_intubation_image5id_fkey FOREIGN KEY (image5id)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_intubation_image6id_fkey FOREIGN KEY (image6id)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_intubation_image7id_fkey FOREIGN KEY (image7id)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_intubation_image8id_fkey FOREIGN KEY (image8id)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_intubation_image9id_fkey FOREIGN KEY (image9id)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_intubation_resultid_fkey FOREIGN KEY (resultid)
      REFERENCES taskresults (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_intubation_calibrationimageid_key UNIQUE (calibrationimageid),
  CONSTRAINT taskresults_intubation_image10id_key UNIQUE (image10id),
  CONSTRAINT taskresults_intubation_image11id_key UNIQUE (image11id),
  CONSTRAINT taskresults_intubation_image12id_key UNIQUE (image12id),
  CONSTRAINT taskresults_intubation_image1id_key UNIQUE (image1id),
  CONSTRAINT taskresults_intubation_image2id_key UNIQUE (image2id),
  CONSTRAINT taskresults_intubation_image3id_key UNIQUE (image3id),
  CONSTRAINT taskresults_intubation_image4id_key UNIQUE (image4id),
  CONSTRAINT taskresults_intubation_image5id_key UNIQUE (image5id),
  CONSTRAINT taskresults_intubation_image6id_key UNIQUE (image6id),
  CONSTRAINT taskresults_intubation_image7id_key UNIQUE (image7id),
  CONSTRAINT taskresults_intubation_image8id_key UNIQUE (image8id),
  CONSTRAINT taskresults_intubation_image9id_key UNIQUE (image9id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE taskresults_intubation
  OWNER TO postgres;
COMMENT ON TABLE taskresults_intubation
  IS 'Intubation Results Table. Stores detailed and unique stage results in a column-oriented design.';
COMMENT ON COLUMN taskresults_intubation.uid IS 'Unique Primary Key ID for taskresults_intubation table.';
COMMENT ON COLUMN taskresults_intubation.resultid IS 'FK to taskresults table uid.';

