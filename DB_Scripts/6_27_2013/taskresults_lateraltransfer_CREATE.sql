﻿-- Table: taskresults_lateraltransfer

-- DROP TABLE taskresults_lateraltransfer;

CREATE TABLE taskresults_lateraltransfer
(
  uid serial NOT NULL, -- Unique Primary Key ID for taskresults_lateraltransfer table.
  resultid integer NOT NULL, -- FK to taskresults table uid.
  handplacementscore double precision NOT NULL DEFAULT 0, -- Total computed single number score for lateral transfer hand placement stage.
  verbalcount boolean NOT NULL, -- Boolean indication for lateral transfer vocal pull stage.
  verbalcounttime timestamp without time zone NOT NULL DEFAULT now(), -- Lateral Transfer vocal pull stage Date and Time.
  verbalcountscore double precision NOT NULL DEFAULT 0, -- Verbal Count score for lateral transfer vocal pull.
  pullstarttime timestamp without time zone NOT NULL DEFAULT now(), -- Lateral Transfer pull detected start Date and Time.
  pullendtime timestamp without time zone NOT NULL DEFAULT now(), -- Lateral Transfer pull ended Date and Time.
  totalsync double precision NOT NULL DEFAULT 0, -- Lateral Transfer Total Sync value.
  rhandsync double precision NOT NULL DEFAULT 0, -- Lateral Transfer Right Hand Sync value.
  lhandsync double precision NOT NULL DEFAULT 0, -- Lateral Transfer Left Hand Sync value.
  rwristsync double precision NOT NULL DEFAULT 0, -- Lateral Transfer Right Wrist Sync value.
  lwristsync double precision NOT NULL DEFAULT 0, -- Lateral Transfer Left Wrist Sync value.
  relbowsync double precision NOT NULL DEFAULT 0, -- Lateral Transfer Right Elbow Sync value.
  lelbowsync double precision NOT NULL DEFAULT 0, -- Lateral Transfer Left Elbow Sync value.
  rshouldersync double precision NOT NULL DEFAULT 0, -- Lateral Transfer Right Shoulder Sync value.
  lshouldersync double precision NOT NULL DEFAULT 0, -- Lateral Transfer Left Shoulder Sync value.
  manikinsync double precision NOT NULL DEFAULT 0, -- Lateral Transfer Manikin Torso-to-Leg Sync value.
  calibrationimageid integer, -- FK to kinect_images table uid.  Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.
  handplacementimageid integer, -- FK to kinect_images table uid.  Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.
  pullstartimageid integer, -- FK to kinect_images table uid.  Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.
  pullendimageid integer, -- FK to kinect_images table uid.  Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.
  overallfeedbackids integer[], -- List of Feedback IDs. Relates to the feedback strings relevant to the results. Feedback strings are stored in the lateral transfer lookup table.
  handplacementfeedbackids integer[], -- List of Feedback IDs. Relates to the feedback strings relevant to the results. Feedback strings are stored in the lateral transfer lookup table.
  pullfeedbackids integer[], -- List of Feedback IDs. Relates to the feedback strings relevant to the results. Feedback strings are stored in the lateral transfer lookup table.
  CONSTRAINT taskresults_lateraltransfer_pkey PRIMARY KEY (uid),
  CONSTRAINT taskresults_lateraltransfer_calibrationimageid_fkey FOREIGN KEY (calibrationimageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_lateraltransfer_handplacementimageid_fkey FOREIGN KEY (handplacementimageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_lateraltransfer_pullendimageid_fkey FOREIGN KEY (pullendimageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_lateraltransfer_pullstartimageid_fkey FOREIGN KEY (pullstartimageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_lateraltransfer_resultid_fkey FOREIGN KEY (resultid)
      REFERENCES taskresults (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_lateraltransfer_calibrationimageid_key UNIQUE (calibrationimageid),
  CONSTRAINT taskresults_lateraltransfer_handplacementimageid_key UNIQUE (handplacementimageid),
  CONSTRAINT taskresults_lateraltransfer_pullendimageid_key UNIQUE (pullendimageid),
  CONSTRAINT taskresults_lateraltransfer_pullstartimageid_key UNIQUE (pullstartimageid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE taskresults_lateraltransfer
  OWNER TO postgres;
COMMENT ON TABLE taskresults_lateraltransfer
  IS 'Lateral Transfer Results Table. Stores detailed and unique stage results in a column-oriented design.';
COMMENT ON COLUMN taskresults_lateraltransfer.uid IS 'Unique Primary Key ID for taskresults_lateraltransfer table.';
COMMENT ON COLUMN taskresults_lateraltransfer.resultid IS 'FK to taskresults table uid.';
COMMENT ON COLUMN taskresults_lateraltransfer.handplacementscore IS 'Total computed single number score for lateral transfer hand placement stage.';
COMMENT ON COLUMN taskresults_lateraltransfer.verbalcount IS 'Boolean indication for lateral transfer vocal pull stage.';
COMMENT ON COLUMN taskresults_lateraltransfer.verbalcounttime IS 'Lateral Transfer vocal pull stage Date and Time.';
COMMENT ON COLUMN taskresults_lateraltransfer.verbalcountscore IS 'Verbal Count score for lateral transfer vocal pull.';
COMMENT ON COLUMN taskresults_lateraltransfer.pullstarttime IS 'Lateral Transfer pull detected start Date and Time.';
COMMENT ON COLUMN taskresults_lateraltransfer.pullendtime IS 'Lateral Transfer pull ended Date and Time.';
COMMENT ON COLUMN taskresults_lateraltransfer.totalsync IS 'Lateral Transfer Total Sync value.';
COMMENT ON COLUMN taskresults_lateraltransfer.rhandsync IS 'Lateral Transfer Right Hand Sync value.';
COMMENT ON COLUMN taskresults_lateraltransfer.lhandsync IS 'Lateral Transfer Left Hand Sync value.';
COMMENT ON COLUMN taskresults_lateraltransfer.rwristsync IS 'Lateral Transfer Right Wrist Sync value.';
COMMENT ON COLUMN taskresults_lateraltransfer.lwristsync IS 'Lateral Transfer Left Wrist Sync value.';
COMMENT ON COLUMN taskresults_lateraltransfer.relbowsync IS 'Lateral Transfer Right Elbow Sync value.';
COMMENT ON COLUMN taskresults_lateraltransfer.lelbowsync IS 'Lateral Transfer Left Elbow Sync value.';
COMMENT ON COLUMN taskresults_lateraltransfer.rshouldersync IS 'Lateral Transfer Right Shoulder Sync value.';
COMMENT ON COLUMN taskresults_lateraltransfer.lshouldersync IS 'Lateral Transfer Left Shoulder Sync value.';
COMMENT ON COLUMN taskresults_lateraltransfer.manikinsync IS 'Lateral Transfer Manikin Torso-to-Leg Sync value.';
COMMENT ON COLUMN taskresults_lateraltransfer.calibrationimageid IS 'FK to kinect_images table uid.  Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';
COMMENT ON COLUMN taskresults_lateraltransfer.handplacementimageid IS 'FK to kinect_images table uid.  Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';
COMMENT ON COLUMN taskresults_lateraltransfer.pullstartimageid IS 'FK to kinect_images table uid.  Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';
COMMENT ON COLUMN taskresults_lateraltransfer.pullendimageid IS 'FK to kinect_images table uid.  Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';
COMMENT ON COLUMN taskresults_lateraltransfer.overallfeedbackids IS 'List of Feedback IDs. Relates to the feedback strings relevant to the results. Feedback strings are stored in the lateral transfer lookup table.';
COMMENT ON COLUMN taskresults_lateraltransfer.handplacementfeedbackids IS 'List of Feedback IDs. Relates to the feedback strings relevant to the results. Feedback strings are stored in the lateral transfer lookup table.';
COMMENT ON COLUMN taskresults_lateraltransfer.pullfeedbackids IS 'List of Feedback IDs. Relates to the feedback strings relevant to the results. Feedback strings are stored in the lateral transfer lookup table.';

