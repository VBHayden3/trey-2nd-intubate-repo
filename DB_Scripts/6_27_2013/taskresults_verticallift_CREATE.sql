﻿-- Table: taskresults_verticallift

-- DROP TABLE taskresults_verticallift;

CREATE TABLE taskresults_verticallift
(
  uid serial NOT NULL, -- Unique Primary Key ID for taskresults_verticallift table.
  resultid integer NOT NULL, -- FK to taskresults table uid.
  accuracy double precision NOT NULL DEFAULT 0, -- Vertical Lift Accuracy.
  verticalliftgrade double precision NOT NULL DEFAULT 0, -- Vertical Lift Grade.
  repgrade double precision NOT NULL DEFAULT 0, -- Vertical Lift Rep Grade.
  calibrationimageid integer, -- FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.
  stage1imageid integer, -- FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.
  stage2imageid integer, -- FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.
  stage3imageid integer, -- FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.
  stage4imageid integer, -- FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.
  CONSTRAINT taskresults_verticallift_pkey PRIMARY KEY (uid),
  CONSTRAINT kinect_images_fk FOREIGN KEY (calibrationimageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT kinect_images_fk1 FOREIGN KEY (stage1imageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT kinect_images_fk2 FOREIGN KEY (stage2imageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT kinect_images_fk3 FOREIGN KEY (stage3imageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT kinect_images_fk4 FOREIGN KEY (stage4imageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_verticallift_resultsid_fkey FOREIGN KEY (resultid)
      REFERENCES taskresults (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_verticallift_unique UNIQUE (calibrationimageid, stage1imageid, stage2imageid, stage3imageid, stage4imageid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE taskresults_verticallift
  OWNER TO postgres;
COMMENT ON TABLE taskresults_verticallift
  IS 'Vertical Lift Results Table. Stores detailed and unique stage results in a column-oriented design.';
COMMENT ON COLUMN taskresults_verticallift.uid IS 'Unique Primary Key ID for taskresults_verticallift table.';
COMMENT ON COLUMN taskresults_verticallift.resultid IS 'FK to taskresults table uid.';
COMMENT ON COLUMN taskresults_verticallift.accuracy IS 'Vertical Lift Accuracy.';
COMMENT ON COLUMN taskresults_verticallift.verticalliftgrade IS 'Vertical Lift Grade.';
COMMENT ON COLUMN taskresults_verticallift.repgrade IS 'Vertical Lift Rep Grade.';
COMMENT ON COLUMN taskresults_verticallift.calibrationimageid IS 'FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';
COMMENT ON COLUMN taskresults_verticallift.stage1imageid IS 'FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';
COMMENT ON COLUMN taskresults_verticallift.stage2imageid IS 'FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';
COMMENT ON COLUMN taskresults_verticallift.stage3imageid IS 'FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';
COMMENT ON COLUMN taskresults_verticallift.stage4imageid IS 'FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';

