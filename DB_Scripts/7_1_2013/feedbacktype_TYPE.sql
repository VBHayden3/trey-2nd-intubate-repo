﻿-- Type: "FeedbackType"

-- DROP TYPE "FeedbackType";

CREATE TYPE "FeedbackType" AS ENUM
   ('None',
    'Good',
    'Bad',
    'Neutral');
ALTER TYPE "FeedbackType"
  OWNER TO postgres;
