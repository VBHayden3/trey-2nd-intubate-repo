﻿-- Table: taskresults_advice

-- DROP TABLE taskresults_advice;

CREATE TABLE taskresults_advice
(
  uid serial NOT NULL, -- unique identifier
  feedbackid integer, -- links the advice back to the feedbackid
  tasktype "TaskType" NOT NULL, -- fulfills the 2nd portion of the feedbackid's primary key pointer.
  advice text, -- the actual advice entry
  CONSTRAINT taskadvice_fkey PRIMARY KEY (uid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE taskresults_advice
  OWNER TO aims;
COMMENT ON TABLE taskresults_advice
  IS 'advice table linking to specific feedback of a task';
COMMENT ON COLUMN taskresults_advice.uid IS 'unique identifier';
COMMENT ON COLUMN taskresults_advice.feedbackid IS 'links the advice back to the feedbackid';
COMMENT ON COLUMN taskresults_advice.tasktype IS 'fulfills the 2nd portion of the feedbackid''s primary key pointer.';
COMMENT ON COLUMN taskresults_advice.advice IS 'the actual advice entry';

