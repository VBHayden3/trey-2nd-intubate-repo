﻿-- Table: taskresults_feedback

-- DROP TABLE taskresults_feedback;

CREATE TABLE taskresults_feedback
(
  uid serial NOT NULL, -- unique identifier
  tasktype "TaskType" NOT NULL,
  feedbacktype "FeedbackType" NOT NULL DEFAULT 'None'::"FeedbackType", -- feedback type of the entry
  feedback text, -- the actuall feedback entry
  CONSTRAINT taskfeedback_pkey PRIMARY KEY (uid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE taskresults_feedback
  OWNER TO aims;
COMMENT ON COLUMN taskresults_feedback.uid IS 'unique identifier';
COMMENT ON COLUMN taskresults_feedback.feedbacktype IS 'feedback type of the entry';
COMMENT ON COLUMN taskresults_feedback.feedback IS 'the actuall feedback entry';

