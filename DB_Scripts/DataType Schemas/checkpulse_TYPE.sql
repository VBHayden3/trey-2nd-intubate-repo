-- Type: checkpulse

-- DROP TYPE checkpulse;

CREATE TYPE "CheckPulse" AS ENUM
   ('No',
    'Incorrectly',
    'Correctly');
ALTER TYPE CheckPulse
  OWNER TO aims;
COMMENT ON TYPE "CheckPulse"
  IS 'State of pulse being checked.';  
