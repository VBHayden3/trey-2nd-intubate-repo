-- Type: "FootSpacing"

-- DROP TYPE "FootSpacing";

CREATE TYPE "FootSpacing" AS ENUM
   ('Ok',
    'Narrow',
    'Wide');
ALTER TYPE "FootSpacing"
  OWNER TO postgres;
COMMENT ON TYPE "FootSpacing"
  IS 'Represents the stance of a users feet.';
