-- Type: "TaskMode"

-- DROP TYPE "TaskMode";

CREATE TYPE "TaskMode" AS ENUM
   ('Practice',
    'Test');
ALTER TYPE "TaskMode"
  OWNER TO aims;
COMMENT ON TYPE "TaskMode"
  IS 'AIMS Training Mode.';
