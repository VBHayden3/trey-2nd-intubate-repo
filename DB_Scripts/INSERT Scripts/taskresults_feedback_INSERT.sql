--
-- PostgreSQL database dump
--

-- Dumped from database version 9.2.3
-- Dumped by pg_dump version 9.2.3
-- Started on 2013-07-15 03:27:47
--
-- TOC entry 2091 (class 0 OID 41226)
-- Dependencies: 215
-- Data for Name: taskresults_feedback; Type: TABLE DATA; Schema: public; Owner: aims
--

INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (1, 'LateralTransfer', 'Good', 'You were in proper position prior to the transfer.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (2, 'LateralTransfer', 'Good', 'Your partner was in proper position prior to the transfer.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (3, 'LateralTransfer', 'Good', 'Both users were properly in position prior to the transfer.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (4, 'LateralTransfer', 'Bad', 'Your hands were out of position prior to the transfer.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (5, 'LateralTransfer', 'Bad', 'Your partner''s hands were out of position prior to the transfer.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (6, 'LateralTransfer', 'Good', 'You verbally coordinated the transfer.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (7, 'LateralTransfer', 'Bad', 'You did not verbally coordinate the transfer.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (8, 'LateralTransfer', 'Bad', 'You did not verbally coordinate the transfer prior to the pull.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (9, 'LateralTransfer', 'Good', 'You and your partner''s movements were synchronous during the transfer.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (10, 'LateralTransfer', 'Bad', 'You and your partner''s movements were non-synchronous during the transfer.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (11, 'LateralTransfer', 'Good', 'You have properly performed Lateral Transfer.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (12, 'LateralTransfer', 'Bad', 'You have improperly performed Lateral Transfer.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (13, 'LateralTransfer', 'Bad', 'Not in proper position prior to the pull.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (17, 'LateralTransfer', 'Bad', 'Your partner''s body was out of position prior to the transfer.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (16, 'LateralTransfer', 'Bad', 'Your partner''s arms were out of position prior to the transfer.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (15, 'LateralTransfer', 'Bad', 'Your body was out of position prior to the transfer.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (14, 'LateralTransfer', 'Bad', 'Your arms were out of position prior to the transfer.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (18, 'Intubation', 'Good', 'You have properly performed Intubation.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (19, 'Intubation', 'Bad', 'You have improperly performed Intubation.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (21, 'Intubation', 'Neutral', 'You have effectively performed Intubation, but in an untimely fashon.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (22, 'Intubation', 'Good', 'You have tilted the patient''s head into sniffing position.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (23, 'Intubation', 'Bad', 'You did not tilt the patient''s head back into sniffing position.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (24, 'Intubation', 'Bad', 'You have improperly tilted the patient''s head.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (20, 'Intubation', 'Neutral', 'Your Intubation was effective, but improper.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (25, 'Intubation', 'Bad', 'You did not perform the Intubation in a timely fashion.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (26, 'Intubation', 'Bad', 'You did not use proper technique while Intubating.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (27, 'Intubation', 'Bad', 'You did not use proper tool positioning while Intubating.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (28, 'Intubation', 'Good', 'You have grabbed the Laryngoscope with the correct hand.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (29, 'Intubation', 'Bad', 'You have grabbed the Laryngoscope with the incorrect hand.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (30, 'Intubation', 'Good', 'The Laryngoscope was picked up on the correct side.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (31, 'Intubation', 'Bad', 'The Laryngoscope was picked up on the incorrect side.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (32, 'Intubation', 'Neutral', 'Unable to ascertain which side the laryngoscope was grabbed on.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (33, 'Intubation', 'Neutral', 'Unable to ascertain which hand is grabbing the Laryngoscope.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (34, 'Intubation', 'Good', 'You have properly inserted the Laryngoscope.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (35, 'Intubation', 'Bad', 'You have improperly inserted the Laryngoscope.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (36, 'Intubation', 'Bad', 'The Laryngoscope potentially damaged the upper teeth.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (37, 'Intubation', 'Bad', 'The Laryngoscope potentially damaged the lower teeth.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (38, 'Intubation', 'Bad', 'The Laryngoscope was inserted incorrectly to the right of the patient''s mouth.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (39, 'Intubation', 'Bad', 'The Laryngoscope was inserted incorrectly to the left of the patient''s mouth.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (40, 'Intubation', 'Good', 'The vocal chords were correctly viewed.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (41, 'Intubation', 'Bad', 'The vocal chords were not viewed.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (43, 'Intubation', 'Bad', 'The Endotracheal Tube was grabbed with the incorrect hand.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (42, 'Intubation', 'Good', 'The Endotracheal Tube was grabbed with the proper hand.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (44, 'Intubation', 'Good', 'The Endotracheal Tube was correctly inserted into the patient''s mouth.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (45, 'Intubation', 'Bad', 'The Endotracheal Tube was improperly inserted into the right side of the patient''s mouth.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (46, 'Intubation', 'Bad', 'The Endotracheal Tube was improperly inserted into the left side of the patient''s mouth.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (47, 'Intubation', 'Bad', 'The Endotracheal Tube wasn''t inserted into the patient''s mouth.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (48, 'Intubation', 'Good', 'The Laryngoscope was removed properly with the left hand.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (49, 'Intubation', 'Bad', 'The Laryngoscope was removed improperly with the right hand.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (50, 'Intubation', 'Good', 'The Laryngoscope was removed.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (51, 'Intubation', 'Bad', 'The Laryngoscope was not removed.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (52, 'Intubation', 'Neutral', 'A lifting motion was detected, but the stylet was not located.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (53, 'Intubation', 'Good', 'The Stylet was removed.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (54, 'Intubation', 'Bad', 'The Stylet was not removed.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (55, 'Intubation', 'Good', 'The Syringe was used to inflate the Baloon Cuff.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (56, 'Intubation', 'Bad', 'The Syringe was not used.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (57, 'Intubation', 'Good', 'The Valve Bag was attached to the Endotracheal Tube.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (58, 'Intubation', 'Bad', 'The Valve Bag was not attached to the Endotracheal Tube.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (59, 'Intubation', 'Good', 'Chest rises were observed, indicating a properly inserted Endotracheal Tube.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (60, 'Intubation', 'Bad', 'Stomach rise was observed, indicating an improperly inserted Endotracheal Tube.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (61, 'Intubation', 'Bad', 'The stage timed out and the lungs were not properly ventilated.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (62, 'CPR', 'Bad', 'The patient was not asked if they were okay. It is important to establish that the patient is indeed in need of assistance');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (63, 'CPR', 'Bad', 'AIMI was never told to call 911. Trained assistance should be notified immediately.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (64, 'CPR', 'Bad', 'AIMI was not told to get a defibrillator. A defibrillator may be necessary if CPR alone does not resuscitate the patient.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (65, 'CPR', 'Good', 'The patient was asked if they were okay, AIMI was told to call 911, and AIMI was told to get a defibrillator. Good communication was established.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (66, 'CPR', 'Bad', 'The patient''s pulse was not checked. CPR should only be performed if the patient''s pulse is absent.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (68, 'CPR', 'Good', 'Your shoulders were correctly positioned directly over the patient''s chest.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (69, 'CPR', 'Bad', 'Your shoulders were not positioned directly over the patient''s chest.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (67, 'CPR', 'Good', 'The patients pulse was checked, ensuring that it was absent.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (70, 'CPR', 'Bad', 'Your elbows did not remain locked while performing compressions.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (71, 'CPR', 'Good', 'Your elbows stayed straight while performing compressions.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (72, 'CPR', 'Good', 'Compressions on average reached a depth of at least two inches.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (73, 'CPR', 'Bad', 'During compressions, your hands were lifted above the patient''s chest.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (74, 'CPR', 'Bad', 'The depth of compressions, on average, were less than two inches.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (75, 'CPR', 'Good', 'Rate of compressions was at least 100 compressions per minute.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (76, 'CPR', 'Bad', 'Rate of compressions was less than 100 compressions per minute.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (77, 'Intubation', 'Bad', 'The Laryngoscope potentially damaged the upper teeth while inserting the Endotracheal Tube.');
INSERT INTO taskresults_feedback (uid, tasktype, feedbacktype, feedback) VALUES (78, 'Intubation', 'Bad', 'The Laryngoscope potentially damaged the lower teeth while inserting the Endotracheal Tube.');


--
-- TOC entry 2097 (class 0 OID 0)
-- Dependencies: 216
-- Name: taskresults_feedback_uid_seq; Type: SEQUENCE SET; Schema: public; Owner: aims
--

SELECT pg_catalog.setval('taskresults_feedback_uid_seq', 78, true);


-- Completed on 2013-07-15 03:27:47

--
-- PostgreSQL database dump complete
--

