﻿-- Package Table Inserts

INSERT INTO packages (uid, name, description, availabletasks) 
VALUES (1, 'None', 'Package with no access', 0);
INSERT INTO packages (uid, name, description, availabletasks) 
VALUES (2, 'All', 'Package with access to all tasks', 1);
INSERT INTO packages (uid, name, description, availabletasks) 
VALUES (4, 'VerticalLiftOnly', 'Package with access to Vertical Lift only', 3);
INSERT INTO packages (uid, name, description, availabletasks) 
VALUES (3, 'IntubationOnly', 'Package with access to Intubation only', 2);
INSERT INTO packages (uid, name, description, availabletasks) 
VALUES (5, 'LateralMovementOnly', 'Package with access to Lateral Movement only', 4);
INSERT INTO packages (uid, name, description, availabletasks) 
VALUES (6, 'IntubationAndVerticalLift', 'Package with access to both Intubation and Vertical Lift', 5);
INSERT INTO packages (uid, name, description, availabletasks) 
VALUES (7, 'IntubationAndLateralMovement', 'Package with access to both Intubation and Lateral Movement', 6);
INSERT INTO packages (uid, name, description, availabletasks) 
VALUES (8, 'VerticalLiftAndLateralMovement', 'Package with access to both Vertical Lift and Lateral Movement', 7);

SELECT pg_catalog.setval('packages_uid_seq', 1, false);

-- Organization Table Inserts

INSERT INTO organizations
(name, packageid)
VALUES('SimIS', 2);

INSERT INTO organizations
(name, packageid)
VALUES('EVMS', 3);

-- Accounts Table Inserts

INSERT INTO accounts 
(uid, username, password, accesslevel, creationdate, expirationdate, changepasswordnextlogin, email, lastlogin, lastloginip, organizationid, securityquestion, securityanswer) 
VALUES (1, 'test', 'testing123', 0, '2012-09-20 14:35:11.051', NULL, false, 'test@test.com', '2013-07-08 17:04:57.505', '127.0.0.1', 1, NULL, NULL);

INSERT INTO accounts 
(uid, username, password, accesslevel, creationdate, expirationdate, changepasswordnextlogin, email, lastlogin, lastloginip, organizationid, securityquestion, securityanswer) 
VALUES (2, 'house', 'testing123', 1, '2012-09-20 14:46:11.134', NULL, false, NULL, '2013-02-25 15:03:46.154', '127.0.0.1', 1, NULL, NULL);

SELECT pg_catalog.setval('accounts_uid_seq', 3, true);

-- Students Table Inserts

INSERT INTO students
(uid, accountid, organizationid, firstname, lastname, enrolledcourseids)
VALUES(1, 1, 2, 'John', 'Doe', '{1}');

SELECT pg_catalog.setval('students_uid_seq', 2, true);

-- Doctors Table Inserts

INSERT INTO doctors
(uid, organizationid, accountid, firstname, lastname)
VALUES(1, 1, 2, 'Gregory', 'House');

SELECT pg_catalog.setval('doctors_uid_seq', 2, true);