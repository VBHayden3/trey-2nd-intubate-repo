﻿-- Table: accounts

-- DROP TABLE accounts;

CREATE TABLE accounts
(
  uid serial NOT NULL, -- unique account id #
  username text, -- account username
  password text, -- account password
  accesslevel smallint, -- account accesslevel (this is in int format and represents the AccessLevel enum value casted to an int)
  creationdate timestamp without time zone DEFAULT now(), -- date and time which the account was created.
  expirationdate timestamp without time zone, -- date and time the account is declared Expired.
  changepasswordnextlogin boolean NOT NULL DEFAULT true, -- initialized to true, determines if the account user needs to change their password the next time they login.
  email text, -- account email address
  lastlogin timestamp without time zone, -- date and time the user last logged in
  lastloginip inet, -- ip address of the last successful login
  organizationid integer, -- unique organization id# which the account belongs to
  securityquestion text,
  securityanswer text,
  CONSTRAINT accounts_pkey PRIMARY KEY (uid),
  CONSTRAINT accounts_organization_fkey FOREIGN KEY (organizationid)
      REFERENCES organizations (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE accounts
  IS 'table of all accounts';
COMMENT ON COLUMN accounts.uid IS 'unique account id #';
COMMENT ON COLUMN accounts.username IS 'account username';
COMMENT ON COLUMN accounts.password IS 'account password';
COMMENT ON COLUMN accounts.accesslevel IS 'account accesslevel (this is in int format and represents the AccessLevel enum value casted to an int)';
COMMENT ON COLUMN accounts.creationdate IS 'date and time which the account was created.';
COMMENT ON COLUMN accounts.expirationdate IS 'date and time the account is declared Expired.';
COMMENT ON COLUMN accounts.changepasswordnextlogin IS 'initialized to true, determines if the account user needs to change their password the next time they login.';
COMMENT ON COLUMN accounts.email IS 'account email address';
COMMENT ON COLUMN accounts.lastlogin IS 'date and time the user last logged in';
COMMENT ON COLUMN accounts.lastloginip IS 'ip address of the last successful login';
COMMENT ON COLUMN accounts.organizationid IS 'unique organization id# which the account belongs to';

