﻿-- Table: administrator_settings

-- DROP TABLE administrator_settings;

CREATE TABLE administrator_settings
(
  id serial NOT NULL, -- Unique Primary Key ID for administrator_settings table.
  administratorid integer NOT NULL, -- FK to administrators table uid.
  setting1 text NOT NULL DEFAULT 'setting 1'::text, -- Placeholder for setting 1.
  setting2 text NOT NULL DEFAULT 'setting 2'::text, -- Placeholder for setting 2.
  setting3 text NOT NULL DEFAULT 'setting 3'::text, -- Placeholder for setting 3.
  CONSTRAINT administrator_settings_pkey1 PRIMARY KEY (id),
  CONSTRAINT administrator_settings_administratorid_fkey1 FOREIGN KEY (administratorid)
      REFERENCES administrators (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT administrator_settings_administratorid_key UNIQUE (administratorid)
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE administrator_settings
  IS 'Table for storing settings unique for a administrator account.';
COMMENT ON COLUMN administrator_settings.id IS 'Unique Primary Key ID for administrator_settings table.';
COMMENT ON COLUMN administrator_settings.administratorid IS 'FK to administrators table uid.';
COMMENT ON COLUMN administrator_settings.setting1 IS 'Placeholder for setting 1.';
COMMENT ON COLUMN administrator_settings.setting2 IS 'Placeholder for setting 2.';
COMMENT ON COLUMN administrator_settings.setting3 IS 'Placeholder for setting 3.';

