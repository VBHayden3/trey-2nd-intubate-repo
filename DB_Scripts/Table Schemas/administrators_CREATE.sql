﻿-- Table: administrators

-- DROP TABLE administrators;

CREATE TABLE administrators
(
  uid serial NOT NULL, -- unique administrator id#
  organizationid integer, -- organization id # which the administrator belongs to
  accountid integer, -- linked account id #
  firstname text, -- administrators first name
  lastname text, -- administrators last name
  CONSTRAINT administrators_pkey PRIMARY KEY (uid), -- unique administrator id# = Primary Key
  CONSTRAINT administrators_account_fkey FOREIGN KEY (accountid)
      REFERENCES accounts (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION, -- links the administrator to the account to which they belong
  CONSTRAINT administrators_organization_fkey FOREIGN KEY (organizationid)
      REFERENCES organizations (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION -- links the administrators to the organization to which they belong
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE administrators
  IS 'Table of all administrators';
COMMENT ON COLUMN administrators.uid IS 'unique administrator id#';
COMMENT ON COLUMN administrators.organizationid IS 'organization id # which the administrator belongs to';
COMMENT ON COLUMN administrators.accountid IS 'linked account id #';
COMMENT ON COLUMN administrators.firstname IS 'administrators first name';
COMMENT ON COLUMN administrators.lastname IS 'administrators last name';

COMMENT ON CONSTRAINT administrators_pkey ON administrators IS 'unique administrator id# = Primary Key';
COMMENT ON CONSTRAINT administrators_account_fkey ON administrators IS 'links the administrator to the account to which they belong';
COMMENT ON CONSTRAINT administrators_organization_fkey ON administrators IS 'links the administrators to the organization to which they belong';


-- Index: fki_administrators_account_fkey

-- DROP INDEX fki_administrators_account_fkey;

CREATE INDEX fki_administrators_account_fkey
  ON administrators
  USING btree
  (accountid);

-- Index: fki_administrators_organization_fkey

-- DROP INDEX fki_administrators_organization_fkey;

CREATE INDEX fki_administrators_organization_fkey
  ON administrators
  USING btree
  (organizationid);

