﻿-- Table: course_assignments

-- DROP TABLE course_assignments;

CREATE TABLE course_assignments
(
  uid serial NOT NULL, -- unique id #
  courseid integer, -- course id# in which the assignemnt belongs
  tasktype integer, -- enum value representing the task type implemented within the assignement
  taskmode integer, -- enum value representing the task mode (practice, test, etc) of the assignment
  startdate timestamp without time zone, -- date and time which the assignment becomes available for students
  duedate timestamp without time zone, -- date and time which the assignment ends being available for students
  name text, -- assignment name
  description text -- assignment description
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE course_assignments
  IS 'Table of all course assignements';
COMMENT ON COLUMN course_assignments.uid IS 'unique id #';
COMMENT ON COLUMN course_assignments.courseid IS 'course id# in which the assignemnt belongs';
COMMENT ON COLUMN course_assignments.tasktype IS 'enum value representing the task type implemented within the assignement';
COMMENT ON COLUMN course_assignments.taskmode IS 'enum value representing the task mode (practice, test, etc) of the assignment';
COMMENT ON COLUMN course_assignments.startdate IS 'date and time which the assignment becomes available for students';
COMMENT ON COLUMN course_assignments.duedate IS 'date and time which the assignment ends being available for students';
COMMENT ON COLUMN course_assignments.name IS 'assignment name';
COMMENT ON COLUMN course_assignments.description IS 'assignment description';

