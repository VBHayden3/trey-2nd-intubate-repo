﻿-- Table: courses

-- DROP TABLE courses;

CREATE TABLE courses
(
  uid serial NOT NULL, -- unique course id#
  doctorid integer, -- id# of the doctor who created the course
  startdate timestamp without time zone, -- date and time that the course begins being available
  enddate timestamp without time zone, -- date and time that the course ends being available
  name text, -- course name
  description text, -- course description
  hasintubation boolean NOT NULL DEFAULT false, -- Does the class include intubation
  hasverticallift boolean NOT NULL DEFAULT false, -- Does the course include vertical lift
  hascpr boolean NOT NULL DEFAULT false, -- Does the course include CPR
  CONSTRAINT courses_pkey PRIMARY KEY (uid),
  CONSTRAINT courses_doctor_fkey FOREIGN KEY (doctorid)
      REFERENCES doctors (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE courses
  IS 'Table of all courses created by doctors.';
COMMENT ON COLUMN courses.uid IS 'unique course id#';
COMMENT ON COLUMN courses.doctorid IS 'id# of the doctor who created the course';
COMMENT ON COLUMN courses.startdate IS 'date and time that the course begins being available';
COMMENT ON COLUMN courses.enddate IS 'date and time that the course ends being available';
COMMENT ON COLUMN courses.name IS 'course name';
COMMENT ON COLUMN courses.description IS 'course description';
COMMENT ON COLUMN courses.hasintubation IS 'Does the class include intubation';
COMMENT ON COLUMN courses.hasverticallift IS 'Does the course include vertical lift';
COMMENT ON COLUMN courses.hascpr IS 'Does the course include CPR';

