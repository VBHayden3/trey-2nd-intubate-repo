﻿-- Table: doctor_settings

-- DROP TABLE doctor_settings;

CREATE TABLE doctor_settings
(
  id serial NOT NULL, -- Unique Primary Key ID for doctor_settings table.
  doctorid integer NOT NULL, -- FK to doctos table uid.
  setting1 text NOT NULL DEFAULT 'setting 1'::text, -- Placeholder for setting 1.
  setting2 text NOT NULL DEFAULT 'setting 2'::text, -- Placeholder for setting 2.
  setting3 text NOT NULL DEFAULT 'setting 3'::text, -- Placeholder for setting 3.
  CONSTRAINT doctor_settings_pkey1 PRIMARY KEY (id),
  CONSTRAINT doctor_settings_doctorid_fkey1 FOREIGN KEY (doctorid)
      REFERENCES doctors (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT doctor_settings_doctorid_key UNIQUE (doctorid)
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE doctor_settings
  IS 'Table for storing settings unique for a doctor account.';
COMMENT ON COLUMN doctor_settings.id IS 'Unique Primary Key ID for doctor_settings table.';
COMMENT ON COLUMN doctor_settings.doctorid IS 'FK to doctos table uid.';
COMMENT ON COLUMN doctor_settings.setting1 IS 'Placeholder for setting 1.';
COMMENT ON COLUMN doctor_settings.setting2 IS 'Placeholder for setting 2.';
COMMENT ON COLUMN doctor_settings.setting3 IS 'Placeholder for setting 3.';

