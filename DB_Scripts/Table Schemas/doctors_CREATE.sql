﻿-- Table: doctors

-- DROP TABLE doctors;

CREATE TABLE doctors
(
  uid serial NOT NULL,
  organizationid integer,
  accountid integer,
  firstname text,
  lastname text,
  CONSTRAINT doctors_pkey PRIMARY KEY (uid),
  CONSTRAINT doctors_account_fkey FOREIGN KEY (accountid)
      REFERENCES accounts (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION, -- links the doctor to their uniquie account id#
  CONSTRAINT doctors_organization_fkey FOREIGN KEY (organizationid)
      REFERENCES organizations (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE doctors
  IS 'Table of all doctors';
COMMENT ON CONSTRAINT doctors_account_fkey ON doctors IS 'links the doctor to their uniquie account id#';


-- Index: fki_doctors_account_fkey

-- DROP INDEX fki_doctors_account_fkey;

CREATE INDEX fki_doctors_account_fkey
  ON doctors
  USING btree
  (accountid);

