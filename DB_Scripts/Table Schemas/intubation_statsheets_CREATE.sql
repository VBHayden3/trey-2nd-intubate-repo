﻿-- Table: intubation_statsheets

-- DROP TABLE intubation_statsheets;

CREATE TABLE intubation_statsheets
(
  uid serial NOT NULL, -- unique identifier
  studentid integer, -- linked student number
  timespracticed integer, -- total times practiced
  timestested integer, -- total times tested
  masterylevel integer, -- current mastery level....
  points integer, -- current points
  averagenovicetestscore double precision, -- average score of tests taken at novice mastery
  averagenovicepracticescore double precision, -- average score of practices taken at novice mastery
  averageintermediatetestscore double precision, -- average score of tests taken at intermediate mastery
  averageintermediatepracticescore double precision, -- average score of practices taken at intermediate mastery
  averagemastertestscore double precision, -- average score of tests taken at master mastery
  averagemasterpracticescore double precision, -- average score of practices taken at master mastery
  timestestednovice integer, -- number of times tested on novice
  timespracticednovice integer, -- number of times practiced on novice
  timestestedintermediate integer, -- number of times tested on intermediate
  timespracticedintermediate integer, -- number of times practiced on intermediate
  timestestedmaster integer, -- number of times tested on master
  timespracticedmaster integer, -- number of times practiced on master
  timelastpracticed timestamp without time zone, -- datetime last practiced
  timelasttested timestamp without time zone, -- datetime last tested
  lasttestresultid integer, -- resultID of the last novice test
  lastpracticeresultid integer, -- result id of the last practice
  lastnovicetestresultid integer, -- resultid of the last test run at novice mastery
  lastnovicepracticeresultid integer, -- resultid of the last practice run at novice
  lastintermediatetestresultid integer, -- resultid of the last test run at intermediate mastery
  lastintermediatepracticeresultid integer, -- resultid of the last practice run at intermediate mastery
  lastmastertestresultid integer, -- resultid of the last test run at master mastery
  lastmasterpracticeresultid integer, -- resultid of the last practice run at master mastery
  CONSTRAINT intubation_statsheet_pkey PRIMARY KEY (uid),
  CONSTRAINT intubation_statsheet_student_fkey FOREIGN KEY (studentid)
      REFERENCES students (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE intubation_statsheets
  IS 'table to store all intubation statsheets for tracking students';
COMMENT ON COLUMN intubation_statsheets.uid IS 'unique identifier';
COMMENT ON COLUMN intubation_statsheets.studentid IS 'linked student number';
COMMENT ON COLUMN intubation_statsheets.timespracticed IS 'total times practiced';
COMMENT ON COLUMN intubation_statsheets.timestested IS 'total times tested';
COMMENT ON COLUMN intubation_statsheets.masterylevel IS 'current mastery level.
0 = novice
1 = intermediate
2 = master';
COMMENT ON COLUMN intubation_statsheets.points IS 'current points';
COMMENT ON COLUMN intubation_statsheets.averagenovicetestscore IS 'average score of tests taken at novice mastery';
COMMENT ON COLUMN intubation_statsheets.averagenovicepracticescore IS 'average score of practices taken at novice mastery';
COMMENT ON COLUMN intubation_statsheets.averageintermediatetestscore IS 'average score of tests taken at intermediate mastery';
COMMENT ON COLUMN intubation_statsheets.averageintermediatepracticescore IS 'average score of practices taken at intermediate mastery';
COMMENT ON COLUMN intubation_statsheets.averagemastertestscore IS 'average score of tests taken at master mastery';
COMMENT ON COLUMN intubation_statsheets.averagemasterpracticescore IS 'average score of practices taken at master mastery';
COMMENT ON COLUMN intubation_statsheets.timestestednovice IS 'number of times tested on novice';
COMMENT ON COLUMN intubation_statsheets.timespracticednovice IS 'number of times practiced on novice';
COMMENT ON COLUMN intubation_statsheets.timestestedintermediate IS 'number of times tested on intermediate';
COMMENT ON COLUMN intubation_statsheets.timespracticedintermediate IS 'number of times practiced on intermediate';
COMMENT ON COLUMN intubation_statsheets.timestestedmaster IS 'number of times tested on master';
COMMENT ON COLUMN intubation_statsheets.timespracticedmaster IS 'number of times practiced on master';
COMMENT ON COLUMN intubation_statsheets.timelastpracticed IS 'datetime last practiced';
COMMENT ON COLUMN intubation_statsheets.timelasttested IS 'datetime last tested';
COMMENT ON COLUMN intubation_statsheets.lasttestresultid IS 'resultID of the last novice test';
COMMENT ON COLUMN intubation_statsheets.lastpracticeresultid IS 'result id of the last practice';
COMMENT ON COLUMN intubation_statsheets.lastnovicetestresultid IS 'resultid of the last test run at novice mastery';
COMMENT ON COLUMN intubation_statsheets.lastnovicepracticeresultid IS 'resultid of the last practice run at novice';
COMMENT ON COLUMN intubation_statsheets.lastintermediatetestresultid IS 'resultid of the last test run at intermediate mastery';
COMMENT ON COLUMN intubation_statsheets.lastintermediatepracticeresultid IS 'resultid of the last practice run at intermediate mastery';
COMMENT ON COLUMN intubation_statsheets.lastmastertestresultid IS 'resultid of the last test run at master mastery';
COMMENT ON COLUMN intubation_statsheets.lastmasterpracticeresultid IS 'resultid of the last practice run at master mastery';

