﻿-- Table: kinect_images

-- DROP TABLE kinect_images;

CREATE TABLE kinect_images
(
  uid serial NOT NULL, -- Unique Primary Key ID for Table.
  "timestamp" timestamp without time zone NOT NULL, -- The Date Time of when the kinect image was captured.
  hascolor boolean NOT NULL, -- Whether or not the color data exists for this kinect image.
  hasdepth boolean NOT NULL, -- Whether or not the depth data exists for this kinect image.
  colordata bytea NOT NULL, -- Compressed binary color data.
  depthdata bytea NOT NULL, -- Compressed binary depth data.
  colorformat smallint NOT NULL, -- Color Image Format.
  depthformat smallint NOT NULL, -- Depth Image Format.
  CONSTRAINT kinect_images_pkey PRIMARY KEY (uid)
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE kinect_images
  IS 'Table for storing stage screenshots for all lessons. Color and depth data are in a compressed binary format.';
COMMENT ON COLUMN kinect_images.uid IS 'Unique Primary Key ID for Table.';
COMMENT ON COLUMN kinect_images."timestamp" IS 'The Date Time of when the kinect image was captured.';
COMMENT ON COLUMN kinect_images.hascolor IS 'Whether or not the color data exists for this kinect image.';
COMMENT ON COLUMN kinect_images.hasdepth IS 'Whether or not the depth data exists for this kinect image.';
COMMENT ON COLUMN kinect_images.colordata IS 'Compressed binary color data.';
COMMENT ON COLUMN kinect_images.depthdata IS 'Compressed binary depth data.';
COMMENT ON COLUMN kinect_images.colorformat IS 'Color Image Format.';
COMMENT ON COLUMN kinect_images.depthformat IS 'Depth Image Format.';

