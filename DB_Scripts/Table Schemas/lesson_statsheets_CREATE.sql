﻿-- Table: lesson_statsheets

-- DROP TABLE lesson_statsheets;

CREATE TABLE lesson_statsheets
(
  uid serial NOT NULL, -- unique identifier
  studentid integer, -- linked student number
  timespracticed integer, -- total times practiced
  timestested integer, -- total times tested
  masterylevel integer, -- current master level
  points integer,
  averagenovicetestscore double precision, -- average score of tests taken at novice level mastery
  averagenovicepracticescore double precision, -- average score of practices taken at novice level mastery
  averageintermediatetestscore double precision, -- average score of tests taken at intermediate level mastery
  averageintermediatepracticescore double precision, -- average score of practices taken at intermediate level mastery
  averagemastertestscore double precision, -- average score of tests taken at master level mastery
  averagemasterpracticescore double precision, -- average score of practices taken at master level mastery
  timestestednovice integer, -- number of times tested on novice level mastery
  timespracticednovice integer, -- number of times practiced on novice level mastery
  timestestedintermediate integer, -- number of times tested on intermediate level mastery
  timespracticedintermediate integer, -- number of times practiced on intermediate level mastery
  timestestedmaster integer, -- number of times tested on master level mastery
  timespracticedmaster integer, -- number of times practiced on master level mastery
  timelastpracticed timestamp without time zone, -- datetime last practiced
  timelasttested timestamp without time zone, -- datetime last tested
  lasttestresultid integer, -- resultid of the last test
  lastpracticeresultid integer, -- resultid of the last practice
  lastnovicetestresultid integer, -- resultid of the result from the last test run on novice level mastery
  lastnovicepracticeresultid integer, -- resultid of the result from the last practice run on novice level mastery
  lastintermediatetestresultid integer, -- resultid of the result from the last test run on intermediate level mastery
  lastintermediatepracticeresultid integer, -- resultid of the result from the last practice run at intermediate level mastery
  lastmastertestresultid integer, -- resultid of the result from the last run test on master level mastery
  lastmasterpracticeresultid integer, -- resultid of the result from the last practice run at master level mastery
  lessontype integer, -- lesson type index within the LessonType enum
  novicepracticesinsuccession integer, -- number of novice practices successfully completed in a row
  intermediatetestsinsuccession integer, -- number of intermediate tests successfully completed in a row
  intermediatepracticesinsuccession integer, -- number of intermediate practices completed successfully in a row
  mastertestsinsuccession integer, -- number of master tests successfully completed in a row
  masterpracticesinsuccession integer, -- number of master practices successfully completed in a row
  novicetestsinsuccession integer, -- number of novice tests successfully completed in a row
  CONSTRAINT lesson_statsheets_pkey PRIMARY KEY (uid),
  CONSTRAINT studentid_lesson_statsheets_fkey FOREIGN KEY (studentid)
      REFERENCES students (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE lesson_statsheets
  IS 'table of all lesson statsheets';
COMMENT ON COLUMN lesson_statsheets.uid IS 'unique identifier';
COMMENT ON COLUMN lesson_statsheets.studentid IS 'linked student number';
COMMENT ON COLUMN lesson_statsheets.timespracticed IS 'total times practiced';
COMMENT ON COLUMN lesson_statsheets.timestested IS 'total times tested';
COMMENT ON COLUMN lesson_statsheets.masterylevel IS 'current master level';
COMMENT ON COLUMN lesson_statsheets.averagenovicetestscore IS 'average score of tests taken at novice level mastery';
COMMENT ON COLUMN lesson_statsheets.averagenovicepracticescore IS 'average score of practices taken at novice level mastery';
COMMENT ON COLUMN lesson_statsheets.averageintermediatetestscore IS 'average score of tests taken at intermediate level mastery';
COMMENT ON COLUMN lesson_statsheets.averageintermediatepracticescore IS 'average score of practices taken at intermediate level mastery';
COMMENT ON COLUMN lesson_statsheets.averagemastertestscore IS 'average score of tests taken at master level mastery';
COMMENT ON COLUMN lesson_statsheets.averagemasterpracticescore IS 'average score of practices taken at master level mastery';
COMMENT ON COLUMN lesson_statsheets.timestestednovice IS 'number of times tested on novice level mastery';
COMMENT ON COLUMN lesson_statsheets.timespracticednovice IS 'number of times practiced on novice level mastery';
COMMENT ON COLUMN lesson_statsheets.timestestedintermediate IS 'number of times tested on intermediate level mastery';
COMMENT ON COLUMN lesson_statsheets.timespracticedintermediate IS 'number of times practiced on intermediate level mastery';
COMMENT ON COLUMN lesson_statsheets.timestestedmaster IS 'number of times tested on master level mastery';
COMMENT ON COLUMN lesson_statsheets.timespracticedmaster IS 'number of times practiced on master level mastery';
COMMENT ON COLUMN lesson_statsheets.timelastpracticed IS 'datetime last practiced';
COMMENT ON COLUMN lesson_statsheets.timelasttested IS 'datetime last tested';
COMMENT ON COLUMN lesson_statsheets.lasttestresultid IS 'resultid of the last test';
COMMENT ON COLUMN lesson_statsheets.lastpracticeresultid IS 'resultid of the last practice';
COMMENT ON COLUMN lesson_statsheets.lastnovicetestresultid IS 'resultid of the result from the last test run on novice level mastery';
COMMENT ON COLUMN lesson_statsheets.lastnovicepracticeresultid IS 'resultid of the result from the last practice run on novice level mastery';
COMMENT ON COLUMN lesson_statsheets.lastintermediatetestresultid IS 'resultid of the result from the last test run on intermediate level mastery';
COMMENT ON COLUMN lesson_statsheets.lastintermediatepracticeresultid IS 'resultid of the result from the last practice run at intermediate level mastery';
COMMENT ON COLUMN lesson_statsheets.lastmastertestresultid IS 'resultid of the result from the last run test on master level mastery';
COMMENT ON COLUMN lesson_statsheets.lastmasterpracticeresultid IS 'resultid of the result from the last practice run at master level mastery';
COMMENT ON COLUMN lesson_statsheets.lessontype IS 'lesson type index within the LessonType enum';
COMMENT ON COLUMN lesson_statsheets.novicepracticesinsuccession IS 'number of novice practices successfully completed in a row';
COMMENT ON COLUMN lesson_statsheets.intermediatetestsinsuccession IS 'number of intermediate tests successfully completed in a row';
COMMENT ON COLUMN lesson_statsheets.intermediatepracticesinsuccession IS 'number of intermediate practices completed successfully in a row';
COMMENT ON COLUMN lesson_statsheets.mastertestsinsuccession IS 'number of master tests successfully completed in a row';
COMMENT ON COLUMN lesson_statsheets.masterpracticesinsuccession IS 'number of master practices successfully completed in a row';
COMMENT ON COLUMN lesson_statsheets.novicetestsinsuccession IS 'number of novice tests successfully completed in a row';

