﻿-- Table: messages

-- DROP TABLE messages;

CREATE TABLE messages
(
  uid serial NOT NULL, -- unique message id#
  courseid integer, -- id# of linked course
  toid integer, -- id# of linked student the message is being sent to
  fromid integer, -- id# of the linked doctor the message is being sent from
  subject text, -- subject title of the message
  body text, -- body text of the message
  datesent timestamp without time zone, -- date and time the message was sent
  hasbeenread boolean DEFAULT false, -- whether the message has been read
  CONSTRAINT messages_pkey PRIMARY KEY (uid),
  CONSTRAINT messages_course_fkey FOREIGN KEY (courseid)
      REFERENCES courses (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION, -- links the message to the course which it was sent in
  CONSTRAINT messages_from_doctor_fkey FOREIGN KEY (fromid)
      REFERENCES doctors (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT messages_to_student_fkey FOREIGN KEY (toid)
      REFERENCES students (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION -- links the message to the student whom the message is addressed to
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE messages
  IS 'Table to store all doctor to student messages';
COMMENT ON COLUMN messages.uid IS 'unique message id#';
COMMENT ON COLUMN messages.courseid IS 'id# of linked course';
COMMENT ON COLUMN messages.toid IS 'id# of linked student the message is being sent to';
COMMENT ON COLUMN messages.fromid IS 'id# of the linked doctor the message is being sent from';
COMMENT ON COLUMN messages.subject IS 'subject title of the message';
COMMENT ON COLUMN messages.body IS 'body text of the message';
COMMENT ON COLUMN messages.datesent IS 'date and time the message was sent';
COMMENT ON COLUMN messages.hasbeenread IS 'whether the message has been read';

COMMENT ON CONSTRAINT messages_course_fkey ON messages IS 'links the message to the course which it was sent in';
COMMENT ON CONSTRAINT messages_to_student_fkey ON messages IS 'links the message to the student whom the message is addressed to';


-- Index: fki_messages_course_fkey

-- DROP INDEX fki_messages_course_fkey;

CREATE INDEX fki_messages_course_fkey
  ON messages
  USING btree
  (courseid);

-- Index: fki_messages_to_student_fkey

-- DROP INDEX fki_messages_to_student_fkey;

CREATE INDEX fki_messages_to_student_fkey
  ON messages
  USING btree
  (toid);

