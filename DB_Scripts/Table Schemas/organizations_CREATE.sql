﻿-- Table: organizations

-- DROP TABLE organizations;

CREATE TABLE organizations
(
  uid serial NOT NULL, -- organization ID#
  name text, -- organization name
  packageid integer,
  CONSTRAINT organizations_pkey PRIMARY KEY (uid),
  CONSTRAINT organizations_package_fkey FOREIGN KEY (packageid)
      REFERENCES packages (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION -- links organizations to the package which they're subscribed to
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE organizations
  IS 'Table of all organizations.';
COMMENT ON COLUMN organizations.uid IS 'organization ID#';
COMMENT ON COLUMN organizations.name IS 'organization name';

COMMENT ON CONSTRAINT organizations_package_fkey ON organizations IS 'links organizations to the package which they''re subscribed to';


-- Index: fki_organizations_package_fkey

-- DROP INDEX fki_organizations_package_fkey;

CREATE INDEX fki_organizations_package_fkey
  ON organizations
  USING btree
  (packageid);

