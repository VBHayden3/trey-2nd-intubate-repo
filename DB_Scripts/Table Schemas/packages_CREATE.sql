﻿-- Table: packages

-- DROP TABLE packages;

CREATE TABLE packages
(
  uid serial NOT NULL, -- unique package id#
  name text, -- package name
  description text, -- package description
  availabletasks integer, -- enum value representing the tasks available in this package
  CONSTRAINT packages_pkey PRIMARY KEY (uid)
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE packages
  IS 'Table of all packages';
COMMENT ON COLUMN packages.uid IS 'unique package id#';
COMMENT ON COLUMN packages.name IS 'package name';
COMMENT ON COLUMN packages.description IS 'package description';
COMMENT ON COLUMN packages.availabletasks IS 'enum value representing the tasks available in this package';

