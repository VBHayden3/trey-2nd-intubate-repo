﻿-- Table: rosters

-- DROP TABLE rosters;

CREATE TABLE rosters
(
  uid serial NOT NULL, -- unique id number
  courseid integer,
  studentlist integer[], -- List of studentIDs
  CONSTRAINT roster_pkey PRIMARY KEY (uid) -- Roster uniquie ID is the primary key.
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE rosters
  IS 'table of course rosters';
COMMENT ON COLUMN rosters.uid IS 'unique id number';
COMMENT ON COLUMN rosters.studentlist IS 'List of studentIDs ';

COMMENT ON CONSTRAINT roster_pkey ON rosters IS 'Roster uniquie ID is the primary key.';

