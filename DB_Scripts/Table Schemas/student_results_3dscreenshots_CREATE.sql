﻿-- Table: student_results_3dscreenshots

-- DROP TABLE student_results_3dscreenshots;

CREATE TABLE student_results_3dscreenshots
(
  uid serial NOT NULL, -- Unique serial id#
  stagescoreid integer, -- linked stage score id #
  coloranddepthdata bytea, -- Color and Depth Data (serialized) and stored in a byte array....
  CONSTRAINT "3dscreenshots_pkey" PRIMARY KEY (uid),
  CONSTRAINT stagescore_fkey FOREIGN KEY (stagescoreid)
      REFERENCES student_results_stage_scores (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE student_results_3dscreenshots
  IS 'Table to store all 3D screenshot color and depth data (serialized byte arrays) for the 3D viewer object.';
COMMENT ON COLUMN student_results_3dscreenshots.uid IS 'Unique serial id#';
COMMENT ON COLUMN student_results_3dscreenshots.stagescoreid IS 'linked stage score id #';
COMMENT ON COLUMN student_results_3dscreenshots.coloranddepthdata IS 'Color and Depth Data (serialized) and stored in a byte array.
Width and height is always 600x480.';

