﻿-- Table: student_results

-- DROP TABLE student_results;

CREATE TABLE student_results
(
  uid serial NOT NULL, -- id# of result
  studentid integer, -- linked student
  tasktype integer, -- int value of task in the Task enum
  passed boolean, -- passed or not (failed)
  totalscore double precision, -- total score of the result
  date timestamp without time zone, -- datetime result sumbitted
  assignmentid integer, -- Linked assignment uid number
  taskmode integer, -- int value of the taskmode in the TaskMode enum
  masterylevel integer, -- int value of the masterylevel in the MasteryLevel enum
  totaltime interval, -- total time of the result
  CONSTRAINT "UID_pkey" PRIMARY KEY (uid),
  CONSTRAINT "StudentID_fkey" FOREIGN KEY (studentid)
      REFERENCES students (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION -- links result to specific student's unique id #
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE student_results
  IS 'Table to store all student results.';
COMMENT ON COLUMN student_results.uid IS 'id# of result';
COMMENT ON COLUMN student_results.studentid IS 'linked student';
COMMENT ON COLUMN student_results.tasktype IS 'int value of task in the Task enum';
COMMENT ON COLUMN student_results.passed IS 'passed or not (failed)';
COMMENT ON COLUMN student_results.totalscore IS 'total score of the result';
COMMENT ON COLUMN student_results.date IS 'datetime result sumbitted';
COMMENT ON COLUMN student_results.assignmentid IS 'Linked assignment uid number';
COMMENT ON COLUMN student_results.taskmode IS 'int value of the taskmode in the TaskMode enum';
COMMENT ON COLUMN student_results.masterylevel IS 'int value of the masterylevel in the MasteryLevel enum';
COMMENT ON COLUMN student_results.totaltime IS 'total time of the result';

COMMENT ON CONSTRAINT "StudentID_fkey" ON student_results IS 'links result to specific student''s unique id #';


-- Index: "fki_StudentID_fkey"

-- DROP INDEX "fki_StudentID_fkey";

CREATE INDEX "fki_StudentID_fkey"
  ON student_results
  USING btree
  (studentid);

