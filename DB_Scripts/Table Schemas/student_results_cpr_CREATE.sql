﻿-- Table: student_results_cpr

-- DROP TABLE student_results_cpr;

CREATE TABLE student_results_cpr
(
  uid serial NOT NULL, -- Unique Primary Key ID for student_results_cpr table.
  studentid integer NOT NULL, -- FK to student table uid.
  masterylevel smallint NOT NULL, -- MasteryLevel enum (Novice, Intermediate, Master) represented as an integer value.
  taskmode smallint NOT NULL, -- TaskMode enum (Practice, Test) represented as an integer value.
  score double precision NOT NULL, -- Total computed single number score for CPR stage.
  lessonstarttime timestamp without time zone NOT NULL, -- Lesson Start Date and Time.
  lessonendtime timestamp without time zone NOT NULL, -- Lesson End Date and Time.
  calibrationimageid integer, -- FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.
  askedforaed boolean, -- CPR asked for AED value.
  askedfor911 boolean, -- CPR asked for 911 value.
  askedifok boolean, -- CPR asked if OK value.
  checkpulseimageid integer, -- FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.
  checkedpulse checkpulse, -- CPR checked pulse value. ENUM (no, incorrectly, correctly)
  CONSTRAINT student_results_cpr_pkey PRIMARY KEY (uid),
  CONSTRAINT kinect_images_fk FOREIGN KEY (calibrationimageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT kinect_images_fk2 FOREIGN KEY (checkpulseimageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT student_results_cpr_studentid_fkey FOREIGN KEY (studentid)
      REFERENCES students (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT student_results_cpr_unique UNIQUE (calibrationimageid, checkpulseimageid)
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE student_results_cpr
  IS 'Top-Level CPR Results Table. Stores detailed and unique stage results in a column-oriented design.';
COMMENT ON COLUMN student_results_cpr.uid IS 'Unique Primary Key ID for student_results_cpr table.';
COMMENT ON COLUMN student_results_cpr.studentid IS 'FK to student table uid.';
COMMENT ON COLUMN student_results_cpr.masterylevel IS 'MasteryLevel enum (Novice, Intermediate, Master) represented as an integer value.';
COMMENT ON COLUMN student_results_cpr.taskmode IS 'TaskMode enum (Practice, Test) represented as an integer value.';
COMMENT ON COLUMN student_results_cpr.score IS 'Total computed single number score for CPR stage.';
COMMENT ON COLUMN student_results_cpr.lessonstarttime IS 'Lesson Start Date and Time.';
COMMENT ON COLUMN student_results_cpr.lessonendtime IS 'Lesson End Date and Time.';
COMMENT ON COLUMN student_results_cpr.calibrationimageid IS 'FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';
COMMENT ON COLUMN student_results_cpr.askedforaed IS 'CPR asked for AED value.';
COMMENT ON COLUMN student_results_cpr.askedfor911 IS 'CPR asked for 911 value.';
COMMENT ON COLUMN student_results_cpr.askedifok IS 'CPR asked if OK value.';
COMMENT ON COLUMN student_results_cpr.checkpulseimageid IS 'FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';
COMMENT ON COLUMN student_results_cpr.checkedpulse IS 'CPR checked pulse value. ENUM (no, incorrectly, correctly) ';

