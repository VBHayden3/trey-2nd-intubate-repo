﻿-- Table: student_results_cpr_compressions

-- DROP TABLE student_results_cpr_compressions;

CREATE TABLE student_results_cpr_compressions
(
  compressionset integer NOT NULL, -- Part of the student_results_cpr_compressions PK. Also identifies the ordering of compression sets for a CPR Lesson run.
  student_results_cpr_uid integer NOT NULL, -- FK to student_results_cpr table. Also part of student_results_cpr_compressions PK.
  avgdepth double precision, -- Average depth of compressions for a CPR set of compressions.
  avgrate double precision,
  avgarmslocked double precision, -- Percent average of arms being locked across compressions for a CPR set of compressions.
  avgshouldersovermanikin double precision, -- Percent average of shoulders being over the manikin across compressions for a CPR setof compressions.
  score double precision, -- Score for a set of CPR compressions.
  compressionimageid integer, -- FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.
  CONSTRAINT student_results_cpr_compressions_pk PRIMARY KEY (compressionset, student_results_cpr_uid),
  CONSTRAINT kinect_images_fk FOREIGN KEY (compressionimageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT student_results_cpr_fk FOREIGN KEY (student_results_cpr_uid)
      REFERENCES student_results_cpr (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT student_results_cpr_compressions_unique UNIQUE (compressionimageid)
)
WITH (
  OIDS=FALSE
);
COMMENT ON COLUMN student_results_cpr_compressions.compressionset IS 'Part of the student_results_cpr_compressions PK. Also identifies the ordering of compression sets for a CPR Lesson run.';
COMMENT ON COLUMN student_results_cpr_compressions.student_results_cpr_uid IS 'FK to student_results_cpr table. Also part of student_results_cpr_compressions PK.';
COMMENT ON COLUMN student_results_cpr_compressions.avgdepth IS 'Average depth of compressions for a CPR set of compressions.';
COMMENT ON COLUMN student_results_cpr_compressions.avgarmslocked IS 'Percent average of arms being locked across compressions for a CPR set of compressions.';
COMMENT ON COLUMN student_results_cpr_compressions.avgshouldersovermanikin IS 'Percent average of shoulders being over the manikin across compressions for a CPR setof compressions.';
COMMENT ON COLUMN student_results_cpr_compressions.score IS 'Score for a set of CPR compressions.';
COMMENT ON COLUMN student_results_cpr_compressions.compressionimageid IS 'FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';

