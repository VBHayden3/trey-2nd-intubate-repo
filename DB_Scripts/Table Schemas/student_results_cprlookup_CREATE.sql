﻿-- Table: student_results_cprlookup

-- DROP TABLE student_results_cprlookup;

CREATE TABLE student_results_cprlookup
(
  uid serial NOT NULL, -- Unique Primary Key ID for student_results_cprlookup table.
  cprid integer NOT NULL, -- FK to student_results_cpr table uid.
  key text NOT NULL, -- Key which labels the parameter being saved.
  value text NOT NULL, -- Value which stores the actual parameter.
  CONSTRAINT student_results_cprlookup_pkey PRIMARY KEY (uid),
  CONSTRAINT student_results_cprlookup_cprid_fkey FOREIGN KEY (cprid)
      REFERENCES student_results_cpr (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE student_results_cprlookup
  IS 'Key-Value Table for storing lookup data per module in a row-oriented configuration.  Table is related to the student_results_cpr table.';
COMMENT ON COLUMN student_results_cprlookup.uid IS 'Unique Primary Key ID for student_results_cprlookup table.';
COMMENT ON COLUMN student_results_cprlookup.cprid IS 'FK to student_results_cpr table uid.';
COMMENT ON COLUMN student_results_cprlookup.key IS 'Key which labels the parameter being saved.';
COMMENT ON COLUMN student_results_cprlookup.value IS 'Value which stores the actual parameter.';

