﻿-- Table: student_results_intubation

-- DROP TABLE student_results_intubation;

CREATE TABLE student_results_intubation
(
  uid serial NOT NULL, -- Unique Primary Key ID for student_results_intubation table.
  studentid integer NOT NULL, -- FK to student table uid.
  masterylevel smallint NOT NULL, -- MasteryLevel enum (Novice, Intermediate, Master) represented as an integer value.
  taskmode smallint NOT NULL, -- TaskMode enum (Practice, Test) represented as an integer value.
  score double precision NOT NULL, -- Total computed single number score for intubation stage.
  lessonstarttime timestamp without time zone NOT NULL, -- Lesson Start Date and Time.
  lessonendtime timestamp without time zone NOT NULL, -- Lesson End Date and Time.
  CONSTRAINT student_results_intubation_pkey PRIMARY KEY (uid),
  CONSTRAINT student_results_intubation_studentid_fkey FOREIGN KEY (studentid)
      REFERENCES students (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE student_results_intubation
  IS 'Top-Level intubation Results Table. Stores detailed and unique stage results in a column-oriented design.';
COMMENT ON COLUMN student_results_intubation.uid IS 'Unique Primary Key ID for student_results_intubation table.';
COMMENT ON COLUMN student_results_intubation.studentid IS 'FK to student table uid.';
COMMENT ON COLUMN student_results_intubation.masterylevel IS 'MasteryLevel enum (Novice, Intermediate, Master) represented as an integer value.';
COMMENT ON COLUMN student_results_intubation.taskmode IS 'TaskMode enum (Practice, Test) represented as an integer value.';
COMMENT ON COLUMN student_results_intubation.score IS 'Total computed single number score for intubation stage.';
COMMENT ON COLUMN student_results_intubation.lessonstarttime IS 'Lesson Start Date and Time.';
COMMENT ON COLUMN student_results_intubation.lessonendtime IS 'Lesson End Date and Time.';

