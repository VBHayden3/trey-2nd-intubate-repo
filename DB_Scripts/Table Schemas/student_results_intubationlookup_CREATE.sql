﻿-- Table: student_results_intubationlookup

-- DROP TABLE student_results_intubationlookup;

CREATE TABLE student_results_intubationlookup
(
  uid serial NOT NULL, -- Unique Primary Key ID for student_results_intubationlookup table.
  intubationid integer NOT NULL, -- FK to student_results_intubation table uid.
  key text NOT NULL, -- Key which labels the parameter being saved.
  value text NOT NULL, -- Value which stores the actual parameter.
  CONSTRAINT student_results_intubationlookup_pkey PRIMARY KEY (uid),
  CONSTRAINT student_results_intubationlookup_intubationid_fkey FOREIGN KEY (intubationid)
      REFERENCES student_results_intubation (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE student_results_intubationlookup
  IS 'Key-Value Table for storing lookup data per module in a row-oriented configuration.  Table is related to the student_results_intubation table.';
COMMENT ON COLUMN student_results_intubationlookup.uid IS 'Unique Primary Key ID for student_results_intubationlookup table.';
COMMENT ON COLUMN student_results_intubationlookup.intubationid IS 'FK to student_results_intubation table uid.';
COMMENT ON COLUMN student_results_intubationlookup.key IS 'Key which labels the parameter being saved.';
COMMENT ON COLUMN student_results_intubationlookup.value IS 'Value which stores the actual parameter.';

