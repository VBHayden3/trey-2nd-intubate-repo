﻿-- Table: student_results_lateraltransfer

-- DROP TABLE student_results_lateraltransfer;

CREATE TABLE student_results_lateraltransfer
(
  uid serial NOT NULL, -- Unique Primary Key ID for student_results_lateraltransfer table.
  studentid integer NOT NULL, -- FK to student table uid.
  masterylevel smallint NOT NULL, -- MasteryLevel enum (Novice, Intermediate, Master) represented as an integer value.
  taskmode smallint NOT NULL, -- TaskMode enum (Practice, Test) represented as an integer value.
  score double precision NOT NULL, -- Total computed single number score for lateral transfer stage.
  lessonstarttime timestamp without time zone NOT NULL, -- Lesson Start Date and Time.
  lessonendtime timestamp without time zone NOT NULL, -- Lesson End Date and Time.
  handplacementscore double precision NOT NULL, -- Total computed single number score for lateral transfer hand placement stage.
  verbalcount boolean NOT NULL, -- Boolean indication for lateral transfer vocal pull stage.
  verbalcounttime timestamp without time zone NOT NULL, -- Lateral Transfer vocal pull stage Date and Time.
  pullstarttime timestamp without time zone NOT NULL, -- Lateral Transfer pull detected start Date and Time.
  pullendtime timestamp without time zone NOT NULL, -- Lateral Transfer pull ended Date and Time.
  totalsync double precision NOT NULL, -- Lateral Transfer Total Sync value.
  rhandsync double precision NOT NULL, -- Lateral Transfer Right Hand Sync value.
  lhandsync double precision NOT NULL, -- Lateral Transfer Left Hand Sync value.
  rwristsync double precision NOT NULL, -- Lateral Transfer Right Wrist Sync value.
  lwristsync double precision NOT NULL, -- Lateral Transfer Left Wrist Sync value.
  relbowsync double precision NOT NULL, -- Lateral Transfer Right Elbow Sync value.
  lelbowsync double precision NOT NULL, -- Lateral Transfer Left Elbow Sync value.
  rshouldersync double precision NOT NULL, -- Lateral Transfer Right Shoulder Sync value.
  lshouldersync double precision NOT NULL, -- Lateral Transfer Left Shoulder Sync value.
  manikinsync double precision NOT NULL, -- Lateral Transfer Manikin Torso-to-Leg Sync value.
  calibrationimageid integer, -- FK to kinect_images table uid.
  handplacementimageid integer, -- FK to kinect_images table uid.
  pullendimageid integer, -- FK to kinect_images table uid.
  pullstartimageid integer, -- FK to kinect_images table uid.
  overallfeedbackids integer[], -- List of Feedback IDs. Relates to the feedback strings relevant to the results. Feedback strings are stored in the lateral transfer lookup table.
  pullfeedbackids integer[], -- List of Feedback IDs. Relates to the feedback strings relevant to the results. Feedback strings are stored in the lateral transfer lookup table.
  handplacementfeedbackids integer[], -- List of Feedback IDs. Relates to the feedback strings relevant to the results. Feedback strings are stored in the lateral transfer lookup table.
  CONSTRAINT student_results_lateraltransfer_pkey PRIMARY KEY (uid),
  CONSTRAINT student_results_lateraltransfer_calibrationimageid_fkey FOREIGN KEY (calibrationimageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT student_results_lateraltransfer_handplacementimageid_fkey FOREIGN KEY (handplacementimageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT student_results_lateraltransfer_pullendimageid_fkey FOREIGN KEY (pullendimageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT student_results_lateraltransfer_pullstartimageid_fkey FOREIGN KEY (pullstartimageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT student_results_lateraltransfer_studentid_fkey FOREIGN KEY (studentid)
      REFERENCES students (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT student_results_lateraltransfer_calibrationimageid_key UNIQUE (calibrationimageid),
  CONSTRAINT student_results_lateraltransfer_handplacementimageid_key UNIQUE (handplacementimageid),
  CONSTRAINT student_results_lateraltransfer_pullendimageid_key UNIQUE (pullendimageid),
  CONSTRAINT student_results_lateraltransfer_pullstartimageid_key UNIQUE (pullstartimageid)
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE student_results_lateraltransfer
  IS 'Top-Level Lateral Transfer Results Table. Stores detailed and unique stage results in a column-oriented design.';
COMMENT ON COLUMN student_results_lateraltransfer.uid IS 'Unique Primary Key ID for student_results_lateraltransfer table.';
COMMENT ON COLUMN student_results_lateraltransfer.studentid IS 'FK to student table uid.';
COMMENT ON COLUMN student_results_lateraltransfer.masterylevel IS 'MasteryLevel enum (Novice, Intermediate, Master) represented as an integer value.';
COMMENT ON COLUMN student_results_lateraltransfer.taskmode IS 'TaskMode enum (Practice, Test) represented as an integer value.';
COMMENT ON COLUMN student_results_lateraltransfer.score IS 'Total computed single number score for lateral transfer stage.';
COMMENT ON COLUMN student_results_lateraltransfer.lessonstarttime IS 'Lesson Start Date and Time.';
COMMENT ON COLUMN student_results_lateraltransfer.lessonendtime IS 'Lesson End Date and Time.';
COMMENT ON COLUMN student_results_lateraltransfer.handplacementscore IS 'Total computed single number score for lateral transfer hand placement stage.';
COMMENT ON COLUMN student_results_lateraltransfer.verbalcount IS 'Boolean indication for lateral transfer vocal pull stage.';
COMMENT ON COLUMN student_results_lateraltransfer.verbalcounttime IS 'Lateral Transfer vocal pull stage Date and Time.';
COMMENT ON COLUMN student_results_lateraltransfer.pullstarttime IS 'Lateral Transfer pull detected start Date and Time.';
COMMENT ON COLUMN student_results_lateraltransfer.pullendtime IS 'Lateral Transfer pull ended Date and Time.';
COMMENT ON COLUMN student_results_lateraltransfer.totalsync IS 'Lateral Transfer Total Sync value.';
COMMENT ON COLUMN student_results_lateraltransfer.rhandsync IS 'Lateral Transfer Right Hand Sync value.';
COMMENT ON COLUMN student_results_lateraltransfer.lhandsync IS 'Lateral Transfer Left Hand Sync value.';
COMMENT ON COLUMN student_results_lateraltransfer.rwristsync IS 'Lateral Transfer Right Wrist Sync value.';
COMMENT ON COLUMN student_results_lateraltransfer.lwristsync IS 'Lateral Transfer Left Wrist Sync value.';
COMMENT ON COLUMN student_results_lateraltransfer.relbowsync IS 'Lateral Transfer Right Elbow Sync value.';
COMMENT ON COLUMN student_results_lateraltransfer.lelbowsync IS 'Lateral Transfer Left Elbow Sync value.';
COMMENT ON COLUMN student_results_lateraltransfer.rshouldersync IS 'Lateral Transfer Right Shoulder Sync value.';
COMMENT ON COLUMN student_results_lateraltransfer.lshouldersync IS 'Lateral Transfer Left Shoulder Sync value.';
COMMENT ON COLUMN student_results_lateraltransfer.manikinsync IS 'Lateral Transfer Manikin Torso-to-Leg Sync value.';
COMMENT ON COLUMN student_results_lateraltransfer.calibrationimageid IS 'FK to kinect_images table uid.';
COMMENT ON COLUMN student_results_lateraltransfer.handplacementimageid IS 'FK to kinect_images table uid.';
COMMENT ON COLUMN student_results_lateraltransfer.pullendimageid IS 'FK to kinect_images table uid.';
COMMENT ON COLUMN student_results_lateraltransfer.pullstartimageid IS 'FK to kinect_images table uid.';
COMMENT ON COLUMN student_results_lateraltransfer.overallfeedbackids IS 'List of Feedback IDs. Relates to the feedback strings relevant to the results. Feedback strings are stored in the lateral transfer lookup table.';
COMMENT ON COLUMN student_results_lateraltransfer.pullfeedbackids IS 'List of Feedback IDs. Relates to the feedback strings relevant to the results. Feedback strings are stored in the lateral transfer lookup table.';
COMMENT ON COLUMN student_results_lateraltransfer.handplacementfeedbackids IS 'List of Feedback IDs. Relates to the feedback strings relevant to the results. Feedback strings are stored in the lateral transfer lookup table.';

