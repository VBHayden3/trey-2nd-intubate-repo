﻿-- Table: student_results_lateraltransferlookup

-- DROP TABLE student_results_lateraltransferlookup;

CREATE TABLE student_results_lateraltransferlookup
(
  uid serial NOT NULL, -- Unique Primary Key ID for student_results_lateraltransferlookup table.
  lateraltransferid integer NOT NULL, -- FK to student_results_lateraltransfer table uid.
  key text NOT NULL, -- Key which labels the parameter being saved.
  value text NOT NULL, -- Value which stores the actual parameter.
  CONSTRAINT student_results_lateraltransferlookup_pkey PRIMARY KEY (uid),
  CONSTRAINT student_results_lateraltransferlookup_lateraltransferid_fkey FOREIGN KEY (lateraltransferid)
      REFERENCES student_results_lateraltransfer (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE student_results_lateraltransferlookup
  IS 'Key-Value Table for storing lookup data per module in a row-oriented configuration.  Table is related to the student_results_lateraltransfer table.';
COMMENT ON COLUMN student_results_lateraltransferlookup.uid IS 'Unique Primary Key ID for student_results_lateraltransferlookup table.';
COMMENT ON COLUMN student_results_lateraltransferlookup.lateraltransferid IS 'FK to student_results_lateraltransfer table uid.';
COMMENT ON COLUMN student_results_lateraltransferlookup.key IS 'Key which labels the parameter being saved.';
COMMENT ON COLUMN student_results_lateraltransferlookup.value IS 'Value which stores the actual parameter.';

