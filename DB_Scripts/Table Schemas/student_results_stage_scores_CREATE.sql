﻿-- Table: student_results_stage_scores

-- DROP TABLE student_results_stage_scores;

CREATE TABLE student_results_stage_scores
(
  uid integer NOT NULL DEFAULT nextval('"student_results_stage_scores_UID_seq"'::regclass), -- unique ID #
  resultid integer,
  stagescore bytea, -- Serialized StageScore object as byte array.
  CONSTRAINT uid_pkey PRIMARY KEY (uid),
  CONSTRAINT resultid_fkey FOREIGN KEY (resultid)
      REFERENCES student_results (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE student_results_stage_scores
  IS 'Stage score entries';
COMMENT ON COLUMN student_results_stage_scores.uid IS 'unique ID #';
COMMENT ON COLUMN student_results_stage_scores.stagescore IS 'Serialized StageScore object as byte array.';

