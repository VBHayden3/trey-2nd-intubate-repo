﻿-- Table: student_results_verticallift

-- DROP TABLE student_results_verticallift;

CREATE TABLE student_results_verticallift
(
  uid serial NOT NULL, -- Unique Primary Key ID for student_results_verticallift table.
  studentid integer NOT NULL, -- FK to student table uid.
  masterylevel smallint NOT NULL, -- MasteryLevel enum (Novice, Intermediate, Master) represented as an integer value.
  taskmode smallint NOT NULL, -- TaskMode enum (Practice, Test) represented as an integer value.
  score double precision NOT NULL, -- Total computed single number score for Vertical Lift stage.
  lessonstarttime timestamp without time zone NOT NULL, -- Lesson Start Date and Time.
  lessonendtime timestamp without time zone NOT NULL, -- Lesson End Date and Time.
  CONSTRAINT student_results_verticallift_pkey PRIMARY KEY (uid),
  CONSTRAINT student_results_verticallift_studentid_fkey FOREIGN KEY (studentid)
      REFERENCES students (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE student_results_verticallift
  IS 'Top-Level Vertical Lift Results Table. Stores detailed and unique stage results in a column-oriented design.';
COMMENT ON COLUMN student_results_verticallift.uid IS 'Unique Primary Key ID for student_results_verticallift table.';
COMMENT ON COLUMN student_results_verticallift.studentid IS 'FK to student table uid.';
COMMENT ON COLUMN student_results_verticallift.masterylevel IS 'MasteryLevel enum (Novice, Intermediate, Master) represented as an integer value.';
COMMENT ON COLUMN student_results_verticallift.taskmode IS 'TaskMode enum (Practice, Test) represented as an integer value.';
COMMENT ON COLUMN student_results_verticallift.score IS 'Total computed single number score for Vertical Lift stage.';
COMMENT ON COLUMN student_results_verticallift.lessonstarttime IS 'Lesson Start Date and Time.';
COMMENT ON COLUMN student_results_verticallift.lessonendtime IS 'Lesson End Date and Time.';

