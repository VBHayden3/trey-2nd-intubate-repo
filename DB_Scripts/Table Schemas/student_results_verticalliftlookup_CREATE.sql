﻿-- Table: student_results_verticalliftlookup

-- DROP TABLE student_results_verticalliftlookup;

CREATE TABLE student_results_verticalliftlookup
(
  uid serial NOT NULL, -- Unique Primary Key ID for student_results_verticalliftlookup table.
  verticalliftid integer NOT NULL, -- FK to student_results_verticallift table uid.
  key text NOT NULL, -- Key which labels the parameter being saved.
  value text NOT NULL, -- Value which stores the actual parameter.
  CONSTRAINT student_results_verticalliftlookup_pkey PRIMARY KEY (uid),
  CONSTRAINT student_results_verticalliftlookup_verticalliftid_fkey FOREIGN KEY (verticalliftid)
      REFERENCES student_results_verticallift (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE student_results_verticalliftlookup
  IS 'Key-Value Table for storing lookup data per module in a row-oriented configuration.  Table is related to the student_results_verticallift table.';
COMMENT ON COLUMN student_results_verticalliftlookup.uid IS 'Unique Primary Key ID for student_results_verticalliftlookup table.';
COMMENT ON COLUMN student_results_verticalliftlookup.verticalliftid IS 'FK to student_results_verticallift table uid.';
COMMENT ON COLUMN student_results_verticalliftlookup.key IS 'Key which labels the parameter being saved.';
COMMENT ON COLUMN student_results_verticalliftlookup.value IS 'Value which stores the actual parameter.';

