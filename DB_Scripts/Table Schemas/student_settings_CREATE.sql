﻿-- Table: student_settings

-- DROP TABLE student_settings;

CREATE TABLE student_settings
(
  id serial NOT NULL, -- Unique Primary Key ID for student_settings table.
  studentid integer NOT NULL, -- FK to students table uid.
  setting1 text NOT NULL DEFAULT 'setting 1'::text, -- Placeholder for setting 1.
  setting2 text NOT NULL DEFAULT 'setting 2'::text, -- Placeholder for setting 2.
  setting3 text NOT NULL DEFAULT 'setting 3'::text, -- Placeholder for setting 3.
  CONSTRAINT student_settings_pkey1 PRIMARY KEY (id),
  CONSTRAINT student_settings_studentid_fkey FOREIGN KEY (studentid)
      REFERENCES students (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT student_settings_studentid_key UNIQUE (studentid)
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE student_settings
  IS 'Table for storing settings unique for a student account.';
COMMENT ON COLUMN student_settings.id IS 'Unique Primary Key ID for student_settings table.';
COMMENT ON COLUMN student_settings.studentid IS 'FK to students table uid.';
COMMENT ON COLUMN student_settings.setting1 IS 'Placeholder for setting 1.';
COMMENT ON COLUMN student_settings.setting2 IS 'Placeholder for setting 2.';
COMMENT ON COLUMN student_settings.setting3 IS 'Placeholder for setting 3.';

