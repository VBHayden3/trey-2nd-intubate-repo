﻿-- Table: students

-- DROP TABLE students;

CREATE TABLE students
(
  uid serial NOT NULL, -- unique student id#
  accountid integer, -- linked account id#
  organizationid integer, -- linked organization id#
  firstname text NOT NULL DEFAULT ''::text, -- student's first name
  lastname text NOT NULL DEFAULT ''::text, -- student's last name
  enrolledcourseids integer[] NOT NULL, -- Array of course ids the student has been enrolled in.
  CONSTRAINT students_pkey PRIMARY KEY (uid),
  CONSTRAINT students_account_fkey FOREIGN KEY (accountid)
      REFERENCES accounts (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT students_organization_key FOREIGN KEY (organizationid)
      REFERENCES organizations (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE students
  IS 'Table of all students';
COMMENT ON COLUMN students.uid IS 'unique student id#';
COMMENT ON COLUMN students.accountid IS 'linked account id#';
COMMENT ON COLUMN students.organizationid IS 'linked organization id#';
COMMENT ON COLUMN students.firstname IS 'student''s first name';
COMMENT ON COLUMN students.lastname IS 'student''s last name';
COMMENT ON COLUMN students.enrolledcourseids IS 'Array of course ids the student has been enrolled in.';


-- Index: fki_students_account_fkey

-- DROP INDEX fki_students_account_fkey;

CREATE INDEX fki_students_account_fkey
  ON students
  USING btree
  (accountid);

