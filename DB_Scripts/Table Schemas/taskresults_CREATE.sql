﻿-- Table: taskresults

-- DROP TABLE taskresults;

CREATE TABLE taskresults
(
  uid serial NOT NULL, -- Unique Primary Key ID for taskresults table.
  studentid integer NOT NULL, -- FK to students table uid.
  assignmentid integer, -- Linked assignment uid number
  tasktype "TaskType" NOT NULL DEFAULT 'Intubation'::"TaskType", -- Task Type enum (CPR, Intubation , LateralTransfer , VerticalLift)
  masterylevel "MasteryLevel" NOT NULL DEFAULT 'Novice'::"MasteryLevel", -- MasteryLevel enum (Novice, Intermediate, Master)
  taskmode "TaskMode" NOT NULL DEFAULT 'Practice'::"TaskMode", -- TaskMode enum (Practice, Test)
  score double precision NOT NULL DEFAULT 0, -- Total computed single number score for the Lesson.
  submissiontime timestamp without time zone NOT NULL DEFAULT now(), -- Lesson Results Submission Date and Time.
  starttime timestamp without time zone NOT NULL DEFAULT now(), -- Lesson Start Date and Time.
  endtime timestamp without time zone NOT NULL DEFAULT now(), -- Lesson End Date and Time.
  CONSTRAINT taskresults_pkey PRIMARY KEY (uid),
  CONSTRAINT taskresults_studentid_fkey FOREIGN KEY (studentid)
      REFERENCES students (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE taskresults
  IS 'Top-Level Task Results Table. Stores common Lesson results in a column-oriented design.';
COMMENT ON COLUMN taskresults.uid IS 'Unique Primary Key ID for taskresults table.';
COMMENT ON COLUMN taskresults.studentid IS 'FK to students table uid.';
COMMENT ON COLUMN taskresults.assignmentid IS 'Linked assignment uid number';
COMMENT ON COLUMN taskresults.tasktype IS 'Task Type enum (CPR, Intubation , LateralTransfer , VerticalLift)';
COMMENT ON COLUMN taskresults.masterylevel IS 'MasteryLevel enum (Novice, Intermediate, Master)';
COMMENT ON COLUMN taskresults.taskmode IS 'TaskMode enum (Practice, Test)';
COMMENT ON COLUMN taskresults.score IS 'Total computed single number score for the Lesson.';
COMMENT ON COLUMN taskresults.submissiontime IS 'Lesson Results Submission Date and Time.';
COMMENT ON COLUMN taskresults.starttime IS 'Lesson Start Date and Time.';
COMMENT ON COLUMN taskresults.endtime IS 'Lesson End Date and Time.';

