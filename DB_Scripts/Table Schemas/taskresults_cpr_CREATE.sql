﻿-- Table: taskresults_cpr

-- DROP TABLE taskresults_cpr;

CREATE TABLE taskresults_cpr
(
  uid serial NOT NULL, -- Unique Primary Key ID for taskresults_cpr table.
  resultid integer NOT NULL, -- FK to taskresults table uid.
  calibrationimageid integer, -- FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.
  askedforaed boolean, -- CPR asked for AED value.
  askedfor911 boolean, -- CPR asked for 911 value.
  askedifok boolean, -- CPR asked if OK value.
  checkpulseimageid integer, -- FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.
  checkedpulse "CheckPulse", -- CPR checked pulse value. ENUM (No, Incorrectly, Correctly)
  feedbackids integer[], -- Array of feedback ids used to link this result to its feedback.
  CONSTRAINT taskresults_cpr_pkey PRIMARY KEY (uid),
  CONSTRAINT kinect_images_fk FOREIGN KEY (calibrationimageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT kinect_images_fk2 FOREIGN KEY (checkpulseimageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_cpr_resultsid_fkey FOREIGN KEY (resultid)
      REFERENCES taskresults (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_cpr_unique UNIQUE (calibrationimageid, checkpulseimageid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE taskresults_cpr
  OWNER TO aims;
COMMENT ON TABLE taskresults_cpr
  IS 'CPR Results Table. Stores detailed and unique stage results in a column-oriented design.';
COMMENT ON COLUMN taskresults_cpr.uid IS 'Unique Primary Key ID for taskresults_cpr table.';
COMMENT ON COLUMN taskresults_cpr.resultid IS 'FK to taskresults table uid.';
COMMENT ON COLUMN taskresults_cpr.calibrationimageid IS 'FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';
COMMENT ON COLUMN taskresults_cpr.askedforaed IS 'CPR asked for AED value.';
COMMENT ON COLUMN taskresults_cpr.askedfor911 IS 'CPR asked for 911 value.';
COMMENT ON COLUMN taskresults_cpr.askedifok IS 'CPR asked if OK value.';
COMMENT ON COLUMN taskresults_cpr.checkpulseimageid IS 'FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';
COMMENT ON COLUMN taskresults_cpr.checkedpulse IS 'CPR checked pulse value. ENUM (No, Incorrectly, Correctly) ';
COMMENT ON COLUMN taskresults_cpr.feedbackids IS 'Array of feedback ids used to link this result to its feedback.';

