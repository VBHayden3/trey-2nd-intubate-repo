﻿-- Table: taskresults_cpr_compressions

-- DROP TABLE taskresults_cpr_compressions;

CREATE TABLE taskresults_cpr_compressions
(
  compressionset integer NOT NULL, -- Part of the taskresults_cpr_compressions PK. Also identifies the ordering of compression sets for a CPR Lesson run.
  taskresults_cpr_uid integer NOT NULL, -- FK to taskresults_cpr table. Also part of taskresults_cpr_compressions PK.
  avgdepth double precision DEFAULT 0, -- Average depth of compressions for a CPR set of compressions.
  avgrate double precision DEFAULT 0, -- Average rate of compressions for a CPR set of compressions.
  avgarmslocked double precision DEFAULT 0, -- Percent average of arms being locked across compressions for a CPR set of compressions.
  avgshouldersovermanikin double precision DEFAULT 0, -- Percent average of shoulders being over the manikin across compressions for a CPR setof compressions.
  score double precision DEFAULT 0, -- Score for a set of CPR compressions.
  compressionimageid integer, -- FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.
  feedbackids integer[], -- Array of feedback ids used to link this compression set to its feedback.
  CONSTRAINT taskresults_cpr_compressions_pk PRIMARY KEY (compressionset, taskresults_cpr_uid),
  CONSTRAINT kinect_images_fk FOREIGN KEY (compressionimageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_cpr_fk FOREIGN KEY (taskresults_cpr_uid)
      REFERENCES taskresults_cpr (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_cpr_compressions_unique UNIQUE (compressionimageid)
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE taskresults_cpr_compressions 
  IS 'CPR Results Compression Sets Table. Stores compression set specific details to a CPR result.';
COMMENT ON COLUMN taskresults_cpr_compressions.compressionset IS 'Part of the taskresults_cpr_compressions PK. Also identifies the ordering of compression sets for a CPR Lesson run.';
COMMENT ON COLUMN taskresults_cpr_compressions.taskresults_cpr_uid IS 'FK to taskresults_cpr table. Also part of taskresults_cpr_compressions PK.';
COMMENT ON COLUMN taskresults_cpr_compressions.avgrate IS 'Average rate of compressions for a CPR set of compressions.';
COMMENT ON COLUMN taskresults_cpr_compressions.avgdepth IS 'Average depth of compressions for a CPR set of compressions.';
COMMENT ON COLUMN taskresults_cpr_compressions.avgarmslocked IS 'Percent average of arms being locked across compressions for a CPR set of compressions.';
COMMENT ON COLUMN taskresults_cpr_compressions.avgshouldersovermanikin IS 'Percent average of shoulders being over the manikin across compressions for a CPR setof compressions.';
COMMENT ON COLUMN taskresults_cpr_compressions.score IS 'Score for a set of CPR compressions.';
COMMENT ON COLUMN taskresults_cpr_compressions.compressionimageid IS 'FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';
COMMENT ON COLUMN taskresults_cpr_compressions.feedbackids IS 'Array of feedback ids used to link this compression set to its feedback.';
