﻿-- Table: taskresults_intubation

-- DROP TABLE taskresults_intubation;

CREATE TABLE taskresults_intubation
(
  uid integer NOT NULL DEFAULT nextval('taskresult_intubation_uid_seq'::regclass), -- unique identifier
  resultid integer NOT NULL, -- linked result id
  calibrationimageid integer, -- pointer to the calibration image, if one exists.
  tiltheadscore double precision, -- score for tilting the head
  tiltheadimageid integer, -- pointer to the tilt head image, if one exists.
  tiltheadfeedbackids integer[] NOT NULL DEFAULT '{}'::integer[], -- array of pointers to feedback entries for tilting the head.
  grablaryngoscopescore double precision, -- score for grabbing the laryngoscope.
  grablaryngoscopeimageid integer, -- pointer to the grab laryngoscope image, if one exists.
  grablaryngoscopefeedbackids integer[] NOT NULL DEFAULT '{}'::integer[], -- array of pointers to feedback entries for grabbing the laryngoscope.
  insertlaryngoscopescore double precision, -- score for inserting the laryngoscope.
  insertlaryngoscopeimageid integer, -- pointer to the insert laryngoscope image, if one exists.
  insertlaryngoscopefeedbackids integer[] NOT NULL DEFAULT '{}'::integer[], -- array of pointers to feedback entries for inserting the laryngoscope.
  viewvocalchordsscore double precision, -- score for viewing the vocal chords.
  viewvocalchordsimageid integer, -- pointer to the view vocal chords image, if one exists.
  viewvocalchordsfeedbackids integer[] NOT NULL DEFAULT '{}'::integer[], -- array of pointers to feedback entries for viewing the vocal chords.
  grabendotrachealtubescore double precision, -- score for grabbing the endotracheal tube.
  grabendotrachealtubeimageid integer, -- pointer to the grab endotracheal tube image, if one exists.
  grabendotrachealtubefeedbackids integer[] NOT NULL DEFAULT '{}'::integer[], -- array of pointers to feedback entries for grabbing the endotracheal tube.
  insertendotrachealtubescore double precision, -- score for inserting the endotracheal tube.
  insertendotrachealtubeimageid integer, -- pointer to the insert endotracheal tube image, if one exists.
  insertendotrachealtubefeedbackids integer[] NOT NULL DEFAULT '{}'::integer[], -- array of pointers to feedback entries for inserting the endotracheal tube.
  removelaryngoscopescore double precision, -- score for removing the laryngoscope.
  removelaryngoscopeimageid integer, -- pointer to the remove laryngoscope image, if one exists.
  removelaryngoscopefeedbackids integer[] NOT NULL DEFAULT '{}'::integer[], -- array of pointers for feedback entries about removing the laryngoscope.
  usesyringescore double precision, -- score for using the syringe.
  usesyringeimageid integer, -- pointer to the using syringe image, if one exists.
  usesyringefeedbackids integer[] NOT NULL DEFAULT '{}'::integer[], -- array of pointers to feedback for using the syringe.
  removestyletscore double precision, -- score for removing the stylet.
  removestyletimageid integer, -- pointer to the remove stylet image, if one exists.
  removestyletfeedbackids integer[] NOT NULL DEFAULT '{}'::integer[], -- array of pointers to feedback entries for removing the stylet.
  baggingscore double precision, -- score for bagging the endotracheal tube.
  baggingimageid integer, -- pointer to the bagging image, if one exists.
  baggingfeedbackids integer[] NOT NULL DEFAULT '{}'::integer[], -- array of pointers to feedback entries for bagging the endotracheal tube.
  chestrisescore double precision, -- score for chest rise and fall.
  chestriseimageid integer, -- pointer to the chest rise and fall image, if one exists.
  chestrisefeedbackids integer[] NOT NULL DEFAULT '{}'::integer[], -- array of pointers to feedback entries for chest rise and fall.
  overallfeedbackids integer[] NOT NULL DEFAULT '{}'::integer[], -- array of pointers to feedback entries for the overall assessment of the task
  CONSTRAINT taskresult_intubation_pkey PRIMARY KEY (uid),
  CONSTRAINT intubationresult_overallresult_fkey FOREIGN KEY (resultid)
      REFERENCES taskresults (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE taskresults_intubation
  OWNER TO aims;
COMMENT ON TABLE taskresults_intubation
  IS 'Intubation Results Table. Stores detailed and unique stage results in a column-oriented design.';
COMMENT ON COLUMN taskresults_intubation.uid IS 'unique identifier';
COMMENT ON COLUMN taskresults_intubation.resultid IS 'linked result id';
COMMENT ON COLUMN taskresults_intubation.calibrationimageid IS 'pointer to the calibration image, if one exists.';
COMMENT ON COLUMN taskresults_intubation.tiltheadscore IS 'score for tilting the head';
COMMENT ON COLUMN taskresults_intubation.tiltheadimageid IS 'pointer to the tilt head image, if one exists.';
COMMENT ON COLUMN taskresults_intubation.tiltheadfeedbackids IS 'array of pointers to feedback entries for tilting the head.';
COMMENT ON COLUMN taskresults_intubation.grablaryngoscopescore IS 'score for grabbing the laryngoscope.';
COMMENT ON COLUMN taskresults_intubation.grablaryngoscopeimageid IS 'pointer to the grab laryngoscope image, if one exists.';
COMMENT ON COLUMN taskresults_intubation.grablaryngoscopefeedbackids IS 'array of pointers to feedback entries for grabbing the laryngoscope.';
COMMENT ON COLUMN taskresults_intubation.insertlaryngoscopescore IS 'score for inserting the laryngoscope.';
COMMENT ON COLUMN taskresults_intubation.insertlaryngoscopeimageid IS 'pointer to the insert laryngoscope image, if one exists.';
COMMENT ON COLUMN taskresults_intubation.insertlaryngoscopefeedbackids IS 'array of pointers to feedback entries for inserting the laryngoscope.';
COMMENT ON COLUMN taskresults_intubation.viewvocalchordsscore IS 'score for viewing the vocal chords.';
COMMENT ON COLUMN taskresults_intubation.viewvocalchordsimageid IS 'pointer to the view vocal chords image, if one exists.';
COMMENT ON COLUMN taskresults_intubation.viewvocalchordsfeedbackids IS 'array of pointers to feedback entries for viewing the vocal chords.';
COMMENT ON COLUMN taskresults_intubation.grabendotrachealtubescore IS 'score for grabbing the endotracheal tube.';
COMMENT ON COLUMN taskresults_intubation.grabendotrachealtubeimageid IS 'pointer to the grab endotracheal tube image, if one exists.';
COMMENT ON COLUMN taskresults_intubation.grabendotrachealtubefeedbackids IS 'array of pointers to feedback entries for grabbing the endotracheal tube.';
COMMENT ON COLUMN taskresults_intubation.insertendotrachealtubescore IS 'score for inserting the endotracheal tube.';
COMMENT ON COLUMN taskresults_intubation.insertendotrachealtubeimageid IS 'pointer to the insert endotracheal tube image, if one exists.';
COMMENT ON COLUMN taskresults_intubation.insertendotrachealtubefeedbackids IS 'array of pointers to feedback entries for inserting the endotracheal tube.';
COMMENT ON COLUMN taskresults_intubation.removelaryngoscopescore IS 'score for removing the laryngoscope.';
COMMENT ON COLUMN taskresults_intubation.removelaryngoscopeimageid IS 'pointer to the remove laryngoscope image, if one exists.';
COMMENT ON COLUMN taskresults_intubation.removelaryngoscopefeedbackids IS 'array of pointers for feedback entries about removing the laryngoscope.';
COMMENT ON COLUMN taskresults_intubation.usesyringescore IS 'score for using the syringe.';
COMMENT ON COLUMN taskresults_intubation.usesyringeimageid IS 'pointer to the using syringe image, if one exists.';
COMMENT ON COLUMN taskresults_intubation.usesyringefeedbackids IS 'array of pointers to feedback for using the syringe.';
COMMENT ON COLUMN taskresults_intubation.removestyletscore IS 'score for removing the stylet.';
COMMENT ON COLUMN taskresults_intubation.removestyletimageid IS 'pointer to the remove stylet image, if one exists.';
COMMENT ON COLUMN taskresults_intubation.removestyletfeedbackids IS 'array of pointers to feedback entries for removing the stylet.';
COMMENT ON COLUMN taskresults_intubation.baggingscore IS 'score for bagging the endotracheal tube.';
COMMENT ON COLUMN taskresults_intubation.baggingimageid IS 'pointer to the bagging image, if one exists.';
COMMENT ON COLUMN taskresults_intubation.baggingfeedbackids IS 'array of pointers to feedback entries for bagging the endotracheal tube.';
COMMENT ON COLUMN taskresults_intubation.chestrisescore IS 'score for chest rise and fall.';
COMMENT ON COLUMN taskresults_intubation.chestriseimageid IS 'pointer to the chest rise and fall image, if one exists.';
COMMENT ON COLUMN taskresults_intubation.chestrisefeedbackids IS 'array of pointers to feedback entries for chest rise and fall.';
COMMENT ON COLUMN taskresults_intubation.overallfeedbackids IS 'array of pointers to feedback entries for the overall assessment of the task';

