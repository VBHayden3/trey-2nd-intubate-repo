﻿-- Table: taskresults_verticallift

-- DROP TABLE taskresults_verticallift;

CREATE TABLE taskresults_verticallift
(
  uid serial NOT NULL, -- Unique Primary Key ID for taskresults_verticallift table.
  resultid integer NOT NULL, -- FK to taskresults table uid.
  avgbackstraight double precision NOT NULL DEFAULT 0, -- Average percentage that the users back was straight during repetitions of vertical lift.
  avgclosetobody double precision NOT NULL DEFAULT 0, -- Average percentage that the object being lifted was close to the user' body during repetitions of vertical lift
  avgfootspacing double precision NOT NULL DEFAULT 0, -- Average percentage that the users foot stance was a good stance during repetitions of vertical lift
  calibrationimageid integer, -- FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.
  feedbackids integer[], -- Array of feedback ids used to link this results to its feedback.
  CONSTRAINT taskresults_verticallift_pkey PRIMARY KEY (uid),
  CONSTRAINT kinect_images_fk FOREIGN KEY (calibrationimageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_verticallift_resultsid_fkey FOREIGN KEY (resultid)
      REFERENCES taskresults (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_verticallift_unique UNIQUE (calibrationimageid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE taskresults_verticallift
  OWNER TO postgres;
COMMENT ON TABLE taskresults_verticallift
  IS 'Vertical Lift Results Table. Stores detailed and unique stage results in a column-oriented design.';
COMMENT ON COLUMN taskresults_verticallift.uid IS 'Unique Primary Key ID for taskresults_verticallift table.';
COMMENT ON COLUMN taskresults_verticallift.resultid IS 'FK to taskresults table uid.';
COMMENT ON COLUMN taskresults_verticallift.avgbackstraight IS 'Average percentage that the users back was straight during repetitions of vertical lift.';
COMMENT ON COLUMN taskresults_verticallift.avgclosetobody IS 'Average percentage that the object being lifted was close to the user'' body during repetitions of vertical lift';
COMMENT ON COLUMN taskresults_verticallift.avgfootspacing IS 'Average percentage that the users foot stance was a good stance during repetitions of vertical lift';
COMMENT ON COLUMN taskresults_verticallift.calibrationimageid IS 'FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';
COMMENT ON COLUMN taskresults_verticallift.feedbackids IS 'Array of feedback ids used to link this results to its feedback.';

