-- Table: taskresults_verticallift_reps

-- DROP TABLE taskresults_verticallift_reps;

CREATE TABLE taskresults_verticallift_reps
(
  taskresults_verticallift_uid integer NOT NULL, -- FK to taskresults_verticallift and part of the PK
  repid integer NOT NULL, -- Identifies which repetition this is in the set associated with a verticallift task run. Part of the PK.
  backstraight boolean NOT NULL DEFAULT false, -- Identifies whether or not the back was straight during a vertical lift.
  closetobody boolean NOT NULL DEFAULT false, -- Identifies whether or not the object being lifted was close to the users body during a vertical lift.
  footspacing "FootSpacing", -- Identifies the foot stance during a vertical lift.
  score double precision NOT NULL DEFAULT 0, -- The score of this vertical lift.
  downimageid integer, -- The image id of the 'down' stage.
  liftobjectimageid integer, -- The image id of the 'liftobject' stage.
  lowerobjectimageid integer, -- The image id of the 'lowerobject' stage.
  riseimageid integer, -- The image id during of the 'rise' stage.
  feedbackids integer[], -- Array of feedback ids used to link this results to its feedback.
  CONSTRAINT taskresults_verticallift_reps_pkey PRIMARY KEY (taskresults_verticallift_uid, repid),
  CONSTRAINT kinect_images_fk FOREIGN KEY (downimageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT kinect_images_fk2 FOREIGN KEY (liftobjectimageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT kinect_images_fk3 FOREIGN KEY (lowerobjectimageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT kinect_images_fk4 FOREIGN KEY (riseimageid)
      REFERENCES kinect_images (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT taskresults_verticallift_fk FOREIGN KEY (taskresults_verticallift_uid)
      REFERENCES taskresults_verticallift (uid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE taskresults_verticallift_reps
  OWNER TO aims;
COMMENT ON TABLE taskresults_verticallift_reps
  IS 'Vertical Lift Reps Table. Stores values collected across multiple repetitions during a Vertical Lift task.';
COMMENT ON COLUMN taskresults_verticallift_reps.taskresults_verticallift_uid IS 'FK to taskresults_verticallift and part of the PK';
COMMENT ON COLUMN taskresults_verticallift_reps.repid IS 'Identifies which repetition this is in the set associated with a verticallift task run. Part of the PK.';
COMMENT ON COLUMN taskresults_verticallift_reps.backstraight IS 'Identifies whether or not the back was straight during a vertical lift.';
COMMENT ON COLUMN taskresults_verticallift_reps.closetobody IS 'Identifies whether or not the object being lifted was close to the users body during a vertical lift.';
COMMENT ON COLUMN taskresults_verticallift_reps.footspacing IS 'Identifies the foot stance during a vertical lift.';
COMMENT ON COLUMN taskresults_verticallift_reps.score IS 'The score of this vertical lift.';
COMMENT ON COLUMN taskresults_verticallift_reps.downimageid IS 'The image id of the ''down'' stage.';
COMMENT ON COLUMN taskresults_verticallift_reps.liftobjectimageid IS 'The image id of the ''liftobject'' stage.';
COMMENT ON COLUMN taskresults_verticallift_reps.lowerobjectimageid IS 'The image id of the ''lowerobject'' stage.';
COMMENT ON COLUMN taskresults_verticallift_reps.riseimageid IS 'The image id during of the ''rise'' stage.';
COMMENT ON COLUMN taskresults_verticallift_reps.feedbackids IS 'Array of feedback ids used to link this results to its feedback.';

