--
-- PostgreSQL database dump
--

-- Dumped from database version 9.2.3
-- Dumped by pg_dump version 9.2.3
-- Started on 2013-07-29 13:28:45

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 2366 (class 1262 OID 40957)
-- Dependencies: 2365
-- Name: aims; Type: COMMENT; Schema: -; Owner: aims
--

COMMENT ON DATABASE aims IS 'aims localhost db';


--
-- TOC entry 226 (class 3079 OID 11727)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2369 (class 0 OID 0)
-- Dependencies: 226
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 697 (class 1247 OID 50174)
-- Name: CheckPulse; Type: TYPE; Schema: public; Owner: aims
--

CREATE TYPE "CheckPulse" AS ENUM (
    'No',
    'Incorrectly',
    'Correctly'
);


ALTER TYPE public."CheckPulse" OWNER TO aims;

--
-- TOC entry 594 (class 1247 OID 40959)
-- Name: FeedbackType; Type: TYPE; Schema: public; Owner: aims
--

CREATE TYPE "FeedbackType" AS ENUM (
    'None',
    'Good',
    'Bad',
    'Neutral'
);


ALTER TYPE public."FeedbackType" OWNER TO aims;

--
-- TOC entry 700 (class 1247 OID 49408)
-- Name: FootSpacing; Type: TYPE; Schema: public; Owner: aims
--

CREATE TYPE "FootSpacing" AS ENUM (
    'Ok',
    'Narrow',
    'Wide'
);


ALTER TYPE public."FootSpacing" OWNER TO aims;

--
-- TOC entry 2370 (class 0 OID 0)
-- Dependencies: 700
-- Name: TYPE "FootSpacing"; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TYPE "FootSpacing" IS 'Represents the stance of a users feet.';


--
-- TOC entry 591 (class 1247 OID 40968)
-- Name: MasteryLevel; Type: TYPE; Schema: public; Owner: aims
--

CREATE TYPE "MasteryLevel" AS ENUM (
    'Novice',
    'Intermediate',
    'Master'
);


ALTER TYPE public."MasteryLevel" OWNER TO aims;

--
-- TOC entry 2371 (class 0 OID 0)
-- Dependencies: 591
-- Name: TYPE "MasteryLevel"; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TYPE "MasteryLevel" IS 'AIMS User Training Level.';


--
-- TOC entry 588 (class 1247 OID 40976)
-- Name: TaskMode; Type: TYPE; Schema: public; Owner: aims
--

CREATE TYPE "TaskMode" AS ENUM (
    'Practice',
    'Test'
);


ALTER TYPE public."TaskMode" OWNER TO aims;

--
-- TOC entry 2372 (class 0 OID 0)
-- Dependencies: 588
-- Name: TYPE "TaskMode"; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TYPE "TaskMode" IS 'AIMS Training Mode.';


--
-- TOC entry 585 (class 1247 OID 40982)
-- Name: TaskType; Type: TYPE; Schema: public; Owner: aims
--

CREATE TYPE "TaskType" AS ENUM (
    'CPR',
    'Intubation',
    'LateralTransfer',
    'VerticalLift'
);


ALTER TYPE public."TaskType" OWNER TO aims;

--
-- TOC entry 2373 (class 0 OID 0)
-- Dependencies: 585
-- Name: TYPE "TaskType"; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TYPE "TaskType" IS 'AIMS Lesson Type.';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 168 (class 1259 OID 40999)
-- Name: accounts; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE accounts (
    uid integer NOT NULL,
    username text,
    password text,
    accesslevel smallint,
    creationdate timestamp without time zone DEFAULT now(),
    expirationdate timestamp without time zone,
    changepasswordnextlogin boolean DEFAULT true NOT NULL,
    email text,
    lastlogin timestamp without time zone,
    lastloginip inet,
    organizationid integer,
    securityquestion text,
    securityanswer text
);


ALTER TABLE public.accounts OWNER TO aims;

--
-- TOC entry 2374 (class 0 OID 0)
-- Dependencies: 168
-- Name: TABLE accounts; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE accounts IS 'table of all accounts';


--
-- TOC entry 2375 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN accounts.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN accounts.uid IS 'unique account id #';


--
-- TOC entry 2376 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN accounts.username; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN accounts.username IS 'account username';


--
-- TOC entry 2377 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN accounts.password; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN accounts.password IS 'account password';


--
-- TOC entry 2378 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN accounts.accesslevel; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN accounts.accesslevel IS 'account accesslevel (this is in int format and represents the AccessLevel enum value casted to an int)';


--
-- TOC entry 2379 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN accounts.creationdate; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN accounts.creationdate IS 'date and time which the account was created.';


--
-- TOC entry 2380 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN accounts.expirationdate; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN accounts.expirationdate IS 'date and time the account is declared Expired.';


--
-- TOC entry 2381 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN accounts.changepasswordnextlogin; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN accounts.changepasswordnextlogin IS 'initialized to true, determines if the account user needs to change their password the next time they login.';


--
-- TOC entry 2382 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN accounts.email; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN accounts.email IS 'account email address';


--
-- TOC entry 2383 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN accounts.lastlogin; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN accounts.lastlogin IS 'date and time the user last logged in';


--
-- TOC entry 2384 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN accounts.lastloginip; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN accounts.lastloginip IS 'ip address of the last successful login';


--
-- TOC entry 2385 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN accounts.organizationid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN accounts.organizationid IS 'unique organization id# which the account belongs to';


--
-- TOC entry 169 (class 1259 OID 41007)
-- Name: accounts_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE accounts_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.accounts_uid_seq OWNER TO aims;

--
-- TOC entry 2386 (class 0 OID 0)
-- Dependencies: 169
-- Name: accounts_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE accounts_uid_seq OWNED BY accounts.uid;


--
-- TOC entry 170 (class 1259 OID 41009)
-- Name: administrator_settings; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE administrator_settings (
    id integer NOT NULL,
    administratorid integer NOT NULL,
    setting1 text DEFAULT 'setting 1'::text NOT NULL,
    setting2 text DEFAULT 'setting 2'::text NOT NULL,
    setting3 text DEFAULT 'setting 3'::text NOT NULL
);


ALTER TABLE public.administrator_settings OWNER TO aims;

--
-- TOC entry 2387 (class 0 OID 0)
-- Dependencies: 170
-- Name: TABLE administrator_settings; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE administrator_settings IS 'Table for storing settings unique for a administrator account.';


--
-- TOC entry 2388 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN administrator_settings.id; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN administrator_settings.id IS 'Unique Primary Key ID for administrator_settings table.';


--
-- TOC entry 2389 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN administrator_settings.administratorid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN administrator_settings.administratorid IS 'FK to administrators table uid.';


--
-- TOC entry 2390 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN administrator_settings.setting1; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN administrator_settings.setting1 IS 'Placeholder for setting 1.';


--
-- TOC entry 2391 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN administrator_settings.setting2; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN administrator_settings.setting2 IS 'Placeholder for setting 2.';


--
-- TOC entry 2392 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN administrator_settings.setting3; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN administrator_settings.setting3 IS 'Placeholder for setting 3.';


--
-- TOC entry 171 (class 1259 OID 41018)
-- Name: administrator_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE administrator_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.administrator_settings_id_seq OWNER TO aims;

--
-- TOC entry 2393 (class 0 OID 0)
-- Dependencies: 171
-- Name: administrator_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE administrator_settings_id_seq OWNED BY administrator_settings.id;


--
-- TOC entry 172 (class 1259 OID 41020)
-- Name: administrators; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE administrators (
    uid integer NOT NULL,
    organizationid integer,
    accountid integer,
    firstname text,
    lastname text
);


ALTER TABLE public.administrators OWNER TO aims;

--
-- TOC entry 2394 (class 0 OID 0)
-- Dependencies: 172
-- Name: TABLE administrators; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE administrators IS 'Table of all administrators';


--
-- TOC entry 2395 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN administrators.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN administrators.uid IS 'unique administrator id#';


--
-- TOC entry 2396 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN administrators.organizationid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN administrators.organizationid IS 'organization id # which the administrator belongs to';


--
-- TOC entry 2397 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN administrators.accountid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN administrators.accountid IS 'linked account id #';


--
-- TOC entry 2398 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN administrators.firstname; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN administrators.firstname IS 'administrators first name';


--
-- TOC entry 2399 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN administrators.lastname; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN administrators.lastname IS 'administrators last name';


--
-- TOC entry 173 (class 1259 OID 41026)
-- Name: administrators_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE administrators_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.administrators_uid_seq OWNER TO aims;

--
-- TOC entry 2400 (class 0 OID 0)
-- Dependencies: 173
-- Name: administrators_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE administrators_uid_seq OWNED BY administrators.uid;


--
-- TOC entry 174 (class 1259 OID 41028)
-- Name: course_assignments; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE course_assignments (
    uid integer NOT NULL,
    courseid integer,
    tasktype integer,
    taskmode integer,
    startdate timestamp without time zone,
    duedate timestamp without time zone,
    name text,
    description text
);


ALTER TABLE public.course_assignments OWNER TO aims;

--
-- TOC entry 2401 (class 0 OID 0)
-- Dependencies: 174
-- Name: TABLE course_assignments; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE course_assignments IS 'Table of all course assignements';


--
-- TOC entry 2402 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN course_assignments.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN course_assignments.uid IS 'unique id #';


--
-- TOC entry 2403 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN course_assignments.courseid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN course_assignments.courseid IS 'course id# in which the assignemnt belongs';


--
-- TOC entry 2404 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN course_assignments.tasktype; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN course_assignments.tasktype IS 'enum value representing the task type implemented within the assignement';


--
-- TOC entry 2405 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN course_assignments.taskmode; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN course_assignments.taskmode IS 'enum value representing the task mode (practice, test, etc) of the assignment';


--
-- TOC entry 2406 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN course_assignments.startdate; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN course_assignments.startdate IS 'date and time which the assignment becomes available for students';


--
-- TOC entry 2407 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN course_assignments.duedate; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN course_assignments.duedate IS 'date and time which the assignment ends being available for students';


--
-- TOC entry 2408 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN course_assignments.name; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN course_assignments.name IS 'assignment name';


--
-- TOC entry 2409 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN course_assignments.description; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN course_assignments.description IS 'assignment description';


--
-- TOC entry 175 (class 1259 OID 41034)
-- Name: course_assignments_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE course_assignments_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.course_assignments_uid_seq OWNER TO aims;

--
-- TOC entry 2410 (class 0 OID 0)
-- Dependencies: 175
-- Name: course_assignments_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE course_assignments_uid_seq OWNED BY course_assignments.uid;


--
-- TOC entry 176 (class 1259 OID 41036)
-- Name: courses; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE courses (
    uid integer NOT NULL,
    doctorid integer,
    startdate timestamp without time zone,
    enddate timestamp without time zone,
    name text,
    description text,
    hasintubation boolean DEFAULT false NOT NULL,
    hasverticallift boolean DEFAULT false NOT NULL,
    hascpr boolean DEFAULT false NOT NULL
);


ALTER TABLE public.courses OWNER TO aims;

--
-- TOC entry 2411 (class 0 OID 0)
-- Dependencies: 176
-- Name: TABLE courses; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE courses IS 'Table of all courses created by doctors.';


--
-- TOC entry 2412 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN courses.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN courses.uid IS 'unique course id#';


--
-- TOC entry 2413 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN courses.doctorid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN courses.doctorid IS 'id# of the doctor who created the course';


--
-- TOC entry 2414 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN courses.startdate; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN courses.startdate IS 'date and time that the course begins being available';


--
-- TOC entry 2415 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN courses.enddate; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN courses.enddate IS 'date and time that the course ends being available';


--
-- TOC entry 2416 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN courses.name; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN courses.name IS 'course name';


--
-- TOC entry 2417 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN courses.description; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN courses.description IS 'course description';


--
-- TOC entry 2418 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN courses.hasintubation; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN courses.hasintubation IS 'Does the class include intubation';


--
-- TOC entry 2419 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN courses.hasverticallift; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN courses.hasverticallift IS 'Does the course include vertical lift';


--
-- TOC entry 2420 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN courses.hascpr; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN courses.hascpr IS 'Does the course include CPR';


--
-- TOC entry 177 (class 1259 OID 41045)
-- Name: courses_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE courses_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.courses_uid_seq OWNER TO aims;

--
-- TOC entry 2421 (class 0 OID 0)
-- Dependencies: 177
-- Name: courses_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE courses_uid_seq OWNED BY courses.uid;


--
-- TOC entry 178 (class 1259 OID 41047)
-- Name: doctor_settings; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE doctor_settings (
    id integer NOT NULL,
    doctorid integer NOT NULL,
    setting1 text DEFAULT 'setting 1'::text NOT NULL,
    setting2 text DEFAULT 'setting 2'::text NOT NULL,
    setting3 text DEFAULT 'setting 3'::text NOT NULL
);


ALTER TABLE public.doctor_settings OWNER TO aims;

--
-- TOC entry 2422 (class 0 OID 0)
-- Dependencies: 178
-- Name: TABLE doctor_settings; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE doctor_settings IS 'Table for storing settings unique for a doctor account.';


--
-- TOC entry 2423 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN doctor_settings.id; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN doctor_settings.id IS 'Unique Primary Key ID for doctor_settings table.';


--
-- TOC entry 2424 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN doctor_settings.doctorid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN doctor_settings.doctorid IS 'FK to doctos table uid.';


--
-- TOC entry 2425 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN doctor_settings.setting1; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN doctor_settings.setting1 IS 'Placeholder for setting 1.';


--
-- TOC entry 2426 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN doctor_settings.setting2; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN doctor_settings.setting2 IS 'Placeholder for setting 2.';


--
-- TOC entry 2427 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN doctor_settings.setting3; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN doctor_settings.setting3 IS 'Placeholder for setting 3.';


--
-- TOC entry 179 (class 1259 OID 41056)
-- Name: doctor_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE doctor_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.doctor_settings_id_seq OWNER TO aims;

--
-- TOC entry 2428 (class 0 OID 0)
-- Dependencies: 179
-- Name: doctor_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE doctor_settings_id_seq OWNED BY doctor_settings.id;


--
-- TOC entry 180 (class 1259 OID 41058)
-- Name: doctors; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE doctors (
    uid integer NOT NULL,
    organizationid integer,
    accountid integer,
    firstname text,
    lastname text
);


ALTER TABLE public.doctors OWNER TO aims;

--
-- TOC entry 2429 (class 0 OID 0)
-- Dependencies: 180
-- Name: TABLE doctors; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE doctors IS 'Table of all doctors';


--
-- TOC entry 181 (class 1259 OID 41064)
-- Name: doctors_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE doctors_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.doctors_uid_seq OWNER TO aims;

--
-- TOC entry 2430 (class 0 OID 0)
-- Dependencies: 181
-- Name: doctors_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE doctors_uid_seq OWNED BY doctors.uid;


--
-- TOC entry 182 (class 1259 OID 41066)
-- Name: intubation_statsheets; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE intubation_statsheets (
    uid integer NOT NULL,
    studentid integer,
    timespracticed integer,
    timestested integer,
    masterylevel integer,
    points integer,
    averagenovicetestscore double precision,
    averagenovicepracticescore double precision,
    averageintermediatetestscore double precision,
    averageintermediatepracticescore double precision,
    averagemastertestscore double precision,
    averagemasterpracticescore double precision,
    timestestednovice integer,
    timespracticednovice integer,
    timestestedintermediate integer,
    timespracticedintermediate integer,
    timestestedmaster integer,
    timespracticedmaster integer,
    timelastpracticed timestamp without time zone,
    timelasttested timestamp without time zone,
    lasttestresultid integer,
    lastpracticeresultid integer,
    lastnovicetestresultid integer,
    lastnovicepracticeresultid integer,
    lastintermediatetestresultid integer,
    lastintermediatepracticeresultid integer,
    lastmastertestresultid integer,
    lastmasterpracticeresultid integer
);


ALTER TABLE public.intubation_statsheets OWNER TO aims;

--
-- TOC entry 2431 (class 0 OID 0)
-- Dependencies: 182
-- Name: TABLE intubation_statsheets; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE intubation_statsheets IS 'table to store all intubation statsheets for tracking students';


--
-- TOC entry 2432 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.uid IS 'unique identifier';


--
-- TOC entry 2433 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.studentid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.studentid IS 'linked student number';


--
-- TOC entry 2434 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.timespracticed; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.timespracticed IS 'total times practiced';


--
-- TOC entry 2435 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.timestested; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.timestested IS 'total times tested';


--
-- TOC entry 2436 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.masterylevel; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.masterylevel IS 'current mastery level.
0 = novice
1 = intermediate
2 = master';


--
-- TOC entry 2437 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.points; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.points IS 'current points';


--
-- TOC entry 2438 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.averagenovicetestscore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.averagenovicetestscore IS 'average score of tests taken at novice mastery';


--
-- TOC entry 2439 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.averagenovicepracticescore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.averagenovicepracticescore IS 'average score of practices taken at novice mastery';


--
-- TOC entry 2440 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.averageintermediatetestscore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.averageintermediatetestscore IS 'average score of tests taken at intermediate mastery';


--
-- TOC entry 2441 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.averageintermediatepracticescore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.averageintermediatepracticescore IS 'average score of practices taken at intermediate mastery';


--
-- TOC entry 2442 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.averagemastertestscore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.averagemastertestscore IS 'average score of tests taken at master mastery';


--
-- TOC entry 2443 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.averagemasterpracticescore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.averagemasterpracticescore IS 'average score of practices taken at master mastery';


--
-- TOC entry 2444 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.timestestednovice; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.timestestednovice IS 'number of times tested on novice';


--
-- TOC entry 2445 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.timespracticednovice; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.timespracticednovice IS 'number of times practiced on novice';


--
-- TOC entry 2446 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.timestestedintermediate; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.timestestedintermediate IS 'number of times tested on intermediate';


--
-- TOC entry 2447 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.timespracticedintermediate; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.timespracticedintermediate IS 'number of times practiced on intermediate';


--
-- TOC entry 2448 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.timestestedmaster; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.timestestedmaster IS 'number of times tested on master';


--
-- TOC entry 2449 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.timespracticedmaster; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.timespracticedmaster IS 'number of times practiced on master';


--
-- TOC entry 2450 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.timelastpracticed; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.timelastpracticed IS 'datetime last practiced';


--
-- TOC entry 2451 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.timelasttested; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.timelasttested IS 'datetime last tested';


--
-- TOC entry 2452 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.lasttestresultid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.lasttestresultid IS 'resultID of the last novice test';


--
-- TOC entry 2453 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.lastpracticeresultid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.lastpracticeresultid IS 'result id of the last practice';


--
-- TOC entry 2454 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.lastnovicetestresultid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.lastnovicetestresultid IS 'resultid of the last test run at novice mastery';


--
-- TOC entry 2455 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.lastnovicepracticeresultid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.lastnovicepracticeresultid IS 'resultid of the last practice run at novice';


--
-- TOC entry 2456 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.lastintermediatetestresultid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.lastintermediatetestresultid IS 'resultid of the last test run at intermediate mastery';


--
-- TOC entry 2457 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.lastintermediatepracticeresultid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.lastintermediatepracticeresultid IS 'resultid of the last practice run at intermediate mastery';


--
-- TOC entry 2458 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.lastmastertestresultid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.lastmastertestresultid IS 'resultid of the last test run at master mastery';


--
-- TOC entry 2459 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN intubation_statsheets.lastmasterpracticeresultid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN intubation_statsheets.lastmasterpracticeresultid IS 'resultid of the last practice run at master mastery';


--
-- TOC entry 183 (class 1259 OID 41069)
-- Name: intubation_statsheets_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE intubation_statsheets_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.intubation_statsheets_uid_seq OWNER TO aims;

--
-- TOC entry 2460 (class 0 OID 0)
-- Dependencies: 183
-- Name: intubation_statsheets_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE intubation_statsheets_uid_seq OWNED BY intubation_statsheets.uid;


--
-- TOC entry 184 (class 1259 OID 41071)
-- Name: kinect_images; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE kinect_images (
    uid integer NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    hascolor boolean NOT NULL,
    hasdepth boolean NOT NULL,
    colordata bytea NOT NULL,
    depthdata bytea NOT NULL,
    colorformat smallint NOT NULL,
    depthformat smallint NOT NULL
);


ALTER TABLE public.kinect_images OWNER TO aims;

--
-- TOC entry 2461 (class 0 OID 0)
-- Dependencies: 184
-- Name: TABLE kinect_images; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE kinect_images IS 'Table for storing stage screenshots for all lessons. Color and depth data are in a compressed binary format.';


--
-- TOC entry 2462 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN kinect_images.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN kinect_images.uid IS 'Unique Primary Key ID for Table.';


--
-- TOC entry 2463 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN kinect_images."timestamp"; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN kinect_images."timestamp" IS 'The Date Time of when the kinect image was captured.';


--
-- TOC entry 2464 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN kinect_images.hascolor; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN kinect_images.hascolor IS 'Whether or not the color data exists for this kinect image.';


--
-- TOC entry 2465 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN kinect_images.hasdepth; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN kinect_images.hasdepth IS 'Whether or not the depth data exists for this kinect image.';


--
-- TOC entry 2466 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN kinect_images.colordata; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN kinect_images.colordata IS 'Compressed binary color data.';


--
-- TOC entry 2467 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN kinect_images.depthdata; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN kinect_images.depthdata IS 'Compressed binary depth data.';


--
-- TOC entry 2468 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN kinect_images.colorformat; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN kinect_images.colorformat IS 'Color Image Format.';


--
-- TOC entry 2469 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN kinect_images.depthformat; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN kinect_images.depthformat IS 'Depth Image Format.';


--
-- TOC entry 185 (class 1259 OID 41077)
-- Name: kinect_images_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE kinect_images_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kinect_images_uid_seq OWNER TO aims;

--
-- TOC entry 2470 (class 0 OID 0)
-- Dependencies: 185
-- Name: kinect_images_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE kinect_images_uid_seq OWNED BY kinect_images.uid;


--
-- TOC entry 186 (class 1259 OID 41079)
-- Name: lesson_statsheets; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE lesson_statsheets (
    uid integer NOT NULL,
    studentid integer,
    timespracticed integer,
    timestested integer,
    masterylevel integer,
    points integer,
    averagenovicetestscore double precision,
    averagenovicepracticescore double precision,
    averageintermediatetestscore double precision,
    averageintermediatepracticescore double precision,
    averagemastertestscore double precision,
    averagemasterpracticescore double precision,
    timestestednovice integer,
    timespracticednovice integer,
    timestestedintermediate integer,
    timespracticedintermediate integer,
    timestestedmaster integer,
    timespracticedmaster integer,
    timelastpracticed timestamp without time zone,
    timelasttested timestamp without time zone,
    lasttestresultid integer,
    lastpracticeresultid integer,
    lastnovicetestresultid integer,
    lastnovicepracticeresultid integer,
    lastintermediatetestresultid integer,
    lastintermediatepracticeresultid integer,
    lastmastertestresultid integer,
    lastmasterpracticeresultid integer,
    lessontype integer,
    novicepracticesinsuccession integer,
    intermediatetestsinsuccession integer,
    intermediatepracticesinsuccession integer,
    mastertestsinsuccession integer,
    masterpracticesinsuccession integer,
    novicetestsinsuccession integer
);


ALTER TABLE public.lesson_statsheets OWNER TO aims;

--
-- TOC entry 2471 (class 0 OID 0)
-- Dependencies: 186
-- Name: TABLE lesson_statsheets; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE lesson_statsheets IS 'table of all lesson statsheets';


--
-- TOC entry 2472 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.uid IS 'unique identifier';


--
-- TOC entry 2473 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.studentid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.studentid IS 'linked student number';


--
-- TOC entry 2474 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.timespracticed; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.timespracticed IS 'total times practiced';


--
-- TOC entry 2475 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.timestested; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.timestested IS 'total times tested';


--
-- TOC entry 2476 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.masterylevel; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.masterylevel IS 'current master level';


--
-- TOC entry 2477 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.averagenovicetestscore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.averagenovicetestscore IS 'average score of tests taken at novice level mastery';


--
-- TOC entry 2478 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.averagenovicepracticescore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.averagenovicepracticescore IS 'average score of practices taken at novice level mastery';


--
-- TOC entry 2479 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.averageintermediatetestscore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.averageintermediatetestscore IS 'average score of tests taken at intermediate level mastery';


--
-- TOC entry 2480 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.averageintermediatepracticescore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.averageintermediatepracticescore IS 'average score of practices taken at intermediate level mastery';


--
-- TOC entry 2481 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.averagemastertestscore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.averagemastertestscore IS 'average score of tests taken at master level mastery';


--
-- TOC entry 2482 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.averagemasterpracticescore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.averagemasterpracticescore IS 'average score of practices taken at master level mastery';


--
-- TOC entry 2483 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.timestestednovice; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.timestestednovice IS 'number of times tested on novice level mastery';


--
-- TOC entry 2484 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.timespracticednovice; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.timespracticednovice IS 'number of times practiced on novice level mastery';


--
-- TOC entry 2485 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.timestestedintermediate; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.timestestedintermediate IS 'number of times tested on intermediate level mastery';


--
-- TOC entry 2486 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.timespracticedintermediate; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.timespracticedintermediate IS 'number of times practiced on intermediate level mastery';


--
-- TOC entry 2487 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.timestestedmaster; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.timestestedmaster IS 'number of times tested on master level mastery';


--
-- TOC entry 2488 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.timespracticedmaster; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.timespracticedmaster IS 'number of times practiced on master level mastery';


--
-- TOC entry 2489 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.timelastpracticed; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.timelastpracticed IS 'datetime last practiced';


--
-- TOC entry 2490 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.timelasttested; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.timelasttested IS 'datetime last tested';


--
-- TOC entry 2491 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.lasttestresultid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.lasttestresultid IS 'resultid of the last test';


--
-- TOC entry 2492 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.lastpracticeresultid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.lastpracticeresultid IS 'resultid of the last practice';


--
-- TOC entry 2493 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.lastnovicetestresultid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.lastnovicetestresultid IS 'resultid of the result from the last test run on novice level mastery';


--
-- TOC entry 2494 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.lastnovicepracticeresultid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.lastnovicepracticeresultid IS 'resultid of the result from the last practice run on novice level mastery';


--
-- TOC entry 2495 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.lastintermediatetestresultid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.lastintermediatetestresultid IS 'resultid of the result from the last test run on intermediate level mastery';


--
-- TOC entry 2496 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.lastintermediatepracticeresultid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.lastintermediatepracticeresultid IS 'resultid of the result from the last practice run at intermediate level mastery';


--
-- TOC entry 2497 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.lastmastertestresultid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.lastmastertestresultid IS 'resultid of the result from the last run test on master level mastery';


--
-- TOC entry 2498 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.lastmasterpracticeresultid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.lastmasterpracticeresultid IS 'resultid of the result from the last practice run at master level mastery';


--
-- TOC entry 2499 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.lessontype; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.lessontype IS 'lesson type index within the LessonType enum';


--
-- TOC entry 2500 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.novicepracticesinsuccession; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.novicepracticesinsuccession IS 'number of novice practices successfully completed in a row';


--
-- TOC entry 2501 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.intermediatetestsinsuccession; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.intermediatetestsinsuccession IS 'number of intermediate tests successfully completed in a row';


--
-- TOC entry 2502 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.intermediatepracticesinsuccession; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.intermediatepracticesinsuccession IS 'number of intermediate practices completed successfully in a row';


--
-- TOC entry 2503 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.mastertestsinsuccession; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.mastertestsinsuccession IS 'number of master tests successfully completed in a row';


--
-- TOC entry 2504 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.masterpracticesinsuccession; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.masterpracticesinsuccession IS 'number of master practices successfully completed in a row';


--
-- TOC entry 2505 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN lesson_statsheets.novicetestsinsuccession; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN lesson_statsheets.novicetestsinsuccession IS 'number of novice tests successfully completed in a row';


--
-- TOC entry 187 (class 1259 OID 41082)
-- Name: lesson_statsheets_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE lesson_statsheets_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lesson_statsheets_uid_seq OWNER TO aims;

--
-- TOC entry 2506 (class 0 OID 0)
-- Dependencies: 187
-- Name: lesson_statsheets_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE lesson_statsheets_uid_seq OWNED BY lesson_statsheets.uid;


--
-- TOC entry 188 (class 1259 OID 41084)
-- Name: messages; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE messages (
    uid integer NOT NULL,
    courseid integer,
    toid integer,
    fromid integer,
    subject text,
    body text,
    datesent timestamp without time zone,
    hasbeenread boolean DEFAULT false
);


ALTER TABLE public.messages OWNER TO aims;

--
-- TOC entry 2507 (class 0 OID 0)
-- Dependencies: 188
-- Name: TABLE messages; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE messages IS 'Table to store all doctor to student messages';


--
-- TOC entry 2508 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN messages.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN messages.uid IS 'unique message id#';


--
-- TOC entry 2509 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN messages.courseid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN messages.courseid IS 'id# of linked course';


--
-- TOC entry 2510 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN messages.toid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN messages.toid IS 'id# of linked student the message is being sent to';


--
-- TOC entry 2511 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN messages.fromid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN messages.fromid IS 'id# of the linked doctor the message is being sent from';


--
-- TOC entry 2512 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN messages.subject; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN messages.subject IS 'subject title of the message';


--
-- TOC entry 2513 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN messages.body; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN messages.body IS 'body text of the message';


--
-- TOC entry 2514 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN messages.datesent; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN messages.datesent IS 'date and time the message was sent';


--
-- TOC entry 2515 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN messages.hasbeenread; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN messages.hasbeenread IS 'whether the message has been read';


--
-- TOC entry 189 (class 1259 OID 41091)
-- Name: messages_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE messages_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.messages_uid_seq OWNER TO aims;

--
-- TOC entry 2516 (class 0 OID 0)
-- Dependencies: 189
-- Name: messages_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE messages_uid_seq OWNED BY messages.uid;


--
-- TOC entry 190 (class 1259 OID 41093)
-- Name: organizations; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE organizations (
    uid integer NOT NULL,
    name text,
    packageid integer
);


ALTER TABLE public.organizations OWNER TO aims;

--
-- TOC entry 2517 (class 0 OID 0)
-- Dependencies: 190
-- Name: TABLE organizations; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE organizations IS 'Table of all organizations.';


--
-- TOC entry 2518 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN organizations.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN organizations.uid IS 'organization ID#';


--
-- TOC entry 2519 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN organizations.name; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN organizations.name IS 'organization name';


--
-- TOC entry 191 (class 1259 OID 41099)
-- Name: organizations_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE organizations_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organizations_uid_seq OWNER TO aims;

--
-- TOC entry 2520 (class 0 OID 0)
-- Dependencies: 191
-- Name: organizations_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE organizations_uid_seq OWNED BY organizations.uid;


--
-- TOC entry 192 (class 1259 OID 41101)
-- Name: packages; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE packages (
    uid integer NOT NULL,
    name text,
    description text,
    availabletasks integer
);


ALTER TABLE public.packages OWNER TO aims;

--
-- TOC entry 2521 (class 0 OID 0)
-- Dependencies: 192
-- Name: TABLE packages; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE packages IS 'Table of all packages';


--
-- TOC entry 2522 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN packages.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN packages.uid IS 'unique package id#';


--
-- TOC entry 2523 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN packages.name; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN packages.name IS 'package name';


--
-- TOC entry 2524 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN packages.description; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN packages.description IS 'package description';


--
-- TOC entry 2525 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN packages.availabletasks; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN packages.availabletasks IS 'enum value representing the tasks available in this package';


--
-- TOC entry 193 (class 1259 OID 41107)
-- Name: packages_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE packages_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.packages_uid_seq OWNER TO aims;

--
-- TOC entry 2526 (class 0 OID 0)
-- Dependencies: 193
-- Name: packages_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE packages_uid_seq OWNED BY packages.uid;


--
-- TOC entry 194 (class 1259 OID 41109)
-- Name: rosters; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE rosters (
    uid integer NOT NULL,
    courseid integer,
    studentlist integer[]
);


ALTER TABLE public.rosters OWNER TO aims;

--
-- TOC entry 2527 (class 0 OID 0)
-- Dependencies: 194
-- Name: TABLE rosters; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE rosters IS 'table of course rosters';


--
-- TOC entry 2528 (class 0 OID 0)
-- Dependencies: 194
-- Name: COLUMN rosters.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN rosters.uid IS 'unique id number';


--
-- TOC entry 2529 (class 0 OID 0)
-- Dependencies: 194
-- Name: COLUMN rosters.studentlist; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN rosters.studentlist IS 'List of studentIDs ';


--
-- TOC entry 195 (class 1259 OID 41115)
-- Name: rosters_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE rosters_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rosters_uid_seq OWNER TO aims;

--
-- TOC entry 2530 (class 0 OID 0)
-- Dependencies: 195
-- Name: rosters_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE rosters_uid_seq OWNED BY rosters.uid;


--
-- TOC entry 196 (class 1259 OID 41117)
-- Name: student_results; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE student_results (
    uid integer NOT NULL,
    studentid integer,
    tasktype integer,
    passed boolean,
    totalscore double precision,
    date timestamp without time zone,
    assignmentid integer,
    taskmode integer,
    masterylevel integer,
    totaltime interval
);


ALTER TABLE public.student_results OWNER TO aims;

--
-- TOC entry 2531 (class 0 OID 0)
-- Dependencies: 196
-- Name: TABLE student_results; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE student_results IS 'Table to store all student results.';


--
-- TOC entry 2532 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN student_results.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results.uid IS 'id# of result';


--
-- TOC entry 2533 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN student_results.studentid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results.studentid IS 'linked student';


--
-- TOC entry 2534 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN student_results.tasktype; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results.tasktype IS 'int value of task in the Task enum';


--
-- TOC entry 2535 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN student_results.passed; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results.passed IS 'passed or not (failed)';


--
-- TOC entry 2536 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN student_results.totalscore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results.totalscore IS 'total score of the result';


--
-- TOC entry 2537 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN student_results.date; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results.date IS 'datetime result sumbitted';


--
-- TOC entry 2538 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN student_results.assignmentid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results.assignmentid IS 'Linked assignment uid number';


--
-- TOC entry 2539 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN student_results.taskmode; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results.taskmode IS 'int value of the taskmode in the TaskMode enum';


--
-- TOC entry 2540 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN student_results.masterylevel; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results.masterylevel IS 'int value of the masterylevel in the MasteryLevel enum';


--
-- TOC entry 2541 (class 0 OID 0)
-- Dependencies: 196
-- Name: COLUMN student_results.totaltime; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results.totaltime IS 'total time of the result';


--
-- TOC entry 197 (class 1259 OID 41120)
-- Name: student_results_3dscreenshots; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE student_results_3dscreenshots (
    uid integer NOT NULL,
    stagescoreid integer,
    coloranddepthdata bytea
);


ALTER TABLE public.student_results_3dscreenshots OWNER TO aims;

--
-- TOC entry 2542 (class 0 OID 0)
-- Dependencies: 197
-- Name: TABLE student_results_3dscreenshots; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE student_results_3dscreenshots IS 'Table to store all 3D screenshot color and depth data (serialized byte arrays) for the 3D viewer object.';


--
-- TOC entry 2543 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN student_results_3dscreenshots.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_3dscreenshots.uid IS 'Unique serial id#';


--
-- TOC entry 2544 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN student_results_3dscreenshots.stagescoreid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_3dscreenshots.stagescoreid IS 'linked stage score id #';


--
-- TOC entry 2545 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN student_results_3dscreenshots.coloranddepthdata; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_3dscreenshots.coloranddepthdata IS 'Color and Depth Data (serialized) and stored in a byte array.
Width and height is always 600x480.';


--
-- TOC entry 198 (class 1259 OID 41126)
-- Name: student_results_3dscreenshots_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE student_results_3dscreenshots_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.student_results_3dscreenshots_uid_seq OWNER TO aims;

--
-- TOC entry 2546 (class 0 OID 0)
-- Dependencies: 198
-- Name: student_results_3dscreenshots_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE student_results_3dscreenshots_uid_seq OWNED BY student_results_3dscreenshots.uid;


--
-- TOC entry 199 (class 1259 OID 41128)
-- Name: student_results_lateraltransfer; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE student_results_lateraltransfer (
    uid integer NOT NULL,
    studentid integer NOT NULL,
    masterylevel smallint NOT NULL,
    taskmode smallint NOT NULL,
    score double precision NOT NULL,
    lessonstarttime timestamp without time zone NOT NULL,
    lessonendtime timestamp without time zone NOT NULL,
    handplacementscore double precision NOT NULL,
    verbalcount boolean NOT NULL,
    verbalcounttime timestamp without time zone NOT NULL,
    pullstarttime timestamp without time zone NOT NULL,
    pullendtime timestamp without time zone NOT NULL,
    totalsync double precision NOT NULL,
    rhandsync double precision NOT NULL,
    lhandsync double precision NOT NULL,
    rwristsync double precision NOT NULL,
    lwristsync double precision NOT NULL,
    relbowsync double precision NOT NULL,
    lelbowsync double precision NOT NULL,
    rshouldersync double precision NOT NULL,
    lshouldersync double precision NOT NULL,
    manikinsync double precision NOT NULL,
    calibrationimageid integer NOT NULL,
    handplacementimageid integer NOT NULL,
    pullendimageid integer NOT NULL,
    pullstartimageid integer NOT NULL,
    overallfeedbackids integer[],
    pullfeedbackids integer[],
    handplacementfeedbackids integer[],
    verbalcountscore double precision
);


ALTER TABLE public.student_results_lateraltransfer OWNER TO aims;

--
-- TOC entry 2547 (class 0 OID 0)
-- Dependencies: 199
-- Name: TABLE student_results_lateraltransfer; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE student_results_lateraltransfer IS 'Top-Level Lateral Transfer Results Table. Stores detailed and unique stage results in a column-oriented design.';


--
-- TOC entry 2548 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.uid IS 'Unique Primary Key ID for student_results_lateraltransfer table.';


--
-- TOC entry 2549 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.studentid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.studentid IS 'FK to student table uid.';


--
-- TOC entry 2550 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.masterylevel; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.masterylevel IS 'MasteryLevel enum (Novice, Intermediate, Master) represented as an integer value.';


--
-- TOC entry 2551 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.taskmode; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.taskmode IS 'TaskMode enum (Practice, Test) represented as an integer value.';


--
-- TOC entry 2552 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.score; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.score IS 'Total computed single number score for lateral transfer stage.';


--
-- TOC entry 2553 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.lessonstarttime; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.lessonstarttime IS 'Lesson Start Date and Time.';


--
-- TOC entry 2554 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.lessonendtime; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.lessonendtime IS 'Lesson End Date and Time.';


--
-- TOC entry 2555 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.handplacementscore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.handplacementscore IS 'Total computed single number score for lateral transfer hand placement stage.';


--
-- TOC entry 2556 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.verbalcount; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.verbalcount IS 'Boolean indication for lateral transfer vocal pull stage.';


--
-- TOC entry 2557 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.verbalcounttime; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.verbalcounttime IS 'Lateral Transfer vocal pull stage Date and Time.';


--
-- TOC entry 2558 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.pullstarttime; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.pullstarttime IS 'Lateral Transfer pull detected start Date and Time.';


--
-- TOC entry 2559 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.pullendtime; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.pullendtime IS 'Lateral Transfer pull ended Date and Time.';


--
-- TOC entry 2560 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.totalsync; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.totalsync IS 'Lateral Transfer Total Sync value.';


--
-- TOC entry 2561 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.rhandsync; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.rhandsync IS 'Lateral Transfer Right Hand Sync value.';


--
-- TOC entry 2562 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.lhandsync; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.lhandsync IS 'Lateral Transfer Left Hand Sync value.';


--
-- TOC entry 2563 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.rwristsync; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.rwristsync IS 'Lateral Transfer Right Wrist Sync value.';


--
-- TOC entry 2564 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.lwristsync; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.lwristsync IS 'Lateral Transfer Left Wrist Sync value.';


--
-- TOC entry 2565 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.relbowsync; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.relbowsync IS 'Lateral Transfer Right Elbow Sync value.';


--
-- TOC entry 2566 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.lelbowsync; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.lelbowsync IS 'Lateral Transfer Left Elbow Sync value.';


--
-- TOC entry 2567 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.rshouldersync; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.rshouldersync IS 'Lateral Transfer Right Shoulder Sync value.';


--
-- TOC entry 2568 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.lshouldersync; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.lshouldersync IS 'Lateral Transfer Left Shoulder Sync value.';


--
-- TOC entry 2569 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.manikinsync; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.manikinsync IS 'Lateral Transfer Manikin Torso-to-Leg Sync value.';


--
-- TOC entry 2570 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.calibrationimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.calibrationimageid IS 'FK to kinect_images table uid.';


--
-- TOC entry 2571 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.handplacementimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.handplacementimageid IS 'FK to kinect_images table uid.';


--
-- TOC entry 2572 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.pullendimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.pullendimageid IS 'FK to kinect_images table uid.';


--
-- TOC entry 2573 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.pullstartimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.pullstartimageid IS 'FK to kinect_images table uid.';


--
-- TOC entry 2574 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.overallfeedbackids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.overallfeedbackids IS 'List of Feedback IDs. Relates to the feedback strings relevant to the results. Feedback strings are stored in the lateral transfer lookup table.';


--
-- TOC entry 2575 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.pullfeedbackids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.pullfeedbackids IS 'List of Feedback IDs. Relates to the feedback strings relevant to the results. Feedback strings are stored in the lateral transfer lookup table.';


--
-- TOC entry 2576 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN student_results_lateraltransfer.handplacementfeedbackids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransfer.handplacementfeedbackids IS 'List of Feedback IDs. Relates to the feedback strings relevant to the results. Feedback strings are stored in the lateral transfer lookup table.';


--
-- TOC entry 200 (class 1259 OID 41134)
-- Name: student_results_lateraltransfer_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE student_results_lateraltransfer_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.student_results_lateraltransfer_uid_seq OWNER TO aims;

--
-- TOC entry 2577 (class 0 OID 0)
-- Dependencies: 200
-- Name: student_results_lateraltransfer_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE student_results_lateraltransfer_uid_seq OWNED BY student_results_lateraltransfer.uid;


--
-- TOC entry 201 (class 1259 OID 41136)
-- Name: student_results_lateraltransferlookup; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE student_results_lateraltransferlookup (
    uid integer NOT NULL,
    lateraltransferid integer NOT NULL,
    key text NOT NULL,
    value text NOT NULL
);


ALTER TABLE public.student_results_lateraltransferlookup OWNER TO aims;

--
-- TOC entry 2578 (class 0 OID 0)
-- Dependencies: 201
-- Name: TABLE student_results_lateraltransferlookup; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE student_results_lateraltransferlookup IS 'Key-Value Table for storing lookup data per module in a row-oriented configuration.  Table is related to the student_results_lateraltransfer table.';


--
-- TOC entry 2579 (class 0 OID 0)
-- Dependencies: 201
-- Name: COLUMN student_results_lateraltransferlookup.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransferlookup.uid IS 'Unique Primary Key ID for student_results_lateraltransferlookup table.';


--
-- TOC entry 2580 (class 0 OID 0)
-- Dependencies: 201
-- Name: COLUMN student_results_lateraltransferlookup.lateraltransferid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransferlookup.lateraltransferid IS 'FK to student_results_lateraltransfer table uid.';


--
-- TOC entry 2581 (class 0 OID 0)
-- Dependencies: 201
-- Name: COLUMN student_results_lateraltransferlookup.key; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransferlookup.key IS 'Key which labels the parameter being saved.';


--
-- TOC entry 2582 (class 0 OID 0)
-- Dependencies: 201
-- Name: COLUMN student_results_lateraltransferlookup.value; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_lateraltransferlookup.value IS 'Value which stores the actual parameter.';


--
-- TOC entry 202 (class 1259 OID 41142)
-- Name: student_results_lateraltransferlookup_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE student_results_lateraltransferlookup_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.student_results_lateraltransferlookup_uid_seq OWNER TO aims;

--
-- TOC entry 2583 (class 0 OID 0)
-- Dependencies: 202
-- Name: student_results_lateraltransferlookup_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE student_results_lateraltransferlookup_uid_seq OWNED BY student_results_lateraltransferlookup.uid;


--
-- TOC entry 203 (class 1259 OID 41144)
-- Name: student_results_stage_scores; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE student_results_stage_scores (
    uid integer NOT NULL,
    resultid integer,
    stagescore bytea
);


ALTER TABLE public.student_results_stage_scores OWNER TO aims;

--
-- TOC entry 2584 (class 0 OID 0)
-- Dependencies: 203
-- Name: TABLE student_results_stage_scores; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE student_results_stage_scores IS 'Stage score entries';


--
-- TOC entry 2585 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN student_results_stage_scores.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_stage_scores.uid IS 'unique ID #';


--
-- TOC entry 2586 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN student_results_stage_scores.stagescore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_results_stage_scores.stagescore IS 'Serialized StageScore object as byte array.';


--
-- TOC entry 204 (class 1259 OID 41150)
-- Name: student_results_stage_scores_UID_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE "student_results_stage_scores_UID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."student_results_stage_scores_UID_seq" OWNER TO aims;

--
-- TOC entry 2587 (class 0 OID 0)
-- Dependencies: 204
-- Name: student_results_stage_scores_UID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE "student_results_stage_scores_UID_seq" OWNED BY student_results_stage_scores.uid;


--
-- TOC entry 205 (class 1259 OID 41152)
-- Name: student_results_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE student_results_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.student_results_uid_seq OWNER TO aims;

--
-- TOC entry 2588 (class 0 OID 0)
-- Dependencies: 205
-- Name: student_results_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE student_results_uid_seq OWNED BY student_results.uid;


--
-- TOC entry 206 (class 1259 OID 41154)
-- Name: student_settings; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE student_settings (
    id integer NOT NULL,
    studentid integer NOT NULL,
    setting1 text DEFAULT 'setting 1'::text NOT NULL,
    setting2 text DEFAULT 'setting 2'::text NOT NULL,
    setting3 text DEFAULT 'setting 3'::text NOT NULL
);


ALTER TABLE public.student_settings OWNER TO aims;

--
-- TOC entry 2589 (class 0 OID 0)
-- Dependencies: 206
-- Name: TABLE student_settings; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE student_settings IS 'Table for storing settings unique for a student account.';


--
-- TOC entry 2590 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN student_settings.id; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_settings.id IS 'Unique Primary Key ID for student_settings table.';


--
-- TOC entry 2591 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN student_settings.studentid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_settings.studentid IS 'FK to students table uid.';


--
-- TOC entry 2592 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN student_settings.setting1; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_settings.setting1 IS 'Placeholder for setting 1.';


--
-- TOC entry 2593 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN student_settings.setting2; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_settings.setting2 IS 'Placeholder for setting 2.';


--
-- TOC entry 2594 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN student_settings.setting3; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN student_settings.setting3 IS 'Placeholder for setting 3.';


--
-- TOC entry 207 (class 1259 OID 41163)
-- Name: student_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE student_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.student_settings_id_seq OWNER TO aims;

--
-- TOC entry 2595 (class 0 OID 0)
-- Dependencies: 207
-- Name: student_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE student_settings_id_seq OWNED BY student_settings.id;


--
-- TOC entry 208 (class 1259 OID 41165)
-- Name: students; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE students (
    uid integer NOT NULL,
    accountid integer,
    organizationid integer,
    firstname text DEFAULT ''::text NOT NULL,
    lastname text DEFAULT ''::text NOT NULL,
    enrolledcourseids integer[] DEFAULT '{}'::integer[] NOT NULL
);


ALTER TABLE public.students OWNER TO aims;

--
-- TOC entry 2596 (class 0 OID 0)
-- Dependencies: 208
-- Name: TABLE students; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE students IS 'Table of all students';


--
-- TOC entry 2597 (class 0 OID 0)
-- Dependencies: 208
-- Name: COLUMN students.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN students.uid IS 'unique student id#';


--
-- TOC entry 2598 (class 0 OID 0)
-- Dependencies: 208
-- Name: COLUMN students.accountid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN students.accountid IS 'linked account id#';


--
-- TOC entry 2599 (class 0 OID 0)
-- Dependencies: 208
-- Name: COLUMN students.organizationid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN students.organizationid IS 'linked organization id#';


--
-- TOC entry 2600 (class 0 OID 0)
-- Dependencies: 208
-- Name: COLUMN students.firstname; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN students.firstname IS 'student''s first name';


--
-- TOC entry 2601 (class 0 OID 0)
-- Dependencies: 208
-- Name: COLUMN students.lastname; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN students.lastname IS 'student''s last name';


--
-- TOC entry 2602 (class 0 OID 0)
-- Dependencies: 208
-- Name: COLUMN students.enrolledcourseids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN students.enrolledcourseids IS 'Array of course ids the student has been enrolled in.';


--
-- TOC entry 209 (class 1259 OID 41174)
-- Name: students_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE students_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.students_uid_seq OWNER TO aims;

--
-- TOC entry 2603 (class 0 OID 0)
-- Dependencies: 209
-- Name: students_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE students_uid_seq OWNED BY students.uid;


--
-- TOC entry 210 (class 1259 OID 41176)
-- Name: taskresults_intubation; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE taskresults_intubation (
    uid integer NOT NULL,
    resultid integer NOT NULL,
    calibrationimageid integer,
    tiltheadscore double precision,
    tiltheadimageid integer,
    tiltheadfeedbackids integer[] DEFAULT '{}'::integer[] NOT NULL,
    grablaryngoscopescore double precision,
    grablaryngoscopeimageid integer,
    grablaryngoscopefeedbackids integer[] DEFAULT '{}'::integer[] NOT NULL,
    insertlaryngoscopescore double precision,
    insertlaryngoscopeimageid integer,
    insertlaryngoscopefeedbackids integer[] DEFAULT '{}'::integer[] NOT NULL,
    viewvocalchordsscore double precision,
    viewvocalchordsimageid integer,
    viewvocalchordsfeedbackids integer[] DEFAULT '{}'::integer[] NOT NULL,
    grabendotrachealtubescore double precision,
    grabendotrachealtubeimageid integer,
    grabendotrachealtubefeedbackids integer[] DEFAULT '{}'::integer[] NOT NULL,
    insertendotrachealtubescore double precision,
    insertendotrachealtubeimageid integer,
    insertendotrachealtubefeedbackids integer[] DEFAULT '{}'::integer[] NOT NULL,
    removelaryngoscopescore double precision,
    removelaryngoscopeimageid integer,
    removelaryngoscopefeedbackids integer[] DEFAULT '{}'::integer[] NOT NULL,
    usesyringescore double precision,
    usesyringeimageid integer,
    usesyringefeedbackids integer[] DEFAULT '{}'::integer[] NOT NULL,
    removestyletscore double precision,
    removestyletimageid integer,
    removestyletfeedbackids integer[] DEFAULT '{}'::integer[] NOT NULL,
    baggingscore double precision,
    baggingimageid integer,
    baggingfeedbackids integer[] DEFAULT '{}'::integer[] NOT NULL,
    chestrisescore double precision,
    chestriseimageid integer,
    chestrisefeedbackids integer[] DEFAULT '{}'::integer[] NOT NULL,
    overallfeedbackids integer[] DEFAULT '{}'::integer[] NOT NULL
);


ALTER TABLE public.taskresults_intubation OWNER TO aims;

--
-- TOC entry 2604 (class 0 OID 0)
-- Dependencies: 210
-- Name: TABLE taskresults_intubation; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE taskresults_intubation IS 'Intubation Results Table. Stores detailed and unique stage results in a column-oriented design.';


--
-- TOC entry 2605 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.uid IS 'unique identifier';


--
-- TOC entry 2606 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.resultid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.resultid IS 'linked result id';


--
-- TOC entry 2607 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.calibrationimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.calibrationimageid IS 'pointer to the calibration image, if one exists.';


--
-- TOC entry 2608 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.tiltheadscore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.tiltheadscore IS 'score for tilting the head';


--
-- TOC entry 2609 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.tiltheadimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.tiltheadimageid IS 'pointer to the tilt head image, if one exists.';


--
-- TOC entry 2610 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.tiltheadfeedbackids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.tiltheadfeedbackids IS 'array of pointers to feedback entries for tilting the head.';


--
-- TOC entry 2611 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.grablaryngoscopescore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.grablaryngoscopescore IS 'score for grabbing the laryngoscope.';


--
-- TOC entry 2612 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.grablaryngoscopeimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.grablaryngoscopeimageid IS 'pointer to the grab laryngoscope image, if one exists.';


--
-- TOC entry 2613 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.grablaryngoscopefeedbackids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.grablaryngoscopefeedbackids IS 'array of pointers to feedback entries for grabbing the laryngoscope.';


--
-- TOC entry 2614 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.insertlaryngoscopescore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.insertlaryngoscopescore IS 'score for inserting the laryngoscope.';


--
-- TOC entry 2615 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.insertlaryngoscopeimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.insertlaryngoscopeimageid IS 'pointer to the insert laryngoscope image, if one exists.';


--
-- TOC entry 2616 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.insertlaryngoscopefeedbackids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.insertlaryngoscopefeedbackids IS 'array of pointers to feedback entries for inserting the laryngoscope.';


--
-- TOC entry 2617 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.viewvocalchordsscore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.viewvocalchordsscore IS 'score for viewing the vocal chords.';


--
-- TOC entry 2618 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.viewvocalchordsimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.viewvocalchordsimageid IS 'pointer to the view vocal chords image, if one exists.';


--
-- TOC entry 2619 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.viewvocalchordsfeedbackids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.viewvocalchordsfeedbackids IS 'array of pointers to feedback entries for viewing the vocal chords.';


--
-- TOC entry 2620 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.grabendotrachealtubescore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.grabendotrachealtubescore IS 'score for grabbing the endotracheal tube.';


--
-- TOC entry 2621 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.grabendotrachealtubeimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.grabendotrachealtubeimageid IS 'pointer to the grab endotracheal tube image, if one exists.';


--
-- TOC entry 2622 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.grabendotrachealtubefeedbackids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.grabendotrachealtubefeedbackids IS 'array of pointers to feedback entries for grabbing the endotracheal tube.';


--
-- TOC entry 2623 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.insertendotrachealtubescore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.insertendotrachealtubescore IS 'score for inserting the endotracheal tube.';


--
-- TOC entry 2624 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.insertendotrachealtubeimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.insertendotrachealtubeimageid IS 'pointer to the insert endotracheal tube image, if one exists.';


--
-- TOC entry 2625 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.insertendotrachealtubefeedbackids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.insertendotrachealtubefeedbackids IS 'array of pointers to feedback entries for inserting the endotracheal tube.';


--
-- TOC entry 2626 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.removelaryngoscopescore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.removelaryngoscopescore IS 'score for removing the laryngoscope.';


--
-- TOC entry 2627 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.removelaryngoscopeimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.removelaryngoscopeimageid IS 'pointer to the remove laryngoscope image, if one exists.';


--
-- TOC entry 2628 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.removelaryngoscopefeedbackids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.removelaryngoscopefeedbackids IS 'array of pointers for feedback entries about removing the laryngoscope.';


--
-- TOC entry 2629 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.usesyringescore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.usesyringescore IS 'score for using the syringe.';


--
-- TOC entry 2630 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.usesyringeimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.usesyringeimageid IS 'pointer to the using syringe image, if one exists.';


--
-- TOC entry 2631 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.usesyringefeedbackids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.usesyringefeedbackids IS 'array of pointers to feedback for using the syringe.';


--
-- TOC entry 2632 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.removestyletscore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.removestyletscore IS 'score for removing the stylet.';


--
-- TOC entry 2633 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.removestyletimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.removestyletimageid IS 'pointer to the remove stylet image, if one exists.';


--
-- TOC entry 2634 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.removestyletfeedbackids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.removestyletfeedbackids IS 'array of pointers to feedback entries for removing the stylet.';


--
-- TOC entry 2635 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.baggingscore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.baggingscore IS 'score for bagging the endotracheal tube.';


--
-- TOC entry 2636 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.baggingimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.baggingimageid IS 'pointer to the bagging image, if one exists.';


--
-- TOC entry 2637 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.baggingfeedbackids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.baggingfeedbackids IS 'array of pointers to feedback entries for bagging the endotracheal tube.';


--
-- TOC entry 2638 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.chestrisescore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.chestrisescore IS 'score for chest rise and fall.';


--
-- TOC entry 2639 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.chestriseimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.chestriseimageid IS 'pointer to the chest rise and fall image, if one exists.';


--
-- TOC entry 2640 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.chestrisefeedbackids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.chestrisefeedbackids IS 'array of pointers to feedback entries for chest rise and fall.';


--
-- TOC entry 2641 (class 0 OID 0)
-- Dependencies: 210
-- Name: COLUMN taskresults_intubation.overallfeedbackids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_intubation.overallfeedbackids IS 'array of pointers to feedback entries for the overall performance of the task';


--
-- TOC entry 211 (class 1259 OID 41193)
-- Name: taskresult_intubation_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE taskresult_intubation_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taskresult_intubation_uid_seq OWNER TO aims;

--
-- TOC entry 2642 (class 0 OID 0)
-- Dependencies: 211
-- Name: taskresult_intubation_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE taskresult_intubation_uid_seq OWNED BY taskresults_intubation.uid;


--
-- TOC entry 212 (class 1259 OID 41195)
-- Name: taskresults; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE taskresults (
    uid integer NOT NULL,
    studentid integer NOT NULL,
    assignmentid integer,
    tasktype "TaskType" DEFAULT 'Intubation'::"TaskType" NOT NULL,
    masterylevel "MasteryLevel" DEFAULT 'Novice'::"MasteryLevel" NOT NULL,
    taskmode "TaskMode" DEFAULT 'Practice'::"TaskMode" NOT NULL,
    score double precision DEFAULT 0 NOT NULL,
    submissiontime timestamp without time zone DEFAULT now() NOT NULL,
    starttime timestamp without time zone DEFAULT now() NOT NULL,
    endtime timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.taskresults OWNER TO aims;

--
-- TOC entry 2643 (class 0 OID 0)
-- Dependencies: 212
-- Name: TABLE taskresults; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE taskresults IS 'Top-Level Task Results Table. Stores common Lesson results in a column-oriented design.';


--
-- TOC entry 2644 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN taskresults.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults.uid IS 'Unique Primary Key ID for taskresults table.';


--
-- TOC entry 2645 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN taskresults.studentid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults.studentid IS 'FK to students table uid.';


--
-- TOC entry 2646 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN taskresults.assignmentid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults.assignmentid IS 'Linked assignment uid number';


--
-- TOC entry 2647 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN taskresults.tasktype; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults.tasktype IS 'Task Type enum (CPR, Intubation , LateralTransfer , VerticalLift)';


--
-- TOC entry 2648 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN taskresults.masterylevel; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults.masterylevel IS 'MasteryLevel enum (Novice, Intermediate, Master)';


--
-- TOC entry 2649 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN taskresults.taskmode; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults.taskmode IS 'TaskMode enum (Practice, Test)';


--
-- TOC entry 2650 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN taskresults.score; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults.score IS 'Total computed single number score for the Lesson.';


--
-- TOC entry 2651 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN taskresults.submissiontime; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults.submissiontime IS 'Lesson Results Submission Date and Time.';


--
-- TOC entry 2652 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN taskresults.starttime; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults.starttime IS 'Lesson Start Date and Time.';


--
-- TOC entry 2653 (class 0 OID 0)
-- Dependencies: 212
-- Name: COLUMN taskresults.endtime; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults.endtime IS 'Lesson End Date and Time.';


--
-- TOC entry 213 (class 1259 OID 41205)
-- Name: taskresults_advice; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE taskresults_advice (
    uid integer NOT NULL,
    feedbackid integer,
    tasktype "TaskType" NOT NULL,
    advice text
);


ALTER TABLE public.taskresults_advice OWNER TO aims;

--
-- TOC entry 2654 (class 0 OID 0)
-- Dependencies: 213
-- Name: TABLE taskresults_advice; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE taskresults_advice IS 'advice table linking to specific feedback of a task';


--
-- TOC entry 2655 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN taskresults_advice.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_advice.uid IS 'unique identifier';


--
-- TOC entry 2656 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN taskresults_advice.feedbackid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_advice.feedbackid IS 'links the advice back to the feedbackid';


--
-- TOC entry 2657 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN taskresults_advice.tasktype; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_advice.tasktype IS 'fulfills the 2nd portion of the feedbackid''s primary key pointer.';


--
-- TOC entry 2658 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN taskresults_advice.advice; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_advice.advice IS 'the actual advice entry';


--
-- TOC entry 214 (class 1259 OID 41211)
-- Name: taskresults_advice_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE taskresults_advice_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taskresults_advice_uid_seq OWNER TO aims;

--
-- TOC entry 2659 (class 0 OID 0)
-- Dependencies: 214
-- Name: taskresults_advice_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE taskresults_advice_uid_seq OWNED BY taskresults_advice.uid;


--
-- TOC entry 221 (class 1259 OID 43874)
-- Name: taskresults_cpr; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE taskresults_cpr (
    uid integer NOT NULL,
    resultid integer NOT NULL,
    calibrationimageid integer,
    askedforaed boolean,
    askedfor911 boolean,
    askedifok boolean,
    checkpulseimageid integer,
    checkedpulse "CheckPulse",
    feedbackids integer[]
);


ALTER TABLE public.taskresults_cpr OWNER TO aims;

--
-- TOC entry 2660 (class 0 OID 0)
-- Dependencies: 221
-- Name: TABLE taskresults_cpr; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE taskresults_cpr IS 'CPR Results Table. Stores detailed and unique stage results in a column-oriented design.';


--
-- TOC entry 2661 (class 0 OID 0)
-- Dependencies: 221
-- Name: COLUMN taskresults_cpr.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_cpr.uid IS 'Unique Primary Key ID for taskresults_cpr table.';


--
-- TOC entry 2662 (class 0 OID 0)
-- Dependencies: 221
-- Name: COLUMN taskresults_cpr.resultid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_cpr.resultid IS 'FK to taskresults table uid.';


--
-- TOC entry 2663 (class 0 OID 0)
-- Dependencies: 221
-- Name: COLUMN taskresults_cpr.calibrationimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_cpr.calibrationimageid IS 'FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';


--
-- TOC entry 2664 (class 0 OID 0)
-- Dependencies: 221
-- Name: COLUMN taskresults_cpr.askedforaed; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_cpr.askedforaed IS 'CPR asked for AED value.';


--
-- TOC entry 2665 (class 0 OID 0)
-- Dependencies: 221
-- Name: COLUMN taskresults_cpr.askedfor911; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_cpr.askedfor911 IS 'CPR asked for 911 value.';


--
-- TOC entry 2666 (class 0 OID 0)
-- Dependencies: 221
-- Name: COLUMN taskresults_cpr.askedifok; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_cpr.askedifok IS 'CPR asked if OK value.';


--
-- TOC entry 2667 (class 0 OID 0)
-- Dependencies: 221
-- Name: COLUMN taskresults_cpr.checkpulseimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_cpr.checkpulseimageid IS 'FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';


--
-- TOC entry 2668 (class 0 OID 0)
-- Dependencies: 221
-- Name: COLUMN taskresults_cpr.checkedpulse; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_cpr.checkedpulse IS 'CPR checked pulse value. ENUM (No, Incorrectly, Correctly) ';


--
-- TOC entry 2669 (class 0 OID 0)
-- Dependencies: 221
-- Name: COLUMN taskresults_cpr.feedbackids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_cpr.feedbackids IS 'Array of feedback ids used to link this result to its feedback.';


--
-- TOC entry 222 (class 1259 OID 43900)
-- Name: taskresults_cpr_compressions; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE taskresults_cpr_compressions (
    compressionset integer NOT NULL,
    taskresults_cpr_uid integer NOT NULL,
    avgdepth double precision DEFAULT 0,
    avgrate double precision DEFAULT 0,
    avgarmslocked double precision DEFAULT 0,
    avgshouldersovermanikin double precision DEFAULT 0,
    score double precision DEFAULT 0,
    compressionimageid integer,
    feedbackids integer[]
);


ALTER TABLE public.taskresults_cpr_compressions OWNER TO aims;

--
-- TOC entry 2670 (class 0 OID 0)
-- Dependencies: 222
-- Name: TABLE taskresults_cpr_compressions; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE taskresults_cpr_compressions IS 'CPR Results Compression Sets Table. Stores compression set specific details to a CPR result.';


--
-- TOC entry 2671 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN taskresults_cpr_compressions.compressionset; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_cpr_compressions.compressionset IS 'Part of the taskresults_cpr_compressions PK. Also identifies the ordering of compression sets for a CPR Lesson run.';


--
-- TOC entry 2672 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN taskresults_cpr_compressions.taskresults_cpr_uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_cpr_compressions.taskresults_cpr_uid IS 'FK to taskresults_cpr table. Also part of taskresults_cpr_compressions PK.';


--
-- TOC entry 2673 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN taskresults_cpr_compressions.avgdepth; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_cpr_compressions.avgdepth IS 'Average depth of compressions for a CPR set of compressions.';


--
-- TOC entry 2674 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN taskresults_cpr_compressions.avgrate; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_cpr_compressions.avgrate IS 'Average rate of compressions for a CPR set of compressions.';


--
-- TOC entry 2675 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN taskresults_cpr_compressions.avgarmslocked; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_cpr_compressions.avgarmslocked IS 'Percent average of arms being locked across compressions for a CPR set of compressions.';


--
-- TOC entry 2676 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN taskresults_cpr_compressions.avgshouldersovermanikin; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_cpr_compressions.avgshouldersovermanikin IS 'Percent average of shoulders being over the manikin across compressions for a CPR setof compressions.';


--
-- TOC entry 2677 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN taskresults_cpr_compressions.score; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_cpr_compressions.score IS 'Score for a set of CPR compressions.';


--
-- TOC entry 2678 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN taskresults_cpr_compressions.compressionimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_cpr_compressions.compressionimageid IS 'FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';


--
-- TOC entry 2679 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN taskresults_cpr_compressions.feedbackids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_cpr_compressions.feedbackids IS 'Array of feedback ids used to link this compression set to its feedback.';


--
-- TOC entry 220 (class 1259 OID 43872)
-- Name: taskresults_cpr_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE taskresults_cpr_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taskresults_cpr_uid_seq OWNER TO aims;

--
-- TOC entry 2680 (class 0 OID 0)
-- Dependencies: 220
-- Name: taskresults_cpr_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE taskresults_cpr_uid_seq OWNED BY taskresults_cpr.uid;


--
-- TOC entry 215 (class 1259 OID 41226)
-- Name: taskresults_feedback; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE taskresults_feedback (
    uid integer NOT NULL,
    tasktype "TaskType" NOT NULL,
    feedbacktype "FeedbackType" DEFAULT 'None'::"FeedbackType" NOT NULL,
    feedback text
);


ALTER TABLE public.taskresults_feedback OWNER TO aims;

--
-- TOC entry 2681 (class 0 OID 0)
-- Dependencies: 215
-- Name: COLUMN taskresults_feedback.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_feedback.uid IS 'unique identifier';


--
-- TOC entry 2682 (class 0 OID 0)
-- Dependencies: 215
-- Name: COLUMN taskresults_feedback.feedbacktype; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_feedback.feedbacktype IS 'feedback type of the entry';


--
-- TOC entry 2683 (class 0 OID 0)
-- Dependencies: 215
-- Name: COLUMN taskresults_feedback.feedback; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_feedback.feedback IS 'the actuall feedback entry';


--
-- TOC entry 216 (class 1259 OID 41233)
-- Name: taskresults_feedback_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE taskresults_feedback_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taskresults_feedback_uid_seq OWNER TO aims;

--
-- TOC entry 2684 (class 0 OID 0)
-- Dependencies: 216
-- Name: taskresults_feedback_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE taskresults_feedback_uid_seq OWNED BY taskresults_feedback.uid;


--
-- TOC entry 217 (class 1259 OID 41235)
-- Name: taskresults_lateraltransfer; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE taskresults_lateraltransfer (
    uid integer NOT NULL,
    resultid integer NOT NULL,
    handplacementscore double precision DEFAULT 0 NOT NULL,
    verbalcount boolean NOT NULL,
    verbalcounttime timestamp without time zone DEFAULT now() NOT NULL,
    verbalcountscore double precision DEFAULT 0 NOT NULL,
    pullstarttime timestamp without time zone DEFAULT now() NOT NULL,
    pullendtime timestamp without time zone DEFAULT now() NOT NULL,
    totalsync double precision DEFAULT 0 NOT NULL,
    rhandsync double precision DEFAULT 0 NOT NULL,
    lhandsync double precision DEFAULT 0 NOT NULL,
    rwristsync double precision DEFAULT 0 NOT NULL,
    lwristsync double precision DEFAULT 0 NOT NULL,
    relbowsync double precision DEFAULT 0 NOT NULL,
    lelbowsync double precision DEFAULT 0 NOT NULL,
    rshouldersync double precision DEFAULT 0 NOT NULL,
    lshouldersync double precision DEFAULT 0 NOT NULL,
    manikinsync double precision DEFAULT 0 NOT NULL,
    calibrationimageid integer,
    handplacementimageid integer,
    pullstartimageid integer,
    pullendimageid integer,
    overallfeedbackids integer[],
    handplacementfeedbackids integer[],
    pullfeedbackids integer[]
);


ALTER TABLE public.taskresults_lateraltransfer OWNER TO aims;

--
-- TOC entry 2685 (class 0 OID 0)
-- Dependencies: 217
-- Name: TABLE taskresults_lateraltransfer; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE taskresults_lateraltransfer IS 'Lateral Transfer Results Table. Stores detailed and unique stage results in a column-oriented design.';


--
-- TOC entry 2686 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.uid IS 'Unique Primary Key ID for taskresults_lateraltransfer table.';


--
-- TOC entry 2687 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.resultid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.resultid IS 'FK to taskresults table uid.';


--
-- TOC entry 2688 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.handplacementscore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.handplacementscore IS 'Total computed single number score for lateral transfer hand placement stage.';


--
-- TOC entry 2689 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.verbalcount; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.verbalcount IS 'Boolean indication for lateral transfer vocal pull stage.';


--
-- TOC entry 2690 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.verbalcounttime; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.verbalcounttime IS 'Lateral Transfer vocal pull stage Date and Time.';


--
-- TOC entry 2691 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.verbalcountscore; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.verbalcountscore IS 'Verbal Count score for lateral transfer vocal pull.';


--
-- TOC entry 2692 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.pullstarttime; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.pullstarttime IS 'Lateral Transfer pull detected start Date and Time.';


--
-- TOC entry 2693 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.pullendtime; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.pullendtime IS 'Lateral Transfer pull ended Date and Time.';


--
-- TOC entry 2694 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.totalsync; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.totalsync IS 'Lateral Transfer Total Sync value.';


--
-- TOC entry 2695 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.rhandsync; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.rhandsync IS 'Lateral Transfer Right Hand Sync value.';


--
-- TOC entry 2696 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.lhandsync; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.lhandsync IS 'Lateral Transfer Left Hand Sync value.';


--
-- TOC entry 2697 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.rwristsync; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.rwristsync IS 'Lateral Transfer Right Wrist Sync value.';


--
-- TOC entry 2698 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.lwristsync; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.lwristsync IS 'Lateral Transfer Left Wrist Sync value.';


--
-- TOC entry 2699 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.relbowsync; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.relbowsync IS 'Lateral Transfer Right Elbow Sync value.';


--
-- TOC entry 2700 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.lelbowsync; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.lelbowsync IS 'Lateral Transfer Left Elbow Sync value.';


--
-- TOC entry 2701 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.rshouldersync; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.rshouldersync IS 'Lateral Transfer Right Shoulder Sync value.';


--
-- TOC entry 2702 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.lshouldersync; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.lshouldersync IS 'Lateral Transfer Left Shoulder Sync value.';


--
-- TOC entry 2703 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.manikinsync; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.manikinsync IS 'Lateral Transfer Manikin Torso-to-Leg Sync value.';


--
-- TOC entry 2704 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.calibrationimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.calibrationimageid IS 'FK to kinect_images table uid.  Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';


--
-- TOC entry 2705 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.handplacementimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.handplacementimageid IS 'FK to kinect_images table uid.  Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';


--
-- TOC entry 2706 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.pullstartimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.pullstartimageid IS 'FK to kinect_images table uid.  Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';


--
-- TOC entry 2707 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.pullendimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.pullendimageid IS 'FK to kinect_images table uid.  Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';


--
-- TOC entry 2708 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.overallfeedbackids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.overallfeedbackids IS 'List of Feedback IDs. Relates to the feedback strings relevant to the results. Feedback strings are stored in the lateral transfer lookup table.';


--
-- TOC entry 2709 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.handplacementfeedbackids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.handplacementfeedbackids IS 'List of Feedback IDs. Relates to the feedback strings relevant to the results. Feedback strings are stored in the lateral transfer lookup table.';


--
-- TOC entry 2710 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN taskresults_lateraltransfer.pullfeedbackids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_lateraltransfer.pullfeedbackids IS 'List of Feedback IDs. Relates to the feedback strings relevant to the results. Feedback strings are stored in the lateral transfer lookup table.';


--
-- TOC entry 218 (class 1259 OID 41256)
-- Name: taskresults_lateraltransfer_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE taskresults_lateraltransfer_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taskresults_lateraltransfer_uid_seq OWNER TO aims;

--
-- TOC entry 2711 (class 0 OID 0)
-- Dependencies: 218
-- Name: taskresults_lateraltransfer_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE taskresults_lateraltransfer_uid_seq OWNED BY taskresults_lateraltransfer.uid;


--
-- TOC entry 219 (class 1259 OID 41258)
-- Name: taskresults_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE taskresults_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taskresults_uid_seq OWNER TO aims;

--
-- TOC entry 2712 (class 0 OID 0)
-- Dependencies: 219
-- Name: taskresults_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE taskresults_uid_seq OWNED BY taskresults.uid;


--
-- TOC entry 224 (class 1259 OID 49417)
-- Name: taskresults_verticallift; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE taskresults_verticallift (
    uid integer NOT NULL,
    resultid integer NOT NULL,
    avgbackstraight double precision DEFAULT 0 NOT NULL,
    avgclosetobody double precision DEFAULT 0 NOT NULL,
    avgfootspacing double precision DEFAULT 0 NOT NULL,
    calibrationimageid integer,
    feedbackids integer[]
);


ALTER TABLE public.taskresults_verticallift OWNER TO aims;

--
-- TOC entry 2713 (class 0 OID 0)
-- Dependencies: 224
-- Name: TABLE taskresults_verticallift; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE taskresults_verticallift IS 'Vertical Lift Results Table. Stores detailed and unique stage results in a column-oriented design.';


--
-- TOC entry 2714 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN taskresults_verticallift.uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_verticallift.uid IS 'Unique Primary Key ID for taskresults_verticallift table.';


--
-- TOC entry 2715 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN taskresults_verticallift.resultid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_verticallift.resultid IS 'FK to taskresults table uid.';


--
-- TOC entry 2716 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN taskresults_verticallift.avgbackstraight; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_verticallift.avgbackstraight IS 'Average percentage that the users back was straight during repetitions of vertical lift.';


--
-- TOC entry 2717 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN taskresults_verticallift.avgclosetobody; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_verticallift.avgclosetobody IS 'Average percentage that the object being lifted was close to the user'' body during repetitions of vertical lift';


--
-- TOC entry 2718 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN taskresults_verticallift.avgfootspacing; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_verticallift.avgfootspacing IS 'Average percentage that the users foot stance was a good stance during repetitions of vertical lift';


--
-- TOC entry 2719 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN taskresults_verticallift.calibrationimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_verticallift.calibrationimageid IS 'FK to kinect_images table uid. Uses a {0|1}:N relationship (optional relationship). No relationship is represented by NULL.';


--
-- TOC entry 2720 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN taskresults_verticallift.feedbackids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_verticallift.feedbackids IS 'Array of feedback ids used to link this results to its feedback.';


--
-- TOC entry 225 (class 1259 OID 49438)
-- Name: taskresults_verticallift_reps; Type: TABLE; Schema: public; Owner: aims; Tablespace: 
--

CREATE TABLE taskresults_verticallift_reps (
    taskresults_verticallift_uid integer NOT NULL,
    repid integer NOT NULL,
    backstraight boolean DEFAULT false NOT NULL,
    closetobody boolean DEFAULT false NOT NULL,
    footspacing "FootSpacing",
    score double precision DEFAULT 0 NOT NULL,
    downimageid integer,
    liftobjectimageid integer,
    lowerobjectimageid integer,
    riseimageid integer,
    feedbackids integer[]
);


ALTER TABLE public.taskresults_verticallift_reps OWNER TO aims;

--
-- TOC entry 2721 (class 0 OID 0)
-- Dependencies: 225
-- Name: TABLE taskresults_verticallift_reps; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON TABLE taskresults_verticallift_reps IS 'Vertical Lift Reps Table. Stores values collected across multiple repetitions during a Vertical Lift task.';


--
-- TOC entry 2722 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN taskresults_verticallift_reps.taskresults_verticallift_uid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_verticallift_reps.taskresults_verticallift_uid IS 'FK to taskresults_verticallift and part of the PK';


--
-- TOC entry 2723 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN taskresults_verticallift_reps.repid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_verticallift_reps.repid IS 'Identifies which repetition this is in the set associated with a verticallift task run. Part of the PK.';


--
-- TOC entry 2724 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN taskresults_verticallift_reps.backstraight; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_verticallift_reps.backstraight IS 'Identifies whether or not the back was straight during a vertical lift.';


--
-- TOC entry 2725 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN taskresults_verticallift_reps.closetobody; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_verticallift_reps.closetobody IS 'Identifies whether or not the object being lifted was close to the users body during a vertical lift.';


--
-- TOC entry 2726 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN taskresults_verticallift_reps.footspacing; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_verticallift_reps.footspacing IS 'Identifies the foot stance during a vertical lift.';


--
-- TOC entry 2727 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN taskresults_verticallift_reps.score; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_verticallift_reps.score IS 'The score of this vertical lift.';


--
-- TOC entry 2728 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN taskresults_verticallift_reps.downimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_verticallift_reps.downimageid IS 'The image id of the ''down'' stage.';


--
-- TOC entry 2729 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN taskresults_verticallift_reps.liftobjectimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_verticallift_reps.liftobjectimageid IS 'The image id of the ''liftobject'' stage.';


--
-- TOC entry 2730 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN taskresults_verticallift_reps.lowerobjectimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_verticallift_reps.lowerobjectimageid IS 'The image id of the ''lowerobject'' stage.';


--
-- TOC entry 2731 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN taskresults_verticallift_reps.riseimageid; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_verticallift_reps.riseimageid IS 'The image id during of the ''rise'' stage.';


--
-- TOC entry 2732 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN taskresults_verticallift_reps.feedbackids; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON COLUMN taskresults_verticallift_reps.feedbackids IS 'Array of feedback ids used to link this results to its feedback.';


--
-- TOC entry 223 (class 1259 OID 49415)
-- Name: taskresults_verticallift_uid_seq; Type: SEQUENCE; Schema: public; Owner: aims
--

CREATE SEQUENCE taskresults_verticallift_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taskresults_verticallift_uid_seq OWNER TO aims;

--
-- TOC entry 2733 (class 0 OID 0)
-- Dependencies: 223
-- Name: taskresults_verticallift_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aims
--

ALTER SEQUENCE taskresults_verticallift_uid_seq OWNED BY taskresults_verticallift.uid;


--
-- TOC entry 2132 (class 2604 OID 41268)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY accounts ALTER COLUMN uid SET DEFAULT nextval('accounts_uid_seq'::regclass);


--
-- TOC entry 2136 (class 2604 OID 41269)
-- Name: id; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY administrator_settings ALTER COLUMN id SET DEFAULT nextval('administrator_settings_id_seq'::regclass);


--
-- TOC entry 2137 (class 2604 OID 41270)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY administrators ALTER COLUMN uid SET DEFAULT nextval('administrators_uid_seq'::regclass);


--
-- TOC entry 2138 (class 2604 OID 41271)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY course_assignments ALTER COLUMN uid SET DEFAULT nextval('course_assignments_uid_seq'::regclass);


--
-- TOC entry 2142 (class 2604 OID 41272)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY courses ALTER COLUMN uid SET DEFAULT nextval('courses_uid_seq'::regclass);


--
-- TOC entry 2146 (class 2604 OID 41273)
-- Name: id; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY doctor_settings ALTER COLUMN id SET DEFAULT nextval('doctor_settings_id_seq'::regclass);


--
-- TOC entry 2147 (class 2604 OID 41274)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY doctors ALTER COLUMN uid SET DEFAULT nextval('doctors_uid_seq'::regclass);


--
-- TOC entry 2148 (class 2604 OID 41275)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY intubation_statsheets ALTER COLUMN uid SET DEFAULT nextval('intubation_statsheets_uid_seq'::regclass);


--
-- TOC entry 2149 (class 2604 OID 41276)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY kinect_images ALTER COLUMN uid SET DEFAULT nextval('kinect_images_uid_seq'::regclass);


--
-- TOC entry 2150 (class 2604 OID 41277)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY lesson_statsheets ALTER COLUMN uid SET DEFAULT nextval('lesson_statsheets_uid_seq'::regclass);


--
-- TOC entry 2152 (class 2604 OID 41278)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY messages ALTER COLUMN uid SET DEFAULT nextval('messages_uid_seq'::regclass);


--
-- TOC entry 2153 (class 2604 OID 41279)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY organizations ALTER COLUMN uid SET DEFAULT nextval('organizations_uid_seq'::regclass);


--
-- TOC entry 2154 (class 2604 OID 41280)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY packages ALTER COLUMN uid SET DEFAULT nextval('packages_uid_seq'::regclass);


--
-- TOC entry 2155 (class 2604 OID 41281)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY rosters ALTER COLUMN uid SET DEFAULT nextval('rosters_uid_seq'::regclass);


--
-- TOC entry 2156 (class 2604 OID 41282)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY student_results ALTER COLUMN uid SET DEFAULT nextval('student_results_uid_seq'::regclass);


--
-- TOC entry 2157 (class 2604 OID 41283)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY student_results_3dscreenshots ALTER COLUMN uid SET DEFAULT nextval('student_results_3dscreenshots_uid_seq'::regclass);


--
-- TOC entry 2158 (class 2604 OID 41284)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY student_results_lateraltransfer ALTER COLUMN uid SET DEFAULT nextval('student_results_lateraltransfer_uid_seq'::regclass);


--
-- TOC entry 2159 (class 2604 OID 41285)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY student_results_lateraltransferlookup ALTER COLUMN uid SET DEFAULT nextval('student_results_lateraltransferlookup_uid_seq'::regclass);


--
-- TOC entry 2160 (class 2604 OID 41286)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY student_results_stage_scores ALTER COLUMN uid SET DEFAULT nextval('"student_results_stage_scores_UID_seq"'::regclass);


--
-- TOC entry 2164 (class 2604 OID 41287)
-- Name: id; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY student_settings ALTER COLUMN id SET DEFAULT nextval('student_settings_id_seq'::regclass);


--
-- TOC entry 2168 (class 2604 OID 41288)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY students ALTER COLUMN uid SET DEFAULT nextval('students_uid_seq'::regclass);


--
-- TOC entry 2189 (class 2604 OID 41289)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults ALTER COLUMN uid SET DEFAULT nextval('taskresults_uid_seq'::regclass);


--
-- TOC entry 2190 (class 2604 OID 41290)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_advice ALTER COLUMN uid SET DEFAULT nextval('taskresults_advice_uid_seq'::regclass);


--
-- TOC entry 2209 (class 2604 OID 43877)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_cpr ALTER COLUMN uid SET DEFAULT nextval('taskresults_cpr_uid_seq'::regclass);


--
-- TOC entry 2192 (class 2604 OID 41292)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_feedback ALTER COLUMN uid SET DEFAULT nextval('taskresults_feedback_uid_seq'::regclass);


--
-- TOC entry 2180 (class 2604 OID 41293)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_intubation ALTER COLUMN uid SET DEFAULT nextval('taskresult_intubation_uid_seq'::regclass);


--
-- TOC entry 2208 (class 2604 OID 41294)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_lateraltransfer ALTER COLUMN uid SET DEFAULT nextval('taskresults_lateraltransfer_uid_seq'::regclass);


--
-- TOC entry 2215 (class 2604 OID 49420)
-- Name: uid; Type: DEFAULT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_verticallift ALTER COLUMN uid SET DEFAULT nextval('taskresults_verticallift_uid_seq'::regclass);


--
-- TOC entry 2262 (class 2606 OID 43560)
-- Name: 3dscreenshots_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY student_results_3dscreenshots
    ADD CONSTRAINT "3dscreenshots_pkey" PRIMARY KEY (uid);


--
-- TOC entry 2259 (class 2606 OID 43562)
-- Name: UID_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY student_results
    ADD CONSTRAINT "UID_pkey" PRIMARY KEY (uid);


--
-- TOC entry 2223 (class 2606 OID 43564)
-- Name: accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT accounts_pkey PRIMARY KEY (uid);


--
-- TOC entry 2225 (class 2606 OID 43566)
-- Name: administrator_settings_administratorid_key; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY administrator_settings
    ADD CONSTRAINT administrator_settings_administratorid_key UNIQUE (administratorid);


--
-- TOC entry 2227 (class 2606 OID 43568)
-- Name: administrator_settings_pkey1; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY administrator_settings
    ADD CONSTRAINT administrator_settings_pkey1 PRIMARY KEY (id);


--
-- TOC entry 2229 (class 2606 OID 43570)
-- Name: administrators_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY administrators
    ADD CONSTRAINT administrators_pkey PRIMARY KEY (uid);


--
-- TOC entry 2734 (class 0 OID 0)
-- Dependencies: 2229
-- Name: CONSTRAINT administrators_pkey ON administrators; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON CONSTRAINT administrators_pkey ON administrators IS 'unique administrator id# = Primary Key';


--
-- TOC entry 2233 (class 2606 OID 43572)
-- Name: courses_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY courses
    ADD CONSTRAINT courses_pkey PRIMARY KEY (uid);


--
-- TOC entry 2235 (class 2606 OID 43574)
-- Name: doctor_settings_doctorid_key; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY doctor_settings
    ADD CONSTRAINT doctor_settings_doctorid_key UNIQUE (doctorid);


--
-- TOC entry 2237 (class 2606 OID 43576)
-- Name: doctor_settings_pkey1; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY doctor_settings
    ADD CONSTRAINT doctor_settings_pkey1 PRIMARY KEY (id);


--
-- TOC entry 2239 (class 2606 OID 43578)
-- Name: doctors_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY doctors
    ADD CONSTRAINT doctors_pkey PRIMARY KEY (uid);


--
-- TOC entry 2242 (class 2606 OID 43580)
-- Name: intubation_statsheet_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY intubation_statsheets
    ADD CONSTRAINT intubation_statsheet_pkey PRIMARY KEY (uid);


--
-- TOC entry 2244 (class 2606 OID 43582)
-- Name: kinect_images_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY kinect_images
    ADD CONSTRAINT kinect_images_pkey PRIMARY KEY (uid);


--
-- TOC entry 2246 (class 2606 OID 43584)
-- Name: lesson_statsheets_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY lesson_statsheets
    ADD CONSTRAINT lesson_statsheets_pkey PRIMARY KEY (uid);


--
-- TOC entry 2250 (class 2606 OID 43586)
-- Name: messages_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (uid);


--
-- TOC entry 2253 (class 2606 OID 43588)
-- Name: organizations_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY organizations
    ADD CONSTRAINT organizations_pkey PRIMARY KEY (uid);


--
-- TOC entry 2255 (class 2606 OID 43590)
-- Name: packages_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY packages
    ADD CONSTRAINT packages_pkey PRIMARY KEY (uid);


--
-- TOC entry 2257 (class 2606 OID 43592)
-- Name: roster_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY rosters
    ADD CONSTRAINT roster_pkey PRIMARY KEY (uid);


--
-- TOC entry 2735 (class 0 OID 0)
-- Dependencies: 2257
-- Name: CONSTRAINT roster_pkey ON rosters; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON CONSTRAINT roster_pkey ON rosters IS 'Roster uniquie ID is the primary key.';


--
-- TOC entry 2264 (class 2606 OID 43594)
-- Name: student_results_lateraltransfer_calibrationimageid_key; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY student_results_lateraltransfer
    ADD CONSTRAINT student_results_lateraltransfer_calibrationimageid_key UNIQUE (calibrationimageid);


--
-- TOC entry 2266 (class 2606 OID 43596)
-- Name: student_results_lateraltransfer_handplacementimageid_key; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY student_results_lateraltransfer
    ADD CONSTRAINT student_results_lateraltransfer_handplacementimageid_key UNIQUE (handplacementimageid);


--
-- TOC entry 2268 (class 2606 OID 43598)
-- Name: student_results_lateraltransfer_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY student_results_lateraltransfer
    ADD CONSTRAINT student_results_lateraltransfer_pkey PRIMARY KEY (uid);


--
-- TOC entry 2270 (class 2606 OID 43600)
-- Name: student_results_lateraltransfer_pullendimageid_key; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY student_results_lateraltransfer
    ADD CONSTRAINT student_results_lateraltransfer_pullendimageid_key UNIQUE (pullendimageid);


--
-- TOC entry 2272 (class 2606 OID 43602)
-- Name: student_results_lateraltransfer_pullstartimageid_key; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY student_results_lateraltransfer
    ADD CONSTRAINT student_results_lateraltransfer_pullstartimageid_key UNIQUE (pullstartimageid);


--
-- TOC entry 2274 (class 2606 OID 43604)
-- Name: student_results_lateraltransferlookup_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY student_results_lateraltransferlookup
    ADD CONSTRAINT student_results_lateraltransferlookup_pkey PRIMARY KEY (uid);


--
-- TOC entry 2278 (class 2606 OID 43606)
-- Name: student_settings_pkey1; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY student_settings
    ADD CONSTRAINT student_settings_pkey1 PRIMARY KEY (id);


--
-- TOC entry 2280 (class 2606 OID 43608)
-- Name: student_settings_studentid_key; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY student_settings
    ADD CONSTRAINT student_settings_studentid_key UNIQUE (studentid);


--
-- TOC entry 2283 (class 2606 OID 43610)
-- Name: students_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY students
    ADD CONSTRAINT students_pkey PRIMARY KEY (uid);


--
-- TOC entry 2289 (class 2606 OID 43612)
-- Name: taskadvice_fkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY taskresults_advice
    ADD CONSTRAINT taskadvice_fkey PRIMARY KEY (uid);


--
-- TOC entry 2291 (class 2606 OID 43614)
-- Name: taskfeedback_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY taskresults_feedback
    ADD CONSTRAINT taskfeedback_pkey PRIMARY KEY (uid);


--
-- TOC entry 2285 (class 2606 OID 43616)
-- Name: taskresult_intubation_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY taskresults_intubation
    ADD CONSTRAINT taskresult_intubation_pkey PRIMARY KEY (uid);


--
-- TOC entry 2307 (class 2606 OID 43912)
-- Name: taskresults_cpr_compressions_pk; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY taskresults_cpr_compressions
    ADD CONSTRAINT taskresults_cpr_compressions_pk PRIMARY KEY (compressionset, taskresults_cpr_uid);


--
-- TOC entry 2309 (class 2606 OID 43914)
-- Name: taskresults_cpr_compressions_unique; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY taskresults_cpr_compressions
    ADD CONSTRAINT taskresults_cpr_compressions_unique UNIQUE (compressionimageid);


--
-- TOC entry 2303 (class 2606 OID 43882)
-- Name: taskresults_cpr_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY taskresults_cpr
    ADD CONSTRAINT taskresults_cpr_pkey PRIMARY KEY (uid);


--
-- TOC entry 2305 (class 2606 OID 43884)
-- Name: taskresults_cpr_unique; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY taskresults_cpr
    ADD CONSTRAINT taskresults_cpr_unique UNIQUE (calibrationimageid, checkpulseimageid);


--
-- TOC entry 2293 (class 2606 OID 43626)
-- Name: taskresults_lateraltransfer_calibrationimageid_key; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY taskresults_lateraltransfer
    ADD CONSTRAINT taskresults_lateraltransfer_calibrationimageid_key UNIQUE (calibrationimageid);


--
-- TOC entry 2295 (class 2606 OID 43628)
-- Name: taskresults_lateraltransfer_handplacementimageid_key; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY taskresults_lateraltransfer
    ADD CONSTRAINT taskresults_lateraltransfer_handplacementimageid_key UNIQUE (handplacementimageid);


--
-- TOC entry 2297 (class 2606 OID 43630)
-- Name: taskresults_lateraltransfer_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY taskresults_lateraltransfer
    ADD CONSTRAINT taskresults_lateraltransfer_pkey PRIMARY KEY (uid);


--
-- TOC entry 2299 (class 2606 OID 43632)
-- Name: taskresults_lateraltransfer_pullendimageid_key; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY taskresults_lateraltransfer
    ADD CONSTRAINT taskresults_lateraltransfer_pullendimageid_key UNIQUE (pullendimageid);


--
-- TOC entry 2301 (class 2606 OID 43634)
-- Name: taskresults_lateraltransfer_pullstartimageid_key; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY taskresults_lateraltransfer
    ADD CONSTRAINT taskresults_lateraltransfer_pullstartimageid_key UNIQUE (pullstartimageid);


--
-- TOC entry 2287 (class 2606 OID 43636)
-- Name: taskresults_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY taskresults
    ADD CONSTRAINT taskresults_pkey PRIMARY KEY (uid);


--
-- TOC entry 2311 (class 2606 OID 49425)
-- Name: taskresults_verticallift_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY taskresults_verticallift
    ADD CONSTRAINT taskresults_verticallift_pkey PRIMARY KEY (uid);


--
-- TOC entry 2315 (class 2606 OID 49448)
-- Name: taskresults_verticallift_reps_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY taskresults_verticallift_reps
    ADD CONSTRAINT taskresults_verticallift_reps_pkey PRIMARY KEY (taskresults_verticallift_uid, repid);


--
-- TOC entry 2313 (class 2606 OID 49427)
-- Name: taskresults_verticallift_unique; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY taskresults_verticallift
    ADD CONSTRAINT taskresults_verticallift_unique UNIQUE (calibrationimageid);


--
-- TOC entry 2276 (class 2606 OID 43642)
-- Name: uid_pkey; Type: CONSTRAINT; Schema: public; Owner: aims; Tablespace: 
--

ALTER TABLE ONLY student_results_stage_scores
    ADD CONSTRAINT uid_pkey PRIMARY KEY (uid);


--
-- TOC entry 2260 (class 1259 OID 43643)
-- Name: fki_StudentID_fkey; Type: INDEX; Schema: public; Owner: aims; Tablespace: 
--

CREATE INDEX "fki_StudentID_fkey" ON student_results USING btree (studentid);


--
-- TOC entry 2230 (class 1259 OID 43644)
-- Name: fki_administrators_account_fkey; Type: INDEX; Schema: public; Owner: aims; Tablespace: 
--

CREATE INDEX fki_administrators_account_fkey ON administrators USING btree (accountid);


--
-- TOC entry 2231 (class 1259 OID 43645)
-- Name: fki_administrators_organization_fkey; Type: INDEX; Schema: public; Owner: aims; Tablespace: 
--

CREATE INDEX fki_administrators_organization_fkey ON administrators USING btree (organizationid);


--
-- TOC entry 2240 (class 1259 OID 43646)
-- Name: fki_doctors_account_fkey; Type: INDEX; Schema: public; Owner: aims; Tablespace: 
--

CREATE INDEX fki_doctors_account_fkey ON doctors USING btree (accountid);


--
-- TOC entry 2247 (class 1259 OID 43647)
-- Name: fki_messages_course_fkey; Type: INDEX; Schema: public; Owner: aims; Tablespace: 
--

CREATE INDEX fki_messages_course_fkey ON messages USING btree (courseid);


--
-- TOC entry 2248 (class 1259 OID 43648)
-- Name: fki_messages_to_student_fkey; Type: INDEX; Schema: public; Owner: aims; Tablespace: 
--

CREATE INDEX fki_messages_to_student_fkey ON messages USING btree (toid);


--
-- TOC entry 2251 (class 1259 OID 43649)
-- Name: fki_organizations_package_fkey; Type: INDEX; Schema: public; Owner: aims; Tablespace: 
--

CREATE INDEX fki_organizations_package_fkey ON organizations USING btree (packageid);


--
-- TOC entry 2281 (class 1259 OID 43650)
-- Name: fki_students_account_fkey; Type: INDEX; Schema: public; Owner: aims; Tablespace: 
--

CREATE INDEX fki_students_account_fkey ON students USING btree (accountid);


--
-- TOC entry 2330 (class 2606 OID 43651)
-- Name: StudentID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY student_results
    ADD CONSTRAINT "StudentID_fkey" FOREIGN KEY (studentid) REFERENCES students(uid);


--
-- TOC entry 2736 (class 0 OID 0)
-- Dependencies: 2330
-- Name: CONSTRAINT "StudentID_fkey" ON student_results; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON CONSTRAINT "StudentID_fkey" ON student_results IS 'links result to specific student''s unique id #';


--
-- TOC entry 2316 (class 2606 OID 43656)
-- Name: accounts_organization_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT accounts_organization_fkey FOREIGN KEY (organizationid) REFERENCES organizations(uid);


--
-- TOC entry 2317 (class 2606 OID 43661)
-- Name: administrator_settings_administratorid_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY administrator_settings
    ADD CONSTRAINT administrator_settings_administratorid_fkey1 FOREIGN KEY (administratorid) REFERENCES administrators(uid);


--
-- TOC entry 2318 (class 2606 OID 43666)
-- Name: administrators_account_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY administrators
    ADD CONSTRAINT administrators_account_fkey FOREIGN KEY (accountid) REFERENCES accounts(uid);


--
-- TOC entry 2737 (class 0 OID 0)
-- Dependencies: 2318
-- Name: CONSTRAINT administrators_account_fkey ON administrators; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON CONSTRAINT administrators_account_fkey ON administrators IS 'links the administrator to the account to which they belong';


--
-- TOC entry 2319 (class 2606 OID 43671)
-- Name: administrators_organization_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY administrators
    ADD CONSTRAINT administrators_organization_fkey FOREIGN KEY (organizationid) REFERENCES organizations(uid);


--
-- TOC entry 2738 (class 0 OID 0)
-- Dependencies: 2319
-- Name: CONSTRAINT administrators_organization_fkey ON administrators; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON CONSTRAINT administrators_organization_fkey ON administrators IS 'links the administrators to the organization to which they belong';


--
-- TOC entry 2320 (class 2606 OID 43676)
-- Name: courses_doctor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY courses
    ADD CONSTRAINT courses_doctor_fkey FOREIGN KEY (doctorid) REFERENCES doctors(uid);


--
-- TOC entry 2321 (class 2606 OID 43681)
-- Name: doctor_settings_doctorid_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY doctor_settings
    ADD CONSTRAINT doctor_settings_doctorid_fkey1 FOREIGN KEY (doctorid) REFERENCES doctors(uid);


--
-- TOC entry 2322 (class 2606 OID 43686)
-- Name: doctors_account_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY doctors
    ADD CONSTRAINT doctors_account_fkey FOREIGN KEY (accountid) REFERENCES accounts(uid);


--
-- TOC entry 2739 (class 0 OID 0)
-- Dependencies: 2322
-- Name: CONSTRAINT doctors_account_fkey ON doctors; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON CONSTRAINT doctors_account_fkey ON doctors IS 'links the doctor to their uniquie account id#';


--
-- TOC entry 2323 (class 2606 OID 43691)
-- Name: doctors_organization_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY doctors
    ADD CONSTRAINT doctors_organization_fkey FOREIGN KEY (organizationid) REFERENCES organizations(uid);


--
-- TOC entry 2324 (class 2606 OID 43696)
-- Name: intubation_statsheet_student_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY intubation_statsheets
    ADD CONSTRAINT intubation_statsheet_student_fkey FOREIGN KEY (studentid) REFERENCES students(uid);


--
-- TOC entry 2342 (class 2606 OID 43701)
-- Name: intubationresult_overallresult_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_intubation
    ADD CONSTRAINT intubationresult_overallresult_fkey FOREIGN KEY (resultid) REFERENCES taskresults(uid);


--
-- TOC entry 2349 (class 2606 OID 43925)
-- Name: kinect_images_fk; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_cpr
    ADD CONSTRAINT kinect_images_fk FOREIGN KEY (calibrationimageid) REFERENCES kinect_images(uid);


--
-- TOC entry 2352 (class 2606 OID 43940)
-- Name: kinect_images_fk; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_cpr_compressions
    ADD CONSTRAINT kinect_images_fk FOREIGN KEY (compressionimageid) REFERENCES kinect_images(uid);


--
-- TOC entry 2357 (class 2606 OID 49454)
-- Name: kinect_images_fk; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_verticallift_reps
    ADD CONSTRAINT kinect_images_fk FOREIGN KEY (downimageid) REFERENCES kinect_images(uid);


--
-- TOC entry 2354 (class 2606 OID 50163)
-- Name: kinect_images_fk; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_verticallift
    ADD CONSTRAINT kinect_images_fk FOREIGN KEY (calibrationimageid) REFERENCES kinect_images(uid);


--
-- TOC entry 2350 (class 2606 OID 43930)
-- Name: kinect_images_fk2; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_cpr
    ADD CONSTRAINT kinect_images_fk2 FOREIGN KEY (checkpulseimageid) REFERENCES kinect_images(uid);


--
-- TOC entry 2358 (class 2606 OID 49459)
-- Name: kinect_images_fk2; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_verticallift_reps
    ADD CONSTRAINT kinect_images_fk2 FOREIGN KEY (liftobjectimageid) REFERENCES kinect_images(uid);


--
-- TOC entry 2359 (class 2606 OID 49464)
-- Name: kinect_images_fk3; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_verticallift_reps
    ADD CONSTRAINT kinect_images_fk3 FOREIGN KEY (lowerobjectimageid) REFERENCES kinect_images(uid);


--
-- TOC entry 2360 (class 2606 OID 49469)
-- Name: kinect_images_fk4; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_verticallift_reps
    ADD CONSTRAINT kinect_images_fk4 FOREIGN KEY (riseimageid) REFERENCES kinect_images(uid);


--
-- TOC entry 2326 (class 2606 OID 43746)
-- Name: messages_course_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY messages
    ADD CONSTRAINT messages_course_fkey FOREIGN KEY (courseid) REFERENCES courses(uid);


--
-- TOC entry 2740 (class 0 OID 0)
-- Dependencies: 2326
-- Name: CONSTRAINT messages_course_fkey ON messages; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON CONSTRAINT messages_course_fkey ON messages IS 'links the message to the course which it was sent in';


--
-- TOC entry 2327 (class 2606 OID 43751)
-- Name: messages_from_doctor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY messages
    ADD CONSTRAINT messages_from_doctor_fkey FOREIGN KEY (fromid) REFERENCES doctors(uid);


--
-- TOC entry 2328 (class 2606 OID 43756)
-- Name: messages_to_student_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY messages
    ADD CONSTRAINT messages_to_student_fkey FOREIGN KEY (toid) REFERENCES students(uid);


--
-- TOC entry 2741 (class 0 OID 0)
-- Dependencies: 2328
-- Name: CONSTRAINT messages_to_student_fkey ON messages; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON CONSTRAINT messages_to_student_fkey ON messages IS 'links the message to the student whom the message is addressed to';


--
-- TOC entry 2329 (class 2606 OID 43761)
-- Name: organizations_package_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY organizations
    ADD CONSTRAINT organizations_package_fkey FOREIGN KEY (packageid) REFERENCES packages(uid);


--
-- TOC entry 2742 (class 0 OID 0)
-- Dependencies: 2329
-- Name: CONSTRAINT organizations_package_fkey ON organizations; Type: COMMENT; Schema: public; Owner: aims
--

COMMENT ON CONSTRAINT organizations_package_fkey ON organizations IS 'links organizations to the package which they''re subscribed to';


--
-- TOC entry 2338 (class 2606 OID 43766)
-- Name: resultid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY student_results_stage_scores
    ADD CONSTRAINT resultid_fkey FOREIGN KEY (resultid) REFERENCES student_results(uid);


--
-- TOC entry 2331 (class 2606 OID 43771)
-- Name: stagescore_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY student_results_3dscreenshots
    ADD CONSTRAINT stagescore_fkey FOREIGN KEY (stagescoreid) REFERENCES student_results_stage_scores(uid);


--
-- TOC entry 2332 (class 2606 OID 43776)
-- Name: student_results_lateraltransfer_calibrationimageid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY student_results_lateraltransfer
    ADD CONSTRAINT student_results_lateraltransfer_calibrationimageid_fkey FOREIGN KEY (calibrationimageid) REFERENCES kinect_images(uid);


--
-- TOC entry 2333 (class 2606 OID 43781)
-- Name: student_results_lateraltransfer_handplacementimageid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY student_results_lateraltransfer
    ADD CONSTRAINT student_results_lateraltransfer_handplacementimageid_fkey FOREIGN KEY (handplacementimageid) REFERENCES kinect_images(uid);


--
-- TOC entry 2334 (class 2606 OID 43786)
-- Name: student_results_lateraltransfer_pullendimageid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY student_results_lateraltransfer
    ADD CONSTRAINT student_results_lateraltransfer_pullendimageid_fkey FOREIGN KEY (pullendimageid) REFERENCES kinect_images(uid);


--
-- TOC entry 2335 (class 2606 OID 43791)
-- Name: student_results_lateraltransfer_pullstartimageid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY student_results_lateraltransfer
    ADD CONSTRAINT student_results_lateraltransfer_pullstartimageid_fkey FOREIGN KEY (pullstartimageid) REFERENCES kinect_images(uid);


--
-- TOC entry 2336 (class 2606 OID 43796)
-- Name: student_results_lateraltransfer_studentid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY student_results_lateraltransfer
    ADD CONSTRAINT student_results_lateraltransfer_studentid_fkey FOREIGN KEY (studentid) REFERENCES students(uid);


--
-- TOC entry 2337 (class 2606 OID 43801)
-- Name: student_results_lateraltransferlookup_lateraltransferid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY student_results_lateraltransferlookup
    ADD CONSTRAINT student_results_lateraltransferlookup_lateraltransferid_fkey FOREIGN KEY (lateraltransferid) REFERENCES student_results_lateraltransfer(uid);


--
-- TOC entry 2339 (class 2606 OID 43806)
-- Name: student_settings_studentid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY student_settings
    ADD CONSTRAINT student_settings_studentid_fkey FOREIGN KEY (studentid) REFERENCES students(uid);


--
-- TOC entry 2325 (class 2606 OID 43811)
-- Name: studentid_lesson_statsheets_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY lesson_statsheets
    ADD CONSTRAINT studentid_lesson_statsheets_fkey FOREIGN KEY (studentid) REFERENCES students(uid);


--
-- TOC entry 2340 (class 2606 OID 43816)
-- Name: students_account_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY students
    ADD CONSTRAINT students_account_fkey FOREIGN KEY (accountid) REFERENCES accounts(uid);


--
-- TOC entry 2341 (class 2606 OID 43821)
-- Name: students_organization_key; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY students
    ADD CONSTRAINT students_organization_key FOREIGN KEY (organizationid) REFERENCES organizations(uid);


--
-- TOC entry 2353 (class 2606 OID 43945)
-- Name: taskresults_cpr_fk; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_cpr_compressions
    ADD CONSTRAINT taskresults_cpr_fk FOREIGN KEY (taskresults_cpr_uid) REFERENCES taskresults_cpr(uid);


--
-- TOC entry 2351 (class 2606 OID 43935)
-- Name: taskresults_cpr_resultsid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_cpr
    ADD CONSTRAINT taskresults_cpr_resultsid_fkey FOREIGN KEY (resultid) REFERENCES taskresults(uid);


--
-- TOC entry 2344 (class 2606 OID 43836)
-- Name: taskresults_lateraltransfer_calibrationimageid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_lateraltransfer
    ADD CONSTRAINT taskresults_lateraltransfer_calibrationimageid_fkey FOREIGN KEY (calibrationimageid) REFERENCES kinect_images(uid);


--
-- TOC entry 2345 (class 2606 OID 43841)
-- Name: taskresults_lateraltransfer_handplacementimageid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_lateraltransfer
    ADD CONSTRAINT taskresults_lateraltransfer_handplacementimageid_fkey FOREIGN KEY (handplacementimageid) REFERENCES kinect_images(uid);


--
-- TOC entry 2346 (class 2606 OID 43846)
-- Name: taskresults_lateraltransfer_pullendimageid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_lateraltransfer
    ADD CONSTRAINT taskresults_lateraltransfer_pullendimageid_fkey FOREIGN KEY (pullendimageid) REFERENCES kinect_images(uid);


--
-- TOC entry 2347 (class 2606 OID 43851)
-- Name: taskresults_lateraltransfer_pullstartimageid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_lateraltransfer
    ADD CONSTRAINT taskresults_lateraltransfer_pullstartimageid_fkey FOREIGN KEY (pullstartimageid) REFERENCES kinect_images(uid);


--
-- TOC entry 2348 (class 2606 OID 43856)
-- Name: taskresults_lateraltransfer_resultid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_lateraltransfer
    ADD CONSTRAINT taskresults_lateraltransfer_resultid_fkey FOREIGN KEY (resultid) REFERENCES taskresults(uid);


--
-- TOC entry 2343 (class 2606 OID 43861)
-- Name: taskresults_studentid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults
    ADD CONSTRAINT taskresults_studentid_fkey FOREIGN KEY (studentid) REFERENCES students(uid);


--
-- TOC entry 2356 (class 2606 OID 49449)
-- Name: taskresults_verticallift_fk; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_verticallift_reps
    ADD CONSTRAINT taskresults_verticallift_fk FOREIGN KEY (taskresults_verticallift_uid) REFERENCES taskresults_verticallift(uid);


--
-- TOC entry 2355 (class 2606 OID 50168)
-- Name: taskresults_verticallift_resultsid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: aims
--

ALTER TABLE ONLY taskresults_verticallift
    ADD CONSTRAINT taskresults_verticallift_resultsid_fkey FOREIGN KEY (resultid) REFERENCES taskresults(uid);


--
-- TOC entry 2368 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2013-07-29 13:28:46

--
-- PostgreSQL database dump complete
--

