//<SIMIS><===============================================================================><SIMIS>
//-//<copyright file company="SimIS Inc">
//-//
//-//SimIS Inc. and HealthCare Simulations, LLC CONFIDENTIAL
//-//
//-//[2014] SimIS Inc. and HealthCare Simulations, LLC Confidential
//-//
//-//All Rights Reserved.
//-//
//-//NOTICE:  All information contained herein is, and remains the property
//-//     of SimIS, Inc. and HealthCare Simulations, LLC and its suppliers, if
//-//     any.  The intellectual and technical concepts contained herein are
//-//     proprietary to SimIS Inc. and HealthCare Simulations, LLC and its
//-//     suppliers and may be covered by U.S. and Foreign Patents, patents
//-//     in process, and are protected by trade secret or copyright law.
//-//
//-//Dissemination of this information or reproduction of this material
//-//     is strictly forbidden unless prior written permission is obtained
//-//     from SimIS Inc. and HealthCare Simulations, LLC.
//-//</copyright>
//<SIMIS><===============================================================================><SIMIS>




using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace Core.Components.Converters
{
    public class BoolToVisibilityConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (Visibility)value == Visibility.Visible;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class VisibilityToBoolConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (Visibility)value == Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? Visibility.Visible : Visibility.Collapsed;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class BoolToInverseVisibilityConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (Visibility)value != Visibility.Visible;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class VisibilityToInverseBoolConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (Visibility)value != Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? Visibility.Collapsed : Visibility.Visible;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    /// <summary>
    /// Takes a collection of visibilities and if the parameter is null, or false, return true if ANY of the visibilities are Visible. If the 
    /// parameter is true, then returns true if ALL of the visibilities are true.
    /// </summary>
    [ValueConversion(typeof(Visibility), typeof(bool))]
    public class VisibilitiesToBoolConverter : MarkupExtension, IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                bool atLeastOneTrue = false;
                bool allTrue = true;

                foreach (var value in values)
                {
                    if ((Visibility)value == Visibility.Visible)
                        atLeastOneTrue = true;
                    else
                        allTrue = false;
                }
                if ((parameter == null || (bool)parameter == false))
                    return atLeastOneTrue;
                else if ((bool)parameter == true)
                    return allTrue;
                else
                    return false;
            }
            catch(Exception exc)
            {
                System.Diagnostics.Debug.Print("BoolToVisibilityConverter VisibilitiesToBoolConverter.Convert() ERROR: {0}", exc.Message);
                return false;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException("Cannot convert back");
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    /// <summary>
    /// Takes a collection of visibilities and if the parameter is null, or false, return true if ANY of the visibilities are Visible. If the 
    /// parameter is true, then returns true if ALL of the visibilities are true.
    /// </summary>
    [ValueConversion(typeof(bool), typeof(bool))]
    public class InverseBoolConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                return !(bool)value;
            }
            catch
            {

            }

            return false;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                return !(bool)value;
            }
            catch
            {

            }

            return false;
        }
    }

    [ValueConversion(typeof(Visibility), typeof(Visibility))]
    public class InverseVisibilityConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (value.GetType() == typeof(Visibility))
                {
                    if ((Visibility)value == Visibility.Collapsed || (Visibility)value == Visibility.Hidden)
                    {
                        return Visibility.Visible;
                    }
                }
                return Visibility.Collapsed;
            }
            catch { return Visibility.Visible; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (value.GetType() == typeof(Visibility))
                {
                    if ((Visibility)value == Visibility.Visible)
                    {
                        return Visibility.Collapsed;
                    }
                }
                return Visibility.Visible;
            }
            catch { return Visibility.Visible; }
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
