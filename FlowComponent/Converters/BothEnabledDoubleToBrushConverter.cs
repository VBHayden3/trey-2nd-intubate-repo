//<SIMIS><===============================================================================><SIMIS>
//-//<copyright file company="SimIS Inc">
//-//
//-//SimIS Inc. and HealthCare Simulations, LLC CONFIDENTIAL
//-//
//-//[2014] SimIS Inc. and HealthCare Simulations, LLC Confidential
//-//
//-//All Rights Reserved.
//-//
//-//NOTICE:  All information contained herein is, and remains the property
//-//     of SimIS, Inc. and HealthCare Simulations, LLC and its suppliers, if
//-//     any.  The intellectual and technical concepts contained herein are
//-//     proprietary to SimIS Inc. and HealthCare Simulations, LLC and its
//-//     suppliers and may be covered by U.S. and Foreign Patents, patents
//-//     in process, and are protected by trade secret or copyright law.
//-//
//-//Dissemination of this information or reproduction of this material
//-//     is strictly forbidden unless prior written permission is obtained
//-//     from SimIS Inc. and HealthCare Simulations, LLC.
//-//</copyright>
//<SIMIS><===============================================================================><SIMIS>




using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;
using System.Globalization;
using System.Windows;

namespace Core.Components
{
    public class BothEnabledDoubleToBrushConverter: IMultiValueConverter
    {
        public object Convert(object[] objs, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(bool)objs[0] || !(bool)objs[1])
                return Brushes.Transparent;
            
            double value = 0;

            if (!Double.TryParse( objs[2].ToString(), out value))
            {
                return Brushes.Transparent;
            }

            //string filePath = "";
            
            //Brush foreground = Brushes.Green;

            //if (value >= 70d)
            //{
            //    foreground = Brushes.Green;
            //}
            //else
            //{
            //    foreground = Brushes.Red;
            //}

            LinearGradientBrush brush = new LinearGradientBrush();
            brush.StartPoint = new Point(0.5, 0);
            brush.EndPoint = new Point(0.5, 1);

            if (value >= 0.70)    // GREEN
            {
                //bottom Gradient
                brush.GradientStops.Add(new GradientStop()
                {
                    Color = Color.FromArgb(255, 255, 255, 255),//white
                    Offset = -0.1
                });
                //bottom Gradient
                brush.GradientStops.Add(new GradientStop()
                {
                    Color = Color.FromArgb(255, 0, 220, 0),//light green
                    Offset = System.Math.Min( System.Math.Max( 0.7 - ( (100-value)/100 * 2.142857 ), 0.25), 0.75 )
                });/*
                //Top Gradient
                brush.GradientStops.Add(new GradientStop()
                {
                    Color = Color.FromArgb(255, 0, 97, 0),//dark green
                    Offset = 0.9
                });*/
                //Top Gradient
                brush.GradientStops.Add(new GradientStop()
                {
                    Color = Color.FromArgb(255, 0, 0, 0),//black
                    Offset = 1.5
                });
            }
            /*
        else if (value >= 0.75)    // Yellow
        {

            //bottom Gradient
            brush.GradientStops.Add(new GradientStop()
            {
                Color = Color.FromArgb(255, 255, 255, 130),//light yellow
                Offset = 0
            });
            //Top Gradient
            brush.GradientStops.Add(new GradientStop()
            {
                Color = Color.FromArgb(255, 255, 255, 0),//dark yellow
                Offset = 1
            });
        }
        else if (value >= 0.85)    // ORANGE
        {

            //bottom Gradient
            brush.GradientStops.Add(new GradientStop()
            {
                Color = Color.FromArgb(255, 255, 160, 60),//light orange
                Offset = 0
            });
            //Top Gradient
            brush.GradientStops.Add(new GradientStop()
            {
                Color = Color.FromArgb(255, 255, 128, 0),//dark orange
                Offset = 1
            });
        }*/

            else // RED
            {
                //bottom Gradient
                brush.GradientStops.Add(new GradientStop()
                {
                    Color = Color.FromArgb(255, 255, 255, 255),//white
                    Offset = -0.1
                });
                //bottom Gradient
                brush.GradientStops.Add(new GradientStop()
                {
                    Color = Color.FromArgb(255, 220, 0, 0),//light red
                    Offset = 0.7
                });/*
                //Top Gradient
                brush.GradientStops.Add(new GradientStop()
                {
                    Color = Color.FromArgb(255, 97, 0, 0),//dark red
                    Offset = 0.9
                });*/

                //Top Gradient
                brush.GradientStops.Add(new GradientStop()
                {
                    Color = Color.FromArgb(255, 0, 0, 0),//black
                    Offset = 1.5
                });
            }

            return brush;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}

