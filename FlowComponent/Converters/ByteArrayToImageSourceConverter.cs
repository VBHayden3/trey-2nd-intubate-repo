//<SIMIS><===============================================================================><SIMIS>
//-//<copyright file company="SimIS Inc">
//-//
//-//SimIS Inc. and HealthCare Simulations, LLC CONFIDENTIAL
//-//
//-//[2014] SimIS Inc. and HealthCare Simulations, LLC Confidential
//-//
//-//All Rights Reserved.
//-//
//-//NOTICE:  All information contained herein is, and remains the property
//-//     of SimIS, Inc. and HealthCare Simulations, LLC and its suppliers, if
//-//     any.  The intellectual and technical concepts contained herein are
//-//     proprietary to SimIS Inc. and HealthCare Simulations, LLC and its
//-//     suppliers and may be covered by U.S. and Foreign Patents, patents
//-//     in process, and are protected by trade secret or copyright law.
//-//
//-//Dissemination of this information or reproduction of this material
//-//     is strictly forbidden unless prior written permission is obtained
//-//     from SimIS Inc. and HealthCare Simulations, LLC.
//-//</copyright>
//<SIMIS><===============================================================================><SIMIS>




using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Core.Components.Converters
{
    public class ByteArrayToImageSourceConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            byte[] byteArrayImage = value as byte[];

            WriteableBitmap bitmap = new WriteableBitmap(1920, 1080, 96, 96, PixelFormats.Bgr32, null);

            if (byteArrayImage != null && byteArrayImage.Length > 0)
            {
                bitmap.WritePixels(new System.Windows.Int32Rect(0, 0, 1920, 1080), byteArrayImage, 640 * 4, 0);
            }

            return bitmap;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }


        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
