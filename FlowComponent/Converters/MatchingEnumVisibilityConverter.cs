﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Core.Components.Converters
{
    [ValueConversion(typeof(Enum), typeof(Visibility))]
    public class MatchingEnumVisiblityConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (value.GetType().BaseType == typeof(Enum) && parameter.GetType().BaseType == typeof(Enum))
                {
                    if (value.Equals(parameter))
                        return Visibility.Visible;
                }
                return Visibility.Collapsed;
            }
            catch
            {
                return Visibility.Collapsed;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
