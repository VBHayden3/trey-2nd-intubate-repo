//<SIMIS><===============================================================================><SIMIS>
//-//<copyright file company="SimIS Inc">
//-//
//-//SimIS Inc. and HealthCare Simulations, LLC CONFIDENTIAL
//-//
//-//[2014] SimIS Inc. and HealthCare Simulations, LLC Confidential
//-//
//-//All Rights Reserved.
//-//
//-//NOTICE:  All information contained herein is, and remains the property
//-//     of SimIS, Inc. and HealthCare Simulations, LLC and its suppliers, if
//-//     any.  The intellectual and technical concepts contained herein are
//-//     proprietary to SimIS Inc. and HealthCare Simulations, LLC and its
//-//     suppliers and may be covered by U.S. and Foreign Patents, patents
//-//     in process, and are protected by trade secret or copyright law.
//-//
//-//Dissemination of this information or reproduction of this material
//-//     is strictly forbidden unless prior written permission is obtained
//-//     from SimIS Inc. and HealthCare Simulations, LLC.
//-//</copyright>
//<SIMIS><===============================================================================><SIMIS>




using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;

namespace Core.Components.Converters
{
    [ValueConversion(typeof(double), typeof(string))]
    public class PrettyDoubleStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.GetType() != typeof(double))
                return null;

            if (parameter != null)
            {
                if (parameter.GetType() == typeof(string))
                {
                    int cnt;
                    if (Int32.TryParse((string)parameter, out cnt))
                    {
                        string precision = ".";
                        for (int i = 0; i < cnt; i++)
                            precision += "#";

                        return String.Format("{0:0" + precision + "}", (double)value);
                    }
                }
            }
            
            return String.Format("{0:0}", (double)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.GetType() != typeof(string))
                return null;

            else
            {
                double result;
                if (Double.TryParse((string)value, out result))
                    return result;
                else
                    return null;
            }
        }
    }
}
