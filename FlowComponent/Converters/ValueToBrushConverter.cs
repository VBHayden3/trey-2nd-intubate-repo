//<SIMIS><===============================================================================><SIMIS>
//-//<copyright file company="SimIS Inc">
//-//
//-//SimIS Inc. and HealthCare Simulations, LLC CONFIDENTIAL
//-//
//-//[2014] SimIS Inc. and HealthCare Simulations, LLC Confidential
//-//
//-//All Rights Reserved.
//-//
//-//NOTICE:  All information contained herein is, and remains the property
//-//     of SimIS, Inc. and HealthCare Simulations, LLC and its suppliers, if
//-//     any.  The intellectual and technical concepts contained herein are
//-//     proprietary to SimIS Inc. and HealthCare Simulations, LLC and its
//-//     suppliers and may be covered by U.S. and Foreign Patents, patents
//-//     in process, and are protected by trade secret or copyright law.
//-//
//-//Dissemination of this information or reproduction of this material
//-//     is strictly forbidden unless prior written permission is obtained
//-//     from SimIS Inc. and HealthCare Simulations, LLC.
//-//</copyright>
//<SIMIS><===============================================================================><SIMIS>




using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Media;

namespace Core.Components
{
    [ValueConversion(typeof(double), typeof(Brush))]
    class ValueToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double number = (double)value;
            double min = 0;
            double max = 100;

            // Get the value limits from parameter
            try
            {
                string[] limits = (parameter as string).Split(new char[] { '|' });
                min = double.Parse(limits[0], CultureInfo.InvariantCulture);
                max = double.Parse(limits[1], CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                throw new ArgumentException("Parameter not valid. Enter in format: 'MinDouble|MaxDouble'");
            }

            if (max <= min)
            {
                throw new ArgumentException("Parameter not valid. MaxDouble has to be greater then MinDouble.");
            }

            if (number >= min && number <= max)
            {
                // Calculate color channels
                double range = (max - min) / 2;
                number -= max - range;
                double factor = 255 / range;
                double red = number < 0 ? number * factor : 255;
                double green = number > 0 ? (range - number) * factor : 255;

                // Create and return brush
                Color color = Color.FromRgb((byte)red, (byte)green, 0);
                SolidColorBrush brush = new SolidColorBrush(color);
                return brush;
            }

            // Fallback brush
            return Brushes.Transparent;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
