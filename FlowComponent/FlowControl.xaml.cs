﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Kinect;
using LightBuzz.Vitruvius;
using AIMS;

namespace AIMS.FlowComponent
{
    public partial class FlowControl : UserControl, INotifyPropertyChanged
    {
        #region Fields
        public const int HalfRealizedCount = 7;
        public const int PageSize = HalfRealizedCount;
        private readonly ICoverFactory coverFactory;
        private readonly Dictionary<int, ImageInfo> imageList = new Dictionary<int, ImageInfo>();
        private readonly Dictionary<string, int> labelIndex = new Dictionary<string, int>();
        private readonly Dictionary<int, string> indexLabel = new Dictionary<int, string>();
        private readonly Dictionary<int, ICover> coverList = new Dictionary<int, ICover>();
        private readonly GestureController activeRecognizer;
        private int index;
        private int firstRealized = -1;
        private int lastRealized = -1;
        #endregion

        public string DisconectedReason;

        /// Array of arrays of contiguous line segements that represent a skeleton.
        /// </summary>
        private static readonly JointType[][] SkeletonSegmentRuns = new JointType[][]
        {
            new JointType[] 
            { 
                JointType.Head, JointType.SpineShoulder, JointType.SpineBase 
            },
            new JointType[] 
            { 
                JointType.HandLeft, JointType.WristLeft, JointType.ElbowLeft, JointType.ShoulderLeft,
                JointType.SpineShoulder,
                JointType.ShoulderRight, JointType.ElbowRight, JointType.WristRight, JointType.HandRight
            },
            new JointType[]
            {
                JointType.FootLeft, JointType.AnkleLeft, JointType.KneeLeft, JointType.HipLeft,
                JointType.SpineBase,
                JointType.HipRight, JointType.KneeRight, JointType.AnkleRight, JointType.FootRight
            }
        };

        /// The sensor we're currently tracking.
        /// </summary>
        private KinectSensor nui;

        public KinectSensor KinectSensor { get { return nui; } set { nui = value; ReadyTheKinect(); } }

        private MultiSourceFrameReader reader = null;

        private void ReadyTheKinect()
        {
            //MessageBox.Show("New Kinect added to Flow Component");

            System.Diagnostics.Debug.WriteLine("FlowControl -> ReadyTheKinect()");

            if (this.nui != null)
            {

                if (!this.nui.IsOpen)
                {
                    this.nui.Open();
                }

                if (reader == null) {

                    // Plug into the kinect frame events
                    reader = this.nui.OpenMultiSourceFrameReader(FrameSourceTypes.Body);

                    // Setup a handler for when a frame arrives
                    reader.MultiSourceFrameArrived += this.OnSkeletonFrameReady;
                }

                //MessageBox.Show("Kinect ready for skeletal tracking.");
            }
        }

        /// <summary>
        /// There is currently no connected sensor.
        /// </summary>
        private bool isDisconnectedField = true;

        /// <summary>
        /// Any message associated with a failure to connect.
        /// </summary>
        private string disconnectedReasonField;

        /// <summary>
        /// Array to receive skeletons from sensor, resize when needed.
        /// </summary>
        private Body[] skeletons = new Body[0];
        //IList<Body> _bodies;

        /// <summary>
        /// Time until skeleton ceases to be highlighted.
        /// </summary>
        private DateTime highlightTime = DateTime.MinValue;

        /// <summary>
        /// The ID of the skeleton to highlight.
        /// </summary>
        private ulong highlightId;

        /// <summary>
        /// The ID if the skeleton to be tracked.
        /// </summary>
        private ulong nearestId;

        /// <summary>
        /// The index of the current image.
        /// </summary>
        private int indexField = 0;

        /// <summary>
        /// Event implementing INotifyPropertyChanged interface.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets a value indicating whether no Kinect is currently connected.
        /// </summary>
        public bool IsDisconnected
        {
            get
            {
                return this.isDisconnectedField;
            }

            private set
            {
                if (this.isDisconnectedField != value)
                {
                    this.isDisconnectedField = value;

                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("IsDisconnected"));
                    }
                }
            }
        }

        /// <summary>
        /// Gets any message associated with a failure to connect.
        /// </summary>
        public string DisconnectedReason
        {
            get
            {
                return this.disconnectedReasonField;
            }

            private set
            {
                if (this.disconnectedReasonField != value)
                {
                    this.disconnectedReasonField = value;

                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("DisconnectedReason"));
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the index number of the image to be shown.
        /// </summary>
        public int Index
        {
            get
            {
                return this.indexField;
            }

            set
            {
                if (this.indexField != value)
                {
                    this.indexField = value;

                    // Notify world of change to Index and Picture.
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("Index"));
                    }

                    UpdateIndex(value);
                }
            }
        }

        /// <summary>
        /// Gets the previous image displayed.
        /// </summary>
        public BitmapImage PreviousPicture { get; private set; }

        /// <summary>
        /// Gets the current image to be displayed.
        /// </summary>
        public BitmapImage Picture { get; private set; }

        /// <summary>
        /// Gets teh next image displayed.
        /// </summary>
        public BitmapImage NextPicture { get; private set; }

        /// <summary>
        /// Handler for skeleton ready handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event args.</param>
        public void OnSkeletonFrameReady(object sender, MultiSourceFrameArrivedEventArgs e)
        {

            System.Diagnostics.Debug.WriteLine("FlowControl -> OnSkeletonFrameReady()");

            // Get a reference to the multi-frame
            var multiSourceFrame = e.FrameReference.AcquireFrame();

            // Get the frame.
            using (var frame = multiSourceFrame.BodyFrameReference.AcquireFrame())
            {
                // Ensure we have a frame.
                if (frame != null)
                {

                    // Resize the skeletons array if a new size (normally only on first call).
                    if (this.skeletons.Length != frame.BodyFrameSource.BodyCount)
                    {
                        this.skeletons = new Body[frame.BodyFrameSource.BodyCount];
                    }

                    // Get the skeletons.
                    frame.GetAndRefreshBodyData(this.skeletons);

                    // Assume no nearest skeleton and that the nearest skeleton is a long way away.
                    Body nearestBody = null;
                    var nearestDistance2 = double.MaxValue;


                    // Look through the skeletons.
                    foreach (Body skeleton in this.skeletons)
                    {
                        // Only consider tracked skeletons.
                        if (skeleton.IsTracked)
                        {
                            System.Diagnostics.Debug.WriteLine("FlowControl -> Found a tracked skeleton");

                            Joint joint = skeleton.Joints[JointType.SpineBase];
                            if (joint == null)
                                continue;

                            // Find the distance squared.
                            var distance2 = (joint.Position.X * joint.Position.X) +
                                (joint.Position.Y * joint.Position.Y) +
                                (joint.Position.Z * joint.Position.Z);

                            // Is the new distance squared closer than the nearest so far?
                            if (distance2 < nearestDistance2)
                            {
                                // Use the new values.
                                nearestDistance2 = distance2;
                                nearestBody = skeleton;
                                nearestId = skeleton.TrackingId;
                            }
                        }
                    }

                    // Update skeleton gestures.
                    if (nearestBody != null)
                    {
                        System.Diagnostics.Debug.WriteLine("FlowControl -> activeRecognizer.Update(nearestBody)...");
                        this.activeRecognizer.Update(nearestBody);
                    }

                    //this.DrawStickMen(this.skeletons);
                }
            }
        }

        /// <summary>
        /// Select a skeleton to be highlighted.
        /// </summary>
        /// <param name="skeleton">The skeleton</param>
        private void HighlightSkeleton(ulong trackingId)
        {
            // Set the highlight time to be a short time from now.
            this.highlightTime = DateTime.UtcNow + TimeSpan.FromSeconds(0.5);

            // Record the ID of the skeleton.
            this.highlightId = trackingId;
        }

        /// <summary>
        /// Draw stick men for all the tracked skeletons.
        /// </summary>
        /// <param name="skeletons">The skeletons to draw.</param>
        private void DrawStickMen(Body[] skeletons)
        {
            // Remove any previous skeletons.
            StickMen.Children.Clear();

            foreach (var skeleton in skeletons)
            {
                // Only draw tracked skeletons.
                if (skeleton.IsTracked)
                {
                    // Draw a background for the next pass.
                    this.DrawStickMan(skeleton, Brushes.WhiteSmoke, 7);
                }
            }

            foreach (var skeleton in skeletons)
            {
                // Only draw tracked skeletons.
                if (skeleton.IsTracked)
                {
                    // Pick a brush, Red for a skeleton that has recently gestures, black for the nearest, gray otherwise.
                    var brush = DateTime.UtcNow < this.highlightTime && skeleton.TrackingId == this.highlightId ? Brushes.Red :
                        skeleton.TrackingId == this.nearestId ? Brushes.Black : Brushes.Gray;

                    // Draw the individual skeleton.
                    this.DrawStickMan(skeleton, brush, 3);
                }
            }
        }

        /// <summary>
        /// Draw an individual skeleton.
        /// </summary>
        /// <param name="skeleton">The skeleton to draw.</param>
        /// <param name="brush">The brush to use.</param>
        /// <param name="thickness">This thickness of the stroke.</param>
        private void DrawStickMan(Body skeleton, Brush brush, int thickness)
        {
            Debug.Assert(skeleton.IsTracked, "The skeleton is being tracked.");

            foreach (var run in SkeletonSegmentRuns)
            {
                var next = this.GetJointPoint(skeleton, run[0]);

                for (var i = 1; i < run.Length; i++)
                {
                    var prev = next;
                    next = this.GetJointPoint(skeleton, run[i]);

                    var line = new Line
                    {
                        Stroke = brush,
                        StrokeThickness = thickness,
                        X1 = prev.X,
                        Y1 = prev.Y,
                        X2 = next.X,
                        Y2 = next.Y,
                        StrokeEndLineCap = PenLineCap.Round,
                        StrokeStartLineCap = PenLineCap.Round
                    };

                    StickMen.Children.Add(line);
                }
            }
        }

        /// <summary>
        /// Convert skeleton joint to a point on the StickMen canvas.
        /// </summary>
        /// <param name="skeleton">The skeleton.</param>
        /// <param name="jointType">The joint to project.</param>
        /// <returns>The projected point.</returns>
        private Point GetJointPoint(Body skeleton, JointType jointType)
        {
            Joint joint = skeleton.Joints[jointType];

            // Points are centered on the StickMen canvas and scaled according to its height allowing
            // approximately +/- 1.5m from center line.
            var point = new Point
            {
                X = (StickMen.Width / 2) + (StickMen.Height * joint.Position.X / 3),
                Y = (StickMen.Width / 2) - (StickMen.Height * joint.Position.Y / 3)
            };

            return point;
        }

        #region Private stuff
        /// <summary>
        /// Actually rotates the cover to the cover at index pos. Will animate if set to true.
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="animate"></param>
        private void RotateCover(int pos, bool animate)
        {
            if (coverList.ContainsKey(pos))
                coverList[pos].Animate(index, animate);
        }

        /// <summary>
        /// Called only internally when Index property changes.
        /// </summary>
        /// <param name="newIndex"></param>
        private void UpdateIndex(int newIndex)
        {
            if (index != newIndex)
            {
                bool animate = Math.Abs(newIndex - index) < PageSize;
                UpdateRange(newIndex);
                int oldIndex = index;
                index = newIndex;
                if (index > oldIndex)
                {
                    if (oldIndex < firstRealized)
                        oldIndex = firstRealized;
                    for (int i = oldIndex; i <= index; i++)
                        RotateCover(i, animate);
                }
                else
                {
                    if (oldIndex > lastRealized)
                        oldIndex = lastRealized;
                    for (int i = oldIndex; i >= index; i--)
                        RotateCover(i, animate);
                }
                camera.Position = new Point3D(Cover.CoverStep * index, camera.Position.Y, camera.Position.Z);
            }
        }
        /// <summary>
        /// Handles the mouse down button click.
        /// Currently checks to see if any cover mesh was hit and then changes the index, thus rotating to that cover.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void viewPort_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var rayMeshResult = (RayMeshGeometry3DHitTestResult)VisualTreeHelper.HitTest(viewPort, e.GetPosition(viewPort));
            if (rayMeshResult != null)
            {
                foreach (int i in coverList.Keys)
                {
                    if (!coverList.ContainsKey(i))
                        continue;
                    if (coverList[i].Matches(rayMeshResult.MeshHit))
                    {
                        Index = i;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Removes a cover from the coverflow.
        /// </summary>
        /// <param name="pos"></param>
        private void RemoveCover(int pos)
        {
            if (!coverList.ContainsKey(pos))
                return;
            coverList[pos].Destroy();
            coverList.Remove(pos);
        }

        /// <summary>
        /// Updates the virtualization range for the coverflow.
        /// </summary>
        /// <param name="newIndex"></param>
        private void UpdateRange(int newIndex)
        {
            int newFirstRealized = Math.Max(newIndex - HalfRealizedCount, 0);
            int newLastRealized = Math.Min(newIndex + HalfRealizedCount, imageList.Count - 1);
            if (lastRealized < newFirstRealized || firstRealized > newLastRealized)
            {
                visualModel.Children.Clear();
                coverList.Clear();
            }
            else if (firstRealized < newFirstRealized)
            {
                for (int i = firstRealized; i < newFirstRealized; i++)
                    RemoveCover(i);
            }
            else if (newLastRealized < lastRealized)
            {
                for (int i = lastRealized; i > newLastRealized; i--)
                    RemoveCover(i);
            }
            for (int i = newFirstRealized; i <= newLastRealized; i++)
            {
                if (!coverList.ContainsKey(i))
                {
                    ICover cover = coverFactory.NewCover(imageList[i].Host, imageList[i].Path, i, newIndex);
                    coverList.Add(i, cover);
                }
            }
            firstRealized = newFirstRealized;
            lastRealized = newLastRealized;
        }
        protected int FirstRealizedIndex
        {
            get { return firstRealized; }
        }
        protected int LastRealizedIndex
        {
            get { return lastRealized; }
        }
        /// <summary>
        /// Add a new image to the coverflow.
        /// </summary>
        /// <param name="info"></param>
        private void Add(ImageInfo info)
        {
            imageList.Add(imageList.Count, info);
            UpdateRange(index);
        }
        #endregion

        /// <summary>
        /// FlowControl is a component which acts as a Coverflow. 
        /// Hands-free guesture recognition has been added in to change from cover to cover.
        /// </summary>
        public FlowControl()
        {
            System.Diagnostics.Debug.WriteLine("FlowControl -> init");
            InitializeComponent();
            // Create the gesture recognizer.
            this.activeRecognizer = this.CreateRecognizer();

            // Wire-up window loaded event.
            /*Loaded += this.OnMainWindowLoaded;*/
            coverFactory = new CoverFactory(visualModel);
        }

        /// <summary>
        /// Hands-Free Kinect gesture recognizer.
        /// Swipe right to move right.
        /// Swipe left to move left.
        /// </summary>
        /// <returns></returns>
        private GestureController CreateRecognizer()
        {
            var gestureController = new GestureController();
            gestureController.GestureRecognized += (s, e) =>
            {
                if (e.GestureType == GestureType.SwipeRight)
                {
                    System.Diagnostics.Debug.WriteLine("FlowControl -> Detected SwipeRight");

                    Index++;
                    // Setup corresponding picture if pictures are available.
                    this.PreviousPicture = this.Picture;
                    this.Picture = this.NextPicture;
                    this.NextPicture = LoadPicture(Index + 1);

                    // Notify world of change to Index and Picture.
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("PreviousPicture"));
                        this.PropertyChanged(this, new PropertyChangedEventArgs("Picture"));
                        this.PropertyChanged(this, new PropertyChangedEventArgs("NextPicture"));
                    }

                    var storyboard = Resources["LeftAnimate"] as Storyboard;
                    if (storyboard != null)
                    {
                        storyboard.Begin();
                    }

                    HighlightSkeleton(e.TrackingID);
                }
            };

            return gestureController;
        }

        /// <summary>
        /// The paths of the picture files.
        /// </summary>
        private readonly string[] picturePaths = CreatePicturePaths();

        /// <summary>
        /// Get list of files to display as pictures.
        /// </summary>
        /// <returns>Paths to pictures.</returns>
        private static string[] CreatePicturePaths()
        {
            var list = new List<string>();

            var commonPicturesPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonPictures);
            list.AddRange(Directory.GetFiles(commonPicturesPath, "*.jpg", SearchOption.AllDirectories));
            if (list.Count == 0)
            {
                list.AddRange(Directory.GetFiles(commonPicturesPath, "*.png", SearchOption.AllDirectories));
            }

            if (list.Count == 0)
            {
                var myPicturesPath = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
                list.AddRange(Directory.GetFiles(myPicturesPath, "*.jpg", SearchOption.AllDirectories));
                if (list.Count == 0)
                {
                    list.AddRange(Directory.GetFiles(commonPicturesPath, "*.png", SearchOption.AllDirectories));
                }
            }

            return list.ToArray();
        }

        /// <summary>
        /// Load the picture with the given index.
        /// </summary>
        /// <param name="index">The index to use.</param>
        /// <returns>Corresponding image.</returns>
        private BitmapImage LoadPicture(int index)
        {
            BitmapImage value;

            if (this.picturePaths.Length != 0)
            {
                var actualIndex = index % this.picturePaths.Length;
                if (actualIndex < 0)
                {
                    actualIndex += this.picturePaths.Length;
                }

                Debug.Assert(0 <= actualIndex, "Index used will be non-negative");
                Debug.Assert(actualIndex < this.picturePaths.Length, "Index is within bounds of path array");

                try
                {
                    value = new BitmapImage(new Uri(this.picturePaths[actualIndex]));
                }
                catch (NotSupportedException)
                {
                    value = null;
                }
            }
            else
            {
                value = null;
            }

            return value;
        }

        /// <summary>
        /// Images are cached in the tumbnail manager to speed up animation/load times.
        /// </summary>
        public IThumbnailManager Cache
        {
            set { Cover.Cache = value; }
        }
        public void GoToNext()
        {
            UpdateIndex(Math.Min(index + 1, imageList.Count - 1));
        }
        public void GoToPrevious()
        {
            UpdateIndex(Math.Max(index - 1, 0));
        }
        public void GoToNextPage()
        {
            UpdateIndex(Math.Min(index + PageSize, imageList.Count - 1));
        }
        public void GoToPreviousPage()
        {
            UpdateIndex(Math.Max(index - PageSize, 0));
        }
        public int Count
        {
            get { return imageList.Count; }
        }
        /// <summary>
        /// Add a new image to the coverflow.
        /// </summary>
        /// <param name="host"></param>
        /// <param name="imagePath"></param>
        public void Add(string host, string imagePath)
        {
            Add(new ImageInfo(host, imagePath));
        }
    }
}
