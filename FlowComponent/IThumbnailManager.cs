﻿using System.Windows.Media;
namespace AIMS.FlowComponent
{
    public interface IThumbnailManager
    {
        ImageSource GetThumbnail(string host, string path);
    }
}
