//<SIMIS><===============================================================================><SIMIS>
//-//<copyright file company="SimIS Inc">
//-//
//-//SimIS Inc. and HealthCare Simulations, LLC CONFIDENTIAL
//-//
//-//[2014] SimIS Inc. and HealthCare Simulations, LLC Confidential
//-//
//-//All Rights Reserved.
//-//
//-//NOTICE:  All information contained herein is, and remains the property
//-//     of SimIS, Inc. and HealthCare Simulations, LLC and its suppliers, if
//-//     any.  The intellectual and technical concepts contained herein are
//-//     proprietary to SimIS Inc. and HealthCare Simulations, LLC and its
//-//     suppliers and may be covered by U.S. and Foreign Patents, patents
//-//     in process, and are protected by trade secret or copyright law.
//-//
//-//Dissemination of this information or reproduction of this material
//-//     is strictly forbidden unless prior written permission is obtained
//-//     from SimIS Inc. and HealthCare Simulations, LLC.
//-//</copyright>
//<SIMIS><===============================================================================><SIMIS>

using System;
using System.Runtime.InteropServices;

namespace Reprise
{
    public class RLM
    {
	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_putenv(String nvp);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_init(String lfPath, String progPath, String licString);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_init_disconn(String lfPath, String progPath, String licString, int promise);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_stat(IntPtr handle);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_checkout(IntPtr handle, String product, String version, int count);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_checkout_product(IntPtr handle, IntPtr product, String version, int count);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_license_stat(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern void rlm_checkin(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern void rlm_close(IntPtr handle);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_errstring(IntPtr license, IntPtr handle, byte[] sarray);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_errstring_num(int code, byte[] sarray);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_auth_check(IntPtr license, String licstring);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern void rlm_forget_isv_down(IntPtr handle);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_get_attr_health(IntPtr handle);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_get_attr_lfpath(IntPtr handle);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_hostid(IntPtr handle, int type, byte[] hostid);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_log(IntPtr handle, String message);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_dlog(IntPtr handle, String message);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_detached_demo(IntPtr handle, int days, String license);
	
	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_detached_demox(IntPtr handle, String product, String version);
	
	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    public static extern void rlm_skip_isv_down(IntPtr handle);

    [DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    public static extern IntPtr rlm_license_akey(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_license_cache(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_license_client_cache(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_license_contract(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_license_detached_demo(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_license_customer(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_license_exp(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_license_exp_days(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_license_goodonce(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_license_hold(IntPtr license);
	
	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_license_host_based(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_license_hostid(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_license_line_item(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_license_issued(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_license_issuer(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_license_max_roam(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_license_max_roam_count(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_license_max_share(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_license_min_checkout(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_license_min_remove(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_license_min_timeout(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_license_named_user_count(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_license_named_user_min_hours(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_license_options(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_license_platforms(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_license_product(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_license_roaming(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_license_server(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_license_single(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_license_share(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_license_soft_limit(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_license_start(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_license_type(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_license_tz(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_license_user_based(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_license_uncounted(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_license_ver(IntPtr license);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_products(IntPtr handle, String product, String version);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern void rlm_products_free(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern void rlm_product_first(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_next(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_product_name(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_product_contract(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_product_customer(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_product_exp(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_product_ver(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_count(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_exp_days(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_current_inuse(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_current_resuse(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_hbased(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_hold(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_product_issuer(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_max_roam(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_max_share(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_max_roam_count(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_min_remove(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_min_checkout(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_min_timeout(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_nres(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_num_roam_allowed(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_product_options(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_roaming(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_share(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_soft_limit(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_thisroam(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_timeout(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_tz(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_tokens(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_type(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_ubased(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_product_client_cache(IntPtr products);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern void rlm_set_active(IntPtr handle);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern void rlm_set_attr_keep_conn(IntPtr handle, int keep);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern void rlm_set_attr_logging(IntPtr handle, int log);
	
	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern void rlm_set_attr_password(IntPtr handle, String password);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern void rlm_set_environ(IntPtr handle, String user, String host, String isvString);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_act_request(IntPtr handle, String url, String isv, String key,
		String hostids, String hostname, int count, String extra, byte[] lic);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_act_keyvalid(IntPtr handle, String url, String key, String hostid);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_activate(IntPtr handle, String url, String key,
		int count, byte[] lic, IntPtr actHandle);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_act_new_handle(IntPtr handle);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_act_destroy_handle(IntPtr actHandle);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_act_set_handle(IntPtr actHandle, int what, IntPtr value);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern IntPtr rlm_act_revoke(IntPtr handle, String url,
		String product);

	[DllImport("rlm1102.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
	public static extern int rlm_act_info(IntPtr handle, String url, 
		String key, byte[] product, byte[] version, ref int date_based, 
        	ref int license_type, byte[] upgrade_version);

	public static String marshalToString(IntPtr ip)
	{
		return Marshal.PtrToStringAnsi(ip);
	}

	// Constants

	// Activation handle member settings
	public const int RLM_ACT_HANDLE_ISV	=1;
	public const int RLM_ACT_HANDLE_HOSTID_LIST =2;
	public const int RLM_ACT_HANDLE_HOSTNAME=3;
	public const int RLM_ACT_HANDLE_EXTRA	=4;
	public const int RLM_ACT_HANDLE_LOG	=5;
	public const int RLM_ACT_HANDLE_REHOST	=6;

	// Max string lengths
	public const int RLM_MAX_LINE      = 1024;	/* Maximum size of a LF line */
	public const int RLM_MAX_PRODUCT	= 40;   /* Max length of a product name */
	public const int RLM_MAX_VER	    = 10;	/* Max length of a version string vvvvvv.rrr */
	public const int RLM_MAX_ISV	    = 10;	/* Maximum ISV name length */
	public const int RLM_ERRSTRING_MAX	= 512;	/* string passed to rlm_errstring() */
	public const int RLM_MAX_LOG	    = 256;	/* Max length of a log string */
	public const int RLM_MAX_HOSTNAME	= 64;	/* Max length of a license server hostname */
	public const int RLM_MAX_USERNAME	= 32;	/* Max length of a username */
	public const int RLM_MAX_HOSTID	= RLM_MAX_HOSTNAME;	/* Max length of hostid data */
	public const int RLM_MAX_HID_KEYWORD_LEN	= 10;	/* Max length of a hostid type keyword */
	public const int RLM_MAX_HOSTID_STRING	= (RLM_MAX_HOSTID+RLM_MAX_HID_KEYWORD_LEN+2);

	// Hostid types
	public const int RLM_HOSTID_NONE	= 0;	/* No hostid specified, e.g. empty string passed to rlm_get_id() - new in v3.0BL4 */
	public const int RLM_HOSTID_32BIT	= 1;
	public const int RLM_HOSTID_STRING	= 2;	/* Equivalent to ANY, ie, works anywhere */
	public const int RLM_HOSTID_ETHER	= 3;
	public const int RLM_HOSTID_USER	= 4;	/* User= */
	public const int RLM_HOSTID_HOST	= 5;	/* Host= */
	public const int RLM_HOSTID_IP     = 6;	/* IP address */
	public const int RLM_HOSTID_ANY	= 7;	/* ANY - valid anywhere */
	public const int RLM_HOSTID_DEMO	= 8;	/* DEMO - valid anywhere */
	public const int RLM_HOSTID_INVALID= 9;	/* INVALID hostid - new in v3.0BL4 */
	public const int RLM_HOSTID_SN		= 10;	/* Serial # - just like STRING, new in v5.0 */
	public const int RLM_HOSTID_RLMID1	= 11;	/* First dongle type (aladdin) - new in v5.0 */
	public const int RLM_HOSTID_RLMID2	= 12;	/* Second dongle type (safenet) - new in v6.0 */
	public const int RLM_HOSTID_RLMID3	= 13;	/* Third dongle type (uniloc) - new in v6.0 */
	public const int RLM_HOSTID_DISKSN	= 14;	/* Disk serial number, new in v9.2 */
	public const int RLM_HOSTID_REHOST	= 15;	/* Rehostable hostid */
	public const int RLM_ISV_HID_TYPE_MIN	= 1000; /* Minimum type value for ISV-def hostid */
	public const int RLM_MAX_ISVDEF	= 32;	/* Max length of an ISV-defined field */

	// License types, from type= keyword
	public const int RLM_LA_BETA_TYPE	= 0x1;
	public const int RLM_LA_EVAL_TYPE	= 0x2;
	public const int RLM_LA_DEMO_TYPE	= 0x4;

	// License sharing definitions, from share= keyword
	public const int RLM_LA_SHARE_USER	= 1;		/* Share if username matches */
	public const int RLM_LA_SHARE_HOST	= 2;		/* Share if hostname matches */
	public const int RLM_LA_SHARE_ISV	= 4;		/* Share if isv-defined matches */

	// General Errors - returned by rlm_stat(handle)
	public const int RLM_EH_NOHANDLE		= -101;	/* No handle supplied to call */
	public const int RLM_EH_READ_NOLICENSE = -102;	/* Can't read license data */
	public const int RLM_EH_NET_INIT		= -103;	/* Network (msg_init()) error */
	public const int RLM_EH_NET_WERR		= -104;	/* Error writing to network */
	public const int RLM_EH_NET_RERR		= -105;	/* Error reading from network */
	public const int RLM_EH_NET_BADRESP	= -106;	/* Unexpected response */
	public const int RLM_EH_BADHELLO		= -107;	/* HELLO message for wrong server */

	public const int RLM_EH_BADPRIVKEY		= -108;	/* Error in private key */
	public const int RLM_EH_SIGERROR		= -109;	/* Error signing authorization */
	public const int RLM_EH_INTERNAL		= -110;	/* Internal error */

	public const int RLM_EH_CONN_REFUSED	= -111;	/* Connection refused at server */
	public const int RLM_EH_NOSERVER		= -112;	/* No server to connect to */
	public const int RLM_EH_BADHANDSHAKE	= -113;	/* Bad communications handshake */
	public const int RLM_EH_CANTGETETHER	= -114;	/* Can't get ethernet address */

	// system call failures */
	public const int RLM_EH_MALLOC			= -115;
	public const int RLM_EH_BIND			= -116;	/* bind() error */
	public const int RLM_EH_SOCKET			= -117;	/* socket() error */

	public const int RLM_EH_BADPUBKEY		= -118;	/* Error in public key */
	public const int RLM_EH_AUTHFAIL		= -119;	/* Authentication failed */
	public const int RLM_EH_WRITE_LF		= -120;	/* Can't write temp license file */
	public const int RLM_EH_NO_REPORTLOG	= -121;	/* No reportlog file on this server */
	public const int RLM_EH_DUP_ISV_HID	= -122;	/* ISV-defined hostid already registered */
	public const int RLM_EH_BADPARAM		= -123;	/* Bad parameter passed to RLM function */
	public const int RLM_EH_ROAMWRITEERR	= -124;	/* Roam File write error */
	public const int RLM_EH_ROAMREADERR	= -125;	/* Roam File read error */
	public const int RLM_EH_HANDLER_INSTALLED = -126;	/* heartbeat handler already installed */
	public const int RLM_EH_CANTCREATLOCK	= -127;	/* Cannot create 'single' lockfile */
	public const int RLM_EH_CANTOPENLOCK	= -128;	/* Cannot open 'single' lockfile */
	public const int RLM_EH_CANTSETLOCK	= -129;		/* Cannot set lock on 'single' */
	public const int RLM_EH_BADRLMLIC	= -130;		/* Bad/missing/expired RLM license */
	public const int RLM_EH_BADHOST	= -131;		/* Bad hostname in LF/port@host */
	public const int RLM_EH_CANTCONNECT_URL= -132;	/* Cannot connect to specified URL */
	public const int RLM_EH_OP_NOT_ALLOWED = -133;	/* Operation not allowed on server */
	public const int RLM_EH_ACT_BADSTAT	= -134;	/* Bad status from activation binary */
	public const int RLM_EH_ACT_BADLICKEY	= -135;	/* Bad license key in activation binary */
	public const int RLM_EH_BAD_HTTP	= -136;	/* Bad HTTP transaction */
	public const int RLM_EH_DEMOEXISTS	= -137;	/* Demo already created */
	public const int RLM_EH_DEMOWRITEERR	= -138;	/* Demo file write error */
	public const int RLM_EH_NO_DEMO_LIC	= -139;	/* No "rlm_demo" license available */
	public const int RLM_EH_NO_RLM_PLATFORM= -140;	/* RLM unlicensed on this platform */
	public const int RLM_EH_EVAL_EXPIRED	= -141;	/* RLM eval expired */
	public const int RLM_EH_SERVER_REJECT 	= -142;	/* Server rejected by client */ 
	public const int RLM_EH_UNLICENSED    	= -143;	/* Unlicensed RLM option */ 
	public const int RLM_EH_SEMAPHORE_FAILURE = -144;	/* Semaphore initialization failure */ 
	public const int RLM_EH_ACT_OLDSERVER	= -145;	/* Activation server too old */ 
	public const int RLM_EH_BAD_LIC_LINE	= -146;	/* Invalid license line in LF */ 
	public const int RLM_EH_BAD_SERVER_HOSTID = -147;	/* Invalid hostid on SERVER line */
	public const int RLM_EH_NO_REHOST_TOP_DIR = -148;	/* No rehostable hostid top-level dir */ 
	public const int RLM_EH_CANT_GET_REHOST	= -149;	/* Cannot get rehost hostid */
	public const int RLM_EH_CANT_DEL_REHOST	= -150;	/* Cannot delete rehost hostid */
	public const int RLM_EH_CANT_CREATE_REHOST = -151; 	/* Cannot create rehostable hostid */
	public const int RLM_EH_REHOST_TOP_DIR_EXISTS = -152; /* Rehostable top dir exists */
	public const int RLM_EH_REHOST_EXISTS = -153; 	/* Rehostable hostid exists */
	public const int RLM_EH_NO_FULFILLMENTS = -154; 	/* No fulfillments to revoke */
	public const int RLM_EH_METER_READERR = -155;	/* Meter read error */
	public const int RLM_EH_METER_WRITEERR = -156;	/* Meter write error */
	public const int RLM_EH_METER_BADINCREMENT = -157;	/* Bad meter increment command */
	public const int RLM_EH_METER_NO_COUNTER = -158;	/* Can't find counter in meter */
	public const int RLM_EH_ACT_UNLICENSED	= -159;	/* Activation Unlicensed */
	public const int RLM_EH_ACTPRO_UNLICENSED = -160;	/* Activation Pro Unlicensed */
	public const int RLM_EH_SERVER_REQUIRED = -161;	/* Counted license requires server */
	public const int RLM_EH_DATE_REQUIRED = -162;	/* REPLACE license requires date */
	public const int RLM_EH_NO_METER_UPGRADE = -163;	/* METERED licenses can't be UPGRADED */
	public const int RLM_EH_NO_CLIENT = -164;		/* disconnected client data can't be found */
	public const int RLM_EH_NO_DISCONN = -165;		/* op not permitted on disconn handle */
	public const int RLM_EH_NO_FILES	= -166;		/* Too many open files */
	public const int RLM_EH_NO_BROADCAST_RESP = -167;	/* No response from broadcast */
	public const int RLM_EH_NO_BROADCAST_HOST = -168;	/* broadcast didn't return hostname */
	public const int RLM_EH_SERVER_TOO_OLD	= -169;	/* Server too old to support this */
	public const int RLM_EH_BADLIC_FROM_SERVER = -170;	/* License from server doesn't authenticate */
	public const int RLM_EH_NO_LIC_FROM_SERVER = -171;	/* No license returned from server */
	public const int RLM_EH_CACHEWRITEERR = -172;	/* Roam file write error */
	public const int RLM_EH_CACHEREADERR = -173;	/* Roam file read error */


	// rlm_checkout() errors - returned by rlm_license_stat(license)
	public const int RLM_EL_NOPRODUCT		= -1;	/* No authorization for product */
	public const int RLM_EL_NOTME			= -2;	/* Authorization is for another ISV */
	public const int RLM_EL_EXPIRED		= -3;	/* Authorization has expired */
	public const int RLM_EL_NOTTHISHOST	= -4;	/* Wrong host for authorization */
	public const int RLM_EL_BADKEY			= -5;	/* Bad key in authorization */
	public const int RLM_EL_BADVER			= -6;	/* Requested version not supported */
	public const int RLM_EL_BADDATE		= -7;	/* bad date format - not permanent or dd-mm-yy */
	public const int RLM_EL_TOOMANY		= -8;	/* checkout request for too many licenses */
	public const int RLM_EL_NOAUTH			= -9;	/* No license auth supplied to call */
	public const int RLM_EL_ON_EXC_ALL		= -10;	/* On excludeall list */
	public const int RLM_EL_ON_EXC			= -11;	/* On feature exclude list */
	public const int RLM_EL_NOT_INC_ALL	= -12;	/* Not on the includeall list */
	public const int RLM_EL_NOT_INC		= -13;	/* Not on the feature include list */
	public const int RLM_EL_OVER_MAX		= -14;	/* Request would go over license MAX */
	public const int RLM_EL_REMOVED		= -15;	/* License (rlm)removed by server */
	public const int RLM_EL_SERVER_BADRESP	= -16;	/* Unexpected response from server */
	public const int RLM_EL_COMM_ERROR		= -17;	/* Error communicating with server */
	public const int RLM_EL_NO_SERV_SUPP	= -18;	/* License server doesn't support this */
	public const int RLM_EL_NOHANDLE		= -19;	/* No license handle */
	public const int RLM_EL_SERVER_DOWN	= -20;	/* Server closed connection */
	public const int RLM_EL_NO_HEARTBEAT	= -21;	/* No heartbeat response received */
	public const int RLM_EL_ALLINUSE		= -22;	/* All licenses in use */
	public const int RLM_EL_NOHOSTID		= -23;	/* No hostid on uncounted license */
	public const int RLM_EL_TIMEDOUT		= -24;	/* License timed out by server */
	public const int RLM_EL_INQUEUE		= -25;	/* In queue for license */
	public const int RLM_EL_SYNTAX			= -26;	/* License syntax error */
	public const int RLM_EL_ROAM_TOOLONG	= -27;	/* Roam time exceeds maximum */
	public const int RLM_EL_NO_SERV_HANDLE	= -28;	/* Server does not know this handle */
	public const int RLM_EL_ON_EXC_ROAM	= -29;	/* On roam exclude list */
	public const int RLM_EL_NOT_INC_ROAM	= -30;	/* Not on roam include list */
	public const int RLM_EL_TOOMANY_ROAMING= -31;	/* Too many licenses roaming */
	public const int RLM_EL_WILL_EXPIRE	= -32;	/* License expires before roam ends */
	public const int RLM_EL_ROAMFILEERR	= -33;	/* Problem with roam file */
	public const int RLM_EL_RLM_ROAM_ERR	= -34;	/* Cannot checkout rlm_roam license */
	public const int RLM_EL_WRONG_PLATFORM	= -35;	/* Wrong platform for client */
	public const int RLM_EL_WRONG_TZ		= -36;	/* Wrong timezone for client */
	public const int RLM_EL_NOT_STARTED	= -37;	/* License start date not reached */
	public const int RLM_EL_CANT_GET_DATE	= -38;	/* time() failure */
	public const int RLM_EL_OVERSOFT		= -39;	/* Over license soft limit */
	public const int RLM_EL_WINDBACK		= -40;	/* Clock setback detected */
	public const int RLM_EL_BADPARAM		= -41;	/* Bad parameter to checkout() call */
	public const int RLM_EL_NOROAM_FAILOVER= -42;	/* Roam operations not permitted on failover servers */
	public const int RLM_EL_BADHOST		= -43;	/* Bad hostname in LF/port@host */
	public const int RLM_EL_APP_INACTIVE	= -44;	/* Application is inactive */
	public const int RLM_EL_NOT_NAMED_USER	= -45;	/* Not a named user */
	public const int RLM_EL_TS_DISABLED	= -46;	/* Terminal Server disabled */
	public const int RLM_EL_VM_DISABLED	= -47;	/* Virtual Machines disabled */
	public const int RLM_EL_PORTABLE_REMOVED= -48;	/* Portable hostid removed */
	public const int RLM_EL_DEMOEXP	= -49;	/* Demo license has expired */
	public const int RLM_EL_FAILED_BACK_UP	= -50;	/* Failed host back up */ 
	public const int RLM_EL_SERVER_LOST_XFER = -51;	/* Server lost it's transferred license */ 
	public const int RLM_EL_BAD_PASSWORD	= -52;	/* Incorrect password for product */ 
					/* Note: RLM_EL_BAD_PASSWORD is an
					   internal error and won't ever be 
					   returned to the client - if the 
					   license password is bad, the client
					   will receive RLM_EL_NO_SERV_SUPP */
	public const int RLM_EL_METER_NO_SERVER	= -53;	/* Metered licenses require a server */
	public const int RLM_EL_METER_NOCOUNT	= -54;	/* Not enough count for metered lic */
	public const int RLM_EL_NOROAM_TRANSIENT = -55;    /* Roam operations not permitted on servers with transient hostids */
	public const int RLM_EL_CANTRECONNECT  = -56;    /* Cannot reconnect to server */
	public const int RLM_EL_NONE_CANROAM   = -57;    /* None of these licenses can roam */
	public const int RLM_EL_SERVER_TOO_OLD = -58;    /* Server too old for operation */


	public const int RLM_ACT_BADPARAM	= -1001;
	public const int RLM_ACT_NO_KEY	= -1002;
	public const int RLM_ACT_NO_PROD	= -1003;	/* No product definition */
	public const int RLM_ACT_CANT_WRITE_KEYS = -1004;	/* Can't write keyf table */
	public const int RLM_ACT_KEY_USED	= -1005;	/* Activation key already used*/
	public const int RLM_ACT_BAD_HOSTID	= -1006;	/* Missing hostid */
	public const int RLM_ACT_BAD_HOSTID_TYPE	= -1007;	/* Invalid hostid type */
	public const int RLM_ACT_BAD_HTTP	= -1008;	/* Bad HTTP transaction - 
							UNUSED after v3.0BL4 */
	public const int RLM_ACT_CANTLOCK	= -1009;	/* Can't lock activation DB */
	public const int RLM_ACT_CANTREAD_DB	= -1010;	/* Can't read activation DB */
	public const int RLM_ACT_CANT_WRITE_FULFILL	= -1011;/* Can't write licf table */
	public const int RLM_ACT_CLIENT_TIME_BAD	= -1012;	/* Clock incorrect on client */
	public const int RLM_ACT_BAD_REDIRECT	= -1013;	/* Bad redirect from server */
	public const int RLM_ACT_TOOMANY_HOSTID_CHANGES = 1014;/* Too many hostid changes */
								/*  for Refresh activation */ 
	public const int RLM_ACT_BLACKLISTED	= -1015;	/* Domain on blacklist */ 
	public const int RLM_ACT_NOT_WHITELISTED	= -1016;	/* Domain not on whitelist */ 
	public const int RLM_ACT_KEY_EXPIRED	= -1017;	/* Activation key expired */ 
	public const int RLM_ACT_NO_PERMISSION	= -1018;	/* HTTP request denied */ 
	public const int RLM_ACT_SERVER_ERROR	= -1019;	/* HTTP internal server error */ 
	public const int RLM_ACT_BAD_GENERATOR	= -1020;	/* Bad/missing generator file */ 
	public const int RLM_ACT_NO_KEY_MATCH	= -1021;	/* No matching key in DB */ 
	public const int RLM_ACT_NO_AUTH_SUPPLIED = -1022;	/* No proxy auth supplied */ 
	public const int RLM_ACT_PROXY_AUTH_FAILED = -1023;	/* proxy auth failed */ 
	public const int RLM_ACT_NO_BASIC_AUTH	= -1024;	/* No basic authentication */ 
	public const int RLM_ACT_GEN_UNLICENSED = -1025;	/* Activation generator unlicensed */ 
	public const int RLM_ACT_DB_READERR	= -1026;	/* Activtion DB read error (MySQL) */
	public const int RLM_ACT_GEN_PARAM_ERR	= -1027;	/* Generating license - bad parameter */
	public const int RLM_ACT_UNSUPPORTED_CMD= -1028;	/* Unsupported command to generator */
	public const int RLM_ACT_REVOKE_TOOLATE	= -1029;	/* Revoke command after expiration */

	// Activation types returned from rlm_act_info
	public const int RLM_ACT_LT_FLOATING = 0; /* Floating */
	public const int RLM_ACT_LT_F_UPGRADE = 4; /* Floating UPGRADE */
	public const int RLM_ACT_LT_UNCOUNTED = 1; /* Nodelocked, Uncounted */
	public const int RLM_ACT_LT_NLU_UPGRADE = 5; /* Nodelocked, Uncounted UPGRADE */
	public const int RLM_ACT_LT_SINGLE = 3; /* Single */
	public const int RLM_ACT_LT_S_UPGRADE = 7; /* Single UPGRADE */
    }
}
